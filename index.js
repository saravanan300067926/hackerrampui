'use strict';
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
const env = process.env.NODE_ENV;



/*NEWRELIC*/
let newrelic, pmxMonitor;
if (process.env.NEW_RELIC_ENABLE_NODEAPP) {
  newrelic = require('newrelic');
  pmxMonitor = require("./dist/tools/pmxMonitor")
}
// for the m-agent to work it overrides the __non_webpack_require__ with require
// so that http require works
global.__non_webpack_require__ = require;

const express = require('express');
const logger = require('morgan');
const path = require('path');
const http = require('http');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const slashes = require("connect-slashes");
const lowercasePaths = require("./src/server/lowerCasePath");
const app = express();
const signalHandler = require("./dist/tools/processSignalHandler").default;

const dir = p => path.join(__dirname, p);

if (newrelic) {
  app.locals.newrelic = newrelic;
}

require('./devSetup')(app);
let routes = require('./dist/routes');

app.engine('html', require('express-dot-engine').__express);
app.set('views', dir('./public/'));
app.set('view engine', 'html');

app.use(logger('DESKTOP_REQUEST :date[iso] :method :url HTTP/:http-version :status :res[content-length] - :response-time ms'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('compression')());
app.use(lowercasePaths());
app.use(slashes(false));
app.use('/tools', express.static(path.join(__dirname, 'node_modules/sw-toolbox')));
app.get('/sw.js', function (req, res) {
  res.set({
    'Content-Type': 'application/javascript',
    'Cache-Control': 'private, no-cache, must-revalidate, max-age=0',
    "Service-Worker-Allowed": "/",
    'Expires': '-1',
    'Pragma': 'no-cache'
  });
  res.sendFile(__dirname + '/public/sw.js');
});
app.get('/swnew.js', function (req, res) {
  res.set({
    'Content-Type': 'application/javascript',
    'Cache-Control': 'private, no-cache, must-revalidate, max-age=0',
    "Service-Worker-Allowed": "/",
    'Expires': '-1',
    'Pragma': 'no-cache'
  });
  res.sendFile(__dirname + '/public/sw.js');
});
app.get("/healthcheck", (req, res) => res.status(200).end());
app.use('/public', express.static(path.join(__dirname, 'public')));
app.get(/\.png$|\.jpg$|\.jpeg$|\.ico$|\.gif$/, function (req, res, next) {
  res.status(404).send('not found');
});


app.set('port', 7500);
routes.run(app);
const server = http.createServer(app);
server.listen(app.get('port'));
server.on('listening', () => console.log('Http server listening on port', app.get('port')));

//signalHandler(server, app);

module.exports = app;
