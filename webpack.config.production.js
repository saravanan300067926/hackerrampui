let webpack = require("webpack"); //to access built-in plugins
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const cssnano = require("cssnano");
const TerserPlugin = require("terser-webpack-plugin");
const postCssConstants = require('postcss-constants');
const COLORS = require('./src/app/resources/colors');

module.exports = {
  mode: "production",
  output: {
    publicPath: "https://constant.myntassets.com/web/assets/",
    filename: "js/[name].[contenthash].js",
    chunkFilename: "js/[name].[contenthash].js"
  },
  optimization: {
    runtimeChunk: {
      name: "manifest"
    },
    splitChunks: {
      chunks: "all",
      cacheGroups: {
        vendors: false,
        default: false,
        vendor: {
          test: /[\\/]node_modules[\\/](react|react-redux|react-loadable|react-router|react-dom|redux-saga|react-router-redux|promise|v-at|bus|jsuri|lscache|compactnums|react-lazyload|madalytics-web|classnames|lodash|core-js|@myntra\/m-agent)[\\/]/,
          name: "vendor",
          priority: 10,
          chunks: "all"
        }
      }
    },
    minimizer: [
      new TerserPlugin({
        sourceMap: false,
        terserOptions: {
          compress: {
            drop_console: true
          },
          output: {
            beautify: false,
            comments: false
          }
        }
      }),
      new OptimizeCSSAssetsPlugin({
        assetNameRegExp: /\.css$/g,
        cssProcessor: cssnano,
        cssProcessorPluginOptions: {
          preset: ["default", { discardComments: { removeAll: true } }]
        },
        canPrint: true
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        exclude: /(src\/client\/components\/globalStyles)/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: true,
              url: true,
              localIdentName: '[path][name]-[local]'
            }
          },
          {
            loader: "less-loader",
            options: {
              strictMath: true,
              noIeCompat: true
            }
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [
                require('autoprefixer'),
                postCssConstants({
                  defaults: {
                    colors: COLORS
                  }
                })
              ]
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production")
      }
    }),
    new webpack.HashedModuleIdsPlugin(),
    new MiniCssExtractPlugin({
      filename: "css/[name].[contenthash].css"
    })
  ]
};
