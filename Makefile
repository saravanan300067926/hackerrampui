export PATH := /opt/rh/devtoolset-2/root/usr/bin:$(PATH)
export MANPATH := /opt/rh/devtoolset-2/root/usr/share/man:$(MANPATH)
export INFOPATH := /opt/rh/devtoolset-2/root/usr/share/info:$(INFOPATH)
export PCP_DIR := /opt/rh/devtoolset-2/root
export PERL5LIB := /opt/rh/devtoolset-2/root//usr/lib64/perl5/vendor_perl:/opt/rh/devtoolset-2/root/usr/lib/perl5:/opt/rh/devtoolset-2/root//usr/share/perl5/vendor_perl:$(PERL5LIB)
export LOGDIR:=/myntra/nodeuser/myntraweb/releases/current/logs
#export LOGDIR:=~/dev/myntraweb/logs
clean:
	rm -rf dist && rm -rf public && rm -rf _nm.tgz;
dev:
	NODE_ENV=development NODE_SUBENV=stage npm run dev
debug:
	NODE_ENV=development NODE_SUBENV=dev npm run debug;
prod:
	NODE_ENV=production NODE_SUBENV=production npm run build && npm run cdn && NODE_ENV=production npm start;
local-setup:
	rm -rf node_modules npm  --registry https://registry.myntra.com install && make build;
setup:
	rm -rf node_modules && NODE_ENV=production npm  --registry https://registry.myntra.com install && NODE_ENV=production NODE_SUBENV=production npm run build;
install:
	NODE_ENV=production npm  --registry https://registry.myntra.com install && make build;
build:
	NODE_ENV=development npm run build;
cdn:
	echo "no cdn task";
live:
	touch .live;
dead:
	rm -rf .live;
release:
	rm -rf node_modules && NODE_ENV=production NODE_SUBENV=fox9 npm  --registry https://registry.myntra.com install && NODE_ENV=production NODE_SUBENV=fox9 npm run build && node ./src/utils/pushToCDN.js && NODE_ENV=production NODE_SUBENV=fox9 pm2 start index.js -f --name myntraweb -i max --node-args="--harmony --harmony_arrow_functions";
soft-release:
	NODE_ENV=production NODE_SUBENV=fox9 npm run build && node ./src/utils/pushToCDN.js && NODE_ENV=production NODE_SUBENV=fox9 pm2 start index.js -f --name myntraweb -i max --node-args="--harmony --harmony_arrow_functions";
serve:
	NODE_ENV=production NODE_SUBENV=production pm2 start index.js -f --name myntraweb -i max --merge-logs -l logs/err.log
kill-serve:
	rm -f .live; sleep 10;
	pm2 delete myntraweb || echo "WARN: myntraweb was not running on PM2.";
todo:
	npm run todo;
tar:
	tar -czf _nm.tgz node_modules

.PHONY: tar
