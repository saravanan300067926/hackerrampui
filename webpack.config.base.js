let path = require("path");
let webpack = require("webpack");
let HtmlWebpackPlugin = require("html-webpack-plugin");
let BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
let AssetsPlugin = require("assets-webpack-plugin");

module.exports = {
  context: path.resolve(__dirname, "src"),
  entry: {
    main: [
      'core-js/es6/symbol',
      'core-js/es6/promise',
      'core-js/fn/array/includes',
      'core-js/fn/string/includes',
      'core-js/fn/array/find',
      'core-js/fn/array/from',
      'core-js/fn/object/assign',
      'core-js/fn/object/values',
      'core-js/fn/array/find-index',
      './app/pages/app.js'
    ]
  },
  stats: { children: false },
  output: {
    path: path.resolve(__dirname, "public"),
    publicPath: "/public/",
    filename: "[name].js",
    chunkFilename: "[name].js"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.(png|jpe?g|svg)$/i,
        loader: "file-loader"
      }
    ]
  },
  resolve: {
    symlinks: false
  },
  plugins: [
    new webpack.ProgressPlugin(),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/templates/', 'index.html'),
      inject: false,
      filename: "shell.html",
      excludeChunks: ["manifest"],
      minify: {
        collapseWhitespace: true,
        minifyJS: true,
        minifyCSS: true
      }
    }),

    new AssetsPlugin({
      includeManifest: true,
      manifestFirst: true,
      includeAllFileTypes: false
    }),

    new BundleAnalyzerPlugin({
      analyzerMode: "static",
      openAnalyzer: false
    })
  ]
};
