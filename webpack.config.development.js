let path = require("path");
let webpack = require("webpack");
const postCssConstants = require('postcss-constants');
const COLORS = require('./src/app/resources/colors');

module.exports = {
  mode: "development",
  entry: {
    main: ["webpack-hot-middleware/client?reload=true"]
  },
  devtool: "source-map",
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: "style-loader" },
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: true,
              url: true,
              localIdentName: '[path][name]-[local]'
            }
          },
          {
            loader: "less-loader",
            options: {
              strictMath: true,
              noIeCompat: true
            }
          },
          {
            loader: "postcss-loader",
            options: {
              plugins: [
                postCssConstants({
                  defaults: {
                    colors: COLORS
                  }
                })
              ]
            }
          }
        ]
      }
    ]
  }
};
