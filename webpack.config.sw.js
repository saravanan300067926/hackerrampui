let webpack = require("webpack");
let CompressionPlugin = require("compression-webpack-plugin");
let path = require("path");
const TerserPlugin = require("terser-webpack-plugin");

let config = {
  mode: "production",
  entry: {
    sw: "./src/sw/sw.js"
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        sourceMap: false,
        terserOptions: {
          compress: {
            drop_console: true
          },
          output: {
            beautify: false,
            comments: false
          }
        }
      })
    ]
  },
  output: {
    path: path.resolve(__dirname, "public"),
    publicPath: "/public/",
    filename: '[name].js'
  },
  module: {
    noParse: [/dtrace-provider$/, /safe-json-stringify$/, /mv/], //bunyan excludes
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production")
      }
    }),
    new CompressionPlugin({
      test: /.js/,
      filename: "[path].gz",
      algorithm: "gzip",
      minRatio: 0.8
    })
  ]
};

module.exports = config;
