//dev setup
let webPackConfig = require("./webpack.config");
let compiler = require("webpack")(webPackConfig);
let errorhandler = require("errorhandler");

module.exports = function(app) {
  if (process.env.NODE_ENV == "development") {
    app.use(errorhandler());
    app.use(
      require("webpack-dev-middleware")(compiler, {
        noInfo: true,
        quiet: false,
        publicPath: webPackConfig.output.publicPath,
        lazy: false,
        watchOptions: {
          aggregateTimeout: 500,
          poll: true
        },
        stats: {
          colors: true,
          chunks: false,
          children: false
        }
      })
    );
    app.use(require("webpack-hot-middleware")(compiler));
  }
};
