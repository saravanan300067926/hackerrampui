const merge = require('webpack-merge');
const base = require('./webpack.config.base');
const dev = require('./webpack.config.development');
const prod = require('./webpack.config.production');
const env = process.env.NODE_ENV;

let config = {};
if (env === 'production') {
  console.log('webpack: prod config selected');
  config = merge(base, prod);
} else {
  console.log('webpack: dev config selected');
  config = merge(base, dev);
}

module.exports = config;
