#!/bin/sh

app=myntraweb
port=$NODE_PORT
instances=max
merge=""

if [ -z "$port" ]; then
    port='7500'
fi

if [ -z "$NODE_ENV" ]; then
    NODE_ENV="development"
fi

if [ "$NODE_ENV" == "development" ]; then
    instances=2
    merge="--merge-logs"
fi

if [ "$NODE_ENV" == "test" ]; then
    instances=2
fi

if [ "$NODE_SUBENV" != "" ]; then
    app=$app"-"$NODE_SUBENV
fi

