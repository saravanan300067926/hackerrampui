import pmx from "@pm2/io";
import heapdump from "heapdump";
import fs from "fs";

//CMD: pm2 trigger myntraweb heapdump
pmx.action("heapdump", (reply) => {
  if ( ! fs.existsSync("./heap") ) {
    fs.mkdirSync("./heap");
  }
  const filename = "./heap/heap_" + Date.now() + ".heapsnapshot";
  heapdump.writeSnapshot(filename, (err, filename) => console.log(err, filename));
  reply("Wrote heapdump to " + filename);
});
