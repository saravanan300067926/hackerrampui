export default function signalHandler(expressServer, app) {
  expressServer.on("error", function(err) {
    console.error(
      "Server start failed: %s port: %s, env: %s, version: %s",
      err,
      app.get("env"),
      process.version
    );
    process.exit(1);
  });

  const events = [
    [ "uncaughtException", 1 ],
    [ "SIGINT", 128 + 2 ],
    [ "SIGTERM", 128 + 15 ]
  ];

  process.on("unhandledRejection", function(event) {
    console.info("Supressing unhandledRejection");
  });

  events.forEach(function(event) {
    const signal = event[0];
    const code = event[1];
    process.on(signal, function() {
      console.error("Process Terminating With Signal " + signal);
      expressServer.close(function(err) {
        console.error("Finished all pending requests. Terminating now.");
        process.exit(code);
      });
    })
  })
}
