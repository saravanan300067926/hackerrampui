'use strict';
const fs = require('fs');
const publicFolder = './public/';
const cssPath = publicFolder + 'css/';
const jsPath = publicFolder + 'js/';

let files = [];

if (fs.existsSync(jsPath)) {
  fs.readdirSync(jsPath).forEach(file => {
    files.push('js/' + file);
  });  
}

if (fs.existsSync(cssPath)) {
  fs.readdirSync(cssPath).forEach(file => {
    files.push('css/' + file);
  });
}

let assetMap = {
  main: {},
  chunks: [
  ]
};

for (let asset of files) {
  if (asset.indexOf('.css') >=0 ) {
    assetMap.main.css = asset;
  } else if (asset.indexOf('.js') >=0 && asset.indexOf('main') >=0 ) {
    assetMap.main.js = asset;
  } else {
    assetMap.chunks.push(asset);
  }
}
fs.writeFileSync('webpack-assets.json', JSON.stringify(assetMap));
