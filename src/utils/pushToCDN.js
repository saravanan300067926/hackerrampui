'use strict';

const cdn = require('./cdn');
const prefix = require('./assetify').prefix;
const fs = require('fs');
const publicFolder = './public/';
const cssPath = publicFolder + 'css/';
const jsPath = publicFolder + 'js/';

let files = [];

fs.readdirSync(jsPath).forEach(file => {
  files.push(jsPath + file);
});
fs.readdirSync(cssPath).forEach(file => {
  files.push(cssPath + file);
});

const params = { prefix : prefix , replace : false };

files.forEach((file) => {
	cdn.push(file,params,(err, data) => {
		if (err) {
		console.error(err);
		return;
		}
		console.log('DONE: ' + data.uri);
	});
});
