'use strict';

const prefix = 'web/assets';

module.exports = function resolve(filename, protocol) {
  if (process.env.NODE_ENV === 'development') {
    return '/' + filename;
  }
  let path = 'http://myntra.myntassets.com/' + prefix + '/';
  if (protocol === "https") {
    path = 'https://constant.myntassets.com/' + prefix + '/';
  }
  return path + filename;
};
module.exports.prefix = prefix;
