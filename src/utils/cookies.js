/**
 * Until myx cookies implementation is fixed, use this factory to avoid
 * writing the entire path everytime.
 * Possible solution: Create a new function and export from myx, which creates a new cookies object
 * for your request, response pair. ToDo fix it.
 *
 * @param req
 * @param res
 * @return {cookies|undefined}
 */
export default function Cookies(req, res) {
  return require('@myntra/myx/lib/server/cookies')(req, res);
}
