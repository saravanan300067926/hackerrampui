import fs from 'fs';
import path from 'path';
let jsName = null;
let cssName = null;

export const getFileNames = (callback) => {
  const pathForFile = path.join(__dirname, '../../public');
  fs.readdir(pathForFile, (err, items) => {
    for (let i = 0; i < items.length; i++) {
      if (items[i].indexOf('headerfooter') !== -1) {
        if (items[i].indexOf('js.gz') !== -1) {
          jsName = items[i];
        } else if (items[i].indexOf('css.gz') !== -1) {
          cssName = items[i];
        }
      }
    }
    console.log({ css: cssName, 'js': jsName });
    callback({ css: cssName, 'js': jsName });
  });
};
