"use strict";

var mcookies = require('@myntra/myx/lib/server/cookies');
var fgList = require('./features').fgList;
var kvList = require('./kvpairs').kvList;

// default configs
var kvpairs = require('./defaults/kvpairsDefault.js');
var features = require('./defaults/featuresDefault.js');

module.exports = function(req, res, next) {
  // Define the values =>
  if (!req.switch || typeof req.switch !== 'object') {
    req.switch = {};
  }

  req.switch.features = {
    namespace: 'portal-features',
    list: fgList,
    tenantId: 'myntra',
    options: {
      username: 'checkout',
      password: 'checkout@123'
    },
    defaultConfig: features
  };

  req.switch.kvpairs = {
    namespace: 'portal-kvpairs',
    list: kvList,
    tenantId: 'myntra',
    options: {
      username: 'checkout',
      password: 'checkout@123'
    },
    defaultConfig: kvpairs
  };

  var cookies = mcookies(req, res);
  var portalSwitchVariantValue = cookies.get('_pv');

  if (req.myx && req.myx.apps && req.myx.apps.isPhonePeWebview) {
    portalSwitchVariantValue = 'phonePeStore';
  }

  if(!portalSwitchVariantValue) {
    /**
     * Variant not found in cookie or in header, therefore,
     * pick variant from a/b test.
     */
    var variantNameFromABTest = req.myx._mxab_ ? (req.myx._mxab_['config.bucket'] || '') : '';
    if(variantNameFromABTest) {
    } else {
      variantNameFromABTest = 'default';
    }
    req.switch.features.variants = [variantNameFromABTest];
    req.switch.kvpairs.variants = [variantNameFromABTest];
    cookies.set('_pv', variantNameFromABTest, {expires: new Date(Date.now() + 5*60*1000), httpOnly: true});
    next();
  } else {
    // if cookie exists, just forward that value, as an array.
    req.switch.features.variants = [portalSwitchVariantValue];
    req.switch.kvpairs.variants = [portalSwitchVariantValue];
    next();
  }
};
