'use strict';

var keys = [
  'web.taxBanner',
  'admissioncontrol.popup.data',
  'pdp.emi.plans',
  'pdp.emi.terms',
  'shipping.charges.cartlimit',
  'myntraweb.promoheader.data',
  'hrdr.salebanner.data',
  'hrdr.salebanner.data',
  'app.android.dlink',
  'app.windows.dlink',
  'app.ios.dlink',
  'pdp.offers.visible',
  'pdpurlchange.categories',
  'prelaunch.brands',
  'earlyslot.config',
  'hrdr.pricereveal.data',
  'oneTapLogin',
  'priorityCheckout.config',
  'sessionExpirySecs',
  'atTokenExpirySecs',
  'rtTokenExpirySecs',
  'extraDevices',
  'pwaNewUserOnboarding.halfCards',
  'pwaNewUserOnboarding.banners',
  'pwaNewUserOnboarding.feedBanners',
  'desktopNewUserOnboarding.freeShipping',
  'pdp.contentRevamp.FTF.exclutionList',
  'xceleratorTagsPriority',
  'xceleratorTagsConfig',
  'product.emi.info',
  'referAndEarn',
  'web.pincodecheck.msg'
];

module.exports = {
  kvList: keys
};
