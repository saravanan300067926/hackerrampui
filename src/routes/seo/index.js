"use strict";

var urls = require('./urls'),
  _ = require('lodash'),
  service = {};//require('myntcommon/lib/seo');

const {
  createOldPDPUrl,
  isNewPDPUrl,
  updateSEOUrls
} = require('./newPdpSeoSupport');

const getPdpMetaData = (originalUrl, data, callback) => {
  let newPdpUrl = false;
  let url = originalUrl;
  if (isNewPDPUrl(url)) {
    newPdpUrl = true;
    url = createOldPDPUrl(data);
  }
  service.getPDPMetadataWeb(url, [data], (err, meta) => {
    if (newPdpUrl && meta) updateSEOUrls(meta, originalUrl);
    callback(err, meta);
  });
}

function seo(url, data = {}, _callback) {
  // on node, you're expected to pass in the data

  var callback = function (err, res) {
    if (res && !err) {
      res.title = res.title || (res.metaData || {}).title;
    }
    _callback && _callback(err, res);
  };

  var type = urls.type(url);
  return callback();
  url = url.charAt(0) === '/' ? url.slice(1) : url;
  if ((type === 'list' && _.isEmpty(data.products)) || ((type === 'pdp' && _.isEmpty(data)))) {
    return callback();
  }

  switch (type) {
    case 'list':
      service.getSearchMetadata(url, data, callback);
      break;
    case 'pdp':
      getPdpMetaData(url, data, callback);
      break;
    case 'home':
      service.getStaticPageMetadata(url, callback);
      break;
    case 'shop':
      service.getStaticPageMetadata(url, callback);
      break;
    default:
      callback();
      // service.getStaticPageMetadata(url, callback);
      break;
  }
}

module.exports = seo;
