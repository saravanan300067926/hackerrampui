const get = require('lodash/get');

const HOST = "https://www.myntra.com/";

const REGEX = /\.*-(\d+)-buy.htm/;

const createOldPDPUrl = (data) => {
    const { id, analytics, name } = data || {};
    const brandName = get(analytics, "brand", "");
    const category = get(analytics, "articleType", "");
    return `/${category}/${brandName}/${name}/${id}/buy`.toLowerCase().replace(/ /g, "-");
};

const isNewPDPUrl = (url) => REGEX.test(url);

const getIdFromUrl = (url) => {
    const idArr = url.match(REGEX);
    return idArr && idArr.length > 1 ? idArr[1] : null;
};

const updateSEOUrls = (data, originalUrl) => {
    data.og_url = `${HOST}${originalUrl}`;
    data.canonicalUri = `${HOST}${originalUrl}`;
  };
  
module.exports = {
    createOldPDPUrl,
    isNewPDPUrl,
    getIdFromUrl,
    updateSEOUrls
};
