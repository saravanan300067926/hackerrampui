"use strict";
var _ = require('lodash'),
    parseUri = require('./parseuri'),
    Uri = require('jsuri'),
    toString = {}.toString;

function pathRegexp(path, keys, sensitive, strict, end) {
    if (toString.call(path) == '[object RegExp]') return path;
    if (Array.isArray(path)) path = '(' + path.join('|') + ')';
    path = path.concat(strict ? '' : '/?').replace(/\/\(/g, '(?:/').replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?(\*)?/g, function(_, slash, format, key, capture, optional, star) {
        keys.push({
            name: key,
            optional: !! optional
        });
        slash = slash || '';
        return '' + (optional ? '' : slash) + '(?:' + (optional ? slash : '') + (format || '') + (capture || (format && '([^/.]+?)' || '([^/]+?)')) + ')' + (optional || '') + (star ? '(/*)?' : '');
    }).replace(/([\/.])/g, '\\$1').replace(/\*/g, '(.*)');
    return new RegExp('^' + path + ((end) ? '$' : ''), sensitive ? '' : 'i');
}
var patterns = {
    'home': ['/'],
    'cart': ['/cart', '/cart/:action'],
    'coupon': ['/coupon', '/coupon/:action'],
    'catalog': ['/catalog', '/catalog/:node'],  
    'notification': ['/my-notifications', '/my-notifications/:action'],
    'shop': ['/shop/:shop'],
    'login': ['/login', '/register', '/forgot','/login/:action'],
    'filters': ['/:search/filters', '/:search/filters/:filter'],
    'pdp': ['/:category/:brand/:style/:styleid/buy', '/*-:styleid-buy.htm$', '/:styleid(\\d+)'],
    'list': ['/:search', '/search/:search'],
    'size-chart': ['/size-chart/:styleid']
};
var _patterns = _.map(patterns, function(arr, key) {
    arr.type = key;
    return {
        key: key,
        arr: arr
    };
});

function _type(uri) {
    uri = parseUri(uri).path;
    var type = (_.find(_patterns, function(spec, i) {
        var arr = spec.arr,
            type = spec.key;
        return _.find(arr, function(pattern) {
            return pathRegexp(pattern, [], false, false, true).test(uri);
        }) ? type : undefined;
    }) || {
        key: 'default'
    }).key;
    if (type === 'list') {
        //couple of special cases
        if (bypass(uri)) {
            type = 'default';
            if (!isNaN(+uri.slice(1))) {
                type = 'pdp';
            }
        }
    }
    return type;
}

//Using a new list where m-site shutdown won't appear
var oldnonterms = [
    'my',
    'home', 'index',
    'catalog',
    'login', 'signin', 'register', 'signup',
    'mymyntra',
    'cart',
    'wishlist',
    'checkout', 'payments',
    'aboutus', 'contactus', 'faqs', 'privacypolicy', 'termsofuse',
    'giftcards', 'brands',
    'lookgood-helpline',
    'recently-viewed',
    'careers',
    'lookgood',
    'premium',
	'deals',
    'mystylist',
    'sdvo'];

var nonterms = ['careers', 'faqs', 'privacypolicy', 'termsofuse', 'contactus'];

function bypass(pathname) {
    pathname = pathname.split('?')[0];

    // remove leading, trailing slashes)
    if (pathname.charAt('0') === '/') {
        pathname = pathname.slice(1);
    }
    if(pathname[pathname.length - 1] === '/'){
        pathname = pathname.slice(0, pathname.length - 1);
    }

    if (_.chain(pathname.split('/')).filter(function(x) {
        return !!x;
    }).value().length > 1) {
        return true;
    }
    if (pathname.match(/\.php/) || (_.indexOf(nonterms, pathname.toLowerCase()) >= 0)) {
        return true;
    }
    // if pdp id
    if (!isNaN(+pathname)) {
        return true;
    }
    return false;
}

function searchUrl(query) {
    query = query.charAt(0) === '/' ? query.slice(1) : query;
    return '/' + query.replace(/\s/img, '-').toLowerCase();
}

function querify(url) {
    if (_type(url) === 'list') {
        var uri = new Uri(url),
            term = decodeURIComponent(uri.path()).slice(1).replace(/\-/img, ' ').replace(/\//img, ''),
            filters = decodeURIComponent(uri.getQueryParamValue('f') || '') || undefined,
            sort = uri.getQueryParamValue('sort'),
            page = +uri.getQueryParamValue('p'),
            rows = +(uri.getQueryParamValue('rows')||48),
            start = +(uri.getQueryParamValue('start') || 0),
            return_docs = uri.getQueryParamValue('return_docs') !== 'false',
            is_facet = uri.getQueryParamValue('is_facet') !=='false';

        var o = {
            query: term,
            filters: filters,
            sort: sort,
            rows: rows,
            start: page ? rows * (page - 1) : start,
            // return_docs: return_docs, // this causes a nasty bug. trim it before sending out instead.
            return_docs: true,
            is_facet: is_facet
        };
        return o;
    }
}

// sanitize the url parameter passed in to only respect relative path
function sanitize(url){
    if (url && url!="undefined") { // undefined check needed as decodeURIComponent was sending it as string.
        // list of domains in regex. please note add $ at end each white listed domain. ( to avoid myntra.com.hacksite.com pattern)
        var whitelistedDomains = [
            /.*\.myntra\.com$/,
            /.*\.flipkart\.com$/
        ];

        var urlObj = new Uri(url);
        for (var i = 0; i < whitelistedDomains.length; i++) {
            // if true the pass entire url
            if (whitelistedDomains[i].test(urlObj.host())) {
                return urlObj.toString();
            } else {
                return new Uri().setPath(urlObj.path()).setQuery(urlObj.query());
            }
        }
    }
    return "/";
}


module.exports = {
    nonterms: nonterms,
    patterns: patterns,
    bypass: bypass,
    type: _type,
    pathRegexp: pathRegexp,
    searchUrl: searchUrl,
    querify: querify,
    sanitize: sanitize
};
