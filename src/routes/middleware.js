"use strict";

var when = require('@myntra/myx/lib/when'),
  async = require('@myntra/myx/lib/async'),
  switchMiddleware = require('switch-express').default,
  switchKeys = require('../switch');

import { push } from '../utils/pump';
import Jsuri from 'jsuri';
import render from '../app/utils/render';
import outline from '@myntra/myx/lib/outline';
const semver = require('semver');

// 'common' middleware
var chain = [
  function (req, res, next) {
    req.myx = req.myx || {};
    req.startTime = new Date().getTime();
    next();
  }, 'apps', 'servejs', 'https', 'device', 'cookies/uidx', 'cookies/abtests', switchKeys, switchMiddleware, 'cookies/sizechart', 'cookies/pumpKeys', 'imageSupport', 'hrdr', 'env', 'appCompatibility', 'authTokenv1', 'microsession', 'sessionv1', 'cookies/utm', 'cookies/leads', 'cookies/uuid', 'notificationCount'
];

module.exports = chain.map(function(w) {
    return (typeof w === 'function') ? w : require('@myntra/myx/lib/server/ware/' + w);
}).concat(function(req, res, next) {
  var w = when(req);
  async.parallel([w.all],
    function() {
      const platform = req.isMobile? 'mobile' : 'desktop';
      push(req, `<script>window.__myx_deviceType__ = '${platform}';</script>`);
      next();
    });
}).concat(function(req, res, next) {
  var w = when(req);
  async.parallel([w.all],
    function() {
      const uagent = require("@myntra/myx/lib/userAgents")(req.headers["user-agent"]);
      let osVersion = '0.0.1';
      let isOSCompatible = false;
      try {
        if (req.myx.apps.android) {
            osVersion = uagent.getMyntraAndroidVersionWithReleaseTags();
            isOSCompatible = semver.gt(semver.valid(semver.coerce(osVersion)), '3.31.0');
        } else if (req.myx.apps.ios) {
            osVersion = uagent.getMyntraIosVersionWithReleaseTags();
            isOSCompatible = semver.gt(semver.valid(semver.coerce(osVersion)), '3.24.0');
        }
    } catch (e) {
        console.error('Not able to detect app version', e);
    }
    outline(req).pump('isOSCompatible', isOSCompatible);
    next();

    });
}).concat(function (req, res, next) {
  var uri = new Jsuri(req.url);
  var url = `${uri.path()}${uri.query()}`;
  const regex = /(<([^>]+)>)/ig;
  const decodeUrl = decodeURIComponent(url);
  if (decodeUrl && regex.test(decodeUrl)) {
    res.status(404);
    render(
      req,
      res,
      {},
      'Page not found',
      outline(req)
    );
  } else {
    next();
  }
});
