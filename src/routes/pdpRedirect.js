import at from 'v-at';
import getPdpUrlChangeRegx from '../app/utils/getPdpUrlChangeRegx';

/**
 * This function is used to redirect the /:category/:brand/:name/:style/buy url
 * to /:category-:brand-:name-:style-buy.htm if the category matches in the list.
 *
 * From switch, we are getting Eg: ['<leaning and development>', '<shoes>'], we are converting to regex
 * /leaning-and-development|shoes/gi
 *
 * @param {*} req - Req Object
 * @param {*} res - Res object
 * @param {*} next - Next function
 */
module.exports = function(req, res, next) {
    const category = at(req, 'params.category');
    if (!category) return next();
    const PDP_URL_CHANGE_REGEX = getPdpUrlChangeRegx(at(req, 'myx.kvpairs') || {});
    if (
      !!PDP_URL_CHANGE_REGEX &&
      PDP_URL_CHANGE_REGEX.test(category)
    ) {
       const redirectUrl = `/${req.url
         .replace(/^\/|\/$/g, '')
         .replace(/\//g, '-')}.htm`;
        return res.redirect(302, redirectUrl);
    }
    next();
};
