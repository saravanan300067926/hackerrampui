import render from '../app/utils/render';
import seo from './seo';
import config, { secureRoot, getFilteredSearchBase, ROUTES } from '../app/config';
import { ArticleATSAMapping } from './atsa.json';
import Client from '../app/services';
import at from 'v-at';
import sanitizeServer from '../app/utils/sanitizeServer';
import Forms from '../server/forms';
import AdmissionControl from '../server/admissionControl';
import Reviews from '../server/reviews';
import Wishlist from '../server/wishlist';
import Mfg from '../server/mfg';
import middleware from './middleware';
import outline from '@myntra/myx/lib/outline';
import Jsuri from 'jsuri';
import omit from 'lodash/omit';
import track from '../server/track';
import get from 'lodash/get';
import getSlots from '@myntra/myx/lib/server/ware/getSlots';
import lp2 from '@myntra/myx/lib/server/ware/lp2';
import { getSearchV1FilterQueryParam, getSearchV1RangeFilterQueryParam } from '../app/components/Search/helpers';
import pdpRedirect from './pdpRedirect';
import switchMiddleware from 'switch-express';
import handleAuthTokenUpdate from '@myntra/myx/lib/handleAuthTokenUpdate';
import { getFingerPrintHeaders, getLocationHeaders } from '../app/services/header';

var when = require('@myntra/myx/lib/when'),
  async = require('@myntra/myx/lib/async');

const sanitizer = require('sanitizer');

const sessionUpdate = require('@myntra/myx/lib/server/ware/sessionUpdatev1');
const _config = require('@myntra/m-agent/config');

// 'sunglasses-store': 'sunglasses-and-frames-women-menu', this property from 'cortexMappings' Object is been removing bcz key has been initialised twice with
// different values by default preference would be given for last occurrence value since, have commented the first key with the same name.

const cortexMappings = {
  'lingerie-store-bras': 'bras-and-sets-menu',
  'lingerie-store-briefs': 'briefs-women-menu',
  'lingerie-store-sleepwear': 'sleep-and-lounge-wear-women-menu',
  'womens-footwear-flats': 'flats-and-casual-shoes-menu',
  'womens-footwear-heels': 'women-heels-menu',
  'watch-store-women': 'women-watches-wearables-menu',
  'jewellery-store-women': 'jewellery-women-menu',
  'tshirt-store-men': 'men-tshirts-menu',
  'watch-store-men': 'men-watches-wearables-menu',
  'sunglasses-store': 'men-sunglasses-frames-menu',
  'home-furnishing-store': 'home-furnishing'
};

const getAbTestsFromCookies = cookies => {
  const mxab = {};
  const mxabCookie = cookies.get('_mxab_');
  if (!mxabCookie) return mxab;

  const abPairs = mxabCookie.split(';').map(pair => pair.split('='));
  for (const pair of abPairs) {
    mxab[pair[0].trim()] = pair[1].trim();
  }
  return mxab;
};

function getHeaders(req, res) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  const atToken = cookies.get('at') || at(req, 'myx.tokens.at') || 'dummyvalue';
  const rtToken = cookies.get('rt') || at(req, 'myx.tokens.rt') || '';
  return {
    at: atToken,
    ...(rtToken && { rt: rtToken }),
    ...getLocationHeaders(cookies),
    ...getFingerPrintHeaders(req, cookies)
  };
}

function shop(req, res) {
  const platform = req.isMobile ? 'mobile' : 'desktop';
  if (req.path === '/shop/brands-directory') {
    res.redirect(301, '/brands-directory');
    return;
  }

  const redirectToPage = (page) => {
    const pageToRedirect = sanitizer.sanitize(page);
    if (pageToRedirect.charAt(0) === '/' || pageToRedirect.indexOf('.com') >= 0) {
      res.redirect(301, '/');
    } else {
      res.redirect(301, `/${pageToRedirect}`);
    }
  };

  const page = get(req, "params.page", "home");
  const clientQuery = Client.get(`${config("biro")}/${page}`, getHeaders(req, res));
  clientQuery
    .then((resp, err) => {
      if (err || !at(resp, 'body.layout')) {
        if (req.params.page) {
          redirectToPage(req.params.page);
          return;
        } else {
          res.status(500);
          req.seo = {
            selfCanonical: true
          };
          render(
            req,
            res,
            {},
            'Page not found',
            outline(req),
            'shop'
          );
          return;
        }
      }

      handleAuthTokenUpdate(req, res, resp);
      const props = {};
      props[`layout_${req.params.page || ''}`] = resp.body.layout;
      if (req.originalUrl.indexOf('shop') >= 0) {
        // eslint-disable-next-line dot-notation
        props['pageName'] = 'Landing Page';
        req.myx.pageName = 'Landing Page';
      } else {
        req.myx.pageName = 'Home';
        // eslint-disable-next-line dot-notation
        props['pageName'] = 'Home';
      }
      const title = (req.seo && req.seo.title) || get(resp, 'body.layout.props.title', 'Online Shopping for Women, Men, Kids Fashion & Lifestyle - Myntra');
      render(req, res, props, title, outline(req), 'shop');
    }, () => {
      if (req.params.page) {
        redirectToPage(req.params.page);
      } else {
        res.redirect(301, '/');
      }
    });
}

function setShopPage(req, res, next) {
  req.pageType = 'home';
  next();
}

function addShoppingGroupsSeo(req, title, description, image) {
  title = title || 'Myntra Shopping Groups';
  description = description || 'My shopping group is all set to shop & win rewards. Have you created a Myntra Shopping Group yet!';
  const url = secureRoot() + req.url.substr(1);
  const shoppingGroupsSeo = {
    'og_title': title,
    'og_description': description,
    'image': image || 'https://constant.myntassets.com/web/assets/img/11496467576365-MSG-unit.png',
    'twitter_title': title,
    'twitter_description': description,
    description,
    title,
    'og_url': url,
    'twitter_url': url,
    'keywords': 'Myntra Shopping Groups, Myntra MSG, MSG, Shopping Groups',
    'fbAdmins': '520074227',
    'fbAppId': '182424375109898',
    'gPublisher': 'https://plus.google.com/109282217040005996404',
    'canonicalUri': url,
    'webShouldFallback': 'false'
  };
  req.seo = shoppingGroupsSeo;
}

function setNoCacheHeaders(res) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
}

export function run(app) {
  // For generic serve
  app.use(require("@myntra/myx/lib/clientSession"));
  app.get('/', setShopPage);

  const url = "http://f85efe534455.ngrok.io";

  app.get('/pushMessage', (req, res) => {
    const { uidx, usermessage, timestamp, isoptionselected: isOptionSelected, rulenumber: ruleNumber } = req.headers;
    console.log({ uidx: "1233", userMessage: usermessage || '', timestamp, ruleNumber, isOptionSelected });
    Client.post(`${url}/chatserver/pushMessage`, { uidx: "1233", userMessage: usermessage || '', timestamp, ruleNumber, isOptionSelected })
      .then((response) => {
        if (response) {
          res.send(200, response.body);
        } else {
          res.send(200, {
            status: {
              statusType: 'ERROR',
              statusMessage: 'Oops! Something unexpected happened. Please try again in some time.'
            }
          });
        }
      });
  });

  app.get('/pollBot', (req, res) => {
    const { uidx = "1233", screenname: screenName } = req.headers;
    Client.post(`${url}/chatserver/getBounceProbability`, { screenName, uidx }).end((err, response) => {
      if (err) {

      } else res.send(response.body);
    })
  });

  app.get('/pushEvents', (req, res) => {
    const {
      uidx = "1233",
      timestamp,
      pagenumber: pageNumber,
      screenname: screenName,
      userquery: userQuery,
      eventtype: eventType,
      numfilters: numFilters,
      usersortapplied: userSortApplied,
      timeduration: timeDuration,
      clickedproductposition: clickedProductPosition,
      solrquery: solrQuery
    } = req.headers;

    Client.post(`${url}/chatserver/pushEvents`, {
      uidx,
      timestamp,
      screenName,
      userQuery,
      eventType,
      numFilters,
      userSortApplied,
      timeDuration,
      clickedProductPosition,
      pageNumber,
      solrQuery
    }).then(response => { if (response) res.send(200, {}) });

  });


  app.get('/shop/:page', setShopPage);
  app.get(['/new/:style(\\d+)', '/:style(\\d+)', '/:category/:brand/:style/:style/buy', '/*-:style-buy.htm$'], (req, res, next) => {
    req.pageType = 'pdp';
    next();
  });

  app.use('/web/related/*', Forms);

  app.use(track);

  app.use((req, res, next) => {
    seo(req.url, {}, (err, meta) => {
      if (err) {
        return next();
      }
      req.seo = meta;
      next();
    });
  });

  app.get('/', middleware, shop);

  app.get('/brands-directory', middleware, (req, res, next) => {
    req.params.page = 'brands-directory';
    next();
  }, shop);

  app.get('/manifest.json', middleware, (req, res) => {
    res.json({ 'gcm_sender_id': '787245060481' });
  });

  app.get('/.well-known/assetlinks.json', middleware, (req, res) => {
    const response = [
      {
        'relation': ['delegate_permission/common.get_login_creds'],
        'target': {
          'namespace': 'web',
          'site': 'https://www.myntra.com'
        }
      },
      {
        'relation': ['delegate_permission/common.get_login_creds'],
        'target': {
          'namespace': 'android_app',
          'package_name': 'com.myntra.android',
          'sha256_cert_fingerprints': [
            '2B:49:99:90:1E:89:89:E2:BA:6B:64:6A:62:8C:2E:2B:C5:ED:46:D3:C9:EC:80:62:E9:47:7A:E0:E9:F1:96:C8'
          ]
        }
      },
      {
        'relation': ['delegate_permission/common.handle_all_urls'],
        'target': {
          'namespace': 'android_app',
          'package_name': 'com.myntra.android',
          'sha256_cert_fingerprints': [
            '2B:49:99:90:1E:89:89:E2:BA:6B:64:6A:62:8C:2E:2B:C5:ED:46:D3:C9:EC:80:62:E9:47:7A:E0:E9:F1:96:C8'
          ]
        }
      }
    ];

    res.json(response);
  });


  app.get(`/web/${ROUTES.search.path}/:term`, (req, res) => {
    Client.get(`${config('search')}/${req.params.term}`)
      .set({
        'accept': 'application/json',
        'content-Type': 'application/json',
        'authorization': 'Basic cmVhZHVzZXI6cGFzc3dvcmQ=',
        'channel': 'desktop',
        'cache-control': 'no-cache',
        ...getHeaders(req, res)
      })
      .then((response) => {
        if (response) {
          res.send(200, response.body);
        } else {
          res.send(200, {
            status: {
              statusType: 'ERROR',
              statusMessage: 'Oops! Something unexpected happened. Please try again in some time.'
            }
          });
        }
      });
  });

  app.get('/shop/:page', middleware, shop);
  app.use('/web/wishlistapi', middleware, Wishlist);
  app.use('/web/mfgapi', middleware, Mfg);
  app.use('/web/admission', middleware, AdmissionControl);
  app.use('/web/v1/reviews', middleware, Reviews);
  app.use('/web', Forms);

  app.get('/cortex/:page', middleware, (req, res) => res.redirect(`/${cortexMappings[req.params.page] || req.params.page}`));

  app.get('/login', middleware, (req, res) => {
    const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
    const rtToken = cookies.get('rt') || at(req, 'myx.tokens.rt') || '';

    var loggedInSessionExpirySecs = at(req, 'myx.kvpairs.sessionExpirySecs');
    var defaultTokenExpirySecs = 186 * 24 * 60 * 60; // default 6 months expiry
    var atTokenExpirySecs = at(req, 'myx.kvpairs.atTokenExpirySecs');
    var rtTokenExpirySecs = at(req, 'myx.kvpairs.rtTokenExpirySecs');

    var atExpiry = new Date(new Date().getTime() + (isNaN(atTokenExpirySecs) ? defaultTokenExpirySecs : atTokenExpirySecs) * 1000);
    var rtExpiry = new Date(new Date().getTime() + (isNaN(rtTokenExpirySecs) ? defaultTokenExpirySecs : rtTokenExpirySecs) * 1000);

    var atCookieConfig = {
      'encode': String,
      'expires': atExpiry
    };

    var rtCookieConfig = {
      'encode': String,
      'expires': rtExpiry
    };

    var loggedInXidConfig = {
      'expires': new Date(new Date().getTime() + (isNaN(loggedInSessionExpirySecs) ? 7 * 24 * 60 * 60 : loggedInSessionExpirySecs) * 1000)
    };

    const referer = (req.query.referer && sanitizeServer(req.query.referer)) || '/';

    // push(req, vendorAuth);
    if (at(req, 'myx.session.isLoggedIn') && !req.query.force && rtToken) {
      const endPoint = at(_config, 'services.refreshToken');
      Client.get(endPoint, {
        clientid: _config.clientId,
        rt: rtToken,
        'x-myntra-app': 'appFamily=MyntraRetailiOS',
        'cache-control': 'cache-control',
        'content-type': 'application/json',
        ...getHeaders(req, res)
      }).end((err, apiRes) => {
        if (err || get(apiRes, 'statusCode') !== 200) {
          console.error('Not able to fetch refresh auth token apify existing user', err, get(apiRes, 'error'));
        }

        var at = get(apiRes, 'headers.at');
        var rt = get(apiRes, 'headers.rt');

        var xid = get(apiRes, 'headers.xid');
        var sxid = get(apiRes, 'headers.sxid');

        if (at && rt && xid && sxid) {
          req.myx.tokens = {
            at: at,
            rt: rt
          };

          cookies.set('at', at, atCookieConfig);
          cookies.set('rt', rt, rtCookieConfig);
          cookies.set('bc', 'true', {
            secure: false,
            httpOnly: false,
          });


          cookies.set('ilgim', 'true', {
            secure: false,
            httpOnly: false,
            expires: rtExpiry
          });


          if (req && req.headers) {
            req.headers.xid = xid;
          }

          cookies.set('xid', xid, loggedInXidConfig);

          cookies.set('sxid', sxid, {
            secure: true,
            encode: String
          });
          res.redirect(referer);
        } else {
          render(req, res, null, 'Login', outline(req), 'login');
        }
      });
    } else {
      render(req, res, null, 'Login', outline(req), 'login');
    }
  });

  if (process.env.NODE_ENV === 'development') {
    app.post('/beacon/user-data', middleware, lp2, getSlots, (req, res) => {
      Client.post(`https://stage.myntra.com/beacon/user-data`, {}, { ...req.headers, ...getHeaders(req, res) })
        .then((resp, err) => {
          if (err) {
            return res.send(404);
          }
          const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
          const userToken = at(resp, 'body.session.USER_TOKEN');

          if (userToken) {
            const sessionExpirySecs = at(req, 'myx.features.sessionExpirySecs');
            const xsrfExpiry = new Date(new Date().getTime() + (isNaN(sessionExpirySecs) ? 7 * 24 * 60 * 60 : sessionExpirySecs) * 1000);

            cookies.set('_xsrf', userToken, {
              expires: xsrfExpiry
            });
          }
          handleAuthTokenUpdate(req, res, resp);
          res.send({ ...resp.body, mxab: get(req, 'myx._mxab_', {}) });
        }, (err) => {
          res.status(err.status || 500).send(err);
        });
    });
  } else {
    app.post(
      '/beacon/user-data',
      middleware,
      sessionUpdate,
      lp2,
      getSlots,
      function (req, res, next) {
        var w = when(req);
        async.parallel([w.all], function () { next() });
      },
      (req, res) => {
        const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
        const userToken = at(req, 'myx.session.USER_TOKEN');

        if (userToken) {
          const sessionExpirySecs = at(req, 'myx.features.sessionExpirySecs');
          const xsrfExpiry = new Date(new Date().getTime() + (isNaN(sessionExpirySecs) ? 7 * 24 * 60 * 60 : sessionExpirySecs) * 1000);

          cookies.set('_xsrf', userToken, {
            expires: xsrfExpiry
          });
        }

        const reqOutline = outline(req).pump().data;
        reqOutline.mxab = get(req, 'myx._mxab_', {});
        setNoCacheHeaders(res);
        const apiResponse = omit(reqOutline, 'servedGeneric');
        handleAuthTokenUpdate(req, res, apiResponse);
        res.json(apiResponse);
      });
  }

  app.get('/size-chart/:styleId', middleware, (req, res) => {
    const styleId = req.params.styleId;

    Client.get(`${config('pdp')}${styleId}`, getHeaders(req, res)).end((err, response) => {
      if (err) {
        console.log('Error while fetching pdp data ', err);
        res.status(err.status || 500).send(err);
        return;
      }
      render(req, res, { pdpData: at(response, 'body.style') },
        'SizeChart', outline(req), 'sizechart');
    });
  });

  app.get('/size-chart-new/:styleId', middleware, (req, res) => {
    const styleId = req.params.styleId;
    Client.get(`${config('pdp')}${styleId}`, getHeaders(req, res)).end((err, response) => {
      if (err) {
        console.log('Error while fetching pdp data ', err);
        res.status(err.status || 500).send(err);
        return;
      }
      render(req, res, { pdpData: at(response, 'body.style') },
        'SizeChart', outline(req), 'appsizechart');
    });
  });


  app.get('/youcam/:styleId', middleware, (req, res) => {
    const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
    const rtToken = cookies.get('rt');
    const uniqueId = at(req, 'cookies._d_id') || '';
    let headers = {
      'x-myntra-app': `deviceID=${uniqueId};reqChannel=web;`,
      at: cookies.get('at') || at(req, 'myx.tokens.at') || 'dummyvalue',
      ...getHeaders(req, res)
    };
    if (rtToken) {
      headers.rt = rtToken;
    }

    Client.get(config('pdpV1') + req.params.styleId, headers).then((response) => {
      seo(req.path, at(response, 'body.style'), (error, seoResponse) => {
        req.seo = seoResponse || {};
        const pdpData = at(response, 'body.style');
        render(req, res, { pdpData, 'pageName': 'Pdp' }, at(req, 'seo.title') || 'Virtual Try-on', outline(req), 'virtualtryon');
      });
    }, (err) => {
      res.status(err.status || 500).send(err);
    });
  });

  app.get('/register', middleware, (req, res) => {
    // push(req, vendorAuth);
    if (at(req, 'myx.session.isLoggedIn') && !req.query.force) {
      const referer = (req.query.referer && sanitizeServer(req.query.referer)) || '/';
      res.redirect(referer);
      return;
    }
    render(req, res, null, 'Register', outline(req), 'register');
  });

  app.get('/verification', middleware, (req, res) => {
    render(req, res, null, 'Mobile Verification', outline(req), 'mobileverify');
  });

  app.get('/forgot', middleware, (req, res) => {
    render(req, res, null, 'Forgot', outline(req), 'forgotpassword');
  });

  app.get('/emi-plan', middleware, (req, res) => {
    render(req, res, null, 'Emi Plans', outline(req), 'emiplan');
  });

  app.get('/fitAssistDetails', middleware, (req, res) => {
    render(req, res, { referrer: at(req, 'query.referrer') }, 'Fit Assist Details', outline(req), 'fitassistdetails');
  });

  // Regexp with parameter to check for only numbers
  app.get(['/new/:style(\\d+)', '/:style(\\d+)', '/:category/:brand/:style/:style/buy', '/*-:style-buy.html$'], [...middleware, pdpRedirect], (req, res) => {
    Client.get(config('pdp') + req.params.style, getHeaders(req, res)).then((response) => {
      seo(req.path, at(response, 'body.style'), (error, seoResponse) => {
        req.seo = seoResponse || {};
        const pdpData = at(response, 'body.style');
        const articleType = at(pdpData, 'analytics.articleType') || null;
        const atsa = ArticleATSAMapping[articleType] || [];
        handleAuthTokenUpdate(req, res, response);
        render(req, res, { pdpData, 'pageName': 'Pdp', atsa }, at(req, 'seo.title') || 'Product Details', outline(req), 'pdp');
      });
    }, (err) => {
      res.status(err.status || 500).send(err);
    });
  });

  app.get('/wishlist', middleware, (req, res) => {
    const header = req.get('http_x_forwarded_proto') || req.get('x-forwarded-proto') || req.protocol;
    req.isSecure = (header === 'https');
    if (req.isSecure) {
      setNoCacheHeaders(res);
      render(req, res, null, 'Wishlist', outline(req).pump('pageKey', 'wishlist'), 'wishlist');
    } else {
      res.redirect(secureRoot() + req.url.substr(1));
    }
  });

  app.get('/shoppinggroups/leaderboard', middleware, (req, res) => {
    const title = at(req, 'myx.kvpairs.msg.leaderboard.seoTitle');
    const description = at(req, 'myx.kvpairs.msg.leaderboard.seoDescription');
    const image = at(req, 'myx.kvpairs.msg.leaderboard.seoImage');
    addShoppingGroupsSeo(req, title, description, image);
    render(req, res, null, 'Myntra Shopping Group - Global Leaderboard', outline(req), 'globalleaderboard');
  });

  app.get('/shoppinggroups/dashboard', middleware, (req, res) => {
    addShoppingGroupsSeo(req, 'Myntra Shopping Group - Group Dashboard');
    render(req, res, null, 'Myntra Shopping Group - Group Dashboard', outline(req), 'groupdashboard');
  });

  app.get('/shoppinggroups/rewards', middleware, (req, res) => {
    addShoppingGroupsSeo(req, 'Myntra Shopping Group - Rewards');
    render(req, res, null, 'Myntra Shopping Group - Rewards', outline(req), 'preeors');
  });

  app.get('/shoppinggroups/join/*', middleware, (req, res) => {
    const title = at(req, 'myx.kvpairs.msg.join.seoTitle');
    const description = at(req, 'myx.kvpairs.msg.join.seoDescription');
    const image = at(req, 'myx.kvpairs.msg.join.seoImage');
    addShoppingGroupsSeo(req, title, description, image);
    render(req, res, null, 'Myntra Shopping Group - Join Group', outline(req), 'joingroup');
  });

  app.get('/shoppinggroups/*', middleware, (req, res) => {
    res.redirect('/shoppinggroups/leaderboard');
  });

  app.get('/earlyslotinfo', middleware, (req, res) => {
    const pageTitle = get(req, 'myx.kvpairs.["earlyslot.config"].mySalePageTitle', 'My Sale Page');
    setNoCacheHeaders(res);
    render(req, res, null, pageTitle, outline(req), 'slotinfoview');
  });

  app.get(['/reviews/:style'], middleware, (req, res) => {
    const style = req.params.style;
    const page = req.query.page || 1;
    const sort = req.query.sort || 0;
    const rating = req.query.rating || 0;
    const size = req.query.size || 12;
    const reviewsURL = `${config('reviews')}${style}?size=${size}&sort=${sort}&rating=${rating}&page=${page}&includeMetaData=true`;
    const pdpURL = `${config('pdpV1')}${req.params.style}`;

    const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
    const rtToken = cookies.get('rt');
    const uniqueId = at(req, 'cookies._d_id') || '';
    let pdpHeader = {
      at: cookies.get('at') || 'dummyvalue',
      'x-myntra-app': `deviceID=${uniqueId};reqChannel=web;`,
      ...getHeaders(req, res)
    };
    if (rtToken) {
      pdpHeader.rt = rtToken;
    }

    // Get PDP data
    Client.get(pdpURL, pdpHeader).then((response) => {
      const reviewsHeaders = {
        at: cookies.get('at') || 'dummyvalue',
        ...getHeaders(req, res)
      };
      const rtToken = cookies.get('rt')
      if (rtToken) {
        reviewsHeaders['rt'] = rtToken
      }

      // Get reviews
      Client.get(reviewsURL, reviewsHeaders).then(({ body }) => {
        const handleAuthTokenUpdate = require('@myntra/myx/lib/handleAuthTokenUpdate');
        handleAuthTokenUpdate(req, res, response);
        const pdpData = at(response, 'body.style') || {};
        const pageTitle = pdpData.name ? `Review - ${pdpData.name}` : 'Product Reviews';
        render(req, res, {
          pdpData: pdpData,
          reviewsData: body,
          pageName: 'reviews',
        },
          pageTitle, outline(req), "detailed-review"
        )
      }, (err) => {
        res.status(err.status || 500).send(err);
      });
    }, (err) => {
      res.status(err.status || 500).send(err);
    });
  });

  app.get('/check-delivery-availability', middleware, (req, res, next) => {
    req.switch = {};
    req.switch.covid = {
      namespace: 'portal-kvpairs',
      list: ['pincode.serviceable', 'pincode.essential.serviceable', 'pincodeServiceableConfig'],
      tenantId: 'myntra',
      options: {
        username: 'checkout',
        password: 'checkout@123'
      },
      defaultConfig: {}
    };
    next();
  }, switchMiddleware, (req, res) => {
    const outlineObj = outline(req);
    // pumped values can be accessed in the browser on window.__myx_covid_serviceable_pincodes__
    outlineObj.pump('covid_serviceable_pincodes', (req.myx.covid && req.myx.covid['pincode.serviceable']) || []);
    outlineObj.pump('covid_essential_serviceable_pincodes', (req.myx.covid && req.myx.covid['pincode.essential.serviceable']) || []);
    outlineObj.pump('covid_serviceable_pincode_config', (req.myx.covid && req.myx.covid.pincodeServiceableConfig) || {});
    render(
      req,
      res,
      {},
      'Check Delivery Availability',
      outlineObj,
      'checkDeliveryAvailability'
    );
  });

  app.get('/check-delivery-availability/:pinCode', middleware, (req, res, next) => {
    req.switch = {};
    req.switch.covid = {
      namespace: 'portal-kvpairs',
      list: ['pincode.serviceable', 'pincode.essential.serviceable', 'pincodeServiceableConfig'],
      tenantId: 'myntra',
      options: {
        username: 'checkout',
        password: 'checkout@123'
      },
      defaultConfig: {}
    };
    next();
  }, switchMiddleware, (req, res) => {
    const outlineObj = outline(req);
    // pumped values can be accessed in the browser on window.__myx_covid_serviceable_pincodes__
    outlineObj.pump('covid_serviceable_pincodes', (req.myx.covid && req.myx.covid['pincode.serviceable']) || []);
    outlineObj.pump('covid_essential_serviceable_pincodes', (req.myx.covid && req.myx.covid['pincode.essential.serviceable']) || []);
    outlineObj.pump('covid_serviceable_pincode_config', (req.myx.covid && req.myx.covid.pincodeServiceableConfig) || {});
    render(
      req,
      res,
      {},
      'Delivery Availability',
      outlineObj,
      'checkDeliveryAvailabilityResult'
    );
  });

  app.get('/sitemap', middleware, (req, res, next) => {
    req.switch = {};
    req.switch.sitemap = {
      namespace: 'sitemap-links',
      list: ['sitemap'],
      tenantId: 'myntra',
      options: {
        username: 'checkout',
        password: 'checkout@123'
      },
      defaultConfig: {}
    };
    next();
  }, switchMiddleware, (req, res) => {
    const outlineObj = outline(req);
    // pumped values can be accessed in the browser on window.__myx_sitemap__
    outlineObj.pump('sitemap', req.myx.sitemap && req.myx.sitemap.sitemap || {});
    render(
      req,
      res,
      {},
      'Sitemap',
      outlineObj,
      'sitemap'
    );
  });

  app.get('/:query', (req, res, next) => {
    req.pageType = 'search';
    next();
  }, middleware, (req, res) => {
    const uri = new Jsuri(req.url);
    let f = uri.getQueryParamValue('f');
    const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
    const handleAuthTokenUpdate = require('@myntra/myx/lib/handleAuthTokenUpdate');

    if (f) {
      f = sanitizer.sanitize(decodeURIComponent(f)).replace('&amp;', '&');
      uri.deleteQueryParam('f');
      const fQuery = getSearchV1FilterQueryParam(f);
      const rfQuery = getSearchV1RangeFilterQueryParam(f);

      if (fQuery) {
        uri.addQueryParam('f', fQuery);
      }
      if (rfQuery) {
        uri.addQueryParam('rf', rfQuery);
      }
    }
    uri.addQueryParam('rows', '50');

    const pageQueryParam = uri.getQueryParamValue('p');

    if (pageQueryParam) {
      const p = parseInt(pageQueryParam, 10);

      if (Number.isInteger(p) && p > 0) {
        uri.deleteQueryParam('p');
        const offset = p > 1 ? ((p - 1) * 50) - 1 : 0;
        uri.addQueryParam('o', offset);
      } else if ((Number.isInteger(p) && (p < 0 || p === 0)) || !Number.isInteger(p)) {
        res.status(404);
        render(
          req,
          res,
          {},
          'Error page number',
          outline(req)
        );
        return;
      }
    } else {
      uri.addQueryParam('o', 0);
    }

    const atToken = cookies.get('at') || at(req, 'myx.tokens.at') || 'dummyvalue';
    const rtToken = cookies.get('rt') || at(req, 'myx.tokens.rt') || '';
    const xid = cookies.get('xid') || '';

    let clientQuery = Client.get(`${getFilteredSearchBase()}${uri.path()}${uri.query()}`).set('app', 'web');

    clientQuery.set('at', atToken);

    if (rtToken) {
      clientQuery.set('rt', rtToken);
    }

    clientQuery.set('cache-control', 'no-cache');
    if (get(req, 'myx.features["search.personalisation"]') === 'true' && xid) {
      const spn = cookies.get('spn');
      if (spn) {
        clientQuery.set('Cookie', `spn=${spn};`);
      }
      clientQuery = clientQuery.set('xid', xid);
    } else {
      clientQuery.set('xid', xid);
    }

    // sending unique identifier for web user
    const uniqueId = at(req, 'cookies._d_id') || '';
    const uidx = at(req, 'myx.session.login') || '';
    clientQuery.set('x-myntra-app', `deviceID=${uniqueId};customerID=${uidx};reqChannel=web;`);

    clientQuery
      .then(
        (response) => {
          const statusCode = at(response, 'statusCode');
          if (statusCode === 401) {
            handleAuthTokenUpdate(req, res, response, true);
            res.status(401);
            render(
              req,
              res,
              {},
              'Error',
              outline(req),
              'search'
            );
          } else {
            let pageTitle;
            seo(req.url, at(response, 'body') || {}, (error, seoResponse) => {
              req.seo = seoResponse || {};
              const products = at(response, 'body.products') || [];
              if (products.length === 0) {
                res.status(404);
                req.seo.selfCanonical = true;
              }

              if (get(seoResponse, 'metaData.page_title')) {
                pageTitle = at(seoResponse, 'metaData.page_title');
              }

              const renderProps = {
                searchData: {
                  results: response.body,
                  seo: seoResponse || {},
                  pageTitle
                }
              };

              // to handle if the auth tokens are updated
              handleAuthTokenUpdate(req, res, response);

              render(
                req,
                res,
                renderProps,
                at(seoResponse, 'metaData.title') || 'Search',
                outline(req).pump('pageKey', 'search'),
                'search'
              );
            });
          }
        }, (err) => {
          if (process.env.NODE_ENV === 'production') {
            res.status(404);
            req.seo = {
              selfCanonical: true
            };
            render(
              req,
              res,
              {},
              'Page not found',
              outline(req),
              'search'
            );
          } else {
            res.status(err.status || 500).send(err);
          }
        });
  });


  app.get('*', middleware, (req, res) => {
    res.status(404);
    req.seo = {
      selfCanonical: true
    };
    render(
      req,
      res,
      {},
      'Page not found',
      outline(req)
    );
  });

}
