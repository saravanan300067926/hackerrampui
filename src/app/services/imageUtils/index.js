import assignIn from 'lodash/assignIn';
import { quality } from './constants';
import uploaders from './uploaders';
import { decodeResolutionsCSV } from './utils';

const defaults = {
  resolutionsCSV: '360X480',
  fetchMinified: true,
  quality: 'LOW'
};

/* USAGE:
 * To get different resolutions of the same image, pass in comma-separated resolutions in imageParams as resolutionsCSV,
 * e.g. '180X240, 150X200'
 * To get same resolutions for different images, set default imageParams and then pass corresponding imageEntry to getURLMap
 */
class ImageUtils {

  constructor(imageParams) {
    this.imageParams = imageParams;
  }

  populateURLMap(imageEntry, imageParams = {}) {
    imageParams = assignIn({}, this.imageParams, imageParams);
    imageParams.resolutions = decodeResolutionsCSV(imageParams.resolutionsCSV);
    imageParams.qualityPercentage = quality[imageParams.quality] || quality[defaults.quality];

    try {
      imageEntry = JSON.parse(imageEntry);
    } catch (ex) {
      // console.warn('Malformed imageEntry JSON', ex);
    }
    return uploaders(imageEntry, imageParams);
  }

}


// const ImageUtils = (imageParams) => {
//   if (!(this instanceof ImageUtils)) return new ImageUtils(imageParams);
//   this.setImageParams(imageParams || {});
// };
//
// assignIn(imageUtils.prototype, {
//
//   setImageParams(imageParams) {
//     this.imageParams = assignIn({}, defaults, this.imageParams, imageParams);
//   },
//
//   populateURLMap(imageEntry, imageParams) {
//     imageParams = assignIn({}, this.imageParams, imageParams || {});
//     imageParams.resolutions = decodeResolutionsCSV(imageParams.resolutionsCSV);
//     imageParams.qualityPercentage = quality[imageParams.quality] || quality[defaults.quality];
//
//     if (typeof imageEntry === 'string') {
//       try {
//         imageEntry = JSON.parse(imageEntry);
//       } catch (e) {
//         console.error('Malformed imageEntry JSON');
//       }
//     }
//     return uploaders(imageEntry, imageParams);
//   }
//
// });

export default ImageUtils;
