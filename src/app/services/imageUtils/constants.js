const tokens = {
  DOMAIN: '($domain)',
  HEIGHT: '($height)',
  WIDTH: '($width)',
  QUALITY_PERCENTAGE: '($qualityPercentage)'
};

const quality = {
  FULL: 100,
  HIGH: 90,
  Q90: 90,
  MID: 80,
  LOW: 60
};

const miniExtension = '_mini';

export {
  tokens,
  quality,
  miniExtension
};
