import s3 from './s3';
import cloudinary from './cloudinary';

const map = {
  S3: s3,
  CL: cloudinary
};

export default (imageEntry, conf) => {
  imageEntry = imageEntry || {};
  imageEntry.resolutionToURLMap = map[imageEntry.servingUploaderType || 'S3'](imageEntry, conf);
  return imageEntry;
};
