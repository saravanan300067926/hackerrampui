import { tokens } from '../constants';
import { addMiniExtension, changeExtension } from '../utils';

const cloudinary = (imageEntry, imageParams) => {
  const map = {};
  for (let i = 0; i < imageParams.resolutions.length; i++) {
    const res = imageParams.resolutions[i];
    if (imageEntry) {
      if (imageEntry.resolutionFormula) {
        map[res.key] = imageEntry.resolutionFormula
          .replace(tokens.WIDTH, res.width)
          .replace(tokens.HEIGHT, res.height)
          .replace(tokens.QUALITY_PERCENTAGE, imageParams.qualityPercentage);
      } else {
        map[res.key] = imageParams.qualityPercentage;
      }
    } else {
      map[res.key] = '';
    }
    if (imageParams.supports && imageParams.supports.webp) {
      map[res.key] = changeExtension(map[res.key], 'webp');
    }
    if (imageParams.fetchMinified) {
      map[res.miniKey] = addMiniExtension(map[res.key]);
    }
  } return map;
};

export default cloudinary;
