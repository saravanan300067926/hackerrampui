import { tokens } from '../constants';
import { addMiniExtension, decodeResolutionsCSV } from '../utils';
import minBy from 'lodash/minBy';

const s3 = (imageEntry, imageParams) => {
  const map = {};
  const supported = imageEntry.supportedResolutions && decodeResolutionsCSV(imageEntry.supportedResolutions);
  if (imageParams.resolutions) {
    imageParams.resolutions.forEach((res) => {
      let closest;
      const supportedRes = supported.filter((val) => val.key === res.key);
      if (supported && !supportedRes.length) {
        closest = minBy(supported, (res2) =>
          Math.pow(res.width - res2.width, 2) + Math.pow(res.height - res2.height, 2)
        );
        res.width = closest.width;
        res.height = closest.height;
      }
      if (imageEntry) {
        if (imageEntry.resolutionFormula) {
          map[res.key] = imageEntry.resolutionFormula
            .replace(tokens.WIDTH, res.width)
            .replace(tokens.HEIGHT, res.height);
        } else {
          map[res.key] = imageEntry.relativePath;
        }
      } else {
        map[res.key] = '';
      }
      if (imageParams.fetchMinified) {
        map[res.miniKey] = addMiniExtension(map[res.key]);
      }
    });
  } return map;
};

export default s3;
