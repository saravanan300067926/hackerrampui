import lastIndexOf from 'lodash/lastIndexOf';
import trim from 'lodash/trim';
import { miniExtension } from './constants';

const addMiniExtension = (filename) => {
  if (!filename) return filename;
  const index = lastIndexOf(filename, '.');
  if (index === -1) {
    return filename + miniExtension;
  } return filename.substr(0, index) + miniExtension + filename.substr(index);
};

const changeExtension = (filename, ext) => {
  if (filename) {
    return filename.replace(/(jpg|jpeg)$/, ext);
  } return null;
};

const decodeResolutionsCSV = (resolutionsCSV) => {
  resolutionsCSV = resolutionsCSV.replace(/x/g, 'X');
  resolutionsCSV = resolutionsCSV.split(',');
  return resolutionsCSV.map((res) => {
    const key = trim(res);
    const wh = key.split('X');
    return {
      key,
      miniKey: `${key} Xmini`,
      width: wh[0],
      height: wh[1]
    };
  });
};

export {
  decodeResolutionsCSV,
  changeExtension,
  addMiniExtension
};
