import get from "lodash/get";

function getMatch(regex, str) {
  const result = regex.exec(str);
  if (Array.isArray(result) && result.length > 1) {
    return result[1];
  }
  else {
    return null;
  }
}

function getPincodeAndSource(cookies) {
  const pincodeCookie = cookies.get("mynt-ulc-api");
  const sourceCookie = cookies.get("mynt-loc-src");

  if (pincodeCookie && sourceCookie) {
    const pincode = getMatch(/pincode:(\d{6})/i, pincodeCookie);
    const source = getMatch(/source:(\w{2,4})/i, sourceCookie);

    if (pincode && source) {
      return {
        pincode: pincode,
        source: source
      }
    }
  }

  return {};
}

export function getLocationHeaders(cookies) {
  const { source, pincode } = getPincodeAndSource(cookies);

  if (source && pincode) {
    return {
      "x-location-context": `pincode=${pincode};source=${source}`
    };
  } else {
    return {};
  }
}

export function getAuthHeaders(req, cookies) {
  const at = cookies.get("at") || get(req, "myx.tokens.at") || "NOT_FOUND";
  const rt = cookies.get("rt") || "";
  return { at, rt };
}

function stringify(object) {
  return Object
    .entries(object)
    .map(([key, value]) => `${key}=${value};`)
    .join("");
}

export function getFingerPrintHeaders(req, cookies) {
  const userAgent = req.get("user-agent");
  const deviceId = cookies.get("_d_id") || "";
  const xForwardedFor = req.header("X-Forwarded-For") || "";

  const isDesktop = get(req, "myx.deviceData.isDesktop", false);
  const reqChannel = isDesktop ? "web" : "mweb";
  const xMyntraApp = stringify({
    deviceId,
    appFamily: userAgent,
    reqChannel
  });

  return {
    deviceId: deviceId,
    "User-Agent": userAgent,
    "X-Forwarded-For": xForwardedFor,
    "x-meta-app": xMyntraApp
  };
}
