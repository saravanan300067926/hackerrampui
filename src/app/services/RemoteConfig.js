import { saveInSession, getFromSession } from '../utils/sessionStorageUtil';
import get from 'lodash/get';
import cookies from '../utils/cookies';
import { isLoggedIn } from '../utils';
import bus from 'bus';
import Client from '../services';
import config from '../config';
import { getItem } from '../utils/localStorageUtil';
import { CACHE_KEY } from '../utils/beacon.js';

export function getMyx() {
  let result;
  const beacon = getItem(`lscache-${CACHE_KEY}`);

  if (beacon) {
    result = Promise.resolve(beacon);
  } else {
    result = new Promise((resolve, reject) => {
      bus.on('beacon-data', (myx) => {
        if (myx) {
          resolve(myx);
        } else {
          reject('NULL MYX');
        }
      });
    });
  }

  return result;
}

export function isNewUserPromise() {
  let result;
  if (!isLoggedIn()) {
    result = Promise.resolve(true);
  } else {
    const val = cookies.get('newUser');
    if (val !== null && val !== undefined) {
      const ftc = val === 'true';
      result = Promise.resolve(ftc);
    } else {
      result = new Promise((resolve, reject) => {
        Client.get(`${config('isNewUser')}`).end((err, res) => {
          const isNew = get(res, 'body.isNew');
          if (err || isNew === undefined) {
            reject(err);
          } else {
            cookies.set('newUser', isNew, 1 / 24 * 2 / 60);
            resolve(isNew);
          }
        });
      });
    }
  }

  return result;
}

// This is to check if morpheus is working properly.
// Also being done with a custom dimension.
function ga(abTest) {
  const value = getFromSession('morpheusGA');
  if (!value && window.ga) {
    saveInSession('morpheusGA', 'EVENT_SENT');
    window.ga(
      'send',
      'event',
      'new_user_onboarding',
      'ab_test_value',
      abTest
    );
  }
}

export function isAbEnabled(abConfig = {}, defaultVal = false) {
  let result;
  const beacon = JSON.parse(getItem(`lscache-${CACHE_KEY}`));
  const { key, control, test } = abConfig;
  if ((beacon && key && control && test)) {
    const abTest = get(beacon, ['mxab', key]);
    if (abTest === control) {
      ga('control');
      result = false;
    } else if (abTest === test) {
      ga('test');
      result = true;
    } else {
      ga('none');
      result = defaultVal;
    }
  } else {
    result = false;
  }

  return result;
}
