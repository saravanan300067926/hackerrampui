// Singleton service client
import smartagent from '@myntra/m-agent';
import { getAuthHeaders, getFingerPrintHeaders, getLocationHeaders } from './header';
const agent = smartagent();
import get from "lodash/get";

function getTimeout(options = {}) {
  return options.timeout || 15000;
}

/**
 * @callback ClientCallback
 * @param  {object} err - SmartAgent Error
 * @param  {object} res - SmartAgent Response
 */

/**
 * Wrapper around m-agent which presents you with a fluent interface
 * and passes common request headers so you don't have to.
 */
class Client {
  constructor() {
    this.clientHeaders = {};

    this._internalGetHeaders = this._internalGetHeaders.bind(this);
    this.withRequestHeaders = this.withRequestHeaders.bind(this);
    this.errorHandler = this.errorHandler.bind(this);
    this.serverLogger = this.serverLogger.bind(this);

    this.get = this.get.bind(this);
    this.put = this.put.bind(this);
    this.post = this.post.bind(this);
  }

  /**
   * @private
   * Private function.
   * Do not call outside of Client class.
   */
  _internalGetHeaders(headers) {
    const apiHeaders = {
      ...headers,
      ...this.clientHeaders,
      Accept: "application/json",
      "Content-Type": "application/json"
    };
    this.clientHeaders = {};
    return apiHeaders;
  }

  /**
   * Extract at, rt, userAgent, deviceId, xff and x-meta-app from the request object.
   * Approach differs from PWA, because this is also used in clientSide api calls,
   * meaning we can't require the module and dynamic import will give a promise.
   * Have to fix myx/cookies implementation.
   *
   * @param {express.request} req - Used to extract headers (cookie and others)
   * @param {myx.cookies} cookies - The cookies abstraction with req and res passed. Use src/utils/cookies.js
   * @return {Client}
   */
  withRequestHeaders(req, cookies) {
    if (typeof window === 'undefined') {
      this.clientHeaders = {
        ...getAuthHeaders(req, cookies),
        ...getFingerPrintHeaders(req, cookies),
        ...getLocationHeaders(cookies)
      };
    }
    return this;
  }

  serverLogger(url, params, headers) {
    if (typeof window === 'undefined') {}
  }

  /**
   * You should call {@link withRequestHeaders} before this
   * unless passing common headers is not required.
   *
   * @param {string} url
   * @param {Object} headers - Pass additional headers
   * @param {{timeout: number}} options
   * @return {{end(fn: ClientCallback): void}}
   */
  get(url, headers, options = {}) {
    this.serverLogger(url, {}, headers);
    return agent
      .get(url)
      .set(this._internalGetHeaders(headers))
      .timeout(getTimeout(options))
      .send();
  }

  /**
   * You should call {@link withRequestHeaders} before this
   * unless passing common headers is not required.
   *
   * @param {string} url
   * @param {Object} headers - Pass additional headers
   * @param {Object} params
   * @param {{timeout: number}} options
   * @return {{end(fn: ClientCallback): void}}
   */
  post(url, params = {}, headers = {}, options = {}) {
    this.serverLogger(url, params, headers);
    return agent
      .post(url)
      .withCredentials()
      .set(this._internalGetHeaders(headers))
      .timeout(getTimeout(options))
      .send(params);
  }

  /**
   * You should call {@link withRequestHeaders} before this
   * unless passing common headers is not required.
   *
   * @param {string} url
   * @param {Object} headers - Pass additional headers
   * @param {Object} params
   * @param {{timeout: number}} options
   * @return {{end(fn: ClientCallback): void}}
   */
  put(url, params = {}, headers = {}, options = {}) {
    this.serverLogger(url, params, headers);
    return agent
      .put(url)
      .set(this._internalGetHeaders(headers))
      .timeout(getTimeout(options))
      .send(params);
  }

  /**
   * Later on can decide if we want to handle common errors here directly.
   * Or if we move to then/catch, we can just use this function and rethrow.
   *
   * @param err
   * @param res
   * @param nodeRes
   * @return {boolean}
   */
  errorHandler(err, res, nodeRes) {
    return agent.handleErrors(err, res, nodeRes);
  }
}

export default (new Client());
