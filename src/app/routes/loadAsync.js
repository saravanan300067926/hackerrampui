import React from 'react';
import loadable from 'react-loadable';
import RouteLoading from './RouteLoading';

const pages = {
  shop: () =>
    import(/* webpackChunkName: "d-shop" */ './../components/Shop'),
  pdp: () => import(/* webpackChunkName: "d-pdp" */ './../components/Pdp'),
  search: () =>
    import(/* webpackChunkName: "d-search" */ './../components/Search'),
  mobileVerify: () =>
    import(/* webpackChunkName: "d-mobileverify" */ './../components/MobileVerify'),
  login: () =>
    import(/* webpackChunkName: "d-login" */ './../components/Login'),
  register: () =>
    import(/* webpackChunkName: "d-register" */ './../components/Register'),
  forgotPassword: () =>
    import(/* webpackChunkName: "d-forgotpassword" */ './../components/ForgotPassword'),
  sizeChart: () =>
    import(/* webpackChunkName: "d-sizechart" */ './../components/SizeChart'),
  appSizeChart: () =>
    import(/* webpackChunkName: "d-appsizechart" */ './../components/SizeChart/AppSizeChart'),
  virtualTryon: () =>
    import(/* webpackChunkName: "d-virtualtryon" */ '../components/VirtualTryon'),
  emiPlan: () =>
    import(/* webpackChunkName: "d-emiplan" */ './../components/EmiPlan'),
  fitAssistDetails: () =>
    import(/* webpackChunkName: "d-fitassistdetails" */ './../components/FitAssistDetails'),
  slotInfoView: () =>
    import(/* webpackChunkName: "d-slotinfoview" */ './../components/SlotInfoView'),
  preEors: () =>
    import(/* webpackChunkName: "d-preeors" */ './../components/Mfg/Rewards/PreEORS'),
  globalLeaderBoard: () =>
    import(/* webpackChunkName: "d-globalleaderboard" */ './../components/Mfg/GlobalLeaderBoard'),
  groupDashboard: () =>
    import(/* webpackChunkName: "d-groupdashboard" */ './../components/Mfg/GroupDashboard'),
  joinGroup: () =>
    import(/* webpackChunkName: "d-joingroup" */ './../components/Mfg/JoinGroup'),
  wishlist: () =>
    import(/* webpackChunkName: "d-wishlist" */ './../components/Wishlist'),
  detailedReviews: () =>
    import(/* webpackChunkName: "d-detailed-review" */ './../components/DetailedReviews'),
  pageNotFound: () =>
    import(/* webpackChunkName: "d-pagenotfound" */ './../components/PageNotFound'),
  expandedFreeShipping: () =>
    import(/* webpackChunkName: "d-expandedFreeShipping" */ '../components/Search/Results/Banners/ExpandedBanner'),
  checkDeliveryAvailability: () =>
    import(/* webpackChunkName: "d-checkDeliveryAvailability" */ '../components/CheckDeliveryAvailability'),
  checkDeliveryAvailabilityResult: () =>
    import(/* webpackChunkName: "d-checkDeliveryAvailabilityResult" */ '../components/CheckDeliveryAvailability/CheckDeliveryAvailabilityResult'),
  sitemap: () => import(/* webpackChunkName: "d-sitemap" */ '../components/Sitemap/Sitemap.jsx')
};

export default function loadAsync(pageKey, LoaderComponent) {
  return loadable({
    loader: () => pages[pageKey](),
    loading: LoaderComponent || (props => <RouteLoading {...props} pageKey={pageKey} />),
    timeout: 10000
  });
}
