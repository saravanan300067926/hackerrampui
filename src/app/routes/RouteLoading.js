import React from "react";
import Loader from '../components/Loader';
import styles from "./routeLoading.css";

const MessgageContainer = props => {
  return (
    <div className={styles.container}>
      <h4 className={styles.textMsg}>{props.text}</h4>
      <button onClick={props.retry} className={styles.retryButton}>
        Refresh
      </button>
    </div>
  );
};

MessgageContainer.propTypes = {
  text: React.PropTypes.string.isRequired,
  retry: React.PropTypes.func.isRequired
};

const RouteLoading = props => {
  if (props.error) {
    // When the loader has errored
    return (
      <MessgageContainer
        text="Oops! Something went wrong."
        retry={props.retry} />
    );
  } else if (props.timedOut) {
    return (
      <MessgageContainer text="Taking a long time..." retry={props.retry} />
    );
  } else if (props.pastDelay) {
    // When the loader has taken longer than the delay
    return <div className={styles.container}><Loader show={true} /></div>;
  } else {
    // When the loader has just started
    return null;
  }
};

export default RouteLoading;

RouteLoading.propTypes = {
  retry: React.PropTypes.func.isRequired,
  timedOut: React.PropTypes.bool,
  pastDelay: React.PropTypes.bool,
  error: React.PropTypes.bool
};
