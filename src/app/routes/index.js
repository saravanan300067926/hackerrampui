import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { initStore } from '../components/Search/store';
import { removeItem } from '../utils/localStorageUtil';
import { Provider } from 'react-redux';
import Application from './../components/Application';
import loadAsync from './loadAsync';
import PdpSearchSwitch from '../components/PdpSearchSwitch';

const Shop = loadAsync('shop');
const Pdp = loadAsync('pdp');


const routes = (
  <Route path="/" component={Application} >
    <IndexRoute component={Shop} />
    <Route path="/verification" component={loadAsync('mobileVerify')} />
    <Route path="/shop/:page" component={Shop} />
    <Route path="/login" component={loadAsync('login')} />
    <Route path="/register" component={loadAsync('register')} />
    <Route path="/forgot" component={loadAsync('forgotPassword')} />
    <Route path="/size-chart/:styleId" component={loadAsync('sizeChart')} />
    <Route path="/size-chart-new/:styleId" component={loadAsync('appSizeChart')} />
    <Route exact path="/youcam/:styleId" component={loadAsync('virtualTryon')} />
    <Route path="/emi-plan" component={loadAsync('emiPlan')} />
    <Route path="/fitAssistDetails" component={loadAsync('fitAssistDetails')} />
    <Route path="/earlyslotinfo" component={loadAsync('slotInfoView')} />
    <Route path="/shoppinggroups/rewards" component={loadAsync('preEors')} />
    <Route path="/shoppinggroups/leaderboard" component={loadAsync('globalLeaderBoard')} />
    <Route path="/shoppinggroups/dashboard" component={loadAsync('groupDashboard')} />
    <Route path="/shoppinggroups/join/:invitecode" component={loadAsync('joinGroup')} />
    <Route path="/brands-directory" page="brands-directory" component={Shop} />
    <Route path="/new/:id" component={Pdp} />
    <Route path="/wishlist" component={loadAsync('wishlist')} />
    <Route exact path="/sitemap" component={loadAsync('sitemap')} />
    <Route path="/:category/:brand/:style/:id/buy" component={Pdp} />
    <Route path="/reviews/:styleID" component={loadAsync('detailedReviews')} />
    <Route path="/check-delivery-availability" component={loadAsync('checkDeliveryAvailability')} />
    <Route path="/check-delivery-availability/:pinCode" component={loadAsync('checkDeliveryAvailabilityResult')} />
    <Route path="/:path-:id-buy.htm" component={Pdp} />
    <Route path="/:id" component={PdpSearchSwitch} />
    <Route path="*" component={loadAsync('pageNotFound')} />
  </Route>
);

const getRoutes = () => {
  const store = initStore();
  const history = syncHistoryWithStore(browserHistory, store);
  removeItem('urt');
  return (
    <Provider store={store}>
      <Router history={history}>{routes}</Router>
    </Provider>
  );
};

export default getRoutes;
export { routes };
