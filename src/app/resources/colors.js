var BLACK = require('./black');
var MELON = require('./melon');
var ORANGE = require('./orange');
var MYNT = require('./mynt');
var BLUE = require('./blue');

var COLORS = {
  "blue": "#118BEB",
  "link": "#526CD0",
  "new_link": "#ff3e6c",
  "red": "#ee5f73",
  "green": "#20BD99",
  "green_light": "#11DAB1",
  "ultra_dark": "#282C3F",
  "light_pink": "#FEEDF6",
  "light_yellow": "#FCF0E2",
  "dark": "#696E79",
  "dark_new": '#535665',
  "dark_new2": '#535766',
  "dark_2": '#7e808c',
  "dark_3": "#d4d5d9",
  "border": "#e9e9ed",
  "white_60": "rgba(255,255,255,0.6)",
  "medium": "#94989F",
  "medium_60": "#BFC1C5",
  "dark_pink": "#EA5E8B",
  "dark_pink_10": "#ff517b",
  "light": "#D5D6D9",
  "ultra_light": "#F5F5F6",
  "white": "#FFFFFF",
  "off_white_20": '#fef9e5',
  "bberry": "#FAFBFC",
  "bberry_10": "#EAEAEC",
  "transparent": "rgba(0,0,0,0)",
  "peach": "#ffeedf",
  "red": "#f16565",
  "light_purple": '#fde3f3',
  "gold": "#FFF5E1",
  "light_red": "#ff3f6c",
  "light_pink2": "#fff0f4",
  "sky_blue": "#a5cff9",
  "sky_blue2": "#f3f4ff",
  "dark_white": "#f4f4f4",
  "light_magenta": "#ecd5f9",
  "Blue_Zodiac1" : "#3e4152"
};

module.exports = Object.assign(COLORS, BLACK, MELON, ORANGE, MYNT, BLUE);
