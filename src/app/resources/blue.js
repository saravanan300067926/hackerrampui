module.exports = {
  'blue_100': '#526CD0',
  'blue_90': '#3466E8',
  'blue_20': '#DDE2F6',
  'blue_10': '#EEF1FB'
};
