import React from "react";

const SvgCardClothes = props => (
  <svg viewBox="0 0 70 70" {...props}>
    <defs>
      <path id="card_clothes_svg__a" d="M31.24 34.256H0V.442h31.24v33.814z" />
      <path id="card_clothes_svg__c" d="M6.74 19.785V.317H.33v19.468h6.41z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <g transform="translate(.718 34.6)">
        <mask id="card_clothes_svg__b" fill="#fff">
          <use xlinkHref="#card_clothes_svg__a" />
        </mask>
        <path
          fill="#B7B7B7"
          d="M0 13.233S3.52 3.238 4.094 2.76C4.668 2.283 9.612.356 10.057.445c.469.094 6.498 2.582 13.707.713 0 0 3.212.252 4.361 1.78 1.149 1.53 3.115 10.686 3.115 10.686l-4.806 2.76-.623 17.453-21.216.42-.857-18.229L0 13.233z"
          mask="url(#card_clothes_svg__b)"
        />
      </g>
      <path
        fill="#D9DBDB"
        d="M4.545 50.628L4.9 37.36.807 47.834zm22.695.356l.535-14.425s1.157.535 2.047 3.117c.89 2.582 2.225 8.548 2.225 8.548l-4.806 2.76zM11.131 35.045c.406 3.057 2.542 5.519 5.251 6.145 3.412.788 7.24-1.448 8.366-5.343a22.456 22.456 0 01-13.617-.802"
      />
      <path
        stroke="#B7B7B7"
        strokeWidth={0.5}
        d="M10.864 35.045c.534.09 7.031-3.828 7.476-4.006.445-.179 1.246-.179 1.425.267.178.445 4.983 4.541 4.983 4.541M12.11 38.162l11.57-.178m-4.895-7.213l-.178-2.85s-1.602-.355-1.157-1.246c.445-.89 1.246-.98 1.513-.356"
      />
      <path
        fill="#E06194"
        d="M28.945 33.892s4.688-13.77 5.453-14.429c.765-.659 7.35-3.312 7.943-3.19.624.13 8.654 3.558 18.257.982 0 0 4.28.347 5.809 2.453 1.53 2.107 4.15 14.722 4.15 14.722l-6.402 3.803-.83 24.046-28.26.578-1.141-25.114-4.98-3.85z"
      />
      <path
        fill="#D25B8C"
        d="M42.465 16.208s6.366 7.354 7.68 9.324c0 0 7.154-4.662 9.516-7.945 0 0-9.648 2.298-17.196-1.38"
      />
      <g transform="translate(63.817 18.451)">
        <mask id="card_clothes_svg__d" fill="#fff">
          <use xlinkHref="#card_clothes_svg__c" />
        </mask>
        <path
          fill="#D25B8C"
          d="M1.357.317S.237 20.073.337 19.782c.101-.29 6.533-3.244 6.402-3.803C6.608 15.419 3.72.843 1.357.317"
          mask="url(#card_clothes_svg__d)"
        />
      </g>
      <path
        fill="#D25B8C"
        d="M28.945 33.892c.393-.02 4.97 4.438 4.979 3.85.009-.588.474-18.279.474-18.279l-5.453 14.43zm27.697-.677h5.25v6.566L59.4 43.064l-2.626-3.283z"
      />
      <path
        stroke="#B7B7B7"
        strokeWidth={0.5}
        d="M42.465 16.208c.263-.329 6.695-5.188 7.351-5.844.657-.657 2.1-1.708 3.938.788 1.838 2.495 6.844 6.102 6.844 6.102M45.616 19.82l11.682.262M50.473 9.707V3.666s3.019-.92 1.312-2.364C50.08-.143 49.422.645 49.422.645s-.656.657-.524 1.314"
      />
    </g>
  </svg>
);

export default SvgCardClothes;
