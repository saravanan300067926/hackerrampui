import React from "react";

const SvgLine = props => (
  <svg viewBox="0 0 2 144" {...props}>
    <path
      fill="none"
      stroke="#9B9B9B"
      strokeDasharray={2}
      strokeLinecap="square"
      d="M1 0v144"
    />
  </svg>
);

export default SvgLine;
