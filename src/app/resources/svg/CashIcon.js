import React from "react";

function SvgComponent(props) {
  return (
    <svg
      id="prefix__Layer_1"
      data-name="Layer 1"
      viewBox="0 0 24 24"
      {...props}
    >
      <defs>
        <style>{".prefix__cls-2{fill:#d9dbdb;fill-rule:evenodd}"}</style>
      </defs>
      <path d="M0 0h24v24H0z" fill="none" />
      <path className="prefix__cls-2" d="M1.73 18.52h21.81V6.46H1.73z" />
      <path d="M1.28 18.07h21.81V6H1.28z" fill="#b7b7b7" fillRule="evenodd" />
      <path className="prefix__cls-2" d="M.75 17.3h21.81v-12H.75z" />
      <path
        d="M16 11.28A4.3 4.3 0 1111.65 7 4.29 4.29 0 0116 11.28z"
        fill="#e06194"
        fillRule="evenodd"
      />
      <path
        d="M12.29 9h1l.35-.52h-3.58L9.71 9h1a1.27 1.27 0 011.05.57h-1.71l-.35.53h2.13s0 1.14-1.93.94v.52l2.1 2.5h.79V14l-2-2.43s1.57 0 1.82-1.36h.66l.35-.53h-1a1.22 1.22 0 00-.33-.68"
        fill="#fff"
        fillRule="evenodd"
      />
    </svg>
  );
}

export default SvgComponent;

