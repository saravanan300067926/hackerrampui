import React from "react";

function SvgComponent(props) {
  return (
    <svg viewBox="0 0 24 23" {...props}>
      <defs>
        <path id="prefix__a" d="M.015.987h4.27V.022H.015v.965z" />
        <path id="prefix__c" d="M.007 4.97h4.74V.078H.006v4.89z" />
      </defs>
      <g fill="none" fillRule="evenodd">
        <path d="M0 0h24v24H0z" />
        <path
          d="M7.823 17.068s1.018.96 2.52.556l1.502-.404s.419-.683.613-.481c.193.202-.037.53.55.229.412-.212 1.308-.102.436.353-.872.454-4.022 2.069-5.427 1.262-1.405-.808-.92-1.262-.92-1.262l.726-.253z"
          fill="#C4C4C4"
        />
        <g transform="translate(.825 21.841)">
          <mask id="prefix__b" fill="#fff">
            <use xlinkHref="#prefix__a" />
          </mask>
          <path
            d="M4.285.274L4.15 1.598S.033 1.588.02 1.536C.008 1.484.019.022.019.022l4.266.252z"
            fill="#C4C4C4"
            mask="url(#prefix__b)"
          />
        </g>
        <path
          d="M17.647 3.681s.254 2.688 1.962 2.84c1.708.15 1.672-2.537 1.6-3.218-.073-.682-2.69-2.953-3.562.378"
          fill="#D9DBDB"
        />
        <path
          d="M17.429 3.946c.05.345 3.297-1.356 3.297-1.356l.482-.612s-.617-1.4-2.071-1.06c-1.454.34-1.817 2.271-1.708 3.028"
          fill="#B7B7B7"
        />
        <path
          d="M20.726 2.59c.083.07.555 1.545.555 1.545s.654-1.362-.073-2.157l-.482.612z"
          fill="#B7B7B7"
        />
        <path
          d="M19.028 6.407v.795s1.163.757 1.599-.114v-.946s-.473.605-1.6.265"
          fill="#D9DBDB"
        />
        <path
          d="M18.955 7.012c-.109-.038-2.398.946-2.47 1.325-.074.379-.26 2.686-.26 2.686l.945-.1.114-.731-.073 2.422.218 4.694 4.724-.227.29-5.716.614-.039s.368-3.102-.504-3.594c-.872-.493-1.853-.909-1.853-.909l-.073.265s-.436.795-1.6.114l-.072-.19z"
          fill="#C4C4C4"
        />
        <path
          fill="#C4C4C4"
          d="M16.302 11.025l-.072 1.627h.727l.109-1.741zM13.439 14.487l.061 2.158a.13.13 0 00.132.129l4.524-.147.036-2.385-4.753.245z"
        />
        <path
          d="M18.192 14.242c.037-.038 2.18-.984 2.18-.984v2.271l-2.216 1.098.036-2.385z"
          fill="#B7B7B7"
        />
        <path
          d="M12.767 14.32c.006.105.09.187.19.187.958.002 5.4.007 5.4-.027 0-.037.024-.731.024-.731l-5.44.074a.196.196 0 00-.189.21l.015.286z"
          fill="#D9DBDB"
        />
        <path fill="#D9DBDB" d="M18.326 13.73l2.163-1.06v.6l-2.157 1.21z" />
        <path
          d="M12.894 13.843c-.073-.075 3.336-1.19 3.336-1.19l4.258-.039c.035 0 .046.05.014.065l-2.194 1.095-5.414.07z"
          fill="#E06194"
        />
        <path
          fill="#B7B7B7"
          d="M17.436 17.105l-.024.908 4.705-.061.019-.948z"
        />
        <g transform="translate(17.391 17.859)">
          <mask id="prefix__d" fill="#fff">
            <use xlinkHref="#prefix__c" />
          </mask>
          <path
            d="M.03.12C-.008.273-.073 3.16.51 5.544c0 0 1.672.228 1.672.114s.145-3.672.145-3.558v3.52s1.744.076 1.78-.038c.037-.113.786-4.294.614-5.502L.029.12z"
            fill="#C4C4C4"
            mask="url(#prefix__d)"
          />
        </g>
        <path fill="#E2E0E0" d="M19.246 17.838h1.272v-.72h-1.272z" />
        <path
          d="M2.74 9.347s-.872 2.473.34 3.431c1.21.96 2.732-.846 3.149-1.413.093-.126.722-1.326.576-1.578-.145-.253-4.065-.44-4.065-.44"
          fill="#D9DBDB"
        />
        <path
          d="M7.198 9.7s2.18-.303 1.793.151c-.388.454-2.229.202-2.229.202l.043-.392.393.039z"
          fill="#C4C4C4"
        />
        <path
          d="M5.115 9.548s-.679.404-.824 1.161h-.34s.34-.706-.193-.656c-.533.05-.97 1.918-.97 1.918l-.29-.202s-.194-1.918.242-2.423l2.375.202zM7.24 17.486l.594-.295a.275.275 0 00.1-.399c-.46-.67-1.744-2.443-2.968-3.2-.24-.148-.506.147-.346.386l2.297 3.42c.073.107.21.145.324.088"
          fill="#B7B7B7"
        />
        <path
          d="M7.116 17.786s.848 1.388 3.711 1.15c2.13-.178 1.958-.229 1.958-.229s.557-.403.654-.151c.097.252-.334.43-.189.48.145.051.37.088.824-.05.465-.14.92.05.29.354-.63.302-1.502.302-2.035.15 0 0-2.304.922-4.741.026a3.037 3.037 0 01-1.096-.72l-.273-.278.897-.732zM23.008 11.326s-.818 5.023-4.125 4.948l.109-.19s1.199-.605 1.09-.795c0 0-.763-.265-.364-.302.4-.038.763-.114.982-.341.218-.227 1.436-2.247 1.509-3.27.109 0 .8-.05.8-.05"
          fill="#D9DBDB"
        />
        <path
          d="M7.21 17.813a.32.32 0 00.123-.368c-.28-.832-1.238-3.594-1.69-3.809-.533-.252-3-.35-3.539-.114-1.24.54-1.21 2.89-1.21 2.89l-.05 5.64 4.266.063-.146-5.652.78 2.049a.18.18 0 00.268.09l1.198-.789zM2.623 9.334l4.575.467s.34-2.271-1.211-2.827c-1.4-.5-2.918.849-3.557 1.97-.092.163.012.371.193.39"
          fill="#C4C4C4"
        />
        <path
          d="M13.518 15.075s.342.178.304.68c-.016.228-.187.425-.406.43-.162.003-.318-.11-.298-.694.036-.454.4-.416.4-.416"
          fill="#BBBBBA"
        />
      </g>
    </svg>
  );
}

export default SvgComponent;

