import '../utils/localStoragePolyfill';
import '../utils/thirdPartyIntegrations';
import ReactDOM from 'react-dom';
import getRoutes from '../routes';
import header from '../components/Header/client';
import footer from '../components/Footer/client';
import { getMyx, isAbEnabled } from '../services/RemoteConfig';
import React from 'react';
import PostLoginAction from './PostLoginAction';

getMyx().then(() => {
  const abConfig = {
    key: 'desktop-user-onboarding',
    control: 'VariantA',
    test: 'VariantB'
  };
  const dimension = isAbEnabled(abConfig, true) ? 'desktop_ab_test' : 'desktop_ab_control';
  if (window.ga) {
    window.ga('set', 'dimension59', dimension);
  }
});

import loadThirdPartyScripts from "../utils/loadThirdPartyScripts";

if (window.location.hostname.includes('.myntra.com')) {
  header();

  ReactDOM.render(
    <PostLoginAction />,
    document.getElementById('amnHalfCard')
  );

  ReactDOM.render(
    getRoutes(),
    document.getElementById('mountRoot')
  );


  try {
    footer();
    loadThirdPartyScripts();
  } catch (e) {
    console.log('error',e);
  }
}
