import React from 'react';
import Modal from '../../common/Modal';
import styles from './PostLoginAction.css';
import AmnIcon from './AmnIcon.jsx';
import Notify from '../../components/Notify'

export default class PostLoginAction extends React.Component{
  constructor(props){
    super(props);
    this.state={
      show:false
    }
    this.onClose = this.onClose.bind(this);
    this.showInfoBar = this.showInfoBar.bind(this);
    this.updatedNumberMessage = this.updatedNumberMessage.bind(this);
    this.loggedInMessage = this.loggedInMessage.bind(this);
    this.amnMessage = this.amnMessage.bind(this);
  }

  componentDidMount() {
    this.urlSearchParam = new URLSearchParams(window.location.search);
    const showAmnHalfCard = this.urlSearchParam.get('showAmn');
    const updatedNumber = this.urlSearchParam.get('updatedNumber');
    const loggedIn = this.urlSearchParam.get('loggedIn');
    const amnAdded = this.urlSearchParam.get('amnAdded');

    showAmnHalfCard == "true" && this.showModal();
    updatedNumber && this.updatedNumberMessage(updatedNumber);
    loggedIn === "true" && this.loggedInMessage();
    amnAdded === "true" && this.amnMessage();
  }

  showModal() {
    this.urlSearchParam.delete("showAmn");
    this.replacePageUrl();
    this.setState({
      show: true
    });
  }

  updatedNumberMessage(updatedNumber) {
    this.urlSearchParam.delete("updatedNumber");
    const message = `Logged in successfully. Your mobile number has beed updated to ${
      updatedNumber
    }`;
    this.showInfoBar(message);
    this.replacePageUrl();
  }

  loggedInMessage() {
    this.urlSearchParam.delete("loggedIn");
    const message = "Logged in successfully";
    this.showInfoBar(message);
    this.replacePageUrl();
  }

  amnMessage() {
    this.urlSearchParam.delete("amnAdded");
    const message = "Alternate mobile number added";
    this.showInfoBar(message);
    this.replacePageUrl();
  }

  replacePageUrl() {
    const urlPrams = this.urlSearchParam.toString();
    const url = `${window.location.pathname}${urlPrams ? "?" : ""}${urlPrams}`;
    window.history.replaceState({},'',url);
  }


  showInfoBar(message) {
    this.refs.notify.info({
      message,
      position: 'center'
    });
  }

  amnButtonClick(){
    const urlSearchParam = new URLSearchParams(window.location.search);
    const referer = window.location.href;
    urlSearchParam.set('referer', referer);
    const url = `/alternateNumber?${urlSearchParam.toString()}`;
    window.location.href = url;
  }



  onClose(){
    this.setState({
      show:false
    })
  }

  render() {
    return (
      <div>
       {this.state.show && <Modal onClose={this.onClose} >
          <div className={styles['amnContainer']}>
          <AmnIcon width='96px' height='96px' />
          <div className={styles['amnHeading']}> Add an alternate mobile </div>
          <div className={styles['amnDescription']}> Secure your account with alternate mobile in case you need to recover your account </div>
         <div className={styles['amnButton']} onClick={this.amnButtonClick}> ADD ALTERNATE NUMBER</div>
          </div>
        </Modal>}
        <Notify ref='notify' />
        </div>
    )
  }
}