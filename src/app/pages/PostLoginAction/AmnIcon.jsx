import React from 'react';
const AmnIcon = props => (
  <svg width='24px' height='24px' viewBox='0 0 24 24' {...props}>
    <g stroke='none' strokeWidth={1} fill='none' fillRule='evenodd'>
      <circle fill='#FFF5F7' cx={12} cy={12} r={12} />
      <path
        d='M2.353 0c.43 0 .778.35.778.78v9.144c0 .43-.348.778-.778.778H.3v-.81a.5.5 0 00.29-.367l.008-.09A.497.497 0 00.3 8.98v-.751h2.248V1.315H.3V.849h.913c.045 0 .083-.054.097-.129l.005-.059c0-.104-.046-.188-.102-.188H.3V0z'
        transform='translate(11.7 6.6)'
        fill='#DF8D9E'
        fillRule='nonzero'
      />
      <path
        d='M3.3 0v.473H1.99c-.045 0-.083.054-.096.129l-.005.06c0 .103.045.187.101.187H3.3v.466H.655V8.23H3.3l.001.751a.492.492 0 00-.11-.034l-.09-.008a.497.497 0 10.2.953l-.001.811H.85a.779.779 0 01-.772-.673l-.007-.105V.78C.07.35.419 0 .85 0H3.3z'
        transform='translate(8.7 6.6)'
        fill='#F3B1BF'
        fillRule='nonzero'
      />
      <ellipse fill='#FFF5F7' cx={14.4} cy={11.4} rx={3} ry={2.7} />
      <g transform='translate(12.6 9.6)' fill='#FF3F6C'>
        <path
          d='M1.8 0a.3.3 0 01.3.3v3a.3.3 0 01-.6 0v-3a.3.3 0 01.3-.3z'
          transform='rotate(90 1.8 1.8)'
        />
        <rect x={1.5} y={0} width={1} height={3.6} rx={0.5} />
      </g>
    </g>
  </svg>
);

export default AmnIcon;