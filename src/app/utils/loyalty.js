import { getFeatures } from './index';
import get from 'lodash/get';

const mfuEnable = getFeatures('mfu.enable') === true;
const mfuEnableWishlist = getFeatures('mfu.enable.wishlist') === true;
const maxPercentOfCart = getFeatures('loyaltypoints.maxPercentOfCart');
const conversionFactor = getFeatures('loyaltypoints.conversionFactor') || 1;
const percentage = maxPercentOfCart ? parseInt(maxPercentOfCart, 10) / 100.0 : 0;

export default function getLoyalty(
  product,
  pageType = 'list'
) {
  let discountedPrice = product.price;
  let loyaltyFlag = get(product, 'loyaltyPointsEnabled', false);

  if (pageType === 'pdp') {
    discountedPrice = get(product, 'price.discounted', false);
    loyaltyFlag = get(product, 'flags.loyaltyPointsEnabled', false);
  }

  let loyaltyEnabled = loyaltyFlag && mfuEnable && percentage;

  if (pageType === 'wishlist') {
    loyaltyFlag = get(product, 'isLoyaltyPointsEnabled', false);
    loyaltyEnabled = loyaltyFlag && mfuEnableWishlist && percentage;
    discountedPrice = get(product, 'price', false);
  }

  const loyaltyDiscount = Math.floor(percentage * discountedPrice);
  const loyalty = {};

  loyalty.enabled = loyaltyEnabled;
  loyalty.discount = parseInt((discountedPrice - loyaltyDiscount), 10);
  loyalty.points = parseInt((loyaltyDiscount / conversionFactor), 10);

  return loyalty;
}
