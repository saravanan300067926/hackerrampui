import get from 'lodash/get';
import getKvPair from './kvpairs';
const SA_IT_PREFIX = 'SA_IT_', // A regular image tag passed down via system attributes
  SA_XT_PREFIX = 'SA_XT_', // System Attributes Xcelerator Tags prefix
  XT_AB_PREFIX = 'tags.',
  LOCAL_XT_PREFIX = 'LOCAL_XT_';

const dynamicPriorities = {
  plp: {
    top: [
      'FIELD_productimagetag',
      'FIELD_eorsPicksTag',
      'SA_IT_productImageTag',
      'LOCAL_XT_new',
    ],
    bottom: [
      'PERSONALISED_PRICING',
      'MFU',
      'FIELD_tdBxGyText',
      'LOCAL_XT_fewLeft',
      'SA_XT_Best_Price'
    ]
  },
  pdp: {
    top: ['SA_IT_productImageTag', 'API_TAGS', 'LOCAL_XT_new'],
    bottom: [
      'PERSONALISED_PRICING',
      'MFU',
      'PRICE_OFFER',
      'LOCAL_XT_fewLeft',
      'EXPRESS_DELIVERY',
      'API_XT_BOTTOM',
      'SA_XT_Best_Price'
    ]
  }
};

const xceleratorTagsConfig = getKvPair('xceleratorTagsConfig') || {
    bottom: [
      {
        name: 'tags.fewLeft',
        tagText: 'Only Few Left!',
        skuThreshold: '15',
        minSku: '2'
      },
      {
        name: 'tags.freeShipping',
        tagText: 'Free Shipping!',
        freeShippingThreshold: '1500'
      }
    ],
    top: [
      {
        name: 'tags.new',
        tagText: 'NEW',
        tdThreshold: '1',
        daysThreshold: '60'
      }
    ]
  };

/**
 * IMPORTANT
 * Whenever new Xcelerator Tags are created (If you do not know what they are, you should
 * not be messing with this file), an entry needs to be added to #tagFunctionMap for the
 * tag with key as it's corresponding tag name, and value as a function that returns
 * the tag's corresponding text string if the tag is applicable (null otherwise). Whatever
 * parameters the function needs should be passed in as part of a single object, and
 * destructured at definition.
 */
const tagFunctionMap = {
  /**
   * FEW LEFT TAG HEURISTIC - Applicable on styles for which of all SKUs, availability of at
   * least #minSku is less than #skuThreshold
   */
  'tags.fewLeft': ({ skuCounts, tagText, skuThreshold, minSku }) => {
    if (!skuThreshold || !minSku) return null;
    const depletingSkuCount = skuCounts.reduce(
      (count, sku) => (count += sku > 0 && sku < skuThreshold ? 1 : 0),
      0
    );
    return depletingSkuCount >= Math.min(minSku, skuCounts.length)
      ? tagText
      : null;
  }
};

/**
 * This function takes raw style data as received from various sources, and returns
 * an applicable Xcelerator Tag. In case multiple tags are applicable, the returned
 * tag is the one that appears earliest in the #config.priorityList parameter.
 *
 * @export
 * @param {string} source - The page from where this function has been called (ex.: WISHLIST)
 * @param {Object} data - The data object that the source page uses to render it's own content, passed as-is
 * @param {Object} enabledTags - Set of tags which are enabled for the current user/device
 * @param {Object} config - List of tag specific onfiguration values required for checking tag applicability
 * @returns {string} - Tag text of the highest priority applicable tag, null if no tags are applicable
 */
export function getApplicableBottomTag(source, data, config) {
  if (!config || !config.bottom || !data) return null;
  // Extract required information based on source
  let price, inventoryInfo;
  switch (source.toUpperCase()) {
    case 'PDP': {
      inventoryInfo = get(data, 'sizes');
      price = get(data, 'price');
      break;
    }
    case 'PLP': {
      inventoryInfo = get(data, 'inventoryInfo');
      price = get(data, 'discountedPrice');
      break;
    }
    default:
      return null;
  }
  
  // get skuCounts
  const skuCounts =
    (inventoryInfo &&
      inventoryInfo.map(skuEntry => skuEntry && skuEntry.inventory)) ||
    [];
  // If any of these don't exist, further tests are meaningless, so return
  if (!(skuCounts && skuCounts.length && price)) return null;
  const testPayload = {
    skuCounts,
    price
  };
  // Iterate over the tag list and return the first applicable one
  for (const tag of config.bottom) {
    if (!tagFunctionMap.hasOwnProperty(tag.name) )
      continue;
    const tagText = tagFunctionMap[tag.name]({
      ...testPayload,
      ...tag
    });
    if (tagText)
      return {
        type: tag.name,
        label: tagText
      };
  }
  // No applicable tag found, return
  return null;
}

/**
 * This function returns a new function that closures over a priority list and a list
 * of tuples made of matchers and corresponding handlers. The new function can then be
 * called with some item data, and returns a `{ type, data }` object where `type` is the
 * highest priority applicable type, and `data` is the data returned by the corresponding
 * handler. A `type` is considered applicable when it matches one of the matchers in the
 * tuple list, and its corresponding handler when called with `itemData` and `type` returns
 * a non-falsy value. If no applicable `type` is found, the function returns a `null`.
 *
 * The `handlerTuplesList` is a list of [ `matcher`, `handler` ] tuples such that:
 *   - `matcher` is either a string in which case it is directly matched with the types from the `priorityList`
 *     OR `matcher` is a regular expression against which the type is tested
 *   - `handler` is a function to which the `itemData` and `type` are passed and which returns the corresponding
 *       type specific data if applicable, and falsy otherwise
 *`
 * @export
 * @param {string[]} priorityList - List of types in decreasing order of priority
 * @param {[string|RegExp, Function][]} handlerTuplesList - A list of [ matcher, handler ] tuples
 * @returns {function} - A prioritizer function that accepts item data and returns the highest priority
 * applicable `type` object in the form of `{ type, data }` if found or a `null`
 */
export function genericPrioritizer(priorityList = [], handlerTuplesList = []) {
  return function(itemData) {
    for (const type of priorityList) {
      const [, handler] =
        handlerTuplesList.find(
          ([matcher]) =>
            typeof matcher === 'string' ? matcher === type : matcher.test(type)
        ) || []; // This allows safe destructuring of the handler above if no item is found
      // A GOTCHA! - Not passing a function as the second item of the tuple would result
      // in ignoring of that type.
      if (typeof handler !== 'function') continue;
      // Another GOTCHA! - Handlers returning falsy values such as `0` will also be treated
      // as non-applicable! If such functionality has to be supported, the simple check below
      // on `data` could probably be modilfied to type checks. If you want it, YOU DO IT. :P
      const data = handler(itemData, type);
      if (!data) continue;
      return {
        type,
        data
      };
    }
    // No item from the priority list is applicable, return a `null`
    return null;
  };
}

export function getSystemAttributesValue(systemAttributes, name='') {
  if (!systemAttributes) return null;
  const attributeEntry = systemAttributes.find(
    ({
      systemAttributeValueEntry: {
        systemAttributeEntry: { attributeName ='' } = {}
      } = {}
    }) => name.toUpperCase() === attributeName.toUpperCase()
  );
  const { systemAttributeValueEntry: { valueName = null } = {} } =
    attributeEntry || {};
  return valueName;
}

function getSystemAttributes(systemAttributes, attributeName) {
  const saEntry =
    systemAttributes &&
    systemAttributes.find(entry => entry['attribute'] === attributeName);
  return saEntry && saEntry['value'];
}

export function getTagObject(label, type, siblingView = false) {
  return {
    label,
    type,
    siblingView
  };
}

export function getLocalImageTagPrioritizer(pageType) {
  const priorityConfig = getKvPair('xceleratorTagsPriority') || dynamicPriorities;
  const topPriorityList = get(
    priorityConfig,
    `${pageType}.top`,
    priorityConfig[pageType].top
  );
  const isPDP = pageType === 'pdp';
  return genericPrioritizer(topPriorityList, [
    // An Image Tag passed down via system attributes such as the `productImageTag` attribute,
    // represented as `SA_IT_productImageTag` in the priority list
    [
      new RegExp(`^${SA_IT_PREFIX}`),
      ({ systemAttributes = [] } = {}, type) => {
        const label = getSystemAttributesValue(
          systemAttributes,
          type.slice(SA_IT_PREFIX.length)
        );
        return label ? getTagObject(label, type) : null;
        //return
      }
    ],

    // Xcelerator Tags passed down via the system attributes pipeline, represented as `SA_XT_Trending`
    // in the priority list
    [
      new RegExp(`^${SA_XT_PREFIX}`),
      ({ systemAttributes = [] } = {}, type) => {
        const label = isPDP
          ? getSystemAttributesValue(systemAttributes, type)
          : getSystemAttributes(systemAttributes, type);
        return label ? (isPDP ? getTagObject(label, type) : label) : null;
      }
    ]
  ]);
}

export function getLocalMiscDataPrioritizer(pageType) {
  const priorityConfig = getKvPair('xceleratorTagsPriority') || dynamicPriorities;
  const pdpBottomPriorityList =
    priorityConfig[pageType].bottom ||
    get(
      priorityConfig,
      `${pageType}.bottom`,
      priorityConfig[pageType].bottom
    );
  return genericPrioritizer(pdpBottomPriorityList, [
    // Xcelerator Tags passed down via the system attributes pipeline, represented as `SA_XT_Best_Price`
    // in the priority list
    [
      new RegExp(`^${SA_XT_PREFIX}`),
      ({ systemAttributes = [] } = {}, type) => {
        const label = getSystemAttributesValue(systemAttributes, type);
        return label ? getTagObject(label, type, true) : null;
      }
    ],
    
    [
      new RegExp(`^${LOCAL_XT_PREFIX}`),
      (data, type) => {
        const bottomTag = getApplicableBottomTag(
          pageType,
          data,
          xceleratorTagsConfig
        );
        return bottomTag &&
          bottomTag.type === type.replace(LOCAL_XT_PREFIX, XT_AB_PREFIX)
          ? bottomTag.label
          : null;
      }
    ]
  ]);
}
