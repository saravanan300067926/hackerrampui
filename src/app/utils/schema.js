import at from 'v-at';
import { getProductImageUrl } from '../../app/components/Pdp/utils';
import last from 'lodash/last';
import mapStateToProps from '../components/Search/Breadcrumbs/mapStateToProps';
import { getDefaultSellerInfo } from '../components/Pdp/helper';

function schemaMarkUp() {
	return `
		<script type="application/ld+json">
	        {
	        	"@context" : "https://schema.org",
	            "@type" : "Organization",
	            "Name" : "Myntra",
	            "URL" : "https://www.myntra.com",
	            "contactPoint" : [{
	            	"@type" : "ContactPoint",
	            	"telephone" : "+91-80-61561999",
	            	"contactType" : "Customer Service"
	            }],
	            "logo" : "https://constant.myntassets.com/web/assets/img/logo.png",
	            "sameAs" : [
	            	"https://www.facebook.com/myntra",
	            	"https://twitter.com/myntra",
	            	"https://plus.google.com/+myntra",
	            	"https://www.instagram.com/myntra",
	            	"https://www.youtube.com/user/myntradotcom"
	            ]
	        }
	    </script>
    `;
}

// As per google single page structed data
// https://search.google.com/structured-data/testing-tool
// Added ratingCount and ratingValue
// As we get totalCount as ratingCount.
function pdpSchemaMarkUp(data) {
  const album = at(data, 'media.albums.0');
  const imageSource = at(album, 'images.0');
  const imgUrl = getProductImageUrl(imageSource, {
    width: 1080,
    height: 1440,
    q: 100
  });
  const brandName = at(data, 'analytics.brand') || at(data, 'brand.name') || '';
  const avgRating = at(data, 'ratings.averageRating');
  const name = at(data, 'name') || '';
  const landingPageUrl = at(data, 'landingPageUrl') || '';
  const topReview = at(data, 'ratings.reviewInfo.topReviews.0');
  const reviewRating = at(topReview, 'userRating') || 0;
  const author = at(topReview, 'userName') || '';
  const reviewText = at(topReview, 'reviewText') || '';
  const isFastFashion = at(data, 'ratings.isFastFashion') || false;
  const sizes = data && at(data, 'sizes') && data.sizes.length > 0
	? data.sizes
	: [];
  const availableSize = sizes.find((size) => !!size.available);
  const availability = availableSize
	? 'InStock'
	: 'OutOfStock';
  let defaultSellerInfo = null;
  try {
	defaultSellerInfo = data && getDefaultSellerInfo(data);
  } catch (err) {
	// handle error
  }
  const price = defaultSellerInfo && defaultSellerInfo.discountedPrice
	? defaultSellerInfo.discountedPrice
	: null;

  return `
		<script type="application/ld+json">
	        {
	        	"@context" : "https://schema.org",
	            "@type" : "Product",
	            "name" : "${name}",
	            "image" : "${imgUrl}",
				"sku" : "${data.id}",
				"mpn" : "${data.id}",
				"description" : "${name}",
				"offers": {
                    "@type": "Offer",
					"priceCurrency": "INR",
					"availability": "${availability}",
					${price ? `"price" : "${price}",` : ''}
					"url": "https://www.myntra.com/${landingPageUrl.toLowerCase()}"
                },
	            "brand" : {
	            	"@type" : "Thing",
	            	"name" : "${brandName}"
				}
				${avgRating && !isFastFashion ? `,"AggregateRating": {
					"@type": "AggregateRating",
					"itemReviewed": "${brandName}",
					"ratingCount": "${at(data, 'ratings.totalCount') || 0}",
					"ratingValue": "${avgRating}"
				}` : '' }
				${topReview && !isFastFashion ? `,"review" : {
					"@type": "Review",
					"reviewRating": {
						"@type": "Rating",
						"ratingValue": "${reviewRating}"
					},
					"author": {
						"@type": "Person",
						"name": "${author}"
					},
					"itemReviewed": {
						"@type" : "Product",
						"name" : "${name}"
					},
					"reviewBody": "${reviewText}"
				}` : ''}
	        }
	    </script>
    `;
}

// JSON.stringify is used to remove the spaces
// which will help in reducing the bytes.
function plpSchemaMarkUp(data) {
	const products = data.results.products;
	var str = '';
	let ldJSON = {
		'@context': 'https://schema.org',
		'@type': 'ItemList',
		itemListElement: []
	};
	try {
		if(products && products.length > 0) {
			for (var i = 0; i < 10 && i < products.length; i++) {
			    const {
			        product,
			        landingPageUrl
			    } = products[i];

			    ldJSON.itemListElement.push({
			        "@type": "ListItem",
			        position: i + 1,
			        url: `https://www.myntra.com/${landingPageUrl.toLowerCase()}`,
			        name: product
			    });
			}
			str = str.concat(
			    `<script type="application/ld+json">${JSON.stringify(
			        ldJSON
			    )}</script>`
			);
		}
		return str;
	} catch (ex) {
	    console.log('PLP schema failed ', ex);
	    return '';
	}
}

function searchActionSchema() {
	return `
		<script type="application/ld+json">
			{
				"@context" : "https://schema.org",
				"@type" : "WebSite",
				"url" : "https://www.myntra.com/",
				"potentialAction" : {
					"@type" : "SearchAction",
					"target" : "https://www.myntra.com/{search_term_string}",
					"query-input" : "required name=search_term_string"
				}
			}
		</script>
	`;
}

function getLinkURL(link = '') {
  return "https://www.myntra.com/" + link.toLowerCase().replace(/ /g, '-');
}

function pdpBreadcrumbSchema(data) {
	let masterCategory = at(data, 'analytics.masterCategory') || '';
	if (masterCategory === 'Apparel') {
		masterCategory = 'Clothing';
	}
	const gender = at(data, 'analytics.gender') || '';
	const articleType = at(data, 'analytics.articleType') || '';
	const brandName = at(data, 'analytics.brand') || '';

	return `
		<script type="application/ld+json">
			{
				"@context" : "https://schema.org",
				"@type" : "BreadcrumbList",
				"itemListElement": [
					{
					    "@type": "ListItem",
					    "position": 1,
					    "item": {
						    "@id" : "${getLinkURL(masterCategory)}",
						    "name" : "${masterCategory}"
					    }
					},
					{
					    "@type": "ListItem",
					    "position": 2,
					    "item": {
						    "@id" : "${getLinkURL(`${gender} ${masterCategory}`)}",
						    "name" : "${gender}"
					    }
					},
					{
					    "@type": "ListItem",
					    "position": 3,
					    "item": {
						    "@id" : "${getLinkURL(articleType)}",
						    "name" : "${articleType}"
					    }
					},
					{
					    "@type": "ListItem",
					    "position": 4,
					    "item": {
						    "@id" : "${getLinkURL(`${brandName} ${articleType}`)}",
						    "name" : "${brandName}"
					    }
					},
					{
					    "@type": "ListItem",
					    "position": 5,
					    "item": {
						    "@id" : "${getLinkURL(brandName)}",
						    "name" : "More by ${brandName}"
					    }
					}
				]
			}
		</script>
	`;
}

function plpBreadcrumbSchema(data) {
  	try {
		const mapStateToPropsObj = mapStateToProps(data, 'schema');
		let crumbs = mapStateToPropsObj.crumbs;
		const searchTerm = mapStateToPropsObj.searchTerm;
		const stripSplCharecters = function (crumb) {
		    return crumb.replace(/[^0-9a-z]/gi, '');
		};
		const query = (mapStateToPropsObj.searchItem || '').replace(/\s/g, '-').toLowerCase();
		const lastBreadcrumb = last(crumbs).text.replace(/\s/g, '-').toLowerCase();
		if (stripSplCharecters(query) === stripSplCharecters(lastBreadcrumb)) {
		    last(crumbs).text = searchTerm;
		} else {
			crumbs.push({
				path: `/${searchTerm.replace(/\s+/g, '-').toLowerCase()}`,
			    text: searchTerm
			});
		}

		let breadcrumbList = [];
		crumbs.map((crumb, index) => (
		    breadcrumbList.push(
		        JSON.stringify({
		            "@type": "ListItem",
		            "position": index + 1,
		            "item": {
		                "@id" : "https://www.myntra.com" + crumb.path,
		                "name" : crumb.text
		            }
		        })
		    )
		));
		return `
			<script type="application/ld+json">
				{
					"@context" : "https://schema.org",
					"@type" : "BreadcrumbList",
					"itemListElement": [ ${breadcrumbList} ]
				}
			</script>
		`;
	} catch (ex){
		console.log('PLP schema failed ', ex);
		return '';
	}

}


export function renderSchema(data) {
	const pageName = data.props && data.props.pdpData ? 'pdp' : data.results ? 'search' : data.props && data.props.pageName === 'Home' ? 'Home' : '' ;
  	return `
		${schemaMarkUp()}
		${pageName === 'pdp' ? pdpSchemaMarkUp(data)  : ''}
		${pageName === 'search' ? plpSchemaMarkUp(data) : ''}
		${pageName === 'Home' ? searchActionSchema() : ''}
		${pageName === 'pdp' ? pdpBreadcrumbSchema(data) : ''}
		${pageName === 'search' ? plpBreadcrumbSchema(data) : ''}
	`;
}
