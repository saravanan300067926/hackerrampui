import at from 'v-at';

const Track = {
  event: function (category, action, label, screenName) {
    if(typeof MyntApp !== 'undefined' &&  typeof MyntApp.mynacoSendEvent === 'function'){
      const gaEventAction = {category, action, label};
      MyntApp.mynacoSendEvent(screenName, action, JSON.stringify(gaEventAction));
    } else if (at(window, 'ga')) {
      window.ga('send', 'event', category, action, label);
    }
  },

  screen: function (path) {
    if (at(window, 'ga')) {
      window.ga('send', 'pageview', path);
    }
  },

  ecommerce: function () {
    if (at(window, 'ga')) {
      window.ga.apply(null, arguments);
    }
  },

  traverse: function (children, id) {
    if (children && children.length) {
      for (let index = 0; index < children.length; index ++) {
        let item = children[index];
        if (at(item, 'props.campa_id') === id) {
          return `${item.type}_${index + 1}`;
        }
        if (at(item, 'children.length')) {
          const suffix = this.traverse(item.children, id);
          if (suffix) {
            return `${item.type}_${index + 1}|${suffix}`;
          }
        }
      }
    }
    return '';
  },

  bannerClicks: function(product) {
    const params = {
      name: at(product, 'props.id'),
      creative: at(product, 'props.src'),
      position: at(product, 'props.url')
    };
    this.ecommerce('ec:addPromo', params);
    this.ecommerce('ec:setAction', 'promo_click');
    this.event('promotion', 'banners-click');
  },

  landingPages: function (layout) {
    const campaId = at(layout, 'props.campa_id');
    if (window && campaId) {
      const path = window.location.pathname;
      const page = path.substring(path.lastIndexOf('/') + 1);
      const data = at(window, `__myx.layout_${page}`);

      if (data && data.children) {
        const position = this.traverse(data.children, campaId);
        const params = {
          id: campaId,
          name: at(layout, 'props.id'),
          creative: at(layout, 'props.src') || at(layout, 'props.url'),
          position: position
        };
        this.ecommerce('ec:addPromo', params);
        this.ecommerce('ec:setAction', 'promo_click');
        this.event('promotion', 'banners-click');
      }
    }
  }
};

export default Track;
