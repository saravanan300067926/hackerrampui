import get from "lodash/get";
import kvPairs from "./kvpairs";

const priorityCheckoutConfigValue = kvPairs("priorityCheckout.config") || {};

const priorityCheckoutConfig =
  typeof priorityCheckoutConfigValue === "string"
    ? JSON.parse(priorityCheckoutConfigValue)
    : priorityCheckoutConfigValue;

function priorityCheckout() {
  return get(priorityCheckoutConfig, "enable", false);
}

export const PRIORITY_CHECKOUT_MESSAGE = get(
  priorityCheckoutConfig,
  "buttonText",
  "EORS EARLY CHECKOUT"
).toUpperCase();

export const CART_FULL_ERROR_CODE = 110116;
export const PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE = `Item was not added to the bag. You've reached the max limit for the bag. Remove some items from the bag.`;

export const MAXIMUM_ALLOWED_CART_SIZE = get(priorityCheckoutConfig, "maxAllowedCartItems", 10);
export function priorityCheckoutCartFull () {
  const session = get(window, '__myx_session__', {});
  const totalCartItems = session['CART:totalQuantity'] || 0;
  return totalCartItems >= MAXIMUM_ALLOWED_CART_SIZE;
}

export default priorityCheckout;
