import { isLocalStorageEnabled, setItem, getItem, removeItem }  from './localStorageUtil';
import cookies from './cookies';
import features from './features';
import Client from '../services';
import at from 'v-at';

const WISHLIST_STORE_NAME = '__myx_wishlist_summary_store__';

const WISHLIST_COOKIE_NAME = 'mws';

const clearSummaryStore = () => {
  removeItem( WISHLIST_STORE_NAME );
}

const setSummaryStore = (newValue) => {
  if (newValue) {
  	setItem( WISHLIST_STORE_NAME , JSON.stringify(newValue));
  }
}

const getSummaryStore = () => {
  const wlSummary = getItem(WISHLIST_STORE_NAME);
  if (wlSummary) {
  	try {
  	  const summaryJson = JSON.parse(wlSummary);
  	  return summaryJson;
  	} catch(e) {
      console.log('Summary is not a proper json');
  	}
  }
  return null;
}

const updateSummaryStore = (newValue) => {
  if(isLocalStorageEnabled() && newValue) {
  	const storeValue = {
  	  summary: newValue,
  	  expiryTime: new Date().getTime() + 15*60*1000
  	};
  	setSummaryStore(storeValue);
  }
}

const fetchWishlistSummary = (successCallback, errorCallback) => {
  Client.get('/web/wishlistapi/summary').end((err, response) => {
    if ((err || at(response, 'body.status.statusType') === 'ERROR')) {
      if (errorCallback) {
        errorCallback();
      }
      clearSummaryStore();
      if (successCallback) {
        successCallback({});
      }
    } else if (response && successCallback) {
      let result = at(response, 'body.data.styleIds') || [];
      result = convertSummaryArrayToMap(result);
      updateSummaryStore(result);
      successCallback(result);
    }
  });
}

const isSummaryStoreExpired = () => {
  const summaryStoreCurrentValue = getSummaryStore();
  if (summaryStoreCurrentValue && summaryStoreCurrentValue.expiryTime) {
  	const currentTime = new Date().getTime();
  	if(currentTime > summaryStoreCurrentValue.expiryTime) {
      return true;
  	}
  }
  return false;
};

const convertSummaryArrayToMap = (summaryArray) => {
  let summaryMap = {};
  const len = summaryArray.length || 0;
  for (let i=0; i < len; i++) {
  	summaryMap[summaryArray[i]] = true;
  }
  return summaryMap;
};

export const setWStateChangedCookie = (value) => {
  cookies.set(WISHLIST_COOKIE_NAME, value);
}

export const getWishlistSummaryMap = (isLoggedIn, callback) => {
  const wlSummaryFetchDisabled = features('wishlist.summary.fetch.disable') === 'true';
  if (wlSummaryFetchDisabled || !isLoggedIn) {
  	clearSummaryStore();
    callback({});
  } else {
  	const summaryStateCookieValue = cookies.get(WISHLIST_COOKIE_NAME);
    if (summaryStateCookieValue && summaryStateCookieValue === 'true') {
  	  setWStateChangedCookie(false);
  	  fetchWishlistSummary(callback);
    } else {
  	  const summaryStoreCurrentValue = getSummaryStore() || {};
	  if (isLocalStorageEnabled() && summaryStoreCurrentValue.summary && !isSummaryStoreExpired()) {
	    callback(summaryStoreCurrentValue.summary);
	  } else {
	    fetchWishlistSummary(callback);
	  }
    }
  }
};

export const addProductToWlSummaryStore = (productId) => {
  const summaryStoreCurrentValue = getSummaryStore();
  if(productId && summaryStoreCurrentValue && summaryStoreCurrentValue.summary && !summaryStoreCurrentValue.summary.hasOwnProperty(productId)) {
  	summaryStoreCurrentValue.summary[productId] = true;
  	setSummaryStore(summaryStoreCurrentValue);
  }
};

export const removeProductFromWlSummaryStore = (productId) => {
  const summaryStoreCurrentValue = getSummaryStore();
  if(summaryStoreCurrentValue && summaryStoreCurrentValue.summary && summaryStoreCurrentValue.summary.hasOwnProperty(productId)) {
  	delete summaryStoreCurrentValue.summary[productId];
    setSummaryStore(summaryStoreCurrentValue);
  }
}