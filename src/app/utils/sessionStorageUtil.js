function isSessionStorageEnabled() {
  const testItem = "test";
  try {
    if (!window.sessionStorage) {
      return false;
    }
    window.sessionStorage.setItem(testItem, {});
    window.sessionStorage.removeItem(testItem);
    return true;
  } catch (e) {
    return false;
  }
}

function saveInSession(key, value) {
  if (isSessionStorageEnabled()) {
    try {
      window.sessionStorage.setItem(key, value);
    } catch (e) {}
  }
}

function getFromSession(key) {
  if (isSessionStorageEnabled()) {
    try {
      return window.sessionStorage.getItem(key);
    } catch (e) {
      return null;
    }
  }
  return null;
}

function removeFromSession(key) {
  if (isSessionStorageEnabled()) {
    try {
      window.sessionStorage.removeItem(key);
    } catch (e) {}
  }
}

export { isSessionStorageEnabled, saveInSession, getFromSession, removeFromSession };
