const Uri = require('jsuri');
import Client from '../services';
import config from '../config';
import at from 'v-at';
import Jsuri from 'jsuri';

export function getFeatures(key) {
  const features =
    (typeof window !== 'undefined' && at(window, '__myx_features__')) || {};
  return features[key];
}

export function isBrowser() {
  return typeof window !== 'undefined';
}

export function isLoggedIn() {
  if (isBrowser()) {
    return at(window, '__myx_session__.isLoggedIn') || false;
  } return false;
}

export function isMobile() {
  if (isBrowser()) {
    return isBrowser() && (at(window, '__myx_deviceType__') === 'mobile' || at(window, '__myx_deviceData__.isMobile'));
  } return false;
}

export function isApp(req) {
  if (req) {
    const deviceChannel = at(req, 'myx.deviceData.deviceChannel') || '';
    const uriObj = new Jsuri(req.originalUrl);
    const appMode = uriObj.getQueryParamValue('mode') || '';
    if (appMode) {
      return deviceChannel.toLowerCase() === 'mobile_app' || appMode.toLowerCase() === 'app';
    }
    return deviceChannel.toLowerCase() === 'mobile_app';
  }
  return false;
}

export function deviceType() {
  return window.__myx_deviceType__ || null;
}

export function isIEBrowser() {
  const ua = window.navigator.userAgent;
  const msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    return true;
  }
  const trident = ua.indexOf('Trident/');
  if (trident > 0) {
    return true;
  }
  const edge = ua.indexOf('Edge/');
  if (edge > 0) {
    return true;
  }
  return false;
}

export function isUCBrowser() {
  if (isBrowser()) {
    return window.navigator.userAgent.indexOf('UCBrowser') >= 0;
  } return false;
}

export function isSafariBrowser() {
  if (isBrowser()){
    return /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
  } return false;
}

export function isChromeBrowser() {
  if (isBrowser()) {
    return window.navigator.userAgent.indexOf('Chrome') >= 0;
  } return false;
}

export function getQueryParameter(param) {
    const url = window.location.href;
    let name = param.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

export function loadFacebookConnect() {
  if (window.fbAsyncInit) {
    return ;
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '182424375109898',
      xfbml      : true,
      version    : 'v2.11'
    });
  };
  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
}

export function fetchTopNav(key, callback) {
  Client.get(config(key)).then((res) => {
    if(res && res.body && res.body.data){
      callback(null, res.body.data);
    } else {
      callback("could not fetch navData", null);
    }
  }, (err) => {
    callback(err, null);
  });
}

export function verifyMobile(key, params, callback) {
  const path = config(key);
  Client.post(path, params).then((res) => {
    callback(null, res);
  }, (err) => {
    callback(err, null);
  });
}

export function _at(o, path, def) {
  let pointer = o, failed = false;

  if (!o || !path) {
    return o;
  }
  path = path.split('.');
  path.forEach(function(p) {
    if (pointer[p] !== null && pointer[p] !== undefined && !failed) {
      pointer = pointer[p];
    } else {
      failed = true;
    }
  });
  return failed ? (o[path] || def) : pointer;
}

export const vendorAuth = `
    <script type="text/javascript" src="https://apis.google.com/js/platform.js"></script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '182424375109898',
          xfbml      : true,
          version    : 'v2.11'
        });
      };
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
`;


export function sanitize(url) {
  if (url) {
    // undefined check needed as decodeURIComponent was sending it as string.
    // list of domains in regex. please note add $ at end each white listed domain. ( to avoid myntra.com.hacksite.com pattern)
    let whitelistedDomains = [/.*\.myntra\.com$/, /.*\.flipkart\.com$/];
    let urlObj;

    try {
      urlObj = new URL(url);
    } catch (e) {
      urlObj = new URL(url, location.origin);
    }

    for (let i = 0; i < whitelistedDomains.length; i++) {
      // if true the pass entire url
      if (whitelistedDomains[i].test(urlObj.hostname)) {
        return urlObj.toString();
      }
    }
  }
  return "/";
}


