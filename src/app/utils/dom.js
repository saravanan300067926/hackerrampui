const pageScroll = () => {
  const wx = window.pageXOffset;
  const wy = window.pageYOffset;
  const x = (wx !== undefined) ? wx : (document.documentElement || document.body.parentNode || document.body).scrollLeft;
  const y = (wy !== undefined) ? wy : (document.documentElement || document.body.parentNode || document.body).scrollTop;
  return {
    top: y,
    left: x
  };
};

const windowHeight = () => {
  return window.innerHeight || window.document.documentElement.clientHeight;
};

const offset = (el) => {
  const rect = el.getBoundingClientRect();
  const scroll = pageScroll();
  return {
    left: rect.left + scroll.left,
    right: rect.right + scroll.left,
    top: rect.top + scroll.top,
    bottom: rect.bottom + scroll.top,
    width: rect.width || el.offsetWidth,
    height: rect.height || el.offsetHeight
  };
}

export {pageScroll, windowHeight, offset}
