import at from 'v-at';

const TrackMadalytics = {
  sendEvent: function(eventType, screenType, data_set, customPayload) {
  	if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
      window.Madalytics.send(eventType, {
        type: screenType || 'web',
        'data_set': data_set || {}
      }, customPayload || {});
    }
  }
};

export default TrackMadalytics;