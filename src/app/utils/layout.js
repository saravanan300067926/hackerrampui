const serveSideRendering = require('../config').get('serverSideRenderingEnabled');
import { isUCBrowser } from './index';

const widths = ['300', '400', '500', '600', '700', '980', '1960'];
export function calculateBottomPadding(props) {
  let { ratio, width, height} = props;
  if (ratio) {
    [width, height] = ratio.split('*').map(e => Number(e));
  }
  else if (!width || !height) {
    width = height = 1;
  }
  return Math.floor((Number(height) / Number(width) * 100) * 100) / 100;
}
export function getSrcSet(src, weight = 1) {
  let image = src;
  const type = src.substr(src.lastIndexOf("."));
  if(type === '.png'){
    image = src.substring(0, src.lastIndexOf(".")) + '.jpg';
  }
  if(serveSideRendering && !isUCBrowser()){
    const defaultSrc = `${image}`.replace(
      'assets.myntassets.com',
      `assets.myntassets.com/w_${Math.floor(980 * weight)},c_limit,q_60,fl_progressive`
    );

    const srcSet = widths.map((width, index) => {
      return `${`${image}`.replace(
        'assets.myntassets.com',
        `assets.myntassets.com/w_${Math.floor(width * weight)},c_limit,q_60,fl_progressive`
      )} ${width}w ${index === widths.length -1 ? '': ','}`;
    }).join('');

    return [defaultSrc, srcSet];
  }
  else{
    // old ie window.devicePixelRatio not defined
    const width = Math.min(screen.width, 980);
    const devicePixelRatio = '2.0';
    return [`${image}`.replace(
      'assets.myntassets.com',
      `assets.myntassets.com/w_${Math.floor(width * weight)},c_limit,fl_progressive,dpr_${devicePixelRatio}`
    ), '']
  }
}
