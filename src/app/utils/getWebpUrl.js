export function getWebpUrl(imgSrc) {
  const searchKey = "assets.myntassets.com/";
  const startPos = imgSrc.indexOf(searchKey) + searchKey.length;
  if (startPos > 0) {
    return imgSrc.substr(0, startPos) + "f_webp," + imgSrc.substr(startPos);
  }
  return imgSrc;
}
