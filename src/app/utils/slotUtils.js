import kvpairs from './kvpairs';
import cookies from './cookies';
import at from 'v-at';

function inPriceRevealMode() {
  let priceRevealData = kvpairs('hrdr.pricereveal.data');
  let priceRevealMode = false;
  if (typeof priceRevealData === 'string') {
    try {
      priceRevealData = JSON.parse(priceRevealData);
      priceRevealMode = priceRevealData.enable === 'true';
    } catch (e) {
      console.log('price reveal data is not a proper json object');
    }
  } else if (typeof priceRevealData === 'object') {
    priceRevealMode = priceRevealData.enable === 'true';
  }
  return priceRevealMode;
}

function isLoggedIn() {
  return at(window, '__myx_session__.isLoggedIn');
}

function stsCookieExists() {
  return cookies.get('sts') || false;
}

function cookieExists(cname) {
  return cookies.get(cname) || false;
}

function isSlotEntryEnabled() {
  return cookieExists('stp') && !cookieExists('stb') && isLoggedIn();
}

function isShowSlotPopup() {
  return !stsCookieExists() && cookieExists('stb') && cookieExists('stp');
}

function showEarlySlotInfo() {
  return cookieExists('stb') && cookieExists('stp');
}

function getSlots(showSlots) {
  let sdata = {};
  if (showSlots) {
    let passCookie = cookies.get('stp');
    if (passCookie) {
      try {
        passCookie = decodeURIComponent(passCookie);
        passCookie = passCookie.replace(/'/g, '"');
        sdata = JSON.parse(passCookie);
      } catch (e) {
        sdata = {};
      }
    }
  }
  return sdata;
}

function getTimeRange(start, end) {
  if (!start || !end) {
    return null;
  }
  let st;
  let et;
  const sHours = start.getHours();
  let sMins = start.getMinutes();
  const eHours = end.getHours();
  let eMins = end.getMinutes();
  if (sMins < 10) {
    sMins = `0${sMins}`;
  }
  if (eMins < 10) {
    eMins = `0${eMins}`;
  }
  if (sHours > 12) {
    st = `${sHours - 12} : ${sMins} PM`;
  } else if (sHours === 0) {
    st = `12  : ${sMins} AM`;
  } else if (sHours === 12) {
    st = `12  : ${sMins} PM`;
  } else {
    st = `${sHours}  : ${sMins} AM`;
  }
  if (eHours > 12) {
    et = `${eHours - 12} : ${eMins} PM`;
  } else if (eHours === 0) {
    et = `12  : ${eMins} AM`;
  } else if (eHours === 12) {
    et = `12  : ${eMins} PM`;
  } else {
    et = `${eHours} : ${eMins} AM`;
  }
  return `${st} - ${et}`;
}

export { isShowSlotPopup, isSlotEntryEnabled, inPriceRevealMode, isLoggedIn, getSlots, getTimeRange, showEarlySlotInfo, cookieExists };
