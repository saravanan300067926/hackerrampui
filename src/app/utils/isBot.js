let botList = [
  "Googlebot",
  "Bingbot",
  "YandexBot",
  "DuckDuckBot",
  "Baiduspider",
  "translate",
  "Googlebot-Image"
];

function isBot(userAgent) {
  for (let bot of botList) {
    if (userAgent && userAgent.includes(bot)) {
      return true;
    }
  }
  return false;
}

module.exports = isBot;
