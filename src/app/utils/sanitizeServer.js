const { URL } = require('url');

export default function sanitizeServer(url) {
  if (url) {
    // undefined check needed as decodeURIComponent was sending it as string.
    // list of domains in regex. please note add $ at end each white listed domain. ( to avoid myntra.com.hacksite.com pattern)
    let whitelistedDomains = [/.*\.myntra\.com$/, /.*\.flipkart\.com$/];
    let urlObj;

    try {
      urlObj = new URL(url);
    } catch (e) {
      urlObj = new URL(url, "https://www.myntra.com");
    }

    for (let i = 0; i < whitelistedDomains.length; i++) {
      // if true the pass entire url
      if (whitelistedDomains[i].test(urlObj.hostname)) {
        return urlObj.toString();
      }
    }
  }
  return "/";
}
