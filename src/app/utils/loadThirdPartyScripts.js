import cookies from "./cookies";

function loadThirdPartyScripts() {
  // mynt-eupv -> mynt existing user page visit
  const repeatUserCookie = 'mynt-eupv';
  if (cookies.get(repeatUserCookie)) {
    window.addEventListener('load', () => {
      // izooto script
      (function (d, s, id) {
        window._izq = window._izq || [];
        window._izq.push(["init"]);
        window.izCacheVer = 1;
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://cdn.izooto.com/scripts/2d34f47ca3a13cbc90559ae77170feca968c14e4.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', "izooto-mynt"));
    });
  } else {
    cookies.set(repeatUserCookie, "1", 180);
  }
}

export default loadThirdPartyScripts;
