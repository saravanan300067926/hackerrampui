import React from 'react';
import serialize from 'serialize-javascript';
import ReactDOMServer from 'react-dom/server';
import assetify from '../../utils/assetify';
import { pump } from '../../utils/pump';
import { headerRender } from '../components/Header/server';
import { footerRender } from '../components/Footer/server';
import { createMemoryHistory, match, RouterContext } from 'react-router';
import { Provider } from 'react-redux';
import { routes } from '../routes';
import { renderMeta } from './meta';
import isNil from 'lodash/isNil';
import head from 'lodash/head';
import at from 'v-at';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import { getRouteConfig, get as getConfig } from '../config';
import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { rootReducer } from '../components/Search/reducers';
import { getSortFilter } from '../components/Search/helpers';
import { renderSchema } from './schema';
import { vendorAuth, isApp } from './index';

const serveSideRendering = require('../config').get('serverSideRenderingEnabled');
const instrumentEnv = getRouteConfig('instrumentation');
const isBot = require('./isBot');

let assets = {};

try {
  assets = require('../../../webpack-assets.json');
} catch (e) {
  console.log(e);
}

let getPreloadTags = pageType => {
  let preloadTags = "";
  if (process.env.NODE_ENV === "production" && !isEmpty(assets)) {
    preloadTags = `
      <link rel="preload" as="script" href="${get(assets, "main.js")}">
      <link rel="preload" as="script" href="${get(assets, "vendor.js")}">
    `;

    if (assets && assets[`d-${pageType}`]) {
      const pageSpecificPreload = `
        <link rel="preload" as="script" href="${get(
        assets,
        `d-${pageType}.js`
      )}">
        <link rel="preload" as="style" href="${get(
        assets,
        `d-${pageType}.css`
      )}">
      `;
      preloadTags = preloadTags + pageSpecificPreload;
    }
  }
  return preloadTags;
};

function getvendorAuthScripts(pageType) {
  if (pageType === "login" || pageType === "register") {
    return vendorAuth;
  }
  return "";
}

function getIzootoScripts() {
  return (
    `
      <script> window._izq = window._izq || []; window._izq.push(["init"]); </script>
      <script src="https://cdn.izooto.com/scripts/2d34f47ca3a13cbc90559ae77170feca968c14e4.js"></script>
    `
  );
}

function renderMyPage(res, {
    ampLinks,
    reactOutput,
    props,
    title,
    myx,
    header,
    footer,
    outline ,
    raw ,
    seo,
    queryParams,
    searchProducts,
    isMobile,
    topnav,
    req,
    initialState,
    pageType
  }) {
  const data = {};
  const protocol = getProtocol(req);
  const fontStyle = getFontStyles(protocol);
  data.title = title || 'Myntra';
  data.ampLinks = ampLinks;
  data.metaTags =  renderMeta(seo, queryParams, searchProducts);
  data.fontStyle = fontStyle;
  data.schema = renderSchema(initialState);
  data.header = header;
  data.props = props;
  outline.pump('pageType', pageType || '');
  data.pumpData = outline.pump();
  data.reactOutput = reactOutput;
  data.footer = footer;
  data.raw = raw;
  data.seo = seo;
  data.topnav = topnav;
  data.instrumentEnv = instrumentEnv;
  data.manifestText = at(assets, 'manifest.text') || '';
  data.vendorAuthScripts = getvendorAuthScripts(pageType);
  data.izootoAuthScripts = '';
  if (!reactOutput && !isApp(req)) {
    data.reactOutput = '<div class="loader-container"><div class="spinner-spinner"></div></div>';
  }

  data.preloadTags = '';
  if (pageType) {
    data.preloadTags = getPreloadTags(pageType);
  }
  res.render("shell", {data});
}

export function getProtocol(req) {
  const hdr = req.get('http_x_forwarded_proto') || req.get('x-forwarded-proto');
  return hdr;
}

function dehydrateStateForBrowser(props) {
  if (!isNil(props)) {
    return props[head(Object.keys(props))];
  }
  return {};
}

// Removed the amphtml from PDP and Home.
function getAmpLink(req){
  if(req && req.pageType === 'search') {
    return`<link rel="amphtml" href="${'https://'+ req.get('host') + '/amp' +req.path}">`;
  }
  else
    return '';
}


function createPrefetchLinks(assetsArray, protocol) {
  let links = '';
  for (const asset of assetsArray) {
    links += `<link rel="prefetch" href="${assetify(asset, protocol)}"/>`;
  }
  return links;
}

function getFontStyles(protocol) {
  let prefix = 'https://constant.myntassets.com';
  if (protocol !== 'https') {
    prefix = 'http://constant.myntassets.com';
  }

  return `<style>
    @font-face {
    font-family: 'Whitney';
    src: url('${prefix}/www/fonts/WhitneyHTF-Book.eot');
    src: url('${prefix}/www/fonts/WhitneyHTF-Book.eot?#iefix') format('embedded-opentype'),
      url('${prefix}/www/fonts/WhitneyHTF-Book.woff') format('woff'),
      url('${prefix}/www/fonts/WhitneyHTF-Book.ttf') format('truetype');
    font-weight: 400;
    font-display: swap;
    font-style: normal;
  }

    @font-face {
    font-family: 'Whitney';
    src: url('${prefix}/www/fonts/WhitneyHTF-SemiBold.eot');
    src: url('${prefix}/www/fonts/WhitneyHTF-SemiBold.eot?#iefix') format('embedded-opentype'),
      url('${prefix}/www/fonts/WhitneyHTF-SemiBold.woff') format('woff'),
      url('${prefix}/www/fonts/WhitneyHTF-SemiBold.ttf') format('truetype');
    font-weight: 500;
    font-display: swap;
    font-style: normal;
  }
  </style>`;
}

function stringifyHeaders(headers) {
  return Object.entries(headers).map(entry => entry.join('=')).join('___');
}

function renderForBot(req, res, webrenderApi) {
  const http = require('http');
  const modurl = require('url');

  const reqUrl = `${req.protocol}://${req.get("host")}${req.url}`;
  const url = `${webrenderApi}/${reqUrl}`;
  const options = modurl.parse(url);
  options.headers = Object.assign({}, req.headers, {host: options.host, 'x-bot-request': 'yes'});
  delete options.headers.cookie;

  http.get(options, response => {
    console.info("[BOT]", response.statusCode, options.href);
    res.status(response.statusCode);
    res.type('html');

    response.on('data', chunk => res.write(chunk));
    response.on('end', () => res.end());
    response.on('error', e => console.error("[BOT]", options.href, e.message));
  });
}

async function render(req, res, props, title, outline, pageType) {
  let isWebrender = req.get("webrender") === "true";
  let userAgent = req.get("user-agent");

  if (false && isBot(userAgent) && !isWebrender && userAgent !== "HAProxyHealthChecker") {
    const webrenderApi = getConfig('webrender').root;
    return renderForBot(req, res, webrenderApi);
  }

  if(props && props.searchData && props.searchData.results) {
    props.searchData.results.products = at(props, 'searchData.results.products') || [];
    at(props, 'searchData.results.products').forEach(function(item){
      item.landingPageUrl = (item.landingPageUrl || '').toLowerCase();
    });
  }

  const memoryHistory = createMemoryHistory(req.url);
  const reduxRouterMiddleware = routerMiddleware(memoryHistory);
  const initialState = {
    ...dehydrateStateForBrowser(props),
    sort: getSortFilter(),
    apiFetchInProgress: false,
    page: 1,
    props,
    baseUrl: req.url,
    isMobile: req.isMobile,
    isLoggedIn: at(req, 'myx.session.isLoggedIn')
  };

  const store = createStore(
    rootReducer,
    { search: initialState },
    applyMiddleware(reduxRouterMiddleware)
  );

  if (!req.headers['user-agent']) {
    console.error('User agent not found in this ****');
    console.error(req.url);
  }

  const isUCBrowser = req.headers['user-agent'] && req.headers['user-agent'].indexOf('UCBrowser') >= 0;

  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {

    renderProps.location.dehydratedState = initialState;
    const reactOutput = (serveSideRendering && !isUCBrowser) ? ReactDOMServer.renderToString(
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    ) : '';

    const header = new Promise((resolve, reject) => {
      headerRender(req, (data) => {
        if (data) {
          resolve(data);
        }
        reject('Invalid Header HTML');
      });
    });

    header.then((data) => { // data : { navData: data, header: headerStr }
      const navData = at(data, 'navData.children');
      if (navData) {
        const groups = navData.map((val) => ({ name: at(val, 'props.title'), linkUrl: at(val, 'props.url') }));
        footerRender(req, groups, (footerHtml) => {
          data.footer = footerHtml; // data : { navData: data, header: headerStr, footer : footerHtml }
          return (data);
        });
      } else {
        footerRender(req, null, (footerHtml) => {
          data.footer = footerHtml; // data : { navData: data, header: headerStr, footer : footerHtml }
          return (data);
        });
      }
    }, (reason) => {
      console.error('Reason ::::::::::::', reason);
    }).catch((err) => {
      console.error('ERROR  : ', err);
    });
    header.then((completeObj) => {
      renderMyPage(res,{
        ampLinks: getAmpLink(req),
        reactOutput,
        props: `window.__myx = ${serialize(props || {}, { isJSON: true})}`,
        title: title,
        myx: req.myx || {},
        header: completeObj.header,
        footer: completeObj.footer,
        outline: outline,
        raw: pump(req),
        seo: req.seo || {},
        queryParams: req.query,
        searchProducts : initialState.results,
        isMobile: req.isMobile,
        topnav: completeObj.navData,
        req:req,
        initialState: initialState,
        pageType: pageType
      });
    }).catch((err) => {
      console.error('ERROR HEADER : ', err);
    });
  });
}

export default render;
