import get from 'lodash/get';

function getPdpUrlChangeRegx(obj) {
  const categoryList = get(obj, "pdpurlchange.categories", []);
  if (!categoryList.length) return;
  return RegExp(categoryList.join("|"), "i");
}

module.exports = getPdpUrlChangeRegx;
