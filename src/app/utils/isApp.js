import Jsuri from "jsuri";
import get from 'lodash/get';
export function isBrowser() {
  return typeof window !== 'undefined';
}

export function isApp() {
  if (isBrowser()) {
    const deviceChannel = get(window, '__myx_deviceData__.deviceChannel') || '';
    const uriObj = new Jsuri(window.location.href);
    const appMode = uriObj.getQueryParamValue('mode') || '';
    if (appMode) {
      return deviceChannel.toLowerCase() === 'mobile_app' || appMode.toLowerCase() === 'app';
    }
    return deviceChannel.toLowerCase() === 'mobile_app';
  } return false;
}

export function isIosApp() {
  if (isBrowser()) {
    const userAgent = get(window, 'navigator.userAgent');
    return userAgent.includes('MyntraRetailiPhone');
  } return false;
}
