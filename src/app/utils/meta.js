import at from 'v-at';
const Jsuri = require("jsuri");
const { isNewPDPUrl } = require("../../routes/seo/newPdpSeoSupport");

function renderSeo(seo, query, searchProducts) {
  let robotsTagContent = seo.robotsTagContent || [],
    uri = seo && seo.canonicalUri || '';

  if((uri.match(/-/g) || []).length > 7 && uri.indexOf("/buy") === -1
    && !isNewPDPUrl(uri) && !robotsTagContent.includes("noindex")
  ) {
    robotsTagContent = ["noindex"];
  }

  let canonicalTags = '';
  let canonicalUri = seo.canonicalUri || 'https://www.myntra.com/';
  if (seo.canonicalUri && query.p && seo.canonicalUri.indexOf("/buy") === -1) {
    canonicalUri = new Jsuri(canonicalUri).addQueryParam('p', query.p).toString();
  }

  if (!at(seo, 'selfCanonical') && !robotsTagContent.includes("noindex")) {
    canonicalTags = `
    <link rel="canonical" href="${canonicalUri}" />`;
  }

  let relPreviousTag = "", prevUri,
    relNextTag = "", nextUri;

  if (searchProducts && searchProducts.totalCount) {
    const totalPages = searchProducts && searchProducts.totalCount ? Math.ceil(searchProducts.totalCount / 50) : '';
    // to check weather pageNumb is available or not using following check
    const urlObj = new Jsuri(canonicalUri);
    const p = urlObj.getQueryParamValue('p');
    const pageNumber = p ? parseInt(p) : 1;

    if (pageNumber > 1) {   // show prev pages
      prevUri = pageNumber === 2 ? urlObj.deleteQueryParam('p') : urlObj.replaceQueryParam('p', pageNumber - 1);
      relPreviousTag = `<link rel="prev" href="${prevUri}" />`;
    }

    if (pageNumber < totalPages) {    // only next pages
      nextUri = urlObj.replaceQueryParam('p', pageNumber + 1);
      relNextTag = `<link rel="next" href="${nextUri}" />`;
    }
  }

  let webShouldFallback = true;
  if (at(seo, 'webShouldFallback') === "false") {
    webShouldFallback = false;
  }

  return `
  <meta itemprop="image"  content="${at(seo, 'metaData.image') || seo.image || seo.socialImage || ''}" />

  <meta name="keywords" content="${seo.keywords || at(seo, 'metaData.keywords') || ''}" />
  <meta name="description" content="${seo.description || at(seo, 'metaData.description') || ''}" />

  <meta name="google-play-app" content="app-id=com.myntra.android" />

  ${canonicalTags}
  ${prevUri ? relPreviousTag : ""}
  ${nextUri ? relNextTag : ""}

  <meta property="fb:admins" content="${seo.fbAdmins || seo.gPublisher || ''}" />
  <meta property="fb:app_id" content="${seo.fbAppId || ''}" />
  <link rel="android-touch-icon" href="https://constant.myntassets.com/web/assets/img/icon.5d108c858a0db793700f0be5d3ad1e120a01a500.png"/>

  <meta name="twitter:card" content="${seo.summary || at(seo, 'metaData.summary') || ''}" />
  <meta name="twitter:site" content="@myntra" />
  <meta name="twitter:title" content="${seo.twitter_title || at(seo, 'metaData.twitter_title') || ''}" />
  <meta name="twitter:description" content="${seo.twitter_description || at(seo, 'metaData.twitter_description') || ''}" />
  <meta name="twitter:image" content="${at(seo, 'metaData.image') || seo.image || seo.socialImage || ''}"/>

  <meta name="twitter:app:country" content="IN" />
  <meta name="twitter:app:name:iphone" content="Myntra" />
  <meta name="twitter:app:id:iphone" content="907394059" />
  <meta name="twitter:app:url:iphone" content="${seo.og_url || at(seo, 'metaData.og_url') || ''}" />
  <meta name="twitter:app:name:googleplay" content="Myntra" />
  <meta name="twitter:app:id:googleplay" content="com.myntra.android" />
  <meta name="twitter:app:url:googleplay" content="${seo.og_url || at(seo, 'metaData.og_url') || ''}" />

  <meta property="og:title" content="${seo.og_title || at(seo, 'metaData.og_title') || ''}" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="${seo.og_url || at(seo, 'metaData.og_url') || ''}" />
  <meta property="og:image" content="${at(seo, 'metaData.image') || seo.image || seo.socialImage || ''}" />
  <meta property="og:image:width" content="200" />
  <meta property="og:image:height" content="200" />
  <meta property="og:description" content="${seo.og_description || at(seo, 'metaData.og_description') || ''}" />
  <meta property="og:site_name" content="Myntra" />
  <meta name='google-signin-clientid' content='787245060481-4padi0c9g2l1pnbie52fmpfdpo43209i.apps.googleusercontent.com' />
  <meta name='google-signin-cookiepolicy' content='single_host_origin' />
  <meta name='google-signin-scope' content='https://www.googleapis.com/auth/plus.me email' />

  <meta property="al:ios:url" content="${seo.og_url || at(seo, 'metaData.og_url') || ''}" />
  <meta property="al:ios:app_store_id" content="907394059" />
  <meta property="al:ios:app_name" content="Myntra Fashion Shopping App" />
  <meta property="al:android:url" content="${seo.og_url || at(seo, 'metaData.og_url') || ''}"  />
  <meta property="al:android:package" content="com.myntra.android" />
  <meta property="al:android:app_name" content="Myntra Fashion Shopping App" />
  <meta property="al:web:should_fallback" content="${webShouldFallback}">
  <meta name='apple-itunes-app' content="app-id=907394059, app-argument=${seo.og_url || at(seo, 'metaData.og_url') || ''}" />
  ${robotsTagContent.length ? `<meta name="robots" content="${robotsTagContent.join(",")}">` : ''}
  `;
}

export function renderMeta(seo, queryParmas, searchProducts) {
  const seoMeta = renderSeo(seo, queryParmas, searchProducts);
  return `
    <link rel="icon" type="image/png"
      href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAADqUExURQAAANobf5cWJP2ALPQdnPMeju8djq0dOL0jPpgbJft7LJckF88acPdmJfUdm9ZNJaAkJZ4gI60YR/IfivIel/Qel/IfjPhvMPpyKrU+HqIXOO8ek7lEH9FaI6ApGrgZUOkdhvEfivlxMrsZWPJ4KtgbedEcaMoaabUZTfVPF/Iek/p4LN9lJvldU5wXHLxIH8EaXNhRMfQcsv+RLvNVEfwdrPQbsfcctP+ULv+OLvYcruoaPfyEKvsepf+JLfUcpfA7O/t5J/ZhGOwcSLwcKu0cYPdQE+xzKe0cnPAbg/hrLchRIfhQTvRUNo6Ap0EAAAAydFJOUwD9MPT+WIUQBEi7HftE6haT0bkc2sE5Zipt6Ofc+rFybDCA+uG8Psaf27WX5uf7/Y2phayV3gAAAspJREFUWMPtlllzokAUhVExkogLrnGPmn2STFXTbA2ikLhk+/9/Z3o1iGh8nuI+we3uj3PORUpJSiuttP7nKmSVhG62cOr5c+1lWIw3K8MX7fzE8923tzctRqhouNk9iZDtqq/uYqDtCFaGnwv3Ve1mT/CvZRAG2INhtFv91BeuO89ov+dQVa05ATifnYirv46NFcwttfprAFNkUQW619/GUOx7NgOg6S8xFLobC5hL99vW7fVQDLO6xrff7isC1qZ73EQ1YwEAKEDXB/xp5wN8gwFLvGRljprIThHZtHQ/CMDv06cpfY8APtwlgaPpsUnUNmSP9e4GBKCvaY6dNbm2A/edLm5qRxJUyRYAZNfXaQ1wjoW+Q699BgCWejBHpTan58F8wc7rHjbcCfjNQqzWlEMCMkwAML9sfuipWHjil/aXyVatzCEJQoCJnvkh3e90PAF4RkBIODACIQCV274gXF9vWZMyEhKSB3HPBQCjWck5/LFBwN04ucumISTcJ50vqiIB40y64AA9DPmFcyGdGSIFtZgAGMmcb8otSXjwIfSYBGcitWQOAPIoYYZ3QiBqKNIl82AHENKXUne8kqQ0RIzGnXI4QuJAEh5CCEOHOcBHfjwkxDia8UUkj/Et8+BByD34E9wcy0LCbN/DjbGdAZFH50AcQEjmgGdAbG7nYNzsfXR7HG7O8rTxhwBCAgjZDEjltzJ78c/CrUgYNdgSidGhAOjQCOn3RsRoyrcxwBVnm7Mr3sEx+vQ89FiEydtEnXF3RkNou8yxDAmAC4hIoKNKAIgEWAocEPAEoikkAEz6Djz+vCGVawGgI+Dv2yPfGAdQsmn06pFeKccB7Uiz3iOEqFLeLxsIGeXWTrMdriBchZOdZovu3HkSk1Ceyc1xrFl6WK0e2rHmuCnPyvn9X1M939r/hRRKpcr+766Vr6d/otJKK629+gcFK7Wn0A9e6AAAAABJRU5ErkJggg=="
      sizes="64x64"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
    <meta name="google-signin-client_id" content="787245060481-4padi0c9g2l1pnbie52fmpfdpo43209i.apps.googleusercontent.com"/>
    <meta name="google-signin-cookiepolicy" content="single_host_origin"/>
    <meta name="google-signin-scope" content="profile email"/>
    <meta name="google-site-verification" content="-eak5o6MaC8dcrUhendmbMxTX-Q7FRNdoXiHJpGY1es" />
    <meta name="msvalidate.01" content="B39BEC92EDF5329220F6E707D7950DB8" />
    ${seoMeta}
  `;
};
