import at from 'v-at';
import lscache from 'lscache';
import cookies from './cookies';
import { isBrowser } from './index';
import Client from '../services';
import bus from 'bus';
import lscachify from './lscachify';
import { saveInSession, getFromSession, removeFromSession } from './sessionStorageUtil.js'

export const CACHE_KEY = 'beacon:user-data';

export const updateGlobals = (beaconData) => {
  if (isBrowser()) {
    Object.keys(beaconData).forEach((keys) => {
      window[`__myx_${keys}__`] = beaconData[keys];
    });
  }
};

export const clearCache = () => {
  lscache.remove(CACHE_KEY);
  lscache.remove(`${CACHE_KEY}-cacheexpiration`);
};

const wrapper = lscachify((dummy, callback) => {
  Client.post(`/beacon/user-data${window.location.search}`).then((res) => {
    callback(null, res);
  }, (error) => {
    console.log('Could not fetch user-data', error);
    cookies.del('bc');
    callback(error, null);
  });
}, 'beacon', 5);

const sessionTrack = (data) => {
  const isLoggedIn = data.session.isLoggedIn;
  const user = data.session.login;
  isLoggedIn ? loggedInSession(user) : loggedOutSession();
};

const loggedInSession = (user) => {
  removeFromSession('loggedOutUserEvent');
  const isEventTriggered = getFromSession('loggedInUser') === user ? true : false;
  if (!isEventTriggered) {
    window.ga('send', 'event', 'logged_in_session', 'true');
    saveInSession('loggedInUser', user);
  }
}

const loggedOutSession = () => {
  removeFromSession('loggedInUser');
  const isEventTriggered = getFromSession('loggedOutUserEvent');
  if (isEventTriggered !== 'true') {
    window.ga('send', 'event', 'logged_out_session', 'true');
    saveInSession('loggedOutUserEvent', 'true');
  }
}

export const beacon = (callback) => {
  const pageUrl = location.href || '';

  if (isBrowser()) {
    if (window.localStorage) {
      const expiry = window.localStorage.getItem('lscache-beacon:user-data-cacheexpiration');
      if (!expiry || cookies.get('bc')) {
        window.localStorage.removeItem('lscache-beacon:user-data-cacheexpiration');
        window.localStorage.removeItem('lscache-beacon:user-data');
      }
    }

    wrapper('user-data', (err, res) => {
      if (!err) {
        const beaconData = at(res, 'body');
        if (Object.keys(beaconData)) {
          updateGlobals(beaconData);
          bus.emit('beacon-data', res.body);
          try {
            sessionTrack(beaconData);
          } catch (e) {
          }
        }
        if (callback) {
          callback(err, beaconData);
        }
      }
    });
  }
};
