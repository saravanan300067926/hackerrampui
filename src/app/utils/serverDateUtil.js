import at from 'v-at';
import { isLocalStorageEnabled, setItem, getItem }  from './localStorageUtil';


function getDateOffset() {
  if (isLocalStorageEnabled()) {
    const storedOffset = getItem('__myx__dateOffset');
    if (storedOffset !== null && typeof storedOffset !== undefined) {
      const intOffset = parseInt(storedOffset, 10);
      return isNaN(intOffset) ? 0 : intOffset;
    } else {
      const serverTimeMs = at(window, '__myx_server__date__.dateInMs');
      if (serverTimeMs) {
        const clientCurrentTimeMs = new Date().getTime();
        let offset = serverTimeMs - clientCurrentTimeMs;
        if (offset >= -10000 && offset <= 10000) {
          offset = 0;
        } else {
          offset = offset < 0 ? (offset + 10000) : ( offset > 0 ? (offset - 10000) : offset);
        }
        setItem('__myx__dateOffset', offset);
        return offset;
      }
    }
  }
  return 0;
}

function syncWithServerDate() {
  getDateOffset();
}

export { getDateOffset, syncWithServerDate };
