import get from "lodash/get";
import cookies from "./cookies";
import { isLoggedIn } from '../utils';
import Client from '../services';
import config from '../config';

const isNewUserPromise = () => {
  if (!isLoggedIn()) {
    return Promise.resolve(false);
  }

  const val = cookies.get("ftc");
  if (val !== null && val !== undefined) {
    const ftc = val === "true";
    console.log(`From ftc cookie, ${ftc}, ${cookies}`);
    return Promise.resolve(ftc);
  } else {
    return new Promise((resolve, reject) => {
      Client.get(`${config('isNewUser')}`).end((err, res) => {
        const isNew = get(res, "body.isNew");
        if (err || isNew === undefined) {
          reject(err);
        } else {
          cookies.set("ftc", isNew, 1);
          console.log(`From API Response, ${isNew}, ${res.body}`);
          resolve(isNew);
        }
      });
    });
  }
};

export default isNewUserPromise;
