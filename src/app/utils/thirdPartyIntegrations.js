
// Module for different 3rd party script integration on beacon-user data load
import bus from 'bus';
import { setItem } from '../utils/localStorageUtil';

// Metail: virtual tryon script loader
function loadMetailScript() {
  const s1 = document.createElement('script');
  s1.setAttribute('src', 'https://mondo.me-tail.net/js/mondo.js');
  const s2 = document.createElement('script');
  s2.id = 'metail-mondo';
  s2['data-retailer-id'] = '515ffe15';
  s2.setAttribute('src', 'https://integrations.me-tail.net/myntra/ui/integration.js');
  document.head.appendChild(s1);
  document.head.appendChild(s2);
}

function setIsTrueFit(value){
  setItem('isTruefit', value);
}

// TrueFit: Size and fit vendor script loader
function loadTrueFitScript() {
  setIsTrueFit('true');
  const scriptLoadInterval = setInterval(() => {
    if(document.querySelector('.tfc-fitrec-product')) {
      clearInterval(scriptLoadInterval);
      (function(){
        const a={};
        function g(l){
            a[l]=function(r,e,o){
                const w=window,d=document,p=[];
                let t,s,x;
                w.tfcapi=t=w.tfcapi||function(){t.q=t.q||[];t.q.push(arguments);};
                o&&o.autoCalculate===false&&p.push('autoCalculate=false');
                x=d.getElementsByTagName('script')[0];
                s=d.createElement('script');
                s.type='text/javascript';
                s.async=true;
                s.src='https://'+r+'-cdn'+(e==='dev'||e==='staging'?'.'+e:'')+'.truefitcorp.com/fitrec/'+r+'/js/'+l+'.js?'+p.join('&');
                x.parentNode.insertBefore(s,x);
            }
        }
        g('fitrec');g('tracker');
        return a;
      })().fitrec('myn', 'prod');
    }
  }, 50);
}

// Pixibo: Size and fit vendor script loader
function loadPixiboScript() {
  setIsTrueFit('false');
  var s1 = document.createElement('script');
  s1.setAttribute('src', 'https://cdn.pixibo.com/clients/myntra/p/pixibo.default.min.js');
  document.head.appendChild(s1);
}

// FitAnalytics: Size and fit vendor script loader
function loadFitAnalyticsScript() {
  setIsTrueFit('false');
  var s1 = document.createElement('script');
  s1.setAttribute('src', 'https://integrations.fitanalytics.com/shop/myntra/pdp.js');
  document.head.appendChild(s1);
}

let is3pInitialized = false;
// Initialize script loading based on abtest values
function initABTestConfigs(abTests) {
  if (!abTests){
    setIsTrueFit('false');
    return;
  }

  // Load metail script
  const isMetailRequired = abTests.metail === 'metail-on';
  if (isMetailRequired && (__myx.searchData || (__myx.pageName && __myx.pageName.toLowerCase() === 'pdp'))) {
    loadMetailScript();
  }

  if (!__myx.pageName || __myx.pageName.toLowerCase() !== 'pdp') {
    return;
  }

  if (is3pInitialized) {
    return;
  }

  // Load size and fit vendor scripts
  const sizeAndFitVendor = abTests.sizefit || 'MFA';
  console.log('Size and fit initialized, recommendation engine ' + sizeAndFitVendor);
  switch(sizeAndFitVendor.toLowerCase()) {
    case 'truefit':
      loadTrueFitScript();
      break;
    case 'pixibo':
      loadPixiboScript();
      break;
    case 'fitanalytics':
      loadFitAnalyticsScript();
      break;
    default:
    setIsTrueFit('false');
  }
  is3pInitialized = true;
}

// Initialize abtests received in beacon call
bus.on('beacon-data', data => {
  const abtests = data.mxab;
  initABTestConfigs(abtests);
});
