import join from 'lodash/join';
import replace from 'lodash/replace';
import getConstants from '../constants';

function isValidMobileNumber(mob){
  const number = parseInt(mob, 10);
  return (number && number > 1000000000 && number < 10000000000);
}
const Validations = {
  isValidEmail(emailId) {
    const expression =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return expression.test(emailId);
  },

  isValidPassword(password) {
    let errorMessage = "Please enter at least ", errors = [], errorCodes = [];
    const passwordLength = getConstants('passwordLength');
    const passwordSpecialCharsEnabled = getConstants('passwordSpecialCharsEnabled');
    const passwordUpperCaseEnabled = getConstants('passwordUpperCaseEnabled');
    const passwordnumberEnabled = getConstants('passwordnumberEnabled');
    if (typeof password === "string") {
      if (password.length < passwordLength) {
        errors.push(`${passwordLength} characters`);
        errorCodes.push("password_length");
      }
      if (passwordSpecialCharsEnabled && !/[\~\!\@\#\$\%\^\&\*\(\)\_\+\-\?\<\>]/.test(password)) {
        errors.push("1 of these special characters ~!@#$%^&*()_+-?<>");
        errorCodes.push("special character");
      }
      if (passwordUpperCaseEnabled && !/[A-Z]/.test(password)) {
        errors.push("1 uppercase character");
        errorCodes.push("uppercase");
      }
      if (passwordnumberEnabled && !/\d/.test(password)) {
        errors.push("1 number");
        errorCodes.push("number");
      }
    }
    return {
      error: errors.length > 0,
      errorCodes: errorCodes,
      errorMessage: errors.length > 0 ? join([...errorMessage, ...replace(join(errors, ","), /,([^,]+)$/, " and $1")], "") : ""
    };
  },

  isValidMobile(mob) {
    return (mob==="" || isValidMobileNumber(mob));
  }
};




export default Validations;
