import React, { PropTypes } from 'react';
import styles from './asyncComponentPlaceholder.css';

const asyncComponentPlaceholder = () => (
  <div className={styles.base}>
    <div className={styles.spinner}></div>
  </div>
);

asyncComponentPlaceholder.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  pastDelay: PropTypes.bool.isRequired
};

export default asyncComponentPlaceholder;
