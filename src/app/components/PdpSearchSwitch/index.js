import React, { PropTypes } from 'react';
// import Pdp from '../Pdp';
// import Search from '../Search';
import isFinite from 'lodash/isFinite';
import loadAsync from '../../routes/loadAsync';

const Pdp = loadAsync('pdp');
const Search = loadAsync('search');

// Remove this component once react-router gets regex matching
const PdpSearchSwitch = props => {
  const query = Number(props.params.id);
  if (isFinite(query)) {
    return <Pdp {...props} />;
  }
  return <Search />;
};

PdpSearchSwitch.propTypes = {
  params: PropTypes.object.isRequired,
  location: PropTypes.object
};

export default PdpSearchSwitch;
