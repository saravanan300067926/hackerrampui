import React from 'react';
import get from 'lodash/get';
import styles from './sitemap.css';

const Sitemap = () => {
  const sitemapLinks = get(window, '__myx_sitemap__.links', []);
  return (
    <div className={styles.sitemapContainer}>
      <h2 className={styles.heading}>Sitemap</h2>
      {sitemapLinks && sitemapLinks.length
        ? sitemapLinks.map(siteMapLink => (
          <div className={styles.sitemapLinks}>
            <a className={styles.sectionLink} href={siteMapLink.link}>
              {siteMapLink.name}
            </a>
            <div className={styles.sitemapLinksSection}>
              {siteMapLink.subLinks && siteMapLink.subLinks.length > 0
                ? siteMapLink.subLinks.map((siteMapSubLink, index) => (
                  <div className={styles.subSectionLinkContainer}>
                    <a className={styles.subSectionLink} href={siteMapSubLink.link}>
                      {siteMapSubLink.name}
                    </a>
                    {index >= siteMapLink.subLinks.length - 1
                      ? null
                      : <div className={styles.subSectionLinkDivider} />
                    }
                  </div>
                ))
                : null
              }
            </div>
          </div>
        ))
        : <div>No links found!</div>
      }
    </div>
  );
};

export default Sitemap;
