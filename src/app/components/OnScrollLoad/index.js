import React from 'react';

export default class OnDemand extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      renderComponent: false
    };
    this.onScrollAction = this.onScrollAction.bind(this);
    this.removeScrollListner = this.removeScrollListner.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScrollAction);
  }

  onScrollAction() {
    this.setState({
      renderComponent: true
    });
    this.removeScrollListner();
  }

  removeScrollListner() {
    window.removeEventListener('scroll', this.onScrollAction);
  }

  render() {
    const { Component, ...rest } = this.props;
    if (!this.state.renderComponent) {
      return <div>Loading...</div>;
    }
    return <Component {...rest} />;
  }
}
