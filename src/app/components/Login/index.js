import React from 'react';
import { clearCache } from '../../utils/beacon';
import cookies from '../../utils/cookies';
import styles from './login.css';
import { isBrowser, getQueryParameter, sanitize, loadFacebookConnect, getFeatures } from '../../utils';
import Loader from '../Loader';
import Track from '../../utils/track';
import at from 'v-at';
import Validations from '../../utils/validations';
import Notify from '../Notify';
import { Link } from 'react-router';
import config from '../../config';
import Client from '../../services';
import get from 'lodash/get';

import { securify } from '../../utils/securify';
const secure = securify();
import trim from 'lodash/trim';

const captchaEnabled = getFeatures('showLoginCaptcha') === 'true';

const DEFAULT_ERROR_MESSAGE = 'Oops! Something went wrong. Please try again in some time';

export function getIdToken(googleUser) {
  const authResponse = googleUser && googleUser.getAuthResponse();
  return get(authResponse, 'id_token', '');
}

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      captchaURL: captchaEnabled ? this.getCaptchUrl() : null,
      isFocussed: false,
      showEmailError: false,
      showPasswordError: false,
      captchaError: null,
      userToken: ''
    };

    this.login = this.login.bind(this);
    this.facebookLogin = this.facebookLogin.bind(this);
    this.gPlusLoginSuccess = this.gPlusLoginSuccess.bind(this);
    this.gPlusLoginFailed = this.gPlusLoginFailed.bind(this);
    this.setFocus = this.setFocus.bind(this);
    this.setBlur = this.setBlur.bind(this);
    this.changeCaptcha = this.changeCaptcha.bind(this);
  }

  componentDidMount() {
    loadFacebookConnect();
    this.setState({ userToken: at(window, '__myx_session__.USER_TOKEN') });
    if (window) {
      window.scrollTo(0, 0);
    }
    cookies.set('bc', 'true');
    // set a cookie for admission control
    if (at(window, '__myx_session__.login') && cookies.get('user_uuid') !== at(window, '__myx_session__.login')) {
      cookies.set('user_uuid', at(window, '__myx_session__.login'), 180);
    }
    if (getQueryParameter('force')) {
      this.refs.notify.error('Sorry! Just as a security precaution, can you please login again?');
      setTimeout(() => this.refs.notify.hide(), 3000);
    }
    this.registerGoogleLogin();
    Track.screen('/login');
  }

  onLoginAction(response, key) {
    if (at(response, 'body.httpCode') === 400) {
      const errorMessage = at(response, 'body.message') || DEFAULT_ERROR_MESSAGE;
      this.refs.notify.error(errorMessage);
      Track.event('login', key, errorMessage);
    } else {
      clearCache();
      window.ga('send', 'event', 'logged_in_session', 'true');
      this.navigateToIndex();
      Track.event('login', key, 'success');
    }
  }

  getCaptchUrl() {
    const randomNumber = Math.floor(Math.random() * 10000000000);
    return `https://www.myntra.com/captcha/captcha.php?id=loginPageCaptcha&rand=${randomNumber}`;
  }

  setFocus() {
    this.setState({
      isFocussed: true
    });
  }

  setBlur() {
    this.setState({
      isFocussed: false
    });
  }

  getReferer() {
    return sanitize(get(this.props, 'location.query.referer') || (`${at(window, 'location.protocol')}//${at(window, 'location.host')}`));
  }

  getCaptcha() {
    if (this.state.captchaURL) {
      const captcha = `${this.state.captchaURL}`;
      return (
        <div>
          <img className={styles.captcha} src={secure(captcha)} />
          <div className={styles.changeCaptcha} onClick={this.changeCaptcha}>
            <img src="https://constant.myntassets.com/web/assets/img/4f8acb92-0001-433f-a8b6-27ac954ef94e1546949499874-refresh_icon.png" />
            Change Text
          </div>
          <fieldset className={`${styles['input-container']} ${styles.captchaInputCtn}`}>
            <input
              ref={"captchaInput"}
              className={styles['user-input-captcha']}
              name="captcha"
              type="text"
              autoComplete="off"
              maxLength="15"
              placeholder="Please enter the text shown above" />
          </fieldset>
          {this.state.captchaError && (
            <div className={styles.captchaError}>
              {this.state.captchaError}
            </div>
          )}
        </div>
      );
    }
    return null;
  }

  getRegisterPath() {
    let referer = new URLSearchParams(window.location.search);
    referer = sanitize(referer.get('referer'));
    let path = '/register';
    if (referer) {
      path = `${path}?referer=${referer}`;
    }
    return path;
  }

  getForgotPath() {
    // const referer = sanitize(at(this.props, 'location.query.referer'));
    let referer = new URLSearchParams(window.location.search);
    referer = sanitize(referer.get('referer'));

    let path = '/forgot';
    if (referer) {
      path = `${path}?referer=${referer}`;
    }
    return path;
  }

  getEmailError() {
    if (this.state.showEmailError) {
      return (
        <div>
          <span className={styles['error-icon']}>!</span>
          <p className={styles['error-message']}>Please Enter a Valid Email Id</p>
        </div>
      );
    }
    return null;
  }

  getPasswordError() {
    if (this.state.showPasswordError) {
      return (
        <div>
          <span className={styles['error-icon']}>!</span>
          <p className={styles['error-message']}>Please enter password</p>
        </div>
      );
    }
    return null;
  }

  changeCaptcha() {
    const captchaInput = this.refs.captchaInput;
    if (captchaInput) {
      captchaInput.value = '';
    }
    this.setState({
      captchaURL: this.getCaptchUrl(),
      captchaError: null
    });
  }

  registerGoogleLogin() {
    const gPlusLoginSuccess = this.gPlusLoginSuccess;
    const gPlusLoginFailed = this.gPlusLoginFailed;

    if (at(window, 'gapi')) {
      window.gapi.load('auth2', () => {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        const auth2 = window.gapi.auth2.init({
          client_id: '787245060481-4padi0c9g2l1pnbie52fmpfdpo43209i.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin'
        });
        auth2.attachClickHandler(document.getElementById('gPlusLogin'),
          {}, gPlusLoginSuccess, gPlusLoginFailed);
      });
    }
  }

  sendLoginRequest(key, parameters) {
    if (this.state.isLoading) {
      return;
    }

    this.setState({
      isLoading: true
    });

    Track.event('login', key, 'submit');

    Client.post(config(key), parameters).end((err, res) => {
      if (err) {
        this.refs.notify.error(DEFAULT_ERROR_MESSAGE);
        this.setState({
          isLoading: false
        });
        return;
      }
      this.onLoginAction(res, key);
      this.setState({
        isLoading: false
      });
    });
  }

  facebookLogin() {
    if (at(window, 'FB')) {
      window.FB.login((response) => {
        if (response.status === 'connected') {
          const parameters = {
            token: at(response, 'authResponse.accessToken'),
            xsrf: this.refs.loginForm.xsrf.value,
            action: 'facebook',
            referer: this.getReferer()
          };
          this.sendLoginRequest('facebook', parameters);
        }
      }, { scope: 'public_profile, email' });
    }
  }

  validate(parameters) {
    let showEmailError = false;
    let showPasswordError = false;
    let captchaError = null;
    if (!Validations.isValidEmail(parameters.email)) {
      showEmailError = true;
    }

    if (typeof parameters.password !== 'string' || parameters.password.length === 0) {
      showPasswordError = true;
    }

    if (captchaEnabled && !parameters.captcha) {
      captchaError = 'Please enter the captcha text.';
    }

    this.setState({
      showEmailError,
      showPasswordError,
      captchaError
    });

    if (captchaEnabled) {
      return !(showEmailError || showPasswordError || captchaError);
    }

    return !(showEmailError || showPasswordError);
  }

  login(event) {
    event.preventDefault();
    const parameters = {
      email: this.refs.loginForm.email.value,
      password: this.refs.loginForm.password.value,
      action: 'signin',
      xsrf: this.refs.loginForm.xsrf.value,
      captcha: this.refs.loginForm.captcha ? this.refs.loginForm.captcha.value : null,
      referer: this.getReferer()
    };

    if (this.validate(parameters)) {
      if (captchaEnabled) {
        this.sendCaptchaVerifyRequest(parameters);
      } else {
        this.sendLoginRequest('signin', parameters);
      }
    }
  }

  sendCaptchaVerifyRequest(parameters) {
    this.setState({
      isLoading: true
    });

    const data = {
      value: trim(parameters.captcha),
      token: parameters.xsrf
    };

    Client.post('/captcha/verify/login', data).end((err, res) => {
      if (err) {
        let errorMessage = DEFAULT_ERROR_MESSAGE;

        if (at(res, 'body.error') === 'forbidden') {
          errorMessage = 'Invalid Session. Please refresh the page and try again.';
        }

        this.refs.notify.error(errorMessage);

        this.setState({
          isLoading: false
        });
        return;
      }

      if (res.body === true) {
        this.setState({
          isLoading: false
        }, () => {
          this.sendLoginRequest('signin', parameters);
        });
      } else {
        this.setState({
          isLoading: false,
          captchaError: 'Wrong captcha text entered. Please try again or change text.'
        });
      }
    });
  }

  gPlusLoginSuccess(googleUser) {
    const parameters = {
      token: getIdToken(googleUser),
      action: 'google',
      xsrf: this.refs.loginForm.xsrf.value,
      referer: this.getReferer()
    };

    this.sendLoginRequest('google', parameters);
  }

  gPlusLoginFailed() {
    this.refs.notify.error(DEFAULT_ERROR_MESSAGE);
  }

  navigateToIndex() {
    window.location.href = this.getReferer();
  }

  render() {
    return (
      <div className={styles.container}>
        <div className={styles.box}>
          <div className={styles.header}>
            <p className={styles.title}>Login to Myntra</p>
          </div>
          <div className={styles['third-party-login']}>
            <p className={styles['button-info-text']}>EASILY USING</p>
            <div className={styles['button-container']}>
              <button className={styles.facebook} onClick={this.facebookLogin}>
                <span className={`header-sprite ${styles['fb-logo']}`}></span>
                FACEBOOK
              </button>
              <button className={styles.google} id="gPlusLogin">
                <span className={`header-sprite ${styles['gplus-logo']}`}></span>
                GOOGLE
              </button>
            </div>
          </div>
          <p className={styles['info-text']}>- OR USING EMAIL -</p>
          <form method="POST" className={styles['login-form']} ref="loginForm" onSubmit={this.login} noValidate>
            <fieldset
              className={this.state.isFocussed ? styles['input-container-pink'] : styles['input-container']}>
              <div className={styles['input-item']}>
                <input
                  onFocus={this.setFocus}
                  onBlur={this.setBlur}
                  className={styles['user-input-email']}
                  name="email" type="email"
                  placeholder="Your Email Address">
                </input>
                {this.getEmailError()}
              </div>
              <div className={styles['input-item']}>
                <input
                  onFocus={this.setFocus}
                  onBlur={this.setBlur}
                  className={styles['user-input-password']}
                  name="password"
                  type="password"
                  placeholder="Enter Password" />
                {this.getPasswordError()}
              </div>
              <input type="hidden" name="xsrf" value={this.state.userToken} />
            </fieldset>
            {this.getCaptcha()}
            <fieldset className={styles['login-button-container']}>
              <button className={styles['login-button']}>Log in</button>
            </fieldset>
          </form>
          <div className={styles['link-container']}>
            <Link className={styles.link} to={this.getForgotPath()}>Recover password</Link>
            <div className={styles['right-links']}>
              <span className={styles['info-text']}>New to Myntra?</span>
              <Link className={styles['create-account-link']} to={this.getRegisterPath()}>Create Account</Link>
            </div>
          </div>
        </div>
        <Notify ref="notify" />
        <Loader show={this.state.isLoading} />
      </div>
    );
  }
}

Login.propTypes = {
  config: React.PropTypes.object,
  history: React.PropTypes.object.isRequired
};

Login.defaultProps = {
  config: (isBrowser() ? { session: window.__myx_session__ } : null)
};

export default Login;
