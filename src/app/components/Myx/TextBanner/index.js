import React, { PropTypes } from 'react';
import styles from './text-banner.css';

const TextBanner = ({ layout }) => {
  const { title, subtitle } = layout.props;
  return (
    <div className={styles.container}>
      {title && <h4 className={styles.title}>{title}</h4>}
      {title && <h4 className={styles.subtitle}>{subtitle}</h4>}
    </div>
  );
};


TextBanner.propTypes = {
  layout: PropTypes.object.isRequired
};


export default TextBanner;
