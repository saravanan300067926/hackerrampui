import React, { PropTypes } from 'react';
import styles from './image-navi.css';


const ImageNavi = ({ layout }) => {
  const { title, url } = layout.props;

  return (
    <li className={styles.base}><a className={styles.link} href={url}>{title}</a></li>
  );
};

ImageNavi.propTypes = {
  layout: PropTypes.object.isRequired
};


export default ImageNavi;
