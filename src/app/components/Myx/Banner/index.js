import React, { PropTypes } from 'react';
import Render from '../Render';


const Banner = ({ layout }) => {
  const aspectLayout = {
    type: 'container',
    props: {
      ...layout.props
    },
    weight: (layout.weight || 1),
    children: [{
      type: 'image',
      props: {
        ...layout.props
      }
    }]
  };
  return (
    <Render layout={aspectLayout} weight={(layout.weight || 1)} />
  );
};


Banner.propTypes = {
  layout: PropTypes.object.isRequired
};

export default Banner;
