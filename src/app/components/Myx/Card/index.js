import React, { PropTypes } from 'react';
import styles from './card.css';
import LazyLoad from 'react-lazyload';
import Track from '../../../utils/track';
import { calculateBottomPadding, getSrcSet } from '../../../utils/layout';
import { securify } from '../../../utils/securify';
import { getWebpUrl } from '../../../utils/getWebpUrl';
const secure = securify();

const Card = ({ layout }) => {
  const [imagesrc, srcSet] = getSrcSet(secure(layout.props.src), layout.weight);
  const webpImageSrc = getWebpUrl(imagesrc);

  return (
    <a className={`${styles.base}`} href={layout.props.url} onClick={() => Track.landingPages(layout)}>
      <div className={styles.container} >
        <div className={styles.card}>
          <div
            className={styles.imageContainer}
            style={{ paddingBottom: `${calculateBottomPadding(layout.props)}%` }}>
            <div className={styles.hideOverflow}>
              <LazyLoad once={true} offset={100} height={10}>
                <picture className="img-responsive">
                  <source
                    srcSet={webpImageSrc}
                    type="image/webp" />
                  <img
                    className={`${styles.image} ${layout.animated ? styles.animated : ''}`}
                    src={secure(imagesrc)}
                    srcSet={srcSet} />
                </picture>
              </LazyLoad>
            </div>
          </div>
        </div>
        <div className={styles.text}>
          <span className={styles.title}>{layout.props.title}</span>
          <span className={styles.subtitle}>{layout.props.subtitle}</span>
        </div>
        {layout.props.shop ? <div className={`${styles.text} ${styles.shop}`} >{layout.props.shop}</div> : null}
      </div>
    </a>
  );
};

Card.propTypes = {
  layout: PropTypes.object.isRequired
};


export default Card;
