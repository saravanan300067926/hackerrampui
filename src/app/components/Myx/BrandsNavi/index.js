import React, { PropTypes } from 'react';
import Render from '../Render';
import styles from './myx-brands-navi.css';


const BrandsNavi = ({ layout }) => {
  const { title, url } = layout.props;
  const children = layout.children || [];
  let ImageNavis = children.map((child, index) => (<Render key={index} layout={child} weight={(layout.weight || 1)} />));
  return (
    <div className={styles.container}>
      <ul>
        {ImageNavis}
        <li className={styles.title}><a href={url}>{title}</a></li>
      </ul>
    </div>
  );
};

BrandsNavi.propTypes = {
  layout: PropTypes.object.isRequired
};


export default BrandsNavi;
