import React, { PropTypes } from 'react';
import styles from './myx.css';
import Render from './Render';

class IndexContainer extends React.Component {
  getChildContext() {
    return { viewPortIndex: this.props.viewPortIndex };
  }

  render() {
    return (<div className={styles.indexContainer}>{this.props.children}</div>);
  }
}

IndexContainer.propTypes = {
  children: PropTypes.object,
  viewPortIndex: PropTypes.number
};


IndexContainer.childContextTypes = {
  viewPortIndex: React.PropTypes.number
};


const Myx = ({ layout }) => {
  if (!layout || !layout.type === 'layout' || !(layout.children && layout.children.length)) {
    return <div><span>Unexpected layout</span></div>;
  }

  return (
    <div className={styles.base}>
      {(layout.children && layout.children.map((child, index) => (
        <IndexContainer key={index} viewPortIndex={index}>
          <Render key={index} layout={child} weight={1} />
        </IndexContainer>
      )))}
    </div>
  );
};

Myx.propTypes = {
  layout: PropTypes.object.isRequired
};

export default Myx;
