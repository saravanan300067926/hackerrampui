// TODO: cleanup, use other primitives
import React, { PropTypes } from 'react';
import styles from './brand-card.css';
import Track from '../../../utils/track';
import LazyLoad from 'react-lazyload';
// import Render from '../Render';
import { calculateBottomPadding, getSrcSet } from '../../../utils/layout';
import { securify } from '../../../utils/securify';
import { getWebpUrl } from '../../../utils/getWebpUrl';
const secure = securify();

const BrandCard = ({ layout }) => {
  const [imagesrc, srcSet] = getSrcSet(secure(layout.props.src), layout.weight);
  const [brandSrc, brandSrcSet] = getSrcSet(secure(layout.props.brandSrc), layout.weight);
  const webpImageSrc = getWebpUrl(imagesrc);
  return (
    <a className={`${styles.base}`} href={layout.props.url} onClick={() => Track.landingPages(layout)}>
      <div className={styles.container}>
        <div className={styles.card}>
          <div
            className={styles.imageContainer}
            style={{ paddingBottom: `${calculateBottomPadding(layout.props)}%` }}>
            <div className={styles.hideOverflow}>
              <LazyLoad offset={50} height={10} once={true}>
                <picture className="img-responsive">
                  <source
                    srcSet={webpImageSrc}
                    type="image/webp" />
                  <img
                    className={`${styles.image} ${layout.props.animated ? styles.animated : ''}`}
                    src={secure(imagesrc)}
                    srcSet={srcSet} />
                </picture>
              </LazyLoad>
            </div>
          </div>
        </div>
        <div className={styles.brand}>
          <LazyLoad offset={50} height={10} once={true}>
            <picture className="img-responsive">
              <source
                srcSet={webpImageSrc}
                type="image/webp" />
              <img
                src={secure(brandSrc)}
                srcSet={brandSrcSet}
                className={`${styles.brandImage} ${layout.props.animated ? styles.animated : ''}`} />
            </picture>
          </LazyLoad>
        </div>
        <div className={styles.text}>
          <span className={styles.title}>{layout.props.title}</span>
          <span className={styles.text}>{layout.props.subtitle}</span>
        </div>
        {layout.props.shop ? <div className={`${styles.text} ${styles.shop}`} >{layout.props.shop}</div> : null}
      </div>
    </a>
  );
};

BrandCard.propTypes = {
  layout: PropTypes.object.isRequired
};


export default BrandCard;
