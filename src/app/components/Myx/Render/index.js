import React, { PropTypes } from 'react';
import Banner from '../Banner';
import Carousel from '../Carousel';
import Card from '../Card';
import Vertical from '../Vertical';
import Image from '../Image';
import SplitNavi from '../SplitNavi';
import Navi from '../Navi';
import NotFound from '../NotFound';
import BrandCard from '../BrandCard';
import Container from '../Container';
import TextBanner from '../TextBanner';
import Brands from '../Brands';
import YoutubeVideo from '../YoutubeVideo';
import defaultProps from './defaultProps';

const Renderers = {
  banner: Banner,
  carousel: Carousel,
  split: Container,
  image: Image,
  card: Card,
  vertical: Vertical,
  splitNavi: SplitNavi,
  navi: Navi,
  brandCard: BrandCard,
  container: Container,
  textBanner: TextBanner,
  grid: Container,
  brands: Brands,
  youtubeVideo: YoutubeVideo
};

const Render = ({ layout, props = {}, weight = 1 }) => {
  if (!layout || !layout.type) {
    return <div><span>{'Invalid Layout'}</span></div>;
  }
  if (!Renderers[layout.type]) {
    return <NotFound layout={layout} />;
  }
  const Component = Renderers[layout.type];
  const passProps = {
    props: {
      ...((defaultProps[layout.type] && defaultProps[layout.type](layout.props, weight)) || {}),
      ...props,
      ...layout.props
    }
  };
  return (
    <Component layout={{ ...layout, weight, ...passProps }} />
  );
};

Render.propTypes = {
  layout: PropTypes.object.isRequired,
  props: PropTypes.object,
  weight: PropTypes.number
};

Render.contextTypes = { viewPortIndex: React.PropTypes.number };


export default Render;
