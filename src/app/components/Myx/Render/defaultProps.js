import { securify } from '../../../utils/securify';
const secure = securify();
const defaultProps = {
  image: ({ animated = true }) => ({ animated }),
  banner: ({ animated = true }) => ({ animated }),
  brandCard: ({ animated = true }) => ({ animated }),
  container: (props, weight) => {
    const styles = {};
    if (!props) {
      return styles;
    }
    if (props.color) {
      styles.backgroundColor = props.color;
    }
    if (props.image) {
      styles.backgroundImage = (props.image.indexOf('linear-gradient') === -1) ? `url(${secure(`${props.image}`.replace(
        'assets.myntassets.com',
        `assets.myntassets.com/w_${Math.floor(980 * weight)},c_limit,fl_progressive`
      ))})` : `${secure(props.image)}`;
    }
    return styles;
  }
};
export default defaultProps;
