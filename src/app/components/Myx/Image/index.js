import React, { PropTypes } from 'react';
import styles from './image.css';
import LazyLoad from 'react-lazyload';
import Track from '../../../utils/track';
import { getSrcSet } from '../../../utils/layout';
import { securify } from '../../../utils/securify';
import { getWebpUrl } from '../../../utils/getWebpUrl';
const secure = securify();

const Image = ({ layout }, context) => {
  const [src, srcSet] = getSrcSet(secure(layout.props.src), layout.weight);
  const webpImageSrc = getWebpUrl(src);

  const imageHoverClass = (layout.props && layout.props.url) ? styles.hand : '';
  const Lazy = (<LazyLoad offset={50} height={10} once={true}>
    <picture className="img-responsive">
      <source
        srcSet={webpImageSrc}
        type="image/webp" />
      <img
        className={`${styles.image} ${layout.props.animated ? styles.animated : ''} ${imageHoverClass}`}
        src={secure(src)}
        alt={layout.props.alt}
        srcSet={srcSet} />
    </picture>
  </LazyLoad>);

  const NotLazy = (
    <div>
      <picture className="img-responsive">
        <source
          srcSet={webpImageSrc}
          type="image/webp" />
        <img
          className={`${styles.image} ${layout.props.animated ? styles.animated : ''} ${imageHoverClass}`}
          src={secure(src)}
          alt={layout.props.alt}
          srcSet={srcSet} />
      </picture>
    </div>
  );
  return (
    <a href={layout.props && layout.props.url} className={styles.base} onClick={() => Track.landingPages(layout)}>
      {(layout.props && (layout.props.noLazy || context.viewPortIndex < 3)) ? NotLazy : Lazy}
    </a>
  );
};

Image.propTypes = {
  layout: PropTypes.object.isRequired
};

Image.contextTypes = { viewPortIndex: React.PropTypes.number };


export default Image;
