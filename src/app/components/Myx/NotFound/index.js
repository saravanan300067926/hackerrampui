// FIXME: render empty view with tracking analitycs call
import React, { PropTypes } from 'react';
import styles from './not-found.css';


const NotFound = ({ layout }) => (
  process.env.NODE_ENV === 'production' ? null :
  (<div className={styles.base}>
    <span>{`${layout.type} component not found ${JSON.stringify(layout)}`}</span>
  </div>)
);

NotFound.propTypes = {
  layout: PropTypes.object.isRequired
};


export default NotFound;
