import React, { PropTypes } from 'react';
import Render from '../Render';
import Track from '../../../utils/track';
import styles from './navi.css';

const Link = ({ layout }) => {
  const { title, url, light, noLine, noLeftPad } = layout.props;
  const leftPadStyle = noLeftPad ? {} : { paddingLeft: '20px' };
  const linkStyle = light ? { fontWeight: 400 } : { fontWeight: 500 };
  const arrow = (light || !url) ? '' : <a
    className={`header-sprite ic_arrow_right ${styles.arrow} ico-ic_arrow_right`}
    href={url} onClick={() => Track.landingPages(layout)} />;
  const pad = arrow ? styles.pad : '';
  return (
    <li className={`${styles.base} ${noLine ? styles.noLine : ''} ${light ? styles.light : ''}`} style={leftPadStyle} >
      <a
        className={`${styles.link} ${pad} ${light ? styles.underline : ''}`}
        href={url} style={linkStyle} onClick={() => Track.landingPages(layout)}>{title}</a>
      {arrow}
    </li>
  );
};

Link.propTypes = {
  layout: PropTypes.object.isRequired
};


class Navi extends React.Component {
  static propTypes = {
    layout: PropTypes.object
  };
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      show: false
    };
  }
  getStyle = () => ({
    visibility: this.state.show ? 'visible' : 'hidden',
    maxHeight: this.state.open ? '100%' : 0
  });
  handleClick = (e) => {
    e.preventDefault();
    if (this.state.open === false) {
      this.setState({ show: true }, () => {
        this.setState({ open: true });
      });
    } else {
      this.setState({ open: false }, () => {
        this.setState({ show: false });
      });
    }
  };
  render() {
    const layout = this.props.layout;
    const { children, light, noLine } = layout;
    if (!(children && children.length)) {
      return <ul className={styles.noStyle}><Link {...this.props} light={light} noLine={noLine} /></ul>;
    }

    const { src, imageHeight, imageWidth } = layout.props;
    let { ratio } = layout.props;
    ratio = ratio || `${imageWidth}*${imageHeight}`;

    const Navis = children.map((child, index) => <Render key={index} layout={child} weight={1} />);

    let heading;
    if (src) {
      const layoutForRender = {
        type: 'container',
        props: {
          ratio
        },
        weight: (layout.weight || 1),
        children: [{
          type: 'image',
          props: {
            animated: (layout.props.animated),
            src,
            weight: (layout.weight || 1)
          }
        }]
      };
      heading = <Render layout={layoutForRender} weight={layout.weight || 1} />;
    } else {
      heading = <ul className={styles.noStyle}><Link {...this.props} light={light} noLine={noLine} /></ul>;
    }

    return (
      <div>
        <div onClick={this.handleClick}>
          {heading}
        </div>
        <ul className={styles.list} style={this.getStyle()}>{Navis}</ul>
      </div>
    );
  }
}


export default Navi;
