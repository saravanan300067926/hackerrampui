import React, { PropTypes } from 'react';
import at from 'v-at';
import Track from '../../../utils/track';
import Render from '../Render';
import { Column, Row } from '../../Grid';
import styles from './container.css';
import myxStyles from '../myx.css';
import { calculateBottomPadding } from '../../../utils/layout';
const supportedStyleAttributes = [
  // positioning
  'padding', 'paddingBottom', 'paddingTop', 'paddingLeft', 'paddingRight', 'margin', 'marginRight', 'marginLeft', 'marginTop', 'marginBottom',
  // background
  'backgroundColor', 'backgroundImage', 'backgroundRepeat'
];

const Container = ({ layout }) => {
  const style = {};
  // TODO :  responsive split as a component - non flex component due to safari wrap bug

  // handling styling
  supportedStyleAttributes.forEach((attribute) => {
    if (layout.props[attribute]) { style[attribute] = layout.props[attribute]; }
  });

  // handling aspect ratio if given
  let containerClass = '';
  let innerContainerStyle = {};
  if (at(layout, 'props.ratio') || (at(layout, 'props.width') && at(layout, 'props.height'))) {
    innerContainerStyle.paddingBottom = `${calculateBottomPadding(layout.props)}%`;
    containerClass = styles.aspectContainer;
  }
  const totalWidth = (layout.children &&
    layout.children.map((child) => (child.props && Number(child.props.width)) || 1).reduce((prev, cur) => (prev + cur))) || 1;
  const layoutWeight = layout.weight || 1;
  // rendering children
  const Children = layout.children && layout.children.map((child, index) => (
    <Column
      key={index}
      width={((child.props && Number(child.props.width)) || 1)}
      trackEvent={() => {
        Track.bannerClicks(child);

        if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
          window.Madalytics.send('streamCardClick', {
            event: 'streamCardClick',
            eventType: 'entity_event',
            'screen': {
              type: 'Homepage',
              url: 'https://developer.myntra.com/lgp/v2.9/stream',
              variant: 'web',
              name: '/stream'
            }
          });
        }
      }} >
      <Render
        layout={child}
        weight={layout.props.noSplit ?
          layoutWeight : (((child.props && Number(child.props.width)) || 1) / totalWidth) * layoutWeight} />
    </Column>)
  );
  let borderStyle = '';
  if (layout.props.border) {
    borderStyle = styles.border;
  } else {
    borderStyle = ['borderRight', 'borderLeft', 'borderTop', 'borderBottom'].map((prop) => (layout.props[prop] ? styles[prop] : '')).join(' ');
  }
  // if container should not split
  const InnerContainer = layout.props.noSplit ? Column : Row;

  return (
    <div className={`${styles.base} ${myxStyles.stretch} ${borderStyle}`} style={style}>
      <div style={innerContainerStyle}>
        <div className={`${styles.container} ${containerClass}`}>
          <InnerContainer>{Children}</InnerContainer>
        </div>
      </div>
    </div>
  );
};

Container.propTypes = {
  layout: PropTypes.object.isRequired
};


export default Container;
