import React, { PropTypes } from 'react';
import Render from '../Render';
import Image from '../Image';
import { Row, Column } from '../../Grid';
import styles from './split-navi.css';
import { calculateBottomPadding } from '../../../utils/layout';

const SplitNavi = ({ layout }) => {
  const { title, src } = layout.props;
  const children = layout.children;
  let Navis = children.map((child, index) =>
    <Render key={index} layout={child} weight={(layout.weight && layout.weight / 2) || 1} props={{ noLeftPad: 'true' }} />);

  return (
    <div className={styles.container}>
      <Row>
        <Column width={1} weight={((layout.weight && layout.weight / 2) || 1)} >
          <div
            className={styles.imageContainer}
            style={{ paddingBottom: `${calculateBottomPadding(layout.props)}%` }}>
            <Image layout={{ weight: (layout.weight && layout.weight / 2) || 1, props: { src }, animated: true }} />
          </div>
        </Column>
        <Column width={1} weight={((layout.weight && layout.weight / 2) || 1)} >
          <ul className={styles.list}>
            <li>
              <p className={styles.listHeading}>{title}</p>
              <p className={styles.listSubHeading}>Popular</p>
            </li>
            {Navis}
          </ul>
        </Column>
      </Row>
    </div>
  );
};

SplitNavi.propTypes = {
  layout: PropTypes.object.isRequired
};


export default SplitNavi;
