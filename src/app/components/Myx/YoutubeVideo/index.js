import React, { PropTypes } from 'react';
import styles from './youtube-video.css';

const YoutubeVideo = ({ layout }) => {
  const { allowsInlineMediaPlayback, videoId } = layout.props;
  const playsinline = allowsInlineMediaPlayback ? 1 : 0;
  return (
    <iframe
      className={styles.video}
      src={`http://www.youtube.com/embed/${videoId}?${playsinline}&autoplay=1`} />
  );
};


YoutubeVideo.propTypes = {
  layout: PropTypes.object.isRequired
};


export default YoutubeVideo;
