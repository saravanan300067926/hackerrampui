import React, { PropTypes } from 'react';
import styles from './carousel.css';
import Slider from 'react-slick';
import Render from '../Render';
import myxStyles from '../myx.css';
import { isBrowser } from '../../../utils';

const ANIMATION = false;
const Carousel = ({ layout }) => {
  const settings = {
    dots: true,
    infinite: !!isBrowser(),
    arrows: true,
    centerMode: true,
    speed: 500,
    slidesToShow: 1,
    draggable: true,
    initialSlide: 0,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 6000,
    pauseOnHover: true,
    waitForAnimate: false
  };

  if (!isBrowser() && layout && layout.children) {
    layout.children = layout.children.slice(0, 1);
  }

// TODO: find a better solution than removing key - same keys causes repeated slid,for infinite,to not be rendered- so that last slide does not get messed up
  let Children = layout.children && layout.children.map((child, index) => (
    <div key={index} className={styles.slide}>
      <Render layout={child} props={{ animated: ANIMATION, noLazy: true }} weight={(layout.weight || 1)} />
    </div>
  ));

  let borderStyle = '';
  if (layout.props.border) {
    borderStyle = styles.border;
  } else {
    borderStyle = ['borderRight', 'borderLeft', 'borderTop', 'borderBottom'].map((prop) => (layout.props[prop] ? styles[prop] : '')).join(' ');
  }

  return (
    <div className={`${myxStyles.stretch} ${borderStyle}`}>
      <div className={styles.base}>
        <Slider {...settings} >{Children}</Slider>
        <div style={{ clear: 'both' }} />
      </div>
    </div>
  );
};

Carousel.propTypes = {
  layout: PropTypes.object.isRequired
};


export default Carousel;
