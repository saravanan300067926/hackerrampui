import React, { PropTypes } from 'react';
import Render from '../Render';


const MyxVerticalSplit = ({ layout }) => {
  const containerLayout = { ...layout, props: { ...layout.props, noSplit: 'true' }, type: 'container' };
  return <Render layout={containerLayout} weight={(layout.weight || 1)} />;
};

MyxVerticalSplit.propTypes = {
  layout: PropTypes.object.isRequired
};


export default MyxVerticalSplit;
