import React, { PropTypes } from 'react';
import Render from '../Render';


const Brands = ({ layout }) => {
  const aspectLayout = {
    type: 'split',
    children: layout.children && layout.children.map(child => ({
      type: 'banner',
      props: {
        ...child.props,
        ratio: '120*70'
      }
    }))
  };
  return (
    <Render layout={aspectLayout} />
  );
};

Brands.propTypes = {
  layout: PropTypes.object.isRequired
};

export default Brands;
