import React from 'react';
import styles from './rewards.css';
import { getRewardsPageData } from '../../utils/actions';
import at from 'v-at';
import { Helper } from '../../utils/helper';
import { PreEORSHeader, EORSHeader, BumperOffer } from './subcomponents';
import classnames from 'classnames/bind';
import AppOnlyDialog from '../../Common/AppOnlyDialog';

const mynt = 'rgb(20,205,168)';
const bindClassnames = classnames.bind(styles);


class PreRewards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: true,
      dataFetchError: false,
      userGroup: '',
      rewardsData: [],
      spendsData: {},
      rewardsState: '',
      campaignState: 'OPEN',
      isFrozen: '',
      nextMilestone: {},
      showAppOnlyDialog: false
    };
  }

  componentDidMount() {
    document.body.scrollTop = 0;
    getRewardsPageData(this.onDataSuccess.bind(this), this.onDataError.bind(this));
  }

  onDataSuccess(respData) {
    const userGroup = at(respData, 'spends.groupId') || '';
    const rewardsState = at(respData, 'rewards.state') || '';
    const campaignState = at(respData, 'spends.state') || '';
    const isFrozen = at(respData, 'spends.isFrozen') || '';
    const nextMilestone = at(respData, 'spends.nextMilestone') || '';

    this.setState({
      rewardsData: at(respData.rewards, 'rewards') || [],
      spendsData: at(respData, 'spends') || {},
      userGroup,
      rewardsState,
      campaignState,
      isFrozen,
      nextMilestone,
      busy: false
    });
  }

  onDataError() {
    this.setState({
      busy: false,
      dataFetchError: true
    });
  }

  moreDetailsRedirect(url) {
    window.location.href = url;
  }

  isAptDevice() {
    return Helper.isMobile() && !Helper.isiPad();
  }

  hideAppOnlyDialog() {
    this.setState({ showAppOnlyDialog: false });
  }

  showAppOnlyDialog() {
    this.setState({ showAppOnlyDialog: true });
  }

  isEmptyObj(ob) {
    return Object.keys(ob).length === 0;
  }

  render() {
    const dataLength = this.state.rewardsData.length || 0;

    const EORSHeaderCond = (!this.isEmptyObj(this.state.nextMilestone) && this.state.campaignState === 'LIVE' && this.state.userGroup);

    let Header = null;
    if (this.state.rewardsState === 'CLOSED') {
      Helper.navigateTo('my/rewards');
    } else if (EORSHeaderCond) {
      // Show the EORSHeader
      Header = (<EORSHeader
        userSpending={at(this.state, 'spendsData.user.props.totalSpending') || ''}
        groupSpending={at(this.state, 'spendsData.standings.props.totalSpending') || ''}
        nextMile={at(this.state, 'nextMilestone.description') || ''} />);
    } else {
      // Show the PreEORS Header
      Header = (<PreEORSHeader
        offer={at(window, '__myx_kvpairs__.msg.rewards.description') || ''}
        allowCreate={!this.state.userGroup && !this.state.isFrozen}
        handleCreateGroupClick={() => this.showAppOnlyDialog()} />);
    }
    if (this.state.busy) {
      return (
        <div className={styles.loaderDiv}>
          <div className={styles.spinner}></div>
        </div>
      );
    } else if (this.state.dataFetchError) {
      return (
        <div className={styles.errorDiv}>
          <span>
            {Helper.getDefaultErrorMessage()}
          </span>
        </div>
      );
    } else if (this.state.rewardsData.length === 0) {
      return (
        <div className={styles.errorDiv}>
        </div>
      );
    }
    const now = new Date();
    const nonBumperRewards = this.state.rewardsData.filter((rewardData) => new Date(rewardData.rewardExpiryDate) > now && rewardData.type !== 'BUMPER');
    const bumperRewards = this.state.rewardsData.filter((rewardData) => new Date(rewardData.rewardExpiryDate) > now && rewardData.type === 'BUMPER');
    return (
      <div className={styles.mfgBaseContainer}>
        <div className={styles.mfgContentContainer}>
          {Header}
          <div className={styles.mfgRewardsBody}>
            <div className={styles.swimLanesHeading}>
              <span className={styles.swimLanesYou}> YOU </span>
              <span className={styles.swimLanesGroup}> GROUP </span>
              <div style={{ paddingTop: '5px' }}>
                <div className={`myntraweb-sprite ${styles.swimLanesHeadingAfter}`}> </div>
              </div>
            </div>
              {
                nonBumperRewards.map((thisReward, i) => (<RewardCard
                  key={`lrewardList${i}`}
                  index={i}
                  isLast={i === dataLength - 2}
                  title={thisReward.title}
                  description={thisReward.description}
                  url={thisReward.url}
                  status={thisReward.status}
                  rewardType={thisReward.type}
                  mileStoneData={thisReward.rewardMeta}
                  redirectFn={(url) => this.moreDetailsRedirect(url)} />))
              }
              {
                bumperRewards.map((thisReward, i) => (<BumperOffer
                  key={`brewardList${i}`}
                  title={thisReward.title}
                  description={thisReward.description}
                  url={thisReward.url} />))
              }
          </div>
        </div>
        <AppOnlyDialog
          show={this.state.showAppOnlyDialog}
          hideHandler={() => this.hideAppOnlyDialog()} />
      </div>
    );
  }
}


const RewardCard = (props) => {
  const { index, title, description, url, redirectFn, mileStoneData, isLast, status, rewardType } = props;
  let parsedMileStones = {};
  let indiMilestone;
  let groupMilestone;
  try {
    parsedMileStones = JSON.parse(mileStoneData);
    indiMilestone = parsedMileStones.individualMileStone || '';
    groupMilestone = parsedMileStones.groupMileStone || '';

    indiMilestone = Helper.numDifferentiation(indiMilestone);
    groupMilestone = Helper.numDifferentiation(groupMilestone);
  } catch (e) {
    console.log('Error in parsing mileStone data', parsedMileStones);
  }
  const rewardCardClass = bindClassnames({
    rewardCard: true,
    achievedRewardCard: Helper.isAchieved(status),
    cartOfferRewardCard: rewardType === 'CART_OFFER'
  });
  const getParsedDesc = (desc) => (
      desc
      .split('\n')
      .map((item, key) => <span key={key}>{item}<br /></span>)
  );
  return (
    <div className="foo" key={`slrewardList${index}`}>
      <div className={styles.rewardBlock}>
        <div className={` ${styles.leftBodyPanel}`}>
          <div className={styles.leftBodyPanelBack} style={isLast ? { backgroundRepeat: 'no-repeat', backgroundPosition: '40% 0%' } : null}></div>
          <MileStoneRenderer
            status={status}
            indiMilestone={indiMilestone}
            groupMilestone={groupMilestone} />
        </div>
        <div className={styles.rightBodyPanel} onClick={() => redirectFn(url)}>
          <div className={rewardCardClass}>
            <div className={styles.rewardText}>
              <div className={`myntraweb-sprite ${Helper.isAchieved(status) ? styles.rewardCardIconUnlocked : styles.rewardCardIconLocked}`} />
              <div className={styles.rewardCardTitle}> {title} </div>
              <div className={styles.rewardCardDescription}> {getParsedDesc(description)} </div>
            </div>
            <div className={styles.rewardCardArrow}>
              <div className={`myntraweb-sprite ${Helper.isAchieved(status) ? styles.rewardCardMoreDark : styles.rewardCardMoreLight}`}>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
RewardCard.propTypes = {
  index: React.PropTypes.number,
  isLast: React.PropTypes.bool,
  title: React.PropTypes.string,
  description: React.PropTypes.string,
  url: React.PropTypes.string,
  status: React.PropTypes.string,
  redirectFn: React.PropTypes.func,
  mileStoneData: React.PropTypes.string,
  rewardType: React.PropTypes.string
};


const MileStoneRenderer = (props) => {
  const { indiMilestone, groupMilestone, status } = props;
  let mileStoneBlock = null;

  if (!indiMilestone && !groupMilestone) {
    mileStoneBlock = null;
  } else if (indiMilestone === '0' && groupMilestone === '0') {
    mileStoneBlock = (
      <div className={styles.milestonesData} style={Helper.isAchieved(status) ? { color: mynt, borderColor: mynt } : null} >
        <div className={styles.joinedMilestone}> Joined </div>
      </div>
    );
  } else if (!indiMilestone) {
    mileStoneBlock = (
      <div
        className={styles.milestonesDataOnlyRight}
        style={Helper.isAchieved(status) ? { color: mynt, borderColor: mynt } : null} >
        <div className={styles.youMilestone}> {indiMilestone} </div>
        <div className={styles.groupMilestoneOnly}> {groupMilestone} </div>
      </div>
    );
  } else if (!groupMilestone) {
    mileStoneBlock = (
      <div
        className={styles.milestonesDataOnlyLeft}
        style={Helper.isAchieved(status) ? { color: mynt, borderColor: mynt } : null} >
        <div className={styles.youMilestoneOnly}> {indiMilestone} </div>
        <div className={styles.groupMilestone}> {groupMilestone} </div>
      </div>
    );
  } else {
    mileStoneBlock = (
      <div
        className={styles.milestonesData}
        style={Helper.isAchieved(status) ? { color: mynt, borderColor: mynt } : null}>
        <div className={styles.youMilestone}> {indiMilestone} </div>
        <div className={styles.divider}></div>
        <div className={styles.groupMilestone}> {groupMilestone} </div>
      </div>
    );
  }
  return (
    mileStoneBlock
  );
};
MileStoneRenderer.propTypes = {
  indiMilestone: React.PropTypes.string,
  groupMilestone: React.PropTypes.string,
  status: React.PropTypes.string
};

export default PreRewards;
