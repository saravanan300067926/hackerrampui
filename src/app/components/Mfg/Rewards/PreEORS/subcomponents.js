import React from 'react';
import styles from './rewards.css';
import { Helper } from '../../utils/helper';
import at from 'v-at';
import MultiLineText from '../../Common/MultiLineText';

export const PreEORSHeader = (props) => (
  <div className={styles.container}>
    <div className={styles.preHeading}>
      <img src={at(window, '__myx_kvpairs__.msg.logoUrl') || ''} alt="Myntra Shopping Groups" />
    </div>
    <div className={styles.mfginfo}>
      <MultiLineText
        text={props.offer} />
    </div>
    {
      props.allowCreate
      ? (
        <div>
          <div className={styles.action} onClick={() => props.handleCreateGroupClick()}>
            <button>Create A Group</button>
          </div>
          <div className={styles.whatIsMfgLink}>
            <div onClick={() => Helper.redirectTo(Helper.getWhatIsMfgLink())}>What are Myntra Shopping Groups?</div>
          </div>
        </div>
      )
      : null
    }
  </div>
);
PreEORSHeader.propTypes = {
  offer: React.PropTypes.string,
  allowCreate: React.PropTypes.bool,
  handleCreateGroupClick: React.PropTypes.func
};


export const EORSHeader = (props) => (
  <div>
    <div className={styles.container}>
      <div className={styles.heading}>Next Reward</div>
      <div className={styles.headerExtraInfo}>{props.nextMile}</div>
    </div>
    <div className={styles.spendInfo}> Shopped:
      <div className={styles.spendInfoCat}>
        You <span className={styles.spendInfoVal}> Rs. {Helper.formatRupees(props.userSpending, true)} </span>
      </div>
      <div className={styles.spendInfoCat}>
        Group <span className={styles.spendInfoVal}> Rs. {Helper.formatRupees(props.groupSpending, true)} </span>
      </div>
    </div>
  </div>
);
EORSHeader.propTypes = {
  nextMile: React.PropTypes.string,
  userSpending: React.PropTypes.string,
  groupSpending: React.PropTypes.string
};

export const BumperOffer = (props) => {
  const { title, description, url } = props;
  const getParsedDesc = (desc) => (
    desc
    .split('\n')
    .map((item, key) => <span key={key}>{item}<br /></span>)
  );
  return (
    <div className={styles.bumperOfferBox}>
      <div className={styles.bumperOfferHead}> {title} </div>
      <div className={styles.bumperOfferBody}> {getParsedDesc(description)} </div>
      <a href={url}>
        <div className={styles.bumperOfferDetails}> View Details
          <div className={`myntraweb-sprite ${styles.viewDetailsArrow}`}></div>
        </div>
      </a>
    </div>
  );
};
BumperOffer.propTypes = {
  title: React.PropTypes.string,
  description: React.PropTypes.string,
  url: React.PropTypes.string
};
