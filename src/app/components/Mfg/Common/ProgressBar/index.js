import React from 'react';
import styles from './progress.css';
import at from 'v-at';

const MAX_WIDTH = 360;

class ProgressBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      barStyles: {}
    };
  }

  componentWillMount() {
  }

  componentDidMount() {
    // set data in state
    const { speed } = this.props;
    const { fillEndWidth } = this.props;

    // Determine the style of 'filled' portion of bar from props.
    const barStyles = {
      display: 'inline-block',
      width: `${this.props.fillEndWidth}%`,
      height: this.props.height,
      backgroundColor: this.props.fillColor,
      border: `1px solid + ${this.props.fillColor}`
    };

    // Determine the style of 'remaining' portion of bar from props.
    const remainingBarStyle = Object.assign({}, barStyles);
    remainingBarStyle.backgroundColor = '#F1F1FB';
    remainingBarStyle.width = `${100 - this.props.fillEndWidth}%`;
    remainingBarStyle.position = 'absolute';
    remainingBarStyle.right = '0';

    // Set the styles in state.
    this.setState({
      barStyles,
      remainingBarStyle,
      speed,
      fillEndWidth
    }, () => {
      // start the 'animation' => movement of the bar.
      const x = setInterval(
        this.recalculate,
        100 / speed,
        this
      );
      this.setState({
        timerId: x
      });
    });
  }

  getLegendSepeartors(legendSeperator, legendStyle) {
    let seperators = null;
    if (legendSeperator && legendSeperator.required && legendSeperator.labels) {
      const legendSeperatorStyles = Object.keys(legendSeperator.labels).map((thisPoint) => {
        const legendSeperatorStyle = Object.assign({}, legendStyle);
        legendSeperatorStyle.right = `${100 - thisPoint}%`;
        legendSeperatorStyle.width = '0px';
        legendSeperatorStyle.marginRight = '0px';
        legendSeperatorStyle.height = at(legendSeperator, 'height') ? `${legendSeperator.height}px` : '20px';
        legendSeperatorStyle.border = at(legendSeperator, 'activeColor') ? `1px solid ${legendSeperator.activeColor}` : '1px solid black';
        if (!legendSeperator.labels[thisPoint].isActive) {
          legendSeperatorStyle.border = at(legendSeperator, 'inactiveColor') ? `1px solid ${legendSeperator.inactiveColor}` : '1px solid grey';
        }
        legendSeperatorStyle.bottom = '4px';
        legendSeperatorStyle.top = 'none';
        legendSeperatorStyle.zIndex = '7';
        return legendSeperatorStyle;
      });
      const legendSeperatorLabels = Object.keys(legendSeperator.labels).map((thisPoint) => {
        const legendSeperatorLabel = Object.assign({}, legendStyle);
        legendSeperatorLabel.right = thisPoint === '100' ? '-15px' : `calc(${100 - thisPoint}% - 28px)`;
        legendSeperatorLabel.top = `-${legendSeperator.height}px`;
        legendSeperatorLabel.backgroundColor = at(legendSeperator, 'activeColor') || 'black';
        if (!legendSeperator.labels[thisPoint].isActive) {
          legendSeperatorLabel.backgroundColor = at(legendSeperator, 'inactiveColor') || 'grey';
        }
        return legendSeperatorLabel;
      });
      seperators = Object.keys(legendSeperator.labels).map((thisMark, i) => (
        <div key={`prgbseperator${thisMark}`}>
          <div style={legendSeperatorStyles[i]}></div>
          <div style={legendSeperatorLabels[i]} className={styles.legendSeperatorLabel}>{legendSeperator.labels[thisMark].value}</div>
        </div>
      ));
    }
    return seperators;
  }

  recalculate = () => {
    // Calculate the new set of values.
    const newBarStyles = Object.assign({}, this.state.barStyles);
    const newRemainingBarStyle = Object.assign({}, this.state.remainingBarStyle);
    if (newBarStyles.width < this.state.fillEndWidth) {
      newBarStyles.width += 5;
      newRemainingBarStyle.width -= 5;

      // Set the new set of values in state
      this.setState({
        barStyles: newBarStyles,
        remainingBarStyle: newRemainingBarStyle
      });
    } else {
      // If animation over, clear the setInterval
      clearInterval(this.state.timerId);
    }
  }

  render() {
    const { legendPosition, legendText, extraStyles, legends, fillText, legendSeperator } = this.props;
    let legendStyle;
    let wrapperExtraStyles;
    const fontSize = 12;
    const wrapperWidth = this.props.width || MAX_WIDTH;
    if (legendPosition === 'bottom') {
      legendStyle = {
        position: 'absolute',
        fontSize: '12px',
        top: `${fontSize * 1.85}px`,
        ...extraStyles
      };
      wrapperExtraStyles = {
        marginBottom: `${fontSize * 1.85}px`,
        width: `${wrapperWidth}px`
      };
    } else {
      legendStyle = {
        position: 'absolute',
        fontSize: '12px',
        bottom: `${fontSize * 2.2}px`,
        ...extraStyles
      };
      wrapperExtraStyles = {
        marginTop: `${fontSize * 1.85}px`,
        width: `${wrapperWidth}px`
      };
    }

    // compute the left alues for progress marks.
    const extraLegendStyles = Object.keys(legends).map((thisPoint) => {
      const temp = Object.assign({}, legendStyle);
      temp.right = `${100 - thisPoint}%`;
      temp.fontWeight = 500;
      return temp;
    });

    return (
      <div className={styles['progress-wrapper']} style={wrapperExtraStyles}>
        <div style={legendStyle}> {legendText} </div>
        {
          Object.keys(legends).map((thisMark, i) => (
            <div key={`prgb${thisMark}`} style={extraLegendStyles[i]}> {legends[thisMark]} </div>
          ))
        }
        {this.getLegendSepeartors(legendSeperator, legendStyle)}
        <div className={styles.fillText}> {fillText} </div>
        <div style={this.state.barStyles}></div>
        <div style={this.state.remainingBarStyle}></div>
      </div>
    );
  }
}

ProgressBar.propTypes = {
  fillStartWidth: React.PropTypes.number,
  fillEndWidth: React.PropTypes.number,
  speed: React.PropTypes.number,
  height: React.PropTypes.number,
  width: React.PropTypes.number,
  fillColor: React.PropTypes.string,
  legends: React.PropTypes.object,
  legendPosition: React.PropTypes.string,
  legendAlign: React.PropTypes.string,
  legendText: React.PropTypes.string,
  fillText: React.PropTypes.string,
  extraStyles: React.PropTypes.object,
  legendSeperator: React.PropTypes.object
};

export default ProgressBar;
