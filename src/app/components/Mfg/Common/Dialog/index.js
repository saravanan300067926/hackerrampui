import React from 'react';
import { Helper } from '../../utils/helper';
import styles from './dialog.css';

class Dialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      show: props.show
    });
  }

  onBackdropClick() {
    if (this.props.backdropClickHandler) {
      this.props.backdropClickHandler();
    } else {
      this.setState({ show: false });
    }
  }

  render() {
    const dialogHeightStyle = {
      height: this.props.dialogHeight ? `${this.props.dialogHeight}px` : '165px',
      bottom: this.props.dialogHeight ? `-${this.props.dialogHeight}px` : '-165px'
    };
    return (
      <div className={!Helper.isMobile() || Helper.isiPad() ? styles.desktop : styles.mobile}>
        <div className={`${styles.backdrop} ${this.state.show ? styles.show : styles.hide}`} onClick={() => this.onBackdropClick()}>
        </div>
        <div className={`${styles.dialogContainer} ${this.state.show ? styles.active : ''}`} style={dialogHeightStyle}>
          {this.props.dialogContentGetter()}
        </div>
      </div>
    );
  }
}

Dialog.propTypes = {
  show: React.PropTypes.bool,
  dialogContentGetter: React.PropTypes.func,
  backdropClickHandler: React.PropTypes.func,
  dialogHeight: React.PropTypes.number
};

export default Dialog;
