import React from 'react';
import styles from './timer.css';

/*
* generic timer takes endDate and starts a timer
* <Timer endDate={deadline} onTimerExpiry={() => {console.log('timer expired')}}/>
*/

let timerId = '';

class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: {}
    };
    this.updateClock = this.updateClock.bind(this);
  }

  componentDidMount() {
    // running immediately once to avoid delay
    this.updateClock();
    timerId = setInterval(this.updateClock, 1000);
  }

  getTimeRemaining = endTime => {
    const t = parseInt(endTime, 10) - new Date().getTime();
    const seconds = Math.floor((t / 1000) % 60);
    const minutes = Math.floor((t / 1000 / 60) % 60);
    const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    const days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
      total: t,
      days,
      hours,
      minutes,
      seconds
    };
  };


  updateClock() {
    const timer = this.getTimeRemaining(this.props.endDate);
    if (timer.total <= 1) {
      clearInterval(timerId);
      window.location.reload();
      if (this.props.onTimerExpiry) {
        this.props.onTimerExpiry();
      }
      return;
    }
    this.setState({
      timer
    });
  }

  render() {
    if (!this.state.timer.total) {
      return null;
    }

    const timer = this.state.timer;
    return (
      <div>
        {timer.days > 0 && (
          <span>
            <span className={styles.timer}>
              {timer.days < 10 && '0'}
              {timer.days}
            </span>
            <small className={styles.timerUnit}> {timer.days === 1 ? 'day' : 'days'} </small>
            <span>&nbsp;</span>
          </span>
        )}
        <span className={styles.timer}>
          {timer.hours < 10 && '0'}
          {timer.hours}
        </span>
        <small className={styles.timerUnit}> {timer.hours <= 1 ? 'hr' : 'hrs'} </small>
        <span>&nbsp;</span>
        <span className={styles.timer}>
          {timer.minutes < 10 && '0'}
          {timer.minutes}
        </span>
        <small className={styles.timerUnit}> min </small>
        <span>&nbsp;</span>
        <span className={styles.timer}>
          {timer.seconds < 10 && '0'}
          {timer.seconds}
        </span>
        <small className={styles.timerUnit}> sec </small>
      </div>
    );
  }
}

Timer.propTypes = {
  endDate: React.PropTypes.number,
  onTimerExpiry: React.PropTypes.func
};

Timer.defaultProps = {
  endDate: new Date()
};

export default Timer;
