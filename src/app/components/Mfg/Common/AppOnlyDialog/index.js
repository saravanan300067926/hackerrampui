import React from 'react';
import styles from './apponlydialog.css';
import Dialog from '../Dialog';
import { isBrowser } from '../../../../utils';
import at from 'v-at';
import statics from '../../../Footer/links.json';

class AppOnlyDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  componentWillReceiveProps(props) {
    if (props.show) {
      document.body.style.overflowY = 'hidden';
      document.body.style.position = 'fixed';
    } else {
      document.body.style.overflowY = 'auto';
      document.body.style.position = 'static';
    }
    this.setState({
      show: props.show
    });
  }

  getAppDownloadLink() {
    let link;
    if (isBrowser()) {
      if (at(window, '__myx_deviceData__.deviceType') === 'desktop') {
        link = 'https://play.google.com/store/apps/details?id=com.myntra.android&hl=en';
      } else if (typeof at(window, '__myx_deviceData__.os')) {
        const os = at(window, '__myx_deviceData__.os');
        const kvpairs = at(window, '__myx_kvpairs__');
        if (os && kvpairs) {
          link = kvpairs[statics.osLinkMap[os]] || statics.kvpairsAppLinkMap[statics.osLinkMap[os]];
        }
      }
    }
    return link || '/mobile/apps';
  }

  getAppOnlyDialogContent() {
    return (
      <div className={styles.appDialogContent}>
        This feature is only available on app.
        <a href={this.getAppDownloadLink()}>
          DOWNLOAD APP
        </a>
      </div>
    );
  }

  render() {
    return (
      <Dialog
        show={this.props.show}
        dialogContentGetter={() => this.getAppOnlyDialogContent()}
        backdropClickHandler={this.props.hideHandler} />
    );
  }
}

AppOnlyDialog.propTypes = {
  show: React.PropTypes.bool,
  hideHandler: React.PropTypes.func
};

export default AppOnlyDialog;
