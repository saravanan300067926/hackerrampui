import React from 'react';

const MultiLineText = (props) => {
  const text = props.text || '';
  return text.indexOf('\n') === -1 ?
    <span>{text}</span> :
    <span>
      {text.split('\n').map((line, key) =>
        <span key={key}>{line}<br /></span>
      )}
    </span>;
};

MultiLineText.propTypes = {
  text: React.PropTypes.string
};

export default MultiLineText;
