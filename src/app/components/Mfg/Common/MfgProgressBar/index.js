import React from 'react';
import styles from './mfgprogressbar.css';
import at from 'v-at';
import ProgressBar from '../ProgressBar';
import { Helper } from '../../utils/helper';
import maxBy from 'lodash/maxBy';

class MfgProgressBar extends React.Component {
  constructor(props) {
    super(props);
    this.userFillPercentageData = {};
    this.groupFillPercentageData = {};
    this.maxUserLegendValue = 0;
    this.maxGroupLegendValue = 0;
  }

  getLegendsData(legendValues, fillPercentageData) {
    const legendsData = {};
    legendValues = legendValues || [];
    const legendValuesLen = legendValues.length;
    if (legendValuesLen > 0) {
      fillPercentageData[0] = 0;
      const key = 100 / legendValuesLen;
      for (let i = 0; i < legendValuesLen; i++) {
        const legendKey = key * (i + 1);
        legendsData[legendKey] = legendValues[i].label;
        fillPercentageData[`${legendValues[i].value}`] = legendKey;
      }
    }
    return legendsData;
  }

  getLengendSeperatorLabels() {
    const legendSeperatorData = {};
    let totalUserSpending = at(this.props.userProgressSpecs, 'totalSpending');
    if (totalUserSpending < 0) {
      totalUserSpending = 0;
    }
    let totalGroupSpending = at(this.props.groupProgressSpecs, 'totalSpending');
    if (totalGroupSpending < 0) {
      totalGroupSpending = 0;
    }
    const userLegendValues = at(this.props.milestones, 'user') || [];
    const groupLegendValues = at(this.props.milestones, 'group') || [];
    const legendValuesLen = userLegendValues.length;
    if (legendValuesLen > 0) {
      const key = 100 / legendValuesLen;
      for (let i = 0; i < legendValuesLen; i++) {
        legendSeperatorData[key * (i + 1)] = {
          value: userLegendValues[i].rewardLabel,
          isActive: totalUserSpending >= userLegendValues[i].value && totalGroupSpending >= groupLegendValues[i].value
        };
      }
    }
    return legendSeperatorData;
  }

  getFillEndWidth(milestones, fillPercentageData, value) {
    let fillEndWidth = fillPercentageData[value] || 0;
    if (!fillEndWidth && value) {
      try {
        let minBoundary = 0;
        let maxBoundary = 0;
        for (let i = 0; i < milestones.length; i++) {
          if (value < milestones[i].value) {
            minBoundary = i - 1 < 0 ? 0 : milestones[i - 1].value;
            maxBoundary = milestones[i].value;
            break;
          }
        }
        const parts = fillPercentageData[maxBoundary] - fillPercentageData[minBoundary];
        const boundaryDiff = maxBoundary - minBoundary;
        fillEndWidth = fillPercentageData[minBoundary] + ((parts / boundaryDiff) * (value - minBoundary));
      } catch (e) {
        fillEndWidth = 0;
      }
    }
    return fillEndWidth;
  }

  setMaxUserLegendValue() {
    const userLegends = at(this.props.milestones, 'user') || [];
    const maxUserLegendValue = maxBy(userLegends, (userLegend) => userLegend.value);
    this.maxUserLegendValue = at(maxUserLegendValue, 'value') || 0;
  }

  setMaxGroupLegendValue() {
    const groupLegends = at(this.props.milestones, 'group') || [];
    const maxGroupLegendValue = maxBy(groupLegends, (groupLegend) => groupLegend.value);
    this.maxGroupLegendValue = at(maxGroupLegendValue, 'value') || 0;
  }

  getUserProgressSpecs() {
    this.setMaxUserLegendValue();
    let totalSpending = at(this.props.userProgressSpecs, 'totalSpending');
    if (totalSpending < 0) {
      totalSpending = 0;
    }
    const width = at(this.props.userProgressSpecs, 'width') || (document.body.clientWidth - 40);
    return {
      height: at(this.props.userProgressSpecs, 'height') || 15,
      speed: 5,
      fillColor: at(this.props.userProgressSpecs, 'fillColor') || '#FFD41C',
      legends: this.getLegendsData(at(this.props.milestones, 'user'), this.userFillPercentageData),
      width,
      fillStartWidth: 0,
      fillEndWidth: this.getFillEndWidth(at(this.props.milestones, 'user'), this.userFillPercentageData, Math.min(totalSpending, this.maxUserLegendValue)),
      fillText: totalSpending > 0 ? `Rs. ${Helper.formatRupees(totalSpending)}` : 'Rs. 0',
      legendText: 'You',
      legendPosition: 'top',
      legendAlign: 'left',
      extraStyles: {
        marginRight: '3px',
        fontSize: '12px',
        color: '#282c3f'
      }
    };
  }

  getGroupProgressSpecs() {
    this.setMaxGroupLegendValue();
    let totalSpending = at(this.props.groupProgressSpecs, 'totalSpending');
    if (totalSpending < 0) {
      totalSpending = 0;
    }
    return {
      height: at(this.props.groupProgressSpecs, 'height') || 15,
      speed: 5,
      fillColor: at(this.props.groupProgressSpecs, 'fillColor') || '#FD9900',
      legends: this.getLegendsData(at(this.props.milestones, 'group'), this.groupFillPercentageData),
      width: at(this.props.groupProgressSpecs, 'width') || (document.body.clientWidth - 40),
      fillStartWidth: 0,
      fillEndWidth: this.getFillEndWidth(at(this.props.milestones, 'group'), this.groupFillPercentageData, Math.min(totalSpending, this.maxGroupLegendValue)),
      fillText: totalSpending > 0 ? `Rs. ${Helper.formatRupees(totalSpending)}` : 'Rs. 0',
      legendText: 'Group',
      legendPosition: 'bottom',
      legendAlign: 'left',
      legendSeperator: {
        required: true,
        height: 65,
        activeColor: '#ff3f6c',
        inactiveColor: '#ff9eb5',
        labels: this.getLengendSeperatorLabels()
      },
      extraStyles: {
        marginRight: '3px',
        fontSize: '12px',
        color: '#282c3f'
      }
    };
  }

  render() {
    return (
      <div className={styles.mfgProgressBarsContainer}>
        <ProgressBar {...this.getUserProgressSpecs()} />
        <ProgressBar {...this.getGroupProgressSpecs()} />
      </div>
    );
  }
}

MfgProgressBar.propTypes = {
  milestones: React.PropTypes.object,
  userProgressSpecs: React.PropTypes.object,
  groupProgressSpecs: React.PropTypes.object
};

export default MfgProgressBar;
