import React from 'react';
import styles from './onandpostcampaign.css';
import GroupHeader from '../GroupHeader';
import Members from '../Members';
import at from 'v-at';
import { Helper } from '../../utils/helper';
import InactiveBoard from '../InactiveBoard';
import MfgProgressBar from '../../Common/MfgProgressBar';
import Cards from '../Cards';
import Track from '../../../../utils/track';

class OnAndPostCampaign extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getState(this.props);
  }

  componentDidMount() {
    Track.event('msg', 'dashboard', `${at(this.state, 'group.size') || 1} members`);
  }

  componentWillReceiveProps(props) {
    this.setState(this.getState(props));
  }

  onViewRewardsClick() {
    Track.event('msg', 'dashboard', 'view rewards');
    window.location.href = Helper.getViewRewardsLink();
  }

  getState(props) {
    return {
      group: {
        isActive: at(props.groupDetails, 'isActive'),
        minSize: at(props.groupDetails, 'minGroupSize') || 3,
        maxSize: at(props.groupDetails, 'maxGroupSize') || 20,
        size: at(props.groupDetails, 'actualGroupSize') || 0,
        name: at(props.groupDetails, 'groupName') || '',
        rank: at(props.groupDetails, 'standings.rank') || 0,
        pic: at(props.groupDetails, 'groupImage') || '',
        members: at(props.groupDetails, 'members') || [],
        admin: at(props.groupDetails, 'groupAdmin') || {}
      },
      totalShoppedAmount: at(props.groupDetails, 'standings.props.totalSpending') || 0,
      totalSavedAmount: at(props.groupDetails, 'standings.props.totalSaving') || 0,
      milestones: at(props.groupDetails, 'milestones') || {},
      user: {
        totalShoppedAmount: at(props.groupDetails, 'user.props.totalSpending') || 0,
        uidx: at(props.groupDetails, 'user.profile.uidx') || ''
      },
      isFrozen: at(props.groupDetails, 'isFrozen'),
      checkedInUsers: at(props.groupDetails, 'offers.checked_in_users') || {},
      cards: at(props.groupDetails, 'offers.cards') || [],
      offersState: at(props.groupDetails, 'offers.state') || ''
    };
  }

  getGroupMembersDetails(showMembersHeader) {
    return (
      <Members
        members={this.state.group.members}
        groupAdmin={this.state.group.admin}
        campaignState="onpost"
        isActive={this.state.group.isActive}
        maxGroupSize={this.state.group.maxSize}
        showMembersHeader={showMembersHeader}
        checkedInUsers={this.state.checkedInUsers}
        offersState={this.state.offersState} />
    );
  }

  getCardsSection() {
    if (this.state.cards.length > 0) {
      return (
        <div>
          <Cards
            data={this.state.cards}
            groupId={this.state.group.id}
            setBusy={this.props.setBusy}
            resetGroupDetails={this.props.resetGroupDetails} />
        </div>
      );
    }
    return null;
  }

  getGroupDashboard() {
    if (this.state.group.isActive) {
      return this.getActiveGroupDashboard();
    }
    return this.getInActiveGroupDashboard();
  }

  getShoppedAndSavedAmountsDiv() {
    return (
      <div className={styles.shoppedSavedAmount}>
        <div className={styles.shoppedAmount}><span>Rs. {Helper.formatRupees(this.state.totalShoppedAmount, true)}</span> shopped</div>
        <div className={styles.savedAmount}><span>Rs. {Helper.formatRupees(this.state.totalSavedAmount, true)}</span> saved</div>
      </div>
    );
  }

  getGroupRanking() {
    if (!this.state.isFrozen) {
      return -1;
    }
    return this.state.group.rank;
  }

  getGroupHeaderSection(rank) {
    return (
      <GroupHeader
        name={this.state.group.name}
        rank={rank}
        icon={this.state.group.pic}
        membersCount={this.state.group.size}
        checkInCount={Object.keys(this.state.checkedInUsers).length}
        offersState={this.state.offersState} />
    );
  }

  getActiveGroupDashboard() {
    const containerWidth = Helper.isMobile() && !Helper.isiPad() ? document.body.offsetWidth : 480;
    const userProgressSpecs = {
      totalSpending: this.state.user.totalShoppedAmount,
      width: containerWidth - 32,
      height: 22
    };
    const groupProgressSpecs = {
      totalSpending: this.state.totalShoppedAmount,
      width: containerWidth - 32,
      height: 22
    };
    return (
      <div>
        <div className={styles.groupDetailsContainer}>
          {this.getGroupHeaderSection(this.getGroupRanking())}
          {this.getShoppedAndSavedAmountsDiv()}
          <div className={styles.mileStoneDisplay}>
            <span className={styles.milestonesHeading}>MILESTONES</span>
            <span className={styles.milestonesAllRewards} onClick={this.onViewRewardsClick}>VIEW REWARDS ></span>
          </div>
          <MfgProgressBar
            milestones={this.state.milestones}
            userProgressSpecs={userProgressSpecs}
            groupProgressSpecs={groupProgressSpecs} />
          {this.getCardsSection()}
        </div>
        <div>
          {this.getGroupMembersDetails(true)}
        </div>
      </div>
    );
  }

  getInActiveGroupDashboard() {
    const group = this.state.group;
    return (
      <div>
        <div className={styles.groupDetailsContainer}>
          {this.getGroupHeaderSection(-1)}
          {this.getCardsSection()}
          <InactiveBoard
            isGroupActive={group.isActive}
            minGroupSize={group.minSize}
            maxGroupSize={group.maxSize}
            groupSize={group.size} />
        </div>
        <div>
          {this.getGroupMembersDetails(true)}
        </div>
      </div>
    );
  }

  render() {
    return this.getGroupDashboard();
  }
}

OnAndPostCampaign.propTypes = {
  groupDetails: React.PropTypes.object,
  setBusy: React.PropTypes.func,
  resetGroupDetails: React.PropTypes.func
};

export default OnAndPostCampaign;
