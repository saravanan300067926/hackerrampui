import React from 'react';
import styles from './groupdashboard.css';
import PreCampaign from './PreCampaign';
import OnAndPostCampaign from './OnAndPostCampaign';
import { getGroupLeaderBoardData } from '../utils/actions';
import { Helper } from '../utils/helper';
import at from 'v-at';
import MFGMadalytics from '../utils/mfgMadalytics';

class GroupDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: true,
      groupDetails: {},
      dataFetchError: false,
      noUserGroup: false
    };
    this.setBusy = this.setBusy.bind(this);
    this.resetGroupDetails = this.resetGroupDetails.bind(this);
  }

  componentWillMount() {
    if (!Helper.isLoggedIn() && !this.isFbBot()) {
      this.redirectToLogin();
    } else {
      this.getMyGroupDetails();
    }
  }

  componentDidMount() {
    document.body.scrollTop = 0;
    Helper.setDocumentTitle('Myntra Shopping Group - Group Dashboard');
  }

  getMyGroupDetails() {
    getGroupLeaderBoardData(this.getGroupDashboardSuccess.bind(this), this.getGroupDashboardError.bind(this));
  }

  getGroupDashboard() {
    if (this.state.busy) {
      return (
        <div className={styles.loaderDiv}>
          <div className={styles.spinner}></div>
        </div>
      );
    }
    if (this.state.noUserGroup) {
      window.location.href = '/shoppinggroups/leaderboard';
      return null;
    }
    if (this.state.dataFetchError) {
      return (
        <div className={styles.errorDiv}>
          <span>
            {Helper.getDefaultErrorMessage()}
          </span>
        </div>
      );
    }
    let campaignBoard = null;
    const campaignStatus = at(this.state.groupDetails, 'campaign.status');
    const isFrozen = at(this.state.groupDetails, 'isFrozen');
    if (campaignStatus) {
      if (campaignStatus === 'OPEN' || (campaignStatus === 'CLOSED' && !isFrozen)) {
        campaignBoard = (<PreCampaign
          setBusy={this.setBusy}
          groupDetails={this.state.groupDetails}
          resetGroupDetails={this.resetGroupDetails} />);
      } else if (campaignStatus === 'LIVE' || campaignStatus === 'CLOSED') {
        campaignBoard = (<OnAndPostCampaign
          setBusy={this.setBusy}
          groupDetails={this.state.groupDetails}
          resetGroupDetails={this.resetGroupDetails} />);
      }
    }
    return (
      <div className={styles.baseContainer}>
        <div className={styles.groupsContainer}>
          {campaignBoard}
        </div>
      </div>
    );
  }

  getGroupDashboardSuccess(data) {
    if (Object.keys(data).length === 0) {
      this.setState({
        busy: false,
        noUserGroup: true
      });
      return;
    }
    this.setState({
      busy: false,
      groupDetails: data
    });
    MFGMadalytics.sendGroupDashboardLoadEvent(at(data, 'groupId') || '', at(data, 'groupName') || '');
  }

  getGroupDashboardError(statuscode) {
    if (statuscode === '403' && !this.isFbBot()) {
      this.redirectToLogin();
    } else {
      this.setState({
        dataFetchError: true,
        busy: false
      });
    }
  }

  setBusy(busy) {
    this.setState({ busy });
  }

  isFbBot() {
    const nav = at(window, 'navigator') || {};
    const ua = nav.userAgent ? nav.userAgent : 'na';
    return (ua.indexOf('facebookexternalhit') !== -1 || ua.indexOf('Facebot') !== -1);
  }

  redirectToLogin() {
    window.location.href = `${Helper.getSecureRoot()}login?referer=${window.location.href}`;
  }

  resetGroupDetails(groupDetails) {
    this.setState({ groupDetails });
  }

  render() {
    return (
      <div>
        {this.getGroupDashboard()}
      </div>
    );
  }
}

export default GroupDashboard;
