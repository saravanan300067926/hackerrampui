import React from 'react';
import CouponActive from './CouponActive';
import CouponInactive from './CouponInactive';
import CheckIn from './CheckIn';
import Info from './Info';
import ShoutOut from './ShoutOut';
import Banner from './Banner';
import { checkinGroup } from '../../utils/actions';
import { Helper } from '../../utils/helper';

class Cards extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data
    };
    this.checkinHandler = this.checkinHandler.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState({ data: props.data });
  }

  getActionObject(button) {
    if (button) {
      let style = {
        'cursor': 'inherit'
      };
      if (button.link) {
        style = {
          'cursor': 'pointer'
        };
      }
      return { ...button, ...{ style }, ...{ handler: this.actionClickHandler.bind(this, button) } };
    }
    return {};
  }

  getAllCards() {
    const allCards = this.state.data.map((card, i) => {
      switch (card.type) {
        case 'CHECK_IN': return (
          <div key={`card${i}`}>
            <CheckIn
              title={card.title || ''}
              action={card.button || {}}
              checkinHandler={this.checkinHandler} />
          </div>
          );
        case 'COUPON_INACTIVE': return (
          <div key={`card${i}`}>
            <CouponInactive
              title={card.title || ''}
              action={this.getActionObject(card.button)}
              description={card.description || ''} />
          </div>
          );
        case 'COUPON': return (
          <div key={`card${i}`}>
            <CouponActive
              title={card.title || ''}
              action={this.getActionObject(card.button)}
              description={card.description || ''} />
          </div>
          );
        case 'INFO': return (
          <div key={`card${i}`}>
            <Info
              title={card.title || ''}
              subtitle={card.subtitle || ''}
              description={card.description || ''} />
          </div>
          );
        case 'SHOUT_OUT': return (
          <div key={`card${i}`}>
            <ShoutOut
              title={card.title || ''}
              description={card.description || ''} />
          </div>
          );
        case 'BANNER': return (
          <div key={`card${i}`}>
            <Banner
              imageUrl={card.image || ''}
              link={card.endpoint || ''} />
          </div>
          );
        default: return (<div key={`card${i}`}></div>);
      }
    });
    return allCards;
  }

  checkinGroupSuccess(data) {
    this.props.setBusy(false);
    this.props.resetGroupDetails(data);
  }

  checkinGroupError(statusCode) {
    this.props.setBusy(false);
    if (statusCode === '403') {
      window.location.href = `${Helper.getSecureRoot()}login?referer=${window.location.href}`;
    }
  }

  checkinHandler() {
    const data = {
      groupId: this.props.groupId
    };
    this.props.setBusy(true);
    checkinGroup(data, { 'X-CSRF-TOKEN': Helper.getXsrfToken() }, this.checkinGroupSuccess.bind(this), this.checkinGroupError.bind(this));
  }

  actionClickHandler(action) {
    if (action.link) {
      window.location.href = action.link;
    }
  }

  render() {
    return (
      <div>
        {this.getAllCards()}
      </div>
    );
  }
}

Cards.propTypes = {
  data: React.PropTypes.array,
  groupId: React.PropTypes.string,
  setBusy: React.PropTypes.func,
  resetGroupDetails: React.PropTypes.func
};

export default Cards;
