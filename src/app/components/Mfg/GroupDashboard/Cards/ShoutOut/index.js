import React from 'react';
import styles from './shoutout.css';

const ShoutOut = (props) =>
(
  <div className={styles.container}>
    {props.title ?
    (<div className={styles.title}>
      {props.title}
    </div>) : null
    }
    <div className={styles.description}>
      {props.description}
    </div>
  </div>
);

ShoutOut.propTypes = {
  title: React.PropTypes.string,
  description: React.PropTypes.string
};

export default ShoutOut;
