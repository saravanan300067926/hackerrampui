import React from 'react';
import styles from './checkin.css';

class CheckIn extends React.Component {

  checkIn() {
    if (this.props.checkinHandler) {
      this.props.checkinHandler();
    }
  }

  render() {
    return (
      <div className={styles.container}>
        <div className={styles.title}>
          {this.props.title}
        </div>
        <div className={styles.action} onClick={() => this.checkIn()}>
          <div>{this.props.action.text || 'CHECK IN'}</div>
        </div>
      </div>
    );
  }
}

CheckIn.propTypes = {
  title: React.PropTypes.string,
  checkinHandler: React.PropTypes.func,
  action: React.PropTypes.object
};

export default CheckIn;
