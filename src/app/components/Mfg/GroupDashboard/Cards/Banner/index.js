import React from 'react';
import styles from './banner.css';
import { securify } from '../../../../../utils/securify';
const secure = securify();

const Banner = (props) => {
  if (props.imageUrl) {
    return (<div className={styles.container} onClick={() => { window.location.href = props.link; }}>
      <img src={secure(props.imageUrl)} />
    </div>);
  }
  return null;
};

Banner.propTypes = {
  imageUrl: React.PropTypes.string,
  link: React.PropTypes.string
};

export default Banner;
