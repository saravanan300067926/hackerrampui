import React from 'react';
import styles from './info.css';

const Info = (props) =>
(
  <div className={styles.container}>
    <div className={styles.title}>
      {props.title}
    </div>
    <div className={styles.subtitle}>
      {props.subtitle}
    </div>
    <div className={styles.description}>
      {props.description}
    </div>
  </div>
);

Info.propTypes = {
  title: React.PropTypes.string,
  subtitle: React.PropTypes.string,
  description: React.PropTypes.string
};

export default Info;
