import React from 'react';
import styles from './couponActive.css';

const CouponActive = (props) =>
(
  <div className={styles.container}>
    <div className={styles.title}>
      {props.title}
    </div>
    <div className={styles.description}>
      {props.description}
    </div>
    {props.action.text ?
    (<div className={styles.action} style={props.action.style} onClick={props.action.handler}>
      <span>{props.action.text}</span>
    </div>) : null
    }
  </div>
);

CouponActive.propTypes = {
  title: React.PropTypes.string,
  description: React.PropTypes.string,
  action: React.PropTypes.object
};

export default CouponActive;
