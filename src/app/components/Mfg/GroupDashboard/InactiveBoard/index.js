import React from 'react';
import styles from './inactiveboard.css';

const InactiveBoard = (props) =>
    (<div className={styles.groupStatusMembersContainer}>
      <div className={styles.groupStatusContainer}>
        <div className={styles.statusText}>{props.isGroupActive ? 'Active' : 'Inactive Group'}</div>
      </div>
      <div className={styles.minGroupSizeInfo}>
        <span>Add</span>
        <span className={styles.bold}> {props.minGroupSize - props.groupSize} more {(props.minGroupSize - props.groupSize) === 1 ? 'member' : 'members'} </span>
        <span>to activate your group.</span>
      </div>
    </div>
    );

InactiveBoard.propTypes = {
  isGroupActive: React.PropTypes.bool,
  minGroupSize: React.PropTypes.number,
  maxGroupSize: React.PropTypes.number,
  groupSize: React.PropTypes.number
};

export default InactiveBoard;
