import React from 'react';
import styles from './groupheader.css';
import at from 'v-at';

class GroupHeader extends React.Component {

  onMfgRankingClick() {
    window.location.href = '/shoppinggroups/leaderboard';
  }

  getNameDiv() {
    let nameStyle = {};
    if (!this.showRank() && !this.props.link) {
      nameStyle = {
        top: '18px'
      };
    }
    return (
      <div className={styles.nameDiv} style={nameStyle}>
        {this.props.name}
      </div>
    );
  }

  getRankDiv() {
    if (this.showRank()) {
      let arrowStyle = {
        'verticalAlign': 'top'
      };
      let textMsg = 'Group Ranking';
      if (this.props.rank === -1) {
        arrowStyle = {
          'verticalAlign': 'middle'
        };
        textMsg = 'All Groups';
      }
      return (
        <div className={styles.rankDiv} onClick={this.onMfgRankingClick}>
          {textMsg} {this.props.rank === -1 ? '' : this.props.rank}
          <span style={arrowStyle}> > </span>
        </div>
      );
    }
    return null;
  }

  getInactiveMembersCountSection() {
    if (this.props.checkInCount === -1) {
      return null;
    }
    if (this.props.offersState === 'PRE_PR') {
      return <span>{this.props.checkInCount ? ` (${this.props.checkInCount} Checked-in)` : ' (All inactive)'}</span>;
    }
    return null;
  }

  getMembersCountDiv() {
    if (this.props.membersCount) {
      let membersStyle = {};
      if (!this.showRank() && !this.props.link) {
        membersStyle = {
          top: '45px'
        };
      }
      return (
        <div className={styles.membersDiv} style={membersStyle}>
          {this.props.membersCount} {this.props.membersCount > 1 ? 'members' : 'member'}
          {this.getInactiveMembersCountSection()}
        </div>
      );
    }
    return null;
  }

  getLinkDiv() {
    if (this.props.link) {
      return (
        <div className={styles.link} onClick={this.props.link.handler}>
          {this.props.link.name}
          <span> > </span>
        </div>
      );
    }
    return null;
  }

  showRank() {
    return !this.props.link && this.props.rank && at(window, '__myx_kvpairs__.msg.dashboard.enableLeaderboardLink');
  }

  render() {
    return (
      <div className={styles.headerContainer}>
        <div className={styles.iconDiv}>
          <img src={this.props.icon}>
          </img>
        </div>
        {this.getNameDiv()}
        {this.getRankDiv()}
        {this.getLinkDiv()}
        {this.getMembersCountDiv()}
      </div>
    );
  }
}

GroupHeader.propTypes = {
  icon: React.PropTypes.string,
  name: React.PropTypes.string,
  rank: React.PropTypes.number,
  membersCount: React.PropTypes.number,
  checkInCount: React.PropTypes.number,
  offersState: React.PropTypes.string,
  link: React.PropTypes.object
};

export default GroupHeader;
