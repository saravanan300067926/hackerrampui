import React from 'react';
import styles from './precampaign.css';
import GroupHeader from '../GroupHeader';
import InactiveBoard from '../InactiveBoard';
import Members from '../Members';
import at from 'v-at';
import { Helper } from '../../utils/helper';
import Cards from '../Cards';
import Timer from '../../Common/Timer';
import Track from '../../../../utils/track';

class PreCampaign extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getState(this.props);
  }

  componentDidMount() {
    Track.event('msg', 'dashboard', `${at(this.state, 'group.size') || 1} members`);
  }

  componentWillReceiveProps(props) {
    this.setState(this.getState(props));
  }

  onViewRewardsClick() {
    Track.event('msg', 'dashboard', 'view rewards');
    window.location.href = Helper.getViewRewardsLink();
  }

  onWhatAreGroupsClick() {
    window.location.href = Helper.getWhatIsMfgLink();
  }

  getState(props) {
    return {
      group: {
        id: at(props.groupDetails, 'groupId') || '',
        isActive: at(props.groupDetails, 'isActive'),
        inviteCode: at(props.groupDetails, 'inviteCode'),
        minSize: at(props.groupDetails, 'minGroupSize') || 3,
        maxSize: at(props.groupDetails, 'maxGroupSize') || 20,
        size: at(props.groupDetails, 'actualGroupSize') || 0,
        name: at(props.groupDetails, 'groupName') || '',
        rank: at(props.groupDetails, 'standings.rank') || 0,
        pic: at(props.groupDetails, 'groupImage') || '',
        members: at(props.groupDetails, 'members') || [],
        admin: at(props.groupDetails, 'groupAdmin') || {},
        isFrozen: at(props.groupDetails, 'isFrozen') || false
      },
      shortlistedItemsCount: at(props.groupDetails, 'standings.props.wishlisted') || 0,
      likedItemsCount: at(props.groupDetails, 'standings.props.likes') || 0,
      campaignState: at(props.groupDetails, 'campaign.status'),
      isFrozen: at(props.groupDetails, 'isFrozen'),
      checkedInUsers: at(props.groupDetails, 'offers.checked_in_users') || {},
      cards: at(props.groupDetails, 'offers.cards') || [],
      offersState: at(props.groupDetails, 'offers.state') || ''
    };
  }

  getInviteCode() {
    if (this.state.group.inviteCode && this.state.group.size < this.state.group.maxSize && !this.state.group.isFrozen) {
      return this.state.group.inviteCode;
    }
    return '';
  }

  getGroupMembersDetails(showMembersHeader, hideSpendingInfo) {
    return (
      <Members
        members={this.state.group.members}
        groupAdmin={this.state.group.admin}
        campaignState="pre"
        isActive={this.state.group.isActive}
        maxGroupSize={this.state.group.maxSize}
        showMembersHeader={showMembersHeader}
        hideSpendingInfo={hideSpendingInfo || false}
        inviteCode={this.getInviteCode()}
        checkedInUsers={this.state.checkedInUsers}
        offersState={this.state.offersState} />
    );
  }

  getGroupDashboard() {
    if (this.state.campaignState === 'CLOSED' && !this.state.isFrozen && this.state.group.isActive) {
      return this.getPostCampaignFrozenDashboard();
    }
    if (this.state.group.isActive) {
      return this.getActiveGroupDashboard();
    }
    return this.getInActiveGroupDashboard();
  }

  getGroupHeaderSection(rank) {
    const link = {
      name: 'VIEW REWARDS',
      handler: this.onViewRewardsClick
    };
    return (
      <GroupHeader
        name={this.state.group.name}
        rank={rank}
        icon={this.state.group.pic}
        membersCount={this.state.group.size}
        checkInCount={Object.keys(this.state.checkedInUsers).length}
        offersState={this.state.offersState}
        link={link} />
    );
  }

  getCardsSection() {
    if (this.state.cards.length > 0) {
      return (
        <div>
          <Cards
            data={this.state.cards}
            groupId={this.state.group.id}
            setBusy={this.props.setBusy}
            resetGroupDetails={this.props.resetGroupDetails} />
        </div>
      );
    }
    return null;
  }

  getMetricDivs() {
    if (at(window, '__myx_kvpairs__.msg.dashboard.enableMemberStats')) {
      return (
        <div className={styles.metricsContainer}>
          <div className={styles.metric}>
            <span>{this.state.shortlistedItemsCount}</span> {this.state.shortlistedItemsCount > 1 ? 'items' : 'item'} shortlisted
          </div>
          <div className={styles.metric}>
            <span>{this.state.likedItemsCount}</span> {this.state.shortlistedItemsCount > 1 ? 'items' : 'item'} liked
          </div>
        </div>
      );
    }
    return null;
  }

  getPostCampaignFrozenDashboard() {
    return (
      <div>
        <div className={styles.groupDetailsContainer}>
          {this.getGroupHeaderSection(-1)}
          {this.getMetricDivs()}
          {this.getCardsSection()}
        </div>
        <div>
          {this.getGroupMembersDetails(true, true)}
        </div>
      </div>
    );
  }

  getActiveGroupDashboard() {
    return (
      <div>
        <div className={styles.groupDetailsContainer}>
          {this.getGroupHeaderSection(-1)}
          {this.getMetricDivs()}
          {this.getCampaignTimeInfoDiv()}
          {this.getCardsSection()}
        </div>
        <div>
          {this.getGroupMembersDetails(true)}
        </div>
      </div>
    );
  }

  getInActiveGroupDashboard() {
    const group = this.state.group;
    return (
      <div>
        <div className={styles.groupDetailsContainer}>
          {this.getGroupHeaderSection(-1)}
          <InactiveBoard
            isGroupActive={group.isActive}
            minGroupSize={group.minSize}
            maxGroupSize={group.maxSize}
            groupSize={group.size} />
          {this.getCampaignTimeInfoDiv()}
          {this.getCardsSection()}
        </div>
        <div>
          {this.getGroupMembersDetails(true)}
        </div>
      </div>
    );
  }

  getCampaignTimeInfoDiv() {
    const freezeStartTime = at(this.props.groupDetails, 'campaign.frz_start_time');
    if (new Date(freezeStartTime) > new Date()) {
      return (
        <div>
          <div className={styles.borderDiv} />
          <div className={styles.campaignTimeInfo}>
            GROUP INVITES CLOSE IN
            <Timer
              endDate={freezeStartTime} />
          </div>
        </div>
      );
    }
    return null;
  }

  render() {
    return this.getGroupDashboard();
  }
}

PreCampaign.propTypes = {
  groupDetails: React.PropTypes.object,
  setBusy: React.PropTypes.func,
  resetGroupDetails: React.PropTypes.func
};

export default PreCampaign;
