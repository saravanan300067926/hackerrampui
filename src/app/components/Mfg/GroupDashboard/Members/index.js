import React from 'react';
import styles from './members.css';
import Member from '../Member';
import at from 'v-at';
import { Helper } from '../../utils/helper';
import Notify from '../../../Notify';
import Track from '../../../../utils/track';

class Members extends React.Component {

  onInviteClick = () => {
    Track.event('msg', 'dashboard', 'invite');
  }

  getHeaderDiv() {
    if (this.props.showMembersHeader) {
      const groupSize = (this.props.members || []).length;
      return (
        <div className={styles.headerDiv}>
          <div>
            <span>TEAM </span>
            <span>{groupSize} {groupSize > 1 ? 'members' : 'member'} (max {this.props.maxGroupSize})</span>
          </div>
        </div>
      );
    }
    return null;
  }

  getInviteDiv() {
    if (this.props.inviteCode) {
      const link = `https://www.myntra.com/shoppinggroups/join/${this.props.inviteCode}`;
      const linkTxt = `${at(window, '__myx_kvpairs__.msg.join.shareMessage')}\n`;
      const linkTextEncoded = encodeURIComponent(`${linkTxt}${link}`);
      const mailContent = `mailto:?subject=Join My Shopping Group&body=${linkTextEncoded}`;
      const fbc = 'https://www.facebook.com/dialog/share?' +
      `app_id=182424375109898&quote=${linkTxt}&href=${link}&redirect_uri=https://www.myntra.com/shoppinggroups/dashboard`;
      const twContent = `https://twitter.com/intent/tweet?text=${linkTextEncoded}`;
      return (
        <div className={styles.inviteGroupBlock}>
          <h3> Invite Friends via</h3>
          <div className={styles.shareBlock}>
            <a href={fbc} onClick={this.onInviteClick}><img src="https://constant.myntassets.com/web/assets/img/11497606587468-facebook.png" /></a>
          </div>
          <div className={styles.shareBlock}>
            <a className="share-button" href={twContent} data-size="large" onClick={this.onInviteClick}>
              <img src="https://constant.myntassets.com/web/assets/img/11497606652368-twitter.png" />
            </a>
          </div>
          <div className={styles.shareBlock}> <a href={mailContent} onClick={this.onInviteClick}>
            <img src="https://constant.myntassets.com/web/assets/img/11497606626960-mail.png" /></a>
          </div>
          {
            (!Helper.isSafariBrowser() && !Helper.isIEBrowser()) ? (
              <div>
                <div className={styles.inviteText}> or </div>
                <p className={`js-copytextarea ${styles.copyLink}`}>{link}</p>
                <div className={styles.action} onClick={this.copyLink}>
                  <button>COPY LINK</button>
                </div>
              </div>
            ) : null
          }
        </div>
      );
    }
    return null;
  }

  selectElemText(elem) {
    //Create a range (a range is a like the selection but invisible)
    const range = document.createRange();

    // Select the entire contents of the element
    range.selectNodeContents(elem);

    // Don't select, just positioning caret:
    // In front
    // range.collapse();
    // Behind:
    // range.collapse(false);

    // Get the selection object
    const selection = window.getSelection();

    // Remove any current selections
    selection.removeAllRanges();

    // Make the range you have just created the visible selection
    selection.addRange(range);
  }

  copyLink = () => {
    Track.event('msg', 'dashboard', 'invite');
    const copyTextarea = document.querySelector('.js-copytextarea');
    try {
      this.selectElemText(copyTextarea);
      document.execCommand('copy');
      this.refs.notify.info({
        message: 'Copied',
        position: 'right'
      });
    } catch (err) {
      // console.error('Oops, unable to copy');
    }
  }


  render() {
    const members = this.props.members.map((member, i) =>
      <div className={styles.membersListItem} key={i}>
        <Member
          campaignState={this.props.campaignState}
          rank={member.rank}
          name={at(member, 'profile.name') || ''}
          pic={at(member, 'profile.image') || ''}
          isAdmin={at(member, 'profile.uidx') === at(this.props.groupAdmin, 'uidx')}
          me={at(member, 'profile.uidx') === Helper.getUidx()}
          totalSpending={this.props.hideSpendingInfo ? 0 : (at(member, 'props.totalSpending') || 0)}
          totalSaving={this.props.hideSpendingInfo ? 0 : (at(member, 'props.totalSaving') || 0)}
          wishlisted={at(member, 'props.wishlisted') || 0}
          likes={at(member, 'props.likes') || 0}
          boughtItemsCount={this.props.hideSpendingInfo ? 0 : (at(member, 'props.itemsBought') || 0)}
          isActive={this.props.isActive}
          isCheckedIn={this.props.checkedInUsers.hasOwnProperty(at(member, 'profile.uidx')) || false}
          offersState={this.props.offersState} />
      </div>
    );
    return (
      <div>
        {this.getHeaderDiv()}
        {this.getInviteDiv()}
        <div className={styles.membersContainer}>
          {members}
        </div>
        <Notify ref="notify" />
      </div>
    );
  }
}

Members.propTypes = {
  members: React.PropTypes.arrayOf(React.PropTypes.object),
  campaignState: React.PropTypes.string,
  inviteCode: React.PropTypes.string,
  groupAdmin: React.PropTypes.object,
  isActive: React.PropTypes.bool,
  maxGroupSize: React.PropTypes.number,
  showMembersHeader: React.PropTypes.bool,
  hideSpendingInfo: React.PropTypes.bool,
  checkedInUsers: React.PropTypes.object,
  offersState: React.PropTypes.string
};

export default Members;
