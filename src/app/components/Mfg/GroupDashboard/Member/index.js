import React from 'react';
import styles from './member.css';
import { Helper } from '../../utils/helper';
import { securify } from '../../../../utils/securify';
const secure = securify();
import at from 'v-at';

class Member extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      campaignState: this.props.campaignState,
      offersState: this.props.offersState,
      rank: this.props.rank,
      name: this.props.name,
      pic: this.props.pic,
      isAdmin: this.props.isAdmin,
      totalSaving: this.props.totalSaving,
      totalSpending: this.props.totalSpending,
      visits: this.props.visits,
      wishlisted: this.props.wishlisted,
      likes: this.props.likes,
      me: this.props.me,
      boughtItemsCount: this.props.boughtItemsCount,
      picLoadError: false
    };
  }

  onProfilePicLoadError = () => {
    this.setState({ picLoadError: true });
  }

  getAdminMarker() {
    if (this.state.isAdmin) {
      return (
        <div className={styles.admin}>
          <span>Admin</span>
        </div>
      );
    }
    return null;
  }

  getProfileNameDiv() {
    return (
      <div className={styles.name}>
        <span>{this.state.name}</span>
      </div>
    );
  }

  getInitials() {
    const name = this.state.name.split(' ');
    if (name.length > 1 && name[0] && name[1]) {
      return name[0].charAt(0).toUpperCase() + name[1].charAt(0).toUpperCase();
    }
    return name.toString().substring(0, 2).toUpperCase();
  }

  getProfilePicDiv() {
    if (this.state.pic && !this.state.picLoadError) {
      return (
        <div className={`${this.state.me ? styles.me : ''} ${styles.pic}`}>
          <div className={styles.picSubImgContainer}>
            <img src={secure(this.state.pic)} onError={this.onProfilePicLoadError} />
          </div>
        </div>
      );
    }
    const backgrounds = Helper.getBackgroundColors();
    const initialsStyle = {
      backgroundColor: backgrounds[Helper.getRandomInt(0, backgrounds.length - 1)]
    };
    return (
      <div className={`${this.state.me ? styles.me : ''} ${styles.pic}`}>
        <div className={styles.picSubContainer} style={initialsStyle}>
          <div>{this.getInitials()}</div>
        </div>
      </div>
    );
  }

  getTotalSpendingDiv() {
    if (this.props.isActive) {
      return (
        <div className={`${styles.totalSpending} ${this.state.totalSpending <= 0 ? styles.totalSpending0 : ''}`}>
          <span>Rs. {Helper.formatRupees(this.state.totalSpending, true)}</span>
        </div>
      );
    }
    return null;
  }

  getMemberStateDiv() {
    if (this.state.offersState === 'PRE_PR') {
      return this.props.isCheckedIn ? '' : (
        <div className={styles.memberState}>
          <span>{this.props.isCheckedIn ? '' : 'Inactive'}</span>
        </div>
      );
    }
    return null;
  }

  getPreCampaignMemberStats() {
    if (this.enableMemberStats()) {
      return (
        <div className={styles.row2}>
          <div>
            <span>In last<span className={styles.bold}>&nbsp;24hrs&nbsp;:&nbsp;</span>
              <span className={styles.metric}>shortlisted&nbsp;
                <span className={styles.bold}>{`${this.state.wishlisted} ${this.state.wishlisted > 1 ? 'items' : 'item'}`}</span>
              </span>
              <div className={styles.dot} />
              <span className={styles.metric}>liked&nbsp;
                <span className={styles.bold}>{`${this.state.likes} ${this.state.likes > 1 ? 'items' : 'item'}`}</span>
              </span>
            </span>
          </div>
        </div>
      );
    }
    return null;
  }

  getOnOrPostCampaignMemberStats() {
    if (this.props.isActive) {
      return (
        <div className={styles.row2}>
          <div>
            <span>
              <span className={styles.metric}>bought&nbsp;
                <span className={styles.bold}>{`${this.state.boughtItemsCount} ${this.state.boughtItemsCount > 1 ? 'items' : 'item'}`}</span>
              </span>
              <div className={styles.dot} />
              <span className={styles.metric}>saved&nbsp;
                <span className={styles.bold}>{` Rs.${this.state.totalSaving}`}</span>
              </span>
            </span>
          </div>
        </div>
      );
    }
    return null;
  }

  getPreCampaignMemberBoard() {
    return (
      <div>
        <div className={styles.row1}>
          {this.getProfilePicDiv()}
          {this.getProfileNameDiv()}
          {this.getAdminMarker()}
          {this.getMemberStateDiv()}
        </div>
        {this.getPreCampaignMemberStats()}
      </div>
    );
  }

  getOnOrPostCampaignMemberBoard() {
    return (
      <div>
        <div className={styles.row1}>
          {this.getProfilePicDiv()}
          {this.getProfileNameDiv()}
          {this.getAdminMarker()}
          {this.getTotalSpendingDiv()}
        </div>
        {this.getOnOrPostCampaignMemberStats()}
      </div>
    );
  }

  getMemberBoard() {
    if (this.state.campaignState === 'pre') {
      return this.getPreCampaignMemberBoard();
    }
    if (this.state.campaignState === 'onpost') {
      return this.getOnOrPostCampaignMemberBoard();
    }
    return null;
  }

  enableMemberStats() {
    return at(window, '__myx_kvpairs__.msg.dashboard.enableMemberStats') || false;
  }

  render() {
    return (
      <div className={styles.memberContainer}>
        {this.getMemberBoard()}
      </div>
    );
  }
}

Member.propTypes = {
  campaignState: React.PropTypes.string,
  offersState: React.PropTypes.string,
  rank: React.PropTypes.number,
  name: React.PropTypes.string,
  pic: React.PropTypes.string,
  isAdmin: React.PropTypes.bool,
  totalSaving: React.PropTypes.number,
  totalSpending: React.PropTypes.number,
  visits: React.PropTypes.number,
  wishlisted: React.PropTypes.number,
  likes: React.PropTypes.number,
  isActive: React.PropTypes.bool,
  me: React.PropTypes.bool,
  boughtItemsCount: React.PropTypes.number,
  isCheckedIn: React.PropTypes.bool
};

export default Member;
