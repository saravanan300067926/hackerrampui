import React from 'react';
import styles from './joinlogin.css';
import { Helper } from '../../utils/helper';

const JoinLogin = () =>
  <div className={Helper.isMobile() ? styles.mobile : styles.desktop}>
    <div className={styles.container}>
      <div className={styles.heading}>PLEASE LOG IN</div>
      <div className={styles.info}>Login to join your shopping group.</div>
      <div className={`myntraweb-sprite ${styles.icon}`}></div>
      <div>
        <a href={`/login?referer=${Helper.isBrowser() ? location.href : ''}`} className={styles.button}>LOGIN</a>
      </div>
    </div>
  </div>;

export default JoinLogin;
