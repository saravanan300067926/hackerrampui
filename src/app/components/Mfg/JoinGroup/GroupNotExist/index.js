import React from 'react';
import styles from './groupnotexist.css';
import { Helper } from '../../utils/helper';

const GroupNotExist = () => (
  <div className={styles.mainContainer}>
    <div className={styles.mainInfoHeading}>
      Sorry!
    </div>
    <div className={styles.mainInfoHeading}>
      You can't join this group.
    </div>
    <div className={styles.description}>
      This group doesn't exist.
    </div>
    <div className={styles.action} onClick={() => Helper.redirectTo('/shoppinggroups/leaderboard')}>
      <button>GOT IT</button>
    </div>
  </div>
);

export default GroupNotExist;
