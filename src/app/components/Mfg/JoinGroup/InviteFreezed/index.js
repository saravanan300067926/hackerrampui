import React from 'react';
import styles from './invitefreezed.css';
import { Helper } from '../../utils/helper';

const InviteFreezed = () => (
  <div className={styles.mainContainer}>
    <div className={styles.mainInfoHeading}>
      Sorry!
    </div>
    <div className={styles.mainInfoHeading}>
      Group invites are closed now.
    </div>
    <div className={styles.description}>
      The groups are now getting ready for India’s biggest fashion sale - EORS and no more people can get in!
    </div>
    <div className={styles.description}>
      You can still create a wishlist and get ready to shop.
    </div>
    <div className={styles.action} onClick={() => Helper.redirectTo(Helper.getSecureRoot())}>
      <button>CREATE WISHLIST</button>
    </div>
  </div>
);

export default InviteFreezed;
