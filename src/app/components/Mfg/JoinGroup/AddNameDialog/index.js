import React from 'react';
import styles from './addnamedialog.css';
import Dialog from '../../Common/Dialog';
import Track from '../../../../utils/track';

class AddNameDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      labelPosition: 'down',
      value: '',
      valid: true,
      errorMessage: 'Enter valid name'
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      show: props.show
    });
  }

  onContinue() {
    Track.event('msg', 'member join', 'add name continue');
    this.setState({ valid: this.state.value && this.state.value.trim() });
    if (this.state.valid) {
      this.props.continueHandler(this.state.value);
    }
  }

  getAddNameDialogContent() {
    return (
      <div className={styles.addNameDialogContent}>
        <div className={styles.heading}>Add Name</div>
        <div className={styles.row}>
          <input
            id="addNameInput"
            className={styles.input}
            onFocus={() => this.floatLabel('float')}
            onBlur={() => this.floatLabel('down')}
            onChange={(event) => this.handleChange(event)}
            maxLength={20}
            value={this.state.value}
            autoComplete={'off'} />
          <label
            htmlFor="addNameInput"
            className={this.state.labelPosition === 'down' ? styles.label : styles.labelFloat}>
              YOUR NAME*
          </label>
          <div className={this.state.valid ? styles.error : styles.errorShow} >
            {this.state.errorMessage}
          </div>
        </div>
        <div className={styles.action} onClick={() => this.onContinue()}>
          <button>CONTINUE</button>
        </div>
        <div className={styles.cancellink} onClick={() => this.props.hideHandler()}>
          CANCEL
        </div>
      </div>
    );
  }

  handleChange(event) {
    const value = event.target.value || '';
    this.setState({ value, valid: value && value.trim() });
  }

  floatLabel(labelPosition) {
    if (labelPosition === 'down' && this.state.value !== '') {
      return;
    }
    Track.event('msg', 'member join', 'edit name');
    this.setState({ labelPosition });
  }

  render() {
    return (
      <Dialog
        show={this.props.show}
        dialogContentGetter={() => this.getAddNameDialogContent()}
        backdropClickHandler={this.props.hideHandler}
        dialogHeight={300} />
    );
  }
}

AddNameDialog.propTypes = {
  show: React.PropTypes.bool,
  hideHandler: React.PropTypes.func,
  continueHandler: React.PropTypes.func
};

export default AddNameDialog;
