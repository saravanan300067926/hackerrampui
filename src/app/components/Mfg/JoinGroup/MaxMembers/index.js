import React from 'react';
import styles from './maxmembers.css';
import { Helper } from '../../utils/helper';
import AppOnlyDialog from '../../Common/AppOnlyDialog';
import MFGMadalytics from '../../utils/mfgMadalytics';

class MaxMembers extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showDialog: false
    };
  }

  getCreateGroupAction() {
    return (
      <div className={styles.action} onClick={() => this.showAppOnlyDialog()}>
        <button>CREATE A GROUP</button>
      </div>
    );
  }

  showAppOnlyDialog() {
    MFGMadalytics.sendCreateGroupInitiateEvent('JoinGroup');
    this.setState({ showDialog: true });
  }

  hideAppOnlyDialog() {
    this.setState({ showDialog: false });
  }

  render() {
    return (
      <div className={styles.mainContainer}>
        <div className={styles.mainInfoHeading}>
          Sorry!
        </div>
        <div className={styles.mainInfoHeading}>
          You can't join this group.
        </div>
        <div className={styles.description}>
          '{this.props.joiningGroup}' has already reached the maximum limit of {this.props.maxMembersCount}.
        </div>
        <div className={styles.description}>
          However, you can create your own shopping group & give '{this.props.joiningGroup}' a competition.
        </div>
        {this.getCreateGroupAction()}
        <div className={styles.whatIsMfgLink} onClick={() => Helper.redirectTo(Helper.getWhatIsMfgLink())}>
          What are Myntra Shopping Groups?
        </div>
        <AppOnlyDialog
          show={this.state.showDialog}
          hideHandler={() => this.hideAppOnlyDialog()} />
      </div>
    );
  }
}

MaxMembers.propTypes = {
  joiningGroup: React.PropTypes.string,
  maxMembersCount: React.PropTypes.number
};

export default MaxMembers;
