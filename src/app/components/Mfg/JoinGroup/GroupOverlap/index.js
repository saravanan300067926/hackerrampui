import React from 'react';
import styles from './groupoverlap.css';
import { Helper } from '../../utils/helper';

const GroupOverlap = (props) => (
  <div className={styles.mainContainer}>
    <div className={styles.mainInfoHeading}>
      Sorry!
    </div>
    <div className={styles.mainInfoHeading}>
      You can't join this group.
    </div>
    <div className={styles.description}>
      You are a member of <span>{props.currentGroup}</span>.
    </div>
    <div className={styles.description}>
      If you wish to join <span>{props.joiningGroup}</span> group, you will have to leave <span>{props.currentGroup}</span> and click on this invite again.
    </div>
    <div className={styles.action} onClick={() => Helper.redirectTo('/shoppinggroups/dashboard')}>
      <button>GOT IT</button>
    </div>
    <div className={styles.myGroup} onClick={() => Helper.redirectTo('/shoppinggroups/dashboard')}>
      <span>Go to <span>'{props.currentGroup}'</span> page</span>
    </div>
  </div>
);

GroupOverlap.propTypes = {
  currentGroup: React.PropTypes.string,
  joiningGroup: React.PropTypes.string
};

export default GroupOverlap;
