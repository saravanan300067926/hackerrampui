import React from 'react';
import styles from './joingroup.css';
import MaxMembers from './MaxMembers';
import GroupOverlap from './GroupOverlap';
import JoinLogin from './JoinLogin';
import InviteFreezed from './InviteFreezed';
import GroupNotExist from './GroupNotExist';
import GroupHeader from '../GroupDashboard/GroupHeader';
import { prepareForJoinGroup, joinGroup } from '../utils/actions';
import { Helper } from '../utils/helper';
import at from 'v-at';
import AppOnlyDialog from '../Common/AppOnlyDialog';
import AddNameDialog from './AddNameDialog';
import MFGMadalytics from '../utils/mfgMadalytics';
import Track from '../../../utils/track';

class JoinGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: '',
      busy: true,
      dataFetchError: false,
      joiningGroup: {
        id: '',
        name: '',
        icon: '',
        maxSize: 0,
        currentSize: 0,
        campaignStatus: '',
        frozen: ''
      },
      currentGroup: {
        id: '',
        name: ''
      },
      showAppOnlyDialog: false,
      showAddNameDialog: false
    };
  }

  componentWillMount() {
    if (Helper.isLoggedIn()) {
      this.prepareJoinGroup();
    }
  }

  componentDidMount() {
    document.body.scrollTop = 0;
  }

  onWhatIsMfgLinkClick = () => {
    Track.event('msg', 'member join', 'what msg click');
    Helper.redirectTo(Helper.getWhatIsMfgLink());
  }

  getCreateGroupAction() {
    if (!Helper.isMobile() || Helper.isiPad()) {
      return null;
    }
    return (
      <div className={styles.next} onClick={() => this.showAppOnlyDialog()}>
        I want to create a new group
      </div>
    );
  }

  getJoinGroupScreen() {
    if (!Helper.isLoggedIn()) {
      return (
        <JoinLogin />
      );
    }
    if (this.state.busy) {
      return (
        <div className={styles.loaderDiv}>
          <div className={styles.spinner}></div>
        </div>
      );
    }
    if (this.state.dataFetchError) {
      return (
        <div className={styles.errorDiv}>
          <span>
            {this.state.errorMessage || Helper.getDefaultErrorMessage()}
          </span>
        </div>
      );
    }
    if (!this.state.joiningGroup.id) {
      const groupNotExist = <GroupNotExist />;
      return this.wrapInContainer(groupNotExist);
    }
    if (this.state.currentGroup.id === this.state.joiningGroup.id) {
      window.location.href = '/shoppinggroups/dashboard';
      return (
        <div className={styles.loaderDiv}>
          <div className={styles.spinner}></div>
        </div>
      );
    }
    if (this.state.joiningGroup.frozen) {
      const inviteFreezed = <InviteFreezed />;
      return this.wrapInContainer(inviteFreezed);
    }
    if (this.state.currentGroup.id) {
      const groupOverlap = (
        <GroupOverlap
          currentGroup={this.state.currentGroup.name}
          joiningGroup={this.state.joiningGroup.name} />
      );
      return this.wrapInContainer(groupOverlap);
    }
    if (this.state.joiningGroup.currentSize >= this.state.joiningGroup.maxSize) {
      const maxMembers = (
        <MaxMembers
          joiningGroup={this.state.joiningGroup.name}
          maxMembersCount={this.state.joiningGroup.maxSize} />
      );
      return this.wrapInContainer(maxMembers);
    }
    const joinScreen = (
      <div className={styles.mainContainer}>
        <div className={styles.mainInfoHeading}>
          You are invited!
        </div>
        <div className={styles.description}>
          You are invited by <span>{this.state.joiningGroup.name}</span> member to join their shopping group.
        </div>
        <GroupHeader
          name={this.state.joiningGroup.name}
          icon={this.state.joiningGroup.icon}
          membersCount={this.state.joiningGroup.currentSize}
          checkInCount={-1} />
        <div className={styles.action} onClick={() => this.joinShoppingGroup()}>
          <button>JOIN GROUP</button>
        </div>
        {this.getCreateGroupAction()}
        <div className={`${styles.next} ${styles.whatIsMfgLink}`} onClick={this.onWhatIsMfgLinkClick}>
          What are Myntra Shopping Groups?
        </div>
        <AppOnlyDialog
          show={this.state.showAppOnlyDialog}
          hideHandler={() => this.hideAppOnlyDialog()} />
        <AddNameDialog
          show={this.state.showAddNameDialog}
          hideHandler={() => this.hideAddNameDialog()}
          continueHandler={(name) => this.joinShoppingGroup(name)} />
      </div>
    );
    return this.wrapInContainer(joinScreen);
  }

  wrapInContainer(joinGroupUi) {
    const joinGroupBaseContainerStyle = {
      minHeight: Helper.isMobile() && !Helper.isiPad() ? `${Math.max(window.innerHeight - 50, 600)}px` : '600px'
    };
    return (
      <div className={styles.baseContainer}>
        <div className={styles.joinGroupBaseContainer} style={joinGroupBaseContainerStyle}>
          {joinGroupUi}
        </div>
      </div>
    );
  }

  showAppOnlyDialog() {
    MFGMadalytics.sendCreateGroupInitiateEvent('JoinGroup');
    this.setState({ showAppOnlyDialog: true });
  }

  hideAppOnlyDialog() {
    this.setState({ showAppOnlyDialog: false });
  }

  showAddNameDialog() {
    this.setState({ showAddNameDialog: true });
  }

  hideAddNameDialog() {
    this.setState({ showAddNameDialog: false });
  }

  joinGroupSuccess(data) {
    if (data) {
      window.location.href = '/shoppinggroups/dashboard';
      MFGMadalytics.sendCreateGroupInitiateEvent(this.state.joiningGroup.id, this.state.joiningGroup.name);
    } else {
      this.joinGroupError();
    }
  }

  joinGroupError(statusCode) {
    if (statusCode === '403') {
      this.redirectToLogin();
      return;
    }
    this.setState({
      busy: false,
      dataFetchError: true,
      errorMessage: 'Could not join group. Please refresh and try again',
      showAddNameDialog: false
    });
  }

  joinShoppingGroup(name) {
    Track.event('msg', 'member join', 'join');
    name = name || '';
    name = name.trim();
    if (name) {
      const data = {
        inviteCode: at(this.props, 'params.invitecode'),
        userName: name
      };
      joinGroup(data, { 'X-CSRF-TOKEN': Helper.getXsrfToken() }, this.joinGroupSuccess.bind(this), this.joinGroupError.bind(this));
    } else {
      const firstName = at(window, '__myx_session__.userfirstname');
      const lastName = at(window, '__myx_session__.userlastname');
      if (firstName || lastName) {
        const data = {
          inviteCode: at(this.props, 'params.invitecode')
        };
        joinGroup(data, { 'X-CSRF-TOKEN': Helper.getXsrfToken() }, this.joinGroupSuccess.bind(this), this.joinGroupError.bind(this));
      } else {
        Track.event('msg', 'member join', 'add name');
        this.showAddNameDialog();
      }
    }
  }

  redirectToLogin() {
    window.location.href = `${Helper.getSecureRoot()}login?referer=${window.location.href}`;
  }

  prepareJoinGroup() {
    prepareForJoinGroup(at(this.props, 'params.invitecode'), this.prepareJoinSuccess.bind(this), this.prepareJoinError.bind(this));
  }

  prepareJoinSuccess(currentGroup, joiningGroup) {
    this.setState({
      busy: false,
      joiningGroup,
      currentGroup
    });
  }

  prepareJoinError(statusCode, statusMessage, errorMsg) {
    if (statusCode === '403') {
      this.redirectToLogin();
      return;
    }
    this.setState({
      busy: false,
      dataFetchError: true,
      errorMessage: errorMsg || ''
    });
  }

  render() {
    return this.getJoinGroupScreen();
  }
}

export default JoinGroup;
