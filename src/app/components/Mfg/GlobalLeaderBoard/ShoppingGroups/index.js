import React from 'react';
import styles from './shoppinggroups.css';
import ShoppingGroup from '../ShoppingGroup';
import at from 'v-at';

const ShoppingGroups = (props) => {
  const shoppingGroups = props.groups.map((group, i) =>
    <ShoppingGroup
      key={i}
      highlight={false}
      rank={group.rank}
      iconUrl={group.groupImage}
      name={group.groupName}
      membersCount={group.memberCount || 0}
      totalShoppedAmount={at(group, 'props.totalSpent') || 0}
      totalSavedAmount={at(group, 'props.totalSaved') || 0} />
  );
  return (
    <div className={styles.groupsContainer}>
      {shoppingGroups}
    </div>
  );
};

ShoppingGroups.propTypes = {
  groups: React.PropTypes.arrayOf(React.PropTypes.object)
};

export default ShoppingGroups;
