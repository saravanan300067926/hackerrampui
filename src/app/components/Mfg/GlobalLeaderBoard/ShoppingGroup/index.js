import React from 'react';
import styles from './shoppinggroup.css';
import { Helper } from '../../utils/helper';

class ShoppingGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getState(this.props);
  }

  componentWillReceiveProps(props) {
    this.setState(this.getState(props));
  }

  onGroupClick() {
    if (this.state.highlight) {
      window.location.href = '/shoppinggroups/dashboard';
    }
  }

  onIconLoadError() {
    this.setState({ iconLoadError: true });
  }

  getState(props) {
    return {
      rank: props.rank,
      iconUrl: props.iconUrl,
      name: props.name,
      membersCount: props.membersCount,
      totalShoppedAmount: props.totalShoppedAmount,
      totalSavedAmount: props.totalSavedAmount,
      highlight: props.highlight,
      iconLoadError: false
    };
  }

  getBorderDiv() {
    return !this.props.highlight ? <div className={styles.borderDiv}></div> : null;
  }

  getIconDiv() {
    if (this.state.iconUrl && !this.state.iconLoadError) {
      return (
        <div className={styles.icon}>
          <img src={this.state.iconUrl} onError={() => this.onIconLoadError()}></img>
        </div>
      );
    }
    const backgrounds = Helper.getBackgroundColors();
    const initialsStyle = {
      backgroundColor: backgrounds[Helper.getRandomInt(0, backgrounds.length - 1)]
    };
    return (
      <div className={styles.icon} style={initialsStyle}>
      </div>
    );
  }

  render() {
    return (
      <div className={`${styles.container} ${this.state.highlight ? styles.highlight : ''}`} onClick={() => this.onGroupClick()}>
        <div className={styles.subcontainer}>
          <div className={styles.rank}>
            {this.state.rank || ''}
          </div>
          {this.getIconDiv()}
          <div className={styles.name}>
            {this.state.name}
          </div>
          <div className={styles.members}>
            {this.state.membersCount} {this.state.membersCount > 1 ? 'members' : 'member'}
          </div>
          <div className={styles.totalShoppedAmount}>
            Rs. {Helper.formatRupees(this.state.totalShoppedAmount, true)}
          </div>
          <div className={styles.totalSavedAmount}>
            Saved Rs. {Helper.formatRupees(this.state.totalSavedAmount, true)}
          </div>
          {this.getBorderDiv()}
        </div>
      </div>
    );
  }
}

ShoppingGroup.propTypes = {
  highlight: React.PropTypes.bool,
  rank: React.PropTypes.number,
  name: React.PropTypes.string,
  iconUrl: React.PropTypes.string,
  membersCount: React.PropTypes.number,
  totalShoppedAmount: React.PropTypes.number,
  totalSavedAmount: React.PropTypes.number
};

export default ShoppingGroup;
