import React from 'react';
import styles from './globalleaderboard.css';
import { Link } from 'react-router';
import ShoppingGroup from './ShoppingGroup';
import ShoppingGroups from './ShoppingGroups';
import { getGlobalLeaderBoardData } from '../utils/actions';
import at from 'v-at';
import { Helper } from '../utils/helper';
import MFGMadalytics from '../utils/mfgMadalytics';
import InfiniteScroll from 'react-infinite-scroller';
import AppOnlyDialog from '../Common/AppOnlyDialog';
import MultiLineText from '../Common/MultiLineText';

class GlobalLeaderBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userGroup: null,
      shoppingGroups: [],
      busy: true,
      dataFetchError: false,
      fetchDataOptions: {
        limit: 20,
        offset: 0,
        offersData: false
      },
      dataFetchCompleted: false,
      groupsFrozen: false,
      showAppOnlyDialog: false,
      groupsTotalCount: 0,
      campaignState: '',
      enabled: at(window, '__myx_kvpairs__.msg.leaderboard.enabled')
    };
  }

  componentWillMount() {
    if (this.state.enabled) {
      getGlobalLeaderBoardData(this.state.fetchDataOptions, this.onDataSuccess.bind(this), this.onDataError.bind(this));
    }
  }

  componentDidMount() {
    document.body.scrollTop = 0;
    Helper.setDocumentTitle('Myntra Shopping Group - Global Leaderboard');
  }

  onDataSuccess(groupLeaderBoardData) {
    if (!groupLeaderBoardData || Object.keys(groupLeaderBoardData).length === 0) {
      this.setState({
        busy: false,
        dataFetchError: true
      });
      return;
    }
    const standings = at(groupLeaderBoardData, 'standings') || [];
    const allShoppingGroups = [...this.state.shoppingGroups, ...standings];
    const totalCount = at(groupLeaderBoardData, 'totalCount');
    if (this.state.fetchDataOptions.offset === 0) {
      //Log an event only for first load success
      MFGMadalytics.sendGlobalLeaderBoardLoadEvent();
    }
    this.setState({
      busy: false,
      userGroup: at(groupLeaderBoardData, 'myGroup'),
      shoppingGroups: allShoppingGroups,
      fetchDataOptions: { offersData: false,
        limit: this.state.fetchDataOptions.limit, offset: (this.state.fetchDataOptions.offset + this.state.fetchDataOptions.limit) },
      dataFetchError: false,
      dataFetchCompleted: (allShoppingGroups.length >= totalCount),
      groupsTotalCount: totalCount,
      groupsFrozen: at(groupLeaderBoardData, 'isFrozen'),
      campaignState: at(groupLeaderBoardData, 'state')
    });
  }

  onDataError(statusCode) {
    if (statusCode === '403') {
      this.redirectToLogin();
      return;
    }
    this.setState({
      busy: false,
      dataFetchError: true
    });
  }

  getBoardsData() {
    if (!this.state.busy) {
      this.setState({
        busy: true
      });
      getGlobalLeaderBoardData(this.state.fetchDataOptions, this.onDataSuccess.bind(this), this.onDataError.bind(this));
    }
  }

  getUserGroupPositionBoard() {
    if (this.state.userGroup) {
      return (
        <div>
          <div className={styles.groupPosHeader}>
            YOUR GROUP POSITION
          </div>
          <ShoppingGroup
            highlight={true}
            rank={this.state.userGroup.rank}
            name={this.state.userGroup.groupName}
            iconUrl={this.state.userGroup.groupImage}
            membersCount={this.state.userGroup.memberCount || 0}
            totalShoppedAmount={at(this.state.userGroup, 'props.totalSpent') || 0}
            totalSavedAmount={at(this.state.userGroup, 'props.totalSaved') || 0} />
        </div>
      );
    }
    return null;
  }

  getIncrementedGroupsCount() {
    const product = this.state.groupsTotalCount * (at(window, '__myx_kvpairs__.msg.leaderboard.multiplier') || 1);
    return isNaN(product) ? this.state.groupsTotalCount : product;
  }

  getShoppingGroupsBoard() {
    const groupsCount = this.getIncrementedGroupsCount();
    if (this.state.shoppingGroups) {
      const loader = <div className={styles.loaderDiv}><div className={styles.spinner}></div></div>;
      return (
        <div>
          <div className={styles.shoppingGroupsHeader}>
            SHOPPING GROUP LEADERBOARD ({groupsCount} {groupsCount > 1 ? 'Groups' : 'Group'})
          </div>
          <InfiniteScroll
            pageStart={0}
            loadMore={() => { this.getBoardsData(); }}
            hasMore={!this.state.dataFetchError && !this.state.dataFetchCompleted}
            loader={loader}>
            <ShoppingGroups
              groups={this.state.shoppingGroups} />
          </InfiniteScroll>
        </div>
      );
    }
    return null;
  }

  getBoards() {
    if (this.state.busy && this.state.shoppingGroups.length === 0) {
      return (
        <div className={styles.loaderDiv}>
          <div className={styles.spinner}></div>
        </div>
      );
    } else if (this.state.dataFetchError) {
      return (
        <div className={styles.errorDiv}>
          <span>
            {Helper.getDefaultErrorMessage()}
          </span>
        </div>
      );
    }
    return (
      <div>
        {this.getUserGroupPositionBoard()}
        {this.getShoppingGroupsBoard()}
      </div>
    );
  }

  getMfgImage() {
    return (
      <div className={styles.heading}>
        <img src={at(window, '__myx_kvpairs__.msg.logoUrl') || ''} alt="Myntra Shopping Groups" />
      </div>
    );
  }

  getViewRewardsLink() {
    return (
      <div className={styles.viewRewardsLink}>
        <Link to={Helper.getViewRewardsLink()}>VIEW MORE REWARDS</Link>
      </div>
    );
  }

  getWhatIsMfgLink() {
    return (
      <div className={styles.whatIsMfgLink}>
        <div onClick={() => Helper.redirectTo(Helper.getWhatIsMfgLink())}>What are Myntra Shopping Groups?</div>
      </div>
    );
  }

  getCreateGroupAction() {
    if (this.state.userGroup || this.state.groupsFrozen || at(window, '__myx_kvpairs__.msg.leaderboard.disableCreateGroupAction')) {
      return null;
    }
    return (
      <div className={styles.action} onClick={() => this.showAppOnlyDialog()}>
        <button>CREATE A GROUP</button>
      </div>
    );
  }

  getFrozenDashboard(text) {
    return (
      <div className={styles.mfgBaseContainer}>
        <div className={styles.mfgContentContainer}>
          <div className={styles.frozenContainer}>
            {this.getMfgImage()}
            <div className={styles.mfginfo}>
              {text}
            </div>
            <div className={styles.action} onClick={() => Helper.redirectTo('/')}>
              <button>CONTINUE SHOPPING</button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  getDisabledDashboard() {
    return (
      <div className={`${styles.mfgBaseContainer} ${styles.disabled}`}>
        <div className={styles.mfgContentContainer}>
          <div className={styles.container}>
            {this.getMfgImage()}
            <div className={styles.disabledMfgtitle}>
              <MultiLineText
                text={at(window, '__myx_kvpairs__.msg.leaderboard.disabledTitle')} />
            </div>
            <div className={styles.disabledMfginfo}>
              <MultiLineText
                text={at(window, '__myx_kvpairs__.msg.leaderboard.disabledText')} />
            </div>
            {this.getCreateGroupAction()}
            {this.getWhatIsMfgLink()}
          </div>
        </div>
        <AppOnlyDialog
          show={this.state.showAppOnlyDialog}
          hideHandler={() => this.hideAppOnlyDialog()} />
      </div>
    );
  }

  showAppOnlyDialog() {
    MFGMadalytics.sendCreateGroupInitiateEvent('GlobalLeaderBoard');
    this.setState({ showAppOnlyDialog: true });
  }

  hideAppOnlyDialog() {
    this.setState({ showAppOnlyDialog: false });
  }

  redirectToLogin() {
    window.location.href = `${Helper.getSecureRoot()}login?referer=${window.location.href}`;
  }

  render() {
    if (!this.state.enabled) {
      return this.getDisabledDashboard();
    } else if (this.state.campaignState === 'CLOSED' && !this.state.groupsFrozen) {
      return this.getFrozenDashboard('This event is over. Wait for the next exciting event.');
    }
    return (
      <div className={styles.mfgBaseContainer}>
        <div className={styles.mfgContentContainer}>
          <div className={styles.container}>
            {this.getMfgImage()}
            <div className={styles.mfginfo}>
              <MultiLineText
                text={at(window, '__myx_kvpairs__.msg.leaderboard.description')} />
            </div>
            {this.getViewRewardsLink()}
            {this.getCreateGroupAction()}
            {this.getWhatIsMfgLink()}
          </div>
          {this.getBoards()}
        </div>
        <AppOnlyDialog
          show={this.state.showAppOnlyDialog}
          hideHandler={() => this.hideAppOnlyDialog()} />
      </div>
    );
  }
}

export default GlobalLeaderBoard;
