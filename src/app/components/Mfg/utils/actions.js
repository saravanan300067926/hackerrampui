import Client from '../../../services';
import config from '../../../config';
import at from 'v-at';

export const getGlobalLeaderBoardData = (options, successCallback, errorCallback) => {
  options = options || {};
  options.limit = options.limit || 100;
  options.offset = options.offset || 0;
  options.offersData = options.offersData || false;
  Client.get(`/web/mfgapi/leaderboard/global?limit=${options.limit}&offset=${options.offset}&offersData=${options.offersData}`).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
    } else if(response && successCallback) {
      successCallback(at(response, 'body.leaderBoardData'), at(response, 'body.offersData'));
    }
  });
}

export const getGroupLeaderBoardData = (successCallback, errorCallback) => {
  Client.get(`/web/mfgapi/leaderboard/group`).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
    } else if(response && successCallback) {
      successCallback(at(response, 'body.dashBoardData'));
    }
  });
}

export const prepareForJoinGroup = (invitecode, successCallback, errorCallback) => {
  Client.get(`/web/mfgapi/preparejoin?invitecode=${invitecode}`).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
    } else if(response && successCallback) {
      successCallback(at(response, 'body.myGroup'), at(response, 'body.joiningGroup'));
    }
  });
}

export const joinGroup = (data, headers, successCallback, errorCallback) => {
  Client.put(`/web/mfgapi/joingroup`, data, headers).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
    } else if(response && successCallback) {
      successCallback(at(response, 'body'));
    }
  });
}

export const checkinGroup = (data, headers, successCallback, errorCallback) => {
  Client.put(`/web/mfgapi/checkin`, data, headers).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
    } else if(response && successCallback) {
      successCallback(at(response, 'body'));
    }
  });
}

export const getRewardsPageData = (successCallback, errorCallback) => {
  Client.get(`/web/mfgapi/rewards/data`).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      console.error('error in checkUserGroup action', err);
      errorCallback(at(response, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
    } else if(response && successCallback) {
      successCallback(at(response, 'body'));
    }
  });
}
