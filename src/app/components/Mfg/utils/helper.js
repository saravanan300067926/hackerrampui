import at from 'v-at';
import { isBrowser } from  '../../../utils';

export const Helper = {
  isMobile: function() {
    if (isBrowser()) {
      return (at(window, '__myx_deviceType__') === 'mobile' || at(window, '__myx_deviceData__.isMobile'));
    } return false;
  },
  isApp: function() {
    if (isBrowser()) {
      const deviceChannel = at(window, '__myx_deviceData__.deviceChannel') || '';
      return deviceChannel.toLowerCase() === 'mobile_app';
    } return false;
  },
  getRootUrl: function() {
    if (isBrowser()) {
      return at(window, '__myx_env__.hosts.root') || 'http://www.myntra.com/';
    } return false;
  },
  getSecureRoot: function() {
    if (isBrowser()) {
      return at(window, '__myx_env__.hosts.secureRoot') || 'https://secure.myntra.com/';
    } return false;
  },
  isLoggedIn: function() {
    if (isBrowser()) {
      return at(window, '__myx_session__.isLoggedIn') || false;
    } return false;
  },
  getXsrfToken: function() {
    if (isBrowser()) {
      return at(window, '__myx_session__.USER_TOKEN') || '';
    } return '';
  },
  getUidx: function() {
    if (isBrowser()) {
      return at(window, '__myx_session__.login') || '';
    } return '';
  },
  navigateTo: function(path, isSecure) {
    if (isBrowser()) {
      path = path || '';
      window.location.href = isSecure ? `${this.getSecureRoot()}${path}` : `${this.getRootUrl()}${path}`;
    }
  },
  getDefaultErrorMessage: function() {
    return 'Something went wrong. Please try again later.';
  },
  numDifferentiation: (val, decimal = 0) => {
    if (val >= 10000000) val = (val / 10000000).toFixed(decimal) + 'Cr';
    else if (val >= 100000) val = (val / 100000).toFixed(decimal) + 'L';
    else if (val >= 1000) val = (val / 1000).toFixed(decimal) + 'k';
    return val;
  },
  formatRupees: function (number, round) {
    if (number < 0 || !number) {
      return 0;
    }
    const _r = typeof round === 'undefined' ? true : round;
    let num = number < 0 ? 0.00 : number;
    num = (_r ? Math.round(num):parseFloat(num, 10).toFixed(2)).toString();
    let one = (num == 1) ? true : false,
        arr = [],
        digits,
        dp = '00',
        curr;
    if(!_r){
        let _dec = num.split('.');
         if(_dec.length > 1){
             num = _dec[0];
             dp = _dec[1];
         }
    }
    while (num) {
        digits = digits ? 2 : 3;
        arr.push(num.slice(-digits));
        num = num.slice(0, -digits);
    }
    curr = arr.reverse().join(',');
    return _r ? curr : curr+'.'+dp;
  },
  isiPad: function() {
    if (isBrowser()) {
      const iPadUA = window && window.navigator ? window.navigator.userAgent : null;
      return iPadUA && iPadUA.match(/iPad/i) != null;
    } return false;
  },
  redirectTo: function(newUrl) {
    if (newUrl) {
      window.location.href = newUrl;
    }
  },

  isAchieved: function(status) {
    if (status === 'ACTIVE' || status === 'PROMISED') {
      return true;
    } else {
      return false;
    }
  },
  getViewRewardsLink: function() {
    return at(window, '__myx_kvpairs__.msg.dashboard.viewRewardsUrl') || '/shoppinggroups/rewards';
  },
  getWhatIsMfgLink: function() {
    return at(window, '__myx_kvpairs__.msg.introUrl') || '/growth/msg';
  },
  setDocumentTitle: function(title) {
    document.title = title || 'Myntra Shopping Group';
  },
  getBackgroundColors: function() {
    return ['#EAB1CC', '#D798BB', '#EBBAA3', '#DD93D6', '#EDA684'];
  },
  getRandomInt: function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },
  getNoRewardsErrorMessage() {
    return 'Something went wrong. Please try after some time.';
  },
  isBrowser() {
    return isBrowser();
  },
  isIEBrowser() {
    const ua = window.navigator.userAgent;
    const msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      return true;
    }
    const trident = ua.indexOf('Trident/');
    if (trident > 0) {
      return true;
    }
    const edge = ua.indexOf('Edge/');
    if (edge > 0) {
      return true;
    }
    return false;
  },
  isSafariBrowser() {
    return window.safari ? true : false;
  }
}
