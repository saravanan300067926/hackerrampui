import TrackMadalytics from '../../../utils/trackMadalytics';

const MFGMadalytics = {
  sendCreateGroupInitiateEvent: function(screenType) {
  	TrackMadalytics.sendEvent('createGroupInitiateWeb', screenType, {
  	  "entity_type": "shopping_group"
  	});
  },
  sendGroupDashboardLoadEvent: function() {
  	TrackMadalytics.sendEvent('ScreenLoad', 'GlobalDashboard', {});
  },
  sendGlobalLeaderBoardLoadEvent: function(groupId, groupName) {
    TrackMadalytics.sendEvent('ScreenLoad', 'GroupDashboard', {
      "entity_type": "shopping_group",
      "entity_name": groupName,
      "entity_id": groupId
    });
  },
  sendJoinGroupEvent: function(groupId, groupName) {
  	TrackMadalytics.sendEvent('joinGroup', 'JoinGroupInviteSuccess', {
  	  "entity_type": "shopping_group",
  	  "entity_name": groupName,
  	  "entity_id": groupId
  	});
  }
};

export default MFGMadalytics;