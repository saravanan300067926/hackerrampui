export default class OutsideClickListener {
  constructor(container, item) {
    this.container = container;
    this.item = item;
    this.listenerAttached = false;
  }

  attach(listener) {
    if (! this.listenerAttached) {
      this.listener = event => {
        if (! this.item.contains(event.target))
          listener();
      };
      this.container.addEventListener('click', this.listener);
      this.listenerAttached = true;
    }
  }

  detach() {
    if (this.listenerAttached) {
      this.container.removeEventListener('click', this.listener);
      this.listenerAttached = false;
    }
  }
}

