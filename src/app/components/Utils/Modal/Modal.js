import React, { PropTypes } from 'react';
import styles from './Modal.css';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
    this.onOverlayClick = this.onOverlayClick.bind(this);
    this.onDialogClick = this.onDialogClick.bind(this);
  }

  componentDidMount() {
    if (this.props.onClose) {
      window.addEventListener('keydown', this.listenKeyboard.bind(this), true);
    }
  }

  componentWillUnmount() {
    if (this.props.onClose) {
      window.removeEventListener('keydown', this.listenKeyboard.bind(this), true);
    }
  }

  onOverlayClick(e) {
    if (this.props.onClose) {
      this.props.onClose(e);
    }
  }

  onDialogClick(e) {
    e.stopPropagation();
    if (this.props.handleDialogClick) {
      this.props.handleDialogClick(e);
    }
  }

  listenKeyboard(event) {
    if (event.key === 'Escape' || event.keyCode === 27) {
      this.props.onClose();
    }
  }

  render() {
    const { overlayStyle, contentStyle, dialogStyle } = {};
    return (
      <div className={styles.modalContent} style={contentStyle} onClick={this.onOverlayClick}>
        <div className={styles.modalDialog} style={dialogStyle} onClick={this.onDialogClick}>
            {this.props.children}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func,
  children: PropTypes.object,
  handleDialogClick: PropTypes.func
};

export default Modal;
