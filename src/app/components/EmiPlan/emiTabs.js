import React from 'react';
import styles from './emi-plans.css';
import EmiTable from './emiTable';
import classNames from 'classnames/bind';

const bindClassNames = classNames.bind(styles);

class EmiTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: 0
    };
    this.handelClick = this.handelClick.bind(this);
  }

  handelClick(e) {
    e.preventDefault();
    e.stopPropagation();
    const bank = parseInt(e.target.dataset.bank, 10);
    this.setState({
      selected: bank
    });
  }

  render() {
    const tabsNav = (
      <ul className={styles.tabsNav}>
        {
          this.props.data.map((item, i) => {
            const listClasses = bindClassNames({
              active: this.state.selected === i
            });

            const iconClasses = bindClassNames({
              'myntraweb-sprite': true,
              arrowRightBold: true,
              show: this.state.selected === i
            });

            return (
              <li key={i} className={listClasses} data-bank={i} onClick={this.handelClick}>{item.bank}
                <span className={iconClasses}></span>
              </li>
            );
          })
        }
      </ul>
    );

    const tabsContent = (
      <ul className={styles.tabsContent}>
        {
          this.props.data.map((item, i) => {
            const tabClasses = bindClassNames({
              tab: true,
              active: this.state.selected === i
            });

            return (
              <li key={i} className={tabClasses}>
                <EmiTable data={item} mobile={false} price={this.props.price} />
              </li>
            );
          })
        }
      </ul>
    );

    return (
      <div className={styles.tabsContainer}>
        {tabsNav}
        {tabsContent}
      </div>
    );
  }
}

EmiTabs.propTypes = {
  data: React.PropTypes.array,
  price: React.PropTypes.number
};

export default EmiTabs;
