import React from 'react';
import styles from './emi-plans.css';
import { isBrowser, isMobile } from '../../utils';
import EmiTabs from './emiTabs';
import EmiList from './emiList';
import EmiTerms from './emiTerms';
import at from 'v-at';
import Jsuri from 'jsuri';
const kvPairs = isBrowser() ? window.__myx_kvpairs__ || {} : {};
const plans = kvPairs['pdp.emi.plans'] || [];
const emiTerms = kvPairs['pdp.emi.terms'];
import isEmpty from 'lodash/isEmpty';

const isApp = () => {
  if (isBrowser()) {
    const deviceChannel = at(window, '__myx_deviceData__.deviceChannel') || '';
    return deviceChannel.toLowerCase() === 'mobile_app';
  } return false;
};

class EmiPlan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showTerms: false
    };
    this.toggleTerms = this.toggleTerms.bind(this);
  }

  toggleTerms() {
    this.setState({
      showTerms: !this.state.showTerms
    });
  }

  render() {
    let productPrice = this.props.price;
    if (!productPrice) {
      const uri = new Jsuri(window.location.href);
      productPrice = uri.getQueryParamValue('p');
    }
    productPrice = parseInt(productPrice, 10);

    if (plans.length && productPrice) {
      const isMobileDevice = isMobile() || isApp();
      return (
        <div className={styles.container}>
          <div className={styles.header}>
            {(!isMobileDevice && this.state.showTerms) ? (
              <h3 onClick={this.toggleTerms} style={{ cursor: 'pointer' }}>
                <span className={`myntraweb-sprite ${styles.showEmiPlans}`}></span>
                EMI PLANS
              </h3>
            ) : (
              <h3>EMI PLANS</h3>
            )}
            {(!isMobileDevice && !this.state.showTerms) && (
              <span className={styles.additionInfoLinks}>
                <a href="/faqs#paymentQueries" className={styles.faqs}>FAQs</a>
                {!isEmpty(emiTerms) && (
                  <span onClick={this.toggleTerms} className={styles.terms}>Terms & Conditions</span>
                )}
              </span>
            )}
          </div>

          {this.state.showTerms ? (
            <div className={styles.desktopTerms}>
              <h5>Terms & Condition</h5>
              <EmiTerms emiTerms={emiTerms} />
            </div>
          ) : (
            <div>
              <div className={styles.disclaimer}>
                Available on listed bank credit cards. Pay easy monthly installments instead of lump-sum amount.
                {isMobileDevice && (<span><br /> <br /></span>)} Find suitable EMI option below and choose same option at payments step while placing the order.
              </div>
              {isMobileDevice ? (
                <EmiList data={plans} price={productPrice} emiTerms={emiTerms} />
              ) : (
                <EmiTabs data={plans} price={productPrice} />
              )}
            </div>
          )}
        </div>
      );
    }
    return (
      <div className={styles.noResults}>
        No EMI Plans found.
      </div>
    );
  }
}

EmiPlan.propTypes = {
  price: React.PropTypes.number
};

export default EmiPlan;
