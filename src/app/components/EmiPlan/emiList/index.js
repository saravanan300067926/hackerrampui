import React from 'react';
import styles from './emi-plans-list.css';
import ListItem from './listItem';
import Terms from '../emiTerms';
import isEmpty from 'lodash/isEmpty';

const EmiList = (props) => (
  <div>
    <ul className={styles.tabsNav}>
      {
        props.data.map((item, i) => (
          <ListItem key={i} data={item} price={props.price} />
        ))
      }
    </ul>
    <div className={styles.terms}>
      <a href="/faqs#paymentQueries" className={styles.faqs}>FAQs</a>
      {!isEmpty(props.emiTerms) && (
        <div>
          Terms & Conditions
          <Terms emiTerms={props.emiTerms} />
        </div>
      )}
    </div>
  </div>
);

EmiList.propTypes = {
  data: React.PropTypes.array,
  price: React.PropTypes.number,
  emiTerms: React.PropTypes.object
};

export default EmiList;
