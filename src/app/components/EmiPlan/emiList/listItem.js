import React from 'react';
import EmiTable from '../emiTable';
import styles from './emi-plans-list.css';
import classNames from 'classnames/bind';

const bindClassNames = classNames.bind(styles);

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      show: !this.state.show
    });
  }

  render() {
    const iconClasses = bindClassNames({
      'myntraweb-sprite': true,
      arrowIcon: true,
      arrowDownBold: this.state.show,
      arrowRightBold: !this.state.show
    });
    return (
      <li>
        <div className={styles.bankName} onClick={this.handleClick}>{this.props.data.bank}
          <span className={iconClasses}></span>
        </div>
        {this.state.show && (
          <EmiTable data={this.props.data} mobile={true} price={this.props.price} />
        )}
      </li>
    );
  }
}

List.propTypes = {
  data: React.PropTypes.object,
  price: React.PropTypes.number
};

export default List;
