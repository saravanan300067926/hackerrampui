import React from 'react';
import styles from './emi-plans.css';
import at from 'v-at';
import classNames from 'classnames/bind';
import sortBy from 'lodash/sortBy';

const bindClassNames = classNames.bind(styles);


const EmiTable = (props) => {
  let plans = at(props, 'data.plans');
  if (plans) {
    plans = sortBy(plans, 'months');
    const tableClasses = bindClassNames({
      emiTable: true,
      mobileTable: props.mobile
    });

    const tableBody = (
      <tbody>
      {
        plans.map((plan, i) => {
          const interest = plan.interest;
          const price = props.price;
          const monthlyEmi = Math.round(price * plan.f);
          const totalCost = Math.round(monthlyEmi * plan.months);
          const totalinterest = Math.round(totalCost - price);
          return (
            <tr key={i}>
              <td>{plan.months}</td>
              <td>{interest}% pa {props.mobile && (<br />)} (Rs. {totalinterest})</td>
              <td>Rs. {monthlyEmi}</td>
              <td>Rs. {totalCost}</td>
            </tr>
          );
        })
      }
      </tbody>
    );

    return (
      <table className={tableClasses}>
        <thead>
          <tr>
            <th>Months</th>
            <th>Interest</th>
            <th>Monthly EMI</th>
            <th>Overall Cost</th>
          </tr>
        </thead>
        {tableBody}
      </table>
    );
  }
  return null;
};

EmiTable.propTypes = {
  data: React.PropTypes.object,
  mobile: React.PropTypes.bool,
  price: React.PropTypes.number
};

export default EmiTable;
