import React from 'react';
import styles from './emi-plans.css';
import isArray from 'lodash/isArray';

const EmiTerms = ({ emiTerms }) => {
  if (emiTerms) {
    let terms = null;
    if (isArray(emiTerms.terms) && emiTerms.terms.length) {
      terms = emiTerms.terms.map((item, index) => (
        <li key={index}>{item}</li>
      ));
    }
    return (
      <div className={styles.termsDetails}>
        <p>{emiTerms.intro}</p>
        <ul>
          {terms}
        </ul>
      </div>
    );
  }
  return null;
};

EmiTerms.propTypes = {
  emiTerms: React.PropTypes.object
};

export default EmiTerms;
