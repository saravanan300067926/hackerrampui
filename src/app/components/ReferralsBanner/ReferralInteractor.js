import get from 'lodash/get';
import { Helper } from '../Mfg/utils/helper';

// referAndEarn.desktopSNE

const ReferralInteractor = {
  getData(beaconData) {
    const dataFromWindow = get(window, '__myx_kvpairs__.referAndEarn.desktopSNE');
    return get(beaconData, 'kvpairs.referAndEarn.desktopSNE', dataFromWindow);
  },

  getAllPoints(beaconData) {
    const pointsFromWindow = get(window, '__myx_lp__.loyaltyPoints.activePoints');
    const points = get(beaconData, 'lp.loyaltyPoints.activePoints', pointsFromWindow) || 0;
    return Number.isFinite(points) && points;
  },

  getMaxSnEPoints(beaconData) {
    const pointsFromWindow = get(window, '__myx_lp__.maxPoints');
    const points = get(beaconData, 'lp.maxPoints', pointsFromWindow) || 0;
    return Number.isFinite(points) && points;
  },

  // Nullable
  getScratchCards(beaconData) {
    const scratchCardsFromWindow = get(window, '__myx_lp__.scratchCards');
    const scratchCards = get(beaconData, 'lp.scratchCards', scratchCardsFromWindow) || [];
    return scratchCards;
  },

  getUnscratchCardsCount(scratchCards) {
    return Array.isArray(scratchCards) && scratchCards.filter(card => !card.isScratched).length;
  },

  isLoggedIn(beaconData) {
    const session = beaconData && beaconData.session;
    return session ? session.login : Helper.isLoggedIn();
  }
};

export default ReferralInteractor;
