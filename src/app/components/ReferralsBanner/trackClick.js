import MadalyticsWeb from 'madalytics-web';

export default function track(points) {
  MadalyticsWeb.configure({
    url: 'https://touch.myntra.com/track-web',
    storage: window.localStorage,
    variant: 'web',
    clientId: 'lzZNMYGoPkQVWOGL3wg81DLeJ4arpd'
  });
  const pathname = window.location.pathname;
  const re = /\d/;
  let pageName = 'list';
  if (pathname === '/') {
    pageName = 'home';
  } else if (pathname.includes('shop')) {
    pageName = 'SIS';
  } else if (pathname.includes('/buy') && re.test(pathname)) {
    pageName = 'PDP';
  }

  MadalyticsWeb.send(
    'widgetClick',
    {},
    {},
    {
      event_type: 'widgetClick',
      event_category: 'EoRS Refer & Earn',
      payload: {
        user_data: {
          myntraPointsBalance: points
        },
        screen: {
          url: window.location.href,
          variant: 'web'
        },
        widget: {
          name: 'EoRSReferInviteBanner',
          type: 'banner'
        },
        widget_items: {
          name: `EoRS Refer & Earn - Refer & Earn banner clicked on ${
            pageName
            } screen`
        }
      }
    }
  );
}
