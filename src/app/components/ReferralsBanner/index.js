import React from 'react';
import styles from './referrals-banner.css';
import get from 'lodash/get';
import bus from 'bus';
import ReferralInteractor from './ReferralInteractor';

function redirect(link) {
  window.location.href = link;
}

const BannerScaffold = props => props.enabled && (
  <div className={styles['loyalty-points-block']}>
    <img className={styles.bannerImg} src={props.image} />
    {props.children}
    <button className={styles['exchange-more-btn']} onClick={() => redirect(props.link)}>
      <span className={styles['extra-button-padding']}>
        {props.buttonText}
        <span className={styles['button-arrow']} />
      </span>
    </button>
  </div>
);

class ReferralsBanner extends React.Component {
  constructor() {
    super();
    this.state = {
      allPoints: null,
      maxSnEPointsLimit: null,
      isLoggedIn: false,
      componentData: null,
      unscratchCardCount: null
    };

    this.storeData = this.storeData.bind(this);
    this.renderDefault = this.renderDefault.bind(this);
    this.renderPointsOnly = this.renderPointsOnly.bind(this);
    this.renderScratchCardAndPoints = this.renderScratchCardAndPoints.bind(this);
    this.getComponent = this.getComponent.bind(this);
  }

  componentDidMount() {
    // Start listening to beacon update events in the bus.
    bus.on('beacon-data', (data) => {
      this.storeData(data);
    });
  }

  /**
  * There are 3 banners and hence 3 renderFunctions
  * This functions evalutates some conditions in state and returns
  * suitable componentType and renderFunction.
  */
  getComponent() {
    let result;
    if (this.state.unscratchCardCount !== 0) {
      result = ['bannerWithScratch', this.renderScratchCardAndPoints];
    } else if (this.state.allPoints !== 0) {
      result = ['bannerWithoutScratch', this.renderPointsOnly];
    } else {
      result = ['newUser', this.renderDefault];
    }
    return result;
  }


  storeData(beaconData) {
    const allPoints = ReferralInteractor.getAllPoints(beaconData);
    const maxSnEPointsLimit = ReferralInteractor.getMaxSnEPoints(beaconData);
    const isLoggedIn = ReferralInteractor.isLoggedIn(beaconData);
    const scratchCards = ReferralInteractor.getScratchCards(beaconData);
    const componentData = ReferralInteractor.getData(beaconData);
    const unscratchCardCount = ReferralInteractor.getUnscratchCardsCount(scratchCards);

    this.setState({
      isLoggedIn,
      componentData,
      allPoints,
      maxSnEPointsLimit,
      unscratchCardCount
    });
  }

  renderScratchCardAndPoints(data) {
    const cardCount = this.state.unscratchCardCount;
    const points = this.state.allPoints;

    const wishText = get(data, 'wishText')

    const ctaH4 = get(data, 'ctaHeading4')
    const scd = get(data, 'singleCardDescription')
    const mcd = get(data, 'multiCardDescription')
    const text = `${ctaH4} ${cardCount} ${cardCount > 1 ? mcd : scd}`;

    if (!points) {
      return (
        <div className={styles['scratch-cards']}>
          <div>{wishText}</div>
          <div className={styles['bold-text']}>
            {text}
          </div>
        </div>
      )
    }

    const scd2 = get(data, 'singleCardDescription2')
    const mcd2 = get(data, 'multiCardDescription2')

    const heading = `${cardCount} ${cardCount > 1 ? mcd2 : scd2}`;
    const heading2 = get(data, 'ctaHeading2')
    const heading3 = get(data, 'ctaHeading3')

    return (
      <div className={styles['scratch-cards']}>
        <div className={styles['bold-text']}>
          {heading}
        </div>
        <div>
          <span>{heading2}</span>
          <span> <strong>₹{points}</strong> {heading3}</span>
        </div>
      </div>
    );
  }

  renderPointsOnly(data) {
    const heading = get(data, 'heading');
    const description = get(data, 'description')
    return (
      <div className={styles['loyalty-points-info']}>
        <div className={styles['loyalty-points-value']}>
          <span className={styles.rs}>₹</span> {this.state.allPoints}
          <span className={styles['loyalty-points-msg']}>{heading}</span>
        </div>
        <div className={styles['loyalty-points-description']}>
          {description} ₹{this.state.maxSnEPointsLimit}
        </div>
      </div>
    )
  }

  renderDefault(data) {
    return (
      <div className={styles['loyalty-points-nonLoggedIn']}>
        {get(data, 'description')}
      </div>
    );
  }


  render() {
    if (get(this.state, 'componentData.enable')) {
      const [type, renderFn] = this.getComponent();
      const data = get(this.state.componentData, type);
      const enabled = get(data, 'enable');
      if (!enabled) {
        return null;
      }

      const image = get(data, 'image');
      const link = get(data, 'link');
      const buttonText = get(data, 'cta');

      return (
        <BannerScaffold
          enabled={enabled}
          image={image}
          link={link}
          buttonText={buttonText}>
          {renderFn(data)}
        </BannerScaffold>
      );
    }
    return null;
  }
}

export default ReferralsBanner;
