import React from 'react';
import styles from './verify.css';
import Notify from '../Notify';
import Loader from '../Loader';
import { verifyMobile, isLoggedIn } from '../../utils';

class MobileVerify extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      verify: new URLSearchParams(window.location.search).get('verify') || '',
      mobile: new URLSearchParams(window.location.search).get('verify') || '',
      submit: false,
      otp: [],
      loading: false,
      incorrectOtp: 0 // 0 implies nothing, 1 implies error 2 implies success
    };
  }

  componentDidMount() {
    if (this.state.verify) {
      this.otp0.focus();
    } else {
      window.ga('send', 'event', 'mobile_verification', 'start_process');
      this.mobileInput.focus();
    }
  }

  updateMobile = e => {
    const mob = e.target.value;
    if (this.validateMobile(mob)) {
      this.setState({ mobile: mob.substring(0, 10), submit: true });
    } else {
      this.setState({ mobile: mob.substring(0, 10), submit: false });
    }
  };

  validateOTPLength = char => {
    const allowed = '1234567890';
    return char.length === 1 && allowed.indexOf(char) > -1;
  };

  validateMobile = mob => {
    const mobTest = /^\d\d{9}$/i;
    return mob.length === 10 && mobTest.test(mob);
  };

  verifyMobilePress = e => {
    const char = String.fromCharCode(e.which);
    const allowed = '1234567890';
    if (allowed.indexOf(char) === -1) {
      e.preventDefault();
      e.stopPropagation();
    }
    if (this.state.submit && e.which === 13) {
      this.verifyNumber('verify');
    }
  };

  verifyOTP = () => {
    this.setState({
      loading: true
    });
    verifyMobile('otpVerify', {
      phone: this.state.mobile,
      otp: this.state.otp.join('')
    }, (err) => {
      if (err) {
        this.setState({ incorrectOtp: 1, otp: [], loading: false });
      } else {
        this.setState({ incorrectOtp: 2, loading: false }, () => {
          window.ga('send', 'event', 'mobile_verification', 'successful');
          setTimeout(() => {
            window.location.href = '/'; //Closing
          }, 3000);
        });
      }
    });
  };

  verifyNumber = type => {
    const msg = 'OTP sent successfully';
    const message = type === 'verify' ? `${msg}, Redirecting ...` : msg;
    this.setState({
      incorrectOtp: 0,
      loading: true
    });
    verifyMobile('mobileVerify', {
      phone: this.state.mobile
    },
      (err) => {
        this.setState({
          loading: false
        });
        if (!err) {
          this.refs.notify.info({
            message
          });
          if (type === 'verify') {
            setTimeout(() => {
              window.location.href = `${window.location.href}?verify=${
                this.state.mobile
                }`;
            }, 1500);
          }
        } else {
          this.refs.notify.error('Oops! Something went wrong. Please try again in some time.');
        }
      }
    );
  };

  otpEntered = e => {
    const otp = e.target.value;
    const attr = Number(e.target.getAttribute('data-val'));
    const otpState = [...this.state.otp];
    const isValidOTP = this.validateOTPLength(otp);
    otpState[attr] = isValidOTP ? otp : '';
    this.setState({ otp: otpState }, () => {
      if (otp.length === 1 && isValidOTP) {
        const name = `otp${(attr + 1) % 4}`;
        this[name].focus();
        if (otpState.filter(val => val !== '').length === 4) {
          this[name].blur();
          this.verifyOTP();
        }
      } else {
        e.target.focus();
      }
    });
  };

  handleBackspace = e => {
    const backspace = e.which === 8;
    const attr = Number(e.target.getAttribute('data-val'));
    const otpState = [...this.state.otp];
    if (backspace) {
      if (otpState[0]) {
        otpState.pop();
      }
      this.setState({ otp: otpState }, () => {
        this[`otp${attr > 0 ? attr - 1 : attr}`].focus();
      });
    }
  };

  renderMobileMarkup = () => {
    const bContainer = `${styles.buttonContainer} ${this.state.submit ? styles.enable : styles.disable}`;
    return (
      <div className={styles.verificationContainer}>
        {this.renderTopImage()}
        <div className={styles.mobContainer}>
          <h3>Enter Mobile Number for Verification</h3>
          <div className={styles.mobileNum}>
            <input type="text" value="+91" readOnly="true" />
            <input
              type="tel"
              name="mobile"
              maxLength="10"
              size="13"
              ref={input => (this.mobileInput = input)}
              onKeyPress={this.verifyMobilePress}
              value={this.state.mobile}
              onChange={this.updateMobile}
              autoComplete="off" />
          </div>
        </div>
        <div className={bContainer}>
          <button
            className={styles.button}
            onClick={() => {
              this.verifyNumber('verify');
            }}>
            <img src={'https://constant.myntassets.com/web/assets/img/0684fd78-7aef-4b22-a18d-c3da6580e09a1557312926098-fab_copy.png'} />
          </button>
        </div>
        <Loader show={this.state.loading} />
        <Notify ref="notify" />
      </div>
    );
  };

  renderOTPInput = num => {
    let name = `otp${num}`;
    const otp = this.state.otp;
    return (
      <input
        name={name}
        type="tel"
        maxLength={1}
        ref={input => (this[name] = input)}
        data-val={num}
        onChange={this.otpEntered}
        onKeyUp={this.handleBackspace}
        value={otp[num] || ''}
        autoComplete="off" />
    );
  };

  renderOTPMarkup = () => {
    let error = this.state.incorrectOtp === 1 ? 'Incorrect OTP' : '';
    if (this.state.incorrectOtp === 2) {
      return (
        <div className={styles.successVerified}>
          <div className={styles.successIcon}>
            <div className={styles.check} />
          </div>
          <div className={styles.successText}>Successfully Verified</div>
          <Notify ref="notify" />
        </div>
      );
    }
    return (
      <div className={styles.verificationContainer}>
        {this.renderTopImage()}
        <div className={styles.mobContainer}>
          <h3>Enter OTP</h3>
          <h4>OTP sent to mobile number {this.state.mobile}</h4>
          <div className={styles.otpContainer}>
            {this.renderOTPInput(0)}
            {this.renderOTPInput(1)}
            {this.renderOTPInput(2)}
            {this.renderOTPInput(3)}
          </div>
          <div className={styles.errorContainer}>{error}</div>
          <button
            className={styles.resendContainer}
            onClick={() => {
              this.verifyNumber('resend');
            }}>
            RESEND OTP
          </button>
        </div>
        <Loader show={this.state.loading} />
        <Notify ref="notify" />
      </div>
    );
  };

  renderTopImage = () => {
    let imgSet =
      'https://constant.myntassets.com/web/assets/img/3a438cb4-c9bf-4316-b60c-c63e40a1a96d1548071106233-mobile-verification.jpg';
    return (
      <div className={styles.image}>
        <img
          height="150px"
          src={imgSet}
          title="Mobile Verification" />
      </div>
    );
  };

  render() {
    if (!isLoggedIn()) {
      window.location.href = `/login?referer=${window.location.href}`;
      return null;
    }
    if (this.state.verify && !/^\d{10}$/.test(this.state.verify)) {
      window.location.href = '/verification';
      return null;
    }
    if (this.state.verify) {
      return this.renderOTPMarkup();
    }
    return this.renderMobileMarkup();
  }
}

export default MobileVerify;
