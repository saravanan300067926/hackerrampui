import React, { PropTypes } from 'react';
import styles from './shop.css';
import Myx from '../Myx';
import { isBrowser } from '../../utils';
import at from 'v-at';
import bus from 'bus';

class Shop extends React.Component {
  static propTypes = {
    layout: PropTypes.object,
    dehydratedState: PropTypes.object,
    location: PropTypes.object,
    'location.dehydratedState': PropTypes.object
  };

  constructor(props) {
    super(props);
    let layout = {
      type: 'layout'
    };
    const pageName = at(this.props, 'routeParams.page') || at(this.props, 'route.page');
    const key = `layout_${pageName || ''}`;
    if (isBrowser() && window.__myx && window.__myx[key]) {
      layout = window.__myx[key];
    }
    this.state = {
      layout
    };
  }

  componentDidMount() {
    // FIXME: load layout if no layout
    if (at(window, '__myx_session__')) {
      this.sendAnalytics();
    } else {
      bus.on('beacon-data', () => {
        setTimeout(this.sendAnalytics, 0);
      });
    }
  }

  sendAnalytics() {
    const pageType = at(window, '__myx.pageName') || '';
    let pathName;
    if (pageType === 'Home') {
      pathName = '/stream';
    } else {
      try {
        pathName = `${pageType}${' -/ '}${window.location.pathname.split('/')[window.location.pathname.split('/').length - 1].trim()}`;
      } catch (e) {
        pathName = 'shop page';
      }
    }

    if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
      window.Madalytics.send('ScreenLoad', {
        type: pageType,
        name: pathName
      });
    }
  }

  render() {
    const layout = isBrowser() ? this.state.layout : this.props.location.dehydratedState;
    return (
      <main className={styles.base}>
        <Myx layout={layout} />
      </main>
    );
  }
}

export default Shop;
