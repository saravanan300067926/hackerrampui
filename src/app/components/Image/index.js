import React from 'react';
import Lazy from 'react-lazyload';

const Image = props => {
  return (
    <Lazy
      height={props.height}
      once={true}
      offset={props.offset || 300}
      width={props.width}>
      <picture className="img-responsive" style={{ width: '100%', height: '100%', display: 'block' }}>
        <source
          srcSet={props.src}
          type="image/webp" />
        <img
          src={props.src}
          className="img-responsive"
          alt={props.alt}
          title={props.title}
          style={props.styles} />
      </picture>
    </Lazy>
  );
};

export default Image;

Image.propTypes = {
};
