import Client from '../../services';

import { Helper } from '../../components/Mfg/utils/helper';

const fireEvents = (screenName, eventType, clickedProductPosition, timeDuration, pageNumber) => {
    const uidx = Helper.getUidx();
    let timestamp = (new Date()).getTime(), userQuery, numFilters, userSortApplied, solrQuery;
    switch (eventType) {
        case 0: userQuery = window.location.pathname.split('/')[1] + window.location.search;
            const urlParams = new URLSearchParams(window.location.search);
            userSortApplied = urlParams.has('sort');
            numFilters = ((urlParams.get('f') || '').split('::') || []).length + ((urlParams.get('rf') || '').split('::') || []).length;
            solrQuery = (window.__myx.searchData.results.filters.primaryFilters.find(filter => filter.id === "Categories") || { filterValues: [{}] }).filterValues.sort((a, b) => b.count - a.count)[0].id;
            break;
        default: break;
    }

    Client.get('/pushEvents', {
        uidx: 1233,
        timestamp,
        screenName,
        userQuery,
        eventType,
        numFilters,
        userSortApplied,
        timeDuration,
        clickedProductPosition,
        pageNumber,
        solrQuery
    }).end(err => err && console.log(err));
}

export default fireEvents;