import React from 'react';
import at from 'v-at';
import get from "lodash/get";
import { isOutOfStock, isSellableInventoryOOS, showAddToBag, isSizeAvailable } from '../Pdp/utils';
import styles from '../Pdp/pdp.css';
import priorityCheckout, { PRIORITY_CHECKOUT_MESSAGE } from '../../utils/priorityCheckoutUtils';
import Track from '../../utils/track';
import Heart from '../../resources/svg/Heart'
import HeartOutline from '../../resources/svg/HeartOutline'

const getPriorityCheckoutButton = (state, addToWishlist) => (
  <div>
    <div className={`${styles['add-to-bag']} ${styles['priority-out-of-stock']}`}>
      Out Of Stock
    </div>
    <div
      onClick={() => addToWishlist('wishlist')}
      className={`${styles.pdpButtonWishlistNow}`}>
      <span className={`${styles.flex} ${styles.column} ${styles.center}} ${styles.wishlistNow}`}>
        {state.priorityCheckoutWishlistAddActionName}
        <span className={styles.outOfStockText}>MAY GET RESTOCKED</span>
      </span>
    </div>
  </div>
);

const getButtonContent = (state, buttonLabel, outOfStock = false) => {
  const isPreOrderitem = get(state, 'isPreOrderItem', false);
  const priorityCheckoutButtonContent = priorityCheckout() ? (
    <div className={`${styles.flex} ${styles.column} ${styles.center} ${styles['align-start']}`}>
      <span>{buttonLabel}</span>
      {!outOfStock && (
        <span className={styles['text-10']}>{PRIORITY_CHECKOUT_MESSAGE}</span>
      )}
    </div>
  ) : buttonLabel;

  return isPreOrderitem ? 'PRE-ORDER' : priorityCheckoutButtonContent;
};

// ADD TO BAG BUTTON ON PDP PAGE
const getAddToCartButton = (state, addToBag, isSizeChart, stylesCTA, goToBag, isMobileApp) => {
  const available = isSizeAvailable(state);
  if (isOutOfStock(state)) {
    return (<div
      className={`${styles['add-to-bag']} ${styles.flex} ${styles.center} ${styles['out-of-stock']} ${isSizeChart ? stylesCTA['sc-addToBag'] : ''}`}>
      {getButtonContent(state, 'OUT OF STOCK', true)}
    </div>);
  } else if (showAddToBag(state)) {
    const sizesAddedToBag = state.sizesAddedToBag;
    const selectedSkuid = state.selectedSkuid || null;
    const sellerPartnerId = selectedSkuid ?
      get((get(state, "data.sizes") || []).find(
         size => size.skuId === selectedSkuid
       ), 'selectedSeller.sellerPartnerId') :
      get(state, 'selectedSeller.sellerPartnerId');
    let buttonClass = state.wishlistEnabled ? styles['add-to-bag'] : styles['add-to-bag-full'];
    if (priorityCheckout()) {
      buttonClass = state.wishlistEnabled ? styles['add-to-bag-with-prior'] : styles['add-to-bag-full-with-prior'];
    }
    if (isMobileApp && sizesAddedToBag.length > 0 && sizesAddedToBag.indexOf(selectedSkuid) !== -1) {
      return (
        <div
          onClick={() => {
            goToBag();
            Track.event('PDP', 'GoToCartPage', `GoToBagButton | ${sellerPartnerId} | ${at(window, '__myx.pdpData.id')}`);
          }}
          className={`${styles.goToCart} ${buttonClass} ${styles.flex} ${styles.center} ${isSizeChart ? stylesCTA['sc-addToBag'] : ''}`}>
          <span>{getButtonContent(state, 'GO TO BAG')}</span>
          <span className={`myntraweb-sprite ${styles.whiteRightArrow}`} />
        </div>
      );
    }
    if (sizesAddedToBag.length > 0 && sizesAddedToBag.indexOf(selectedSkuid) !== -1) {
      return (
        <a
          href="/checkout/cart"
          onClick={() => {
            Track.event('PDP', 'GoToCartPage', `GoToBagButton | ${sellerPartnerId} | ${at(window, '__myx.pdpData.id')}`);
          }}
          className={`${styles.goToCart} ${buttonClass} ${styles.flex} ${styles.center} ${isSizeChart ? stylesCTA['sc-addToBag'] : ''}`}>
          <span>{getButtonContent(state, 'GO TO BAG')}</span>
          <span className={`myntraweb-sprite ${styles.whiteRightArrow}`} />
        </a>
      );
    }
    return (<div
      onClick={() => addToBag(`${isSizeChart ? 'addToCart-sizeChart' : 'addToCart'}`)}
      className={`${buttonClass} ${styles.flex} ${styles.center}
      ${isSizeChart ? `${stylesCTA['sc-addToBag']} ${!selectedSkuid ? stylesCTA['sc-addToBag-disabled'] : ''}` : ''}
      ${!available ? `${stylesCTA['sc-addToBag-disabled']}` : ''}`}>
      <span className={`myntraweb-sprite ${styles.whiteBag} ${styles.flex} ${styles.center}`} />
      {getButtonContent(state, 'ADD TO BAG')}
    </div>);
  }
  return null;
};

const getWishlistButton = (state, addToWishlist, isSizeChart, stylesCTA, gotToWishlist, isMobile, rest) => {
  const isWishlisted = state.wishlistAddActionName !== 'WISHLIST';
  const btnClass = showAddToBag(state) ? styles['add-to-wishlist'] : styles['add-to-wishlist-full'];
  const scWishlistClass = !showAddToBag(state) && isSizeChart ? stylesCTA['add-to-wishlist-full-sc'] : '';
  const whBtnClass = isWishlisted ? styles['add-to-wishlist-disabled'] : styles['add-to-wishlist'];
  let buttonTxt = !isWishlisted ? 'WISHLIST' : 'WISHLISTED';
  const { isPreLaunch, isHeartIconEnabled } = rest;
  if (buttonTxt === 'WISHLIST' && isPreLaunch) {
    buttonTxt = 'WISHLIST NOW';
  }
  if (state.wishlistEnabled) {
    if (isMobile && isWishlisted) {
      return (
        <div
          onClick={() => gotToWishlist()}
          className={`${btnClass} ${whBtnClass} ${styles.flex} ${styles.center} ${isSizeChart ? stylesCTA['sc-wishlist'] : ''}
${isPreLaunch ? stylesCTA['wishlisted-prelaunch'] : ''} ${scWishlistClass}`}>
          {isHeartIconEnabled ? <Heart className={styles.heartButtonIcon} /> :
            <span className={`myntraweb-sprite ${styles.darkWishlistIcon} ${styles.flex} ${styles.center}`} />}
          {buttonTxt}
        </div>
      );
    }
    return (
      <div
        onClick={() => addToWishlist('wishlist')}
        className={`${scWishlistClass} ${btnClass} ${whBtnClass} ${styles.flex} ${styles.center}
${isSizeChart ? stylesCTA['sc-wishlist'] : ''} ${isPreLaunch ? styles.prelaunchCta : ''}
${isPreLaunch && isSizeChart ? styles.prelaunchCtaSC : ''} ${isMobile && isPreLaunch ? stylesCTA.appPrelaunchCTA : ''}
${isWishlisted ? styles.wishlistedPrelaunchBtn : ''}
`}>
        {isHeartIconEnabled ? <HeartOutline className={styles.heartButtonIcon} /> :
         isWishlisted ? (
          <span className={`myntraweb-sprite ${styles.whiteWishlistIcon} ${styles.flex} ${styles.center} ${isPreLaunch ? styles.disableSprite : ''}`} />
          ) : (
          <span className={`myntraweb-sprite ${styles.darkWishlistIcon} ${styles.flex} ${styles.center} ${isPreLaunch ? styles.disableSprite : ''}`} />
          )
        }
        <span className={isPreLaunch ? styles.prelaunchTxt : ''}>{buttonTxt}</span>
        {isPreLaunch && <span className={styles.launchDate}>{get(rest, 'prelaunchAttr[0].metaInfo')}</span>}
      </div>
    );
  }
  return null;
};

const getPreLaunchAttr = (systemAttributes = []) => systemAttributes.filter(
  ({ systemAttributeValueEntry: { systemAttributeEntry } }) => systemAttributeEntry.attributeName === 'Enable Buy Button'
);

const ActionCTA = (props) => {
  const state = at(props, 'state');
  const stylesCTA = at(props, 'stylesCTA');
  const addToWishlist = at(props, 'addToWishlist');
  const addToBag = at(props, 'addToBag');
  const isSizeChart = at(props, 'isSizeChart');
  const goToBag = at(props, 'goToBag');
  const gotToWishlist = at(props, 'gotToWishlist');
  const isMobile = at(props, 'isMobile');
  const isHeartIconEnabled = at(props, 'isHeartIconEnabled');
  const systemAttributes = at(props, 'systemAttributes');
  const prelaunchAttr = (systemAttributes && getPreLaunchAttr(systemAttributes)) || [];
  const isPreLaunch = at(prelaunchAttr[0], 'systemAttributeValueEntry.valueName') === 'No';
  if ((priorityCheckout() && isSellableInventoryOOS(state) && !isOutOfStock(state))) {
    return getPriorityCheckoutButton(state, addToWishlist);
  }
  return (
    <div className={isSizeChart ? stylesCTA['size-chart-cta'] : ''}>
      <div
        className={`${styles['action-container']} ${styles.fixed}
${isSizeChart ? stylesCTA['size-chart'] : ''} ${isSizeChart && isPreLaunch ? stylesCTA['prelaunch-cta'] : ''} `}>
        {!isPreLaunch && getAddToCartButton(state, addToBag, isSizeChart, stylesCTA, goToBag, isMobile)}
        {getWishlistButton(state, addToWishlist, isSizeChart, stylesCTA, gotToWishlist, isMobile, {
          isPreLaunch, prelaunchAttr, isHeartIconEnabled
        })}
      </div>
    </div>
  );
};

export default ActionCTA;
