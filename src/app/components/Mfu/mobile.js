import React from 'react';
import styles from './pointsbanner.css';
import at from 'v-at';
import get from 'lodash/get';
import bus from 'bus';
import { compactNumbers } from 'compactnums';

class PointsBanner extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.displayLP();
    // Start listening to beacon upadate events in the bus.
    bus.on('beacon-data', (data) => {
      this.displayLP(data);
    });
  }

  displayLP(data) {
    const features = window.__myx_features__ || {};
    const mfuEnable = get(data, 'features["mfu.enable"]', features['mfu.enable']) === true;
    const mfuEnableWishlist = get(data, 'features["mfu.enable.wishlist"]', features['mfu.enable.wishlist']) === true;
    const userLP = data && data.lp ? data.lp.points : at(window, '__myx_lp__.points');
    const loggedIn = data && data.session ? data.session.login : at(window, '__myx_session__.login');
    const showPoints = loggedIn && this.hasValidPoints(userLP);
    const redirectUri = data && data.features ? data.features['mfu.exchange.url'] : features['mfu.exchange.url'];
    const isWishList = get(window, 'location.pathname') === '/wishlist';
    const isMFUSaleOn = isWishList ? mfuEnableWishlist : mfuEnable;
    this.setState({ isMFUSaleOn, userLP, showPoints, redirectUri });
  }

  hasValidPoints(lp) {
    return (lp && !isNaN(lp));
  }

  redirectToExchangePage = () => {
    window.location.href = this.state.redirectUri;
  }

  render() {
    let view;
    if (!this.state.isMFUSaleOn) {
      view = null;
    } else if (!this.state.showPoints) {
      view = (
        <div className={styles['mobile-loyalty-points-block']}>
          <div className={`myntraweb-sprite ${styles['mobile-coinicon']}`}> </div>
          <div className={styles['mobile-loyalty-points-nonLoggedIn']}> Exchange to save more now! </div>
          <button className={styles['mobile-exchange-more-btn']} onClick={this.redirectToExchangePage}>
            <span className={styles['mobile-extra-button-padding']}> EARN POINTS </span>
          </button>
        </div>
      );
    } else {
      view = (
        <div className={styles['mobile-loyalty-points-block']}>
          <div className={`myntraweb-sprite ${styles['mobile-coinicon']}`}> </div>
          <div className={styles['mobile-loyalty-points-value']}> {compactNumbers(this.state.userLP)} </div>
          <div className={styles['mobile-loyalty-points-msg']}> Points earned </div>
          <button className={styles['mobile-exchange-more-btn']} onClick={this.redirectToExchangePage}>
            <span className={styles['mobile-extra-button-padding']}> EARN MORE </span>
          </button>
        </div>
      );
    }
    return view;
  }
}


export default PointsBanner;
