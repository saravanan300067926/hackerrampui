import React from 'react';
import { Row } from '../Grid';
import ProductResults from './MobileFilters/Results';
import MobileFilters from './MobileFilters';


const MobileSearch = () => (
  <div>
    <Row><ProductResults /></Row>
    <MobileFilters />
  </div>
);

export default MobileSearch;
