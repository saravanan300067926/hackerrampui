const getUpdatedValuesForPrice = (priceFilter, action) => {
  const { checkedValues } = priceFilter;
  const { checked, option } = action.data;
  return checked
    ? checkedValues.concat(option)
    : checkedValues.filter(value => value !== option);
};

const getUpdatedValuesForGender = (genderFilter, action) => {
  let { checkedValues } = genderFilter;
  const selectedValues = action.data.option.split(',');
  checkedValues = action.data.checked
    ? selectedValues
    : checkedValues.filter(item => !selectedValues.includes(item));
  return checkedValues;
};

const getUpdatedValuesForCheckBox = (changedFilter, action) => {
  const updatedCheckedValues = action.data.checked
    ? changedFilter.checkedValues.concat(action.data.option)
    : changedFilter.checkedValues
      .filter(item => item.toLowerCase() !== action.data.option.toLowerCase());

  return updatedCheckedValues;
};

const getUpdatedValuesForDiscount = ({ data }) => {
  const updatedCheckedValues = data.checked ? [data.option] : [];
  return updatedCheckedValues;
};

const getUpdatePincode = (action) => {
  const pinNum = action.data.pincode;
  return action.data.option === 'delivery_time' ? pinNum.toString() : '';
};
const getUpdatedSort = (sort, action) => {
  const updatedSortValues = sort.values.map(element => {
    if (element.option === action.data.option) {
      return {
        ...element,
        checked: action.data.checked
      };
    }
    return {
      ...element,
      checked: false
    };
  });
  return {
    ...sort,
    values: updatedSortValues
  };
};

function getUpdatedCommonFilters(oldFilters, action) {
  const { group, option, checked } = action.data;
  const targetFilter = oldFilters.find(filter => filter.id.toLowerCase() === group.toLowerCase());
  const updatedOldFilters = oldFilters.filter(filter => filter.id.toLowerCase() !== group.toLowerCase());

  if (!targetFilter && checked) {
    const UpdatedTargetFilter = [{ id: group, values: [option] }];
    return [...updatedOldFilters, ...UpdatedTargetFilter];
  }

  let values = [];
  if (checked) {
    values = [...targetFilter.values, option];
  } else {
    values = targetFilter.values.filter(value => value !== option);
  }

  return values.length > 0 ? [...updatedOldFilters, { ...targetFilter, values }] : updatedOldFilters;
}

function getUpdatedRangeFilters(oldRangeFilters, action) {
  const { group, option, checked } = action.data;
  const targetFilter = oldRangeFilters.find(filter => filter.id.toLowerCase() === group.toLowerCase());
  const updatedOldRangeFilters = oldRangeFilters.filter(filter => filter.id.toLowerCase() !== group.toLowerCase());

  if (!targetFilter && checked) {
    const updatedTargetFilter = [{
      id: group,
      values: [{
        id: option,
        start: option.split(' TO ')[0],
        end: option.split(' TO ')[1]
      }]
    }];
    return [...updatedOldRangeFilters, ...updatedTargetFilter];
  }

  let values = [];
  if (checked) {
    values = [...targetFilter.values,
      {
        id: option,
        start: parseInt(option.split(' TO ')[0], 10),
        end: parseInt(option.split(' TO ')[1], 10)
      }];
  } else {
    values = targetFilter.values.filter(value => value.id !== option);
  }

  return values.length > 0 ? [...updatedOldRangeFilters, { ...targetFilter, values }] : updatedOldRangeFilters;
}

const getUpdatedAppliedParams = (appliedParams, action) => {
  let filters = [];
  let rangeFilters = [];
  if (action.type === 'SORT_UPDATED') {
    if (action.data.option === 'delivery_time') {
      const pincode = action.data.pincode;
      const pin = pincode.toString();
      return {
        ...appliedParams,
        sort: action.data.option,
        pincode: pin
      };
    }
    return {
      ...appliedParams,
      sort: action.data.option
    };
  }
  if (action.type === 'RADIOBUTTON_UPDATED' || action.data.group.toLowerCase() === 'price') {
    rangeFilters = getUpdatedRangeFilters(appliedParams.rangeFilters, action);
    return {
      ...appliedParams,
      rangeFilters
    };
  }
  if (action.type === 'CHECKBOX_UPDATED') {
    filters = getUpdatedCommonFilters(appliedParams.filters, action);
    return {
      ...appliedParams,
      filters
    };
  }

  return appliedParams;
};

const getUpdatedFilters = (allFilters, action) => {
  let changedFilter;
  let filterGroup;
  const actionType = action.type;
  let checkedValues;
  let updatedFilter = {};
  let updatedFilterGroup = [];

  for (const key of Object.keys(allFilters)) {
    changedFilter = allFilters[key].find(filter => filter.id.toLowerCase() === action.data.group.toLowerCase());
    if (changedFilter) {
      filterGroup = key;
      break;
    }
  }

  if (actionType === 'PRICE_UPDATED') {
    checkedValues = getUpdatedValuesForPrice(changedFilter, action);
  } else if (actionType === 'GENDER_UPDATED') {
    checkedValues = getUpdatedValuesForGender(changedFilter, action);
  } else if (actionType === 'RADIOBUTTON_UPDATED') {
    checkedValues = getUpdatedValuesForDiscount(action);
  } else if (actionType === 'CHECKBOX_UPDATED') {
    checkedValues = getUpdatedValuesForCheckBox(changedFilter, action);
  } else {
    return allFilters;
  }

  updatedFilter = { ...changedFilter, checkedValues };
  updatedFilterGroup = allFilters[filterGroup].filter(filter => filter.id.toLowerCase() !== action.data.group.toLowerCase());
  return {
    ...allFilters,
    [filterGroup]: [...updatedFilterGroup, updatedFilter]
  };
};

const getResetValuesForAllFilters = (results) => {
  const resetResults = { ...results };

  // reset appliedParams
  resetResults.appliedParams = {
    'filters': [],
    'geoFilters': [],
    'rangeFilters': [],
    'sort': ''
  };

  // reset filters
  const keys = Object.keys(resetResults.filters);
  for (const key of keys) {
    for (const filter of resetResults.filters[key]) {
      filter.checkedValues = [];
    }
  }

  return { ...resetResults };
};

const getResetValuesForSorts = (sorts) => {
  const resetValues = sorts.values.map(element => (
    {
      ...element,
      checked: false
    }
  ));
  return {
    ...sorts,
    values: resetValues
  };
};

const getNewState = (state, action) => {
  let newState = {};
  let results = {};
  let filters = {};
  let appliedParams = {};
  let sort = {};
  let updatedSort = {};
  let pincode = '';
  let updatePincode = '';

  state = state.search;
  pincode = state.pincode;
  results = state.results;
  filters = results.filters;
  appliedParams = results.appliedParams;
  sort = state.sort;
  if (action.type === 'MORE_PRODUCTS_CLICKED') {
    newState = {
      ...state,
      page: action.data.page
    };
    return newState;
  }

  if (action.type === 'RESET_ALL_FILTERS') {
    const resetResults = getResetValuesForAllFilters(results);
    const resetSort = getResetValuesForSorts(sort);
    newState = {
      ...state,
      page: 1,
      results: resetResults,
      sort: resetSort
    };
    return newState;
  }

  const updatedFilters = getUpdatedFilters(filters, action);
  const updatedAppliedParams = getUpdatedAppliedParams(appliedParams, action);

  if (action.type === 'SORT_UPDATED') {
    updatedSort = getUpdatedSort(sort, action);
    updatePincode = getUpdatePincode(action);
  } else {
    updatedSort = sort;
    updatePincode = pincode;
  }

  newState = {
    ...state,
    page: 1,
    results: {
      ...results,
      appliedParams: updatedAppliedParams,
      filters: updatedFilters
    },
    sort: updatedSort,
    pincode: updatePincode
  };
  return newState;
};

export default getNewState;
