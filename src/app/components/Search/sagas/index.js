import { takeLatest } from 'redux-saga';
import { put, call, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import stateToQuery, { buildFetchUrl } from './stateToQuery';
import getNewState from './getNewState';
import config from '../../../config';
import get from '../services';
import lscache from 'lscache';
// import update from 'react-addons-update';
import at from 'v-at';
import { removeProductDuplicates } from '../helpers';
import cookies from '../../../utils/cookies';

const getCurrentState = state => state.search;

function removeDuplicates(response) {
  response.results.products = removeProductDuplicates(at(response, 'results.products'));
}

function* fetchData(action) {
  let response = {};
  const previousState = yield select(state => getCurrentState(state));
  const newStateOptimistic = yield select(state => getNewState(state, action));
  const cacheKey = stateToQuery(newStateOptimistic);

  yield put(push(cacheKey));
  yield put({
    type: 'API_FETCH_INPROGRESS',
    response: {
      // apiFetchInProgress: true
    }
  });

  try {
    const uniqueId = cookies.get('_d_id') || '';
    const uidx = cookies.get('myx.session.login') || '';

    const fetchUrl = buildFetchUrl(newStateOptimistic.page, newStateOptimistic.rows, cacheKey);
    response = yield call(
      get, ...[
        `${config('filteredSearch')}${fetchUrl}`,
        { 'app': 'web', 'x-myntra-app': `deviceID=${uniqueId};customerID=${uidx};reqChannel=web;` }]
    );

    response = { ...newStateOptimistic, results: response.body };
    removeDuplicates(response);

    const products = at(response, 'results.products');

    if (products && products.length > 0) {
      lscache.set(cacheKey, response, 5);
    }

    yield put({
      type: 'API_FETCH_SUCCEEDED',
      response
    });
  } catch (error) {
    console.log(error);
    yield put({
      type: 'API_FETCH_FAILED',
      response: {
        apiFetchInProgress: false,
        apiFetchFailed: true
      }
    });
    yield put.sync({
      type: 'STATE_CHANGED',
      response: previousState
    });
  }
}


function* updateFilters() {
  yield* takeLatest(
    [
      'CHECKBOX_UPDATED',
      'RADIOBUTTON_UPDATED',
      'GENDER_UPDATED',
      'SORT_UPDATED',
      'ATSA_UPDATED',
      'PRICE_UPDATED',
      'MORE_PRODUCTS_CLICKED',
      'RESET_ALL_FILTERS'
    ],
    fetchData
  );
}

function* rootSaga() {
  yield updateFilters();
}

export { rootSaga };
