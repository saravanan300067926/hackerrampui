import { FILTER_APPLIEDFILTER_DISCREPENCY_MAP, arrayToObject } from '../helpers';
import find from 'lodash/find';
import get from 'lodash/get';


function buildPincodeFilter(pin) {
  return pin ? `pincode=${pin}` : '';
}

/**
 * Generates sort query
 * e.g. sort=discount,price_asc
 *
 * @param {String} sortValues: comma separated Sort query Values
 */
function buildSortQueryFromSortString(sortValues = '') {
  const checkedSortValues = sortValues.split(',').filter(value => value !== '');
  return checkedSortValues.length === 0
    ? ''
    : `sort=${encodeURIComponent(checkedSortValues.join(','))}`;
}

/**
 * Generates sort query
 * e.g. sort=discount,price_asc
 *
 * @param {Array} sortValues: array containing sort options
 */
function buildSortQuery(sortValues = [], pincode) {
  const checkedSortValues = sortValues
    .filter(sortValue => sortValue.checked)
    .map(value => value.option);
  if (checkedSortValues.length === 0) {
    return '';
  }
  return checkedSortValues[0] === 'delivery_time'
    ? `sort=${encodeURIComponent(checkedSortValues.join(','))}&pincode=${pincode}`
    : `sort=${encodeURIComponent(checkedSortValues.join(','))}`;
}

/**
 * Generates filter query
 * e.g. f=color:Black,Grey::gender:men,women::categories:Backpacks,Flip Flops
 *
 * @param {Array} appliedFilters : Array of applied Filters
 */
function buildFilterQueryFromAppliedFilters(filters = [], appliedFilters = []) {
  const query = appliedFilters
    .filter(item => item.id !== 'Color')
    .map(filter => `${filter.id}:${filter.values.join(',')}`);
  filters = arrayToObject(filters);

  let appliedColors = find(appliedFilters, ['id', 'Color']);

  if (appliedColors) {
    appliedColors = get(appliedColors, 'values', []);
    const filterValues = get(filters, 'Color.filterValues', []);
    const colorQuery = filterValues
      .filter(color => appliedColors.includes(color.id))
      .map(colorFilter => `${colorFilter.id}_${colorFilter.meta}`);

    if (colorQuery.length > 0) {
      query.push(`Color:${colorQuery.join(',')}`);
    }
  }

  return query.length === 0
    ? ''
    : `f=${encodeURIComponent(query.sort().join('::'))}`;
}

/**
 * Generates filter query
 * e.g. f=color:Black,Grey::gender:men,women::categories:Backpacks,Flip Flops
 *
 * @param {Object} filters : state filter object excluding range filter
 * @param {Array} customFilterQuery : Array of custom query values
 */
function buildFilterQuery(filters = {}, customFilterQuery = [], preSetFilterQuery = []) {
  const filterArray = [];
  const query = [];
  Object.keys(filters).map(key =>
    Object.keys(filters[key]).map(itemKey =>
      filterArray.push(filters[key][itemKey])
    )
  );

  for (const filterGroup of filterArray) {
    const title = filterGroup.id;
    const filterValues = filterGroup.filterValues || [];
    let checkedValues = filterGroup.checkedValues || [];

    // different query format for color filters
    if (title === 'Color') {
      checkedValues = filterValues
        .filter(({ id }) => checkedValues.includes(id))
        .map(({ id, meta }) => `${id}_${meta}`);
    }

    if (checkedValues && checkedValues.length > 0) {
      checkedValues = checkedValues.sort().join(',');
      query.push(`${title}:${checkedValues}`);
    }
  }

  // add custom filters
  query.push(...preSetFilterQuery);
  query.push(...customFilterQuery);
  return query.length === 0
    ? ''
    : `f=${encodeURIComponent(query.sort().join('::'))}`;
}

/**
 * Generates range filter query from passed appliedRangeFilter array
 *
 * @param {Array} appliedRangeFilters : response.appliedParams.rangeFilters
 */
function buildRangeFilterQueryFromAppliedRangeFilters(appliedRangeFilters) {
  const query = appliedRangeFilters.map(filter => {
    const filterValues = filter.values.map(item => {
      const { id } = item;
      const [from, to] = id.split(' TO ');
      return `${from}_${to}_${id}`;
    });
    return `${filter.id}:${filterValues.join(',')}`;
  });

  return query.length === 0
    ? ''
    : `rf=${encodeURIComponent(query.sort().join('::'))}`;
}
/**
 * Generates range filter query
 * e.g. price:100.0_500.0_100.0 TO 500.0,500.0_1000.0_500.0 TO 1000.0,discount range:30.0_100.0_30.0 TO 100.0
 *
 * @param {Array} rangeFilters : range filters array from state
 */
function buildRangeFilterQuery(rangeFilters = []) {
  const query = [];

  for (const filterGroup of rangeFilters) {
    const title = FILTER_APPLIEDFILTER_DISCREPENCY_MAP[filterGroup.id] || filterGroup.id;
    const checkedValues = filterGroup.checkedValues;
    if (checkedValues && checkedValues.length > 0) {
      const checkedRfValues = checkedValues.map(value => {
        const parts = [...value.split(' TO '), value];
        return parts.join('_');
      });

      query.push(`${title}:${checkedRfValues.join(',')}`);
    }
  }

  return query.length === 0
    ? ''
    : `rf=${encodeURIComponent(query.sort().join('::'))}`;
}

function getPresetFilters(filters, appliedFilterParams) {
  if (appliedFilterParams.length === 0) return [];
  const PrimSecFilters = [...filters.primaryFilters, ...filters.secondaryFilters];
  return appliedFilterParams.filter((item) => {
    let index = PrimSecFilters.length;
    while (index--) {
      if (item.id === PrimSecFilters[index].id) return false;
    }
    return true;
  });
}

const stateToParams = state => {
  const { rangeFilters, ...filters } = state.results.filters;
  const appliedFilterParams = state.results.appliedParams.filters || [];
  const sortValues = state.sort.values;
  const pinstr = state.pincode;
  const pincode = parseInt(pinstr, 10);
  const customFilters = state.customFilters;
  const preSetFilters = getPresetFilters(filters, appliedFilterParams);

  const customFilterQuery = [];
  const preSetFilterQuery = [];

  for (const item of customFilters) {
    customFilterQuery.push(`${item.key}:${item.value}`);
  }

  for (const item of preSetFilters) {
    preSetFilters.sort().join(',');
    preSetFilterQuery.push(`${item.id}:${item.values}`);
  }

  const paginationQuery = state.page > 1 ? `p=${state.page}` : '';

  const sortQuery = buildSortQuery(sortValues, pincode);
  const filterQuery = buildFilterQuery(filters, customFilterQuery, preSetFilterQuery);
  const rangeFilterQuery = buildRangeFilterQuery(rangeFilters);

  return [sortQuery, filterQuery, rangeFilterQuery, paginationQuery].filter(
    value => value !== ''
  );
};

const buildQueryFromAppliedParams = (state) => {
  const { filters, rangeFilters, sort, pincode } = state.results.appliedParams;
  const primaryFilters = state.results.filters.primaryFilters;
  const pin = parseInt(pincode, 10);
  const zpincodeQuery = buildPincodeFilter(pin);
  const sortQuery = buildSortQueryFromSortString(sort);
  const filterQuery = buildFilterQueryFromAppliedFilters(primaryFilters, filters);
  const rangeFilterQuery = buildRangeFilterQueryFromAppliedRangeFilters(rangeFilters);
  return [sortQuery, zpincodeQuery, filterQuery, rangeFilterQuery].filter(
    value => value !== ''
  );
};

const stateToQuery = state => {
  let query = stateToParams(state);

  // state is build from API response and checkedValues are not present
  // Try building the cache key from appliedParams object
  if (query.filter(item => !item.includes('sort')).length === 0) {
    query = buildQueryFromAppliedParams(state);
  }
  query = query.sort().join('&');
  query = `${window.location.pathname}?${query}`;
  const path = window.location.pathname;

  if (query.replace('?', '') === path) {
    return path;
  }
  return query;
};

export const buildFetchUrl = (page, limit, cacheKey) => {
  const offset = page > 1 ? ((page - 1) * 50) - 1 : 0;
  if (!cacheKey.includes('?')) {
    return `${cacheKey}?rows=${limit}&o=${offset}`;
  }
  return `${cacheKey}&rows=${limit}&o=${offset}`;
};

export default stateToQuery;
