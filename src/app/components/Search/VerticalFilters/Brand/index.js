import React, { PropTypes } from 'react';
import styles from './brand.css';
import commonStyles from '../../../../resources/common.css';
import verticalFilterStyles from '../vertical-filters.css';
import FilterDirectory from '../FilterDirectory';
import FilterSearchBox from '../FilterSearch';

class Brand extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      search: ''
    };
    this.toggleDirectory = this.toggleDirectory.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
  }

  getMoreButton(itemsLeft) {
    return (
      <div onClick={this.toggleDirectory} className={styles.more}>+ {itemsLeft} more</div>
     );
  }

  toggleDirectory(e) {
    e.stopPropagation();
    this.setState({
      expanded: !this.state.expanded
    });
  }

  updateSearch(e) {
    const query = e.target.value.toLowerCase();
    this.setState({
      search: query
    });
  }

  clearSearch() {
    this.setState({
      search: ''
    });
  }

  render() {
    let moreButton;
    let searchBox;
    let { data, checkedData } = this.props;

    const filteredItems = data
      .filter(e => {
        const query = this.state.search.toLowerCase();
        return !query || e.id.toLowerCase().includes(query);
      });

    if (data.length > 8) {
      moreButton = this.getMoreButton(data.length - 8);
      searchBox = <FilterSearchBox onQueryChange={this.updateSearch} clearSearch={this.clearSearch} title="Brand" />;
    }

    data = filteredItems.slice(0, 8).map((brandObject, index) => {
      const { id, count } = brandObject;
      const checked = checkedData.includes(id);
      return (
        <li key={index}>
          <label className={`${verticalFilterStyles.label} ${commonStyles.customCheckbox}`}>
            <input
              className={styles.input}
              type="checkbox"
              value={id}
              onChange={this.props.handleChange}
              checked={checked} />{id}<span className={styles.num}>({count})</span>
            <div className={commonStyles.checkboxIndicator}></div>
          </label>
        </li>
      );
    });

    return (
      <div className={`${verticalFilterStyles.filters} ${styles.container}`}>
        <span className={verticalFilterStyles.header}>{this.props.title}</span>
        {searchBox}
        <ul className={styles.list}>{data}</ul>
        {moreButton}
        {this.state.expanded &&
          <FilterDirectory
            type="brand"
            filterDirectory={this.props.brandDirectory}
            checkedItems={checkedData}
            directoryIndices={this.props.directoryIndices}
            toggleDirectory={this.toggleDirectory}
            onChange={this.props.handleChange}
            expanded={this.state.expanded} />
        }
      </div>
    );
  }
}

Brand.propTypes = {
  data: PropTypes.array,
  checkedData: PropTypes.array,
  title: PropTypes.string,
  handleChange: PropTypes.func,
  brandDirectory: PropTypes.object,
  directoryIndices: PropTypes.array
};

export default Brand;
