import React, { PropTypes } from 'react';
import styles from './FilterDirectory.css';
import commonStyles from '../../../../resources/common.css';
import classNames from 'classnames/bind';
import hScroll from '@myntra/myx/lib/horizontal-scroll';
import Indices from './indices';
import { pageScroll, offset } from '../../../../utils/dom';

const bindClassNames = classNames.bind(styles);

class Directory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: null,
      filterTohighlight: null
    };
    this.updateSearch = this.updateSearch.bind(this);
    this.handleIndicesOnMouseEnter = this.handleIndicesOnMouseEnter.bind(this);
    this.handleIndicesOnMouseLeave = this.handleIndicesOnMouseLeave.bind(this);
    this.handleIndicesOnClick = this.handleIndicesOnClick.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const pageHeader = document.getElementById('desktop-header-cnt');
    // scrolls directoryPanel into view
    if (pageHeader && this.directoryPanel) {
      const pageHeaderOffsetBottom = offset(pageHeader).bottom;
      const directoryOffsetTop = offset(this.directoryPanel).top;
      if (pageHeaderOffsetBottom > directoryOffsetTop) {
        window.scrollTo(0, pageScroll().top - (pageHeaderOffsetBottom - directoryOffsetTop) - 20);
      }
    }
    if (this.searchInput) {
      this.searchInput.focus();
    }

    document.addEventListener('click', this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClick, false);
  }

  handleClick(e) {
    if (!this.directoryPanel.contains(e.target)) {
      e.preventDefault();
      this.props.toggleDirectory(e);
    }
  }

  isChecked(item = []) {
    return this.props.checkedItems.includes(item.id);
  }

  updateSearch(e) {
    const query = e.target.value.toLowerCase();
    this.setState({
      search: query
    });
  }

  handleIndicesOnMouseEnter(e) {
    const key = e.target.dataset.item;
    if (key) {
      this.setState({
        filterTohighlight: key.toLowerCase()
      });
    }
  }

  handleIndicesOnMouseLeave() {
    this.setState({
      filterTohighlight: null
    });
  }

  handleIndicesOnClick(e) {
    const key = e.target.dataset.item;
    const parent = this.refs.filterList;
    const scrollToElement = this.refs[key];
    hScroll(parent, scrollToElement, 10);
  }

  render() {
    let directory = {};
    const listItems = [];

    if (this.state.search) {
      for (const key in this.props.filterDirectory) {
        if (Object.prototype.hasOwnProperty.call(this.props.filterDirectory, key)) {
          const matchedItems = this.props.filterDirectory[key].filter((filter) => (
            filter.id.toLowerCase().indexOf(this.state.search) > -1
          ));
          if (matchedItems.length > 0) {
            directory[key] = matchedItems;
          }
        }
      }
    } else {
      directory = this.props.filterDirectory;
    }

    if (this.props.expanded) {
      for (const key in directory) {
        if (Object.prototype.hasOwnProperty.call(this.props.filterDirectory, key)) {
          let labelClasses = '';
          if (this.state.filterTohighlight) {
            labelClasses = bindClassNames({
              disabled: !(this.state.filterTohighlight === key.toLowerCase())
            });
          }
          const items = directory[key].map((filter, index) => (
            <li key={index}>
              <label className={`${labelClasses} ${commonStyles.customCheckbox}`}>
                <input
                  className={styles.input}
                  type="checkbox"
                  value={filter.id}
                  onChange={this.props.onChange}
                  checked={this.isChecked(filter)} />{filter.id}<span className={styles.count}>({filter.count})</span>
                <div className={commonStyles.checkboxIndicator}></div>
              </label>
            </li>
          ));
          listItems.push(
            <li
              className={`${styles.listTitle} ${labelClasses}`}
              ref={key}
              data-item={key}
              key={`tag-${key}`}>{key}</li>
          );
          listItems.push(items);
        }
      }
    }

    const activeKeys = Object.keys(directory);

    const panelClasses = bindClassNames({
      panel: true,
      expanded: this.props.expanded
    });

    return (
      <div className={panelClasses} ref={(div) => { this.directoryPanel = div; }}>
        <div className={styles.titleBar}>
          <input
            placeholder={`Search ${this.props.type}`}
            className={styles.searchInput}
            type="text"
            ref={(input) => { this.searchInput = input; }}
            onKeyUp={this.updateSearch} />
          <Indices
            activeKeys={activeKeys}
            handleIndicesOnMouseEnter={this.handleIndicesOnMouseEnter}
            handleIndicesOnMouseLeave={this.handleIndicesOnMouseLeave}
            handleIndicesOnClick={this.handleIndicesOnClick}
            directoryIndices={this.props.directoryIndices} />
          <span onClick={this.props.toggleDirectory} className={`myntraweb-sprite ${styles.close}`} />
        </div>
        <div>
          <ul className={styles.list} ref="filterList">
            {listItems}
          </ul>
        </div>
      </div>
    );
  }
}

Directory.propTypes = {
  filterDirectory: PropTypes.object,
  directoryIndices: PropTypes.array,
  toggleDirectory: PropTypes.func,
  onChange: PropTypes.func,
  expanded: PropTypes.bool,
  type: PropTypes.string,
  checkedItems: PropTypes.array
};

export default Directory;
