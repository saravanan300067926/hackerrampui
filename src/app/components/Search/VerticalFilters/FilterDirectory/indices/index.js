import React, { PropTypes } from 'react';
import styles from '../FilterDirectory.css';
import classNames from 'classnames/bind';

const bindClassNames = classNames.bind(styles);

const Indices = ({ activeKeys, directoryIndices, handleIndicesOnMouseEnter, handleIndicesOnMouseLeave, handleIndicesOnClick }) => {
  const items = directoryIndices.map((item, i) => {
    const listClasses = bindClassNames({
      disabled: activeKeys.indexOf(item) < 0
    });

    return (
      <li
        onMouseEnter={handleIndicesOnMouseEnter}
        onMouseLeave={handleIndicesOnMouseLeave}
        onClick={handleIndicesOnClick}
        data-item={item}
        className={listClasses}
        key={i}>{item}</li>
    );
  });

  return (
    <ul className={styles.indices}>
      {items}
    </ul>
  );
};

Indices.propTypes = {
  handleIndicesOnMouseEnter: PropTypes.func,
  handleIndicesOnMouseLeave: PropTypes.func,
  handleIndicesOnClick: PropTypes.func,
  directoryIndices: PropTypes.array,
  activeKeys: PropTypes.array
};

export default Indices;
