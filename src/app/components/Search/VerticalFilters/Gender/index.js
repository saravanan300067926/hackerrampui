import React, { PropTypes } from 'react';
import styles from './gender.css';
import verticalFilterStyles from '../vertical-filters.css';
import commonStyles from '../../../../resources/common.css';

const getClassName = checked => {
  if (checked) {
    return `${styles.label} ${styles.checked}`;
  }
  return styles.label;
};

const Gender = ({ data, title, handleChange }) => {
  const listItems = data
    .map((genderObject, index) => (
      <li key={index}>
        <label className={`${commonStyles.customRadio} ${getClassName(genderObject.checked)}`}>
          <input
            className={styles.input}
            type="radio"
            value={genderObject.value}
            checked={genderObject.checked}
            onChange={handleChange} />
          {genderObject.title}
          <div className={commonStyles.radioIndicator} />
        </label>
      </li>
    ));

  return (
    <div className={verticalFilterStyles.filters}>
      {title && <span className={verticalFilterStyles.header}>{title}</span>}
      <ul className={styles.list}>{listItems}</ul>
    </div>
  );
};

Gender.propTypes = {
  data: PropTypes.array,
  title: PropTypes.string,
  handleChange: PropTypes.func
};

export default Gender;
