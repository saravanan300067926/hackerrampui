import {
  arrayToObject,
  combineReducersState,
  sortByCount,
  sortByDiscount,
  normalizeGender,
  sortObjectKeysByAlpha,
  injectCheckedFilters,
  injectCheckedRangeFilter
 } from '../helpers';
import get from 'lodash/get';
import remove from 'lodash/remove';
import differenceBy from 'lodash/differenceBy';

let filterSummary = [];

function getFilterSummary(state) {
  const combinedState = combineReducersState(state);

  const { rangeFilters, primaryFilters, secondaryFilters } = combinedState.results.filters;
  const summaryFilters = arrayToObject([...primaryFilters, ...secondaryFilters]);

  const appliedSummaryFilters = arrayToObject(get(combinedState, 'results.appliedParams.filters', {}));
  const appliedRangeFilters = arrayToObject(get(combinedState, 'results.appliedParams.rangeFilters', {}));


  const filterSummaryState = injectCheckedFilters(summaryFilters, appliedSummaryFilters);
  const rangeFilterSummaryState = injectCheckedRangeFilter(arrayToObject(rangeFilters), appliedRangeFilters);

  const filters = [...Object.values(filterSummaryState), ...Object.values(rangeFilterSummaryState)];

  const selectedFilters = [];

  const exclude = {};

  filters.filter(
    element => (
      !(element.id.toLowerCase() in exclude) && (element.filterValues.length > 0)
    )
  ).forEach((filter) => {
    const filterValues = filter.filterValues || [];
    const checkedValues = filter.checkedValues || [];
    if (filterValues.length > 0) {
      const self = filter;
      filterValues.forEach((value) => {
        if (checkedValues.includes(value.id)) {
          value.group = self.id;
          value.value = value.id;
          selectedFilters.push(value);
        }
      });
    }
  });

  // filter that need to be added to summary
  const toBeAdded = differenceBy(selectedFilters, filterSummary, 'id');
  if (toBeAdded.length > 0) {
    const cloneSummary = [...filterSummary];
    toBeAdded.forEach((value) => {
      cloneSummary.push(value);
    });
    filterSummary = [...cloneSummary];
  }

  // filter that need to be removed from summary
  const toBeRemoved = differenceBy(filterSummary, selectedFilters, 'id');
  if (toBeRemoved.length > 0) {
    const cloneSummary = [...filterSummary];
    toBeRemoved.forEach((value) => {
      const compare = {};
      compare.id = value.id;
      remove(cloneSummary, compare);
    });
    filterSummary = [...cloneSummary];
  }

  return filterSummary;
}

const mapStateToProps = state => {
  const combinedState = combineReducersState(state);
  const filters = get(combinedState, 'results.filters', {});
  const appliedFilters = get(combinedState, 'results.appliedParams', {});

  const primaryFilters = arrayToObject(filters.primaryFilters);
  const rangeFilters = arrayToObject(filters.rangeFilters);
  const appliedPrimaryFilters = arrayToObject(appliedFilters.filters);
  const appliedRangeFilters = arrayToObject(appliedFilters.rangeFilters);

  const primaryFilterState = injectCheckedFilters(primaryFilters, appliedPrimaryFilters);
  const rangeFilterState = injectCheckedRangeFilter(rangeFilters, appliedRangeFilters);
  const appliedSort = get(appliedFilters, 'sort', '');
  const brandDirectory = sortObjectKeysByAlpha(get(primaryFilters, 'Brand'));
  const categoryDirectory = sortObjectKeysByAlpha(get(primaryFilters, 'Categories'));
  const directoryIndices = '#abcdefghijklmnopqrstuvwxyz'.split('');

  const selectedFilters = getFilterSummary(state) || [];

  return {
    apiFetchInProgress: combinedState.apiFetchInProgress,
    gender: normalizeGender(get(primaryFilterState, 'Gender')),
    categories: sortByCount(get(primaryFilterState, 'Categories')),
    brand: sortByCount(get(primaryFilterState, 'Brand')),
    colour: sortByCount(get(primaryFilterState, 'Color')),
    price: get(rangeFilterState, 'Price'),
    discount: sortByDiscount(get(rangeFilterState, 'Discount Range')),
    appliedSort,
    brandDirectory,
    categoryDirectory,
    directoryIndices,
    isfilterSelected: selectedFilters.length > 0
  };
};

export default mapStateToProps;
