import React, { PropTypes } from 'react';
import styles from './header.css';
import verticalFilterStyles from '../vertical-filters.css';


const Header = (props) => (
  <div className={`${verticalFilterStyles.filters} ${styles.container}`}>
    <span className={styles.title}>FILTERS</span>
    {props.isfilterSelected && (
      <span
        onClick={() => { props.resetAllFilters(); props.onClearAll(false); }}
        onMouseEnter={() => { props.onClearAll(true); }}
        onMouseLeave={() => { props.onClearAll(false); }}
        className={styles.clearAllBtn}>
        CLEAR ALL
      </span>)}
  </div>
);

Header.propTypes = {
  isfilterSelected: PropTypes.bool,
  resetAllFilters: PropTypes.func,
  onClearAll: PropTypes.func
};

export default Header;
