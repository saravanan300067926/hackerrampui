import React, { PropTypes } from 'react';
import styles from './discount.css';
import commonStyles from '../../../../resources/common.css';
import verticalFilterStyles from '../vertical-filters.css';

const getLabel = discountLabel => `${Math.round(discountLabel.split(' TO ')[0])}% and above`;
const isChecked = (checkedItems = [], item) => checkedItems.includes(item.id);

const Discount = ({ data, checkedData, title, handleChange }) => {
  const listItems = data.map((item, index) => (
    <li key={index}>
      <label className={`${commonStyles.customRadio} ${verticalFilterStyles.label}`}>
        <input
          className={styles.input}
          type="radio"
          name="discount-product"
          value={item.id}
          checked={isChecked(checkedData, item)}
          onChange={handleChange} />
        {getLabel(item.id)}
        <div className={commonStyles.radioIndicator}></div>
      </label>
    </li>
  ));
  return (
    <div className={verticalFilterStyles.filters}>
      <span className={verticalFilterStyles.header}>{title}</span>
      <ul className={styles.list}>{listItems}</ul>
    </div>
  );
};

Discount.propTypes = {
  data: PropTypes.array,
  checkedData: PropTypes.array,
  title: PropTypes.string,
  handleChange: PropTypes.func
};

export default Discount;
