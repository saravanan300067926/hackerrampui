import Track from '../../../utils/track';

const getGaEventAction = (checked) => (
  checked ? 'FilterGenericOn' : 'FilterGenericOff'
);

const trackGaEvent = (checked, group, option) => {
  Track.event('ListPage', getGaEventAction(checked), `${group} | ${option}`);
};

const mapDispatchToProps = dispatch => ({
  updateCategories(e) {
    const checked = e.target.checked;
    const option = e.target.value;
    const group = 'Categories';

    dispatch({
      type: 'CHECKBOX_UPDATED',
      data: {
        option,
        checked,
        group
      }
    });

    trackGaEvent(checked, group, option);
  },
  updateBrand(e) {
    const checked = e.target.checked;
    const option = e.target.value;
    const group = 'Brand';

    dispatch({
      type: 'CHECKBOX_UPDATED',
      data: {
        option,
        checked,
        group
      }
    });

    trackGaEvent(checked, group, option);
  },
  updateColour(e) {
    const checked = e.target.checked;
    const option = e.target.value;
    const group = 'Color';

    dispatch({
      type: 'CHECKBOX_UPDATED',
      data: {
        option,
        checked,
        group
      }
    });

    trackGaEvent(checked, group, option);
  },
  updateDiscount(e) {
    const checked = e.target.checked;
    const option = e.target.value;
    const group = 'Discount Range';

    dispatch({
      type: 'RADIOBUTTON_UPDATED',
      data: {
        option,
        checked,
        group
      }
    });

    trackGaEvent(checked, group, option);
  },
  updateGender(e) {
    const checked = e.target.checked;
    const option = e.target.value;
    const group = 'Gender';

    dispatch({
      type: 'GENDER_UPDATED',
      data: {
        option,
        checked,
        group
      }
    });

    trackGaEvent(checked, group, option);
  },
  updatePrice(e) {
    const checked = e.target.checked;
    const option = e.target.value;
    const group = 'Price';

    dispatch({
      type: 'PRICE_UPDATED',
      data: {
        option,
        checked,
        group
      }
    });

    trackGaEvent(checked, group, option);
  },

  resetAllFilters() {
    dispatch({
      type: 'RESET_ALL_FILTERS'
    });
  }
});

export default mapDispatchToProps;
