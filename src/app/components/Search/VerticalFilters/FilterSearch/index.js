import React, { PropTypes } from 'react';
import styles from './filter-search.css';
import Track from '../../../../utils/track';


class FilterSearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
    this.expandSearchBox = this.expandSearchBox.bind(this);
    this.trackSearchClick = this.trackSearchClick.bind(this);
  }

  expandSearchBox() {
    if (this.state.expanded) {
      this.props.clearSearch();
    }
    this.setState({
      expanded: !this.state.expanded
    }, () => {
      if (this.state.expanded) {
        this.searchInput.focus();
      } else {
        this.searchInput.blur();
        this.searchInput.value = '';
      }
    });
  }

  trackSearchClick() {
    Track.event('ListPage', 'search_within_filter', `${this.props.title}`);
  }

  render() {
    const inputClasses = [styles.inputBox];
    const containerClasses = [styles.filterSearchBox];

    if (this.state.expanded) {
      containerClasses.push(styles.expanded);
    } else {
      inputClasses.push(styles.hidden);
    }

    return (
      <div className={containerClasses.join(' ')}>
        <input
          className={inputClasses.join(' ')}
          ref={(input) => { this.searchInput = input; }}
          type="text"
          placeholder={`Search for ${this.props.title}`}
          onKeyUp={this.props.onQueryChange} />
        <span
          onClick={() => {
            this.expandSearchBox();
            this.trackSearchClick();
          }}
          className={this.state.expanded ? `myntraweb-sprite ${styles.closeSearch}` : `myntraweb-sprite ${styles.iconSearch}`}>
        </span>
      </div>
    );
  }
}

FilterSearchBox.propTypes = {
  onQueryChange: PropTypes.func.isRequired,
  clearSearch: PropTypes.func.isRequired,
  title: PropTypes.string
};

export default FilterSearchBox;
