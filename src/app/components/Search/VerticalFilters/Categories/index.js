import React, { PropTypes } from 'react';
import styles from './categories.css';
import commonStyles from '../../../../resources/common.css';
import verticalFilterStyles from '../vertical-filters.css';
import FilterDirectory from '../FilterDirectory';
import FilterSearchBox from '../FilterSearch';

class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      search: ''
    };
    this.toggleDirectory = this.toggleDirectory.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
  }

  getMoreButton(itemsLeft) {
    return (
      <div onClick={this.toggleDirectory} className={styles.more}>+ {itemsLeft} more</div>
    );
  }

  toggleDirectory(e) {
    e.stopPropagation();
    this.setState({
      expanded: !this.state.expanded
    });
  }

  updateSearch(e) {
    const query = e.target.value.toLowerCase();
    this.setState({
      search: query
    });
  }

  clearSearch() {
    this.setState({
      search: ''
    });
  }

  render() {
    let moreButton;
    let searchBox;
    const listClasses = [styles.list];

    let listItems = this.props.data;
    const checkedItems = this.props.checkedData;


    const filteredItems = listItems
      .filter(e => {
        const query = this.state.search.toLowerCase();
        return !query || e.id.toLowerCase().includes(query);
      });

    if (listItems.length > 8) {
      moreButton = this.getMoreButton(listItems.length - 8);
      searchBox = <FilterSearchBox onQueryChange={this.updateSearch} clearSearch={this.clearSearch} title="Categories" />;
    }

    listItems = filteredItems.slice(0, 8).map((categoriesObject, index) => {
      const { id, count } = categoriesObject;
      const checked = checkedItems.includes(id);
      return (
        <li key={index}>
          <label className={`${commonStyles.customCheckbox} ${verticalFilterStyles.label}`}>
            <input
              className={styles.input}
              type="checkbox"
              value={id}
              onChange={this.props.handleChange}
              checked={checked} />{id}<span className={styles.num}>({count})</span>
            <div className={commonStyles.checkboxIndicator}></div>
          </label>
        </li>
      );
    });

    return (
      <div className={`${verticalFilterStyles.filters} ${styles.container}`}>
        <span className={verticalFilterStyles.header}>{this.props.title}</span>
        {searchBox}
        <ul className={listClasses.join(' ')}>{listItems}</ul>
        {moreButton}
        {this.state.expanded &&
          <FilterDirectory
            type="categories"
            filterDirectory={this.props.categoryDirectory}
            directoryIndices={this.props.directoryIndices}
            checkedItems={this.props.checkedData}
            toggleDirectory={this.toggleDirectory}
            onChange={this.props.handleChange}
            expanded={this.state.expanded} />
        }
      </div>
    );
  }
}

Categories.propTypes = {
  data: PropTypes.array,
  checkedData: PropTypes.array,
  title: PropTypes.string,
  handleChange: PropTypes.func,
  categoryDirectory: PropTypes.object,
  directoryIndices: PropTypes.array
};

export default Categories;
