import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Gender from './Gender';
import Categories from './Categories';
import Brand from './Brand';
import Colour from './Colour';
import Price from './Price';
import Discount from './Discount';
import Header from './FilterHeader';
import { pageScroll, windowHeight } from '../../../utils/dom';
import scroll from '@myntra/myx/lib/scroll';
import ConditionalRender from '../ConditionalRender';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';
import at from 'v-at';
import { isBrowser } from '../../../utils';

let filterClicked = false;

class VerticalFilters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sideBarPositionFix: false
    };
    this.setFilterClicked = this.setFilterClicked.bind(this);
    this.fixSideBarPosition = this.fixSideBarPosition.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (filterClicked && !nextProps.apiFetchInProgress) {
      if (isBrowser()) {
        if ((windowHeight() / 2) < pageScroll().top) {
          scroll(0);
        } else {
          window.scrollTo(0, 0);
        }
      }
      filterClicked = false;
      this.setState({
        sideBarPositionFix: true
      });
    }
  }

  setFilterClicked() {
    filterClicked = true;
    this.setState({
      sideBarPositionFix: false
    });
  }

  fixSideBarPosition() {
    this.setState({
      sideBarPositionFix: true
    });
  }

  render() {
    return (
      <section onChange={this.setFilterClicked}>
        <ConditionalRender sideBarPositionFix={this.state.sideBarPositionFix} isClearAll={at(this.props, 'isClearAll')}>
          <Header
            isfilterSelected={at(this.props, 'isfilterSelected')}
            resetAllFilters={at(this.props, 'resetAllFilters')}
            onClearAll={at(this.props, 'onClearAll')}
            data={[1, 2, 3]} />
          <Gender
            data={this.props.gender}
            handleChange={this.props.updateGender} />
          <Categories
            categoryDirectory={at(this.props, 'categoryDirectory')}
            directoryIndices={at(this.props, 'directoryIndices')}
            data={at(this.props, 'categories.filterValues')}
            checkedData={at(this.props, 'categories.checkedValues')}
            title={at(this.props, 'categories.id')}
            handleChange={this.props.updateCategories} />
          <Brand
            brandDirectory={at(this.props, 'brandDirectory')}
            directoryIndices={at(this.props, 'directoryIndices')}
            data={at(this.props, 'brand.filterValues')}
            checkedData={at(this.props, 'brand.checkedValues')}
            title={at(this.props, 'brand.id')}
            handleChange={this.props.updateBrand} />
          <Price
            data={at(this.props, 'price.filterValues')}
            checkedData={at(this.props, 'price.checkedValues')}
            title={at(this.props, 'price.id')}
            handleChange={this.props.updatePrice} />
          <Colour
            data={at(this.props, 'colour.filterValues')}
            checkedData={at(this.props, 'colour.checkedValues')}
            title={at(this.props, 'colour.id')}
            handleChange={this.props.updateColour}
            fixSideBarPosition={this.fixSideBarPosition} />
          <Discount
            data={at(this.props, 'discount.filterValues')}
            checkedData={at(this.props, 'discount.checkedValues')}
            title={at(this.props, 'discount.id')}
            handleChange={this.props.updateDiscount} />
        </ConditionalRender>
      </section>
    );
  }
}

VerticalFilters.propTypes = {
  gender: PropTypes.array,
  categories: PropTypes.object,
  brand: PropTypes.object,
  price: PropTypes.object,
  colour: PropTypes.object,
  discount: PropTypes.object,
  updateCategories: PropTypes.func,
  updateBrand: PropTypes.func,
  updateColour: PropTypes.func,
  updateDiscount: PropTypes.func,
  updatePrice: PropTypes.func,
  updateGender: PropTypes.func,
  apiFetchInProgress: PropTypes.bool,
  brandDirectory: PropTypes.object,
  categoryDirectory: PropTypes.object,
  directoryIndices: PropTypes.array,
  appliedParams: PropTypes.object,
  isfilterSelected: PropTypes.bool,
  resetAllFilters: PropTypes.func,
  onClearAll: PropTypes.func,
  isClearAll: PropTypes.bool
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VerticalFilters);
