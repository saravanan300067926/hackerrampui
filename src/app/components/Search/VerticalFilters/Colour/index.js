import React, { PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from './colour.css';
import colourMap from './colourMap';
import commonStyles from '../../../../resources/common.css';
import verticalFilterStyles from '../vertical-filters.css';
import at from 'v-at';
import FilterSearchBox from '../FilterSearch';

const bindClassNames = classNames.bind(styles);

// const lightColors = ['white', 'off white', 'cream', 'beige', 'peach'];

const getStyle = colourName => ({
  backgroundColor: `${colourMap[colourName.toLowerCase().replace(' ', '-')]}`
});

class Colour extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAll: false,
      search: ''
    };
    this.showAllColours = this.showAllColours.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
  }

  getListClasses(colour) {
    const isMultiColour = colour.toLowerCase() === 'multi';
    return bindClassNames({
      label: true,
      'myntraweb-sprite': isMultiColour,
      'multiColorLabel': isMultiColour,
      'colorDisplay': true
    });
  }

  showAllColours(e) {
    e.stopPropagation();
    this.setState({
      showAll: true,
      search: ''
    }, () => {
      this.props.fixSideBarPosition();
    });
  }

  updateSearch(e) {
    const query = e.target.value.toLowerCase();
    this.setState({
      search: query
    });
  }

  clearSearch() {
    this.setState({
      search: ''
    });
  }

  render() {
    let moreButton;
    let searchBox;
    let listItems = this.props.data;
    const checkedItems = this.props.checkedData || [];

    const filteredItems = listItems
      .filter(e => {
        const query = this.state.search.toLowerCase();
        return !query || e.id.toLowerCase().includes(query);
      });

    if (listItems.length > 8) {
      searchBox = <FilterSearchBox onQueryChange={this.updateSearch} clearSearch={this.clearSearch} title="Color" />;
    }

    if (!this.state.showAll && listItems.length > 13) {
      listItems = filteredItems.slice(0, 7);
      moreButton = (
        <div className={styles.more} onClick={this.showAllColours}>
          <span >+ {this.props.data.length - 7} more</span>
        </div>
      );
    } else {
      listItems = filteredItems;
    }

    listItems = listItems.filter((element) => {
      const id = at(element, 'id');
      if (id) {
        return id.toLowerCase() !== 'na';
      }
      return false;
    });

    listItems = listItems.map((colourObject, index) => (
      <li
        key={index}
        className={styles.listItem}>
        <label
          className={commonStyles.customCheckbox}
          data-count={colourObject.count}>
          <span
            data-colorhex={`${colourObject.id.toLowerCase()}`}
            className={this.getListClasses(colourObject.id)}
            style={getStyle(colourObject.id)} />
            {colourObject.id} <span className={styles.num}>({colourObject.count})</span>
          <input
            className={commonStyles.checkbox}
            type="checkbox"
            value={colourObject.id}
            checked={checkedItems.includes(colourObject.id)}
            onClick={this.props.handleChange} />
          <div className={commonStyles.checkboxIndicator} />
        </label>
      </li>
    ));

    return (
      <div className={verticalFilterStyles.filters}>
        <span className={verticalFilterStyles.header}>{this.props.title}</span>
        {searchBox}
        <ul>{listItems}</ul>
        {moreButton}
      </div>
    );
  }
}

Colour.propTypes = {
  data: PropTypes.array,
  checkedData: PropTypes.array,
  title: PropTypes.string,
  handleChange: PropTypes.func,
  fixSideBarPosition: PropTypes.func
};

export default Colour;
