import React, { PropTypes } from 'react';
import styles from './price.css';
import commonStyles from '../../../../resources/common.css';
import verticalFilterStyles from '../vertical-filters.css';

const getLabel = (min, max) => `Rs. ${parseInt(min, 10)} to Rs. ${parseInt(max, 10)}`;
const isChecked = (checkedItems = [], itemId) => checkedItems.includes(itemId);

const Price = ({ data, checkedData, title, handleChange }) => {
  const listItems = data.map((priceObject, index) => {
    const { id, count } = priceObject;
    const [rangeMin, rangeMax] = id.split(' TO ');

    return (
      <li key={index}>
        <label className={`${commonStyles.customCheckbox} ${verticalFilterStyles.label}`}>
          <input
            className={styles.input}
            type="checkbox"
            value={id}
            onChange={handleChange}
            checked={isChecked(checkedData, id)} />{getLabel(rangeMin, rangeMax)}
          <span className={styles.num}>({count})</span>
          <div className={commonStyles.checkboxIndicator}></div>
        </label>
      </li>
    );
  });

  return (
    <div className={verticalFilterStyles.filters}>
      <span className={verticalFilterStyles.header}>{title}</span>
      <ul className={styles.list}>{listItems}</ul>
    </div>
  );
};

Price.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array,
  checkedData: PropTypes.array,
  handleChange: PropTypes.func
};

export default Price;
