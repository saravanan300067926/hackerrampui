import React, { PropTypes, Component } from 'react';
import styles from './atsa.css';
import get from 'lodash/get';
import commonStyles from '../../../../resources/common.css';
import Track from '../../../../utils/track';
import at from 'v-at';
import { getFilterId } from '../../helpers';

function getFilterTitle(filterId) {
  if (filterId.toLowerCase() === 'global size' || filterId.toLowerCase() === 'size_facet') {
    return 'Size';
  }
  return getFilterId(filterId);
}

class Atsa extends Component {
  constructor(props) {
    super(props);
    let selectedFilter = '';

    for (const filter of props.data) {
      const values = filter.filterValues;
      const checkedValues = filter.checkedValues;
      if (
        values.find(value => checkedValues.includes(value.id))
      ) {
        selectedFilter = filter.id;
        break;
      }
    }

    this.state = {
      selectedFilter,
      expanded: false
    };
    this.handleClickFilter = this.handleClickFilter.bind(this);
    this.handleClickValue = this.handleClickValue.bind(this);
    this.showAllFilters = this.showAllFilters.bind(this);
  }

  handleClickFilter = event => {
    const value = event.target.value;
    const selectedFilter = this.state.selectedFilter;

    if (value === selectedFilter) {
      this.setState({
        selectedFilter: ''
      });
      this.props.atsaExpandHandler(false);
    } else {
      this.setState({
        selectedFilter: value
      });
      this.props.atsaExpandHandler(true);
    }
  };

  handleClickValue = event => {
    const option = event.target.value;
    const checked = event.target.checked;
    const filters = this.props.data;
    const selectedFilters = this.state.selectedFilter;

    const group = filters
      .find(
        element => element.id === selectedFilters
      ).id;

    this.props.handleChange({
      option,
      checked,
      group
    });

    Track.event('ListPage', checked ? 'FilterATSAOn' : 'FilterATSAOff', `${group} | ${option}`);
  };


  showAllFilters(e) {
    e.stopPropagation();
    this.setState({
      expanded: true
    });
  }

  render() {
    let moreButton;
    let filters = this.props.data.sort((a, b) => {
      if (!a.id && !b.id) {
        return 0;
      }
      const comparison = a.id.toLowerCase().localeCompare(b.id.toLowerCase()); /* case insensitive comparison */
      if (comparison === 0) {  /* If strings are equal in case insensitive comparison */
        return a.id.localeCompare(b.id); /* Return case sensitive comparison instead */
      }
      return comparison; /* Otherwise return result */
    });
    const selectedFilter = this.state.selectedFilter;
    const atsaFilterClasses = [styles.filters];
    const atsaValuesClasses = [styles.values];
    const atsaContainerClasses = [styles.atsaFiltersOptionCtn];

    if (!this.state.expanded && filters && filters.length > 7) {
      filters = filters.slice(0, 7);
      moreButton = (
        <div className={styles.more} onClick={this.showAllFilters}>
          <span >+ {this.props.data.length - 7} more</span>
        </div>
      );
    }

    const listItems = filters.map(
      (element, index) => (
        <li key={index}>
          <label className={element.id === selectedFilter ? styles.selected : ''}><input
            type="radio"
            value={element.id}
            onChange={this.handleClickFilter} />
            <h4 className={styles.title}>{getFilterTitle(element.id)} </h4>
            <span className={`${element.id === selectedFilter ? styles.upArrow : styles.downArrow} myntraweb-sprite`} />
          </label>
        </li>
      )
    );

    if (listItems.length === 0) {
      atsaFilterClasses.push(styles.hidden);
    }

    const selectedFilterGroup = filters.find(element => element.id === selectedFilter);
    let values = get(selectedFilterGroup, 'filterValues', []);
    const checkedValues = get(selectedFilterGroup, 'checkedValues', []);

    values = values.filter((element) => {
      const option = at(element, 'id');
      if (option) {
        return option.toLowerCase() !== 'na';
      }
      return false;
    });

    const valueItems = values.map(
      (element, index) => (
        <li key={index}>
          <label className={commonStyles.customCheckbox} data-key={at(selectedFilterGroup, 'key')}>
            <input
              type="checkbox"
              value={element.id}
              checked={checkedValues.includes(element.id)}
              onChange={this.handleClickValue} />{element.id}
            <div className={commonStyles.checkboxIndicator}></div>
          </label>
        </li>
      )
    );

    if (valueItems.length === 0) {
      atsaValuesClasses.push(styles.hidden);
    }

    if (valueItems.length > 0) {
      atsaContainerClasses.push(styles.atsaShowContainer);
    }

    return (
      <div className={styles.base}>
        <ul className={atsaFilterClasses.join(' ')}>
          {listItems}
          <li>{moreButton}</li>
        </ul>
        <div className={atsaContainerClasses.join(' ')}>
          <ul className={atsaValuesClasses.join(' ')}>{valueItems}</ul>
        </div>
      </div>
    );
  }
}

Atsa.propTypes = {
  data: PropTypes.array,
  handleChange: PropTypes.func,
  atsaExpandHandler: PropTypes.func
};

export default Atsa;
