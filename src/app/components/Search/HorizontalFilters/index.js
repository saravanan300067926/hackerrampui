import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import styles from './horizontal-filters.css';
import Sort from './Sort';
import Atsa from './Atsa';
import Modal from './Sort/modal';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';
import FilterSummary from './FilterSummary';
import Track from '../../../utils/track';

class HorizontalFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      isOpen: false
    };
    this.toggleExpand = this.toggleExpand.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.trackSortClick = this.trackSortClick.bind(this);
  }

  toggleModal() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  toggleExpand(value) {
    this.setState({
      expanded: value
    });
  }

  trackSortClick() {
    Track.event('ListPage', 'sort', 'delivery_time');
  }

  render() {
    const baseClasses = this.props.isClearAll ? [styles.baseOnClearAllHover] : [styles.base];
    if (this.state.expanded || this.props.selectedFilters.length > 0) {
      baseClasses.push(styles.hideBorder);
      baseClasses.push(styles.addBoxShadow);
    }

    return (
      <section className={baseClasses.join(' ')}>
        <div className={styles.filtersFirstContainer}>
          <div className={styles.sortContainer}>
            <div className={styles.sort}>
              <Sort
                show={this.state.isOpen}
                onShow={this.toggleModal}
                trackSortClick={this.trackSortClick}
                pincode={this.props.pincode}
                data={this.props.sort}
                handleChange={this.props.updateSort} />
            </div>
          </div>
          <Atsa
            atsaExpandHandler={this.toggleExpand}
            data={this.props.filters}
            handleChange={this.props.updateAtsa} />
          <Modal
            handleChange={this.props.updateModal}
            show={this.state.isOpen}
            onClose={this.toggleModal} />
        </div>
        <FilterSummary expanded={this.state.expanded} />
      </section>
    );
  }
}

HorizontalFilters.propTypes = {
  count: PropTypes.number,
  title: PropTypes.string,
  updateSort: PropTypes.func,
  updateModal: PropTypes.func,
  updateAtsa: PropTypes.func,
  sort: PropTypes.array,
  filters: PropTypes.array,
  searchMessage: PropTypes.string,
  isClearAll: PropTypes.bool,
  pincode: PropTypes.string,
  selectedFilters: PropTypes.array
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HorizontalFilters);
