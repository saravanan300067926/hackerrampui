import get from 'lodash/get';
import filterSummartStatePropMapper from './FilterSummary/mapStateToProps';
import {
  combineReducersState,
  normalizeFilters,
  arrayToObject,
  getSearchTerm,
  injectCheckedFilters,
  getCleanedUpFilters
} from '../helpers';

const mapStateToProps = state => {
  const combinedState = combineReducersState(state);
  const primaryFilters = arrayToObject(combinedState.results.filters.primaryFilters);
  const appliedSecondaryFilters = arrayToObject(get(combinedState, 'results.appliedParams.filters', {}));
  let secondaryFilters = arrayToObject(combinedState.results.filters.secondaryFilters);
  secondaryFilters = injectCheckedFilters(secondaryFilters, appliedSecondaryFilters);
  const selectedFilters = filterSummartStatePropMapper(state).data;

  const filters = { ...primaryFilters, ...secondaryFilters };
  const normalizedFilters = normalizeFilters(filters);
  const atsaFilters = getCleanedUpFilters(normalizedFilters);
  const title = getSearchTerm(combinedState);
  const searchMessage = get(combinedState, 'searchMessage');

  return {
    sort: combinedState.sort.values,
    count: combinedState.results.totalCount,
    title,
    filters: atsaFilters,
    searchMessage,
    pincode: combinedState.pincode,
    selectedFilters
  };
};

export default mapStateToProps;
