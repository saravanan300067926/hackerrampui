import Track from '../../../utils/track';

const mapDispatchToProps = dispatch => ({
  updateSort(e) {
    Track.event('ListPage', 'Sort', e.target.value);
    dispatch({
      type: 'SORT_UPDATED',
      data: {
        option: e.target.value,
        checked: e.target.checked,
        group: 'Sort'
      }
    });
  },
  updateAtsa(data) {
    dispatch({
      type: 'CHECKBOX_UPDATED',
      data
    });
  },
  updateModal(sortOption, pincode) {
    dispatch({
      type: 'SORT_UPDATED',
      data: {
        option: sortOption,
        checked: true,
        group: 'Sort',
        pincode
      }
    });
  }
});

export default mapDispatchToProps;
