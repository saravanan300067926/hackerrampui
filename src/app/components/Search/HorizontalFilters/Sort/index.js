import React, { PropTypes } from 'react';
import styles from './sort.css';
import { isBrowser } from '../../../../utils';

const featureGates = isBrowser() ? window.__myx_features__ || {} : {};
const sortDeliveryEnabled = featureGates['search.enableSortDeliveryTime'] === 'true';

const Sort = ({ data, handleChange, onShow, pincode, trackSortClick }) => {
  let selectedSort = 'Recommended';
  let selectedOption = null;
  const listItems = data.map((sortObject, index) => {
    if (sortObject.checked) {
      selectedSort = sortObject.title;
      selectedOption = sortObject.option;
    }
    if (sortObject.option === 'delivery_time') {
      return sortDeliveryEnabled ?
        (
        <li key={index}>
          <label className={`${styles.label} ${sortObject.checked ? styles.selected : ''}`}>
            <input
              type="radio"
              value={sortObject.option}
              checked={sortObject.checked}
              onClick={() => {
                onShow();
                trackSortClick();
              }} />
            {sortObject.title}
          </label>
        </li>
        ) : null;
    }
    return (
      <li key={index}>
        <label className={`${styles.label} ${sortObject.checked ? styles.selected : ''}`}>
          <input
            type="radio"
            value={sortObject.option}
            checked={sortObject.checked}
            onChange={handleChange} />
          {sortObject.title}
        </label>
      </li>
    );
  });
  if (selectedOption === 'delivery_time') {
    return (
      <div>
        <div className={styles.sortBy}>
          Sort by : <span>{selectedSort} - {pincode}</span>
          <span className={`myntraweb-sprite ${styles.downArrow}`}></span>
          <ul className={styles.list}>{listItems}</ul>
        </div>
      </div>
    );
  }
  return (
    <div>
      <div className={styles.sortBy}>
        Sort by : <span>{selectedSort}</span>
        <span className={`myntraweb-sprite ${styles.downArrow}`}></span>
        <ul className={styles.list}>{listItems}</ul>
      </div>
    </div>
  );
};

Sort.propTypes = {
  data: PropTypes.array,
  onShow: PropTypes.func,
  trackSortClick: PropTypes.func,
  show: PropTypes.bool,
  handleChange: PropTypes.func,
  pincode: PropTypes.string
};

export default Sort;
