import React from 'react';
import PropTypes from 'prop-types';
import styles from './modal.css';

const haveLocalStorage = typeof localStorage !== 'undefined' && typeof localStorage === 'object';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pincode: '',
      isPinTure: false,
      isSubmmitPressed: false
    };
    this.handlePinValue = this.handlePinValue.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.storeInBrowserSession = this.storeInBrowserSession.bind(this);
  }

  storeInBrowserSession(data) {
    try {
      if (haveLocalStorage && data) {
        localStorage.setItem(data.label, data.content);
      }
    } catch (e) {
      console.log('Something wrong with localStorage while storing, ', e);
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ isSubmmitPressed: true });
    const value = this.state.pincode;
    const intValue = parseInt(value, 10);
    if (intValue.toString().length === 6) {
      this.setState({
        isPinTure: false,
        isSubmmitPressed: false
      }, () => {
        this.storeInBrowserSession({ label: 'pincode', content: intValue });
        this.props.handleChange('delivery_time', intValue);
        this.props.onClose();
      });
    }
  }

  handlePinValue(event) {
    const value = event.target.value;
    const intValue = parseInt(value, 10);

    if (!isNaN(intValue) && intValue && intValue.toString().length < 7) {
      this.setState({ pincode: intValue });
    }

    if (value === '') {
      this.setState({
        pincode: value
      });
    }
  }
  render() {
    if (!this.props.show) {
      return null;
    }
    let validtyNotification = styles.disable;
    if (this.state.isSubmmitPressed && !this.state.isPinTure) validtyNotification = styles.validtyNotification;
    return (
      <div className={styles.backdropStyle} onClick={this.props.onClose}>
        <div className={styles.modalStyle} onClick={(e) => e.stopPropagation()}>
          <div className={styles.title}>
            Check Delivery Time
          </div>
          <div className={styles.descrip}>
          Enter your PIN Code to Sort & Filter items according to preferred delivery time
          </div>
          <form onSubmit={this.handleSubmit} className={styles.form} autoComplete="off">
            <input
              type="text"
              placeholder="Enter pincode"
              className={styles.pinContainer}
              onChange={this.handlePinValue}
              value={this.state.pincode}
              name="pincode">
            </input>
            <input type="submit" className={styles.check} value="CHECK" />
          </form>
          <div className={validtyNotification}>
            Please enter a valid PIN Code
          </div>
          <a href="#" className={styles.close} onClick={this.props.onClose} />
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func,
  show: PropTypes.bool,
  handleChange: PropTypes.func,
  children: PropTypes.node

};

export default Modal;
