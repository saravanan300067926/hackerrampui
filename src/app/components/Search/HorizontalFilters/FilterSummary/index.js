import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import styles from './filter-summary.css';
import colourMap from '../../VerticalFilters/Colour/colourMap';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';

const bindClassNames = classNames.bind(styles);

const getColourStyle = colourName => ({
  backgroundColor: `${colourMap[colourName.replace(' ', '-')]}`
});

const getPriceLabel = label => {
  const range = label.split(' TO ');
  const from = parseInt(range[0], 10);
  const to = parseInt(range[1], 10);
  return `Rs. ${from} To Rs. ${to}`;
};

const getDiscountLabel = discount => {
  const range = discount.split(' TO ');
  const from = parseFloat(range[0]);
  return `${Math.round(from)} % and above`;
};

class FilterSummary extends Component {
  constructor(props) {
    super(props);
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  getColourClasses(colour) {
    const isMultiColour = colour === 'Multi';
    return bindClassNames({
      colourBox: true,
      'myntraweb-sprite': isMultiColour,
      'multiColorBox': isMultiColour
    });
  }

  handleOnChange(e) {
    const option = e.target.value;
    const checked = e.target.checked;
    const group = e.target.dataset.group;

    this.props.updateFilters({
      option,
      checked,
      group
    });
  }

  render() {
    const filters = this.props.data;
    const filterCtnClasses = [styles.selectedFilterContainer];
    const filterItems = filters.map(
      (element, index) => {
        let filterName = element.id;
        const group = element.group.toLowerCase();

        let input = (
          <input
            type="checkbox"
            value={element.id}
            checked={element.checked || true}
            data-group={element.group}
            onChange={this.handleOnChange} />
        );

        if (group === 'color') {
          filterName = (
            <span>
              <span
                data-colorhex={element.id.toLowerCase()}
                className={this.getColourClasses(element.id)}
                style={getColourStyle(element.id.toLowerCase())} />
                {element.id}
            </span>
            );
        } else if (group === 'discount range') {
          filterName = <span> {getDiscountLabel(element.id)}</span>;
        } else if (group === 'price') {
          filterName = getPriceLabel(element.id);
          input = (
            <input
              type="checkbox"
              value={element.id}
              onChange={this.handleOnChange}
              data-group={element.group}
              checked={element.checked || true} />
          );
        }

        return (
          <li key={index}>
            <div className={styles.filter}>
              {filterName}
              <label className={styles.removeFilter}>
                {input}
                <span className={`myntraweb-sprite ${styles.removeIcon}`} />
              </label>
            </div>
          </li>
        );
      }
    );

    if (filterItems.length === 0) {
      filterCtnClasses.push(styles.hidden);
    }
    if (this.props.expanded) {
      filterCtnClasses.push(styles.reduceTopPadding);
    }

    return (
      <div className={filterCtnClasses.join(' ')}>
        <ul className={styles.filterList}>
          {filterItems}
        </ul>
      </div>
    );
  }
}

FilterSummary.propTypes = {
  data: PropTypes.array,
  updateFilters: PropTypes.func,
  expanded: PropTypes.bool
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterSummary);
