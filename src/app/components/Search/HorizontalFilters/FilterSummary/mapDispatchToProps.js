import Track from '../../../../utils/track';

const genericFilters = ['categories', 'brand', 'price', 'colour', 'discount'];

const getGaEventAction = (group) => {
  let typeOfFilter;

  if (genericFilters.indexOf(group.toLowerCase()) > -1) {
    typeOfFilter = 'FilterGenericOff';
  } else {
    typeOfFilter = 'FilterATSAOff';
  }

  return typeOfFilter;
};

const mapDispatchToProps = dispatch => ({
  updateFilters(data) {
    let type = 'CHECKBOX_UPDATED';

    if (data.group === 'Discount Range') {
      type = 'RADIOBUTTON_UPDATED';
    } else if (data.group === 'Price') {
      type = 'PRICE_UPDATED';
    }

    dispatch({
      type,
      data
    });

    Track.event('ListPage', getGaEventAction(data.group), `${data.group} | ${data.option}`);
  }
});

export default mapDispatchToProps;
