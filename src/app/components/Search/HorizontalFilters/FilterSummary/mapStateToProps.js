import get from 'lodash/get';
import differenceBy from 'lodash/differenceBy';
import remove from 'lodash/remove';
import { arrayToObject, combineReducersState, injectCheckedFilters, injectCheckedRangeFilter } from '../../helpers';

let filterSummary = [];

const mapStateToProps = state => {
  const combinedState = combineReducersState(state);

  const { rangeFilters, primaryFilters, secondaryFilters } = combinedState.results.filters;
  const summaryFilters = arrayToObject([...primaryFilters, ...secondaryFilters]);

  const appliedSummaryFilters = arrayToObject(get(combinedState, 'results.appliedParams.filters', {}));
  const appliedRangeFilters = arrayToObject(get(combinedState, 'results.appliedParams.rangeFilters', {}));


  const filterSummaryState = injectCheckedFilters(summaryFilters, appliedSummaryFilters);
  const rangeFilterSummaryState = injectCheckedRangeFilter(arrayToObject(rangeFilters), appliedRangeFilters);

  const filters = [...Object.values(filterSummaryState), ...Object.values(rangeFilterSummaryState)];

  const selectedFilters = [];

  const exclude = {
    gender: '',
    department: '',
    sections: '',
    subcategories: '',
    sort: ''
  };

  filters.filter(
    element => (
      !(element.id.toLowerCase() in exclude) && (element.filterValues.length > 0)
    )
  ).forEach((filter) => {
    const filterValues = filter.filterValues || [];
    const checkedValues = filter.checkedValues || [];
    if (filterValues.length > 0) {
      const self = filter;
      filterValues.forEach((value) => {
        if (checkedValues.includes(value.id)) {
          value.group = self.id;
          value.value = value.id;
          selectedFilters.push(value);
        }
      });
    }
  });

  // filter that need to be added to summary
  const toBeAdded = differenceBy(selectedFilters, filterSummary, 'id');
  if (toBeAdded.length > 0) {
    const cloneSummary = [...filterSummary];
    toBeAdded.forEach((value) => {
      cloneSummary.push(value);
    });
    filterSummary = [...cloneSummary];
  }

  // filter that need to be removed from summary
  const toBeRemoved = differenceBy(filterSummary, selectedFilters, 'id');
  if (toBeRemoved.length > 0) {
    const cloneSummary = [...filterSummary];
    toBeRemoved.forEach((value) => {
      const compare = {};
      compare.id = value.id;
      remove(cloneSummary, compare);
    });
    filterSummary = [...cloneSummary];
  }

  return {
    data: filterSummary
  };
};

export default mapStateToProps;
