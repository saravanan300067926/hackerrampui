import React, { PropTypes } from 'react';
import { combineReducersState } from './helpers';
import { connect } from 'react-redux';
import get from 'lodash/get';
import PageNotFound from '../PageNotFound';
import Desktop from './DesktopSearch';

import fireEvents from '../FireCustomEvents';

const mapStateToProps = state => {
  const combinedState = combineReducersState(state);
  let pageNotFound = false;
  const products = get(combinedState, 'results.products');
  if (
    get(combinedState.results, 'totalCount') === 0
    || typeof (combinedState.results) === 'undefined'
    || (products && products.length === 0)
  ) {
    pageNotFound = true;
  }
  return {
    pageNotFound
  };
};


class Search extends React.Component {



  render() {
    const { pageNotFound } = this.props;
    if (pageNotFound) {
      return <PageNotFound />;
    }
    return (
      <Desktop />
    );
  }
}

Search.propTypes = {
  pageNotFound: PropTypes.bool
};

export default connect(
  mapStateToProps
)(Search);
