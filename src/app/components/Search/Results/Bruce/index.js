import React from 'react';


import Style from './voice.css';
import Client from '../../../../services';

require('./microsoft.cognitiveservices.speech.sdk.bundle-min.js');

const Mic = props => {
    return <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
        viewBox="0 0 435.2 435.2"  {...props}>
        <g>
            <g>
                <path d="M356.864,224.768c0-8.704-6.656-15.36-15.36-15.36s-15.36,6.656-15.36,15.36c0,59.904-48.64,108.544-108.544,108.544
           c-59.904,0-108.544-48.64-108.544-108.544c0-8.704-6.656-15.36-15.36-15.36c-8.704,0-15.36,6.656-15.36,15.36
           c0,71.168,53.248,131.072,123.904,138.752v40.96h-55.808c-8.704,0-15.36,6.656-15.36,15.36s6.656,15.36,15.36,15.36h142.336
           c8.704,0,15.36-6.656,15.36-15.36s-6.656-15.36-15.36-15.36H232.96v-40.96C303.616,355.84,356.864,295.936,356.864,224.768z"/>
            </g>
        </g>
        <g>
            <g>
                <path d="M217.6,0c-47.104,0-85.504,38.4-85.504,85.504v138.752c0,47.616,38.4,85.504,85.504,86.016
           c47.104,0,85.504-38.4,85.504-85.504V85.504C303.104,38.4,264.704,0,217.6,0z"/>
            </g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
    </svg>
}


class Bruce extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentText: '',
            listening: false,
            final: false
        };
        ['setText', 'getSpeech', 'sendMessage', 'readMessage'].forEach(method => this[method] = this[method].bind(this));
    }



    getSpeech() {
        this.setState({ listening: true });
        const speechConfig = SpeechSDK.SpeechConfig.fromSubscription('54682099f25f44e3a75f4ba95c283804', 'centralindia');
        speechConfig.speechRecognitionLanguage = "en-US";
        const audioConfig = SpeechSDK.AudioConfig.fromDefaultMicrophoneInput();
        const recognizer = new SpeechSDK.SpeechRecognizer(speechConfig, audioConfig);
        recognizer.recognizeOnceAsync(
            (result) => {
                this.setState({ currentText: result.text, listening: false });
                recognizer.close();
                this.sendMessage(result.text);
            },
            (err) => {
                this.setState({ listening: false });
                recognizer.close();
            });
    }

    sendMessage(text) {
        Client.get('/pushMessage', { uidx: "1233", userMessage: text, timestamp: (new Date()).getTime() }).end((err, response) => {
            if (err) {

            } else {
                const { messageEntityList = [], actionType, targetURL } = response.body;
                const length = messageEntityList.length;
                this.readMessage(messageEntityList[length - 1]);
                if (actionType) {
                    switch (actionType) {
                        case 'goto':
                            if (['giftcard', 'checkout/cart', 'my/orders'].indexOf(targetURL) > -1) {
                                window.location.replace("www.stage.myntra.com/" + targetURL);
                            }
                            window.location = targetURL;
                            break;
                    }
                }
            }
        })
    }

    readMessage(messageEntityList) {
        let messageToBeRead = "", soundContext;
        if (messageEntityList && messageEntityList.sender !== 'me') {
            messageToBeRead = (messageEntityList || {}).message;
        }
        const AudioContext = window.AudioContext || window.webkitAudioContext || false;
        if (AudioContext) {
            soundContext = new AudioContext();
        } else {
            alert("AudioContext not supported");
        }
        const speechConfig = SpeechSDK.SpeechConfig.fromSubscription('54682099f25f44e3a75f4ba95c283804', 'centralindia');
        const synthesizer = new SpeechSDK.SpeechSynthesizer(speechConfig);
        synthesizer.speakTextAsync(
            messageToBeRead,
            function (result) {
                if (result.audioData && soundContext) {
                    var source = soundContext.createBufferSource();
                    soundContext.decodeAudioData(result.audioData, function (newBuffer) {
                        source.buffer = newBuffer;
                        source.connect(soundContext.destination);
                        source.start(0);
                    });
                }

                synthesizer.close();
            },
            function (err) {
                window.console.log(err);

                synthesizer.close();
            });
    };



    setText(text) {
        this.setState({ currentText: text });
    }

    render() {
        return <div className={Style.container} onClick={this.getSpeech}>
            {this.state.currentText && <div className={Style.textContainer}><div className={Style.text}>{this.state.currentText}</div></div>}
            <Mic className={`${Style.mic} ${this.state.listening ? Style.listening : Style.passive}`} height={24} width={24} />
        </div>
    }
}

export default Bruce;