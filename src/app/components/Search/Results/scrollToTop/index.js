import React from 'react';
import scroll from '@myntra/myx/lib/scroll';
import classNames from 'classnames/bind';
import { pageScroll, windowHeight } from '../../../../utils/dom';
import styles from './scrollToTop.css';

const isBrowser = (typeof window !== 'undefined');
const bindClassNames = classNames.bind(styles);

class ScrollToTop extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false
    };

    this.clickHandler = this.clickHandler.bind(this);
    this.scrollHandler = this.scrollHandler.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrollHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollHandler);
  }

  scrollHandler() {
    if (isBrowser && (windowHeight() * 1.5) < pageScroll().top) {
      this.setState({
        show: true
      });
    } else if (this.state.show) {
      this.setState({
        show: false
      });
    }
  }

  clickHandler() {
    if (isBrowser) {
      scroll(0);
    }
  }

  render() {
    const scrollDivClasses = bindClassNames({
      button: true,
      'myntraweb-sprite': true,
      isVisible: this.state.show
    });

    return (
      <div
        onClick={this.clickHandler}
        className={scrollDivClasses} />
    );
  }
}

export default ScrollToTop;
