import React, { PropTypes } from 'react';
import Product from './Product';
import styles from './results.css';
import { connect } from 'react-redux';
import mapStateToProps from './mapStateToProps';
import mapDispatchToProps from './mapDispatchToProps';
import Notify from '../../Notify';
import Loader from '../../Loader';
import HalfCardContainer from '../../HalfCard';
import ScrollToTop from './scrollToTop';
import { isBrowser, getFeatures } from '../../../utils';
import { getMyx, isAbEnabled } from '../../../services/RemoteConfig';
import isEmpty from 'lodash/isEmpty';
import { inPriceRevealMode, getSlots, isShowSlotPopup } from '../../../utils/slotUtils';
import { getWishlistSummaryMap, addProductToWlSummaryStore } from '../../../utils/wishlistSummaryManager';
import at from 'v-at';
import bus from 'bus';
import Track from '../../../utils/track';
import Madalytics from 'madalytics-web';
import AdmissionControl from '../../AdmissionControl';
import { securify } from '../../../utils/securify';
import getLoyalty from '../../../utils/loyalty';
import Image from './Product/Image';
import Pagination from './Pagination/Pagination';
import Client from '../../../../app/services';
import { getItem, setItem, removeItem } from '../../../utils/localStorageUtil.js';
import {
  getLocalImageTagPrioritizer,
  getLocalMiscDataPrioritizer
} from '../../../utils/xceleratorUtil.js';
import ChatBot from './Chatbot';
import Bruce from './Bruce';

import fireEvents from '../../FireCustomEvents';

const secure = securify();
const wishlistEnabled = getFeatures('wishlist.enable') === 'true';
const recommendationEnabled = getFeatures('pdp.recommendation.enable') === 'true';
const priceRevealMode = isBrowser() ? inPriceRevealMode() : null;
const abConfigXceleratorTag = {
  key: 'web.xceleratorTags',
  control: 'disabled',
  test: 'enabled'
};

if (isBrowser()) {
  window.Madalytics = Madalytics;
}

class Results extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      activeCardId: null,
      isLoggedIn: false,
      wishlistSummary: {},
      showSlotPopup: false,
      similarItems: null,
      enableProductScroller: true,
      hasXceleratorTag: false
    };
    this.notify = this.notify.bind(this);
    this.loading = this.loading.bind(this);
    this.slotPopupHandler = this.slotPopupHandler.bind(this);
    this.setactiveCardId = this.setactiveCardId.bind(this);
    this.setWishlistSavedState = this.setWishlistSavedState.bind(this);
    this.getMoreProducts = this.getMoreProducts.bind(this);
    this.hideSaleSlot = this.hideSaleSlot.bind(this);
    this.onToggle = this.onToggle.bind(this);
    this.showSimilarData = this.showSimilarData.bind(this);
    this.getProductImageUrl = this.getProductImageUrl.bind(this);
    this.mapSimilarItemstoProduct = this.mapSimilarItemstoProduct.bind(this);
    this.addToWishlist = this.addToWishlist.bind(this);
    this.autoWishlist = this.autoWishlist.bind(this);
    this.xceleratorTagCache = {};
  }

  componentDidMount() {
    bus.on('user.loggedIn', (isLoggedIn) => {
      this.setState({
        isLoggedIn: true
      });
      getWishlistSummaryMap(isLoggedIn, this.setWishlistSavedState);
    });
    this.setXceleratorAB();
    getWishlistSummaryMap((at(window, '__myx_session__.isLoggedIn') || this.state.isLoggedIn), this.setWishlistSavedState);

    if (at(window, '__myx_session__')) {
      this.autoWishlist();
      this.madalyticsScreenLoad();
      this.madalyticsProductListload(this.props.data);
    } else {
      bus.on('beacon-data', () => {
        this.autoWishlist();
        setTimeout(() => {
          this.madalyticsScreenLoad();
          this.madalyticsProductListload(this.props.data);
        }, 0);
      });
    }
    document.addEventListener('visibilitychange', () => {
      if (document.hidden !== undefined) {
        this.setState({ enableProductScroller: !document.hidden });
      }
    }, false);

    fireEvents('LIST', 0);
    this.fireEventDuration = (function () {
      const start = (new Date()).getTime();
      return () => {
        const end = (new Date()).getTime();
        fireEvents('LIST', 1, null, end - start);
      }
    })();
  }

  componentDidUpdate(prevState) {
    fireEvents('LIST', 0);
  }


  componentWillUnmount() {
    this.fireEventDuration();
  }


  componentWillReceiveProps(nextProps) {
    this.loading(nextProps.apiFetchInProgress);

    // to scroll the container
    if (!nextProps.apiFetchInProgress) {
      window.scrollTo(0, 0);
    }

    if (nextProps.apiFetchFailed) {
      this.notify('Oops! Something went wrong. Please try again in some time.');
    }
    this.setState({
      activeCardId: null
    });
    this.madalyticsProductListload(nextProps.data);
  }

  onToggle() {
    document.getElementsByTagName('body')[0].style.overflow = 'auto';
    this.setState({ similarItems: null });
  }

  setactiveCardId(index) {
    this.setState({
      activeCardId: index
    });
  }

  setWishlistSavedState(wishlistSummary) {
    if (wishlistSummary) {
      this.setState({
        wishlistSummary
      });
    }
  }

  getMoreProducts(page) {
    Track.event('ListPage', 'Load', `Depth | ${this.props.page + 1}`);
    this.props.getMoreProducts(page);
  }

  getProductImageUrl(imgObj = null, config = {}) {
    if (imgObj) {
      const secureSrc = at(imgObj, 'secureSrc');
      const imageFormula = secureSrc || at(imgObj, 'src');
      const url = imageFormula.replace('($width)', config.width).replace('($height)', config.height).replace('($qualityPercentage)', config.q);
      return secure(url);
    }
    return null;
  }
  getPreLaunchAttr = (systemAttributes = []) => systemAttributes.filter(
    ({ attribute }) => attribute === 'Enable Buy Button'
  )

  setXceleratorAB() {
    getMyx()
      .then(myx => {
        if (isAbEnabled(abConfigXceleratorTag)) {
          this.setState({
            hasXceleratorTag: true
          });
        }
      })
      .catch(() => { });
  }

  getXceleratorTag(productData) {
    const xceleratorTags = {
      imageTag: {},
      titleTag: {}
    };
    const { hasXceleratorTag } = this.state;
    if (!hasXceleratorTag) return xceleratorTags;
    const getLocallyPrioritizedImageTag = getLocalImageTagPrioritizer('plp');
    const getLocallyPrioritizedMiscData = getLocalMiscDataPrioritizer('plp');
    if (productData) {
      const imageTag = getLocallyPrioritizedImageTag(productData);
      const titleTag = getLocallyPrioritizedMiscData(productData);
      xceleratorTags.imageTag = imageTag || {};
      xceleratorTags.titleTag = titleTag || {};
    }
    return xceleratorTags;
  }

  autoWishlist() {
    const autoWishlist = getItem('autoWishlist');
    if (autoWishlist && at(window, '__myx_session__.isLoggedIn')) {
      removeItem('autoWishlist');
      this.addToWishlist(JSON.parse(autoWishlist));
    }
  }

  _sendWishlistAnalytics() {
    if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
      window.Madalytics.send('AddToCollection', {
        type: 'list',
        url: window.location.href,
        variant: 'web',
        name: `${'Shopping Page-Search Result Page '}${window.location.pathname}`,
        'data_set': {
          'entity_name': at(this.state, 'data.name') || at(this.state, 'data.brand.name'),
          'entity_type': 'list',
          'entity_id': at(this.props, 'params.id'),
          category: at(this.props, 'params.category'),
          brand: at(this.props, 'params.brand'),
          style: at(this.props, 'params.style')
        },
        'custom': {
          'v1': 'wishlist'
        }
      });
    }
  }
  hideSaleSlot() {
    this.setState({
      showSlotPopup: false
    });
    document.body.style.overflow = 'auto';
  }

  loading(show) {
    this.setState({
      isLoading: show
    });
  }

  notify(message) {
    this.refs.notify.error(message);
  }

  slotPopupHandler(show) {
    this.setState({
      showSlotPopup: show
    });
  }

  madalyticsScreenLoad() {
    try {
      if (isBrowser) {
        const entityName = `${'Shopping Page-Search Result '}${window.location.pathname.replace('/', '').replace('-', ' ')}`;
        // screenload event to send initial data
        window.Madalytics.send('ScreenLoad', {
          'name': entityName,
          'variant': 'web',
          'data_set': {
            'entity_type': 'list',
            'entity_name': entityName,
            'entity_optional_attributes': {
              'scroll-position': ''
            }
          },
          'type': 'list',
          'url': window.location.href
        });
      }
    } catch (ex) {
      console.log('Something went wrong while sending screenload event ', ex);
    }
  }

  madalyticsProductListload(data) {
    data = data || [];
    try {
      if (isBrowser) {
        const entityName = window.location.pathname.replace('/', '').replace('-', ' ');
        const title = `${'Shopping Page-Search Result '}${document.title}`;
        const dataSet = data && data.map((val) => (
          {
            entity_name: val.product,
            entity_optional_attributes: {
              price: val.originalPrice,
              discounted_price: val.discountedPrice,
              discount: val.discount,
              seller_partner_id: `${val.sellerPartnerId}`
            }
          }
        ));
        // pageload event to send list data
        window.Madalytics.send('Product list loaded', {
          'data_set': {
            'entity_name': entityName,
            'entity_type': 'list'
          },
          'name': title,
          'type': 'list',
          'variant': 'web',
          'url': window.location.href,
          'widget_items': {
            'data_set': {
              'data': dataSet
            }
          }
        });
      }
    } catch (ex) {
      console.log('Something went wrong while sending pageload event ', ex);
    }
  }

  showSimilarData(similarItems) {
    this.setState({ similarItems });
  }

  mapSimilarItemstoProduct(similar) {
    const similarImage = this.getProductImageUrl(similar.defaultImage, {
      width: 210,
      height: 280,
      q: 90
    });
    const { mrp, discounted, discount = null } = similar.price || {};
    return {
      url: similar.landingPageUrl,
      brand: similar.brand ? similar.brand.name : '',
      title: similar.info,
      styleId: similar.id,
      image: similarImage,
      srcSetImg: similarImage,
      searchImage: similarImage,
      additionalInfo: similar.info,
      originalPrice: mrp,
      discountedPrice: discounted,
      discount,
      imageHeight: 230,
      discountPercentage: discount ? discount.label : '',
      loyalty: getLoyalty(similar, 'pdp') // Page type is pdpd because similar items response is simialr to PDP
    };
  }

  addToWishlist(data, callback) {
    fireEvents('LIST', 4);
    if (!at(window, '__myx_session__.isLoggedIn')) {
      setItem('autoWishlist', JSON.stringify(data));
      window.location.href = `/login?referer=${window.location.toString()}`;
      return;
    }

    const endPointUrl = '/web/wishlistapi/addition';
    const headers = { 'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN') };
    const parameters = {
      styleId: data.styleId
    };

    this.loading(true);
    const defaultErrorMessage = 'Oops! Something went wrong. Please try again in some time.';

    Client.post(endPointUrl, parameters, headers).end((err, response) => {
      this.loading(false);
      if (err) {
        if (at(response, 'body.code') === 1004 || at(response, 'body.status.statusCode') === '403') {
          window.location.href = `/login?referer=${window.location.toString()}`;
        } else {
          this.notify(defaultErrorMessage);
        }
      } else {
        if (at(response, 'body.code') === 1004 || at(response, 'body.status.statusCode') === '403') {
          window.location.href = `/login?referer=${window.location.toString()}`;
        }
        if (at(response, 'body.code') === 10003 || at(response, 'body.code') === 1001) {
          this.notify(at(response, 'body.message'));
        } else if ((at(response, 'body.meta.code') !== 200 || at(response, 'body.status.statusType') === 'ERROR') && at(response, 'body.status') !== 200) {
          this.notify(at(response, 'body.meta.errorDetail') || defaultErrorMessage);
        } else {
          this._sendWishlistAnalytics(data);
          if (typeof callback === 'function') {
            callback();
          }
          addProductToWlSummaryStore(parameters.styleID);
          const showSlotPopup = isShowSlotPopup();
          if (showSlotPopup) {
            this.slotPopupHandler(true);
          } else {
            const eventPayload = {
              res: response,
              skuid: parameters.styleID,
              productImage: data.imgSrc
            };
            bus.emit('wishlist.add', eventPayload);
          }
        }
      }
    });
  }

  render() {
    const remainingItems = Array.from(Array((5 - (this.props.data.length % 5))), () => 0);
    const { xceleratorTagCache = {} } = this;
    let ProductList = this.props.data.map((product, index) => {
      let imgSrc;
      let imgSrc2;
      let srcSetImg = product.searchImage;
      const allSkuForSizes = {};
      const sizes = [];
      const inWishlist = this.state.wishlistSummary.hasOwnProperty(at(product, 'styleId'));
      const prelaunchAttr = this.getPreLaunchAttr(product.systemAttributes) || [];
      const isPreLaunch = at(prelaunchAttr[0], 'value') === 'No';
      const { styleId } = product;
      let xceleratorTag;
      if (xceleratorTagCache[styleId]) {
        xceleratorTag = xceleratorTagCache[styleId];
      } else {

        xceleratorTag = this.getXceleratorTag(product);
        if (!isEmpty(xceleratorTag.imageTag) || !isEmpty(xceleratorTag.titleTag)) {
          xceleratorTagCache[styleId] = xceleratorTag;
        }
      }
      if (product.allSkuForSizes) {
        product.allSkuForSizes.forEach((sizeInfo) => {
          const skuId = sizeInfo.skuId;
          const skuSize = sizeInfo.label;
          allSkuForSizes[skuSize] = skuId;
          sizes.push(skuSize);
        });
      }

      return (
        <Product
          index={index}
          page={this.props.page}
          preLaunch={index === 0}
          key={product.styleId}
          xceleratorTag={xceleratorTag}
          cardIndex={index}
          activeCardId={this.state.activeCardId}
          {...product}
          notifyHandler={this.notify}
          loadingHandler={this.loading}
          setactiveCardId={this.setactiveCardId}
          inPriceRevealMode={priceRevealMode}
          imgSrc={imgSrc}
          imgSrc2={imgSrc2}
          srcSetImg={srcSetImg}
          allSkuForSizes={allSkuForSizes}
          sizes={sizes}
          inWishlist={inWishlist}
          wishlistEnabled={wishlistEnabled}
          addToWishlist={this.addToWishlist}
          addProductToWlSummaryStore={addProductToWlSummaryStore}
          showSimilarData={this.showSimilarData}
          slotPopupHandler={this.slotPopupHandler}
          isHoverDisabled={false}
          isProductScrollerEnabled={this.state.enableProductScroller}
          isPreLaunch={isPreLaunch}
          prelaunchAttr={prelaunchAttr}
          isSimilarProduct={false} />
      );
    }).concat(...remainingItems.map((it, index) => <li className={styles.liDummy} key={`dummy-${index}`} />));

    return (
      <section style={{ width: '100%' }}>
        <Loader show={this.state.isLoading} />
        <AdmissionControl
          showSlotPopup={this.state.showSlotPopup}
          hideSaleSlot={this.hideSaleSlot}
          slots={getSlots(this.state.showSlotPopup)} />
        <Notify ref="notify" />
        <ScrollToTop />
        <ul className={styles.base}>{ProductList}</ul>
        <ChatBot />
        <Bruce />
        <div className={styles.showMoreContainer}>
          <Pagination
            currentIndex={this.props.page}
            getMoreProducts={this.getMoreProducts}
            apiFetchInProgress={this.props.apiFetchInProgress}
            totalProducts={this.props.totalProducts} />
        </div>
        {
          recommendationEnabled ? <HalfCardContainer
            on={this.state.similarItems !== null}
            onToggle={this.onToggle}
            title={'Similar Products'}
            render={() => {
              let xceleratorTag = {
                imageTag: {},
                titleTag: {}
              };
              if (this.state.similarItems) {
                return (<div className={styles.similarItemContainer}>
                  <ul className={`${styles.base} ${styles.similarGrid}`}>
                    {this.state.similarItems.length > 0 ? this.state.similarItems.map((item, index) =>
                      (<Product
                        {...this.mapSimilarItemstoProduct(item)}
                        key={`similar-${index}`}
                        isHoverDisabled={true}
                        isSimilarProduct={true}
                        isProductScrollerEnabled={false}
                        overflow={true}
                        xceleratorTag={xceleratorTag}
                        index={index} />)
                    ) : (
                        <li className={styles.noSimilarContainer}>
                          <img
                            height={370}
                            src={'https://constant.myntassets.com/web/assets/img/6fd5fff9-0f04-403c-9006-a3de8d484d8b1540466020183-bg-art-3x.png'}
                            alt={'No Similar Items available'}
                            title={'No Similar Items available'} />
                          <div className={styles.noSimilartext}> No Similar Items </div>
                        </li>
                      )}
                  </ul>
                </div>);
              }
              return null;
            }} /> : null
        }
      </section >
    );
  }
}

Results.propTypes = {
  data: PropTypes.array,
  apiFetchInProgress: PropTypes.bool,
  apiFetchFailed: PropTypes.bool,
  totalRemainingProductsCount: PropTypes.number,
  getMoreProducts: PropTypes.func,
  page: PropTypes.number,
  totalProducts: PropTypes.number
};

export default connect(mapStateToProps, mapDispatchToProps)(Results);
