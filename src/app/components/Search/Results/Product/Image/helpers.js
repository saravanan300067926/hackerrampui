import get from 'lodash/get';
const cloudinaryRoot = 'assets.myntassets.com';

// Refer : https://wicg.github.io/netinfo/#dfn-effective-connection-types
function getEffectiveConnectionType() {
  const navigator =
    (typeof window !== 'undefined' && get(window, 'navigator')) || {};
  const connection =
    navigator.connection ||
    navigator.mozConnection ||
    navigator.webkitConnection ||
    {};
  return connection.effectiveType || '';
}

export function getFeatures(key) {
  const features =
    (typeof window !== 'undefined' && get(window, '__myx_features__')) || {};
  return features[key];
}

export function getImageQuality() {
  const effectiveType = getEffectiveConnectionType();

  if (effectiveType === 'slow-2g' || effectiveType === '2g') {
    return 40; // 40% image quality.
  } else if (effectiveType === '3g') {
    return 60; // 60% image quality.
  } else if (effectiveType === '4g') {
    return 60; // 60% image quality. Since we're making higher DPR images available on such devices.
  }

  return '60'; // 60% image quality. Since networkInformation API is not supported in firefox and safari
}

function stripQAutoParamFromUrl(src) {
  if (src.indexOf('q_auto/') !== -1) {
    const key = 'q_auto/';
    const pos = src.indexOf(key);
    return src.slice(0, pos) + src.slice(pos + key.length);
  }
  return src;
}

export function getProgressiveImage(src, attr = {}) {
  // Firefox and safari does not support network information API so sending dpr = 2 for all requests
  if (!navigator.effectiveType && !attr.dpr) {
    attr.dpr = 2;
  }
  const imgSize = getFeatures('web.images.width') || 210;
  const searchKey = 'assets.myntassets.com/';
  const startPos = src.indexOf(searchKey) + searchKey.length;

  // if URL contains q_auto, remove it,
  src = stripQAutoParamFromUrl(src);

  let cloudinaryProps = Object.keys(attr).reduce(
    (url, key) => (url += `${key}_${attr[key]},`),
    ''
  );
  if (!attr.q) {
    cloudinaryProps += `q_${getImageQuality()},`;
  }
  if (!attr.w) {
    cloudinaryProps += `w_${imgSize},c_limit,`;
  }

  cloudinaryProps += 'fl_progressive/';
  return src.substr(0, startPos) + cloudinaryProps + src.substr(startPos);
}

export function getValuesForSrcSet(src, dprAB) {
  let srcsetValues;
  if (getEffectiveConnectionType() === '4g' && dprAB) {
    srcsetValues = `
    ${getProgressiveImage(src, { f: 'webp', dpr: '1.0' })} ,
    ${getProgressiveImage(src, { f: 'webp', dpr: '1.5' })} 1.5x,
    ${getProgressiveImage(src, { f: 'webp', dpr: '1.8' })} 1.8x,
    ${getProgressiveImage(src, { f: 'webp', dpr: '2.0' })} 2.0x,
    ${getProgressiveImage(src, { f: 'webp', dpr: '2.2' })} 2.2x,
    ${getProgressiveImage(src, { f: 'webp', dpr: '2.4' })} 2.4x,
    ${getProgressiveImage(src, { f: 'webp', dpr: '2.6' })} 2.6x,
    ${getProgressiveImage(src, { f: 'webp', dpr: '2.8' })} 2.8x`;
  } else {
    srcsetValues = `${getProgressiveImage(src, { f: 'webp', dpr: '1.5' })}`;
  }
  return srcsetValues;
}

export function getProtocolRelativeCloudinaryUrl(src) {
  if (src.indexOf('myntra.myntassets.com') !== -1) {
    src = src.replace('myntra.myntassets.com', cloudinaryRoot);
  }
  // make protocol relative
  src = src.replace(/https?:/, '');
  return window.location.protocol + src;
}

export function getRandomBackgroundColors() {
  const backgroundColors = ['#ffedf3', '#fff2df', '#f4fff9', '#e5f1ff'];
  const randNum = Math.floor(Math.random() * 1000) % 4;
  return backgroundColors[randNum];
}
