import React from 'react';
import PropTypes from 'prop-types';
import Lazy from 'react-lazyload';
import {
  getProgressiveImage,
  getProtocolRelativeCloudinaryUrl,
  getValuesForSrcSet,
  getRandomBackgroundColors
} from './helpers';

class Image extends React.PureComponent {


  getPictureTag = () => {
    const imageSrc = getProtocolRelativeCloudinaryUrl(this.props.src);
    return (<picture className="img-responsive" style={{ width: '100%', height: '100%', display: 'block' }}>
      <source
        srcSet={getValuesForSrcSet(imageSrc, this.props.dprAB)}
        type="image/webp" />
      <img
        src={getProgressiveImage(imageSrc)}
        className="img-responsive"
        alt={this.props.alt}
        title={this.props.title}
        style={{ width: '100%', display: 'block' }} />
    </picture>);
  }

  render() {
    let backStyle = this.props.noBack ? {} : { background: getRandomBackgroundColors() };
    return (
      <div
        style={backStyle}>
        {this.props.noLazy ? <div style={{ height: `${this.props.height}px`, width: '100%' }}>{this.getPictureTag()}</div> : <Lazy
          height={this.props.height}
          once={true}
          overflow={this.props.overflow || false}
          offset={this.props.lazyloadVerticalOffset || 100}
          width={"100%"}>{this.getPictureTag()}</Lazy>
        }
      </div>
    );
  }
}

Image.propTypes = {
  noLazy: PropTypes.bool,
  noBack: PropTypes.bool,
  dprAB: PropTypes.bool,
  height: PropTypes.number,
  src: PropTypes.string,
  alt: PropTypes.string,
  title: PropTypes.string
};

export default Image;
