import React, { PropTypes } from 'react';
import config from '../../../../config';
import styles from './product.css';
import Image from './Image';
import commonStyles from '../../../../resources/coin.css';
import Client from '../../../../../app/services';
import bus from 'bus';
import at from 'v-at';
import getKVPairs from '../../../../utils/kvpairs';
import Track from '../../../../utils/track';
import { isShowSlotPopup, inPriceRevealMode } from '../../../../utils/slotUtils';
import StarIcon from '../../../Ratings/StarIcon';
import priorityCheckout, {
  PRIORITY_CHECKOUT_MESSAGE,
  CART_FULL_ERROR_CODE,
  PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE
} from '../../../../utils/priorityCheckoutUtils';
import Slider from 'react-slick';
import { isBrowser, getFeatures } from '../../../../utils';
import SimilarIcon from '../../../Pdp/ImageGrid/SimilarIcon';
import keyBy from 'lodash/keyBy';
import { formatCount } from '../../helpers';
import XceleratorTag from '../../../../common/xceleratorTag';
import fireEvents from '../../../FireCustomEvents';

const recommendationEnabled = getFeatures('pdp.recommendation.enable') === 'true';
const enableImageSlider = getFeatures('list.imageSlider.enable') === 'true';
const ratingsEnabled = getFeatures('web.ratings.enable') === true;
const sellableInventoryModeEnabled = getFeatures('sellableInventoryModeEnabled') || false;


class Product extends React.Component {
  constructor(props) {
    super(props);

    this.showSizeOptions = this.showSizeOptions.bind(this);
    this.hideSizeOptions = this.hideSizeOptions.bind(this);
    this.setSelectedSize = this.setSelectedSize.bind(this);
    this.trackProductClick = this.trackProductClick.bind(this);
    this.trackRightClick = this.trackRightClick.bind(this);
    this.getAvailableSizes = this.getAvailableSizes.bind(this);
    this.onMouseEnterProduct = this.onMouseEnterProduct.bind(this);
    this.onMouseLeaveProduct = this.onMouseLeaveProduct.bind(this);
    this.getSliderComponents = this.getSliderComponents.bind(this);
    this.getDefaultImage = this.getDefaultImage.bind(this);
    this.onShowSimilarClick = this.onShowSimilarClick.bind(this);
    this.getProductStyle = this.getProductStyle.bind(this);
    this.setInWishlist = this.setInWishlist.bind(this);
    this.getActions = this.getActions.bind(this);
    this.getWishlistButton = this.getWishlistButton.bind(this);
    this.onHoverTimeout = null;

    this.state = {
      inWishlist: false,
      selectedSize: null,
      selectedSkuId: null,
      startSlider: false,
      isSliderActive: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.styleId !== nextProps.styleId) {
      this.setState({
        inWishlist: false,
        selectedSize: null,
        selectedSkuId: null
      });
    }
    if (nextProps.isProductScrollerEnabled !== this.props.isProductScrollerEnabled) {
      clearTimeout(this.onHoverTimeout);
      this.onHoverTimeout = null;
      this.setState({
        startSlider: false,
        isSliderActive: false
      });
    }
  }

  onMouseEnterProduct() {
    if (this.imagesOrder.length > 0 && enableImageSlider) {
      this.onHoverTimeout = setTimeout(() => this.setState({ startSlider: true, isSliderActive: true }), 50);
    }
    return null;
  }

  onMouseLeaveProduct() {
    if (this.imagesOrder.length > 0) {
      clearTimeout(this.onHoverTimeout);
      this.onHoverTimeout = null;
      return this.setState({ startSlider: false, isSliderActive: false });
    }
    return null;
  }

  onShowSimilarClick() {
    document.getElementsByTagName('body')[0].style.overflow = 'hidden';
    Track.event('ListPage', 'similar_products_icon');
    const endPointUrl = `${config('recommendations')}/${this.props.styleId}`;
    this.props.loadingHandler(true);

    Client.get(endPointUrl).end((err, response) => {
      this.props.loadingHandler(false);
      if (err) {
        this.props.notifyHandler('Oops! Something went wrong. Please try again in some time.');
      } else {
        const similarLength = at(response, 'body.related.length');
        if (similarLength && similarLength > 0) {
          const related = keyBy(at(response, 'body.related'), 'type');
          const similarItems = at(related, 'Similar.products') || [];
          this.props.showSimilarData(similarItems);
        } else {
          this.props.showSimilarData([]);
        }
      }
    });
  }

  getDefaultImage(view) {
    const { images = [] } = this.props;
    for (let i = 0; i < images.length; i++) {
      if (images[i].view === view) {
        return images[i].src;
      }
    }
    return this.props.searchImage;
  }

  getLoyalty() {
    const loyalty = this.props.loyalty;
    return (
      <div className={styles.loyalty}>
        {`Or pay Rs. ${loyalty.discount} + `}
        <div className={commonStyles.coin} />
        {` ${loyalty.points} Points`}
      </div>
    );
  }

  getPersonalisedPriceCoupon() {
    const PPCoupon = this.props.personalizedCoupon;
    return (
      <div className={styles.PPPromo}>
        {`Pre-applied Deal ${PPCoupon}`}
      </div>
    );
  }

  getPrice() {
    return (
      <div className={styles.price}>
        {this.props.discount ? (
          <span>
            <span className={styles.discountedPrice}>Rs. {this.props.discountedPrice}</span>
            <span className={styles.strike}>Rs. {this.props.originalPrice}</span>
          </span>
        ) : (
            <span>Rs. {this.props.originalPrice}</span>
          )}
        {this.props.discountPercentage && (
          <span className={styles.discountPercentage} dangerouslySetInnerHTML={{ __html: this.props.discountPercentage }} />
        )}
      </div>
    );
  }

  getActions() {
    const isPriorityCheckoutStyle = priorityCheckout() ? { top: '245px', padding: '16px 10px 0px 10px' } : {};
    const isPriceRevealMode = this.props.inPriceRevealMode ? { justifyContent: 'center' } : { ...isPriorityCheckoutStyle };
    const isPreLaunch = at(this.props, 'isPreLaunch');
    return (
      <div className={`${styles.actions} ${isPreLaunch ? styles.prelaunchActions : ''}`} style={isPriceRevealMode}>
        {/* {to be enabled after Seller Integration on Listpage} */}
        {/* {!isPreLaunch && this.getAddToCartButton()} */}
        {this.getWishlistButton()}
      </div>
    );
  }

  getWishlistButton() {
    if (this.props.wishlistEnabled) {
      const isItemWishlisted = this.state.inWishlist || this.props.inWishlist;
      let wishlistStyle = isItemWishlisted || priorityCheckout() ? { padding: '8px' } : {};
      const isPreLaunch = at(this.props, 'isPreLaunch');
      const preLaunchWishlistStyle = isPreLaunch ? { backgroundColor: '#ff3e6c', textAlign: 'center', width: '100%', color: '#fff' } : {};
      const preLaunchWishlistedStyle = isPreLaunch ? { textAlign: 'center', width: '100%' } : {};
      wishlistStyle = isItemWishlisted
        ? { ...wishlistStyle, backgroundColor: '#535766', color: 'white', border: '1px solid #7E818C', ...preLaunchWishlistedStyle }
        : { ...wishlistStyle, ...preLaunchWishlistStyle };
      // True to be removed after Seller Integration on Listpage
      wishlistStyle = true || this.props.inPriceRevealMode ? { ...wishlistStyle, width: '100%', textAlign: 'center' } : { ...wishlistStyle };
      let buttonText = isItemWishlisted ? 'wishlisted' : 'wishlist';
      if (buttonText === 'wishlist' && isPreLaunch) {
        buttonText = 'wishlist now';
      }
      const params = at(this.props, 'params');
      const data = {
        styleId: at(this.props, 'styleId'),
        entity_name: at(this.state, 'data.name') || at(this.state, 'data.brand.name'),
        entity_id: at(params, 'id'),
        category: at(params, 'category'),
        brand: at(params, 'brand'),
        style: at(params, 'style'),
        imgSrc: at(this.props, 'imgSrc')
      };
      return (
        <span
          className={`${styles.actionsButton} ${styles.wishlist} ${isPreLaunch ? styles.prelaunchBtn : ''}`}
          style={wishlistStyle}
          onClick={(event) => {
            event.preventDefault();
            event.stopPropagation();
            if (this.state.inWishlist || this.props.inWishlist) {
              return;
            }
            this.props.addToWishlist(data, this.setInWishlist);
          }}>
          {buttonText}
          {isPreLaunch && <span className={`${isItemWishlisted ? styles.launchDateWishlisted : styles.launchDate}`}>
            {at(getKVPairs('prelaunch.brands'), 'date')}</span>
          }
        </span>
      );
    }
    return null;
  }

  getAddToCartButton() {
    if (!this.props.inPriceRevealMode) {
      const addToBagButton = priorityCheckout() ? (
        <span style={{ padding: '8px', marginRight: '5px' }} className={`${styles.actionsButton} ${styles.addToBag}`} onClick={this.showSizeOptions}>
          <span>Add to bag</span>
          <span className={styles.eorsText}>{PRIORITY_CHECKOUT_MESSAGE}</span>
        </span>
      )
        :
        (
          <span className={`${styles.actionsButton} ${styles.addToBag}`} onClick={this.showSizeOptions}>
            <span>{this.props.isPreOrderItem ? 'pre-order' : 'Add to bag'}</span>
          </span>
        );

      return addToBagButton;
    }
    return null;
  }

  getSizeOptions() {
    const sizeItems = [];
    this.props.sizes.map((size, i) => this.props.inventoryInfo.filter(item => {
      if (item.label === size && item.inventory > 0) {
        const buttonClasses = [styles.sizeButton];

        if (size === this.state.selectedSize) {
          buttonClasses.push(styles.sizeButtonSelected);
        }

        sizeItems.push(
          <button
            key={`sizeDiv${i}`}
            className={buttonClasses.join(' ')}
            onClick={this.setSelectedSize} >
            {size}
          </button>
        );
        return true;
      }
      return false;
    })
    );

    const sizeDisplayClasses = [styles.sizeDisplayDiv];
    if (this.props.loyalty.enabled) {
      sizeDisplayClasses.push(styles.sizeDisplayDivLoyalty);
    }

    if (this.isCardActive()) {
      sizeDisplayClasses.push(styles.showSizeDisplayDiv);
    }

    if (priorityCheckout()) {
      if (this.props.loyalty.enabled) {
        sizeDisplayClasses.push(styles.sizeDisplayPriorityLoyalty);
      } else {
        sizeDisplayClasses.push(styles.sizeDisplayPriority);
      }
    }

    return (
      <div className={sizeDisplayClasses.join(' ')}>
        <div className={styles.sizeDisplayHeader}>
          <span>Select a size</span>
          <span className={`myntraweb-sprite ${styles.sizeDisplayRemoveMark}`} onClick={this.hideSizeOptions} />
        </div>
        <div className={styles.sizeButtonsContaier}>
          {sizeItems}
        </div>
      </div>
    );
  }

  getAvailableSizes() {
    return this.props.inventoryInfo.map((item, index) => {
      const text = (index === this.props.inventoryInfo.length - 1) ? `${item.label}` : `${item.label}, `;
      const inventory = sellableInventoryModeEnabled || inPriceRevealMode() || priorityCheckout() ? item.sellableInventoryCount : item.inventory;
      if (inventory > 0) {
        return (<span className={styles.sizeInventoryPresent} key={`avaialbalesizes-${this.props.styleId}-${item.label}`}>{text}</span>);
      }
      return (<span className={styles.sizeNoInventoryPresent} key={`avaialbalesizes-${this.props.styleId}-${item.label}`}>{text}</span>);
    });
  }

  setSelectedSize(event) {
    event.preventDefault();
    event.stopPropagation();

    const selectedSizeText = event.target.innerText;
    const skuId = parseInt(this.props.allSkuForSizes[selectedSizeText], 10);

    if (skuId) {
      this.setState({
        selectedSize: selectedSizeText,
        selectedSkuId: skuId
      }, this.addToBag);
    }
  }

  getImageComponent(image, slick, index) {
    const defaultTag = {
      imageTag: {},
      titleTag: {}
    };
    const { imageTag: { data: imgTag = '' } = {} } = this.props.xceleratorTag || defaultTag;
    const imageComp = (
      <Image
        height={this.props.imageHeight || 280}
        src={image || this.props.srcSetImg}
        dprAB={true}
        alt={this.props.title}
        title={this.props.title}
        overflow={this.props.overflow || false}
        noLazy={this.props.index <= 10}
        noBack={slick === true} />
    );
    if (slick) {
      return (<div className={styles.slide} key={`imagecomp-${index}`}>
        {imageComp}{imgTag !== '' && <XceleratorTag page="plp" tag={imgTag} position="onImage" />}
      </div>);
    }
    return (<div className={styles.sliderContainer} style={{ display: this.state.isSliderActive && !this.isCardActive() ? 'none' : 'block' }}>
      {imageComp}{imgTag !== '' && <XceleratorTag page="plp" tag={imgTag} position="onImage" />}
    </div>);
  }

  getSliderComponents() {
    let i = 0;
    if (this.imagesOrder.length > 0) {
      for (i = 0; i < this.imagesOrder.length; i++) {
        if (this.imagesOrder[i].src === this.currentImage) {
          break;
        }
      }
      this.imagesOrder = [this.imagesOrder[i], ...this.imagesOrder.slice(0, i), ...this.imagesOrder.slice(i + 1)];
      return this.imagesOrder.map((item, index) => this.getImageComponent(item.src, true, index));
    }
    return this.props.srcSetImg;
  }

  getProductStyle() {
    if (this.isCardActive()) {
      return { display: 'none' };
    } else if (this.props.isHoverDisabled) {
      return { display: 'block' };
    }
    return {};
  }

  getRating() {
    const { rating, ratingCount, isFastFashion } = this.props;
    return ratingsEnabled && rating > 0 && !isFastFashion ? (
      <div className={styles.ratingsContainer}>
        <span>{rating.toFixed(1).substring(2) === '0' ? rating.toFixed(0) : rating.toFixed(1)}</span>
        <StarIcon rating={rating} cssClasses={styles.starIcon} />
        {
          ratingCount && (
            <div className={styles.ratingsCount}>
              <div className={styles.separator}>|</div>
              {formatCount(ratingCount, 1)}
            </div>
          )
        }
      </div>
    ) : null;
  }

  settings = {
    dots: true,
    infinite: !!isBrowser(),
    arrows: false,
    centerMode: true,
    speed: 500,
    slidesToShow: 1,
    draggable: false,
    initialSlide: 0,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1300,
    pauseOnHover: false,
    waitForAnimate: false,
    dotsClass: 'slick-smalldots'
  };

  getExtraInfo() {
    const { titleTag: { data: titleTag = '' } = {} } = this.props.xceleratorTag;

    if (this.props.loyalty.enabled) {
      return this.getLoyalty();
    } else if (this.props.personalizedCoupon) {
      return this.getPersonalisedPriceCoupon();
    } else if (titleTag !== '') {
      return <XceleratorTag page='plp' tag={titleTag} position='inInfo' />;
    }
    return null;
  }

  setInWishlist() {
    this.setState({
      inWishlist: true
    });
  }

  currentImage = this.getDefaultImage('default');

  imagesOrder = Array.isArray(this.props.images) ? this.props.images : [];

  showSizeOptions(event) {
    event.preventDefault();
    event.stopPropagation();
    Track.event('ListPage', 'add_to_bag');
    this.props.setactiveCardId(this.props.styleId);
  }

  hideSizeOptions() {
    this.props.setactiveCardId(null);
  }

  isCardActive() {
    return this.props.activeCardId === this.props.styleId;
  }

  addToBag() {
    if (this.state.selectedSkuId) {
      this.props.loadingHandler(true);
      this.hideSizeOptions();

      const endPointUrl = config('cart');
      const headers = { 'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN') };
      const parameters = {
        skuid: this.state.selectedSkuId,
        styleid: this.props.styleId,
        xsrf: at(window, '__myx_session__.USER_TOKEN')
      };

      Client.post(endPointUrl, parameters, headers).end((err, response) => {
        this.props.loadingHandler(false);
        this.setState({
          selectedSize: null,
          selectedSkuId: null
        });

        const errMsg = 'Oops! Something went wrong. Please try again in some time.';

        if (Client.errorHandler(err, response)) {
          let message = '';

          if (
            priorityCheckout() &&
            at(response, 'body.meta.statusCode') === CART_FULL_ERROR_CODE
          ) {
            message = PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE;
          }

          message =
            message ||
            at(response, 'body.meta.errorDetail') || errMsg;

          this.props.notifyHandler(message);

          this.setState({
            isLoading: false
          });
        } else {
          this.setState({
            isLoading: false
          });

          const showSlotPopup = isShowSlotPopup();
          if (showSlotPopup) {
            this.props.slotPopupHandler(true);
          } else {
            const eventPayload = {
              res: response,
              skuid: parameters.skuid
            };
            bus.emit('cart.add', eventPayload);
          }
        }
      });
    }
  }

  trackProductClick() {
    // send product position in the list = index + 1, as index starts from 0
    if (!this.props.isSimilarProduct) {
      Track.event('ListPage', 'product_click', `${this.props.index + 1}`);
    } else {
      Track.event('ListPage',
        'product_click_similar_product_sidebar', `${this.props.index + 1}`);
    }
    fireEvents('LIST', 2, this.props.index + 1, null, this.props.page);
  }

  trackRightClick() {
    Track.event('ListPage', 'right_click', `product_position | ${this.props.index + 1}`);
  }

  render() {
    const cardStyles = [styles.base];
    const productDescStyle = this.getProductStyle();
    const isHoverDisabled = this.props.isHoverDisabled;
    const { imageTag: { data: imgTag = '' } = {} } = this.props.xceleratorTag;
    let imageComp = this.getImageComponent(this.currentImage);
    let slideComp = this.getSliderComponents();
    if (this.isCardActive()) {
      cardStyles.push(styles.isActive);
    }
    return (
      <li
        className={cardStyles.join(' ')}
        onClick={this.trackProductClick}
        onContextMenu={this.trackRightClick}
        onMouseEnter={this.onMouseEnterProduct}
        onMouseLeave={this.onMouseLeaveProduct}
        key={at(this.props, 'styleId') || at(this.props, 'index')} >
        {this.props.isPreOrderItem && (
          <div className={`myntraweb-sprite ${styles.preOrderLabel}`}>PRE-ORDER</div>
        )}
        <div className={this.isCardActive() ? `${styles.thumbShim} ${styles.thumbShimActive}` : `${styles.thumbShim}`} />
        {this.getRating()}
        <a target={'_blank'} href={this.props.url} style={{ display: 'block' }}>
          <div className={styles.imageSliderContainer}>
            {this.state.startSlider && !this.isCardActive() && !isHoverDisabled ?
              (<div className={styles.sliderContainer}>
                <Slider {...this.settings}>{slideComp}</Slider>
                {imgTag !== '' && <XceleratorTag page={"plp"} tag={imgTag} position={"onImage"} />}
              </div>) : null}
            {imageComp}
          </div>
          <div className={styles.productMetaInfo}>
            <h3 className={styles.brand}>{this.props.brand}</h3>
            <h4 className={styles.product} style={productDescStyle}>{this.props.additionalInfo}</h4>
            {!isHoverDisabled ? <h4 className={styles.sizes}>Sizes: {this.getAvailableSizes()}</h4> : null}
            {this.getPrice()}
            {this.getExtraInfo()}
          </div>
        </a>
        {!isHoverDisabled && recommendationEnabled ? <SimilarIcon
          onShowSimilarClick={this.onShowSimilarClick}
          similarItemsAvailable={true}
          className={styles.similarItemCta} /> : null}
        {!isHoverDisabled ? this.getActions() : null}
        {!isHoverDisabled ? this.getSizeOptions() : null}
      </li>
    );
  }
}

Product.propTypes = {
  url: PropTypes.string,
  brand: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.string,
  originalPrice: PropTypes.number,
  discountedPrice: PropTypes.number,
  discount: PropTypes.number,
  discountPercentage: PropTypes.string,
  styleId: PropTypes.number,
  notifyHandler: PropTypes.func,
  loadingHandler: PropTypes.func,
  slotPopupHandler: PropTypes.func,
  wishlistEnabled: PropTypes.bool,
  allSkuForSizes: PropTypes.object,
  inventoryInfo: PropTypes.array,
  images: PropTypes.array,
  systemAttributes: PropTypes.array,
  sizes: PropTypes.array,
  cardIndex: PropTypes.number,
  setactiveCardId: PropTypes.func,
  activeCardId: PropTypes.number,
  inPriceRevealMode: PropTypes.bool,
  imageEntryDefault: PropTypes.string,
  imgSrc: PropTypes.string,
  imgSrc2: PropTypes.string,
  srcSetImg: PropTypes.string,
  searchImage: PropTypes.string,
  isPreOrderItem: PropTypes.bool,
  inWishlist: PropTypes.bool,
  index: PropTypes.number,
  additionalInfo: PropTypes.string,
  loyalty: PropTypes.object,
  showSimilarData: PropTypes.func,
  isHoverDisabled: PropTypes.bool,
  isProductScrollerEnabled: PropTypes.bool,
  isSimilarProduct: PropTypes.bool,
  imageHeight: PropTypes.number,
  rating: PropTypes.number,
  personalizedCoupon: PropTypes.string,
  ratingCount: PropTypes.number,
  addToWishlist: PropTypes.func,
  isFastFashion: PropTypes.bool,
  xceleratorTag: PropTypes.object
};

Product.defaultProps = {
  isFastFashion: false,
  xceleratorTag: {}
};

export default Product;
