import { combineReducersState } from '../helpers';
import getLoyalty from '../../../utils/loyalty';
import getPdpUrlChangeRegx from '../../../utils/getPdpUrlChangeRegx';

const mapStateToProps = state => {
  const combinedState = combineReducersState(state);
  let products = combinedState.results.products;
  let remainingProducts = 0;
  remainingProducts = combinedState.results.totalCount - (combinedState.page * 50) - 1;
  remainingProducts = remainingProducts > 0 ? remainingProducts : 0;
  const PDP_URL_CHANGE_REGEX = getPdpUrlChangeRegx(window.__myx_kvpairs__);
  products = products.map(item => {
    const {
      productId: styleId,
      brand,
      landingPageUrl: landingPageUrl,
      product: title,
      additionalInfo: additionalInfo,
      inventoryInfo,
      advanceOrderTag,
      productimagetag,
      searchImage,
      discount,
      mrp: originalPrice,
      price: discountedPrice,
      discountDisplayLabel,
      images,
      rating,
      personalizedCoupon,
      ratingCount,
      systemAttributes,
      isFastFashion,
      buyButtonWinnerSellerPartnerId: sellerPartnerId
    } = item;

    const loyalty = getLoyalty(item);
    const pdpUrl = !!PDP_URL_CHANGE_REGEX && PDP_URL_CHANGE_REGEX.test(landingPageUrl)
      ? `${landingPageUrl.replace(/\//g, '-')}.htm` : landingPageUrl;
    return {
      url: pdpUrl ? `${pdpUrl.toLowerCase()}` : `${styleId}`,
      brand,
      title,
      additionalInfo: (additionalInfo || title || '').replace(`${brand} `, ''),
      originalPrice,
      discountedPrice,
      discountPercentage: discountDisplayLabel,
      styleId,
      sellerPartnerId,
      allSkuForSizes: inventoryInfo,
      inventoryInfo,
      isPreOrderItem: advanceOrderTag === 'Pre-order' || productimagetag === 'Pre-order',
      searchImage,
      product: title,
      discount,
      loyalty,
      images: (Array.isArray(images) ? images.map(image => {
        if (image.view === 'default') {
          return { src: searchImage, view: image.view };
        }
        return { src: image.src, view: image.view };
      }) : [{ src: searchImage, view: 'default' }]),
      rating,
      isFastFashion,
      personalizedCoupon,
      ratingCount,
      systemAttributes
    };
  });

  return {
    data: products,
    apiFetchInProgress: combinedState.apiFetchInProgress,
    apiFetchFailed: combinedState.apiFetchFailed,
    totalRemainingProductsCount: remainingProducts,
    totalProducts: combinedState.results.totalCount,
    page: combinedState.page
  };
};

export default mapStateToProps;
