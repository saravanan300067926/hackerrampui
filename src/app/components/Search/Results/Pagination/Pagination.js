import React, { PropTypes } from 'react';
import getPaginationData from './util';
import styles from './pagination.css';
import Jsuri from 'jsuri';

const Pagination = (props) => {
  const paginationData = getPaginationData(props.totalProducts, 50, props.currentIndex);
  const pages = paginationData.pages;

  // fix for not showing only 1 page
  if (pages.length === 1) {
    return null;
  }

  const uri = new Jsuri(window.location.href);

  const buttons = pages.map((item, i) => {
    const itemClass = [styles[item.type]];

    if (!uri.hasQueryParam('p') && item.index > 1) {
      uri.addQueryParam('p', item.index);
    }
    if (uri.hasQueryParam('p') && item.index === 1) {
      uri.deleteQueryParam('p');
    } else if (uri.hasQueryParam('p') && item.index > 1) {
      uri.replaceQueryParam('p', item.index);
    }

    if (item.hasOwnProperty('status') && !item.status) {
      itemClass.push(styles.disabled);
    }

    const tags = {};

    if (item.type === 'prev') {
      tags.rel = 'prev';
    }

    if (item.type === 'next') {
      tags.rel = 'next';
    }

    return (
      <li className={itemClass.join(' ')} key={i} onClick={() => { props.getMoreProducts(item.index); }}>
        <a {...tags} className={styles.pageLink} href={uri.toString()} onClick={(e) => { e.preventDefault(); }}>
          {item.type === 'prev' && (<span className={styles.arrowLeft} />)}
          {item.view}
          {item.type === 'next' && (<span className={styles.arrowRight} />)}
        </a>
      </li>
    );
  });

  return (
    <ul className={styles.container}>
      <li className={styles.paginationMeta}>Page {props.currentIndex} of {paginationData.totalPages}</li>
      {buttons}
    </ul>
  );
};

Pagination.propTypes = {
  currentIndex: PropTypes.number,
  totalProducts: PropTypes.number,
  getMoreProducts: PropTypes.func
};

export default Pagination;
