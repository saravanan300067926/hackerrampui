import React from 'react';
import styles from './FreeShippingBanner.css';
import { getMyx, isNewUserPromise } from '../../../../services/RemoteConfig';
import get from 'lodash/get';
import interactor from './FreeShippingInteractor';
import loadAsync from '../../../../routes/loadAsync';
import Spinner from '../../../Spinner/index.js';

const BannerLoader = () => (
  <div className={styles['banner-expanded']}>
    <div className={styles.spinner}>
      <Spinner />
    </div>
  </div>
);

const ExpandedBanner = loadAsync('expandedFreeShipping', BannerLoader);


function joinStyles(classNames) {
  return classNames
    .map(className => styles[className])
    .reduce((classAcc, className) =>
      `${classAcc} ${className}`,
      '');
}

const SideBar = ({ expanded, toggleExpanded, data }) => {
  if (! data) {
    return null;
  }
  const view = expanded ? 'expanded' : 'collapsed';
  const sideBarStyle = joinStyles(['sidebar', `sidebar-${view}`]);
  const arrowStyle = joinStyles(['arrow', `arrow-${view}`]);

  return (
    <div className={sideBarStyle} onClick={toggleExpanded}>
      <div className={arrowStyle} />
      <p className={styles['sidebar-content']}>
        {data}
      </p>
    </div>
  );
};

SideBar.propTypes = {
  expanded: React.PropTypes.bool,
  toggleExpanded: React.PropTypes.func,
  data: React.PropTypes.string
};

export default class FreeShippingBanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      isNewUser: false,
      isLoggedIn: false
    };

    this.updateState = this.updateState.bind(this);
    this.toggleExpanded = this.toggleExpanded.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    this.updateState();
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  toggleExpanded() {
    if (! this.state.expanded && window.ga) {
      window.ga('send', 'event', 'new_user_onboarding', 'click_offer_card', interactor.getPage());
    }
    this.setState((prevState) => ({ expanded: !prevState.expanded }));
  }

  updateState() {
    getMyx()
      .then(myx => Promise.all([
        Promise.resolve(get(myx, 'session.isLoggedIn')),
        isNewUserPromise()
      ]))
      .then(([isLoggedIn, isNewUser]) => {
        const newState = {
          isLoggedIn,
          isNewUser
        };
        this.setState(newState);
      })
      .catch(() => {});
  }

  handleClickOutside(event) {
    if (this.bannerRef && !this.bannerRef.contains(event.target)) {
      this.setState({ expanded: false });
    }
  }

  render() {
    const data = interactor.getData();
    const enabled = interactor.featureEnabled() && interactor.pageEnabled();

    return this.state.isNewUser && enabled && (
      <div
        ref={ref => {
          this.bannerRef = ref;
        }}>
        <SideBar
          data={data.sidebar}
          expanded={this.state.expanded}
          toggleExpanded={this.toggleExpanded} />
        {this.state.expanded && data &&
          <ExpandedBanner
            isLoggedIn={this.state.isLoggedIn}
            content={data.content}
            image={data.image}
            coupon={data.coupon}
            button={data.button}
            trustBuilders={data.trustBuilders} />
        }
      </div>
    );
  }
}

FreeShippingBanner.propTypes = {
  page: React.PropTypes.string
};
