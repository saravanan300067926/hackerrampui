import React from 'react';
import iconProvider from './FreeShippingIconProvider';
import styles from './FreeShippingBanner.css';

function joinStyles(classNames) {
  return classNames
    .map(className => styles[className])
    .reduce((classAcc, className) =>
      `${classAcc} ${className}`,
      '');
}

export const Description = ({ data }) => data && (
  <div className={styles.description}>
    <div className={styles['pre-header']}>
      {data['pre-header'] || ''}
    </div>
    <div className={joinStyles(['header', 'header-primary'])}>
      {data['main-header'] || ''}
    </div>
    <div className={joinStyles(['header', 'header-secondary'])}>
      {data['secondary-header'] || ''}
    </div>
  </div>
);

Description.propTypes = {
  data: React.PropTypes.object
};

export const Coupon = ({ data }) => data && (
  <div className={styles.coupon}>
    <div>
      <span className={styles.text}>
        {data.text || ''}
      </span>
      <span className={styles.code}>
        {data.code || ''}
      </span>
    </div>
    <div className={styles.footer}>
      {data.subtitle || ''}
    </div>
  </div>
);

Coupon.propTypes = {
  data: React.PropTypes.object
};

export const Button = ({ data }) => data && (
  <div className={styles.signup}>
    <a className={styles.button} href={`${data.link}?referer=${window.location.href}`}>
      <div className={styles.text}>
        {data.text}
      </div>
    </a>
  </div>
);

Button.propTypes = {
  data: React.PropTypes.object
};

export const Image = ({ data }) => data && (
  <div className={styles.image}>
    <img className={styles.imageContent} src={data.url} alt="" />
  </div>
);

Image.propTypes = {
  data: React.PropTypes.object
};

export function objectEntries(obj) {
  const entries = [];
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      entries.push([key, obj[key]]);
    }
  }
  return entries;
}

export const TrustBuilders = ({ data }) => data && (
  <div className={styles['trust-builders']}>
    {objectEntries(data)
      .map(([iconName, text], index) => {
        const Icon = iconProvider(iconName);
        return (
          <div key={index} className={styles.item}>
            {Icon ? <Icon color="#03a685" className={styles.icon} /> : null}
            <span className={styles.text}>{text}</span>
          </div>
        );
      })
    }
  </div>
);

TrustBuilders.propTypes = {
  data: React.PropTypes.object
};
