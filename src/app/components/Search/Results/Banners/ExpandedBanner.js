import React from 'react';
import { Description, Coupon, Image, Button, TrustBuilders } from './SubComponents.js';
import styles from './FreeShippingBanner.css';

const ExpandedBanner = (props) => (
  <div className={styles['banner-expanded']}>
    <div className={styles['first-row']}>
      <Description data={props.content} />
      <Image data={props.image} />
    </div>
    <div className={styles['second-row']}>
      <Coupon data={props.coupon} />
      {!props.isLoggedIn && <Button data={props.button} />}
    </div>
    <TrustBuilders data={props.trustBuilders} />
  </div>
);

ExpandedBanner.propTypes = {
  isLoggedIn: React.PropTypes.bool,
  content: React.PropTypes.object,
  image: React.PropTypes.object,
  coupon: React.PropTypes.object,
  button: React.PropTypes.object,
  trustBuilders: React.PropTypes.object
};


export default ExpandedBanner;
