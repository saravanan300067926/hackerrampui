import ExchangeIcon from '../../../../resources/svg/ExchangeIcon';
import trustGenuine from '../../../../resources/svg/trustGenuine';
import tryAndBuy from '../../../../resources/svg/TryAndBuy';

export default function iconProvider(iconName) {
  switch (iconName) {
    case 'genuine':
      return trustGenuine;
    case 'exchange':
      return ExchangeIcon;
    case 'try':
      return tryAndBuy;
    default:
      return null;
  }
}
