import get from 'lodash/get';
import { isAbEnabled } from '../../../../services/RemoteConfig';

const abConfig = {
  key: 'desktop-user-onboarding',
  control: 'VariantA',
  test: 'VariantB'
};

const switchKey = 'desktopNewUserOnboarding.freeShipping';

const FreeShippingInteractor = {
  getData() {
    return get(window, ['__myx_kvpairs__', switchKey], {});
  },

  featureEnabled() {
    return get(window, ['__myx_features__', switchKey], false) && isAbEnabled(abConfig, true);
  },

  getPage() {
    return window.__myx_pageType__;
  },

  pageEnabled() {
    const pages = get(this.getData(), 'pages', []);
    return pages.includes(this.getPage());
  }
};

export default FreeShippingInteractor;
