import React, { Component } from 'react';

import Style from './message.css';

const TextMessage = (props) => {
  const meta = props.message.data.meta || null
  const text = props.message.data.text || ''
  const author = props.message.author
  return (
    <div className={Style["sc-message--text"]}>
      {
        props.message &&
        author === "me" &&
        props.onDelete &&
        <button className={Style['delete-message']} onClick={() => props.onDelete(props.message)}>
          x
          </button>
      }
      {text}
      {meta && <p className={Style['sc-message--meta']}>{meta}</p>}
    </div>
  )
}

export default TextMessage