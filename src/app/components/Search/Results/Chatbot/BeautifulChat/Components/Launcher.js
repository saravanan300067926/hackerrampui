import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ChatWindow from './ChatWindow';

import Style from './launcher.css';

class Launcher extends Component {

  constructor() {
    super();
    this.state = {
      isOpen: false
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.props.polling(this.state.isOpen);
    }, 15000);
  }

  handleClick() {
    if (this.props.handleClick !== undefined) {
      this.props.handleClick();
    } else {
      this.props.messageRead();
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }
  }

  render() {
    const isOpen = this.props.hasOwnProperty('isOpen') ? this.props.isOpen : this.state.isOpen;
    const unReadMessage = this.props.unReadMessage;
    const classList = [
      Style['sc-launcher'],
      (isOpen ? Style['opened'] : ''),
    ];
    return (
      <div>
        <div className={classList.join(' ')} onClick={this.handleClick.bind(this)}>
          <MessageCount count={this.props.newMessagesCount} isOpen={isOpen || !unReadMessage} />
          <img className={Style["sc-open-icon"]} src={'https://i.imgur.com/Jb7xXDy.png'} />
          <img className={Style["sc-closed-icon"]} src={'https://svgshare.com/i/MwV.svg'} />
        </div>
        <ChatWindow
          messageList={this.props.messageList}
          onUserInputSubmit={this.props.onMessageWasSent}
          agentProfile={this.props.agentProfile}
          isOpen={isOpen}
          onClose={this.handleClick.bind(this)}
          showEmoji={this.props.showEmoji}
          showFile={this.props.showFile}
          onKeyPress={this.props.onKeyPress}
          onKeyPressDebounce={this.props.onKeyPressDebounce}
          onDelete={this.props.onDelete}
        />
      </div>
    );
  }
}

const MessageCount = (props) => {
  if (props.isOpen === true) { return null }
  return (
    <div className={Style["sc-new-messsages-count"]}>
      {props.count}
    </div>
  )
}

Launcher.propTypes = {
  onMessageWasReceived: PropTypes.func,
  onMessageWasSent: PropTypes.func,
  newMessagesCount: PropTypes.number,
  isOpen: PropTypes.bool,
  handleClick: PropTypes.func,
  messageList: PropTypes.arrayOf(PropTypes.object),
  showEmoji: PropTypes.bool,
  showFile: PropTypes.bool,
  onKeyPress: PropTypes.func,
  onDelete: PropTypes.func
};

Launcher.defaultProps = {
  newMessagesCount: 0
}

export default Launcher;
