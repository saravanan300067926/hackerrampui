import React, { Component } from 'react';

import Style from './header.css';

class Header extends Component {

  render() {
    return (
      <div className={Style["sc-header"]}>
        <img className={Style["sc-header--img"]} src={this.props.imageUrl} height={55} width={55} alt="" />
        <div className={Style["sc-header--team-name"]}> {this.props.teamName} </div>
        <div className={Style["sc-header--close-button"]} onClick={this.props.onClose}>
          <img src='https://i.imgur.com/Jb7xXDy.png' alt="close" />
        </div>
      </div>
    );
  }
}

export default Header;
