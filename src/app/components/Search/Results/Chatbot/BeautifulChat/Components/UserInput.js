import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { render } from 'react-dom'
import SendIcon from './icons/SendIcon'
import EmojiIcon from './icons/EmojiIcon'
import EmojiPicker from './emoji-picker/EmojiPicker'
import FileIcons from './icons/FileIcon'
import _ from 'lodash'

import Style from './userInput.css';

class UserInput extends Component {

  constructor() {
    super()
    this.state = {
      inputActive: false,
      file: null
    };
    ['handleKey', '_handleFileSubmit', '_handleEmojiPicked', '_submitText', '_handleFileSubmit'].forEach(method => this[method] = this[method].bind(this));
  }

  handleKey(event) {
    if (event.keyCode === 13 && !event.shiftKey) {
      this._submitText(event)
    }
  }



  _submitText(event) {
    event.preventDefault()
    const text = this.userInput.textContent
    const file = this.state.file
    if (file) {
      if (text && text.length > 0) {
        this.props.onSubmit({
          author: 'me',
          type: 'file',
          data: { text, file }
        })
        this.setState({ file: null })
        this.userInput.innerHTML = ''
      } else {
        this.props.onSubmit({
          author: 'me',
          type: 'file',
          data: { file }
        })
        this.setState({ file: null })
      }
    } else {
      if (text && text.length > 0) {
        this.props.onSubmit({
          author: 'me',
          type: 'text',
          data: { text }
        })
        this.userInput.innerHTML = ''
      }
    }
  }

  _handleEmojiPicked(emoji) {
    this.props.onSubmit({
      author: 'me',
      type: 'emoji',
      data: { emoji }
    })
  }

  _handleFileSubmit(file) {
    this.setState({ file })
  }

  render() {
    return (
      <div>
        {
          this.state.file &&
          <div className={Style['file-container']} >
            <span className={Style['icon-file-message']}><img src={'https://svgshare.com/i/Mv0.svg'} alt='genericFileIcon' height={15} /></span>
            {this.state.file && this.state.file.name}
            <span className={Style['delete-file-message']} onClick={() => this.setState({ file: null })} ><img src={'https://i.imgur.com/Jb7xXDy.png'} alt='close icon' height={10} title='Remove the file' /></span>
          </div>
        }
        <form className={`${Style['sc-user-input']} ${(this.state.inputActive ? 'active' : '')}`}>
          <div
            role="button"
            tabIndex="0"
            onFocus={() => { this.setState({ inputActive: true }) }}
            onBlur={() => { this.setState({ inputActive: false }) }}
            ref={(e) => { this.userInput = e }}
            onKeyDown={this.handleKey}
            onKeyPress={() => {
              this.props.onKeyPress(this.userInput.textContent)
            }}
            contentEditable="true"
            placeholder="Write a reply..."
            className={Style["sc-user-input--text"]}
          >
          </div>
          <div className={Style["sc-user-input--buttons"]}>
            <div className={Style["sc-user-input--button"]}></div>
            <div className={Style["sc-user-input--button"]}>
              {this.props.showEmoji && <EmojiIcon onEmojiPicked={this._handleEmojiPicked.bind(this)} />}
            </div>
            {this.props.showFile &&
              <div className={Style["sc-user-input--button"]}>
                <FileIcons onChange={(file) => this._handleFileSubmit(file)} />
              </div>
            }
            <div className={Style["sc-user-input--button"]}>
              <SendIcon onClick={this._submitText.bind(this)} />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

UserInput.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  showEmoji: PropTypes.bool,
  showFile: PropTypes.bool,
  onKeyPress: PropTypes.func
}

UserInput.defaultProps = {
  showEmoji: true,
  showFile: true
}

export default UserInput
