import PropTypes from 'prop-types'
import React, { Component } from 'react'
import MessageList from './MessageList'
import UserInput from './UserInput'
import Header from './Header'

import Style from './chatwindow.css';


const Option = (props) => {
  return <div onClick={(e) => props.onClick({
    author: 'me',
    type: 'text',
    data: { text: props.text },
    isOptionSelected: 1
  })}
    className={Style.option}>
    {props.text}
  </div>

}

const OptionsList = props => {
  return <div className={Style.optionList}>
    {props.options.map(option => <Option key={option} text={option} onClick={props.onUserInputSubmit} />)}
  </div>
}


class ChatWindow extends Component {
  constructor(props) {
    super(props);
    ['onUserInputSubmit', 'onMessageReceived'].forEach(method => this[method] = this[method].bind(this));
  }

  onUserInputSubmit(message) {
    this.props.onUserInputSubmit(message)
  }

  onMessageReceived(message) {
    this.setState({ messages: [...this.state.messages, message] })
  }

  render() {
    let messageList = this.props.messageList || []
    let classList = [
      Style["sc-chat-window"],
      (this.props.isOpen ? Style["opened"] : Style["closed"])
    ];
    const options = messageList.length !== 0 ? (messageList[messageList.length - 1] || {}).options : null;

    return (
      <div className={classList.join(' ')}>
        <Header
          teamName={this.props.agentProfile.teamName}
          imageUrl={this.props.agentProfile.imageUrl}
          onClose={this.props.onClose}
        />
        <MessageList
          messages={messageList}
          imageUrl={this.props.agentProfile.imageUrl}
          onDelete={this.props.onDelete}
        />
        {options && <OptionsList options={options} onUserInputSubmit={this.onUserInputSubmit} />}
        <UserInput
          showEmoji={this.props.showEmoji}
          onSubmit={this.onUserInputSubmit}
          showFile={this.props.showFile}
          onKeyPress={this.props.onKeyPress} />
      </div>
    )
  }
}

ChatWindow.propTypes = {
  showEmoji: PropTypes.bool,
  showFile: PropTypes.bool,
  onKeyPress: PropTypes.func
}

export default ChatWindow
