import React, { Component } from 'react'
import TextMessage from './TextMessage'
import EmojiMessage from './EmojiMessage'
import FileMessage from './FileMessage'

import Style from './message.css';


class Message extends Component {

  constructor(props) {
    super(props);
    ['_renderMessageOfType'].forEach(method => this[method] = this[method].bind(this));
  }

  _renderMessageOfType(type) {
    switch (type) {
      case 'text':
        return <TextMessage message={this.props.message} onDelete={this.props.onDelete} />
      case 'emoji':
        return <EmojiMessage {...this.props.message} />
      case 'file':
        return <FileMessage onDelete={this.props.onDelete} message={this.props.message} />
    }
  }

  render() {
    let contentClassList = [
      Style["sc-message--content"],
      (this.props.message.author === "me" ? Style["sent"] : Style["received"])
    ];
    return (
      <div className={Style["sc-message"]}>
        <div className={contentClassList.join(" ")}>
          <div className={Style["sc-message--avatar"]} style={{
            backgroundImage: `url(${'https://svgshare.com/i/Mvv.svg'})`
          }}></div>
          {this._renderMessageOfType(this.props.message.type)}
        </div>
      </div>)
  }
}

export default Message