import React, { Component } from 'react'

import Style from './message.css';

const EmojiMessage = (props) => {
  return <div className={Style["sc-message--emoji"]}>{props.data.emoji}</div>
}

export default EmojiMessage;