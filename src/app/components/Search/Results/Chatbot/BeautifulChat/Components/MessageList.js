import React, { Component } from 'react';
import Message from './Messages'

import Style from './messageList.css';

class MessageList extends Component {

  componentDidUpdate(prevProps, prevState) {
    this.scrollList.scrollTop = this.scrollList.scrollHeight;
  }

  render() {
    return (
      <div className={Style["sc-message-list"]} ref={el => this.scrollList = el}>
        {this.props.messages.map((message, i) => {
          return <Message message={message} key={i} onDelete={this.props.onDelete} />
        })}
      </div>)
  }
}

export default MessageList