import React from 'react';

import { Launcher } from './BeautifulChat';

import Client from '../../../../services';

class ChatBot extends React.Component {
    constructor() {
        super();
        this.state = {
            unReadMessage: false,
            messageList: [{ author: 'me', data: {} }]
            // messageList: [{
            //     author: 'them',
            //     type: 'text',
            //     data: {
            //         text: 'some text'
            //     },
            // },
            // {
            //     author: 'them',
            //     type: 'text',
            //     data: {
            //         text: 'random text'
            //     },
            // },
            // {
            //     author: 'me',
            //     type: 'emoji',
            //     data: {
            //         code: 'someCode'
            //     }
            // },
            // {
            //     author: 'me',
            //     type: 'file',
            //     data: {
            //         name: 'file.mp3',
            //         url: 'https:123.rf/file.mp3'
            //     },
            // },
            // {
            //     author: 'me',
            //     type: 'emoji',
            //     data: {
            //         code: 'someCode'
            //     }
            // },
            // {
            //     author: 'me',
            //     type: 'file',
            //     data: {
            //         name: 'file.mp3',
            //         url: 'https:123.rf/file.mp3'
            //     },
            // },
            // {
            //     author: 'them',
            //     type: 'text',
            //     data: {
            //         text: 'Would you like to proceed?'
            //     },
            //     options: ["wow", "awesome"],
            // }]
        };
        ['_onMessageWasSent', '_sendMessage',
            'newMessageArrived', 'messageRead', 'polling'].forEach(method => this[method] = this[method].bind(this));
    }

    componentDidMount() {
        Client.get('/pushMessage', { uidx: "1233", userMessage: '', timestamp: (new Date()).getTime() }).end((err, response) => {
            if (err) {

            } else {
                const { messageEntityList = [] } = response.body;
                const messageList = messageEntityList.filter(entity => !!entity.message).map((entity) => ({ author: entity.sender, type: 'text', data: { text: entity.message } }))
                this.setState({ messageList });
            }
        })
    }

    newMessageArrived() {
        this.setState({ unReadMessage: true });
    }

    messageRead() {
        this.setState({ unReadMessage: false });
    }

    _onMessageWasSent(message) {
        message.data.text && this.setState((prevState) => ({
            messageList: [...prevState.messageList, message]
        }));
        Client.get('/pushMessage', { uidx: "1233", userMessage: message.data.text, timestamp: (new Date()).getTime(), ruleNumber: message.ruleNumber, isOptionSelected: message.isOptionSelected }).end((err, response) => {
            if (err) {

            } else {
                const { messageEntityList = [], actionType, targetURL } = response.body;
                const length = messageEntityList.length;
                this._sendMessage(messageEntityList[length - 1]);
                if (actionType) {
                    switch (actionType) {
                        case 'goto':
                            if (['giftcard', 'checkout/cart', 'my/orders'].indexOf(targetURL) > -1) {
                                window.location.replace("https://www.stage.myntra.com/" + targetURL);
                            } else {
                                window.location = targetURL;
                            }
                            break;
                    }
                }
            }
        })
    }

    polling(isOpen) {
        if (!isOpen) {
            Client.get("/pollBot", { uidx: "1233" }).end((err, response) => {
                if (err) {

                } else {
                    if (response.body.probability > 0.7) {
                        this._onMessageWasSent({ author: 'me', data: {}, ruleNumber: response.body.ruleNumber });
                        clearInterval();
                    }
                }
            });
        }
    }

    _sendMessage(messageEntityList) {
        messageEntityList && messageEntityList.sender !== 'me' && this.setState((prevState) => ({
            messageList: [...prevState.messageList, {
                author: 'them',
                type: 'text',
                data: { text: (messageEntityList || {}).message },
                options: (messageEntityList || {}).options
            }],
            unReadMessage: true
        }))
    }

    render() {
        return (<div>
            <Launcher
                agentProfile={{
                    teamName: 'Thanos',
                    imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSEjsFORU6njJpN_KndC2r3U-HVofpn7X0WYQ&usqp=CAU'
                }}
                onMessageWasSent={this._onMessageWasSent}
                messageList={this.state.messageList}
                onKeyPress={() => { }}
                messageRead={this.messageRead}
                polling={this.polling}
                showEmoji
                newMessagesCount={1}
                unReadMessage={this.state.unReadMessage}
            />
        </div>)
    }
}

export default ChatBot;
