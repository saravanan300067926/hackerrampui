const mapDispatchToProps = dispatch => ({
  getMoreProducts(page) {
    dispatch({
      type: 'MORE_PRODUCTS_CLICKED',
      data: {
        page
      }
    });
  }
});

export default mapDispatchToProps;
