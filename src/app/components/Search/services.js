const agent = require('@myntra/m-agent')();

const get = (url, headers) => (
  agent.get(url).set({ ...{ Accept: 'application/json', 'Content-Type': 'application/json' }, ...headers })
  .send()
);

export default get;
