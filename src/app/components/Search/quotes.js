const quotes = {
  'list': [
    {
      'quote': '\u201cYou can have anything you want in life if you dress for it.\u201d',
      'author': 'Edith Head'
    },
    {
      'quote': '\u201cFashion is the armor to survive the reality of everyday life.\u201d',
      'author': 'Bill Cunningham'
    },
    {
      'quote': '\u201cClothes mean nothing until someone lives in them.\u201d',
      'author': 'Marc Jacobs'
    },
    {
      'quote': '\u201cThe joy of dressing is an art.\u201d',
      'author': 'John Galliano'
    },
    {
      'quote': '\u201cIn difficult times, fashion is always outrageous.\u201d',
      'author': 'Elsa Schiaparelli'
    },
    {
      'quote': '\u201cFashion is what youre offered four times a year by designers. And style is what you choose.\u201d',
      'author': 'Lauren Hutton'
    },
    {
      'quote': '\u201cFashion is very important. It is life-enhancing and, like everything that gives pleasure, it is worth doing well.\u201d',
      'author': 'Vivienne Westwood'
    },
    {
      'quote': '\u201cStyle is a way to say who you are without having to speak.\u201d',
      'author': 'Rachel Zoe'
    },
    {
      'quote': '\u201cElegance is not standing out, but being remembered.\u201d',
      'author': 'Giorgio Armani'
    },
    {
      'quote': '\u201cWhat you wear is how you present yourself to the world, \n' +
      'especially today, when human contacts are so quick. Fashion is instant language.\u201d',
      'author': 'Miuccia Prada'
    },
    {
      'quote': '\u201cIn order to be irreplaceable one must always be different.\u201d',
      'author': 'Coco Chanel'
    }
  ]
};

export default quotes;
