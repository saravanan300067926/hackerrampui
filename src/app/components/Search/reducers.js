import { combineReducers } from 'redux';
import { isBrowser } from '../../utils';
import { routerReducer, LOCATION_CHANGE } from 'react-router-redux';
import lscache from 'lscache';
import {
  getSortFilter,
  removeProductDuplicates,
  filterProductsByImageEntry,
  getSearchV1FilterQueryParam,
  getSearchV1RangeFilterQueryParam
} from './helpers.js';
import Jsuri from 'jsuri';
import at from 'v-at';
import differenceBy from 'lodash/differenceBy';

function getRangeFilterValuesFromStrings(rangeFilterStrings = []) {
  return rangeFilterStrings.map(filterString => {
    const valueParts = filterString.split('_');
    return {
      start: parseInt(valueParts[0], 10),
      end: parseInt(valueParts[1], 10),
      id: valueParts[2]
    };
  });
}

function getFiltersFromUrl(uri) {
  const f = uri.getQueryParamValue('f');
  const filterParam = getSearchV1FilterQueryParam(f);
  const rangeFilterParam = getSearchV1RangeFilterQueryParam(f);
  const filtersFromUrl = [];

  if (filterParam) {
    const paramKeys = filterParam.split('::');
    for (const item of paramKeys) {
      const param = item.split(':');
      const id = param[0];
      let values = param[1].split(',');

      // remove the meta info from color query
      if (id === 'Color') {
        values = values.map(value => value.split('_')[0]);
      }
      filtersFromUrl.push({ id, values });
    }
  }

  if (rangeFilterParam) {
    const rfParamKeys = rangeFilterParam.split('::');
    for (const item of rfParamKeys) {
      const param = item.split(':');
      const id = param[0];
      const valueStrings = param[1].split(',');
      const values = getRangeFilterValuesFromStrings(valueStrings);
      filtersFromUrl.push({ id, values });
    }
  }

  return filtersFromUrl;
}

function getFiltersFromState(state) {
  const { filters = [], rangeFilters = [] } = at(state, 'results.appliedParams') || {};
  const filtersFromState = [];
  for (const filter of filters) {
    const { id, values } = filter;
    if (id) {
      filtersFromState.push({ id, values });
    }
  }
  for (const rangeFilter of rangeFilters) {
    const { id, values } = rangeFilter;
    if (id) {
      filtersFromState.push({ id, values });
    }
  }

  return filtersFromState;
}

const getCustomFilter = (state) => {
  let customFilters = [];
  const uri = new Jsuri(window.location.href);
  const filtersFromUrl = getFiltersFromUrl(uri);
  const filtersFromState = getFiltersFromState(state);
  customFilters = differenceBy(filtersFromUrl, filtersFromState, 'id');
  return customFilters;
};

const getInitialState = () => {
  if (!isBrowser()) {
    return {};
  }
  let state = null;

  if (!state) {
    state = at(window, '__myx.searchData') || {};
    if (state && state.results) {
      state.results.products = removeProductDuplicates(at(state, 'results.products'));
      state = filterProductsByImageEntry(state.results);
    }

    let sortFilters; let customFilters = [];
    // if filter format is wrong, this will return custom 404 page
    try {
      customFilters = getCustomFilter(state);
      sortFilters = getSortFilter(window.location.search);
    } catch (error) {
      console.error('wrong filter query', error);
      return { ...state, apiFetchInProgress: false, apiFetchFailed: true };
    }

    const uri = new Jsuri(window.location.href);
    const p = uri.getQueryParamValue('p');
    const pincodeQueryParam = uri.getQueryParamValue('pincode');
    const pincode = pincodeQueryParam ? parseInt(pincodeQueryParam, 10) : '';

    state = {
      ...state,
      customFilters,
      sort: sortFilters,
      apiFetchInProgress: false,
      apiFetchFailed: false,
      seo: at(window, '__myx.searchData.seo') || {},
      page: p ? parseInt(p, 10) : 1,
      rows: 50,
      pincode
    };
  }

  const products = at(state, 'results.products');

  if (products && products.length > 0) {
    lscache.set(
      `${window.location.pathname}${window.location.search}`,
      state,
      5
    );
  }

  return state;
};

const searchData = getInitialState();

const searchReducer = (state = searchData, action) => {
  let newState;
  switch (action.type) {

    case 'API_FETCH_SUCCEEDED':
      newState = action.response;
      newState.apiFetchInProgress = false;
      return { ...state, ...newState };

    case 'API_FETCH_FAILED':
      newState = action.response;
      return { ...state, ...newState };

    case 'API_FETCH_INPROGRESS':
      newState = action.response;
      return { ...state, ...newState };


    case 'STATE_CHANGED':
      newState = action.response;
      return {
        ...state,
        ...newState
      };

    case 'CACHE_FETCH_SUCCEEDED':
      newState = action.response;
      return { ...state, ...newState };

    case LOCATION_CHANGE:
      if (action.payload.action === 'POP') {
        const key = `${action.payload.pathname}${action.payload.search}`;
        const hydratedState = lscache.get(key);
        return hydratedState;
      }
      return state;


    default: // DO NOTHING
  }
  return state;
};

const rootReducer = combineReducers({
  routing: routerReducer,
  search: searchReducer
});

export { rootReducer };
