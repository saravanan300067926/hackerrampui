import { combineReducersState, getSearchTerm } from '../helpers';

const mapStateToProps = state => {
  const combinedState = combineReducersState(state);
  const title = getSearchTerm(combinedState);

  return {
    count: combinedState.results.totalCount,
    title
  };
};

export default mapStateToProps;
