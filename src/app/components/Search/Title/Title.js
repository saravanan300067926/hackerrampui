import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import mapStateToProps from './mapStateToProps';
import styles from './title.css';

const Title = ({ title, count }) => (
  <div className={styles.container}>
    <h1 className={styles.title}>{title}</h1>
    <span className={styles.count}> - {count} {count > 1 ? 'items' : 'item'}</span>
  </div>
);

Title.propTypes = {
  title: PropTypes.string,
  count: PropTypes.number
};

export default connect(mapStateToProps)(Title);
