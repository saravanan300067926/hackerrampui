import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import { rootReducer } from './reducers';
import { rootSaga } from './sagas/index';

const initStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const reduxRouterMiddleware = routerMiddleware(browserHistory);
  const store = createStore(
    rootReducer,
    // intialStateValue,
    applyMiddleware(sagaMiddleware, reduxRouterMiddleware)
  );
  sagaMiddleware.run(rootSaga);
  return store;
};

export { initStore };
