export const clickedFilters = data => ({ type: 'CLICKED_FILTER', data });

export const updateSelectedFilter = data => ({ type: 'FILTER_OPTION_SELECTED', data });

export const inFilterSearch = data => ({ type: 'IN_FILTER_SEARCH', data });

export const applyFilters = data => ({ type: 'APPLY_FILTERS', which: 'mobile', data });
