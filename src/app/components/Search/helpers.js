// Changes data from {value1: count1, value2: count2}
// to [ { $key: value1, $count: count1},  { $key: value2, $count: count2}]
import keyBy from 'lodash/keyBy';
import get from 'lodash/get';
import uniqBy from 'lodash/uniqBy';
import querystring from 'querystring';
import groupBy from 'lodash/groupBy';
import forOwn from 'lodash/forOwn';
import startCase from 'lodash/startCase';
import { isBrowser } from '../../utils';
import isEmpty from 'lodash/isEmpty';
import at from 'v-at';
const Jsuri = require('jsuri');

const transformData = data => (
  Object.keys(data)
    .map(key => ({ attribute: key, count: data[key] }))
    .sort((first, second) => second.count - first.count)
);

const arrayToObject = (array, keyName = 'id') => keyBy(array, element => element[keyName]);

const combineReducersState = (state) => {
  if (state && typeof state === 'object') {
    const combinedState = {};
    Object.keys(state).forEach(keys => Object.assign(combinedState, state[keys]));
    return combinedState;
  } else {
    return state;
  }
};

const widthRatios = ['1.5', '2', '2.5', '3'];

const srcSet = (src) => {
  if (src) {
    return widthRatios.map((ratio, index) =>
      `${src.replace('assets.myntassets.com', `assets.myntassets.com/dpr_${ratio}`)
        .replace('q_95', 'q_80')} ${ratio}x${index === widthRatios.length - 1 ? '' : ','}`
    ).join('');
  } return '';
};

/**
 *
 * TODO: Remove this after complete migration to search v2
 * Search v1 URL compatibility layer for biro page and direct landings
 *
 * @param {Object} uri : search v1 filter query URI object
 * @return {string}: f query from search v1 filterQuery
 */
export function getSearchV1FilterQueryParam(filterQuery = '') {
  return filterQuery.split('::')
    .filter(query => !query.includes('price') && !query.includes('discount'))
    .map(param => {
      const parts = param.split(':');
      const id = getFilterId(parts[0]);
      const values = parts[1];

      if (!values) return '';

      if (id.toLowerCase() === 'color') {
        const modifiedColors = values
          .split(',')
          .map(getColorValue)
          .join(',');
        return `${id}:${modifiedColors}`;
      }
      return `${id}:${values}`;
    })
    .join('::');
}

/**
 *
 * TODO: Remove this after complete migration to search v2
 * Search v1 URL compatibility layer for biro page and direct landings
 *
 * @param {object} uri : search v1 filter query URI object
 * @return {string}: rf query from search v1 filterQuery
 */
export function getSearchV1RangeFilterQueryParam(filterQuery = '') {
  const discountFilter = filterQuery.split('::')
    .filter(query => query.includes('discount'))
    .map(param => {
      const parts = param.split(':');
      const id = getFilterId(parts[0]);
      const value = parts[1].split(':');

      if(!value) return '';
      return `${id}:${value}_100.0_${value} TO 100.0`;
    })
    .join('');

  const priceFilter = filterQuery.split('::')
    .filter(query => query.includes('price'))
    .reduce((query, param) => {
      const id = getFilterId(param.split(':')[0]);
      const value = param.split(':')[1];

      if(!value) return '';
      const [start, end] = value.split(',');
      const modifiedValue = `${start}_${end}_${start} TO ${end}`;
      return query === '' ? `${id}:${modifiedValue}` : `${query},${modifiedValue}`;
    }, '');

  if (priceFilter && discountFilter) {
    return [priceFilter, discountFilter].join('::');
  }
  return priceFilter || discountFilter;
}

export const getFilterId = (filterId) => {
  const map = {
    age_facet: 'Age',
    offer: 'Offers',
    offers: 'Offers',
    size_facet: 'size_facet',
    size_facets: 'size_facet',
    sizes_facet: 'size_facet',
    sizes_facets: 'size_facet',
    tag_coupon: 'Coupons',
    dre_comboid: 'ComboId',
    dre_offertype: 'Offers',
    discounted_price: 'Price',
    global_attr_brand: 'Brand',
    brands_filter_facet: 'Brand',
    global_attr_gender_string: 'Gender',
    discount_percentage: 'Discount Range',
    global_attr_article_type: 'Categories',
    global_attr_base_colour_hex_facet: 'Color',
    global_attr_style_category_facet: 'Bundles',
    global_attr_article_type_facet: 'Categories',
    global_attr_master_category_facet: 'Sections',
    global_attr_sub_category_facet: 'Sub Categories',
    print_or_pattern_type_article_attr: 'Print or Pattern Type',
    gender: 'Gender',
    brands: 'Brand',
    color: 'Color',
    colour: 'Color',
    colours: 'Color',
    colors: 'Color',
    discount: 'Discount Range',
    price: 'Price',
    male: 'Men',
    female: 'Women',
    all: 'Unisex',
    filtered_keywords: 'keywords',
  }
  return map[filterId.toLowerCase()] || filterId;
}

export const getColorValue = colorValue => {
	const map = {
    aqua: 'Aqua_3ca8ce',
    beige: 'Beige_e8e6cf',
    black: 'Black_36454f',
    blue: 'Blue_0074D9',
    bronze: 'Bronze_cc8240',
    brown: 'Brown_8b4513',
    burgundy: 'Burgundy_a03245',
    charcoal: 'Charcoal_36454f',
    coffeebrown: 'Coffee Brown_4b302f',
    cognac: 'Cognac_834a3a',
    copper: 'Copper_aa6c39',
    coral: 'Coral_ff7f50',
    cream: 'Cream_ede6b9',
    fluorescentgreen: 'Fluorescent Green_8dc04a',
    gold: 'Gold_e5c74a',
    green: 'Green_5eb160',
    grey: 'Grey_808080',
    greymelange: 'Grey Melange_9fa8ab',
    gunmetal: 'Gun Metal_d9d4bc',
    khaki: 'Khaki_c3b091',
    lavender: 'Lavender_d6d6e5',
    limegreen: 'Lime Green_5db653',
    magenta: 'Magenta_b9529f',
    maroon: 'Maroon_b03060',
    mauve: 'Mauve_e0b0ff',
    metalic: 'Metallic_e0d0c5',
    metallic: 'Metallic_e0d0c5',
    multi: 'Multi_5eb160',
    mushroombrown: 'Mushroom Brown_ba8f65',
    mustard: 'Mustard_eadc32',
    navy: 'Navy_3c4477',
    navyblue: 'Navy Blue_3c4477',
    nude: 'Nude_dbaf97',
    offwhite: 'Off White_f2f2f2',
    olive: 'Olive_3D9970',
    orange: 'Orange_f28d20',
    peach: 'Peach_ffe5b4',
    pink: 'Pink_f1a9c4',
    purple: 'Purple_800080',
    red: 'Red_d34b56',
    rose: 'Rose_dd2f86',
    rust: 'Rust_b7410e',
    seagreen: 'Sea Green_2e8b57',
    silver: 'Silver_b3b3b3',
    skin: 'Skin_d6af99',
    steel: 'Steel_b3b3b3',
    tan: 'Tan_d2b48c',
    taupe: 'Taupe_483c32',
    teal: 'Teal_008080',
    transparent: 'Transparent_eeeeee',
    turquoise: 'Turquoise Blue_40e0d0',
    turquoiseblue: 'Turquoise Blue_40e0d0',
    white: 'White_f2f2f2',
    yellow: 'Yellow_eadc32'
  };
  return map[colorValue.replace(/ /g, '').toLowerCase()] || colorValue
}

export const getSortFilter = (searchUrl = '') => {
  let sortFilter = {};

  sortFilter = {
    key: 'sort',
    title: 'Sort',
    common: true,
    values: [
      {
        title: 'What\'s New',
        checked: false,
        option: 'new'
      },
      {
        title: 'Popularity',
        checked: false,
        option: 'popularity'
      },
      {
        title: 'Better Discount',
        checked: false,
        option: 'discount'
      },
      {
        title: 'Price: High to Low',
        checked: false,
        option: 'price_desc'
      },
      {
        title: 'Price: Low to High',
        checked: false,
        option: 'price_asc'
      },
      //This needs api call with pincode data
      {
        title: 'Faster Delivery',
        checked: false,
        option: 'delivery_time'
      }
    ]
  };

  const sortValue = get(
    querystring.parse(
      searchUrl.replace('?', '')
    ),
    'sort',
    false
  );

  sortFilter.values.forEach((value) => {
    if (value.option === sortValue) {
      value.checked = true;
    }
  });

  return sortFilter;
};

const sortByCount = filterGroup => {
  if (filterGroup) {
    return ({
      ...filterGroup,
      filterValues: filterGroup.filterValues.sort((curr, next) => {
        if (next.count === curr.count) {
          return next.id.localeCompare(curr.id);
        }
        return next.count - curr.count;
      })
    })
  } return ({
    ...filterGroup
  });
};

const sortByDiscount = discountGroup => {
  if (discountGroup) {
    return ({
      ...discountGroup,
      values: discountGroup.filterValues.sort(
        (curr, next) => parseInt(next.option, 10) - parseInt(curr.option, 10)
      )
    });
  } return ({
    ...discountGroup
  });
};


const sortByIndices = filterGroup => {
  if (filterGroup) {
    const groups = groupBy(filterGroup.filterValues, (o) => {
      const firstLetter = o.id.substr(0, 1).toLowerCase();
      if (isNaN(firstLetter)) {
        return firstLetter;
      } return '#';
    });
    return groups;
  } return {};
};

const sortObjectKeysByAlpha = filterGroup => {
  if (filterGroup) {
    const groups = sortByIndices(filterGroup);
    const sortedObject = {};
    const keys = Object.keys(groups).sort();
    keys.forEach((item) => {
      sortedObject[item] = groups[item];
    });
    return sortedObject;
  } return {};
};

const genders = [
  {
    title: 'Men',
    value: ['men', 'men women']
  },
  {
    title: 'Women',
    value: ['women', 'men women']
  },
  {
    title: 'Boys',
    value: ['boys', 'boys girls']
  },
  {
    title: 'Girls',
    value: ['girls', 'boys girls']
  }
];

const isChecked = (data, option) => {
  if (data) {
    const { checkedValues = [] } = data;
    return checkedValues.includes(option);
  }
  return false;
}

const normalizeGender = data => (
  genders
  .map(gender => {
      const { title, value } = gender;
      const checked = (
        isChecked(data, value[0])
        || (isChecked(data, value[0]) && isChecked(data, value[1]))
      );

      return {
        title,
        checked,
        value
      };
    })
    .filter(gender => {
      const { value } = gender;
      if (data && data.filterValues) {
        return (
          data.filterValues.find(element => element.id === value[0])
        );
      } return false;
    })
);

let exclude = {
  gender: '',
  categories: '',
  brand: '',
  price: '',
  color: '',
  'discount range': '',
  department: '',
  subcategories: '',
  sections: '',
  'sub categories': '',
  sort: '',
};

const normalizeFilters = (filters) => {
  const refPath = at(window, 'location.href');
  const uriObj = new Jsuri(refPath);
  const searchFilterQuery = uriObj.hasQueryParam('f') ? uriObj.getQueryParamValue('f') : '';

  if (searchFilterQuery) {
    searchFilterQuery.includes('Global Size') ? exclude['size_facet'] = '' : exclude['global size'] = '';
  } else {
    exclude['global size'] = '';
  }
  const atsaFilters = Object.values(filters)
    .filter(
      element => (
        !(element.id.toLowerCase() in exclude)
        && (element.filterValues.length > 0)
      )
    )
    .sort((prev, next) => {
      if (prev.id < next.id) return -1;
      if (prev.id > next.id) return 1;
      return 0;
    })
    .map(element => {
      const filterValues = element.filterValues;
      filterValues.sort((prev, next) => {
        if (prev.id < next.id) return -1;
        if (prev.id > next.id) return 1;
        return 0;
      });

      return {
        ...element,
        filterValues
      };
    });

  return atsaFilters;
};

//filter out "na" and "none" filter values
function cleanFilterValues(filter) {
  filter.filterValues = filter.filterValues.filter((element) => {
    const option = get(element, 'id');
    if (option) {
      return option.toLowerCase() !== 'na' && option.toLowerCase() !== 'none';
    }
    return false;
  });
  return filter;
}

// remove filters with empty filter values
function getCleanedUpFilters(filterList) {
  return filterList
    .map(cleanFilterValues)
    .filter(item => item.filterValues.length > 0);
}

const getSearchTerm = combinedState => {
  let searchTerm = combinedState.pageTitle;

  if (isBrowser()) {
    searchTerm =  at(window, '__myx.searchData.pageTitle');
  }

  if (!searchTerm) {
    searchTerm = startCase(get(combinedState, 'seo.uri'));
  } else if (!searchTerm && isBrowser()) {
    searchTerm = window.location.pathname
      .replace(/\//g, '')
      .replace(/-/g, ' ')
      .replace('menu', '');
    searchTerm = decodeURIComponent(searchTerm);
  }

  const pathSearched = get(combinedState, 'locationBeforeTransitions.pathname', '');
  return searchTerm || pathSearched.substring(1);
};

/*
 * Function returns product list without duplicates
 * */
function removeProductDuplicates(products) {
  return uniqBy(products, (product) => (product.productId));
}

// removes the product with no images enrty
const filterProductsByImageEntry = state => {
  let products = get(state, 'products', []);
  if (products && products.length > 0) {
    products = products.filter((item) => {
      let imageEntryProductObj = {};
      imageEntryProductObj = item.images;
      return !isEmpty(imageEntryProductObj);
    });
    return { results: { ...state, products } };
  }
  return state;
};

const FILTER_APPLIEDFILTER_DISCREPENCY_MAP = {
  Color: 'colors'
};

function injectCheckedFilters(filters, appliedFilters) {
  const filterState = filters;
  forOwn(filters, (filter, id) => {
    const appliedFilterId =
      FILTER_APPLIEDFILTER_DISCREPENCY_MAP[id] || id.toLowerCase();
    filter.checkedValues = get(
      appliedFilters,
      `${appliedFilterId}.values`,
      get(appliedFilters, `${id}.values`, [])
    );
  });
  return filterState;
}

function injectCheckedRangeFilter(filters, appliedFilters) {
  const filterState = filters;
  forOwn(filters, (filter, id) => {
    const appliedFilterId = FILTER_APPLIEDFILTER_DISCREPENCY_MAP[id] || id;
    filter.checkedValues = get(
      appliedFilters,
      `${appliedFilterId}.values`,
      get(appliedFilters, `${id}.values`, [])
    ).map(value => value.id);
  });
  return filterState;
}

function formatCount(num, digits) {
  let i;
  const si = [
    { suffix: 1, symbol: '' },
    { suffix: 1e3, symbol: 'k' },
    { suffix: 1e6, symbol: 'M' },
    { suffix: 1e9, symbol: 'G' },
    { suffix: 1e12, symbol: 'T' },
    { suffix: 1e15, symbol: 'P' },
    { suffix: 1e18, symbol: 'E' }
  ];
  const regEx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].suffix) {
      break;
    }
  }
  return (
    (num / si[i].suffix).toFixed(digits).replace(regEx, '$1') + si[i].symbol
  );
}


export { transformData, arrayToObject, combineReducersState, srcSet, sortByCount, sortByDiscount, normalizeGender, normalizeFilters, getCleanedUpFilters, exclude, sortObjectKeysByAlpha, getSearchTerm, removeProductDuplicates, filterProductsByImageEntry,
injectCheckedFilters, injectCheckedRangeFilter, FILTER_APPLIEDFILTER_DISCREPENCY_MAP, formatCount };
