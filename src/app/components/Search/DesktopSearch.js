import React from 'react';
import { Row, Column } from '../Grid';
import Breadcrumbs from './Breadcrumbs';
import VerticalFilters from './VerticalFilters';
import HorizontalFilters from './HorizontalFilters';
import Results from './Results';
import styles from './search.css';
import Track from '../../utils/track';
// import { pageScroll } from '../../utils/dom';
import Title from './Title/Title';

class DesktopSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clearAllHover: false
    };
    this.toggleClearAllHover = this.toggleClearAllHover.bind(this);
  }
  componentDidMount() {
    Track.event('ListPage', 'Land', `SearchPath | ${window.location.pathname}`);
    // window.addEventListener('unload', () => {
    //   Track.event('ListPage', 'scrollDepth', `depth | ${pageScroll()}`);
    // });
  }
  toggleClearAllHover(value) {
    this.setState({
      clearAllHover: value
    });
  }

  render() {
    return (
      <main className={styles.base}>
        <Row><Breadcrumbs /></Row>
        <Row><Title /></Row>
        <Row>
          <Column width={0} className={styles.leftContainer}>
            <VerticalFilters
              onClearAll={this.toggleClearAllHover}
              isClearAll={this.state.clearAllHover} />
          </Column>
          <Column width={1} className={styles.rightContainer}>
            <div id="desktopSearchResults">
              <Row><HorizontalFilters
                isClearAll={this.state.clearAllHover} /></Row>
              <Row className={styles.searchProductsContainer}><Results /></Row>
            </div>
          </Column>
        </Row>
      </main>
    );
  }
}

export default DesktopSearch;
