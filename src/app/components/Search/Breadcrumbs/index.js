import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styles from './breadcrumbs.css';
import mapStateToProps from './mapStateToProps';
import last from 'lodash/last';

const Breadcrumbs = ({ crumbs, searchItem, searchTerm }) => {
  const query = (searchItem || '').replace(/\s/g, '-').toLowerCase();
  const lastBreadcrumb = last(crumbs).text.replace(/\s/g, '-').toLowerCase();
  const stripSplCharecters = crumb => crumb.replace(/[^0-9a-z]/gi, '');
  const isCharEquals = stripSplCharecters(searchItem) !== stripSplCharecters(searchTerm);
  if (stripSplCharecters(query) === stripSplCharecters(lastBreadcrumb)) {
    crumbs.pop();
  }
  const listElements = crumbs.map((crumb, index) => (
    <li
      className={styles.item}
      key={index}
      itemScope="">
      <a className={styles.crumb} href={`${crumb.path}?src=bc`} itemProp="url">
        {index === crumbs.length - 1 && !isCharEquals ? <h1 className={styles.crumb} style={{ fontSize: '14px', margin: '0' }}>{searchTerm}</h1> :
          <span itemProp="title">{crumb.text}</span>}
      </a>
    </li>
  ));
  if (isCharEquals) {
    listElements.push((
      <li
        className={styles.item}
        key={listElements.length}>
        <span className={styles.crumb} style={{ fontSize: '14px', margin: '0' }}>{searchTerm}</span>
      </li>
    ));
  }

  return (
    <div className={styles.base}>
      <ul className={styles.list}>{listElements}</ul>
    </div>
  );
};

Breadcrumbs.propTypes = {
  crumbs: PropTypes.array,
  searchTerm: PropTypes.string,
  searchItem: PropTypes.string
};

export default connect(mapStateToProps)(Breadcrumbs);
