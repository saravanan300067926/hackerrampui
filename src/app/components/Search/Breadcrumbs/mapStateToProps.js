import { arrayToObject, combineReducersState, sortByCount, normalizeGender, getSearchTerm, injectCheckedFilters } from '../helpers';
import at from 'v-at';
import { stripSpecialChars } from '../../Header/helpers.js';

const get = (item) => {
  let name = null;

  if (item.id && item.id.toLowerCase() === 'gender') {
    if (item.values && item.values.length === 1) {
      name = item.values[0].title;
    }
    return name;
  } else if (item.filterValues && item.filterValues.length === 1) {
    name = item.filterValues[0].id;
  }

  return name;
};

const clean = (str) => {
  if (str) {
    return stripSpecialChars(str.replace(/\s/img, '-').replace(/\//mg, '%2F'));
  }
  return '';
};

const createBreadcrumb = (filters) => {
  let section = get(filters.sections);
  const brand = get(filters.brand);
  const category = get(filters.categories);
  const gender = get(filters.gender);

  if (section === 'Apparel') {
    section = 'Clothing';
  } else if (section === 'Home') {
    section = 'Home Furnishing';
  }

  const arr = [{
    path: '/',
    text: 'Home'
  }];

  if (section) {
    arr.push({
      path: `/${clean(section)}`,
      text: section,
      type: 'mc'
    });
    if (gender) {
      arr.push({
        path: `/${clean(`${gender}-${section}`)}`,
        text: `${gender} ${section}`,
        type: 'g-mc'
      });
    }
  } else if (gender) {
    arr.push({
      path: `/${clean(gender)}`,
      text: gender,
      type: 'g'
    });
  }
  if (category) {
    arr.push({
      path: `/${clean(category)}`,
      text: category,
      type: 'cat'
    });
    if (brand) {
      arr.push({
        path: `/${clean(`${brand}-${category}`)}`,
        text: `${brand} ${category}`,
        type: 'b-cat'
      });
    }
  } else if (brand) {
    arr.push({
      path: `/${clean(brand)}`,
      text: brand,
      type: 'b'
    });
  }

  return arr;
};


const mapStateToProps = (state, type) => {
  const combinedState = typeof(type) === 'string' ? state : combineReducersState(state);
  const primaryFilters = arrayToObject(at(combinedState, 'results.filters.primaryFilters') || []);
  const appliedPrimaryFilters = arrayToObject(at(combinedState, 'results.appliedParams.filters') || []);
  const filters = injectCheckedFilters(primaryFilters, appliedPrimaryFilters);

  const normalizedGender = {
    ...at(filters, 'Gender'),
    values: normalizeGender(at(filters, 'Gender'))
  };

  const crumbs = createBreadcrumb({
    gender: normalizedGender,
    categories: sortByCount(at(filters, 'Categories')),
    brand: sortByCount(at(filters, 'Brand')),
    sections: sortByCount(at(filters, 'Sections'))
  });

  const searchTerm = getSearchTerm(combinedState) || '';
  const searchItem = at(combinedState, 'seo.uri') || '';

  return {
    searchItem,
    searchTerm,
    crumbs
  };
};

export default mapStateToProps;
