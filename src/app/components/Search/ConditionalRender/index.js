import React, { PropTypes } from 'react';
import { pageScroll, windowHeight, offset } from '../../../utils/dom';
import styles from './condition-render.css';
import at from 'v-at';

class ConditionalRender extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebarClass: 'boundary-top',
      top: '',
      bottom: ''
    };
    this.onScrollSidebarFix = this.onScrollSidebarFix.bind(this);
  }

  componentDidMount() {
    this.scrollTop = pageScroll().top;
    this.header = document.getElementById('desktop-header-cnt');
    this.footer = document.getElementById('web-footerMount');
    this.searchResults = document.getElementById('desktopSearchResults');
    this.topBoundary = offset(this.searchResults).top;
    if (this.refs.sidebar) {
      document.addEventListener('scroll', this.onScrollSidebarFix);
    }
    this.onScrollSidebarFix();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.sideBarPositionFix) {
      this.onScrollSidebarFix();
    }
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.onScrollSidebarFix);
  }

  onScrollSidebarFix() {
    const fixedTop = this.header.offsetHeight;
    const winHeight = windowHeight() - fixedTop;
    const scrollTopPrev = this.scrollTop;
    const sidebarTop = offset(this.refs.sidebar).top;
    const sidebarHeight = this.refs.sidebar.offsetHeight;
    const bottomBoundary = offset(this.footer).top;
    const boundaryBottom = bottomBoundary - this.topBoundary - sidebarHeight;

    this.scrollTop = pageScroll().top;

    const scrollDirection = scrollTopPrev <= this.scrollTop ? 'down' : 'up';
    const hung = sidebarTop - this.topBoundary - (this.scrollTop - scrollTopPrev);

    if (this.searchResults.offsetHeight <= winHeight) {
      /* For Short Search Results */
      this.position('boundary-top');
    } else if (sidebarHeight <= winHeight) {
      /* For Short Filters */
      if (this.contains('boundary-top', 'hung', 'fixed-bottom')
        && this.scrollTop > this.topBoundary) {
        // Case I : From boundary-top to fixed-top as we scroll down
        this.position('fixed-top', fixedTop);
      } else if (this.contains('fixed-top', 'boundary-top', 'hung', 'fixed-bottom')
        && this.scrollTop >= this.topBoundary + boundaryBottom - fixedTop) {
        // Case II : From fixed-top to boundary-bottom as we scroll down
        this.position('boundary-bottom', boundaryBottom);
      } else if (this.contains('fixed-top', 'boundary-bottom', 'hung', 'fixed-bottom')
        && this.scrollTop <= this.topBoundary) {
        // Case III : From fixed-top to boundary-top as we scroll up, reverse of Case I
        this.position('boundary-top');
      } else if (this.contains('boundary-bottom', 'hung', 'fixed-bottom')
        && this.scrollTop < this.topBoundary + boundaryBottom - fixedTop) {
        // Case IV : From boundary-bottom to fixed-top as we scroll up, reverse of Case II
        this.position('fixed-top', fixedTop);
      }
    } else {
      // Case I : From boundary-top to fixed-bottom as we scroll down
      if (this.contains('boundary-top') && this.scrollTop + winHeight > this.topBoundary + sidebarHeight - fixedTop) {
        this.position('fixed-bottom');
      } else if (this.contains('fixed-top') && scrollDirection === 'down') {
        // Case II : From fixed-top to hung as we scroll down
        if (this.scrollTop + winHeight + fixedTop < this.topBoundary + hung + sidebarHeight - fixedTop) {
          this.position('hung', hung);
        } else {
          this.position('fixed-bottom');
        }
      } else if (this.contains('hung') && this.scrollTop + winHeight >= sidebarTop + sidebarHeight - fixedTop) {
        // Case III : From hung to fixed-bottom as we scroll down, similar to Case I
        this.position('fixed-bottom');
      } else if (this.contains('fixed-bottom', 'hung', 'boundary-top', 'fixed-top', 'boundary-bottom') && sidebarTop + sidebarHeight >= bottomBoundary) {
        // Case IV : From fixed-bottom to boundary-bottom as we scroll down
        this.position('boundary-bottom', boundaryBottom);
      }

      // Case V : From fixed-top to boundary-top as we scroll up, reverse of Case I, similar to Case IV
      if (this.contains('fixed-top', 'hung', 'boundary-bottom', 'fixed-bottom') && this.scrollTop <= this.topBoundary) {
        this.position('boundary-top');
      } else if (this.contains('hung') && scrollDirection === 'up' && (this.scrollTop + fixedTop) <= sidebarTop) {
        // Case VI : From hung to fixed-top as we scroll up, reverse of Case II, similar to Case III
        this.position('fixed-top', fixedTop);
      } else if (this.contains('fixed-bottom') && scrollDirection === 'up') {
        // Case VII : From fixed-bottom to hung as we scroll up, reverse of Case III, similar to Case II
        if (this.scrollTop > this.topBoundary + hung) {
          this.position('hung', hung);
        } else {
          this.position('fixed-top', fixedTop);
        }
      } else if (this.contains('boundary-bottom') && this.scrollTop + fixedTop < sidebarTop) {
        // Case VIII : From boundary-bottom to fixed-top as we scroll up, reverse of Case IV, similar to Cases I & VI
        this.position('fixed-top', fixedTop);
      } if (this.contains('boundary-bottom') && this.topBoundary + boundaryBottom > sidebarTop - fixedTop + sidebarHeight) {
        // Case IX : Corrections in case of erroneous behaviour - load more items
        this.position('fixed-bottom');
      }
    }
  }

  position(pos, top) {
    top = pos === 'fixed-bottom' ? 'auto' : top && `${top}px` || 0;
    const bottom = pos === 'fixed-bottom' ? 0 : 'auto';

    this.setState({
      sidebarClass: pos,
      top,
      bottom
    });
  }

  contains(...theArgs) {
    if (typeof(theArgs[0]) === 'string') {
      for (let i = 0; i < theArgs.length; i++) {
        if (this.state.sidebarClass.indexOf(theArgs[i]) !== -1) return true;
      }
    }
    return false;
  }

  render() {
    const Children = React.Children.toArray(this.props.children);
    const sideBarStyle = {
      top: this.state.top,
      bottom: this.state.bottom
    };
    const verticalFiltersclass = this.props.isClearAll ? styles.verticalFiltersOnClearAll : styles.verticalFilters;
    return (
      <div className={`${verticalFiltersclass} ${styles[this.state.sidebarClass]}`} ref="sidebar" style={sideBarStyle}>
        {Children.filter(child => at(child, 'props.data.length') > 1)}
      </div>
    );
  }
}

ConditionalRender.propTypes = {
  children: PropTypes.array,
  sideBarPositionFix: PropTypes.bool,
  isClearAll: PropTypes.bool
};

export default ConditionalRender;
