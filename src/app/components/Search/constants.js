const sort = {
  common: true,
  title: 'sort',
  values: [
    { option: 'Price - high to low' },
    { option: 'Price - low to high' },
    { option: 'Popularity' },
    { option: 'Discount' },
    { option: 'What\'s New' }
  ]
};

const moreFilters = {
  common: true,
  title: 'more Filters',
  category: 0,
  values: [{
    option: ''
  }]
};

const sizeAge = {
  common: true,
  title: 'size/Age',
  category: 0,
  values: [{
    option: ''
  }]
};

const displayImageWidth = 180;
const displayImageHeight = 240;

const SORT = 'sort';
const FILTER = 'filter';
const PRICE = 'price';
const DISCOUNT = 'discount';
const COLOUR = 'colour';
const BRAND = 'brand';
const CATEGORIES = 'categories';
const GENDER = 'gender';
const SIZE = 'size';
const AGE = 'age';
const OFFER = 'offers';
const SECTIONS = 'sections';
const SUBCATEGORY = 'subcategories';
const DEPARTMENT = 'department';

const SORT_UPDATED = 'SORT_UPDATED';
const PINCODE_UPDATE = 'PINCODE_UPDATE';
const TOGGLE_MODAL = 'TOGGLE_MODAL';
const APPLY_FILTERS = 'APPLY_FILTERS';
const CATEGORIES_SELECTED = 'CATEGORIES_SELECTED';
const TRACK_PRICE = 'TRACK_PRICE';

const colors = {
  'aqua': '#3ca8ce',
  'beige': '#e8e6cf',
  'black': '#36454f',
  'blue': '#0074D9',
  'bronze': '#cc8240',
  'brown': '#915039',
  'burgundy': '#a03245',
  'charcoal': '#36454f',
  'coffee-brown': '#4b302f',
  'cognac': '#834a3a',
  'copper': '#aa6c39',
  'cream': '#ede6b9',
  'fluorescent-green': '#8dc04a',
  'gold': '#e5c74a',
  'green': '#5eb160',
  'grey': '#9fa8ab',
  'grey-melange': '#9fa8ab',
  'gunmetal': '#d9d4bc',
  'khaki': '#c3b091',
  'lavender': '#d6d6e5',
  'lime-green': '#5db653',
  'magenta': '#b9529f',
  'maroon': '#b03060',
  'mauve': '#e0b0ff',
  'metalic': '#e0d0c5',
  'metallic': '#e0d0c5',
  'mushroom-brown': '#ba8f65',
  'mustard': '#cc9c33',
  'navy': '#3c4477',
  'navy-blue': '#3c4477',
  'nude': '#dbaf97',
  'off-white': '#f2f2f2',
  'olive': '#3D9970',
  'orange': '#f28d20',
  'peach': '#ffe5b4',
  'pink': '#f1a9c4',
  'purple': '#800080',
  'red': '#d34b56',
  'rose': '#dd2f86',
  'rust': '#b7410e',
  'sea-green': '#2e8b57',
  'silver': '#b3b3b3',
  'skin': '#d6af99',
  'steel': '#b3b3b3',
  'tan': '#d2b48c',
  'taupe': '#483c32',
  'teal': '#008080',
  'turquoise': '#40e0d0',
  'turquoise-blue': '#40e0d0',
  'transparent': '#eeeeee',
  'yellow': '#eadc32',
  'coral': '#FF7F50',
  'white': '#ffffff'
};

const radios = [SORT, SIZE, AGE, OFFER];
const blackListed = [SECTIONS, SUBCATEGORY];

const RANGE_SEPERATOR = ' TO ';

export {
  sort,
  colors,
  displayImageWidth, displayImageHeight,
  moreFilters, sizeAge,
  radios, blackListed,
  RANGE_SEPERATOR,
  SORT, FILTER, PRICE, DISCOUNT, COLOUR, BRAND, CATEGORIES, GENDER, SIZE, AGE, OFFER, SECTIONS, SUBCATEGORY, DEPARTMENT,
  SORT_UPDATED, PINCODE_UPDATE, TOGGLE_MODAL, APPLY_FILTERS, CATEGORIES_SELECTED, TRACK_PRICE
};
