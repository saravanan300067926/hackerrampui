import React from 'react';
import get from 'lodash/get';
import styles from './check-delivery-availability.css';

class CheckDeliveryAvailability extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pinCode: ''
    };
  }

  componentDidMount() {
    if (this.pinCodeInputRef) {
      this.pinCodeInputRef.focus();
    }

    const pincodeServiceableConfig = get(window, '__myx_covid_serviceable_pincode_config__.deliveryCheck', {});
    if (pincodeServiceableConfig.title) {
      document.title = pincodeServiceableConfig.title;
    }
  }

  changeHandler = (event) => {
    const PINCODE_REGEX = /^\d{0,6}$/;
    const pinCode = event.target.value;

    if (PINCODE_REGEX.test(pinCode)) {
      this.setState({ pinCode });
    }
  }

  keyPressHandler = (event) => {
    if (event.key === 'Enter' && event.target.value && event.target.value.length === 6) {
      window.location.href = `/check-delivery-availability/${event.target.value}`;
    }
  }

  render() {
    const { pinCode } = this.state;
    const isValidPinCode = pinCode && pinCode.length === 6;
    const pincodeServiceableConfig = get(window, '__myx_covid_serviceable_pincode_config__.deliveryCheck', {});

    return (
      <div className={styles.container}>
        <div className={styles.checkDeliveryAvailability}>
          <img
            className={styles.deliveryImg}
            src={get(pincodeServiceableConfig, 'img', '')}
            alt="Check delivery availability" />
          <div className={styles.heading}>{get(pincodeServiceableConfig, 'heading', '')}</div>
          <div className={styles.text}>{get(pincodeServiceableConfig, 'message', '')}</div>
          <div className={styles.pinCodeSubmitContainer}>
            <input
              type="tel"
              autoFocus
              ref={ref => (this.pinCodeInputRef = ref)}
              className={styles.pinCodeInput}
              value={pinCode}
              onChange={this.changeHandler}
              onKeyPress={this.keyPressHandler}
              placeholder={get(pincodeServiceableConfig, 'form.pincodeInput.placeholder', '')} />
            <a
              href={isValidPinCode ? `${get(pincodeServiceableConfig, 'form.submit.path', '')}${pinCode}` : null}
              className={`${styles.pinCodeSubmit} ${isValidPinCode ? '' : styles.disabled}`} >
              {get(pincodeServiceableConfig, 'form.submit.text', '')}
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckDeliveryAvailability;
