import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { withRouter } from 'react-router';
import styles from './check-delivery-availability.css';

class CheckDeliveryAvailabilityResult extends React.Component {
  state = { serviceProvided: null }

  componentDidMount() {
    const servicablePinCodes = get(window, '__myx_covid_serviceable_pincodes__', []);
    const essentialServicablePinCodes = get(window, '__myx_covid_essential_serviceable_pincodes__', []);
    const pincodeServiceableResultConfig = get(window, '__myx_covid_serviceable_pincode_config__', {});

    const { pinCode } = this.props.params;

    if (servicablePinCodes.includes(+pinCode)) {
      document.title = get(pincodeServiceableResultConfig, 'allServices.title', '');
      this.setState({ serviceProvided: 'ALL' });
    } else if (essentialServicablePinCodes.includes(+pinCode)) {
      document.title = get(pincodeServiceableResultConfig, 'essentialServices.title', '');
      this.setState({ serviceProvided: 'ESSENTIAL' });
    } else {
      document.title = get(pincodeServiceableResultConfig, 'noServices.title', '');
      this.setState({ serviceProvided: 'NONE' });
    }
  }

  renderDeliveryAvailabilityResult = (config) => {
    const { pinCode } = this.props.params;

    return (
      <div className={styles.container}>
        <div className={styles.checkDeliveryAvailability}>
          <img
            className={styles.deliveryCheckResultImg}
            src={config.img}
            alt="Check delivery availability" />
          <div className={styles.heading}>{config.heading} {pinCode}</div>
          {config.subHeading ? <div className={styles.subHeading}>{config.subHeading}</div> : null}
          <div className={styles.text}>{config.message}</div>
          {config.link ? <a href={config.link.path} className={styles.link}>{config.link.text}</a> : null}
          {config.button ? <a href={config.button.path} className={styles.button}>{config.button.text}</a> : null}
        </div>
      </div>
    );
  }

  render() {
    const { serviceProvided } = this.state;
    const pincodeServiceableResultConfig = get(window, '__myx_covid_serviceable_pincode_config__', {});

    if (serviceProvided === 'ALL' && pincodeServiceableResultConfig.allServices) {
      return this.renderDeliveryAvailabilityResult(pincodeServiceableResultConfig.allServices);
    } else if (serviceProvided === 'ESSENTIAL' && pincodeServiceableResultConfig.essentialServices) {
      return this.renderDeliveryAvailabilityResult(pincodeServiceableResultConfig.essentialServices);
    } else if (serviceProvided === 'NONE' && pincodeServiceableResultConfig.noServices) {
      return this.renderDeliveryAvailabilityResult(pincodeServiceableResultConfig.noServices);
    }

    return null;
  }
}

CheckDeliveryAvailabilityResult.propTypes = {
  params: PropTypes.object
};

export default withRouter(CheckDeliveryAvailabilityResult);
