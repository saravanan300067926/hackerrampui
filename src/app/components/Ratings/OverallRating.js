import React from 'react';
import PropTypes from 'prop-types';
import defaultStyles from './index.css';
import StarIcon from './StarIcon';
import { isSafariBrowser } from '../../utils';
import { formateCount } from './helper';

const scrollToDetailedRating = () => {
  const detailedRatingElement = document.querySelector('#detailedRatingContainer');
  if (isSafariBrowser()) {
    detailedRatingElement.scrollIntoView({ block: 'nearest' });
    window.scrollBy(0, -200);
  } else {
    detailedRatingElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
  }
};

const OverallRating = ({ data }) => {
  if (!data) return null;

  const { averageRating, totalCount } = data;

  return (
    <div className={defaultStyles.overallRatingContainer}>
      <div className={defaultStyles.overallRating} onClick={scrollToDetailedRating}>
        <div>{averageRating.toFixed(1).substring(2) === '0' ? averageRating.toFixed(0) : averageRating.toFixed(1)}</div>
        <StarIcon rating={averageRating} cssClasses={defaultStyles.starIcon} />
        <div className={defaultStyles.separator}>|</div>
        <div className={defaultStyles.ratingsCount}>{formateCount(totalCount, 1)} Ratings</div>
      </div>
    </div>
  );
};

OverallRating.propTypes = {
  data: PropTypes.shape({
    averageRating: PropTypes.number.isRequired,
    totalCount: PropTypes.number.isRequired
  })
};

export default OverallRating;
