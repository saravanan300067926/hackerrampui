import React from 'react';
import PropTypes from 'prop-types';
import defaultStyles from './index.css';

const RatingBar = ({ rating, total }) => (
  <div className={`${defaultStyles.flexRow} ${defaultStyles.ratingBarContainer}`}>
    <div className={defaultStyles.rating}>
      <span className={defaultStyles.ratingLevel}>{rating.rating}</span>
      <span className={`myntraweb-sprite ${defaultStyles.grayStarIcon}`} />
    </div>
    <progress min="0" max={total} value={rating.count} data-rating={rating.rating} />
    <div className={defaultStyles.count}>{rating.count > 0 ? rating.count : ''}</div>
  </div>
);

RatingBar.propTypes = {
  rating: PropTypes.object,
  total: PropTypes.number
};

export default RatingBar;
