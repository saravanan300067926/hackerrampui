import React from 'react';
import PropTypes from 'prop-types';
import defaultStyles from './index.css';
import RatingBar from './RatingsBar';
import StarIcon from './StarIcon';
import { formateCount } from './helper';

const DetailedRating = ({ data, inPdp }) => {
  if (!data) return null;

  const { averageRating, totalCount, ratingInfo } = data;
  const overallRating = [{ rating: 1, count: 0 }, { rating: 2, count: 0 },
    { rating: 3, count: 0 }, { rating: 4, count: 0 }, { rating: 5, count: 0 }];

  ratingInfo.forEach((r) => {
    const index = overallRating.findIndex(item => item.rating === r.rating);
    overallRating[index] = r;
  });

  return (
    <div id="detailedRatingContainer" className={`${defaultStyles.detailedRatingContainer} ${inPdp ? defaultStyles.inPdp : ''}`}>
      <div id="headingContainer" className={defaultStyles.header}>Ratings
        <span className={`myntraweb-sprite ${defaultStyles.productRatingsIcon}`} />
      </div>
      <div className={`${defaultStyles.flexRow} ${defaultStyles.margin22}`}>
        <div className={defaultStyles.flexColumn}>
          <div className={`${defaultStyles.flexRow} ${defaultStyles.averageRating}`}>
            <span>{averageRating.toFixed(1).substring(2) === '0' ? averageRating.toFixed(0) : averageRating.toFixed(1)}</span>
            <StarIcon rating={averageRating} cssClasses={defaultStyles.starIcon} />
          </div>
          <div className={defaultStyles.countDesc}>{formateCount(totalCount, 1)} Verified Buyers</div>
        </div>
        <div className={defaultStyles.separator} />
        <div>
          {overallRating.reverse().map(rating => <RatingBar key={rating.rating} total={totalCount} rating={rating} />)}
        </div>
      </div>
    </div>
  );
};

DetailedRating.propTypes = {
  inPdp: PropTypes.bool,
  data: PropTypes.shape({
    averageRating: PropTypes.number.isRequired,
    totalCount: PropTypes.number.isRequired,
    ratingInfo: PropTypes.array.isRequired
  })
};

export default DetailedRating;
