export function getStyleBasedOnRating(rating) {
  rating = parseFloat(rating);
  if (rating < 2) return 'productRatingsLowIcon';
  else if (rating < 3) return 'productRatingsAverageIcon';
  else if (rating < 4) return 'productRatingsGoodIcon';
  else if (rating <= 5) return 'productRatingsExcellentIcon';

  return null;
}
