import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.css';
import { getStyleBasedOnRating } from './helpers';

const StarIcon = ({ rating, cssClasses }) => (
  <span className={`myntraweb-sprite ${cssClasses} ${styles[getStyleBasedOnRating(rating)]}`} />
);

StarIcon.propTypes = {
  rating: PropTypes.number,
  cssClasses: PropTypes.string
};

export default StarIcon;
