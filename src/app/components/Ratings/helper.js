export function formateCount(num, digits) {
  let i;
  const si = [
    { suffix: 1, symbol: '' },
    { suffix: 1e3, symbol: 'k' },
    { suffix: 1e6, symbol: 'M' },
    { suffix: 1e9, symbol: 'G' },
    { suffix: 1e12, symbol: 'T' },
    { suffix: 1e15, symbol: 'P' },
    { suffix: 1e18, symbol: 'E' }
  ];
  const regEx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].suffix) {
      break;
    }
  }
  return (
    (num / si[i].suffix).toFixed(digits).replace(regEx, '$1') + si[i].symbol
  );
}
