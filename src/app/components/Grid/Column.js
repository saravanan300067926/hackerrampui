import React, { PropTypes } from 'react';
import styles from './column.css';

function getStyle(width) {
  let style = {};
  // ms lowecase https://facebook.github.io/react/tips/inline-styles.html
  style = {
    WebkitBoxFlex: width,
    WebkitFlex: width,
    MozBoxFlex: width,
    msFlex: width,
    flex: width
  };
  return style;
}

const Column = ({ children, width = 1, className = '', trackEvent }) => (
  <div
    onClick={trackEvent}
    className={`${className} ${styles.base}`}
    style={getStyle((Number(width) || 1))}>
    {children}
  </div>
);

Column.propTypes = {
  children: React.PropTypes.oneOfType([
    React.PropTypes.element,
    React.PropTypes.string,
    React.PropTypes.arrayOf(React.PropTypes.element)
  ]),
  width: PropTypes.number,
  className: PropTypes.string,
  trackEvent: PropTypes.func
};


export default Column;
