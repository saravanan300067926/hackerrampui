import React, { PropTypes } from 'react';
import styles from './row.css';

// FIXME: hacky fix for webkitflex, find out issue with autoprefixer
const Row = ({ children, className = '' }) => (
  <div className={`${className} ${styles.base}`} style={{ display: '-webkit-flex' }}>
    {children}
  </div>
);

Row.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  className: PropTypes.string,
  onClick: PropTypes.func
};


export default Row;
