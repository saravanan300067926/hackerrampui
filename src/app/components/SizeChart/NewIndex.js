import React from 'react';
import at from 'v-at';
import Madalytics from '../../utils/trackMadalytics';
import stylesWeb from './sizeChartWeb.css';
import scaleAndUnitStyles from './scaleAndUnits.css';
import classNames from 'classnames/bind';
import ActionCTA from '../ActionCTA';
import SizeTable from './SizeTable';
import { getProductImageUrl } from '../Pdp/utils';
import PriceInfo from '../Pdp/PriceInfo';
import get from "lodash/get";
import find from "lodash/find";
import { isSellerAvailableForSku, getDeliveryMessage, getDefaultSkuSeller } from '../Pdp/helper';
import serviceabilityAPI from '../Pdp/serviceabilityAPI';
import { transformSizes } from './helper.js';

let bindClassNames;

const FOOT_WEAR_DEFAULT_SIZE = {
  'Men': 'UK',
  'Boys': 'EURO',
  'Girls': 'EURO',
  'Women': 'UK',
  'Women-FH': 'EURO',
  'Unisex': 'UK'
};
const INCH = 'Inches';
const CM = 'cm';
let styles;

/*
* Gets default scale based on gender and articleType
*/
function getDefaultScale(gender, articleType) {
  let defaultScale = FOOT_WEAR_DEFAULT_SIZE[gender];

  if (gender === 'Women' && (articleType === 'Heels' || articleType === 'Flats')) {
    defaultScale = FOOT_WEAR_DEFAULT_SIZE['Women-FH'];
  }

  return defaultScale;
}

class SizeChart extends React.Component {
  constructor(props) {
    super(props);
    let stateVal = at(this.props, 'location.dehydratedState');
    stateVal = at(this.props, 'data');
    styles = Object.assign(stylesWeb, scaleAndUnitStyles);

    bindClassNames = classNames.bind(styles);
    const measurements = at(stateVal, 'sizes.0.measurements') || [];
    const unit = get(measurements, '0.unit');
    const sizes = at(stateVal, 'sizes') || [];
    this.state = {
      selectedSkuid: props.selectedSkuid,
      unit,
      scalesVisible: this.getScalesVisible(),
      sizeChartData: sizes,
      showSellers: false,
      deliveryMessage: null,
      serviceabilityCallInProgress: false
    };

    this.convertDataUnits = this.convertDataUnits.bind(this);
    this.onSelectScale = this.onSelectScale.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.getSelectedSellerPartnerId = this.getSelectedSellerPartnerId.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.fetchDeliveryDate = this.fetchDeliveryDate.bind(this);
  }

  componentDidMount() {
    document.addEventListener('keydown', this.onKeyPress);
    this.fetchDeliveryDate(this.props);
    const { sizeChartData, unit } = this.state;
    this.setState({
      sizeChartData: transformSizes(sizeChartData, unit)
    })
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyPress);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedSkuid !== this.state.selectedSkuid) {
      this.setState({
        selectedSkuid: nextProps.selectedSkuid
      });
    }
    this.fetchDeliveryDate(nextProps);
  }

  fetchDeliveryDate(nextProps) {
    const pincode = localStorage.getItem("pincode") || null;
    let selectedSkuid = nextProps.selectedSkuid;
    if (!selectedSkuid) {
      selectedSkuid = getDefaultSkuSeller(nextProps.data).skuId;
    }
    const selectedSkuData = find(
      get(nextProps, "data.sizes"),
      ({ skuId }) => skuId === selectedSkuid
    );
    const selectedSellerPartnerId = get(
      selectedSkuData,
      "selectedSeller.sellerPartnerId"
    );
    const sellerServiceable = get(
      selectedSkuData,
      "selectedSeller.serviceable"
    );
    if (!selectedSkuid || !pincode) {
      this.setState({
        deliveryMessage: null,
        selectedSellerPartnerId
      });
    } else if (
      pincode &&
      sellerServiceable &&
      !this.state.serviceabilityCallInProgress &&
      (!this.state.deliveryMessage ||
        selectedSkuid !== this.props.selectedSkuid ||
        selectedSellerPartnerId !== this.state.selectedSellerPartnerId)
    ) {
      this.setState({
        deliveryMessage: null,
        selectedSellerPartnerId,
        serviceabilityCallInProgress: true
      });
      serviceabilityAPI
        .checkV2(
          get(nextProps, "data"),
          [
            {
              skuId: selectedSkuid,
              sellerPartnerId: selectedSellerPartnerId
            }
          ],
          pincode
        )
        .then(serviceability => {
          const promiseDate = get(
            (get(serviceability, "itemServiceabilityEntries") || []).find(
              itemEntry => {
                return (
                  Number(itemEntry.skuId) === selectedSkuid &&
                  Number(itemEntry.itemReferenceId) === selectedSellerPartnerId
                );
              }
            ),
            "serviceabilityEntries.0.promiseDate"
          );
          if (promiseDate) {
            const deliveryMessage = `${getDeliveryMessage(
              promiseDate,
              get(nextProps, "data.flags.disableBuyButton"),
              get(nextProps, "data.serviceability.launchDate"),
              get(nextProps, "data.preOrder")
            )} - ${pincode}`;
            this.setState({
              deliveryMessage,
              selectedSellerPartnerId,
              serviceabilityCallInProgress: false
            });
          } else {
            this.setState({
              deliveryMessage: null,
              selectedSellerPartnerId,
              serviceabilityCallInProgress: false
            });
          }
        });
    }
  }

  onSelectScale(e) {
    const target = e.currentTarget.parentElement;
    const scale = target.id;
    const scalesVisible = at(this, 'state.scalesVisible');

    const newScales = [...scalesVisible];
    if (target.firstElementChild.checked) {
      newScales.push(scale);
    } else {
      const index = newScales.indexOf(scale);
      if (index !== -1) {
        newScales.splice(index, 1);
      }
    }

    Madalytics.sendEvent('SizeChartSwitch', 'react', {
      'entity_id': at(window, '__myx.pdpData.id')
    }, { widget: { type: 'size_unit', name: scale } });

    this.setState({
      scalesVisible: newScales
    });
  }

  onKeyPress = event => {
    if (event.key === 'Escape') {
      this.props.hide();
    }
  };

  getScalesVisible() {
    const analyticsData = at(window, '__myx.pdpData.analytics');
    if (at(analyticsData, 'masterCategory') !== 'Footwear') {
      return [];
    }

    const gender = at(analyticsData, 'gender');
    const articleType = at(analyticsData, 'articleType');

    return [getDefaultScale(gender, articleType)];
  }

  /* get Image to be displayed on size chart */
  getSelectedImage() {
    const styleImage = at(this.props, 'data.media.albums.0');
    if (styleImage) {
      const imgConfig = {
        width: 350,
        height: 500,
        q: 20
      };
      return getProductImageUrl(at(styleImage, 'images.0'), imgConfig);
    }
    return null;
  }

  convertDataUnits(e) {
    const target = e.currentTarget;
    const isInch = target.id === INCH;

    Madalytics.sendEvent('SizeChartSwitch', 'react', {
      'entity_id': at(window, '__myx.pdpData.id')
    }, { widget: { type: 'standard', name: isInch ? 'inches' : 'cms' } });

    this.setState({
      unit: target.id,
      sizeChartData: transformSizes(this.state.sizeChartData, target.id)
    });
  }

  getSelectedSellerPartnerId() {
    const data = this.props.data || {};
    return get(
      find(get(data, "sizes"), ({ skuId }) => skuId === this.state.selectedSkuid),
      "selectedSeller.sellerPartnerId"
    );
  }

  handleClose() {
    this.setState({
      showSellers: false
    })
  }

  render() {

    const data = this.props.data || {};
    const sizes = get(this, 'state.sizeChartData');
    const { selectedSkuid, deliveryMessage } = this.state;
    const selectedSkuData = find(
      get(data, "sizes"),
      ({ skuId }) => skuId === selectedSkuid
    );
    const selectedSellerPartnerId = this.getSelectedSellerPartnerId();
    const selectedSellerData =
      (selectedSkuid
        ? find(
          get(selectedSkuData, "sizeSellerData"),
          seller => seller.sellerPartnerId === selectedSellerPartnerId
        )
        : get(data, "selectedSeller", {}));
    const discountData =
      (selectedSkuid
        ? find(
          get(data, "discounts"),
          discount =>
            discount.discountId === get(selectedSellerData, "discountId")
        ) : get(selectedSellerData, "discount")) || {};
    const mrp =
      get(selectedSkuData, "price") ||
      get(data, "mrp") ||
      get(preloadData, "mrp");
    const discountedPrice = get(selectedSellerData, "discountedPrice");
    const sellerName =
      get(
        find(
          get(data, "sellers"),
          seller => seller.sellerPartnerId === selectedSellerPartnerId
        ),
        "sellerName"
      );
    const sizeSellers = (get(selectedSkuData, "sizeSellerData") || []).filter(
      seller => isSellerAvailableForSku(selectedSkuData, seller)
    );
    const moreSellersMessage = `${sizeSellers.length - 1} more seller${
      sizeSellers.length > 2 ? "s" : ""
      } available `;
    const bestPrice = Math.min(
      ...sizeSellers.map(seller => seller.discountedPrice || mrp)
    );

    const sizeTableSellerInfo = {
      bestPrice,
      moreSellersMessage,
      sellerName,
      sizeSellers,
      discountedPrice,
      selectedSellerData
    }

    const ProductInfo = {
      Brand: get(this.props, 'data.brand.name', ''),
      productName: get(this.props, 'data.name', ''),
      ImageURL: this.getSelectedImage()
    }


    if (this.props.showSizeChart) {
      return (
        <div className={styles.container}>
          <div className={styles.content}>
            <div className={styles.header}>
              <button className={styles.close} onClick={this.props.hide}>
                <span className={`myntraweb-sprite ${styles.modalclose}`} />
              </button>
              <img className={styles['sizechart-image']} src={this.getSelectedImage()} />
              <div className={styles.productInfo}>
                <div className={styles.title}>
                  {at(this.props, 'data.brand.name')}
                </div>
                <div className={styles.subtitle}>
                  {at(this.props, 'data.name')}
                </div>
                <PriceInfo
                  discountData={discountData}
                  mrp={mrp}
                  discountedPrice={discountedPrice} />
                <span className={styles.deliveryMessage}>{deliveryMessage}</span>
              </div>
            </div>
            <SizeTable
              closeSizeChart={this.props.hide}
              data={data}
              styles={styles}
              showSellers={this.state.showSellers}
              handleClose={this.handleClose}
              sizes={sizes}
              recoTextObj={at(this, 'props.recoTextObj')}
              bindClassNames={bindClassNames}
              unit={at(this, 'state.unit')}
              showReco={at(this, 'props.showReco')}
              isSizeChart={true}
              scalesVisible={at(this, 'state.scalesVisible')}
              selectSize={at(this, 'props.selectSize')}
              newSizeChart={true}
              selectedSkuid={at(this, 'state.selectedSkuid')}
              sizeProfiles={at(this, 'props.sizeProfiles')}
              selectProfile={at(this, 'props.selectProfile')}
              selectedProfile={at(this, 'props.selectedProfile')}
              convertDataUnits={this.convertDataUnits}
              profilesShown={this.state.profilesShown}
              showProfiles={this.showProfiles}
              relatedProducts={at(this, 'props.relatedProducts')}
              showSimilarProducts={at(this, 'props.showSimilarProducts')}
              sizeTableSellerInfo={sizeTableSellerInfo}
              ProductInfo={ProductInfo}
              serviceabilityCallDone={this.props.serviceabilityCallDone}
              addToBag={at(this.props, 'addToBag')}
              onSelectScale={at(this, 'onSelectScale')} />
            <ActionCTA
              isSizeChart={true}
              stylesCTA={styles}
              systemAttributes={at(window, '__myx.pdpData.systemAttributes')}
              state={at(this.props, 'state')}
              addToBag={at(this.props, 'addToBag')}
              addToWishlist={at(this.props, 'addToWishlist')} /></div>
        </div>
      );
    } return null;
  }
}

SizeChart.propTypes = {
  serviceabilityCallDone: React.PropTypes.bool,
  showSizeChart: React.PropTypes.bool,
  hide: React.PropTypes.func
};

export default SizeChart;
