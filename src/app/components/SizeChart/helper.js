const INCH_TO_CM = 2.54;
const CM = 'cm';
const INCH = 'Inches';

/*
* Transform size data based on operation
*/
const transformSizes = (sizes = [], unit) => {
  if (unit === CM || unit === INCH) {
    const operation = unit === CM ? '*' : '/';
    sizes.forEach((size) => {
      if (size.measurements) {
        size.measurements.forEach((m) => {
          if (m.unit !== unit) {
            m.value = checkForSizeRange(m, operation, INCH_TO_CM);
            m.unit = unit;
          }
        });
      }
    });
  }
  return sizes;
}

/*
* Check for size range and change the value
*/
const checkForSizeRange = (size = [], operation, factor) => {
  if (size.minValue && (size.minValue !== size.maxValue)) {
    switch (operation) {
      case '*':
        return `${(size.minValue * factor).toFixed(1)} - ${(size.maxValue * factor).toFixed(1)}`;
      case '/':
        return `${(size.minValue * factor).toFixed(1)} - ${(size.maxValue * factor).toFixed(1)}`;
      default:
        return `${Number(size.minValue).toFixed(1)} - ${Number(size.maxValue).toFixed(1)}`;
    }
  }
  switch (operation) {
    case '*':
      return (Number(size.value) * factor).toFixed(1);
    case '/':
      return (Number(size.value) / factor).toFixed(1);
    default:
      return Number(size.value).toFixed(1);
  }
}

export { transformSizes };