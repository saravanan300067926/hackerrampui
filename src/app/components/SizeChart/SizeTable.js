import React from 'react';
import at from 'v-at';
import get from 'lodash/get';
import throttle from 'lodash/throttle';
import commonStyles from '../../resources/common.css';
import { securify } from '../../utils/securify';
import SizeReco from '../Pdp/SizeReco';
import { getFeatures } from '../../utils';
import { inPriceRevealMode } from '../../utils/slotUtils';
import priorityCheckout from '../../utils/priorityCheckoutUtils';
import Sellers from '../Pdp/Sellers';
import { isPreLaunch } from '../Pdp/utils';

const sellableInventoryModeEnabled = getFeatures('sellableInventoryModeEnabled') || false;
const secure = securify();
const EMPTY_ARR_READ_ONLY = [];
const FOOT_WEAR_SCALES = ['UK', 'US', 'EURO'];
const FOOT_WEAR_DEFAULT_SIZE = {
  'Men': 'UK',
  'Boys': 'EURO',
  'Girls': 'EURO',
  'Women': 'UK',
  'Women-FH': 'EURO',
  'Unisex': 'UK'
};
const INCH = 'Inches';
const CM = 'cm';

const getDefaultScale = (gender, articleType) => {
  let defaultScale = FOOT_WEAR_DEFAULT_SIZE[gender];

  if (gender === 'Women' && (articleType === 'Heels' || articleType === 'Flats')) {
    defaultScale = FOOT_WEAR_DEFAULT_SIZE['Women-FH'];
  }

  return defaultScale;
};

const getScaleSelector = (props) => {
  const sizeList = at(props, 'sizes.0.allSizesList') || EMPTY_ARR_READ_ONLY;
  const gender = at(window, '__myx.pdpData.analytics.gender');
  const articleType = at(window, '__myx.pdpData.analytics.articleType');
  return (
    <div className={at(props, 'styles').scaleSelector}>
        {
            sizeList.map((size) => (
                (FOOT_WEAR_SCALES.indexOf(at(size, 'prefix')) !== -1 && at(size, 'prefix') !== getDefaultScale(gender, articleType)) &&
                  <div className={at(props, 'styles').scale}>
                    <label id={size.prefix} className={at(props, 'bindClassNames')('scaleCheckbox', commonStyles.customCheckbox)} >
                      <input
                        type="checkbox"
                        defaultChecked={at(props, 'scalesVisible').indexOf(at(size, 'prefix')) !== -1}
                        onClick={at(props, 'onSelectScale')} />
                      <div className={commonStyles.checkboxIndicator} />
                    </label>
                    <span className={at(props, 'styles').scaleText}>{at(size, 'prefix')}</span>
                  </div>
            ))
        }
    </div>
    );
};

const getInchesCmToggle = (props) => {
  const inchClass = at(props, 'bindClassNames')('metric', 'left', { 'selected': at(props, 'unit') === INCH });
  const cmClass = at(props, 'bindClassNames')('metric', 'right', { 'selected': at(props, 'unit') === CM });

  return (
    <div className={at(props, 'styles').inchCmToggle}>
      <button id={INCH} className={inchClass} onClick={at(props, 'convertDataUnits')}>in</button>
      <button id={CM} className={cmClass} onClick={at(props, 'convertDataUnits')}>cm</button>
    </div>
    );
};

const getAllMeasurements = (props) => {
  let maxDim = 0;
  let measurements = [];
  at(props, 'sizes').forEach((size) => {
    if (size.measurements && size.measurements.length > maxDim) {
      maxDim = size.measurements.length;
      measurements = size.measurements;
    }
  });
  return measurements;
};

const showSize = (scalesVisible, size) => {
  const sizePrefix = at(size, 'prefix');
  let toShowSize;

  if (FOOT_WEAR_SCALES.indexOf(sizePrefix) !== -1) {
    toShowSize = (scalesVisible.length === 0 || scalesVisible.indexOf(sizePrefix) !== -1);
  } else {
    toShowSize = true;
  }
  return toShowSize;
};

const getSizeHeader = (props) => {
  const sizes = at(props, 'sizes.0.allSizesList') || EMPTY_ARR_READ_ONLY;
  const scalesVisible = at(props, 'scalesVisible');
  const measurements = getAllMeasurements(props);
  const currentUnit = at(props, 'unit') === INCH ? 'in' : 'cm';

  return (
    <thead>
      <tr className={at(props, 'styles').newRow}>
        <th className={at(props, 'styles').newCell} />
        {sizes.map((size) => (
            showSize(scalesVisible, size) && <th className={`${at(props, 'styles').newCell} ${at(props, 'styles')['cell-title']}`}>
                {(at(size, 'prefix') || at(size, 'size') || 'Size')}
            </th>
        ))}
        {measurements.map((m) => {
          const unit = (m.unit === 'Inches' || m.unit === 'cm') ? currentUnit : m.unit;
          return <th className={`${at(props, 'styles').newCell} ${at(props, 'styles')['cell-title']}`}>{`${m.name} (${unit})`}</th>;
        })}
      </tr>
    </thead>
    );
};

const getMeasurement = (measurements, name) => measurements.find((m) => (m.name === name));

const getSizeInfo = (props) => {
  const sizes = at(props, 'sizes');
  const scalesVisible = at(props, 'scalesVisible');
  const allMeasurements = getAllMeasurements(props);
  const selectedSkuId = at(props, 'selectedSkuid');
  const selectSize = at(props, 'selectSize');
  const recoTextObj = at(props, 'recoTextObj');
  const recommendedSkuId = at(recoTextObj, 'recommendedSkuId');
  return (
    <tbody className={at(props, 'styles')['table-body']}>
        {
            sizes.map((size, index) => {
              const measurements = size.measurements || [];
              let available = size.available &&
              (size.sizeSellerData || []).some(
                seller =>
                  (seller.hasOwnProperty("serviceable") && seller.serviceable) ||
                  !seller.hasOwnProperty("serviceable")
              );
              if (sellableInventoryModeEnabled || inPriceRevealMode() || priorityCheckout()) {
                available = get(size, "selectedSeller.sellableInventoryCount");
              }
              const skuId = size.skuId;
              const sellerPartnerId = get(size, "selectedSeller.sellerPartnerId");
              const checked = (skuId === selectedSkuId);
              const selectRecommended = (skuId === recommendedSkuId);
              return (<tr
                className={
                `${at(props, 'styles').newRow} ${!available ? at(props, 'styles').disabled : ''} ${checked ? at(props, 'styles').bold : ''}
${selectRecommended ? at(props, 'styles')['select-rc-row'] : ''}`
              }
                onClick={(e) => {
                  selectSize(e, skuId, null, sellerPartnerId);
                }}>
                <label className={`${commonStyles.customRadio} ${commonStyles.newCustomRadio}`}>
                  <input
                    className={`${at(props, 'styles.input')} ${at(props, 'styles.inputSizeChart')}`}
                    type="radio"
                    id={index + skuId}
                    key={index + skuId}
                    checked={checked} />
                  <div
                    className={`${commonStyles.radioIndicator} ${at(props, 'styles').radioIndicator}
${commonStyles.radioIndicatorNew} ${at(props, 'styles').radioIndicatorNew}`} />
                </label>
                {
                    size.allSizesList.map((s) => (
                        showSize(scalesVisible, s) &&
                          <td className={`${at(props, 'styles').newCell} ${!available ? at(props, 'styles')['line-through'] : ''}`}>{s.sizeValue}</td>
                    ))
                }
                    {
                        allMeasurements.map((m) => {
                          const mm = getMeasurement(measurements, m.name);
                          return (<td className={`${at(props, 'styles').newCell} ${!available ? at(props, 'styles')['line-through'] : ''}`}>
                                {mm ? mm.value : '-'}
                          </td>);
                        })
                    }
              </tr>);
            })
        }
    </tbody>
    );
};

const getSizeChartTable = (props = {}) => {
  const sizes = at(props, 'sizes');
  if (sizes.length) {
    let containerStyles = {};
    if (props.isApp) {
      containerStyles.width = "100%";
      containerStyles.overflowX = "auto";
    }

    return (
      <div style={containerStyles} className={at(props, 'styles').newTableContainer}>
        <table className={at(props, 'styles').tableNew}>
            {getSizeHeader(props)}
            {getSizeInfo(props)}
        </table>
      </div>
    );
  }
  return null;
};

const getSizeChartImage = (props) => (
  <div className={at(props, 'styles')['image-size']}>
    <div className={at(props, 'styles')['measure-title']}>How to measure yourself</div>
    <img
      className={at(props, 'styles')['image-size-chart']}
      src={`${secure(at(window, '__myx.pdpData.sizechart.sizeChartUrl') || at(window, '__myx.pdpData.sizechart.sizeRepresentationUrl'))}`} />
  </div>
);

const getSellerInfo = (props) => {
  const styles = at(props, 'styles');
  const priceRevealOrPrelaunch = isPreLaunch() || inPriceRevealMode();
  const { bestPrice, moreSellersMessage, sellerName, sizeSellers, discountedPrice } = props.sizeTableSellerInfo;
  return props.selectedSkuid && !priceRevealOrPrelaunch ? (
    <div>
      <div className={styles.divider} />
      <div className={styles.sellerInfo}>
        <div>
          Seller: <b>{sellerName}</b>
        </div>
        <div>
          {sizeSellers.length > 1 && (
            <div>
              <span
                className={styles.sizeMoreSellers}
                onClick={props.showSellersPopup}
              >
                {moreSellersMessage}
              </span>
              {bestPrice < discountedPrice ? (
                <span>
                  from Rs.
                  {` ${bestPrice}`}
                </span>
              ) : null}
            </div>
          )}
        </div>
      </div>
    </div>
  ) : null;
}

class SizeTable extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tabSelected: 'sizechart',
      translateStyle: { transition: 'transform 0.3s ease-out' },
      showSellersPopup: false
    };
    this.trackScrolling = this.trackScrolling.bind(this);
    this.throttledScroll = throttle(this.trackScrolling, 500, {
      leading: false,
      trailing: true
    });
    this.showSellersPopup = this.showSellersPopup.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  componentDidMount() {
    if (at(this, 'props.isApp')) {
      window.addEventListener('scroll', this.throttledScroll, false);
    } else {
      const sizeChartPopup = document.querySelector('.sizeChartWeb-content');
      sizeChartPopup.addEventListener('scroll', this.throttledScroll, false);
    }
    const { top: sizechartTop } = this.refs.sizechart.getBoundingClientRect();
    this.setState({ scrollPosition: sizechartTop });
  }

  componentWillUnmount() {
    if (at(this, 'props.isApp')) {
      window.removeEventListener('scroll', this.throttledScroll, false);
    } else {
      const sizeChartPopup = document.querySelector('.sizeChartWeb-content');
      sizeChartPopup.removeEventListener('scroll', this.throttledScroll, false);
    }
  }
  trackScrolling() {
    const refs = at(this, 'refs');
    const { top: measurementTop } = refs.measurement.getBoundingClientRect();
    const { top: sizechartTop } = refs.sizechart.getBoundingClientRect();
    const { top: navTop, height: navHeight } = refs.navTab.getBoundingClientRect();
    const scrollDirection = this.state.scrollPosition;
    this.setState({ scrollPosition: sizechartTop });
    if (navTop <= 0 && measurementTop <= 260) {
      this.setState({ tabSelected: 'measurement' });
    } else if (
      (sizechartTop > 100 && navTop <= navHeight) ||
      (this.state.tabSelected === 'measurement' &&
        sizechartTop > scrollDirection)
    ) {
      this.setState({
        tabSelected: 'sizechart',
        translateStyle: { transition: 'transform 0.3s ease-out', transform: 'translateY(0px)' }
      });
    }
  }

  changeTab = tabSelected => {
    const refs = at(this, 'refs');
    this.setState({
      tabSelected
    });
    let translateValue = 0;
    const { height: sizechartHeight, top: sizechartTop } = refs.sizechart.getBoundingClientRect();
    if (tabSelected === 'measurement') {
      translateValue = -(sizechartHeight - 12);
    }
    const { top: navTop } = refs.navTab.getBoundingClientRect();
    if (navTop <= 0 && tabSelected === 'sizechart') {
      if (at(this, 'props.isApp')) {
        window.scroll({ top: sizechartTop, behavior: 'smooth' });
      } else {
        const sizeChartPopup = document.querySelector('.sizeChartWeb-content');
        sizeChartPopup.scroll(0, sizechartTop);
      }
    }
    this.setState({
      translateStyle: { transition: 'transform 0.3s ease-out', transform: `translateY(${translateValue}px)` }
    });
  };

  showSellersPopup() {
    this.setState({
      showSellersPopup: true
    })
  }

  handleClose(closePoint) {
    this.setState({
      showSellersPopup: false
    });
    if(closePoint === 'addToBag') {
      this.props.closeSizeChart();
    }
  }

  render() {
    const { sizeSellers, selectedSellerData } = this.props.sizeTableSellerInfo || {};
    const { Brand, productName, ImageURL } = this.props.ProductInfo || {};
    const { selectedSkuid, data } = this.props;
    const styles = at(this, 'props.styles');
    const sizeChartSelected = this.state.tabSelected === 'sizechart';
    const translateStyle = at(this, 'state.translateStyle');
    if (at(this, 'props.sizes')) {
      const category = at(window, '__myx.pdpData.analytics.masterCategory');
      let disclaimerText = at(window, '__myx.pdpData.sizeChartDisclaimerText');
      if (disclaimerText) {
        const disclaimerTextUnit = at(this, 'props.unit') === INCH ? ' Inches' : ' Cms';
        disclaimerText += disclaimerTextUnit;
      }
      return (
        <div>
          <div className={styles.tabContainer} ref="navTab">
            <div
              className={`${styles.sizeChartTab} ${sizeChartSelected ? styles.tabSelected : ''}`}
              onClick={() => { this.changeTab('sizechart'); }}>
              Size Chart
            </div>
            <div
              className={`${styles.measureTab} ${!sizeChartSelected ? styles.tabSelected : ''}`}
              onClick={() => { this.changeTab('measurement'); }}>
              How to measure
            </div>
          </div>
          <div className={styles['info-container']}>
            <div ref="sizechart" style={translateStyle}>
              {category === 'Footwear' && <div>{getScaleSelector(this.props)}</div>}
              <div className={styles.inchCmContainer}>{getInchesCmToggle(this.props)}</div>
              {at(this, 'props.showReco') && <SizeReco {...this.props} />}
              <div className={styles.info}>
                {getSizeChartTable(this.props)}
              </div>
              {!this.props.isApp && getSellerInfo({...this.props, showSellersPopup: this.showSellersPopup})}
              <div className={styles.divider} />
            </div>
            <div ref="measurement" style={translateStyle}>
              {disclaimerText && <div className={styles.unitText}>{`* ${disclaimerText}`}</div>}
              {getSizeChartImage(this.props)}
            </div>
          </div>
          { this.state.showSellersPopup &&
            <Sellers
              handleClose={this.handleClose}
              data={data}
              Brand={Brand}
              productName={productName}
              ImageURL={ImageURL}
              sizeSellers={sizeSellers}
              selectedSkuid={selectedSkuid}
              selectedSellerData={selectedSellerData}
              serviceabilityCallDone={this.props.serviceabilityCallDone}
              addToBag={this.props.addToBag}/>
          }
        </div>
      );
    }
    return null;
  }
}

export default SizeTable;
