/* global MyntApp:false */
/* global webkit:false */

import React from 'react';
import at from 'v-at';
import bus from 'bus';
import get from 'lodash/get';
import find from "lodash/find";
import Loader from '../Loader';
import TrackMadalytics from '../../utils/trackMadalytics';
import stylesApp from './sizeChartApp.css';
import scaleAndUnitStyles from './scaleAndUnits.css';
import classNames from 'classnames/bind';
import ActionCTA from '../ActionCTA';
import priorityCheckout, { CART_FULL_ERROR_CODE, PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE } from '../../utils/priorityCheckoutUtils';
import Client from '../../services';
import { getProductImageUrl } from '../Pdp/utils';
import Track from '../../utils/track';
import config from '../../config';
import { isBrowser } from '../../utils';
import { inPriceRevealMode, isShowSlotPopup, isSlotEntryEnabled } from '../../utils/slotUtils';
import { addProductToWlSummaryStore } from '../../utils/wishlistSummaryManager';
import SizeTable from './SizeTable';
import { getFromSession, saveInSession } from '../../utils/sessionStorageUtil';
import isEmpty from 'lodash/isEmpty';
import keyBy from 'lodash/keyBy';
import { isAbEnabled, getMyx } from '../../services/RemoteConfig';
import { getDefaultSellerInfo, setSelectedSeller } from '../Pdp/helper';
import PriceInfo from '../Pdp/PriceInfo';
import { transformSizes } from './helper.js';


const abConfigOnlyIcon = {
  key: 'icon.wishlistCount',
  control: 'disabled',
  test: 'onlyicon'
};
const abConfigAllChange = {
  key: 'icon.wishlistCount',
  control: 'disabled',
  test: 'allchange'
}

let bindClassNames;
const featureGates = isBrowser() ? window.__myx_features__ || {} : {};
const wishlistEnabled = featureGates['wishlist.enable'] === 'true';
const sizeRecommendationEnabled = featureGates['pdp.mfa.recommendation.enable'] === 'true';
const recommendationEnabled = featureGates['pdp.recommendation.enable'] === 'true';

const INCH_TO_CM = 2.54;
const FOOT_WEAR_DEFAULT_SIZE = {
  'Men': 'UK',
  'Boys': 'EURO',
  'Girls': 'EURO',
  'Women': 'UK',
  'Women-FH': 'EURO',
  'Unisex': 'UK'
};
const INCH = 'inch';
const CM = 'cm';
let styles;
const isAndroid = isBrowser() && at(window, '__myx_apps__.android');

const isMyntAppEnabled = keys => (
  typeof MyntApp !== 'undefined' &&
  keys.reduce((acc, key) => acc && typeof MyntApp[key] === 'function', true)
);

const isWebkitEnabled = keys => (
  typeof webkit !== 'undefined' &&
  webkit.messageHandlers &&
  keys.reduce(
    (acc, key) =>
      acc &&
      webkit.messageHandlers[key] &&
      typeof webkit.messageHandlers[key].postMessage === 'function',
    true
  )
);

function isEmptySimilar(related) {
  return (isEmpty(related) || isEmpty(at(related, 'Similar')) || isEmpty(at(related, 'Similar.products')));
}

/*
* Gets default scale based on gender and articleType
*/
function getDefaultScale(gender, articleType) {
  let defaultScale = FOOT_WEAR_DEFAULT_SIZE[gender];

  if (gender === 'Women' && (articleType === 'Heels' || articleType === 'Flats')) {
    defaultScale = FOOT_WEAR_DEFAULT_SIZE['Women-FH'];
  }

  return defaultScale;
}

class SizeChart extends React.Component {
  constructor(props) {
    super(props);

    let data = at(this.props, 'location.dehydratedState');
    const selectedSkuid = parseInt(get(props, 'location.query.selectedSku'), 10) || null;
    const sellerPartnerId = parseInt(get(props, 'location.query.sellerPartnerId'), 10) || null;
    if (typeof window !== 'undefined') {
      data = at(window, '__myx.pdpData');
    }
    if (data) {
      data.selectedSeller = getDefaultSellerInfo(data, Number(sellerPartnerId));
      setSelectedSeller(data, data.selectedSeller, selectedSkuid);
    }
    styles = Object.assign(stylesApp, scaleAndUnitStyles);

    bindClassNames = classNames.bind(styles);

    const measurements = at(data, 'sizes.0.measurements') || [];
    const unit = get(measurements, '0.unit');
    const sizes = at(data, 'sizes') || [];
    const isWishListed = get(props, 'location.query.isWishListed') === 'true';
    this.state = {
      unit,
      scalesVisible: this.getScalesVisible(),
      sizeChartData: sizes,
      isLoggedIn: false,
      data,
      selectedSkuid,
      showSelectSizeError: false,
      sizesAddedToBag: get(props, 'location.query.skuInBag') ? [parseInt(get(props, 'location.query.skuInBag'), 10)] : [],
      wishlistEnabled: isBrowser() ? wishlistEnabled : true,
      checkSizeServiceability: false,
      wishlistAddActionName: isWishListed ? 'WISHLISTED' : 'WISHLIST',
      priorityCheckoutWishlistAddActionName: isWishListed ? 'WISHLISTED' : 'WISHLIST NOW',
      showReco: true,
      recoTextObj: {},
      related: {},
      sizeProfiles: [],
      selectedProfile: '',
      recoLoading: false,
      profilesShown: false,
      isHeartIconEnabled: false
    };

    this.convertDataUnits = this.convertDataUnits.bind(this);
    this.onSelectScale = this.onSelectScale.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.addToBag = this.addToBag.bind(this);
    this.addToWishlist = this.addToWishlist.bind(this);
    this.getSelectedSellerPartnerId = this.getSelectedSellerPartnerId.bind(this);
  }

  componentDidMount() {
    getMyx()
      .then(myx => {
        const isHeartIconEnabled = isAbEnabled(abConfigAllChange) || isAbEnabled(abConfigOnlyIcon)
        if (this.state.isHeartIconEnabled !== isHeartIconEnabled)
          this.setState({ isHeartIconEnabled })
      })
    document.addEventListener('keydown', this.onKeyPress);
    this.loadData();
    const { sizeChartData, unit } = this.state;
    this.setState({
      sizeChartData: transformSizes(sizeChartData, unit)
    })
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyPress);
  }

  onSelectScale = (e) => {
    const target = e.currentTarget.parentElement;
    const scale = target.id;
    const scalesVisible = at(this, 'state.scalesVisible');

    const newScales = [...scalesVisible];
    if (target.firstElementChild.checked) {
      newScales.push(scale);
    } else {
      const index = newScales.indexOf(scale);
      if (index !== -1) {
        newScales.splice(index, 1);
      }
    }

    TrackMadalytics.sendEvent('SizeChartSwitch', 'react', {
      'entity_id': at(window, '__myx.pdpData.id')
    }, { widget: { type: 'size_unit', name: scale } });

    this.setState({
      scalesVisible: newScales
    });
  };
  onKeyPress = event => {
    if (event.key === 'Escape') {
      this.props.hide();
    }
  };
  onLoginClick = () => {
    this.trackLoginEvent('click');
  };
  getScalesVisible = () => {
    const analyticsData = at(window, '__myx.pdpData.analytics');
    if (at(analyticsData, 'masterCategory') !== 'Footwear') {
      return [];
    }

    const gender = at(analyticsData, 'gender');
    const articleType = at(analyticsData, 'articleType');

    return [getDefaultScale(gender, articleType)];
  };
  getSizeRecoPayload = (pdpData, profileId) => {
    const skuIds = (at(pdpData, 'sizes') || []).map(size => size.skuId);
    const payload = {
      uidx: at(window, '__myx_session__.login'),
      availableStyleSkuMap: {
        [pdpData.id]: skuIds
      },
      articleType: at(pdpData, 'analytics.articleType')
    };

    if (profileId) {
      Object.assign(payload, { pidx: profileId });
    }

    return payload;
  };
  /* get Image to be displayed on size chart */
  getSelectedImage() {
    const styleImage = at(this.state, 'data.media.albums.0');
    if (styleImage) {
      const imgConfig = {
        width: 350,
        height: 500,
        q: 20
      };
      return getProductImageUrl(at(styleImage, 'images.0'), imgConfig);
    }
    return null;
  }
  fetchRelated = (recommendedSize) => {
    if (recommendationEnabled) {
      const styleId = at(this.props, 'params.styleId');
      let url = `${config('recommendations')}/${styleId}`;
      url = recommendedSize ? `${url}/${recommendedSize}` : `${url}`;
      Client.get(url)
        .end((err, response) => {
          if (err) {
            return;
          }
          if (at(response, 'body.related.length')) {
            const related = keyBy(at(response, 'body.related'), 'type');
            const similarItems = at(related, 'Similar.products') || [];
            this.setState({
              related,
              similarItemsAvailable: similarItems.length > 0
            });
          }
        });
    }
  };
  fetchSizeProfiles = (data) => {
    const fetchProfilesPath = at(data, 'sizeRecoLazy.sizeProfileAction');
    if (fetchProfilesPath) {
      const currentProfile = getFromSession('profileSelected');
      let selectedProfile;
      Client.get(`${config('sizeReco')}${fetchProfilesPath.slice(1)}`).end((err, response) => {
        if (err) {
          console.log('Size profiles fetch error: ', err);
          this.fetchSizeReco(data);    // fetch fallback recommendation
          return;
        }
        const profiles = at(response, 'body.data.sizeProfile.profileList');
        if (profiles) {
          if (profiles.findIndex(elem => (elem.pidx === currentProfile)) !== -1) {
            selectedProfile = currentProfile;
          } else {
            selectedProfile = at(profiles, '0.pidx') || '';
          }
        } else {
          selectedProfile = '';
        }

        this.setState({
          sizeProfiles: profiles,
          selectedProfile
        });
        this.fetchSizeReco(data, selectedProfile);
      });
    } else {
      this.setState({
        showReco: false
      }, () => {
        this.fetchRelated();
      });
    }
  };
  loadProfiles = () => {
    const data = at(this.state, 'data');
    if (!at(window, '__myx_session__')) {
      if (localStorage.getItem('lscache-beacon:user-data')) {
        this.fetchSizeProfiles(data);
      } else {
        bus.on('beacon-data', () => {
          setTimeout(() => {
            this.fetchSizeProfiles(data);
          }, 0);
        });
      }
    } else {
      this.fetchSizeProfiles(data);
    }
  };
  loadData = () => {
    if (sizeRecommendationEnabled) {
      this.loadProfiles();
    }
  };

  fetchSizeReco = (pdpData, profileId) => {
    const payload = this.getSizeRecoPayload(pdpData, profileId);
    const fetchRecoPath = at(pdpData, 'sizeRecoLazy.action').slice(1);
    Client.post(`${config('sizeReco')}${fetchRecoPath}`, payload)
      .end((err, resp) => {
        if (err) {
          console.log('fetch reco error: ', err);
          this.fetchRelated();
          return;
        }
        const entityDetails = {
          'entity_id': at(pdpData, 'id'),
          'entity_type': 'product',
          'entity_name': at(pdpData, 'name')
        };
        const recoTextObj = at(resp, 'body');

        // Send event
        TrackMadalytics.sendEvent(`RecommendedSize-sizeChart-${isAndroid ? 'android' : 'ios'}`, 'react', entityDetails, {
          custom: {
            v1: `${at(recoTextObj, 'title')} | ${at(recoTextObj, 'description')}`,
            v2: profileId,
            v3: at(recoTextObj, 'level'),
            v4: at(recoTextObj, 'recommendedSkuId')
          }
        });

        this.setState({
          recoTextObj,
          recoLoading: false
        }, () => {
          const recommendedSkuId = at(recoTextObj, 'recommendedSkuId');
          if (recommendedSkuId) {
            // Get Correspoding label
            const sizes = at(pdpData, 'data.sizes') || [];
            const recommedSkuFromSize = sizes.filter(item => item.skuId === recommendedSkuId);
            if (recommedSkuFromSize.length > 0) {
              return this.fetchRelated(at(recommedSkuFromSize[0], 'label'));
            }
            return this.fetchRelated();
          }
          return this.fetchRelated();
        });
      });
  };

  trackLoginEvent = (type) => {
    let eventName;
    let custom = {};
    if (type === 'load') {
      eventName = 'Fit assist - load login page';
      custom = { widget: { name: 'Not sure about what size to buy?' } };
    } else {
      eventName = 'Fit assist - Login';
    }
    const pdpData = at(window, '__myx.pdpData');
    const entityDetails = {
      'entity_id': at(pdpData, 'id'),
      'entity_type': 'product',
      'entity_name': at(pdpData, 'name')
    };
    TrackMadalytics.sendEvent(eventName, 'pdp', entityDetails, custom);
  };

  convertDataUnits = (e) => {
    const target = e.currentTarget;
    const isInch = target.id === INCH;

    TrackMadalytics.sendEvent('SizeChartSwitch', 'react', {
      'entity_id': at(window, '__myx.pdpData.id')
    }, { widget: { type: 'standard', name: isInch ? 'inches' : 'cms' } });

    this.setState({
      unit: target.id,
      sizeChartData: transformSizes(this.state.sizeChartData, target.id)
    });
  };

  goToBag = () => {
    if (isMyntAppEnabled(['openBag'])) {
      MyntApp.openBag();
    }

    if (isWebkitEnabled(['openBag'])) {
      webkit.messageHandlers.openBag.postMessage('openBag');
    }
  };

  gotToWishlist = () => {
    if (isMyntAppEnabled(['openWishlist'])) {
      MyntApp.openWishlist();
    }

    if (isWebkitEnabled(['openWishlist'])) {
      webkit.messageHandlers.openWishlist.postMessage('openWishlist');
    }
  };

  addToBag = () => {
    if (this.state.selectedSkuid) {
      const skuData = (get(this.state, "data.sizes") || []).find(
        size => size.skuId === this.state.selectedSkuid
      );
      const sellerPartnerId = Number(get(skuData, 'selectedSeller.sellerPartnerId')) ||
        Number(get(this.state, 'data.selectedSeller.sellerPartnerId'));
      const parameters = {
        skuid: this.state.selectedSkuid,
        styleid: this.state.data.id,
        xsrf: at(window, '__myx_session__.USER_TOKEN'),
        sellerPartnerId,
        quantity: 1
      };
      const styleOption = this.state.data.sizes.filter((option) => option.skuId === this.state.selectedSkuid)[0];
      const pdpData = at(window, '__myx.pdpData');
      Track.event('pdp', 'add_to_cart_button', styleOption.label);
      TrackMadalytics.sendEvent(`addToCart-sizeChart-${isAndroid ? 'android' : 'ios'}`, 'react', {
        'entity_id': at(pdpData, 'id'),
        'entity_type': 'product',
        'entity_optional_attributes': {
          discountedPrice: at(skuData, 'selectedSeller.discountedPrice'),
          price: at(pdpData, 'mrp'),
          'sku-id': this.state.selectedSkuid
        }
      }, { custom: { v1: this.state.selectedProfile, v2: at(this, 'state.recoTextObj.recommendedSkuId') } });

      const gender = at(this.state, 'data.analytics.gender');
      const masterCategory = at(this.state, 'data.analytics.masterCategory');
      const articleType = at(this.state, 'data.analytics.articleType');

      Track.ecommerce('ec:addProduct', {
        id: at(this.props, 'params.styleId'),
        name: at(this.state, 'data.name') || at(this.state, 'data.brand.name'),
        category: `${gender}/${masterCategory}/${articleType}`,
        brand: at(this.state, 'data.brand.name'),
        variant: this.state.selectedSkuid,
        mrp: at(this.state, 'data.mrp'),
        quantity: 1,
        price: at(skuData, 'selectedSeller.discountedPrice') || at(this.state, 'data.mrp')
      });

      this.setState({
        isLoading: true
      });

      const endPointUrl = config('cart');
      const headers = { 'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN') };

      Client.post(endPointUrl, parameters, headers).end((err, response) => {
        const errMsg = 'Oops! Something went wrong. Please try again in some time.';

        if (Client.errorHandler(err, response)) {
          let message = '';

          if (
            priorityCheckout() &&
            at(response, 'body.meta.statusCode') === CART_FULL_ERROR_CODE
          ) {
            message = PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE;
          }

          message =
            message ||
            at(response, 'body.meta.errorDetail') || errMsg;

          this.refs.notify.error(message);

          this.setState({
            isLoading: false
          });
        } else {
          const skuidSizeAddedToBag = this.state.sizesAddedToBag;
          if (skuidSizeAddedToBag.indexOf(parameters.skuid) === -1) {
            skuidSizeAddedToBag.push(parameters.skuid);
          }
          this.setState({
            sizesAddedToBag: skuidSizeAddedToBag
          });

          this.setState({
            isLoading: false
          });

          const albums = at(this.state, 'data.media.albums.0');
          const styleImage = at(albums, 'images.0');
          const imgConfig = {
            width: 48,
            height: 64,
            q: 100
          };

          const eventPayload = {
            res: response,
            skuid: parameters.skuid,
            productImage: getProductImageUrl(styleImage, imgConfig)
          };


          if (isMyntAppEnabled(['itemAddedToCartV2'])) {
            MyntApp.itemAddedToCartV2(parameters.skuid, parameters.sellerPartnerId);
          } else if (isMyntAppEnabled(['itemAddedToCart'])) {
            MyntApp.itemAddedToCart(parameters.skuid);
          }

          if (isWebkitEnabled(['itemAddedToCartV2'])) {
            webkit.messageHandlers.itemAddedToCartV2.postMessage({
              skuId: parameters.skuid,
              sellerPartnerId: parameters.sellerPartnerId
            });
          } else if (isWebkitEnabled(['itemAddedToCart'])) {
            webkit.messageHandlers.itemAddedToCart.postMessage(parameters.skuid);
          }
          /* if (typeof MyntApp !== 'undefined' && typeof MyntApp.itemAddedToCart === 'function') {
             MyntApp.itemAddedToCart(parameters.skuid);
           }*/
          const showSlotPopup = isShowSlotPopup();
          if (!showSlotPopup) {
            bus.emit('cart.add', eventPayload);
          }

          this.setState({
            showSlotPopup
          });
        }
      });
    } else {
      this.setState({
        showSelectSizeError: true
      });
      Track.event('pdp', 'add_to_cart_button', null);
    }
  };

  selectSize = (e, selectedSkuid, callback, sellerPartnerId) => {
    if (e) {
      //e.stopPropagation();
    }
    if (this.state.selectedSkuid !== selectedSkuid) {
      const styleOptions = at(this.state, 'data.sizes');
      const selectedStyleOption = styleOptions.filter((option) => (option.skuId === selectedSkuid))[0];
      let availableOption = styleOptions.filter((option) => (option.skuId === selectedSkuid && option.available))[0];
      // in price reveal mode && in slotMode check for sellableInventory
      if (inPriceRevealMode() && isSlotEntryEnabled() && !this.state.isPreOrderItem) {
        availableOption = styleOptions.filter((option) => {
          if (at(option, 'selectedSeller.sellableInventoryCount')) {
            return (option.skuId === selectedSkuid) && option.available;
          }
          return false;
        })[0];
      }
      if (availableOption) {
        this.setState({
          selectedSkuid,
          showSelectSizeError: false,
          checkSizeServiceability: true
        });

        if (isMyntAppEnabled(['sizeSelectedV2'])) {
          MyntApp.sizeSelectedV2(selectedSkuid, sellerPartnerId);
        } else if (isMyntAppEnabled(['sizeSelected'])) {
          MyntApp.sizeSelected(selectedSkuid);
        }

        if (isWebkitEnabled(['sizeSelectedV2'])) {
          webkit.messageHandlers.sizeSelectedV2.postMessage({
            skuId: selectedSkuid,
            sellerPartnerId: sellerPartnerId
          });
        } else if (isWebkitEnabled(['sizeSelected'])) {
          webkit.messageHandlers.sizeSelected.postMessage(selectedSkuid);
        }
        /* if (typeof MyntApp !== 'undefined' && typeof MyntApp.sizeSelected === 'function') {
           MyntApp.sizeSelected(selectedSkuid);
         }*/
        Track.event('pdp', 'size_select', availableOption.label);
      } else {
        const pdpData = at(window, '__myx.pdpData');
        const entityDetails = {
          'entity_id': at(pdpData, 'id'),
          'entity_type': 'product',
          'entity_name': at(pdpData, 'name'),
          'entity_optional_attributes': {
            price: at(pdpData, 'mrp'),
            discount: at(selectedStyleOption, 'selectedSeller.discount'),
            discountedPrice: at(selectedStyleOption, 'selectedSeller.discountedPrice'),
            article_type: at(pdpData, 'analytics.articleType'),
            gender: at(pdpData, 'analytics.gender'),
            master_category: at(pdpData, 'analytics.masterCategory'),
            sub_category: at(pdpData, 'analytics.subCategory')
          }
        };
        TrackMadalytics.sendEvent('broken size clicked', 'pdp', entityDetails);
      }
    } else {
      this.setState({ checkSizeServiceability: false });
    }
  };

  addToWishlist = (type) => {
    if (type === 'wishlist' && this.state.wishlistAddActionName === 'WISHLISTED') {
      return;
    }
    const sellerPartnerId = get(
      (get(this.state, "data.sizes") || []).find(
        size => size.skuId === this.state.selectedSkuid
      ),
      "selectedSeller.sellerPartnerId"
    );
    if (this.state.selectedSkuid || type === 'wishlist') {
      if (at(window, '__myx_session__.isLoggedIn') || type === 'cart') {
        let parameters = {};
        let headers = {};
        let endPointUrl = config(type);
        if (type === 'wishlist') {
          endPointUrl = '/web/wishlistapi/addition';
          parameters = {
            styleId: at(this.state, 'data.id')
          };
          headers = { 'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN') };
          if (this.state.selectedSkuid) {
            parameters.skuId = this.state.selectedSkuid;
            parameters.sellerPartnerId = sellerPartnerId;
          }
          Track.event('wishlist', 'add_to_wishlist');
        } else if (type === 'cart') {
          parameters = {
            skuid: this.state.selectedSkuid,
            styleid: this.state.data.id,
            xsrf: at(window, '__myx_session__.USER_TOKEN')
          };
          const styleOption = this.state.data.sizes.filter((option) => option.skuId === this.state.selectedSkuid)[0];
          const pdpData = at(window, '__myx.pdpData');
          Track.event('pdp', 'add_to_cart_button', styleOption.label);
          if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
            window.Madalytics.send('addToCart', {
              type: 'Checkout Cart',
              url: window.location.href,
              variant: 'web',
              name: `Shopping Page-PDP${window.location.pathname}`,
              'data_set': {
                'entity_id': at(pdpData, 'id'),
                'entity_type': 'product',
                'entity_optional_attributes': {
                  discountedPrice: at(styleOption, 'selectedSeller.discountedPrice'),
                  price: at(pdpData, 'mrp'),
                  'sku-id': this.state.selectedSkuid
                }
              }
            });
          }

          const gender = at(this.state, 'data.analytics.gender');
          const masterCategory = at(this.state, 'data.analytics.masterCategory');
          const articleType = at(this.state, 'data.analytics.articleType');

          Track.ecommerce('ec:addProduct', {
            id: at(this.props, 'params.styleId'),
            name: at(this.state, 'data.name') || at(this.state, 'data.brand.name'),
            category: `${gender}/${masterCategory}/${articleType}`,
            brand: at(this.state, 'data.brand.name'),
            variant: this.state.selectedSkuid,
            mrp: at(this.state, 'data.mrp'),
            quantity: 1,
            price: at(styleOption, 'selectedSeller.discountedPrice') || at(this.state, 'data.mrp')
          });
        }
        this.setState({
          isLoading: true
        });
        Client.post(endPointUrl, parameters, headers).end((err, response) => {
          let errMsg = 'Oops! Something went wrong. Please try again in some time.';
          if (err) {
            this.setState({
              isLoading: false
            });

            if (at(response, 'body.status.statusCode') === '403' && type === 'wishlist') {
              window.location.href = `/login?referer=${window.location.toString()}`;
              return;
            }

            this.refs.notify.error(errMsg);
            return;
          }

          if (response && at(response, 'body.error')) {
            this.setState({
              isLoading: false
            });
            try {
              errMsg = at(JSON.parse(at(response, 'body.error.response.text')), 'status.statusMessage');
              const errCode = at(JSON.parse(at(response, 'body.error.response.text')), 'status.statusCode');
              if (priorityCheckout() && errCode === CART_FULL_ERROR_CODE) {
                errMsg = PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE;
              }
            } catch (e) {
              console.log(e);
            }
            this.refs.notify.error(errMsg);
            return;
          }
          if (!err && !at(response, 'body.error') && type === 'cart') {
            const skuidSizeAddedToBag = this.state.sizesAddedToBag;
            if (skuidSizeAddedToBag.indexOf(parameters.skuid) === -1) {
              skuidSizeAddedToBag.push(parameters.skuid);
            }
            this.setState({
              sizesAddedToBag: skuidSizeAddedToBag
            });
          }
          this.setState({
            isLoading: false
          });

          const albums = at(this.state, 'data.media.albums.0');
          const styleImage = at(albums, 'images.0');
          const imgConfig = {
            width: 48,
            height: 64,
            q: 100
          };

          const eventPayload = {
            res: response,
            skuid: parameters.skuid,
            productImage: getProductImageUrl(styleImage, imgConfig)
          };

          if (type === 'wishlist') {
            this.handleWishlistResponse(response, parameters.skuId ? parameters.skuId : this.state.data.id, sellerPartnerId);
          }

          const showSlotPopup = isShowSlotPopup() && type === 'wishlist';
          if (!showSlotPopup) {
            bus.emit(`${type}.add`, eventPayload);
          }

          this.setState({
            showSlotPopup
          });
        });
      } else {
        window.location.href = `/login?referer=${window.location.toString()}`;
      }
    } else {
      this.setState({
        showSelectSizeError: true
      });
      Track.event('pdp', 'add_to_cart_button', null);
    }
  };

  _sendWishlistAnalytics = () => {
    TrackMadalytics.sendEvent(`AddToCollection-sizeChart-${isAndroid ? 'android' : 'ios'}`, 'PDP', {
      'entity_name': at(this.state, 'data.name') || at(this.state, 'data.brand.name'),
      'entity_type': 'PDP',
      'entity_id': at(this.props, 'params.styleId')
    }, {
      'custom': {
        'v1': 'wishlist'
      }
    });
  };

  handleWishlistResponse = (response, skuId, sellerPartnerId) => {
    if (at(response, 'body.status.statusCode') === '403') {
      window.location.href = `/login?referer=${window.location.toString()}`;
      return;
    }
    if (at(response, 'body.status.response.meta.error') === 'MAX_LIMIT_REACHED') {
      this.refs.notify.error('You have reached the item limit of your Wishlist. Remove few to add more');
      return;
    }
    if (at(response, 'body.meta.code') !== 200 || at(response, 'body.status.statusType') === 'ERROR') {
      if (at(response, 'body.status') !== 200) {
        this.refs.notify.error('Oops! Something went wrong. Please try again in some time.');
        return;
      }
    }

    addProductToWlSummaryStore(at(this.state, 'data.id'));
    this._sendWishlistAnalytics();

    if (isMyntAppEnabled(['itemWishlistedV2'])) {
      MyntApp.itemWishlistedV2(skuId, sellerPartnerId);
    } else if (isMyntAppEnabled(['itemWishlisted'])) {
      MyntApp.itemWishlisted(skuId);
    }

    if (isWebkitEnabled(['itemWishlistedV2'])) {
      webkit.messageHandlers.itemWishlistedV2.postMessage({
        skuId: skuId,
        sellerPartnerId: sellerPartnerId
      });
    } else if (isWebkitEnabled(['itemWishlisted'])) {
      webkit.messageHandlers.itemWishlisted.postMessage(skuId);
    }
    /*
    if (typeof MyntApp !== 'undefined' && typeof MyntApp.itemWishlisted === 'function') {
      MyntApp.itemWishlisted(skuId);
    }*/
    this.setState({ wishlistAddActionName: 'WISHLISTED', priorityCheckoutWishlistAddActionName: 'WISHLISTED' });
  };
  selectProfile = (e) => {
    const id = e.currentTarget.id;

    const skuData = (get(this.state, "data.sizes") || []).find(
      size => size.skuId === this.state.selectedSkuid
    );
    const selectedSeller = get(skuData, 'selectedSeller') || get(this.state, 'data.selectedSeller');

    if (id === 'nil') {
      this.setState({
        showReco: false,
        profilesShown: false
      });
    } else {
      const pdpData = at(window, '__myx.pdpData');
      const entityDetails = {
        'entity_id': at(pdpData, 'id'),
        'entity_type': 'product',
        'entity_name': at(pdpData, 'name'),
        'entity_optional_attributes': { price: at(pdpData, 'mrp'), discountedPrice: at(selectedSeller, 'discountedPrice') }
      };
      // Send Event
      TrackMadalytics.sendEvent(`SizeProfileSelect-sizeChart-${isAndroid ? 'android' : 'ios'}`, 'react', entityDetails, { custom: { v1: id } });

      this.setState({
        recoLoading: true,
        profilesShown: false,
        selectedProfile: id
      });

      saveInSession('profileSelected', id);

      this.fetchSizeReco(at(window, '__myx.pdpData'), id);
    }
  };
  showProfiles = () => {
    if (at(window, '__myx_deviceData__.isMobile') || (at(window, '__myx_deviceType__')) === 'mobile') {
      this.setState({
        profilesShown: true
      });
    }
  };

  getSelectedSellerPartnerId() {
    const data = this.state.data || {};
    return get(
      find(get(data, "sizes"), ({ skuId }) => skuId === this.state.selectedSkuid),
      "selectedSeller.sellerPartnerId"
    );
  }

  render() {
    const { selectedSkuid, data } = this.state;
    const sizes = get(this, 'state.sizeChartData');
    const selectedSkuData = find(
      get(data, "sizes"),
      ({ skuId }) => skuId === selectedSkuid
    );
    const selectedSellerPartnerId = this.getSelectedSellerPartnerId();
    const selectedSellerData =
      (selectedSkuid
        ? find(
            get(selectedSkuData, "sizeSellerData"),
            seller => seller.sellerPartnerId === selectedSellerPartnerId
          )
        : get(data, "selectedSeller", {}));
    const discountData =
      (selectedSkuid
        ? find(
            get(data, "discounts"),
            discount =>
              discount.discountId === get(selectedSellerData, "discountId")
          ) : get(selectedSellerData, "discount")) || {};
    const mrp =
      get(selectedSkuData, "price") ||
      get(data, "mrp") ||
      get(preloadData, "mrp");
    const discountedPrice = get(selectedSellerData, "discountedPrice");

    return (
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.header}>
            <img className={styles['sizechart-image']} src={this.getSelectedImage()} />
            <div className={styles.productInfo}>
              <div className={styles.title}>
                {at(this.state, 'data.brand.name')}
              </div>
              <div className={styles.subtitle}>
                {at(this.state, 'data.name')}
              </div>
              <PriceInfo
                discountData={discountData}
                mrp={mrp}
                discountedPrice={discountedPrice} />
            </div>
          </div>
          <SizeTable
            styles={styles}
            sizes={sizes}
            bindClassNames={bindClassNames}
            unit={at(this, 'state.unit')}
            scalesVisible={at(this, 'state.scalesVisible')}
            selectSize={this.selectSize}
            selectedSkuid={this.state.selectedSkuid}
            convertDataUnits={this.convertDataUnits}
            onSelectScale={at(this, 'onSelectScale')}
            showReco={this.state.showReco}
            recoTextObj={this.state.recoTextObj}
            recoLoading={this.state.recoLoading}
            sizeProfiles={this.state.sizeProfiles}
            selectProfile={this.selectProfile}
            selectedProfile={this.state.selectedProfile}
            relatedProducts={!isEmptySimilar(this.state.related)}
            profilesShown={this.state.profilesShown}
            showSimilarProducts={this.showSimilarProducts}
            showProfiles={this.showProfiles}
            onLoginClick={this.onLoginClick}
            isApp={true}
            isSizeChart={true}
            serviceabilityCallDone={this.props.serviceabilityCallDone}
            trackLoginEvent={this.trackLoginEvent} />
          <ActionCTA
            stylesCTA={styles}
            isHeartIconEnabled={this.state.isHeartIconEnabled}
            isSizeChart={true}
            isMobile={true}
            state={this.state}
            systemAttributes={at(window, '__myx.pdpData.systemAttributes')}
            addToBag={this.addToBag}
            goToBag={this.goToBag}
            gotToWishlist={this.gotToWishlist}
            addToWishlist={this.addToWishlist} />
        </div>
        <Loader show={this.state.isLoading} />
      </div>
    );
  }
}

SizeChart.propTypes = {
  serviceabilityCallDone: React.PropTypes.bool,
  showSizeChart: React.PropTypes.bool,
  hide: React.PropTypes.func
};

export default SizeChart;
