import React from 'react';
import at from 'v-at';
import Madalytics from '../../utils/trackMadalytics';
import stylesWeb from './sizeChartWeb.css';
import stylesApp from './sizeChartApp.css';
import commonStyles from '../../resources/common.css';
import scaleAndUnitStyles from './scaleAndUnits.css';
import classNames from 'classnames/bind';
import cloneDeep from 'lodash/cloneDeep';
import { securify } from '../../utils/securify';
const secure = securify();
let bindClassNames;

const EMPTY_ARR_READ_ONLY = [];
const INCH_TO_CM = 2.54;
const FOOT_WEAR_SCALES = ['UK', 'US', 'EURO'];
const FOOT_WEAR_DEFAULT_SIZE = {
  'Men': 'UK',
  'Boys': 'EURO',
  'Girls': 'EURO',
  'Women': 'UK',
  'Women-FH': 'EURO',
  'Unisex': 'UK'
};
const INCH = 'inch';
const CM = 'cm';
let styles;

/*
* Check for size range and change the value
*/

function checkForSizeRange(size, operation, factor) {
  if (size.minValue && (size.minValue !== size.maxValue)) {
    switch (operation) {
      case '*' :
        return `${(size.minValue * factor).toFixed(1)} - ${(size.maxValue * factor).toFixed(1)}`;
      case '/':
        return `${(size.minValue * factor).toFixed(1)} - ${(size.maxValue * factor).toFixed(1)}`;
      default:
        return `${Number(size.minValue).toFixed(1)} - ${Number(size.maxValue).toFixed(1)}`;
    }
  }
  switch (operation) {
    case '*' :
      return (Number(size.value) * factor).toFixed(1);
    case '/':
      return (Number(size.value) / factor).toFixed(1);
    default:
      return Number(size.value).toFixed(1);
  }
}

/*
* Transform size data based on operation
*/
function transformSizes(sizes, operation, factor) {
  sizes.forEach((size) => {
    if (size.measurements) {
      size.measurements.forEach((m) => {
        if (m.unit === 'cm' || m.unit === 'Inches') {
          m.value = checkForSizeRange(m, operation, factor);
        }
      });
    }
  });
  return sizes;
}

/*
* Gets default scale based on gender and articleType
*/
function getDefaultScale(gender, articleType) {
  let defaultScale = FOOT_WEAR_DEFAULT_SIZE[gender];

  if (gender === 'Women' && (articleType === 'Heels' || articleType === 'Flats')) {
    defaultScale = FOOT_WEAR_DEFAULT_SIZE['Women-FH'];
  }

  return defaultScale;
}

class SizeChart extends React.Component {
  constructor(props) {
    super(props);

    let stateVal = at(this.props, 'location.dehydratedState');
    if (typeof window !== 'undefined') {
      stateVal = at(window, '__myx.pdpData');
    }

    // If deviceChannel is mobile_app, Size Chart is rendered in App mode, else in Web Mode. Styles are applied accordingly
    if (at(window, '__myx_deviceData__.deviceChannel') === 'mobile_app') {
      this.appMode = true;
      styles = Object.assign(stylesApp, scaleAndUnitStyles);
    } else {
      styles = Object.assign(stylesWeb, scaleAndUnitStyles);
    }

    bindClassNames = classNames.bind(styles);

    const measurements = at(stateVal, 'sizes.0.measurements') || [];
    const units = measurements.map(m => m.unit);
    const unit = units.indexOf(CM) !== -1 ? CM : INCH;
    this[unit === INCH ? 'sizeDataInches' : 'sizeDataCm'] = at(stateVal, 'sizes');

    this.state = { unit, scalesVisible: this.getScalesVisible(), sizeChartData: at(stateVal, 'sizes') || [] };

    this.convertDataUnits = this.convertDataUnits.bind(this);
    this.onSelectScale = this.onSelectScale.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  componentDidMount() {
    document.addEventListener('keydown', this.onKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyPress);
  }

  onSelectScale(e) {
    const target = e.currentTarget.parentElement;
    const scale = target.id;
    const scalesVisible = at(this, 'state.scalesVisible');

    const newScales = [...scalesVisible];
    if (target.firstElementChild.checked) {
      newScales.push(scale);
    } else {
      const index = newScales.indexOf(scale);
      if (index !== -1) {
        newScales.splice(index, 1);
      }
    }

    Madalytics.sendEvent('SizeChartSwitch', 'react', {
      'entity_id': at(window, '__myx.pdpData.id')
    }, { widget: { type: 'size_unit', name: scale } });

    this.setState({
      scalesVisible: newScales
    });
  }

  onKeyPress = event => {
    if (event.key === 'Escape') {
      this.props.hide();
    }
  }

  getAllMeasurements() {
    let maxDim = 0;
    let measurements = [];
    at(this, 'state.sizeChartData').forEach((size) => {
      if (size.measurements && size.measurements.length > maxDim) {
        maxDim = size.measurements.length;
        measurements = size.measurements;
      }
    });

    return measurements;
  }

  getMeasurement(measurements, name) {
    return measurements.find((m) => (m.name === name));
  }

  getSizeHeader() {
    const sizes = at(this, 'state.sizeChartData.0.allSizesList') || EMPTY_ARR_READ_ONLY;
    const scalesVisible = at(this, 'state.scalesVisible');
    const measurements = this.getAllMeasurements();
    const currentUnit = at(this, 'state.unit') === INCH ? 'in' : 'cm';

    return (
      <thead>
        <tr>
          {
            sizes.map((size) => (
              this.showSize(scalesVisible, size) && <th className={styles.cell}>
                {(at(size, 'prefix') || at(size, 'size') || 'Size')}
              </th>
            ))
          }
          {
            measurements.map((m) => {
              const unit = (m.unit === 'Inches' || m.unit === 'cm') ? currentUnit : m.unit;
              return <th className={styles.cell}>{`${m.name} (${unit})`}</th>;
            })
          }
        </tr>
      </thead>
    );
  }

  getSizeInfo() {
    const sizes = at(this, 'state.sizeChartData');
    const scalesVisible = at(this, 'state.scalesVisible');
    const allMeasurements = this.getAllMeasurements();

    return (
      <tbody className={styles['table-body']}>
        {
          sizes.map((size) => {
            const measurements = size.measurements || [];
            return (<tr className={styles.row}>
              {
                size.allSizesList.map((s) => (
                  this.showSize(scalesVisible, s) &&
                    <td className={styles.cell}>{s.sizeValue}</td>
                ))
              }
              {
                allMeasurements.map((m) => {
                  const mm = this.getMeasurement(measurements, m.name);
                  return (<td className={styles.cell}>
                    {mm ? Number(mm.value).toFixed(1) : '-'}
                  </td>);
                })
              }
            </tr>);
          })
        }
      </tbody>
    );
  }

  getSizeChartImage() {
    return (
      <div className={styles['image-size']}>
        <img
          className={styles['image-size-chart']}
          src={`${secure(at(window, '__myx.pdpData.sizechart.sizeChartUrl') || at(window, '__myx.pdpData.sizechart.sizeRepresentationUrl'))}`} />
      </div>
    );
  }

  getSizeChartTable() {
    const sizes = at(this, 'state.sizeChartData');

    if (sizes.length) {
      return (
        <div className={styles.tableContainer}>
          <table className={styles.table}>
            {this.getSizeHeader()}
            {this.getSizeInfo()}
          </table>
        </div>
      );
    }
    return null;
  }

  getInchesCmToggle() {
    const that = this;
    const inchClass = bindClassNames('metric', 'left', { 'selected': at(that, 'state.unit') === INCH });
    const cmClass = bindClassNames('metric', 'right', { 'selected': at(that, 'state.unit') === CM });

    return (
      <div className={styles.inchCmToggle}>
        <span id={INCH} className={inchClass} onClick={this.convertDataUnits}>in</span>
        <span id={CM} className={cmClass} onClick={this.convertDataUnits}>cm</span>
      </div>
    );
  }

  getScaleSelector() {
    const sizeList = at(this, 'state.sizeChartData.0.allSizesList') || EMPTY_ARR_READ_ONLY;
    const gender = at(window, '__myx.pdpData.analytics.gender');
    const articleType = at(window, '__myx.pdpData.analytics.articleType');
    return (
      <div className={styles.scaleSelector}>
        <span className={styles.scaleHeader}>Show size in</span>
        {
          sizeList.map((size) => (
            (FOOT_WEAR_SCALES.indexOf(at(size, 'prefix')) !== -1 && at(size, 'prefix') !== getDefaultScale(gender, articleType)) &&
              <div className={styles.scale}>
                <label id={size.prefix} className={bindClassNames('scaleCheckbox', commonStyles.customCheckbox)} >
                  <input
                    type="checkbox"
                    defaultChecked={at(this, 'state.scalesVisible').indexOf(at(size, 'prefix')) !== -1}
                    onClick={this.onSelectScale} />
                  <div className={commonStyles.checkboxIndicator}></div>
                </label>
                <span className={styles.scaleText}>{at(size, 'prefix')}</span>
              </div>
          ))
        }
      </div>
    );
  }

  getScalesVisible() {
    const analyticsData = at(window, '__myx.pdpData.analytics');
    if (at(analyticsData, 'masterCategory') !== 'Footwear') {
      return [];
    }

    const gender = at(analyticsData, 'gender');
    const articleType = at(analyticsData, 'articleType');

    return [getDefaultScale(gender, articleType)];
  }

  showSize(scalesVisible, size) {
    const sizePrefix = at(size, 'prefix');
    let toShowSize;

    if (FOOT_WEAR_SCALES.indexOf(sizePrefix) !== -1) {
      toShowSize = (scalesVisible.length === 0 || scalesVisible.indexOf(sizePrefix) !== -1);
    } else {
      toShowSize = true;
    }

    return toShowSize;
  }

  convertDataUnits(e) {
    const target = e.currentTarget;
    const isInch = target.id === INCH;

    if (!isInch && !this.sizeDataCm) {
      this.sizeDataCm = cloneDeep(at(this, 'sizeDataInches'));
      transformSizes(this.sizeDataCm, '*', INCH_TO_CM);
    } else if (!this.sizeDataInches) {
      this.sizeDataInches = cloneDeep(at(this, 'sizeDataCm'));
      transformSizes(this.sizeDataInches, '/', INCH_TO_CM);
    }

    Madalytics.sendEvent('SizeChartSwitch', 'react', {
      'entity_id': at(window, '__myx.pdpData.id')
    }, { widget: { type: 'standard', name: isInch ? 'inches' : 'cms' } });

    this.setState({
      unit: isInch ? INCH : CM,
      sizeChartData: isInch ? this.sizeDataInches : this.sizeDataCm
    });
  }

  renderContent() {
    if (this.state.sizeChartData) {
      const category = at(window, '__myx.pdpData.analytics.masterCategory');
      let disclaimerText = at(window, '__myx.pdpData.sizeChartDisclaimerText');

      if (disclaimerText) {
        const disclaimerTextUnit = at(this, 'state.unit') === INCH ? ' Inches' : ' Cms';
        disclaimerText += disclaimerTextUnit;
      }

      return (
        <div className={styles['info-container']}>
          <div>
            {category === 'Footwear' && <div>{this.getScaleSelector()}</div>}
            <div className={styles.inchCmContainer}>{this.getInchesCmToggle()}</div>
          </div>
          <div className={styles.info}>
            {this.getSizeChartTable()}
          </div>
          {disclaimerText && <div className={styles.unitText}>{`* ${disclaimerText}`}</div>}
          {this.getSizeChartImage()}
        </div>
      );
    }
    return null;
  }

  render() {
    if (this.appMode || this.props.showSizeChart) {
      return (
        <div className={styles.container}>
          <div className={styles.content}>
            {
              !this.appMode ? <div className={styles.header}>
                <p className={styles.title}>
                  {at(this.props, 'data.name') || at(this.props, 'data.brand.name')}
                </p>
                <p className={styles.subtitle}>Size Options</p>
                <button className={styles.close} onClick={this.props.hide}>
                  <span className={`myntraweb-sprite ${styles.modalclose}`}></span>
                </button>
              </div> : null
            }
            {this.renderContent()}
          </div>
        </div>
      );
    } return null;
  }
}

SizeChart.propTypes = {
  showSizeChart: React.PropTypes.bool,
  hide: React.PropTypes.func
};

export default SizeChart;
