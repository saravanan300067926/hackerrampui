import React from 'react';
import styles from './image-grid.css';
import at from 'v-at';
import { getProductImageUrl } from '../utils';
import commonStyles from '../../../resources/common.css';
import ImageZoom from '../ImageZoom';
import Track from '../../../utils/track';
import GridItem from './GridItem';
import SimilarIcon from './SimilarIcon';
import BrightcoveVideo from '../BrightcoveVideo';
import XceleratorTag from '../../../common/xceleratorTag';

const singleImageConfig = {
  width: 852,
  height: 1136,
  q: 90
};


class ImageGrid extends React.Component {
  constructor(props) {
    super(props);

    const images = at(props, 'data.media.albums.0.images') || [];

    this.state = {
      images,
      loadImageZoom: false,
      playVideo: false,
      selectedImage: 0

    };
    this.toggleImageZoom = this.toggleImageZoom.bind(this);
    this.setSelectedImage = this.setSelectedImage.bind(this);
    this.playTheVideo = this.playTheVideo.bind(this);
    this.onShowSimilarClick = this.onShowSimilarClick.bind(this);
  }

  onShowSimilarClick(e) {
    e.stopPropagation();
    Track.event('pdp', 'similar_products_icon_click', `productId | ${at(window, '__myx.pdpData.id')}`);
    this.props.showSimilarProducts();
  }

  setSelectedImage(index) {
    this.setState({
      selectedImage: index
    });
  }

  playTheVideo() {
    this.setState({
      playVideo: true
    });
  }

  toggleImageZoom(index) {
    this.setState({
      loadImageZoom: !this.state.loadImageZoom,
      selectedImage: index
    }, () => {
      this.props.onZoom(this.state.loadImageZoom);
    });

    Track.event('pdp', 'image', `zoom | ${at(window, '__myx.pdpData.id')}`);
  }

  render() {
    let GridItems;
    const images = this.state.images;
    const brightData = at(this.props, 'data.media.videos.0');
    const brightConditions = at(brightData, 'host') === 'Brightcove' && at(brightData, 'view') === 'rampwalk';
    const inlineStyle = brightConditions ? { float: 'left', marginRight: '1%' } : undefined;
    const { imageTag: { data: { label: imgTag } = {} } = {} } = at(this.props, 'xceleratorTag');
    if (images.length === 1) {
      const src = getProductImageUrl(images[0], singleImageConfig);
      GridItems = [(
        <div key="p-image-1" className={styles.singleImageContainer}>
          <div onClick={() => this.toggleImageZoom(0)} className={styles.col50} style={inlineStyle} key="child-image-1">
            <div className={styles.imageContainer}>
              <div className={styles.image} style={{ backgroundImage: `url(${src})` }} />
              {imgTag && <XceleratorTag page="pdp" tag={imgTag} position="onImage" />}
              <SimilarIcon
                onShowSimilarClick={this.onShowSimilarClick}
                similarItemsAvailable={this.props.similarItemsAvailable} />
              <div className={styles.skeletonLoader} />
            </div>
          </div>
        </div>
      )];
    } else {
      GridItems = images.map((item, index) => (
        <GridItem
          imageIndex={index}
          onShowSimilarClick={this.onShowSimilarClick}
          playVideo={this.state.playVideo}
          playTheVideo={this.playTheVideo}
          xceleratorTag={imgTag}
          similarItemsAvailable={this.props.similarItemsAvailable}
          toggleZoom={this.toggleImageZoom}
          data={item} key={`p-image-${index}`} />
      ));
    }

    // Brightcove video integration only if there are images in current item
    if (images.length) {
      const brightcovePosition = images.length === 2 ? 1 : 2;
      if (brightData && brightConditions) {
        GridItems.splice(
          brightcovePosition,
          0,
          <BrightcoveVideo
            brightcovePosition={brightcovePosition}
            key={GridItems.length + 1}
            onZoom={this.props.onZoom}
            videoId={`ref:${brightData.id}`} />
        );
      }
    }

    return (
      <div className={`${styles.container} ${commonStyles.clearfix}`}>
        {GridItems}
        {this.state.loadImageZoom ? <ImageZoom
          zoom={this.state.loadImageZoom}
          hide={this.toggleImageZoom}
          data={this.props.data}
          setSelectedImage={this.setSelectedImage}
          selectedImage={this.state.selectedImage} /> : null}
      </div>
    );
  }
}

ImageGrid.propTypes = {
  data: React.PropTypes.object.isRequired,
  onZoom: React.PropTypes.func.isRequired,
  showSimilarProducts: React.PropTypes.func.isRequired,
  similarItemsAvailable: React.PropTypes.bool.isRequired,
  xceleratorTag: React.PropTypes.object
};

export default ImageGrid;
