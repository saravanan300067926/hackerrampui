import React from 'react';
import styles from './image-grid.css';
import { getProductImageUrl } from '../utils';
import LazyLoad from 'react-lazyload';
import SimilarIcon from './SimilarIcon';
import XceleratorTag from '../../../common/xceleratorTag';

const imgConfig = {
  width: 540,
  height: 720,
  q: 90
};

const GridItem = (props) => {
  const item = props.data;
  const { xceleratorTag = '', imageIndex } = props;
  const config = {};
  if (item.videoId) {
    config.onClick = () => {
      props.playTheVideo();
    };
  } else {
    config.onClick = () => {
      props.toggleZoom(props.imageIndex);
    };
  }

  const src = getProductImageUrl(item, imgConfig);
  
  let imageHtml = (
    <div className={styles.imageContainer}>
      <div className={styles.image} style={{ backgroundImage: `url(${src})` }} />
      {imageIndex === 0 && xceleratorTag !== '' && <XceleratorTag page="pdp" tag={xceleratorTag} position="onImage" />}
      {props.imageIndex === 1 && (
        <SimilarIcon
          similarItemsAvailable={props.similarItemsAvailable}
          onShowSimilarClick={props.onShowSimilarClick} />
      )}
      <div className={styles.skeletonLoader} />
    </div>
  );

  return (
    <div {...config} className={styles.col50}>
      {props.imageIndex < 3 ? imageHtml : (
        <LazyLoad height={500} offset={100} once>
          {imageHtml}
        </LazyLoad>
      )}
    </div>
  );
};

GridItem.propTypes = {
  data: React.PropTypes.object.isRequired,
  toggleZoom: React.PropTypes.func.isRequired,
  imageIndex: React.PropTypes.number.isRequired,
  onShowSimilarClick: React.PropTypes.func.isRequired,
  similarItemsAvailable: React.PropTypes.bool.isRequired,
  xceleratorTag: React.PropTypes.element
};

GridItem.defaults = {
  imageIndex: 0
};

export default GridItem;
