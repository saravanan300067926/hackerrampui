import styles from './image-grid.css';
import React from 'react';


const SimilarIcon = (props) => {
  if (props.similarItemsAvailable) {
    return (
      <div onClick={props.onShowSimilarClick} className={`${styles.similarColorsCta} ${props.className}`}>
        <span className={`myntraweb-sprite ${styles.similarColorsIcon}`} />
        <span className={styles.iconText}>VIEW SIMILAR</span>
      </div>
    );
  }
  return null;
};

SimilarIcon.propTypes = {
  onShowSimilarClick: React.PropTypes.func.isRequired,
  similarItemsAvailable: React.PropTypes.bool.isRequired,
  className: React.PropTypes.string
};


export default SimilarIcon;
