import React from 'react';
import at from 'v-at';
import classNames from 'classnames/bind';
import styles from '../SizeButtons/size-buttons.css';
import Loader from '../../Loader';
import getKvPair from '../../../utils/kvpairs';
import { isBrowser } from '../../../utils';

const bindClass = classNames.bind(styles);

const featureGates = isBrowser() ? window.__myx_features__ || {} : {};

const sizeRecommendationEnabled = featureGates['pdp.mfa.recommendation.enable'] === 'true';

const shouldShowMoreProfiles = (recoTextObj, profiles, profile) =>
  at(recoTextObj, 'type') === 'personalized'
    && at(recoTextObj, 'level') === 'profile'
    && profile && profiles.length > 1;

const RecoTextWrapper = ({children, showMoreProfiles}) => (
  <div
    className={bindClass(
      'recoTextContainer',
      { recoTextWithMoreProfiles: showMoreProfiles }
    )}
  >
    <img
      src="https://constant.myntassets.com/pwa/assets/img/size-reco-inch-tape.png"
      alt="size recommendation"
      className={styles.sizeRecoImg}
    />
    {children}
  </div>
);

const getRecoText = (props) => {
  const { recoTextObj, selectedProfile } = props;
  const recoType = at(recoTextObj, 'type');
  const profiles = at(props, 'sizeProfiles') || [];
  const profile = profiles.find((pr) => (at(pr, 'pidx') === selectedProfile));
  const isSizeChart = at(props, 'isSizeChart');
  const isMobile = at(window, '__myx_deviceData__.isMobile');
  const allSizes = at(window, '__myx.pdpData.sizes') || [];
  const outOfStockSizes = allSizes.filter(size => !size.available).map(size => size.skuId);
  const recoSizeOutOfStock = outOfStockSizes.indexOf(recoTextObj.recommendedSkuId) !== -1;
  const isSizeOutOfStock = outOfStockSizes.length > 0;

  let personalisedReco;
  let recoText;
  const viewSimilarEle = (props.relatedProducts && !isMobile) ?
    <span className={styles.viewSimilar} onClick={() => { props.showSimilarProducts(isSizeChart); }}>View Similar</span> : null;
  const viewSimilarComp = recoSizeOutOfStock ?
  (<div className={bindClass({ similarWrapper: isSizeChart && !isMobile, similarWrapperApp: isSizeChart && isMobile })}>
    <span> Size out of stock </span>
      {viewSimilarEle}
  </div>) : null;
  switch (recoType) {
    case 'oos':
      if (!at(window, '__myx_session__.isLoggedIn')) {
        props.trackLoginEvent('load');
        recoText = (<span>
          <span>Not sure about what size to buy?</span>
          <a className={styles.login} href={`/login?referer=${at(window, 'location.href')}`} onClick={props.onLoginClick}>Login</a>
        </span>);
      } else if (props.relatedProducts) {
        recoText = (<span>
          <span>Size out of stock?</span>
          <span className={styles.viewSimilar} onClick={() => { props.showSimilarProducts(isSizeChart); }}> View Similar </span>
        </span>);
      } else if (!isSizeOutOfStock) {
        recoText = (<span>
          <span>Tag your past purchases to find your perfect size.</span>
          <div className={styles.tagInfoIcon}>i</div>
          <div className={styles.tagInfoText}>
            {getKvPair('pdp.tagInfo')}
          </div>
        </span>);
      } else {
        recoText = null;
      }
      break;
    case 'mss':
      recoText = (<span>
        <strong>{recoTextObj.title}</strong>&nbsp;
        {recoTextObj.description}
      </span>);
      break;
    case 'personalized':
      if (recoTextObj.level === 'profile') {
        const profile = profiles.find((pr) => (at(pr, 'pidx') === at(props, 'selectedProfile')));
        personalisedReco = (<span className={bindClass({ personalisedReco: isMobile })}>
          <span>
            We recommend&nbsp;
            <strong>{recoTextObj.label}</strong>&nbsp;
            for&nbsp;
            <strong>{profile.name}</strong>
          </span>
          {viewSimilarComp}
        </span>);
      } else {
        personalisedReco = (<span>
          <span>
            We recommend&nbsp;
            <strong>{recoTextObj.label}</strong>&nbsp;
            {recoTextObj.description}
          </span>
          {viewSimilarComp}
        </span>);
      }
      recoText = recoTextObj.label && personalisedReco;
      break;
    default:
      recoText = null;
  }
  return recoText ? (
    <RecoTextWrapper showMoreProfiles={shouldShowMoreProfiles(recoTextObj, profiles, profile)}>
      {recoText}
    </RecoTextWrapper>
  ) : null;
};

const getRecoTextWithSizeEnabled = (props, recoText) => {
  const recoTextObj = at(props, 'recoTextObj');
  const recoLoading = at(props, 'recoLoading');
  const recommendedSkuId = at(recoTextObj, 'recommendedSkuId');
  const isSizeChart = at(props, 'isSizeChart');
  if (sizeRecommendationEnabled) {
    if (recoLoading) {
      return (<Loader show={true} />);
    }
    return (<div className={bindClass({ shakeText: (recommendedSkuId && at(props, 'selectedSkuid') !== recommendedSkuId) })}>
        {(isSizeChart) ? (
          <div>
            <div className={bindClass('sc-reco-txt')}>
              {recoText}
            </div>
            <div className={bindClass('sharpCorner', { hide: !(isSizeChart) })}></div>
          </div>

        ) : (
            recoText
        )}
    </div>);
  }
  return null;
};

const getSizeProfileList = (props) => {
  const profiles = at(props, 'sizeProfiles');
  const listItems = profiles.map((item) => (
    <li
      className={bindClass('profileItem', { selected: props.selectedProfile === item.pidx })}
      id={item.pidx} onClick={props.selectProfile}>{item.name}</li>
    ));
  let profileList;
  if (at(window, '__myx_deviceData__.isMobile') || (at(window, '__myx_deviceType__')) === 'mobile') {
    profileList = (<div className={bindClass('profilesListMobile')}>{listItems}</div>);
  } else {
    profileList = (
      <ul className={bindClass('profilesListWeb')}>{listItems}</ul>
    );
  }
  return profileList;
};

const getSizeRecommendation = (props) => {
  const profiles = at(props, 'sizeProfiles') || [];
  const profile = profiles.find((pr) => (at(pr, 'pidx') === at(props, 'selectedProfile')));
  const isMobile = at(window, '__myx_deviceData__.isMobile') || (at(window, '__myx_deviceType__')) === 'mobile';
  const profilesShown = at(props, 'profilesShown');
  const recoTextObj = at(props, 'recoTextObj');
  const isSizeChart = at(props, 'isSizeChart');
  const recoLoading = at(props, 'recoLoading');
  const recoText = getRecoText(props);

  return (
    <div
      className={bindClass(
        'recoContainer',
        { recoContainerMobile: isMobile },
        { hide: !recoText },
        { recoWrapper: !isSizeChart && !recoLoading }
      )}>
      <div className={bindClass('recText', { hide: profilesShown })}>
            {getRecoTextWithSizeEnabled(props, recoText)}
      </div>
      {shouldShowMoreProfiles(recoTextObj, profiles, profile) ?
        <div
          className={bindClass(
            {
              moreProfilesMobile: isMobile,
              moreProfilesWeb: !isMobile,
              showProfiles: at(props, 'profilesShown'),
              moreProfileSC: isSizeChart && !isMobile }
            )}>
          <span className={bindClass('profileListHeader', 'pNameHeader', { hide: profilesShown || recoLoading })} onClick={at(props, 'showProfiles')}>
            Not{` ${profile.name}?`}
          </span>
            {getSizeProfileList(props)}
        </div> : null
        }
    </div>
    );
};

export default getSizeRecommendation;
