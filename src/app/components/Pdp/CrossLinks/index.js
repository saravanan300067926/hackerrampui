import React, { PropTypes } from 'react';
import styles from './index.css';
import Track from '../../../utils/track';

const sanitizeUrl = (url) => {
  const query = url.split('=');
  query[1] = query.length > 1 ? encodeURIComponent(query[1]) : query[1];
  return query.join('=');
};

const clickedLink = (index, id) => {
  const sectionsArr = ['MoreCategoryByBrand', 'MoreColorCategory', 'MoreCategory'];
  Track.event('pdp', 'DiscoverMore', sectionsArr[index], id);
};

const CrossLinks = ({ data }) => {
  let links = null;
  if (data.crossLinks && data.crossLinks.length) {
    links = data.crossLinks.map((val, index) => {
      const styleName = index === data.crossLinks.length - 1 ?
        `${styles.links} ${styles.linksLastChild}` : `${styles.links}`;
      return (
        <a
          className={styleName}
          href={`/${sanitizeUrl(val.url)}`}
          onClick={() => clickedLink(index, data.id)}
          key={index}>
          <div>{val.title}<span className={`${styles.arrow}`}></span></div>
        </a>
      );
    });
    return <div className={styles.crossLinkContainer}> {links} </div>;
  } return links;
};

CrossLinks.propTypes = {
  data: PropTypes.object
};

export default CrossLinks;
