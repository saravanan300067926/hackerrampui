import { getItem, setItem } from "../../../utils/localStorageUtil";
import { ulcRegex, Cookie } from "../../../common/LocationContext/data/Cookies";
import cookies from '../../../utils/cookies';

// Add addressId regex when it is implemented
export const addressOnCart = {
  name: "mynt-ulc",

  correctPincodeMismatch() {
    const cookie = Cookie.get(this.name, { pincode: ulcRegex.pincode });

    const cookiePincode = cookie.pincode;
    const localStoragePincode = getItem("pincode");

    if (cookiePincode && localStoragePincode) {
      setItem("pincode", cookiePincode);
    }
  },

  updatePincode(pincode) {
    Cookie.set(this.name, {
      pincode
    });
  },

  clear() {
    cookies.del(this.name);
  }
};
