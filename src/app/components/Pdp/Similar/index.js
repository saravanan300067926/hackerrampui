import React from 'react';
import at from 'v-at';
import ProductList from '../ProductList';
import styles from './similar.css';
import isArray from 'lodash/isArray';

class Similar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: null
    };
  }

  render() {
    const similarProducts = at(this.props, 'related.Similar');
    const products = at(similarProducts, 'products');
    let productsAvailable = false;

    if (isArray(products) && products.length > 0) {
      productsAvailable = true;
    } else if (products === null) {
      productsAvailable = false;
    }

    if (similarProducts && productsAvailable) {
      return (
        <div className={styles.container} ref="simContainer">
          <h3 className={styles.heading}>SIMILAR PRODUCTS</h3>
          <ProductList eventName={"recommendation_click"} products={similarProducts.products} from={at(this.props, 'data.id')} />
        </div>
      );
    }
    return null;
  }
}

Similar.propTypes = {
  data: React.PropTypes.object.isRequired,
  related: React.PropTypes.object.isRequired
};

export default Similar;
