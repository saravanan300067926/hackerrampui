import React from 'react';
import styles from './brightcove-video.css';
import ReactPlayerLoader from '@brightcove/react-player-loader';

const ACCOUNT_ID = 5745608584001;

class BrightcoveVideo extends React.Component {

  constructor(props) {
    super(props);
    this.player = null;
    this.brightcovePlaceholderRef = null;

    this.state = {
      expand: false,
      mute: true,
      play: true,
      // 'forcePause' will be used in IntersectionObserver to check if user has paused the video.
      forcePause: false,
      end: false
    };

    [
      'onKeyPress',
      'onSuccess',
      'handleReady',
      'handleEnd',
      'handlePlay',
      'handlePause',
      'handleReplay',
      'handleMute',
      'handleExpand',
      'handleCloseExpanded',
      'isInViewport'
    ].forEach(method => (this[method] = this[method].bind(this)));
  }

  componentDidMount() {
    window.addEventListener('load', this.isInViewport);
    document.addEventListener('keydown', this.onKeyPress);
  }

  componentWillUnmount() {
    window.removeEventListener('load', this.isInViewport);
    document.removeEventListener('keydown', this.onKeyPress);
  }

  onKeyPress(e) {
    if (e.key === 'Escape') {
      this.handleExpand(false);
    }
  }

  // Gets called when player is successfully loaded
  onSuccess(e) {
    this.player = e.ref;
    this.player.on('ready', this.handleReady);
    this.player.on('play', () => this.setState({ play: true, end: false }));
    this.player.on('pause', this.handlePause);
    this.player.on('ended', this.handleEnd);
  }

  handleReady() {
    // this.handlePlay();
    this.handleMute();
  }

  handleEnd() {
    this.player.play();
    setTimeout(() => {
      this.player.pause();
      this.setState({ end: true });
    }, 100);
  }

  handlePlay(from = null) {
    if (!this.state.play && from !== 'observer') {
      // User Intersection
      this.setState({ play: true, forcePause: false, end: false }, () => {
        this.player.play();
      });
    } else {
      // From IntersectionObserver
      if (!this.state.forcePause) {
        this.setState({ play: true, end: false }, () => {
          this.player.play();
        });
      }
    }
  }

  handlePause(from = null) {
    if (this.state.play && from !== 'observer') {
      // User Intersection
      this.setState({ play: false, forcePause: true }, () => {
        this.player.pause();
      });
    } else {
      // From IntersectionObserver
      if (!this.state.forcePause) {
        this.setState({ play: false }, () => {
          this.player.pause();
        });
      }
    }
  }

  handleReplay() {
    this.setState({ end: false }, () => {
      this.handlePlay();
    });
  }

  handleMute(flag = true) {
    this.setState({ mute: flag }, () => {
      this.player.muted(flag);
    });
  }

  handleExpand(flag) {
    this.setState({ expand: flag }, () => {
      this.props.onZoom(flag);
    });
  }

  isInViewport() {
    if (this.brightcovePlaceholderRef) {
      const observer = new IntersectionObserver(entries => {
        for (const entry of entries) {
          if (entry.isIntersecting) {
            this.handlePlay('observer');
          } else {
            this.handlePause('observer');
          }
        }
      });
      observer.observe(this.brightcovePlaceholderRef);
    }
  }

  handleCloseExpanded() {
    if (this.state.expand) {
      this.handleExpand(false);
    }
  }

  render() {
    const { brightcovePosition } = this.props;
    const { play, mute, expand, end } = this.state;
    const classNameValue = [styles.col50];
    if (expand) {
      classNameValue.push(styles.expandView);
    }
    if (brightcovePosition % 2 !== 0) {
      classNameValue.push(styles.hasMarginLeft);
    }
    return (
      <div className={`${classNameValue.join(' ')}`} onClick={this.handleCloseExpanded} >
        <div className={styles.wrapper} onClick={e => e.stopPropagation()}>
          <ReactPlayerLoader
            debugger={false}
            playerId={'default'}
            accountId={ACCOUNT_ID}
            onSuccess={this.onSuccess}
            videoId={this.props.videoId} />
          <div className={styles.placeholder} ref={(el) => { this.brightcovePlaceholderRef = el; }} />
          {!end && <Mute handleMute={this.handleMute} mute={mute} />}
          {(!end || expand) ? <Expand handleExpand={this.handleExpand} expand={expand} /> : null}
          {expand && <CloseExpanded handleCloseExpanded={this.handleCloseExpanded} />}
          {!play && <Play handlePlay={this.handlePlay} />}
          {end ? <Replay handleReplay={this.handleReplay} /> : null}
        </div>
      </div>
    );
  }
}
BrightcoveVideo.propTypes = {
  brightcovePosition: React.PropTypes.number,
  onZoom: React.PropTypes.func.isRequired,
  videoId: React.PropTypes.string.isRequired
};


function Play({ handlePlay }) {
  return (
    <div className={`myntraweb-sprite ${styles.play}`} onClick={handlePlay} />
  );
}
Play.propTypes = {
  handlePlay: React.PropTypes.func.isRequired
};


function Replay({ handleReplay }) {
  return (
    <div className={`myntraweb-sprite ${styles.replay}`} onClick={handleReplay} />
  );
}
Replay.propTypes = {
  handleReplay: React.PropTypes.func.isRequired
};


function Mute({ handleMute, mute }) {
  return (
    <div className={`myntraweb-sprite ${mute ? styles.mute : styles.unmute}`} onClick={() => handleMute(!mute)} />
  );
}
Mute.propTypes = {
  mute: React.PropTypes.bool,
  handleMute: React.PropTypes.func.isRequired
};


function Expand({ handleExpand, expand }) {
  return (
    <div className={`myntraweb-sprite ${expand ? styles.shrink : styles.expand}`} onClick={() => handleExpand(!expand)} />
  );
}
Expand.propTypes = {
  expand: React.PropTypes.bool,
  handleExpand: React.PropTypes.func.isRequired
};

function CloseExpanded({ handleCloseExpanded }) {
  return (
    <button className={styles.closeExpanded} onClick={handleCloseExpanded}>
      <span className={`myntraweb-sprite ${styles.closeIcon}`}></span>
    </button>
  );
}
CloseExpanded.propTypes = {
  handleCloseExpanded: React.PropTypes.func.isRequired
};

export default BrightcoveVideo;
