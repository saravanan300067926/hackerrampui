import React from 'react';
import at from 'v-at';

import styles from './disclaimer.css';

function Disclaimer(props) {
  // disclaimer text
  const disclaimer = at(props, 'data.disclaimerTitle');
  if (disclaimer) {
    return (
      <p className={styles.container}>{disclaimer}</p>
    );
  }
  return null;
}

Disclaimer.propTypes = {
  data: React.PropTypes.object.isRequired
};

export default Disclaimer;
