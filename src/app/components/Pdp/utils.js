import at from 'v-at';
import get from 'lodash/get';
import filter from 'lodash/filter';
import { inPriceRevealMode, isSlotEntryEnabled } from '../../utils/slotUtils';
import { securify } from '../../utils/securify';
import priorityCheckout from '../../utils/priorityCheckoutUtils';
import { isBrowser } from '../../utils';
const secure = securify();

function getFilteredkeys(keys) {
  const filteredKeys = keys.filter((key) =>
      (key !== 'default_deprecated' && key !== 'size_representation' && key !== 'size_chart' && key !== 'search' && key !== 'swatchImage'));
  const keyOrder = ['default', 'front', 'back', 'right', 'left', 'top', 'bottom'];
  const orderedKeys = keyOrder.filter((item) => filteredKeys.some((key) => item === key)) || [];
  const remainingKeys = filteredKeys.filter(item => !keyOrder.some((key) => item === key)) || [];
  return orderedKeys.concat(remainingKeys);
}

function getTimeRange(start, end) {
  if (!start || !end) {
    return null;
  }
  let st;
  let et;
  const sHours = start.getHours();
  let sMins = start.getMinutes();
  const eHours = end.getHours();
  let eMins = end.getMinutes();
  if (sMins < 10) {
    sMins = `0${sMins}`;
  }
  if (eMins < 10) {
    eMins = `0${eMins}`;
  }
  if (sHours > 12) {
    st = `${sHours - 12} : ${sMins} PM`;
  } else if (sHours === 0) {
    st = `12  : ${sMins} AM`;
  } else if (sHours === 12) {
    st = `12  : ${sMins} PM`;
  } else {
    st = `${sHours}  : ${sMins} AM`;
  }
  if (eHours > 12) {
    et = `${eHours - 12} : ${eMins} PM`;
  } else if (eHours === 0) {
    et = `12  : ${eMins} AM`;
  } else if (eHours === 12) {
    et = `12  : ${eMins} PM`;
  } else {
    et = `${eHours} : ${eMins} AM`;
  }
  return `${st} - ${et}`;
}

// Strictly needs Stye Response (_state)
function isSellableInventoryOOS(_state) {
  const state = _state;
  const styleOptions = at(state, 'data.sizes') || [];
  const itemAvailable = styleOptions.some((item) => {
    const sellableInventoryCount = at(item, 'selectedSeller.sellableInventoryCount');
    if (sellableInventoryCount) {
      return item.available;
    }
    return false;
  });
  return !(itemAvailable);
}

function showAddToBag(_state, preLaunch) {
  if (preLaunch) {
    return false;
  }
  const state = _state;
  const isPreOrderItem = state.isPreOrderItem || at(state, 'data.preOrder') || false;
  if (priorityCheckout() && !isPreOrderItem) {
    return !isSellableInventoryOOS(state);
  }
  if (inPriceRevealMode() && !isPreOrderItem) {
    return isSlotEntryEnabled() && !isSellableInventoryOOS(state);
  }
  return true;
}

function isOutOfStock(state) {
  // style type DEL check is done at the service end
  // we will check only of outOfStock flag
  return at(state, 'data.flags.outOfStock');
}

function isSizeAvailable(state) {
  return (get(state, "data.sizes") || []).some(
    size => size.available
  );
}

function getProductDescriptor(descriptors = [], title = '') {
  const description = filter(descriptors, (obj) => (
    obj.title === title
  ));
  if (description && description[0]) {
    return description[0].description;
  }
  return null;
}

function getProductImageUrl(imgObj = null, config = {}) {
  if (imgObj) {
    const secureSrc = at(imgObj, 'secureSrc');
    const imageFormula = secureSrc || at(imgObj, 'src');
    let url = imageFormula.replace('($width)', config.width).replace('($height)', config.height).replace('($qualityPercentage)', config.q);
    if (config.type) {
      const searchKey = "assets.myntassets.com/";
      const startPos = url.indexOf(searchKey) + searchKey.length;
      if (startPos > 0) {
        url = url.substr(0, startPos) + `${config.type},` + url.substr(startPos);
      }
    }
    return secure(url);
  }
  return null;
}


function getEndTimerData(timerEndDate) {
  const timeEnd = parseInt(timerEndDate, 10);
  if (timeEnd && !isNaN(timeEnd)) {
    const curDate = new Date().getTime();
    const endDate = timeEnd * 1000;
    const diff = endDate - curDate;
    let msec = diff;
    if (diff <= 0) {
      return {
        hour: '-1',
        minute: '-1',
        second: '-1',
        showTimer: false
      };
    }

    let hours = Math.floor(msec / 1000 / 60 / 60);
    hours = hours < 10 ? `0${hours}` : hours;
    msec -= hours * 1000 * 60 * 60;
    let minutes = Math.floor(msec / 1000 / 60);
    minutes = minutes < 10 ? `0${minutes}` : minutes;
    msec -= minutes * 1000 * 60;
    let seconds = Math.floor(msec / 1000);
    seconds = seconds < 10 ? `0${seconds}` : seconds;
    msec -= seconds * 1000;
    return {
      hour: hours,
      minute: minutes,
      second: seconds,
      showTimer: true
    };
  }
  return null;
}

const isGreaterDate = (date) => {
  const time = parseInt(date, 10);
  if (time && !isNaN(time)) {
    const curDate = new Date().getTime();
    return (time * 1000) > curDate;
  }
  return false;
};

const isLesserDate = (date) => {
  const time = parseInt(date, 10);
  if (time && !isNaN(time)) {
    const curDate = new Date().getTime();
    return (time * 1000) < curDate;
  }
  return false;
};

const isPreLaunch = () => {
  let systemAttributes = [];
  if (isBrowser()) {
    systemAttributes = get(window, '__myx.pdpData.systemAttributes') || [];
  }
  const prelaunchAttr = systemAttributes.filter(
    ({ systemAttributeValueEntry: { systemAttributeEntry } }) => systemAttributeEntry.attributeName === 'Enable Buy Button'
  ) || [];
  return get(prelaunchAttr[0], 'systemAttributeValueEntry.valueName') === 'No';
}


export { getFilteredkeys, getTimeRange, isSellableInventoryOOS, isOutOfStock, showAddToBag, getProductDescriptor,
  getProductImageUrl, getEndTimerData, isGreaterDate, isLesserDate, isSizeAvailable, isPreLaunch };
