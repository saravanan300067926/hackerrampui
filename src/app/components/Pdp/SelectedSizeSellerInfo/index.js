import React from 'react';
import find from "lodash/find";
import get from 'lodash/get';
import { isSellerAvailableForSku } from '../helper.js';
import styles from './SelectedSizeSellerInfo.css';
import Sellers from '../Sellers';
import PriceInfo from '../PriceInfo';
import { getProductImageUrl } from '../utils';
import TrackMadalytics from '../../../utils/trackMadalytics';
import { showAddToBag, isPreLaunch } from '../utils';
import Track from '../../../utils/track';
import { getDeliveryMessage } from '../helper';

class SelectedSizeSellerInfo extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        selectedSkuid: '',
        showSeller: false,
        promiseDate: null
    };
    this.handleSellerPopUpClose = this.handleSellerPopUpClose.bind(this);
    this.showSellerPopup = this.showSellerPopup.bind(this);
    this.getSelectedImage = this.getSelectedImage.bind(this);
    this.getData = this.getData.bind(this);
  }

  componentWillReceiveProps(nextProps){
    const updateDelivery = nextProps.promiseDate && nextProps.promiseDate !== this.state.promiseDate
    if (nextProps.selectedSkuid !== this.state.selectedSkuid || updateDelivery) {
      this.setState({
        selectedSkuid: nextProps.selectedSkuid,
        promiseDate: nextProps.promiseDate
      });
    }
  }

  handleSellerPopUpClose() {
    this.setState({
        showSeller: false
    });
  }

  sendMAEvent(sellersCounnt) {
    const custom = {
      widget: {
        name: "seller",
        type: "card"
      },
      widget_items: {
        name: "more-sellers",
        data_set: {
          data: [
            {
              name: "seller-count",
              id: sellersCounnt
            }
          ]
        },
        type: "button"
      }
    };
    const eventName = 'More Sellers Click';
    const entityDetails = {};
    TrackMadalytics.sendEvent(eventName, 'pdp', entityDetails, custom);
  }

  showSellerPopup() {
    this.setState({
        showSeller: true
    });
  }

  getSelectedImage() {
    const styleImage = get(this.props, "data.media.albums.0");
    if (styleImage) {
      const imgConfig = {
        width: 80,
        height: 120,
        q: 20
      };
      return getProductImageUrl(get(styleImage, "images.0"), imgConfig);
    }
    return null;
  };

  getData() {
    const pincode = localStorage.getItem("pincode") || null;
    const data = this.props.data || {};
    const { selectedSkuid, promiseDate } = this.state;
    const deliveryMessage = pincode && this.props.serviceabilityCallDone ? `${getDeliveryMessage(
      promiseDate,
      get(data, "flags.disableBuyButton"),
      get(data, "serviceability.launchDate"),
      get(data, "preOrder")
    )} ${promiseDate ? `- ${pincode}` : ''}` : null;

    const selectedSizeData = (
      get(data, "sizes") || []
    ).find(size => size.skuId === selectedSkuid);
    const productBrand = get(data, "brand.name");
    const productName = get(data, "name");
    const productImage = this.getSelectedImage();

    const selectedSkuData = find(
      get(data, "sizes"),
      ({ skuId }) => skuId === selectedSkuid
    );
    const selectedSellerPartnerId = get(
      selectedSkuData,
      "selectedSeller.sellerPartnerId"
    );
    const selectedSellerData = find(
      get(selectedSkuData, "sizeSellerData"),
      seller => seller.sellerPartnerId === selectedSellerPartnerId
    );
    const discountData =
      find(
        get(data, "discounts"),
        discount =>
          discount.discountId === get(selectedSellerData, "discountId")
      );
    const mrp =
      get(selectedSkuData, "price") ||
      get(data, "mrp");
    const discountedPrice = get(selectedSellerData, "discountedPrice");
    const sellerName =
      get(
        find(
          get(data, "sellers"),
          seller => seller.sellerPartnerId === selectedSellerPartnerId
        ),
        "sellerName"
      );

    const sizeSellers = (get(selectedSkuData, "sizeSellerData") || []).filter(
      seller => isSellerAvailableForSku(selectedSizeData, seller)
    );
    const moreSellersMessage = `${sizeSellers.length - 1} more seller${
      sizeSellers.length > 2 ? "s" : ""
    } available `;
    const bestPrice = Math.min(
      ...sizeSellers.map(seller => seller.discountedPrice || mrp)
    );
    const state = {
      data: this.props.data
    };
    const preLaunch = isPreLaunch();
    const showSellerInfo = showAddToBag(state, preLaunch);

    return {
      deliveryMessage,
      productBrand,
      productName,
      productImage,
      discountData,
      discountedPrice,
      sellerName,
      moreSellersMessage,
      bestPrice,
      showSellerInfo,
      mrp,
      sizeSellers,
      selectedSellerData
    }
  }

  render() {
    const { deliveryMessage,
            productBrand,
            productName,
            productImage,
            discountData,
            discountedPrice,
            sellerName,
            moreSellersMessage,
            bestPrice,
            showSellerInfo,
            mrp,
            sizeSellers,
            selectedSellerData
          } = this.getData();
    const data = this.props.data || {};
    const { selectedSkuid } = this.state;
    return selectedSkuid ? (
      <div className={styles.sellerInfoContainer}>
          <div className="size-price">
            <PriceInfo
              discountData={discountData}
              mrp={mrp}
              discountedPrice={discountedPrice} />
            <span className={styles.deliveryMessage}>{deliveryMessage}</span>
            {showSellerInfo &&
              <div className={styles.sellerName}>
                Seller: <b>{sellerName}</b>
              </div>
            }
            {sizeSellers.length > 1 && showSellerInfo && (
              <div className={styles.moreSellerLink}>
                <span
                  className={styles.sizeMoreSellers}
                  onClick={() =>
                    {
                      this.showSellerPopup();
                      this.sendMAEvent(sizeSellers.length);
                      Track.event('pdp', 'click_on_view_sellers');
                    }}
                >
                  {moreSellersMessage}
                </span>
                {bestPrice < discountedPrice ? (
                  <span className={styles.bestPrice}>
                    from Rs.
                    {` ${bestPrice}`}
                  </span>
                ) : null}
              </div>
            )}
          </div>
          {this.state.showSeller &&
            <Sellers
              data={data}
              sizeSellers={sizeSellers}
              handleClose={this.handleSellerPopUpClose}
              selectedSkuid={selectedSkuid}
              Brand={productBrand}
              productName={productName}
              ImageURL={productImage}
              addToBag={this.props.addToBag}
              serviceabilityCallDone={this.props.serviceabilityCallDone}
              selectedSellerData={selectedSellerData} />}
      </div>
    ) : null;
  }
}

SelectedSizeSellerInfo.propTypes = {
  data: React.PropTypes.object,
  selectedSkuid: React.PropTypes.number,
  addToBag: React.PropTypes.func,
  getPromiseDate: React.PropTypes.func,
  promiseDate: React.PropTypes.string,
  serviceabilityCallDone: React.PropTypes.bool
};

export default SelectedSizeSellerInfo;
