import React from 'react';
import at from 'v-at';
import ProductList from '../ProductList';
import styles from './crosssell.css';
import isArray from 'lodash/isArray';

class CrossSell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: null
    };
  }

  render() {
    const crossSellProducts = at(this.props, 'related.CrossSell');
    const products = at(crossSellProducts, 'products');
    let productsAvailable = false;

    if (isArray(products) && products.length > 0) {
      productsAvailable = true;
    } else if (products === null) {
      productsAvailable = false;
    }

    if (crossSellProducts && productsAvailable) {
      return (
        <div className={styles.container} ref="crossContainer">
          <h3 className={styles.heading}>Customers also liked</h3>
          <ProductList eventName={"cross_sell_click"} products={crossSellProducts.products} from={at(this.props, 'data.id')} />
        </div>
      );
    }
    return null;
  }
}

CrossSell.propTypes = {
  data: React.PropTypes.object.isRequired,
  related: React.PropTypes.object.isRequired
};

export default CrossSell;
