/*
 * IMPORTANT:
 * Segregated the PDP Dynamically fetched Components
 * so that unnecessary bytes don't add up in the main bundle.
 */

import React from 'react';
import Loadable from 'react-loadable';
import Loader from '../Loader';
import Spinner from '../Spinner';
import styles from './pdp.css';


const FixedLoader = props => {
  if (props.pastDelay) {
    // When the loader has taken longer than the delay
    return <Loader show={true} />;
  }
  return null;
};

FixedLoader.propTypes = {
  pastDelay: React.PropTypes.bool
};

const OnPageLoader = props => {
  if (props.pastDelay) {
    // When the loader has taken longer than the delay
    return <div className={styles.onPageLoader}><Spinner /></div>;
  }
  return null;
};

OnPageLoader.propTypes = {
  pastDelay: React.PropTypes.bool
};

const pages = {
  emiPlan: () =>
    import(/* webpackChunkName: "emiplans" */ '../EmiPlan'),
  sizeChart: () =>
    import(/* webpackChunkName: "sizechart" */ '../SizeChart/NewIndex'),
  deliveryHalfCard: () =>
    import(/* webpackChunkName: "d-deliveryhalfcard" */ '../Pdp/Pincode/HalfCards/DeliveryHalfCard')
};

export default function loadAsync(pageKey, fixed) {
  return Loadable({
    loader: () => pages[pageKey](),
    loading: props => {
      return fixed ? <FixedLoader {...props} pageKey={pageKey} /> : <OnPageLoader {...props} pageKey={pageKey} />
    },
    timeout: 10000
  });
}
