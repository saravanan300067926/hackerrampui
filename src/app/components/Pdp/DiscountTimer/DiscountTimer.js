import React from 'react';
import isEmpty from 'lodash/isEmpty';
import styles from '../pdp.css';
import at from 'v-at';
import get from "lodash/get";
import { SlotTimer } from '../../Header/slotTimer';
import { getEndTimerData, isGreaterDate, isLesserDate } from '../utils';
import { getDefaultSellerInfo } from '../helper';

let showDiscountTimerID = null;

class DiscountTimer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      discountTimerData: {},
      showDiscountTimer: false,
      timerTitle: ''
    };
    this.getDicountTimerInfo = this.getDicountTimerInfo.bind(this);
  }

  componentDidMount() {
    this.fetchDiscountTimer();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.selectedSkuid !== this.props.selectedSkuid) {
      this.fetchDiscountTimer(nextProps.selectedSkuid)
    }
  }

  fetchDiscountTimer(selectedSkuid) {
    const showSlotTimer = SlotTimer.show();
    const { startDate, endDate } = this.getDicountTimerInfo(selectedSkuid);

    // stop any running discount timer
    if (showDiscountTimerID) {
      window.clearInterval(showDiscountTimerID);
    }

    if (startDate && endDate && !showSlotTimer && !this.state.showDiscountTimer) {
      const isDisEndDateGreater = isGreaterDate(endDate);
      const isDisStartDateLesser = isLesserDate(startDate);
      if (isDisEndDateGreater && isDisStartDateLesser) {
        try {
          const saleBanner = document.getElementById('headerSaleBannerCtn');
          if (saleBanner) {
            saleBanner.style.display = 'none';
          }
        } catch (e) {
          console.log('dom error', e);
        }

        const getTimer = () => {
          const completeData = getEndTimerData(endDate);

          this.setState({
            discountTimerData: completeData
          });

          if (!at(completeData, 'showTimer')) {
            window.clearInterval(showDiscountTimerID);
            this.setState({
              showDiscountTimer: false
            });
            window.location.reload(true);
          } else {
            this.setState({
              showDiscountTimer: true
            });
          }
        };

        // start once
        getTimer();
        showDiscountTimerID = setInterval(() => {
          getTimer();
        }, 1000);
      }
    }
  }

  getDicountTimerInfo(selectedSkuid) {
    let discount;
    let skuId = selectedSkuid || this.props.selectedSkuid;
    if (!skuId) {
      const uriSellerPartnerId = new URLSearchParams(
        window.location.search
      ).get("sellerPartnerId");
      const uriSkuId = new URLSearchParams(window.location.search).get("skuId");
      discount = get(
        getDefaultSellerInfo(
          get(this.props, "data"),
          Number(uriSellerPartnerId),
          Number(uriSkuId)
        ),
        "discount"
      );
    } else {
      discount = get(
        (get(this.props, "data.sizes") || []).find(
          size => size.skuId === skuId
        ),
        "selectedSeller.discount"
      );
    }
    this.setState({
      timerTitle: get(discount, "discountText")
    })
    const startDate = Number(get(discount, "timerStart"));
    const endDate = Number(get(discount, "timerEnd"));
    return {
      startDate,
      endDate
    }
  }

  render() {
    if (this.state.showDiscountTimer && this.state.timerTitle && !isEmpty(at(this.state, 'discountTimerData'))) {
      return (
        <div className={styles.discountTimer}>
          <span className={styles.timerTitle}>{at(this.state, 'timerTitle')}</span>
          <span className={styles.discountTime}>
            <span><b>{at(this.state, 'discountTimerData.hour')}</b>h</span>
            <span className={styles.discountTimerSep}>:</span>
            <span><b>{at(this.state, 'discountTimerData.minute')}</b>m</span>
            <span className={styles.discountTimerSep}>:</span>
            <span><b>{at(this.state, 'discountTimerData.second')}</b>s</span>
          </span>
        </div>
      );
    } return null;
  }
}

DiscountTimer.propTypes = {
  data: React.PropTypes.object.isRequired,
  selectedSkuid: React.PropTypes.string
};

export default DiscountTimer;
