import React from 'react';
import at from 'v-at';
import styles from './index.css';
import getKVPair from '../../../utils/kvpairs';
import { getProductDescriptor } from '../utils';

const ArticleATSABlacklist = getKVPair('pdp.contentRevamp.FTF.exclutionList') || []
const ATSAValueRemoved = ['NA', 'Others', 'Other', 'None'];

class ArticleTable extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showmore: false
    };
    this.getcStyleNote = this.getcStyleNote.bind(this);
    this.showMore = this.showMore.bind(this);
    this.getSortedAttributes = this.getSortedAttributes.bind(this);
    this.renderTable = this.renderTable.bind(this);
    this.getSortedAttributesAfterBlackList = this.getSortedAttributesAfterBlackList.bind(this);
    this.renderShowMore = this.renderShowMore.bind(this);
  }

  getSortedAttributes(articleType, articleAttributes) {
    // Filtering out ATSA which need to be considered and sort them
    const atsaToConsider = at(window.__myx, 'atsa') || [];
    const sortedAttributes = [];
    if (articleAttributes) {
      for (let atsa = 0; atsa < atsaToConsider.length; atsa++) {
        const value = articleAttributes[atsaToConsider[atsa]];
        if (value && ATSAValueRemoved.indexOf(value) === -1) {
          sortedAttributes.push({ key: atsaToConsider[atsa], value });
        }
      }
      // In case article type is not defined in json
      if (sortedAttributes.length === 0) {
        for (const atsa in articleAttributes) {
          if (articleAttributes.hasOwnProperty(atsa) && ATSAValueRemoved.indexOf(articleAttributes[atsa]) === -1) {
            sortedAttributes.push({ key: atsa, value: articleAttributes[atsa] });
          }
        }
      }
    }
    return sortedAttributes;
  }

  getSortedAttributesAfterBlackList(sortedAttributes) {
    return sortedAttributes.filter((attr) => (ArticleATSABlacklist.indexOf(attr.key) === -1));
  }

  getcStyleNote() {
    const styleNote = getProductDescriptor(at(this.props, 'data.descriptors'), 'style_note');
    if (styleNote) {
      return (<div className={styles.sizeFitDesc} style={{ marginTop: '0px' }}>
        <h4 className={styles.sizeFitDescTitle}>Complete The Look</h4>
        <p className={styles.sizeFitDescContent} dangerouslySetInnerHTML={{ __html: styleNote }}></p>
      </div>);
    }
    return null;
  }

  showMore() {
    this.setState({ showmore: true });
  }

  renderTable(sortedArray) {
    return sortedArray.map((element, index) => {
      const { key, value } = element;
      return (<div className={styles.row} key={`product-${element}-${index}`}>
        <div className={styles.rowKey}>{key}</div>
        <div className={styles.rowValue}>{value}</div>
      </div>);
    });
  }

  renderShowMore(sortedAttributesAfterBlackList) {
    if (sortedAttributesAfterBlackList.length > 0) {
      return this.state.showmore ? (<div className={styles.showMoreContainer}>
        <div className={styles.tableContainer}>{this.renderTable(sortedAttributesAfterBlackList.slice(8))}</div>
        {this.getcStyleNote()}
      </div>) : null;
    }
    return (<div className={styles.showMoreContainer}>
      {this.getcStyleNote()}
    </div>);
  }

  render() {
    const articleType = at(this.props, 'data.analytics.articleType') || null;
    const articleAttributes = at(this.props, 'data.articleAttributes') || null;
    const sortedAttributes = this.getSortedAttributes(articleType, articleAttributes);
    const sortedAttributesAfterBlackList = this.getSortedAttributesAfterBlackList(sortedAttributes);
    const showLength = 8;
    return (
      <div className={styles.sizeFitDesc}>
        {
          sortedAttributesAfterBlackList.length > 0
            ? <h4 className={styles.sizeFitDescTitle} style={{ paddingBottom: '12px' }}>Specifications</h4>
            : null
        }

        <div className={styles.tableContainer}>
          {this.renderTable(sortedAttributesAfterBlackList.slice(0, showLength))}
        </div>

        {
            !this.state.showmore && sortedAttributesAfterBlackList.length > 0 && sortedAttributesAfterBlackList.length > showLength
              ? <div onClick={this.showMore} className={styles.showMoreText}>See More</div>
              : null
        }

        {this.renderShowMore(sortedAttributesAfterBlackList)}
      </div>
    );
  }
}

ArticleTable.propTypes = {
  data: React.PropTypes.object.isRequired
};

export default ArticleTable;
