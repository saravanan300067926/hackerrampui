import React from 'react';
import at from 'v-at';
import get from "lodash/get";
import styles from './pdp-offers.css';
import keyBy from 'lodash/keyBy';
import Client from '../../../services';
import Track from '../../../utils/track';
import config from '../../../config';
import features from '../../../utils/features';
import { isMobile, isBrowser } from '../../../utils';
import lowerCase from 'lodash/lowerCase';
import classNames from 'classnames/bind';
import { isOutOfStock } from '../utils';
import MfuOffer from './mfuOffer';
import {getDefaultSkuSeller} from '../helper';

const kvPairs = isBrowser() ? window.__myx_kvpairs__ || {} : {};
const offerClasses = classNames.bind(styles);
const isMobileDevice = isMobile();
const offersExpanded = features('pdp.offers.expanded') === 'true';
const bestPriceCheckEnabled = features('disableCouponServiceOnPDP') === 'false';
const offerVisible = kvPairs['pdp.offers.visible'];

const setEMIValueInOffers = (offers, selectedSkuId, pdpData) => {
  const emiPlans = JSON.parse(kvPairs["product.emi.info"]);
  let discountedPrice = get(pdpData, "selectedSeller.discountedPrice");
  if (selectedSkuId) {
    discountedPrice = get(
      (get(pdpData, "sizes") || []).find(size => size.skuId === selectedSkuId),
      "selectedSeller.discountedPrice"
    );
  }
  offers = offers.map(offer => {
    if (offer.type === "EMI") {
      const emiValue = Math.ceil(
        discountedPrice *
          (emiPlans.minInterestRate / 100 + 1) /
          emiPlans.maxMonths
      );
      offer.description = `EMI starting from Rs.${emiValue}/month`;
    }
    return offer;
  });
};

const setEBDOffer = (offers, selectedSkuId, pdpData) => {
  let discount = get(pdpData, "selectedSeller.discount");
  if (selectedSkuId) {
    discount = get(
      (get(pdpData, "sizes") || []).find(size => size.skuId === selectedSkuId),
      "selectedSeller.discount"
    );
  }
  if (get(discount, "type") === 512) {
    const ebd = get(pdpData, "earlyBirdOffer");
    if (get(ebd, "title") && !offers.some(offer => offer.type === "EARLYBIRD")) {
      offers.push({
        type: "EARLYBIRD",
        title: get(ebd, "title"),
        description: get(ebd, "description"),
        action: get(discount, "link"),
        image: null
      });
    }
  } else {
    offers = (offers || []).filter(offer => offer.type !== "EARLYBIRD");
  }
  return offers;
};

const setComboOffer = (offers, selectedSkuId, pdpData) => {
  let discount = get(pdpData, "selectedSeller.discount");
  if (selectedSkuId) {
    discount = get(
      (get(pdpData, "sizes") || []).find(size => size.skuId === selectedSkuId),
      "selectedSeller.discount"
    );
  }
  if (get(discount, "heading")) {
    const combo = {
      type: "COMBO",
      title: get(discount, "heading"),
      description: get(discount, "description"),
      action: get(discount, "link"),
      freeItem: get(discount, "freeItem"),
      freeitemImage: get(discount, "freeItemImage")
    };
    let found = false;
    for (let i = 0; i < offers.length; i++) {
      if (offers[i].type === "COMBO") {
        offers[i] = combo;
        found = true;
        break;
      }
    }
    if (!found) {
      offers.push(combo);
    }
  } else {
    offers = (offers || []).filter(offer => offer.type !== "COMBO");
  }
  return offers;
};

class Offers extends React.Component {
  constructor(props) {
    super(props);

    let offers = at(props, 'data.offers');
    offers = keyBy(offers, 'type');
    const offerAvailable = offers[offerVisible] || offers.EMI || offers.COMBO;

    this.state = {
      showAllOffers: false,
      couponData: {},
      bestPriceCheckEnabled: bestPriceCheckEnabled && !props.isPreOrderItem,
      offersExpanded,
      offerAvailable,
      fetchRequestInProgress: false,
      bestPriceFetched: false
    };
    this.getOfferHtml = this.getOfferHtml.bind(this);
    this.showOffers = this.showOffers.bind(this);
  }

  componentDidMount() {
    if (this.state.offersExpanded) {
      this.showOffers();
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.selectedSkuid !== nextProps.selectedSkuid) {
      this.showOffers();
    }
  }

  getOfferHtml() {
    let allOffers = at(this.props, 'data.offers');
    let selectedSkuId = this.props.selectedSkuid;
    if (!selectedSkuId) {
      const sellerPartnerId = new URLSearchParams(window.location.search).get(
        "sellerPartnerId"
      );
      const skuId = new URLSearchParams(window.location.search).get("skuId");
      selectedSkuId = getDefaultSkuSeller(
        get(this.props, "data"),
        Number(sellerPartnerId),
        Number(skuId)
      ).skuId;
    }
    setEMIValueInOffers(allOffers, selectedSkuId, this.props.data);
    allOffers = setEBDOffer(allOffers, selectedSkuId, this.props.data);
    allOffers = setComboOffer(allOffers, selectedSkuId, this.props.data);
    const offers = allOffers;

    const loyaltyPointsEnabled = at(this.props, 'data.flags.loyaltyPointsEnabled');
    return (
      <div className={styles.extraOffer}>
        {loyaltyPointsEnabled && (<MfuOffer />)}
        {
          offers.map((offer, index) => {
            const content = (
              <div>
                <div className={styles.offerColumn}>
                  {offers.offerImage ? <div className={`${styles.offerImage}`}>
                    <img src={offers.offerImage} />
                  </div> : null
                  }
                  <div className={`${styles.offerColumnText}`}>
                    <div className={`${styles.offerTitle}`}>
                      <b>{at(offer, 'title')}</b>
                    </div>
                    <span className={`${styles.offerDesc}`} dangerouslySetInnerHTML={{ __html: at(offer, 'description') }} />
                  </div>
                  <div className={`${styles.viewMoreLink}`}>
                    {this.showViewMore(offer)}
                  </div>
                </div>
              </div>
            );

            if (at(offer, 'type') === 'EMI') {
              return (
                <div className={styles.offer} key={`offer-${index}`} onClick={this.props.toggleEmiPlans}>
                  {content}
                </div>
              );
            }

            return (
              <a className={styles.offer} key={`offer-${index}`} target="_blank" href={at(offer, 'action')}>
                {content}
              </a>
            );
          })
        }
      </div>
    );
  }

  getOfferHeader() {
    if (!this.state.showAllOffers && !this.state.offersExpanded) {
      const baseOffer = this.state.offerAvailable;
      let buttonHtml = null;
      let moreOfferCount = 0;

      if (baseOffer) {
        const allOffers = at(this.props, 'data.offers');

        if (allOffers && allOffers.length) {
          moreOfferCount = allOffers.length;
        }

        if (!this.state.bestPriceCheckEnabled) {
          moreOfferCount = moreOfferCount - 1;
        }

        const cta = moreOfferCount > 1 ? 'offers' : 'offer';
        const text = moreOfferCount <= 0 ? 'view offer' : `view ${moreOfferCount} more ${cta}`;
        buttonHtml = (<span className={styles.moreOffersButton} onClick={this.showOffers}>{text}</span>);
      } else if (this.state.bestPriceCheckEnabled) {
        buttonHtml = (
          <span onClick={this.showOffers} className={`${styles.moreOffersButton} ${styles.tapForBest}`}>{isMobileDevice ? 'Tap' : 'Click'} for best price</span>
        );
      }

      return (
        <div className={styles.offerController}>
          {baseOffer && (
            <div className={styles.offerHeading}>{at(baseOffer, 'title')} </div>
          )}
          {buttonHtml}
        </div>
      );
    }
    return null;
  }

  getCouponHtml() {
    if (this.state.bestPriceFetched) {
      const bestPrice = at(this.state, 'couponData.bestPrice');
      const coupon = at(bestPrice, 'display');
      const message = at(bestPrice, 'message');
      let couponInfo = '';

      if (coupon && coupon.length) {
        couponInfo = (
          <div>
            <div className={styles.offerTitle}>
              <b>
                Best Price: <span className={styles.price}>Rs. {at(bestPrice, 'price.discounted')}</span>
              </b>
            </div>
            <ul className={styles.offerDesc}>
              {
                coupon.map((item, index) => {
                  const label = item.label;
                  let labelClass = '';

                  if (label) {
                    labelClass = offerClasses({
                      boldText: lowerCase(label).indexOf('coupon code') !== -1
                    });
                  }

                  return (
                    <li key={index}>
                      <div className={`${styles.bullet}`} />
                      <div className={`${styles.labelMarkup}`}>{item.label} <span className={labelClass}>{item.value}</span></div>
                    </li>
                  );
                })
              }
            </ul>
          </div>
        );
      }

      return (
        <div className={styles.offerBlock}>
          <div className={styles.offer}>
            {couponInfo}
            {message && (
              <div className={`${styles.offerTitle} ${styles.couponNotFound}`}>{message}</div>
            )}
          </div>
        </div>
      );
    }

    return null;
  }

  showViewMore(offer) {
    return at(offer, 'type') === 'EMI' ? (
      <span>
        <span className={styles.showOfferLink}>View Plan
          <span className={`${styles.arrow}`} />
        </span>
      </span>
    ) : (
      <span className={styles.showOfferLink}>View
        <span className={`${styles.arrow}`}></span>
      </span>
    );
  }

  fetchOffer() {
    Track.event('pdp', 'tap-for-offer');
    if (!this.state.offersExpanded) {
      this.props.toggleRequestLoader();
    }

    let sellerPartnerId = get(
      this.props,
      "data.selectedSeller.sellerPartnerId"
    );
    if (this.props.selectedSkuid) {
      sellerPartnerId = get(
        (get(this.props, "data.sizes") || []).find(
          size => size.skuId === this.props.selectedSkuid
        ),
        "selectedSeller.sellerPartnerId"
      );
    }
    if (!sellerPartnerId) {
      this.setState({
        couponData: null
      });
      return;
    }
    const styleId = get(this.props, 'data.id');
    Client.get(config('coupons') + `/${styleId}/${sellerPartnerId}`).end((err, res) => {
      if (!this.state.offersExpanded) {
        this.props.toggleRequestLoader();
      }

      this.setState({
        fetchRequestInProgress: false
      });

      if (err) {
        this.props.notify.error('Oops! Something went wrong. Please try again in some time.');
        return;
      }

      if (at(res, 'body.status.statusType') === 'ERROR') {
        this.props.notify.error('Oops! Something went wrong. Please try again in some time.');
        return;
      }

      this.setState({
        couponData: res && res.body,
        showAllOffers: true,
        bestPriceFetched: true
      });
    });
  }

  showOffers() {
    if (!this.state.fetchRequestInProgress) {
      this.setState({
        fetchRequestInProgress: true
      }, () => {
        if (this.state.bestPriceCheckEnabled) {
          this.fetchOffer();
        } else {
          this.setState({
            showAllOffers: true
          });
        }
      });
    }
  }

  listOffers() {
    if (this.state.showAllOffers || this.state.offersExpanded) {
      return (
        <div>
          {this.getCouponHtml()}
          {this.getOfferHtml()}
        </div>
      );
    }
    return null;
  }

  render() {
    const offers = at(this.props, 'data.offers');
    if (!isOutOfStock(this.props) && ((offers && offers.length) || this.state.bestPriceCheckEnabled)) {
      return (
        <div className={styles.container}>
          <h4>BEST OFFERS <span className={`myntraweb-sprite ${styles.similarColorsIcon}`} /></h4>
          {this.getOfferHeader()}
          {this.listOffers()}
        </div>
      );
    }
    return null;
  }
}

Offers.propTypes = {
  data: React.PropTypes.object,
  toggleRequestLoader: React.PropTypes.func,
  isPreOrderItem: React.PropTypes.bool,
  notify: React.PropTypes.object,
  emiPlans: React.PropTypes.array,
  toggleEmiPlans: React.PropTypes.func,
  selectedSkuid: React.PropTypes.string
};

export default Offers;
