import React, { Component } from 'react';
import at from 'v-at';
import bus from 'bus';
import styles from './pdp-offers.css';
import { compactNumbers } from 'compactnums';
import get from 'lodash/get';

class MfuOffer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.displayLP();
    // Start listening to beacon upadate events in the bus.
    bus.on('beacon-data', (data) => {
      this.displayLP(data);
    });
  }

  redirectToExchangePage = () => {
    window.location.href = this.state.redirectUri;
  }

  displayLP(data) {
    const features = window.__myx_features__ || {};
    const isMFUSaleOn = get(data, 'features["mfu.enable"]', features['mfu.enable']) === true;
    const userLP = data && data.lp ? data.lp.points : at(window, '__myx_lp__.points');
    const loggedIn = data && data.session ? data.session.login : at(window, '__myx_session__.login');
    const redirectUri = data && data.features ? data.features['mfu.exchange.url'] : features['mfu.exchange.url'];
    const showPoints = loggedIn && this.hasValidPoints(userLP);
    this.setState({ isMFUSaleOn, userLP, showPoints, redirectUri });
  }

  hasValidPoints(lp) {
    return (lp && !isNaN(lp));
  }

  render() {
    let title;
    if (!this.state.isMFUSaleOn) {
      return null;
    } else if (!this.state.showPoints) {
      title = 'Use points, save more!';
    } else {
      title = `You have ${compactNumbers(this.state.userLP)} points currently`;
    }
    return (
      <a className={styles.offer} onClick={this.redirectToExchangePage}>
        <div className={styles.offerColumn}>
          <div className={styles.offerColumnText}>
            <div className={`${styles.offerTitle} ${styles.mfuTitleContainer}`}>
              <span className={`myntraweb-sprite ${styles.mfuIcon}`} /> <b>{title}</b>
            </div>
            <span className={`${styles.offerDesc}`}>
              You can earn more points by exchanging items
            </span>
          </div>
          <div className={styles.viewMoreLink}>
            <span className={styles.showOfferLink}>Exchange & earn
              <span className={`${styles.arrow}`} />
            </span>
          </div>
        </div>
      </a>
    );
  }
}

export default MfuOffer;
