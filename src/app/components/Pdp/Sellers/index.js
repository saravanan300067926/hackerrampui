import React from 'react';
import styles from './sellers.css';
import Modal from '../../Utils/Modal/Modal.js';
import SellersList from './SellersList';
import serviceabilityAPI from '../serviceabilityAPI.js';
import get from "lodash/get";
import TrackMadalytics from '../../../utils/trackMadalytics';
import { constructDataSetForScreen } from '../../../common/analytics/common';
import { isSellerAvailableForSku, getDeliveryMessage } from '../helper.js';


const getSelectedSizeData = props => {
  const selectedSkuId = get(props, "selectedSkuid");
  if (selectedSkuId) {
    return (
      (get(props, "data.sizes") || []).find(
        size => size.skuId === selectedSkuId
      ) || {}
    );
  }
  return {};
};

const sendAllSellersMAEvent = props => {
  const selectedSizeData = getSelectedSizeData(props);
  const sizeSellers = getSellers(selectedSizeData);
  const data = props.data;
  const custom = {
    widget: {
      name: "all-sellers-list-pdp",
      type: "card"
    },
    widget_items: {
      data_set: {
        data: data.sellers.map(seller =>
          constructDataSetForScreen({
            ...props.data,
            skuId: props.selectedSkuid,
            sellerPartnerId: sizeSellers.sellerPartnerId,
            discountedPrice: sizeSellers.discountedPrice
          })
        )
      }
    }
  }
  const eventName = 'screenLoad';
  const entityDetails = {};
  TrackMadalytics.sendEvent(eventName, 'pdp', entityDetails, custom);
}

const getSellers = selectedSizeData => {
  return (get(selectedSizeData, "sizeSellerData") || []).filter(seller =>
    isSellerAvailableForSku(selectedSizeData, seller)
  );
};

class Sellers extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      selectedSeller: '',
      deliveryMessages: {},
      selectedSellerPartnerId: get(props, 'selectedSellerData.sellerPartnerId', '')
    }
    this.getProductInfo = this.getProductInfo.bind(this);
    this.changeSellerHandler = this.changeSellerHandler.bind(this);
    this.fetchServiceability = this.fetchServiceability.bind(this);
    this.getQueryPairs = this.getQueryPairs.bind(this);
  }

  componentDidMount() {
    if (this.props.selectedSkuid) {
      this.fetchServiceability(this.props);
    }
    sendAllSellersMAEvent(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedSkuid) {
      this.fetchServiceability(nextProps);
    }
    sendAllSellersMAEvent(nextProps);
  }
  
  getQueryPairs() {
    return (
      this.props.sizeSellers.map(seller => {
        return {
          skuId: this.props.selectedSkuid,
          sellerPartnerId: seller.sellerPartnerId
        };
      })
    );
  }

  fetchServiceability(props) {
    const pincode = localStorage.getItem("pincode") || null;
    const data = get(this.props, 'data');
    if (pincode && props.serviceabilityCallDone) {
      const queryPairs = this.getQueryPairs();

      serviceabilityAPI.checkV2(data, queryPairs, pincode).then(serviceability => {
        const entries = (
          get(serviceability, "itemServiceabilityEntries") || []
        ).filter(itemEntry => {
          return Number(itemEntry.skuId) === props.selectedSkuid;
        });
        let deliveryMessages = {};
        (entries || []).forEach(entry => {
          const promiseDate = get(entry, "serviceabilityEntries.0.promiseDate");
          deliveryMessages[
            Number(entry.itemReferenceId)
          ] = getDeliveryMessage(
            promiseDate,
            get(data, "flags.disableBuyButton"),
            get(data, "serviceability.launchDate"),
            get(data, "preOrder")
          );
        });

        this.setState({
          deliveryMessages
        });
      });
    } else {
      this.setState({
        deliveryMessages: {}
      });
    }
  }

  getProductInfo(selectedSize) {
    const { Brand, productName, ImageURL } = this.props;
    return (
      <div className={styles.productInfo}>
        <img
          src={ImageURL} />
        <div className={styles.info}>
          <div className={styles.brand}>{Brand}</div>
          <div className={styles.productName}>{productName}</div> 
          <div>Size: {<b>{selectedSize}</b>}</div>
        </div>
      </div>
    );
  }

  changeSellerHandler(e) {
  this.setState({
    selectedSellerPartnerId: Number(e.target.value)
  })
  }

  render() {
    const selectedSizeData = getSelectedSizeData(this.props);
    const selectedSize = get(selectedSizeData, 'label', '');
    const allSellers = get(this.props, 'data.sellers');
    const discounts = get(this.props, 'data.discounts');
    return (
      <Modal onClose={this.props.handleClose}>
        <div className={styles.SellersContainer}>
        <span
          onClick={this.props.handleClose}
          className={`myntraweb-sprite ${styles.modalclose}`} />
          {this.getProductInfo(selectedSize)}
          <div className={styles.selectSellerTitle}>Select Seller</div>
          <SellersList
            serviceabilityCallDone={this.props.serviceabilityCallDone}
            sizeSellers={this.props.sizeSellers}
            selectedSellerPartnerId={this.state.selectedSellerPartnerId}
            deliveryMessages={this.state.deliveryMessages}
            allSellers={allSellers}
            preOrder={get(this.props, "data.preOrder")}
            mrp={get(this.props, 'data.mrp')}
            discounts={discounts}
            handleClose={this.props.handleClose}
            addToBag={this.props.addToBag}/>
        </div>
      </Modal>
    );
  }
}

Sellers.propTypes = {
  data: React.PropTypes.object,
  handleClose: React.PropTypes.func,
  sizeSellers: React.PropTypes.object,
  selectedSkuid: React.PropTypes.string,
  Brand: React.PropTypes.string,
  productName: React.PropTypes.string,
  ImageURL: React.PropTypes.string,
  selectedSellerData: React.PropTypes.object,
  addToBag: React.PropTypes.func,
  serviceabilityCallDone: React.PropTypes.bool
};

export default Sellers;
