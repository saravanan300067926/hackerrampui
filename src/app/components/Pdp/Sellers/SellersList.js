import React from "react";
import styles from './sellers.css';
import find from "lodash/find";
import get from 'lodash/get';
import PriceInfo from '../PriceInfo';
import { isPreLaunch } from '../utils';

const getSellerName = (allSellers, sellerPartnerId) => {
  return get(
    find(
      allSellers,
      seller => seller.sellerPartnerId === sellerPartnerId
    ),
    "sellerName"
  );
}

const SellersList = props => {
  const { sizeSellers, mrp, addToBag, allSellers=[], discounts=[], deliveryMessages, handleClose } = props;
  const pincode = localStorage.getItem("pincode") || null;
  return (
    <div className={styles.sellerList}>
      {
        sizeSellers.map((seller) => {
          const discountData = (discounts || []).find(
            discount => discount.discountId === seller.discountId
          );
          const sellerName = seller.name ? seller.name : getSellerName(allSellers, seller.sellerPartnerId);
          const deliveryMessage = deliveryMessages[seller.sellerPartnerId] ? deliveryMessages[seller.sellerPartnerId] : 'Loading...';
          return (
            <div key={seller.sellerPartnerId} className={styles.sellerListItemContainer}>
              <div className={styles.sellersInfoContainer}>
                <div className={styles.price}>{sellerName}</div>
                <PriceInfo
                  discountData={discountData}
                  mrp={mrp}
                  fontSize={'14px'}
                  discountedPrice={seller.discountedPrice} />
                {pincode && props.serviceabilityCallDone && <div className={styles.deliveryInfo}>{deliveryMessage}</div>}
              </div>
              {!isPreLaunch() &&
                <div
                  onClick={(e) => {
                    addToBag('addToCart', seller.sellerPartnerId);
                    handleClose('addToBag');
                    }}
                  className={styles.addToBagCTA}>
                  {props.preOrder ? "PRE-ORDER" : "ADD TO BAG"}
                </div> 
              }
            </div>
          );
        })
      }
    </div>
  );
}

export default SellersList;