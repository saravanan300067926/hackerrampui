import DebitCard from '../../../resources/svg/DebitCard';
import DeliveryTruck from '../../../resources/svg/DeliveryTruck';
import ExchangeIcon from '../../../resources/svg/ExchangeIcon';
import TryAndBuy from '../../../resources/svg/TryAndBuy';

export default function iconProvider(iconName) {
  switch (iconName) {
    case 'card':
      return DebitCard;
    case 'exchange':
      return ExchangeIcon;
    case 'truck':
      return DeliveryTruck;
    case 'try':
      return TryAndBuy;
    default:
      return null;
  }
}
