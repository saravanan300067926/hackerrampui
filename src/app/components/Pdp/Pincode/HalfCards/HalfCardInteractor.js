import get from 'lodash/get';
import { isAbEnabled } from '../../../../services/RemoteConfig';

const abConfig = {
  key: 'desktop-user-onboarding',
  control: 'VariantA',
  test: 'VariantB'
};


const HalfCardInteractor = {
  getData(type) {
    return get(window, ['__myx_kvpairs__', 'pwaNewUserOnboarding.halfCards', type], {});
  },

  featureEnabled() {
    return get(window, ['__myx_features__', 'pwaNewUserOnboarding.halfCards'], false) && isAbEnabled(abConfig, true);
  },

  hasHalfCard(type) {
    const hasCard =
      ['try', 'return', 'exchange'].includes(type) &&
      this.getData(type) !== null;

    if (type === 'returnAndExchange') {
      return this.hasHalfCard('return') && this.hasHalfCard('exchange');
    }

    return hasCard;
  },

  getHeader(type) {
    const header = get(this.getData(type), 'desktopHeader');
    return type !== 'returnAndExchange' ? header : 'Easy Exchange & Return';
  },

  getFooter(type) {
    const actualType = type !== 'returnAndExchange' ? type : 'exchange';
    return get(this.getData(actualType), 'footer');
  }
};

export default HalfCardInteractor;
