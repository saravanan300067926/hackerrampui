import React from 'react';
import styles from './HalfCard.css';
import interactor from '../HalfCardInteractor';
import iconProvider from './HalfCardIconProvider';
import throttle from 'lodash/throttle';

function objectEntries(obj) {
  const entries = [];
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      entries.push([key, obj[key]]);
    }
  }
  return entries;
}

function decode(escapedHTML) {
  return escapedHTML.replace(/&lt;/g, '<').replace(/&gt;/g, '>');
}

function renderDiv(className, content) {
  return (
    <div
      className={className}
      dangerouslySetInnerHTML={{ __html: decode(content) }} />
  );
}

const Header = props =>
  props.content &&
  props.content.length &&
  renderDiv(styles['delivery-halfcard-header'], props.content);

const InfoItem = props => {
  const Icon = iconProvider(props.icon);
  return (
    <div className={styles['delivery-halfcard-info-item']}>
      <div className={styles['delivery-halfcard-index']}>{props.index + 1}</div>
      {Icon ? <Icon className={styles['delivery-halfcard-icon']} /> : null}
      {renderDiv(styles['delivery-halfcard-description'], props.children)}
    </div>
  );
};

InfoItem.propTypes = {
  icon: React.PropTypes.string,
  index: React.PropTypes.number,
  children: React.PropTypes.string
};

function getLineHeight(count) {
  let itemSize;
  if (window.innerHeight <= 700) {
    itemSize = 64 + 6;
  } else if (window.innerHeight <= 800) {
    itemSize = 72 + 8;
  } else if (window.innerHeight <= 1000) {
    itemSize = 84 + 12;
  } else {
    itemSize = 96 + 16;
  }
  return (count - 1) * itemSize;
}

export default class HalfCard extends React.Component {
  constructor(props) {
    super(props);

    this.getThrottledReloadFunction = this.getThrottledReloadFunction.bind(this);
    this.reloadFunction = this.getThrottledReloadFunction();
  }

  componentDidMount() {
    window.addEventListener('resize', this.reloadFunction);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.reloadFunction);
  }

  getThrottledReloadFunction() {
    return throttle(() => {
      this.forceUpdate();
      console.log('Hello');
    }, 1000).bind(this);
  }

  render() {
    const { type, showHeader } = this.props;
    const { header, body } = interactor.getData(type);
    const content = objectEntries(body);

    const Line = iconProvider('line');

    return (
      content &&
      interactor.featureEnabled() && (
        <div className={styles['delivery-halfcard']}>
          {Line ? <Line
            style={{ height: `${getLineHeight(content.length)}px` }}
            className={styles['delivery-halfcard-vertical-line']} /> : null}

          {showHeader && <Header content={header} />}

          {content.map(([icon, innerHTML], key) => (
            <InfoItem key={key} index={key} icon={icon}>
              {innerHTML}
            </InfoItem>
          ))}
        </div>
      )
    );
  }
}
HalfCard.propTypes = {
  type: React.PropTypes.string,
  showHeader: React.PropTypes.bool
};
