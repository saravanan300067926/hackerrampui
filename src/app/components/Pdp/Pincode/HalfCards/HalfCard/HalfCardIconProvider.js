import CashIcon from '../../../../../resources/svg/CashIcon';
import cardAgentCustomer from '../../../../../resources/svg/cardAgentCustomer';
import cardAgentDeliver from '../../../../../resources/svg/cardAgentDeliver';
import cardClothes from '../../../../../resources/svg/cardClothes';
import cardExchange from '../../../../../resources/svg/cardExchange';
import cardReturn from '../../../../../resources/svg/cardReturn';
import cardTry from '../../../../../resources/svg/cardTry';
import line from '../../../../../resources/svg/DottedLine';

export default function halfCardIconProvider(iconName) {
  switch (iconName) {
    case 'cardCash':
      return CashIcon;
    case 'cardAgentCustomer':
      return cardAgentCustomer;
    case 'cardAgentDeliver':
      return cardAgentDeliver;
    case 'cardClothes':
      return cardClothes;
    case 'cardExchange':
      return cardExchange;
    case 'cardReturn':
      return cardReturn;
    case 'cardTry':
      return cardTry;
    case 'line':
      return line;
    default:
      return null;
  }
}
