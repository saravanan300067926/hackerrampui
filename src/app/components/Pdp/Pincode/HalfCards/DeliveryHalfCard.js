import React from 'react';
import HalfCard from './HalfCard/HalfCard.js';
import HalfCardContainer from '../../../HalfCard';
import HalfCardInteractor from './HalfCardInteractor';
import styles from './DeliveryHalfCard.css';

function isValidType(type) {
  return (
    (type && type === 'returnAndExchange') ||
    type === 'return' ||
    type === 'exchange' ||
    type === 'try'
  );
}

function getHalfCards(type) {
  let component;
  if (type === 'returnAndExchange') {
    component = (
      <div className={styles['half-card-container']}>
        <HalfCard type="exchange" showHeader={true} />
        <HalfCard type="return" showHeader={true} />
      </div>
    );
  } else {
    component = (
      <div className={styles['half-card-container']}>
        <HalfCard type={type} />
      </div>
    );
  }
  return component;
}

const DeliveryHalfCard = props => {
  const on = isValidType(props.type);
  return on ?
    <HalfCardContainer
      title={HalfCardInteractor.getHeader(props.type)}
      subTitle="HOW IT WORKS?"
      on={on}
      onToggle={props.onToggle}
      closeOnLeft={true}
      width="wide"
      footer={HalfCardInteractor.getFooter(props.type)}
      render={() => getHalfCards(props.type)} />
    : null;
};

DeliveryHalfCard.propTypes = {
  type: React.PropTypes.string,
  onToggle: React.PropTypes.func
};

export default DeliveryHalfCard;
