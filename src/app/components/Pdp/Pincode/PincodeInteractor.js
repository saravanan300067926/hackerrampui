import get from 'lodash/get';
import { isAbEnabled } from '../../../services/RemoteConfig';

const abConfig = {
  key: 'desktop-user-onboarding',
  control: 'VariantA',
  test: 'VariantB'
};

const PincodeInteractor = {
  featureEnabled() {
    return get(window, ['__myx_features__', 'pwaNewUserOnboarding.deliveryOptions'], false) && isAbEnabled(abConfig, true);
  }
};

export default PincodeInteractor;
