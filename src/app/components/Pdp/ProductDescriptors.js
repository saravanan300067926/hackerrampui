import React from 'react';
import styles from './pdp.css';
import at from 'v-at';
import ArticleTable from './ArticleTable';
import Track from '../../utils/track';
import { getProductDescriptor } from './utils';

class ProductDescriptors extends React.Component {

  getProductDetails() {
    const description = getProductDescriptor(at(this.props, 'data.descriptors'), 'description');
    if (description) {
      return (
        <div className={styles['product-description']}>
          <h4 className={styles['product-description-title']}>Product Details <span className={`myntraweb-sprite ${styles.productDetailsIcon}`} /></h4>
          <p className={styles['product-description-content']} dangerouslySetInnerHTML={{ __html: description }}></p>
        </div>
      );
    }
    return null;
  }

  getMaterialAndCare() {
    const material = getProductDescriptor(at(this.props, 'data.descriptors'), 'materials_care_desc');
    if (material) {
      return (
        <div className={styles.sizeFitDesc}>
          <h4 className={styles.sizeFitDescTitle}>Material & Care</h4>
          <p className={styles.sizeFitDescContent} dangerouslySetInnerHTML={{ __html: material }}></p>
        </div>
      );
    }
    return null;
  }

  getSizeFitDesc() {
    const sizeFitDesc = getProductDescriptor(at(this.props, 'data.descriptors'), 'size_fit_desc');
    if (sizeFitDesc) {
      return (
        <div className={styles.sizeFitDesc}>
          <h4 className={styles.sizeFitDescTitle}>Size & Fit</h4>
          <p className={styles.sizeFitDescContent} dangerouslySetInnerHTML={{ __html: sizeFitDesc }}></p>
        </div>
      );
    } return null;
  }

  updateGA = () => {
    Track.event('PDP', 'ProductDetails', `ProductDetailsCheck | ${at(window, '__myx.pdpData.id')}`);
  }

  render() {
    return (
      <div className={styles.productDescriptors}>
        <input
          className={styles.inputProductDetails}
          onClick={() => this.updateGA()}
          type="checkbox"
          id="productDetails"
          value="productDescriptors" />
        <label className={styles.inputProductDetailsLabel} htmlFor="productDetails">
          Product Details
          <span className={styles.expandProductDetails}></span>
        </label>
        <div className={styles.productDescriptorsContainer}>
          {this.getProductDetails()}
          {this.getSizeFitDesc()}
          {this.getMaterialAndCare()}
          <ArticleTable data={this.props.data} />
        </div>
      </div>
    );
  }
}

ProductDescriptors.propTypes = {
  data: React.PropTypes.object.isRequired
};

export default ProductDescriptors;
