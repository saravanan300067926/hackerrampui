import React from "react";
import styles from "./PriceInfo.css";
import get from "lodash/get";
import trim from 'lodash/trim';
import replace from 'lodash/replace';

const getPrice = props => {
  const discountData = get(props, "discountData");
  const mrp = get(props, "mrp");
  if (discountData) {
    const discountedPrice = get(props, "discountedPrice");
    return discountedPrice || mrp;
  }
  return mrp;
}

const MRPPopup = props => {
  if (!props.showMrpPopUp) {
    return null;
  }
  const mrp = get(props, "mrp");
  let discountData = get(props, "discountData");
  const finalPrice = getPrice();
  if (typeof discountData !== 'undefined') {
    const discountText = props.discountText || get(discountData, 'label', '');
    discountData = trim(replace(discountText || '', /\(|\)/g, ''));
  }
  return (<div className={styles['mrp-verbiage']} tabIndex="0">
    <div>
      <b>Price Details</b>
    </div>
    <div>
      Maximum Retail Price<span className={styles['mrp-verbiage-amt']}>Rs. {mrp}</span>
    </div>
    <div>
      (Incl. of all taxes)
    </div>
    <hr />
    <div>
      Discount<span className={styles['mrp-verbiage-amt']}>{discountData}</span>
    </div>
    <div>
      <b>Selling Price</b><span className={styles['mrp-verbiage-amt']}>{`Rs. ${finalPrice}`}</span>
    </div>
    <div style={{ 'margin': '0px' }}>
      (Incl. of all taxes)
    </div>
  </div>);
}

const DiscountContainer = props => {
  const customFontSize = props.fontSize || '16px';
  let finalPrice = '';
  const discountData = get(props, "discountData");
  const mrp = get(props, "mrp");
  const discountedPrice = get(props, "discountedPrice");
  if (discountData || get(props, 'discountText')) {
    const discountedPrice = get(props, "discountedPrice");
    finalPrice = discountedPrice || mrp;
  } else {
    finalPrice = mrp;
  }
  const discountText = props.discountText || get(discountData, "label", "");

  if (typeof discountData !== 'undefined' || get(props, 'discountText')) {
    if (discountedPrice && discountedPrice < mrp) {
      return (
        <div style={{ fontSize: customFontSize}} className={styles['discount-container']}>
          <span className={styles.price} tabIndex="0"><strong>{`Rs. ${finalPrice}`}</strong></span>
          <MRPPopup {...props}/>
          <span className={styles.mrp}><s>Rs. {mrp}</s></span>
          <span className={styles.discount}>{discountText || ''}</span>
        </div>
      );
    }
    return (<div style={{ fontSize: customFontSize}} className={styles['discount-container']}>
      <span className={styles.price} tabIndex="0"><strong>{`Rs. ${finalPrice}`}</strong></span>
      <MRPPopup {...props}/>
      <span className={styles.discount} dangerouslySetInnerHTML={{ __html: discountText || '' }} />
    </div>);
  }
  return (<div className={styles['discount-container']}>
    <strong className={styles.price}>{`Rs. ${finalPrice}`}</strong>
  </div>);
};

export default props => {
  return (
    <DiscountContainer {...props} />
  );
};
