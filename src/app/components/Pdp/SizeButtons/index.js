import React from 'react';
import at from 'v-at';
import get from "lodash/get";
import { Link } from 'react-router';
import { isOutOfStock, getProductDescriptor } from '../utils';
import styles from './size-buttons.css';
import { inPriceRevealMode } from '../../../utils/slotUtils';
import { isBrowser, getFeatures } from '../../../utils';
import getKvPair from '../../../utils/kvpairs';
import some from 'lodash/some';
import findIndex from 'lodash/findIndex';
import TrueFitPDPContainer from '../../SizeAndFit/TrueFit/PDPContainer';
import { getItem } from '../../../utils/localStorageUtil';
import SizeReco from '../SizeReco';
import priorityCheckout from '../../../utils/priorityCheckoutUtils';

const sellableInventoryModeEnabled = getFeatures('sellableInventoryModeEnabled') || false;

class SizeButtons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      trueFitContainer: false
    };
    const loadTrueFitContainer = setInterval(() => {
      if (getItem('isTruefit') === 'true') {
        clearInterval(loadTrueFitContainer);
        this.setState({
          trueFitContainer: true
        });
      }
      if (getItem('isTruefit') === 'false') {
        clearInterval(loadTrueFitContainer);
      }
    }, 50);
  }

  getSizeChartErrorMesage() {
    if (isBrowser()) {
      try {
        const container = document.getElementById('sizeButtonsContainer');
        if (container && this.props.showSelectSizeError) {
          if (!container.scrollIntoViewIfNeeded) {
            container.scrollIntoView();
          } else {
            container.scrollIntoViewIfNeeded();
          }
        }
      } catch (ex) {
        console.log(ex);
      }
    }
    if (this.props.showSelectSizeError) {
      return (
        <span className={styles['size-error-message']}>Please select a size</span>
      );
    }
    return null;
  }

  getInventoryCount(skuId, showBubblePrice) {
    if (this.props.hideInventoryCount) {
      return null;
    }
    const styleOptions = at(this.props, 'data.sizes');
    const isPreOrderitem = at(this.props, 'data.preOrder') || false;
    if (styleOptions instanceof Array) {
      const selectedOption = styleOptions.filter(option => (option.skuId === skuId))[0];
      let inventoryCount = get(selectedOption, 'selectedSeller.availableCount');

      if ((sellableInventoryModeEnabled || inPriceRevealMode() || priorityCheckout()) && !isPreOrderitem) {
        inventoryCount = at(selectedOption, 'selectedSeller.sellableInventoryCount') || 0;
      }

      if (selectedOption && inventoryCount > 0 && inventoryCount < 5 && !isOutOfStock(this.props)) {
        return (
          <span style={{bottom: `${showBubblePrice ? '-10px' : '-1px'}`}} className={styles['inventory-left']}>{inventoryCount} left</span>
        );
      }
    }
    return (
      <span className={styles['inventory-left-hidden']}>Hide</span>
    );
  }

  getSkuIdFromSizeLabel(label) {
    const sizes = at(this.props, 'data.sizes') || [];
    const sizeIndex = findIndex(sizes, (size) => (size.label === label));
    return sizeIndex !== -1 && at(this.props.data, `sizes.${sizeIndex}.skuId`);
  }

  getSizeButtons() {
    const commonPrice = get(this.props, 'data.commonPrice');
    const styleOptions = at(this.props, 'data.sizes');
    const productStyleId = at(this.props, 'data.id');
    if (styleOptions && styleOptions.length) {
      const isBigButton = some(styleOptions, (item) => {
        const price = !commonPrice && get(item, "selectedSeller.discountedPrice");
        const labelSize = item.label && item.label.length;
        return labelSize > 3 || typeof price === 'number';
      });
      const sizeButtonsStyle = this.props.showSelectSizeError ? 'size-buttons-error' : 'size-buttons';
      return (
        <div className={styles[sizeButtonsStyle]}>
          {
            styleOptions.map((styleOption, index) => {
              let isAvailable;
              if (sellableInventoryModeEnabled || inPriceRevealMode() || priorityCheckout()) {
                isAvailable = get(styleOption, 'available') &&
                  get(styleOption, 'selectedSeller.sellableInventoryCount');
              } else {
                isAvailable = get(styleOption, 'available') &&
                  get(styleOption, 'selectedSeller.availableCount');
              }
              const isSelected = at(this.props, 'selectedSkuid') === styleOption.skuId;
              let className = isSelected && isAvailable ? styles['size-button-selected'] : styles['size-button'];

              if (!isAvailable) {
                className = styles['size-button-disabled'];
              }

              if (isBigButton) {
                className = `${className} ${styles['big-size']}`;
              }

              const discountedPrice = get(styleOption, "selectedSeller.discountedPrice");
              const sellerPartnerId = get(styleOption, "selectedSeller.sellerPartnerId");
              const showBubblePrice = !commonPrice && discountedPrice && isAvailable;

              if (styleOption.originalStyle === false && productStyleId !== styleOption.styleId) {
                return (
                  <Link className={styles.sizeButtonAsLink} key={`sizebutton-link-${index}`} to={`/${styleOption.styleId}?skuId=${styleOption.skuId}`}>
                    <div className={styles.tipAndBtnContainer}>
                      <div className={styles.buttonContainer}>
                        <button className={className} disabled={!isAvailable} key={index}>
                          <span className={isAvailable ? styles['size-strike-hide'] : styles['size-strike-show']}></span>
                          <p className={styles['unified-size']}>
                            {styleOption.label}
                            {
                              showBubblePrice
                                ? <div className={styles['sku-price']}> Rs. {discountedPrice}</div>
                                : null
                            }
                          </p>
                        </button>
                        {this.getInventoryCount(styleOption.skuId, showBubblePrice)}
                      </div>
                      {this.getToolTip(styleOption.skuId)}
                    </div>
                  </Link>
                );
              }

              return (
                <div className={styles.tipAndBtnContainer} key={`sizebutton-${index}`}>
                  <div className={styles.buttonContainer}>
                    <button className={className} key={index} onClick={(e) => this.props.selectSize(e, styleOption.skuId, null, sellerPartnerId)}>
                      <span className={isAvailable ? styles['size-strike-hide'] : styles['size-strike-show']}></span>
                      <p className={styles['unified-size']}>
                        {styleOption.label}
                        {
                          showBubblePrice
                            ? <div className={styles['sku-price']}> Rs. {discountedPrice}</div>
                            : null
                        }
                      </p>
                    </button>
                    {this.getInventoryCount(styleOption.skuId, showBubblePrice)}
                  </div>
                  {this.getToolTip(styleOption.skuId)}
                </div>
              );
            })
          }
        </div>
      );
    }
    return null;
  }
  getSizeChartLink() {
    const isSizechartAvailable =
      get(this.props, "data.sizechart.sizeChartUrl") ||
      get(this.props, "data.sizechart.sizeRepresentationUrl");

    if (isSizechartAvailable) {
      return (
        <span className={styles['size-chart']}>
          <button className={styles['show-size-chart']} onClick={this.props.showSizeChart}>Size Chart</button>
          <span className={`${styles.arrow}`}></span>
        </span>
      );
    }
    return null;
  }

  getSizeType() {
    const size = at(this.props, 'data.sizes.0');
    const sizeType = at(size, 'sizeType');

    if (sizeType) {
      return (
        <span>({sizeType})</span>
      );
    }
    return null;
  }

  getHeader() {
    const props = this.props;
    if (!props.hideHeader) {
      return (
        <div className={styles['size-header']}>
          <h4 className={styles['select-size']}>SELECT SIZE {this.getSizeType()}</h4>
          {this.getSizeChartLink()}
        </div>
      );
    }
    return null;
  }

  getSizeChartDescription() {
    const sizeFitDescription = getProductDescriptor(at(this.props.data, 'descriptors'), 'size_fit_desc');
    return sizeFitDescription ? (
      <div className={styles.sizeChartInfo}>
        <div className={styles['size-chart-description']} dangerouslySetInnerHTML={{ __html: sizeFitDescription }}></div>
      </div>
    ) : null;
  }

  getToolTip(hoveredSkuId) {
    let measurementObj = null;
    const props = this.props;
    const analyticsData = at(window, '__myx.pdpData.analytics');
    const genderAT = `${at(analyticsData, 'gender')}_${at(analyticsData, 'articleType')}`;
    const hideSizeTooltip = getKvPair('myntraweb.pdp.hideSizeTooltip') || [];
    if (hoveredSkuId && hideSizeTooltip.indexOf(genderAT) === -1) {
      const sizes = at(this.props, 'data.sizes') || [];
      const sizeIndex = findIndex(sizes, (size) => (size.skuId === hoveredSkuId));
      measurementObj = sizeIndex !== -1 && at(props.data, `sizes.${sizeIndex}.measurements.0`);
    }
    if (measurementObj) {
      return (<div className={styles.sizeTip}>
        <div className={styles.sizeTipMeta}>
          <div className={styles.bodymeasure}>
            <span className={styles.measurementType}>{measurementObj.type}:</span>
            <span className={styles.measurementName}>{`${measurementObj.name} - ${measurementObj.displayText}`}</span>
          </div>
          {this.getSizeChartDescription()}
        </div>
      </div>);
    }

    return this.getSizeChartDescription() ? (
      <div className={styles.sizeTip}>
        <div className={styles.sizeTipMeta}>
          {this.getSizeChartDescription()}
        </div>
      </div>
    ) : null;
  }

  getTrueFitPDPContainer() {
    if (this.state.trueFitContainer) {
      return <TrueFitPDPContainer data={this.props.data} onAddToBag={data => this.selectSizeAndAddToBag(data)} />;
    }
    return null;
  }

  selectSizeAndAddToBag(recommendationData) {
    const { size } = recommendationData;
    const selectedSkuId = this.getSkuIdFromSizeLabel(size);
    this.props.selectSize(null, selectedSkuId, () => this.props.addToBagCallback('cart'));
  }

  render() {
    if (at(this.props, "data.flags.outOfStock")) {
      return (
        <div className={styles['out-of-stock']}>
          This product is currently sold out
        </div>
      );
    }
    return (
      <div key={at(this.props, 'data.id')} className={styles['size-container']} id="sizeButtonsContainer">
        {this.getHeader()}
        {this.getSizeChartErrorMesage()}
        {this.getSizeButtons()}
        {this.getTrueFitPDPContainer()}
        {at(this, 'props.showReco') && <SizeReco {...this.props} />}
      </div>
    );
  }
}

SizeButtons.propTypes = {
  data: React.PropTypes.object.isRequired,
  hideHeader: React.PropTypes.bool,
  hideInventoryCount: React.PropTypes.bool,
  showSelectSizeError: React.PropTypes.bool.isRequired,
  selectSize: React.PropTypes.func.isRequired,
  selectedSkuid: React.PropTypes.number,
  showSizeChart: React.PropTypes.func,
  showReco: React.PropTypes.bool,
  recoTextObj: React.PropTypes.object,
  recoLoading: React.PropTypes.bool,
  selectedProfile: React.PropTypes.string,
  sizeProfiles: React.PropTypes.array,
  selectProfile: React.PropTypes.func,
  relatedProducts: React.PropTypes.bool,
  profilesShown: React.PropTypes.bool,
  showProfiles: React.PropTypes.func,
  showSimilarProducts: React.PropTypes.func,
  trackLoginEvent: React.PropTypes.func,
  onLoginClick: React.PropTypes.func,
  addToBagCallback: React.PropTypes.func
};

export default SizeButtons;
