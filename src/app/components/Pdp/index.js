import React from 'react';
import config from '../../config';
import Client from '../../services';
import Loader from '../Loader';
import loadPdpAsync from './loadPdpAsync';
import bus from 'bus';
import Notify from '../Notify';
import ProductDescriptors from './ProductDescriptors';
import Track from '../../utils/track';
import Colors from './Colors';
import SizeButtons from './SizeButtons';
import BreadCrumbs from './BreadCrumbs';
import CrossLinks from './CrossLinks';
import Meta from './Meta';
import Supplier from './Supplier';
import Offers from './Offers';
import styles from './pdp.css';
import at from 'v-at';

import AdmissionControl from '../AdmissionControl';
import {
  isSellableInventoryOOS, isOutOfStock, showAddToBag, getProductImageUrl
} from './utils';
import { isBrowser, getFeatures } from '../../utils';
import { isShowSlotPopup, getSlots, inPriceRevealMode } from '../../utils/slotUtils';
import { getWishlistSummaryMap, addProductToWlSummaryStore } from '../../utils/wishlistSummaryManager';
import get from 'lodash/get';
import keyBy from 'lodash/keyBy';
import isString from 'lodash/isString';
import classNames from 'classnames/bind';
import TrackMadalytics from '../../utils/trackMadalytics';
import { saveInSession, getFromSession } from '../../utils/sessionStorageUtil';
import isEmpty from 'lodash/isEmpty';
import getKvPair from '../../utils/kvpairs';
import getLoyalty from '../../utils/loyalty';
import commonStyles from '../../resources/coin.css';
import commonUtilityStyles from '../../resources/common.css';
import priorityCheckout, {
  PRIORITY_CHECKOUT_MESSAGE,
  CART_FULL_ERROR_CODE,
  PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE
} from '../../utils/priorityCheckoutUtils';
import OverallRating from '../Ratings/OverallRating';
import DetailedReviews from '../DetailedReviews';
import ImageGrid from './ImageGrid';
import scroll from '@myntra/myx/lib/scroll';
import DiscountTimer from './DiscountTimer/DiscountTimer';
import replace from 'lodash/replace';
import trim from 'lodash/trim';
import ActionCTA from '../ActionCTA';

import Similar from './Similar';
import CrossSell from './CrossSell';
import Pincode from './Pincode';

import fireEvents from '../FireCustomEvents';

const SizeChart = loadPdpAsync('sizeChart', true);
const EmiPlan = loadPdpAsync('emiPlan', true);
const DeliveryHalfCard = loadPdpAsync('deliveryHalfCard', true);

import { getItem, setItem, removeItem } from '../../utils/localStorageUtil.js';
import SelectedSizeSellerInfo from './SelectedSizeSellerInfo';
import { getDefaultSellerInfo, setSelectedSeller, getFilteredPDPDataByServiceability, getDefaultSkuSeller } from './helper.js';

import {
  getLocalImageTagPrioritizer,
  getLocalMiscDataPrioritizer
} from '../../utils/xceleratorUtil.js';
import XceleratorTag from '../../common/xceleratorTag';
import serviceabilityAPI from './serviceabilityAPI';
import { addressOnCart } from './AddressOnCart/AddressOnCart';
import ChatBot from '../Search/Results/Chatbot';
import Bruce from '../Search/Results/Bruce';

const pdpBindClassNames = classNames.bind(styles);
const kvPairs = isBrowser() ? window.__myx_kvpairs__ || {} : {};
const wishlistEnabled = getFeatures('wishlist.enable') === 'true';
const emiPlans = kvPairs['pdp.emi.plans'] || [];
const recommendationEnabled = getFeatures('pdp.recommendation.enable') === 'true';
const sizeRecommendationEnabled = getFeatures('pdp.mfa.recommendation.enable') === 'true';
const crossSellEnabled = getFeatures('pdp.crossSell.enable') === 'true';
const ratingsEnabled = getFeatures('web.ratings.enable') === true;
const sellableInventoryModeEnabled = getFeatures('sellableInventoryModeEnabled') || false;

function isEmptySimilar(related) {
  return (isEmpty(related) || isEmpty(at(related, 'Similar')) || isEmpty(at(related, 'Similar.products')));
}

class Pdp extends React.Component {

  constructor(props, context) {
    super(props, context);
    const data = isBrowser() ? null : this.props.location.dehydratedState;
    this.state = {
      selectedSkuid: parseInt(get(props, 'location.query.skuId'), 10) || null,
      showSelectSizeError: false,
      showCouponBar: false,
      couponData: null,
      showSizeChart: false,
      isLoading: false,
      zoom: false,
      data,
      showSlotPopup: false,
      checkSizeServiceability: false,
      wishlistAddActionName: 'WISHLIST',
      priorityCheckoutWishlistAddActionName: 'WISHLIST NOW',
      isLoggedIn: false,
      isPreOrderItem: false,
      sizesAddedToBag: [],
      wishlistEnabled: isBrowser() ? wishlistEnabled : true,
      showMoreColors: false,
      mobileImageZoom: false,
      related: {},
      crossSell: {},
      showEmiPlans: false,
      showDiscountTimer: false,
      showReco: true,
      recoTextObj: {},
      sizeProfiles: [],
      selectedProfile: '',
      recoLoading: false,
      profilesShown: false,
      pincodeEntered: false,
      similarItemsAvailable: false,
      xceleratorTag: {},
      promiseDate: null,
      serviceablePairs: null,
      serviceable: null,
      serviceabilityCallDone: false
    };

    this.addToBag = this.addToBag.bind(this);
    this.addToWishlist = this.addToWishlist.bind(this);
    this.selectSize = this.selectSize.bind(this);
    this.onZoom = this.onZoom.bind(this);
    this.error = this.error.bind(this);
    this.showSizeChart = this.showSizeChart.bind(this);
    this.selectProfile = this.selectProfile.bind(this);
    this.hideSizeChart = this.hideSizeChart.bind(this);
    this.hideSaleSlot = this.hideSaleSlot.bind(this);
    this.showMoreColors = this.showMoreColors.bind(this);
    this.toggleMobileImageZoom = this.toggleMobileImageZoom.bind(this);
    this.toggleRequestLoader = this.toggleRequestLoader.bind(this);
    this.toggleEmiPlans = this.toggleEmiPlans.bind(this);
    this.showSimilarProducts = this.showSimilarProducts.bind(this);
    this.showProfiles = this.showProfiles.bind(this);
    this.onLoginClick = this.onLoginClick.bind(this);
    this.trackLoginEvent = this.trackLoginEvent.bind(this);
    this.isPincodeEntered = this.isPincodeEntered.bind(this);
    this.getBrandMarkup = this.getBrandMarkup.bind(this);
    this.getDescMarkup = this.getDescMarkup.bind(this);
    this.showServiceabilityHalfCard = this.showServiceabilityHalfCard.bind(this);
    this.hideServiceabilityHalfCard = this.hideServiceabilityHalfCard.bind(this);
    this.isFastFashion = this.isFastFashion.bind(this);
    this.autoWishlist = this.autoWishlist.bind(this);
    this.filterUnserviceableSellers = this.filterUnserviceableSellers.bind(this);
    this.getPromiseDate = this.getPromiseDate.bind(this);
    this.setData = this.setData.bind(this);
    this.loadData = this.loadData.bind(this);
    this.getPreSelectedSkuId = this.getPreSelectedSkuId.bind(this);
  }

  componentDidMount() {
    this.fireEventDuration = (function () {
      const start = (new Date()).getTime();
      return () => {
        const end = (new Date()).getTime();
        fireEvents('PDP', 1, null, end - start);
      }
    })();
    addressOnCart.correctPincodeMismatch();
    bus.on('user.loggedIn', (isLoggedIn) => {
      this.setState({
        isLoggedIn
      });
      getWishlistSummaryMap(isLoggedIn, this.setWishlistStateToSaved.bind(this));
    });
    getWishlistSummaryMap((at(window, '__myx_session__.isLoggedIn') || this.state.isLoggedIn), this.setWishlistStateToSaved.bind(this));
    document.body.style.overflow = 'auto';
    if (window) {
      window.scrollTo(0, 0);
    }

    if (at(window, '__myx_session__')) {
      this.autoWishlist();
    } else {
      bus.on('beacon-data', () => {
        this.autoWishlist();
      });
    }
    this.loadData();
  }

  componentWillUnmount() {
    this.fireEventDuration();
  }

  componentDidUpdate(previousProps) {
    if (previousProps.params.id === at(this.props, 'params.id')) {
      return;
    }
    document.body.style.overflow = 'auto';
    this.state.wishlistAddActionName = 'WISHLIST';
    getWishlistSummaryMap((at(window, '__myx_session__.isLoggedIn') || this.state.isLoggedIn), this.setWishlistStateToSaved.bind(this));
    if (window) {
      window.scrollTo(0, 0);
    }
    this.loadData();
  }

  filterUnserviceableSellers(response, pincode, PostFilterEnablecheckServiceability = true) {
    const { data, selectedSkuid } = this.state;
    let newState,
      pincodeEntered = true;
    if (response === null) {
      newState = {
        ...getFilteredPDPDataByServiceability(
          data,
          null,
          selectedSkuid
        ),
        serviceable: null,
        promiseDate: null,
        serviceabilityCallDone: false
      };
      pincodeEntered = false;
    } else {
      const serviceablePairs = (response || []).filter(
        pair => pair.isServiceable
      );
      if (serviceablePairs.length > 0) {
        newState = {
          ...getFilteredPDPDataByServiceability(
            data,
            serviceablePairs,
            selectedSkuid,
          ),
          serviceabilityCallDone: true
        };
      } else {
        newState = {
          ...getFilteredPDPDataByServiceability(
            get(this.state, "data"),
            [],
            selectedSkuid
          ),
          serviceabilityCallDone: false
        };
      }
    }
    const serviceable = (get(newState, "data.sizes") || []).some(
      size => size.available
    );
    if (serviceable && selectedSkuid !== newState.selectedSkuid) {
      this.refs.notify.info({
        message: `Size availability and seller updated${pincodeEntered ? " for your pincode" : ""}`,
        position: 'right'
      });
    }
    newState.serviceable = serviceable;
    this.setState(newState, () => {
      this.getPromiseDate(pincode, PostFilterEnablecheckServiceability);
    });
  }

  onZoom(zoom) {
    if (zoom) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
    this.setState({
      zoom
    });
  }

  onLoginClick() {
    this.trackLoginEvent('click');
  }

  getPrice() {
    const data = this.state.data || {};
    const mrp = at(data, 'mrp');
    let discountData, discountedPrice;
    if (!this.state.selectedSkuid) {
      discountData = get(data, "selectedSeller.discount");
      discountedPrice = get(data, "selectedSeller.discountedPrice");
    } else {
      const selectedSizeData = (get(data, "sizes") || []).find(
        size => size.skuId === this.state.selectedSkuid
      );
      discountData = get(selectedSizeData, "selectedSeller.discount");
      discountedPrice = get(selectedSizeData, "selectedSeller.discountedPrice");
    }
    return discountedPrice || mrp;
  }

  getLoyaltyHtml() {
    const data = this.state.data || {};
    const loyalty = getLoyalty(data, 'pdp');

    if (!loyalty.enabled || isNaN(loyalty.discount) || isNaN(loyalty.points)) {
      return null;
    }

    return (
      <div>
        <div className={styles.loyaltyContainer}>
          Or pay <strong>Rs. {loyalty.discount}</strong> + <span className={`${commonStyles.coin} ${styles.loyaltycoin}`} />
          {` ${loyalty.points} Points`}
        </div>
      </div>
    );
  }

  getAdditionalMessage() {
    return <span className={styles.vatInfo}>inclusive of all taxes</span>;
  }

  getMRPPopup() {
    const data = this.state.data || {};
    const mrp = at(data, 'mrp');

    let discountData;
    if (!this.state.selectedSkuid) {
      discountData = get(data, "selectedSeller.discount");
    } else {
      const selectedSizeData = (get(data, "sizes") || []).find(
        size => size.skuId === this.state.selectedSkuid
      );
      discountData = get(selectedSizeData, "selectedSeller.discount");
    }

    const finalPrice = this.getPrice();
    if (typeof discountData !== 'undefined') {
      const discountText = at(discountData, 'label');
      discountData = trim(replace(discountText || '', /\(|\)/g, ''));
    }
    return (<div className={styles['mrp-verbiage']} tabIndex="0">
      <div>
        <b>Price Details</b>
      </div>
      <div>
        Maximum Retail Price<span className={styles['mrp-verbiage-amt']}>Rs. {mrp}</span>
      </div>
      <div>
        (Incl. of all taxes)
      </div>
      <hr />
      <div>
        Discount<span className={styles['mrp-verbiage-amt']}>{discountData}</span>
      </div>
      <div>
        <b>Selling Price</b><span className={styles['mrp-verbiage-amt']}>{`Rs. ${finalPrice}`}</span>
      </div>
      <div style={{ 'margin': '0px' }}>
        (Incl. of all taxes)
      </div>
    </div>);
  }

  getDiscountContainer() {
    const data = this.state.data || {};
    const mrp = at(data, 'mrp');

    let discountData, discountedPrice;
    if (!this.state.selectedSkuid) {
      discountData = get(data, "selectedSeller.discount");
      discountedPrice = get(data, "selectedSeller.discountedPrice");
    } else {
      const selectedSizeData = (get(this.state, "data.sizes") || []).find(
        size => size.skuId === this.state.selectedSkuid
      );
      discountData = get(selectedSizeData, "selectedSeller.discount");
      discountedPrice = get(selectedSizeData, "selectedSeller.discountedPrice");
    }

    const finalPrice = this.getPrice();

    const discountText = at(discountData, 'label');
    if (discountedPrice < mrp) {
      return (
        <p className={styles['discount-container']}>
          <span className={styles.price} tabIndex="0"><strong>{`Rs. ${finalPrice}`}</strong></span>
          {this.getMRPPopup()}
          <span className={styles.mrp}><s>Rs. {mrp}</s></span>
          <span className={styles.discount}>{discountText || ''}</span>
        </p>
      );
    }
    return (
      <p className={styles['discount-container']}>
        <span className={styles.price} tabIndex="0"><strong>{`Rs. ${finalPrice}`}</strong></span>
        {this.getMRPPopup()}
      </p>
    );
  }

  getPriorityCheckoutButton() {
    return (
      <div>
        <div className={`${styles['add-to-bag']} ${styles['priority-out-of-stock']}`}>
          Out Of Stock
        </div>
        <div
          onClick={() => this.addToWishlist('wishlist')} className={`${styles.pdpButtonWishlistNow}`}>
          <span className={`${styles.flex} ${styles.column} ${styles.center}} ${styles.wishlistNow}`}>
            {this.state.priorityCheckoutWishlistAddActionName}
            <span className={styles.outOfStockText}>MAY GET RESTOCKED</span>
          </span>
        </div>
      </div>
    );
  }

  getWishlistButton() {
    const btnClass = showAddToBag(this.state) ? styles['add-to-wishlist'] : styles['add-to-wishlist-full'];
    const whBtnClass = this.state.wishlistAddActionName !== 'WISHLIST' ? styles['add-to-wishlist-disabled'] : styles['add-to-wishlist'];

    if (this.state.wishlistEnabled) {
      return (
        <div
          onClick={() => this.addToWishlist('wishlist')}
          className={`${btnClass} ${whBtnClass} ${styles.flex} ${styles.center}`}>
          {this.state.wishlistAddActionName !== 'WISHLIST' ? (
            <span className={`myntraweb-sprite ${styles.whiteWishlistIcon} ${styles.flex} ${styles.center}`}></span>
          ) : (
              <span className={`myntraweb-sprite ${styles.darkWishlistIcon} ${styles.flex} ${styles.center}`}></span>
            )}
          {this.state.wishlistAddActionName}
        </div>
      );
    }
    return null;
  }
  getButtonContent(buttonLabel, outOfStock = false) {
    const isPreOrderitem = get(this.state, 'isPreOrderItem', false);
    const priorityCheckoutButtonContent = priorityCheckout() ? (
      <div className={`${styles.flex} ${styles.column} ${styles.center} ${styles['align-start']}`}>
        <span>{buttonLabel}</span>
        {!outOfStock && (
          <span className={styles['text-10']}>{PRIORITY_CHECKOUT_MESSAGE}</span>
        )}
      </div>
    ) : buttonLabel;

    return isPreOrderitem ? 'PRE-ORDER' : priorityCheckoutButtonContent;
  }

  // ADD TO BAG BUTTON ON PDP PAGE
  getAddToCartButton() {
    if (isOutOfStock(this.state)) {
      return (<div
        className={`${styles['add-to-bag']} ${styles.flex} ${styles.center} ${styles['out-of-stock']}`}>
        {this.getButtonContent('OUT OF STOCK', true)}
      </div>);
    } else if (showAddToBag(this.state)) {
      const sizesAddedToBag = this.state.sizesAddedToBag;
      const selectedSkuid = this.state.selectedSkuid || null;
      const sellerPartnerId = selectedSkuid ?
        get((get(this.state, "data.sizes") || []).find(
          size => size.skuId === selectedSkuid
        ), 'selectedSeller.sellerPartnerId') :
        get(this.state, 'selectedSeller.sellerPartnerId');
      let buttonClass = this.state.wishlistEnabled ? styles['add-to-bag'] : styles['add-to-bag-full'];
      if (priorityCheckout()) {
        buttonClass = this.state.wishlistEnabled ? styles['add-to-bag-with-prior'] : styles['add-to-bag-full-with-prior'];
      }
      if (sizesAddedToBag.length > 0 && sizesAddedToBag.indexOf(selectedSkuid) !== -1) {
        return (
          <a
            href="/checkout/cart"
            onClick={() => {
              Track.event('PDP', 'GoToCartPage', `GoToBagButton | ${sellerPartnerId} | ${at(window, '__myx.pdpData.id')}`);
              fireEvents('PDP', 5);
            }}
            className={`${styles.goToCart} ${buttonClass} ${styles.flex} ${styles.center}`}>
            <span>{this.getButtonContent('GO TO BAG')}</span>
            <span className={`myntraweb-sprite ${styles.whiteRightArrow}`}></span>
          </a>
        );
      }
      return (
        <div
          onClick={() => this.addToBag('cart')}
          className={`${buttonClass} ${styles.flex} ${styles.center}`}>
          <span className={`myntraweb-sprite ${styles.whiteBag} ${styles.flex} ${styles.center}`}></span>
          {this.getButtonContent('ADD TO BAG')}
        </div>
      );
    }
    return null;
  }

  getReturnable() {
    if (!at(this.state, 'data.flags.isReturnable')) {
      return (
        <p className={styles['returnable-content']}>
          This item cannot be returned.
        </p>
      );
    }
    return null;
  }

  setWishlistStateToSaved(wlSummary) {
    if (wlSummary && wlSummary.hasOwnProperty(at(this.props, 'params.id'))) {
      this.setState({ wishlistAddActionName: 'WISHLISTED', priorityCheckoutWishlistAddActionName: 'WISHLISTED' });
    }
  }

  getBrandMarkup() {
    let brandName = at(this.state, 'data.analytics.brand') || at(this.state, 'data.brand.name');
    return brandName ? <h1 className={styles.title}>{brandName}</h1> : null;
  }

  getDescMarkup() {
    const brandName = at(this.state, 'data.analytics.brand') || at(this.state, 'data.brand.name');
    const name = at(this.state, 'data.name');
    return name ? <h1 className={ratingsEnabled && this.state.data.ratings ? styles.name : `${styles.name} ${styles.bb1}`}>
      {name.replace(`${brandName} `, '')}</h1> : null;
  }

  getEmiPlanModal() {
    const price = this.getPrice();

    return (
      <div className={styles.emiPlansContainer}>
        <div className={styles.emiInner}>
          <div onClick={this.toggleEmiPlans} className={`myntraweb-sprite ${styles.emiModalclose}`} />
          <EmiPlan price={price} />
        </div>
      </div>
    );
  }

  getSizeRecoPayload(pdpData, profileId) {
    const skuIds = (at(pdpData, 'sizes') || []).map(size => size.skuId);
    const payload = {
      uidx: at(window, '__myx_session__.login'),
      availableStyleSkuMap: {
        [pdpData.id]: skuIds
      },
      articleType: at(pdpData, 'analytics.articleType')
    };

    if (profileId) {
      Object.assign(payload, { pidx: profileId });
    }

    return payload;
  }

  updateXceleratorTag() {
    const { data: productData } = this.state;
    const enableXcelerator = true;
    // If no ab just return
    if (!enableXcelerator) return;
    const getLocallyPrioritizedImageTag = getLocalImageTagPrioritizer('pdp');
    const getLocallyPrioritizedMiscData = getLocalMiscDataPrioritizer('pdp');
    const xceleratorTag = {
      imageTag: {},
      titleTag: {}
    };

    if (productData) {
      const imageTag = getLocallyPrioritizedImageTag(productData);
      const titleTag = getLocallyPrioritizedMiscData(productData);
      xceleratorTag.imageTag = imageTag || {};
      xceleratorTag.titleTag = titleTag || {};
    }
    this.setState({
      xceleratorTag
    });
  }

  autoWishlist() {
    const autoWishlist = getItem('pdpAutoWishlist');
    if (autoWishlist && at(window, '__myx_session__.isLoggedIn')) {
      removeItem('pdpAutoWishlist');
      this.addToWishlist(autoWishlist);
    }
  }

  isFastFashion() {
    return get(this.state, 'data.ratings.isFastFashion', false);
  }

  isPincodeEntered(status) {
    this.setState({
      pincodeEntered: status,
      checkSizeServiceability: false
    });
  }

  _sendWishlistAnalytics() {
    const sellerPartnerId = get(this.state.data, 'selectedSeller.sellerPartnerId') || get(
      (get(this.state, "data.sizes") || []).find(
        size => size.skuId === this.state.selectedSkuid
      ),
      "selectedSeller.sellerPartnerId"
    );
    const discountedPrice = 1000;
    const price = get(this.state, 'data.mrp');
    if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
      window.Madalytics.send('AddToCollection', {
        type: 'PDP',
        url: window.location.href,
        variant: 'web',
        name: `${'Shopping Page-PDP '}${window.location.pathname}`,
        'data_set': {
          'entity_name': at(this.state, 'data.name') || at(this.state, 'data.brand.name'),
          'entity_type': 'PDP',
          'entity_id': at(this.props, 'params.id'),
          category: at(this.props, 'params.category'),
          brand: at(this.props, 'params.brand'),
          style: at(this.props, 'params.style'),
          "entity_optional_attributes": {
            "discountedPrice": discountedPrice,
            "price": price,
            "seller_partner_id": sellerPartnerId
          }
        },
        'custom': {
          'v1': 'wishlist'
        }
      });
    }
  }

  trackLoginEvent(type) {
    let eventName;
    let custom = {};
    if (type === 'load') {
      eventName = 'Fit assist - load login page';
      custom = { widget: { name: 'Not sure about what size to buy?' } };
    } else {
      eventName = 'Fit assist - Login';
    }
    const pdpData = at(window, '__myx.pdpData');
    const entityDetails = {
      'entity_id': at(pdpData, 'id'),
      'entity_type': 'product',
      'entity_name': at(pdpData, 'name')
    };
    TrackMadalytics.sendEvent(eventName, 'pdp', entityDetails, custom);
  }

  selectSize(e, selectedSkuid, callback, sellerPartnerId) {
    if (e) {
      e.stopPropagation();
    }
    if (this.state.selectedSkuid !== selectedSkuid) {
      const styleOptions = at(this.state, 'data.sizes');
      const skuData = (styleOptions || []).find(
        size => size.skuId === selectedSkuid
      );
      let availableOption = styleOptions.filter((option) => (option.skuId === selectedSkuid && option.available))[0];
      // in price reveal mode && in slotMode check for sellableInventory
      if ((sellableInventoryModeEnabled || inPriceRevealMode() || priorityCheckout()) && !this.state.isPreOrderItem) {
        availableOption = styleOptions.filter((option) => {
          if (at(option, 'selectedSeller.sellableInventoryCount')) {
            return (option.skuId === selectedSkuid);
          }
          return false;
        })[0];
      }
      if (availableOption) {
        this.setState({
          selectedSkuid,
          showSelectSizeError: false,
          checkSizeServiceability: true,
          promiseDate: null
        },
          () => {
            this.getPromiseDate();
            callback;
          }
        );
        Track.event('pdp', 'size_select', `${availableOption.label} | ${sellerPartnerId} | ${get(this.state, 'data.id')}`);
      } else {
        const pdpData = at(window, '__myx.pdpData');
        const entityDetails = {
          'entity_id': at(pdpData, 'id'),
          'entity_type': 'product',
          'entity_name': at(pdpData, 'name'),
          'entity_optional_attributes': {
            price: at(pdpData, 'mrp'),
            discount: at(skuData, 'selectedSeller.discount'),
            discountedPrice: at(skuData, 'selectedSeller.discountedPrice'),
            article_type: at(pdpData, 'analytics.articleType'),
            gender: at(pdpData, 'analytics.gender'),
            master_category: at(pdpData, 'analytics.masterCategory'),
            sub_category: at(pdpData, 'analytics.subCategory')
          }
        };
        TrackMadalytics.sendEvent('broken size clicked', 'pdp', entityDetails);
      }
    } else {
      this.setState({ checkSizeServiceability: false });
    }
  }

  selectProfile(e) {
    const id = e.currentTarget.id;

    const skuData = (get(this.state, "data.sizes") || []).find(
      size => size.skuId === this.state.selectedSkuid
    );
    const selectedSeller = get(skuData, 'selectedSeller') || get(this.state, 'data.selectedSeller');

    if (id === 'nil') {
      this.setState({
        showReco: false,
        profilesShown: false
      });
    } else {
      const pdpData = at(window, '__myx.pdpData');
      const entityDetails = {
        'entity_id': at(pdpData, 'id'),
        'entity_type': 'product',
        'entity_name': at(pdpData, 'name'),
        'entity_optional_attributes': { price: at(pdpData, 'mrp'), discountedPrice: at(selectedSeller, 'discountedPrice') }
      };
      // Send Event
      TrackMadalytics.sendEvent('SizeProfileSelect', 'react', entityDetails, { custom: { v1: id } });

      this.setState({
        recoLoading: true,
        profilesShown: false,
        selectedProfile: id
      });

      saveInSession('profileSelected', id);

      this.fetchSizeReco(at(window, '__myx.pdpData'), id);
    }
  }

  showProfiles() {
    if (at(window, '__myx_deviceData__.isMobile') || (at(window, '__myx_deviceType__')) === 'mobile') {
      this.setState({
        profilesShown: true
      });
    }
  }

  _sendAnalytics(dStyleData) {
    if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
      window.Madalytics.send('ScreenLoad', {
        type: 'PDP',
        url: window.location.href,
        variant: 'web',
        name: `Shopping Page-PDP${window.location.pathname}`,
        'data_set': {
          'entity_name': at(dStyleData, 'name'),
          'entity_type': 'product',
          'entity_id': at(this.props, 'params.id'),
          'entity_optional_attributes': {
            "seller_partner_id": `${get(dStyleData, 'sellerPartnerId')}`
          },
          category: at(this.props, 'params.category'),
          brand: at(this.props, 'params.brand'),
          style: at(this.props, 'params.style')
        }
      });
    }
  }

  trackGTM(styleData) {
    const dStyleData = {};
    if (styleData) {
      const articleType = at(styleData, 'analytics.articleType');
      const name = at(styleData, 'name');
      const brand = at(styleData, 'analytics.brand');
      const id = at(styleData, 'id');
      const nameForUrl = isString(name) && name.replace(/ /g, '-');
      let landingPageUrl = `${articleType}/${brand}/${nameForUrl}/${id}/buy`;
      landingPageUrl = isString(landingPageUrl) && landingPageUrl.toLowerCase();

      dStyleData.sellerPartnerId = get(styleData, 'selectedSeller.sellerPartnerId');
      dStyleData.productArticleType = articleType;
      dStyleData.productBrandName = brand;
      dStyleData.productCategory = at(styleData, 'analytics.masterCategory');
      dStyleData.productCleanURL = landingPageUrl;
      dStyleData.productDiscount = at(styleData, 'selectedSeller.discount.discountPercent');
      dStyleData.productDisplayName = name;
      dStyleData.productInStock = at(styleData, 'sizes'); // call a function to check if it has available: true
      dStyleData.productPrice = at(styleData, 'mrp');
      dStyleData.productPriceAfterDiscount = at(styleData, 'selectedSeller.discountedPrice');
      dStyleData.productStyleId = id;

      const album = at(styleData, 'media.albums.0');
      const imageSource = at(album, 'images.0');

      dStyleData.productSmallImage = getProductImageUrl(imageSource, {
        width: 48,
        height: 64,
        q: 100
      });

      dStyleData.productBigImage = getProductImageUrl(imageSource, {
        width: 1080,
        height: 1440,
        q: 100
      });
    }
    const dataLayerContent = at(window, 'dataLayer') || [];
    dataLayerContent.forEach((value, index) => {
      if (Object.keys(value).indexOf('productStyleId') !== -1) {
        dataLayerContent.splice(index, 1);
      }
    });
    dataLayerContent.push(dStyleData);
    if (isBrowser()) {
      window.dataLayer = dataLayerContent;
      if (at(window, '__myx_session__')) {
        this._sendAnalytics(dStyleData);
      } else {
        bus.on('beacon-data', () => {
          setTimeout(() => {
            this._sendAnalytics(dStyleData);
          }, 0);
        });
      }
    }
  }

  loadProfiles(data) {
    if (!at(window, '__myx_session__')) {
      if (localStorage.getItem('lscache-beacon:user-data')) {
        this.fetchSizeProfiles(data);
      } else {
        bus.on('beacon-data', () => {
          setTimeout(() => {
            this.fetchSizeProfiles(data);
          }, 0);
        });
      }
    } else {
      this.fetchSizeProfiles(data);
    }
  }

  getPreSelectedSkuId() {
    const data = get(window, '__myx.pdpData');
    let skuIdfromUrl = parseInt(get(this.props, 'location.query.skuId'), 10);
    const skuIdAvailable = get(
      (get(data, "sizes") || []).find(size => size.skuId === skuIdfromUrl),
      "available"
    );
    let selectedSkuid = null;
    if (skuIdAvailable && skuIdfromUrl) {
      selectedSkuid = skuIdfromUrl;
    }
    // Auto Select for OneSize Item
    if (!selectedSkuid) {
      const styleOptions = get(data, 'sizes');
      selectedSkuid = ((styleOptions && styleOptions.length === 1 && get(styleOptions, '0.available')) ? styleOptions[0].skuId : null);
    }
    return selectedSkuid;
  }

  setData() {
    const data = get(window, '__myx.pdpData');
    const skuIdfromUrl = parseInt(get(this.props, 'location.query.skuId'), 10);
    const sellerPartnerId = new URLSearchParams(window.location.search).get(
      "sellerPartnerId"
    );
    const selectedSkuid = this.getPreSelectedSkuId();

    if (data) {
      data.selectedSeller = getDefaultSellerInfo(data, Number(sellerPartnerId), Number(skuIdfromUrl));
      setSelectedSeller(data, data.selectedSeller, Number(skuIdfromUrl));
    }

    const isPreOrderItem = at(data, 'preOrder') || false;

    if (data) {
      this.trackGTM(data);
      this.setState({
        selectedSkuid,
        data,
        isLoading: false,
        showSizeChart: false,
        showCouponBar: false,
        isPreOrderItem
      }, this.updateXceleratorTag);

      if (sizeRecommendationEnabled) {
        this.loadProfiles(data);
      } else {
        this.fetchRelated();
      }
      this.fetchCrossLink();
    }
  }

  loadData() {
    const data = at(window, '__myx.pdpData');
    const selectedSkuid = this.getPreSelectedSkuId();
    const productId = data && data.id && data.id.toString();
    if (data && (get(this.props, 'params.id') === productId)) {
      this.setData();
    } else {
      this.setState({
        isLoading: true,
        selectedSkuid,
        showSelectSizeError: false,
        showCouponBar: false,
        showSizeChart: false,
        couponData: null,
        zoom: false,
        data: null,
        isPreOrderItem: false,
        showDiscountTimer: false
      });

      Client.get(config('pdp') + at(this.props, 'params.id')).end((err, response) => {
        if (err) {
          this.setState({
            isLoading: false
          });
          this.refs.notify.error('Oops! Something went wrong. Please try again in some time.');
          return;
        }
        const responseData = at(response, 'body');
        const newStyleOptions = at(responseData, 'sizes');
        if (isBrowser()) {
          window.__myx.pdpData = responseData;
        }
        let preSelectSkuid = selectedSkuid;
        if (newStyleOptions && newStyleOptions.length === 1) {
          preSelectSkuid = at(newStyleOptions, '0.skuId');
        }
        this.setState({
          data: responseData,
          selectedSkuid: preSelectSkuid,
          isLoading: false
        }, () => {
          this.updateXceleratorTag();
          this.setData();
        });
        if (sizeRecommendationEnabled) {
          this.loadProfiles(responseData);
        } else {
          this.fetchRelated();
        }
        this.fetchCrossLink();
        this.trackGTM(at(response, 'body.data.0'));
      });
    }
  }

  handleWishlistResponse(response) {
    if (at(response, 'body.status.statusCode') === '403') {
      window.location.href = `/login?referer=${window.location.toString()}`;
      return;
    }
    if (at(response, 'body.code') === 10003 || at(response, 'body.code') === 1001) {
      this.refs.notify.error(at(response, 'body.message'));
      return;
    }

    if (at(response, 'body.meta.code') !== 200 || at(response, 'body.status.statusType') === 'ERROR') {
      if (at(response, 'body.status') !== 200) {
        this.refs.notify.error('Oops! Something went wrong. Please try again in some time.');
        return;
      }
    }

    addProductToWlSummaryStore(at(this.state, 'data.id'));
    this._sendWishlistAnalytics();
    this.setState({ wishlistAddActionName: 'WISHLISTED', priorityCheckoutWishlistAddActionName: 'WISHLISTED' });
  }


  addToBag(eventName, sellerPartnerId) {
    fireEvents('PDP', 3);
    const skuData = (get(this.state, "data.sizes") || []).find(
      size => size.skuId === this.state.selectedSkuid
    );
    sellerPartnerId = sellerPartnerId ||
      Number(get(skuData, 'selectedSeller.sellerPartnerId')) ||
      Number(get(this.state, 'data.selectedSeller.sellerPartnerId'));
    if (this.state.selectedSkuid) {
      const parameters = {
        skuid: Number(this.state.selectedSkuid),
        styleid: this.state.data.id,
        xsrf: at(window, '__myx_session__.USER_TOKEN'),
        sellerPartnerId,
        quantity: 1
      };

      const styleOption = this.state.data.sizes.filter((option) => option.skuId === this.state.selectedSkuid)[0];
      const pdpData = at(window, '__myx.pdpData');
      Track.event('pdp', 'add_to_cart_button', `${styleOption.label} | ${sellerPartnerId} | ${get(this.state, 'data.id')}`);
      TrackMadalytics.sendEvent(eventName, 'react', {
        'entity_id': at(pdpData, 'id'),
        'entity_type': 'product',
        'entity_optional_attributes': {
          discountedPrice: at(skuData, 'selectedSeller.discountedPrice'),
          price: at(pdpData, 'mrp'),
          'sku-id': `${this.state.selectedSkuid}`,
          "seller_partner_id": `${sellerPartnerId}`
        }
      }, { custom: { v1: this.state.selectedProfile, v2: at(this, 'state.recoTextObj.recommendedSkuId') } });

      const gender = at(this.state, 'data.analytics.gender');
      const masterCategory = at(this.state, 'data.analytics.masterCategory');
      const articleType = at(this.state, 'data.analytics.articleType');

      Track.ecommerce('ec:addProduct', {
        id: at(this.props, 'params.id'),
        name: at(this.state, 'data.name') || at(this.state, 'data.brand.name'),
        category: `${gender}/${masterCategory}/${articleType}`,
        brand: at(this.state, 'data.brand.name'),
        variant: this.state.selectedSkuid,
        mrp: at(this.state, 'data.mrp'),
        quantity: 1,
        price: at(skuData, 'selectedSeller.discountedPrice') || at(this.state, 'data.mrp')
      });

      this.setState({
        isLoading: true
      });

      const endPointUrl = config('cart');
      const headers = { 'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN') };

      Client.post(endPointUrl, parameters, headers).end((err, response) => {
        const errMsg = 'Oops! Something went wrong. Please try again in some time.';

        if (Client.errorHandler(err, response)) {
          let message = '';

          if (
            priorityCheckout() &&
            at(response, 'body.meta.statusCode') === CART_FULL_ERROR_CODE
          ) {
            message = PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE;
          }

          message =
            message ||
            at(response, 'body.meta.errorDetail') || errMsg;

          this.refs.notify.error(message);

          this.setState({
            isLoading: false
          });
        } else {
          const skuidSizeAddedToBag = this.state.sizesAddedToBag;
          if (skuidSizeAddedToBag.indexOf(parameters.skuid) === -1) {
            skuidSizeAddedToBag.push(parameters.skuid);
          }
          this.setState({
            sizesAddedToBag: skuidSizeAddedToBag,
            isLoading: false,
            showSlotPopup: isShowSlotPopup()
          });

          const albums = at(this.state, 'data.media.albums.0');
          const styleImage = at(albums, 'images.0');
          const imgConfig = {
            width: 48,
            height: 64,
            q: 100
          };

          const eventPayload = {
            res: response,
            skuid: parameters.skuid,
            productImage: getProductImageUrl(styleImage, imgConfig)
          };

          bus.emit('cart.add', eventPayload);
        }
      });
    } else {
      this.setState({
        showSelectSizeError: true
      });
      Track.event('pdp', 'add_to_cart_button', null);
    }
  }

  addToWishlist(type) {
    fireEvents('PDP', 4);
    if (type === 'wishlist' && this.state.wishlistAddActionName === 'WISHLISTED') {
      return;
    }
    if (this.state.selectedSkuid || type === 'wishlist') {
      if (at(window, '__myx_session__.isLoggedIn') || type === 'cart') {
        const sellerPartnerId = get(this.state.data, 'selectedSeller.sellerPartnerId');
        let parameters = {};
        let headers = {};
        let endPointUrl = config(type);
        if (type === 'wishlist') {
          endPointUrl = '/web/wishlistapi/addition';
          parameters = {
            styleId: at(this.state, 'data.id'),
            sellerPartnerId
          };
          headers = { 'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN') };
          if (this.state.selectedSkuid) {
            parameters.skuId = this.state.selectedSkuid;
          }
          Track.event('wishlist', 'add_to_wishlist', `${sellerPartnerId} | ${get(this.state, 'data.id')}`);
        }
        this.setState({
          isLoading: true
        });

        Client.post(endPointUrl, parameters, headers).end((err, response) => {
          let errMsg = 'Oops! Something went wrong. Please try again in some time.';
          if (err) {
            this.setState({
              isLoading: false
            });

            if (at(response, 'body.status.statusCode') === '403' && type === 'wishlist') {
              window.location.href = `/login?referer=${window.location.toString()}`;
              return;
            }

            this.refs.notify.error(errMsg);
            return;
          }

          if (response && at(response, 'body.error')) {
            this.setState({
              isLoading: false
            });
            try {
              errMsg = at(JSON.parse(at(response, 'body.error.response.text')), 'status.statusMessage');
              const errCode = at(JSON.parse(at(response, 'body.error.response.text')), 'status.statusCode');
              if (priorityCheckout() && errCode === CART_FULL_ERROR_CODE) {
                errMsg = PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE;
              }
            } catch (e) {
              // No Console in prod code.
            }
            this.refs.notify.error(errMsg);
            return;
          }
          if (!err && !at(response, 'body.error') && type === 'cart') {
            const skuidSizeAddedToBag = this.state.sizesAddedToBag;
            if (skuidSizeAddedToBag.indexOf(parameters.skuid) === -1) {
              skuidSizeAddedToBag.push(parameters.skuid);
            }
            this.setState({
              sizesAddedToBag: skuidSizeAddedToBag
            });
          }
          this.setState({
            isLoading: false
          });

          const albums = at(this.state, 'data.media.albums.0');
          const styleImage = at(albums, 'images.0');
          const imgConfig = {
            width: 48,
            height: 64,
            q: 100
          };

          const eventPayload = {
            res: response,
            skuid: parameters.skuid,
            productImage: getProductImageUrl(styleImage, imgConfig)
          };

          if (type === 'wishlist') {
            this.handleWishlistResponse(response);
          }

          const showSlotPopup = isShowSlotPopup() && type === 'wishlist';
          if (!showSlotPopup) {
            bus.emit(`${type}.add`, eventPayload);
          }

          this.setState({
            showSlotPopup
          });
        });
      } else {
        setItem('pdpAutoWishlist', 'wishlist');
        window.location.href = `/login?referer=${window.location.toString()}`;
      }
    } else {
      this.setState({
        showSelectSizeError: true
      });
      Track.event('pdp', 'add_to_cart_button', null);
    }
  }

  error(message) {
    if (message) {
      this.refs.notify.error(message);
    }
  }

  preOrderItemScarcityCheck() {
    const styleOptions = at(this.state, 'data.sizes');
    const isPreOrderItem = this.state.isPreOrderItem;
    const scarcityUpperBound = at(this.state, 'data.preOrder.scarcity') || 10;
    if (isPreOrderItem) {
      // loop over styleOptions array, and find max inventory count.
      let maxPreOrderInventoryCount = 0;
      for (let i = 0; i < styleOptions.length; i++) {
        const thisStyle = styleOptions[i];
        if (thisStyle.inventory && thisStyle.inventory > maxPreOrderInventoryCount) {
          maxPreOrderInventoryCount = thisStyle.inventory;
        }
      }

      // For Pre order items => Range for showingscarcity threshold is  (0, 10) and is always shown.
      if (maxPreOrderInventoryCount > 0 && maxPreOrderInventoryCount <= scarcityUpperBound && !isOutOfStock(this.state)) {
        return (
          <span className={styles['preorder-scarcity']}>
            Only {maxPreOrderInventoryCount} {maxPreOrderInventoryCount > 1 ? ' items' : ' item'} left
          </span>
        );
      }
    }
    return null;
  }

  launchDateForPreOrderItem() {
    if (this.state.isPreOrderItem) {
      const launchDate = at(this.state, 'data.preOrder.launchDate') || null;
      return (
        <div
          className={styles['preorder-launch-date']}>
          {
            launchDate
              ? (<div>Launching on <span className={styles['preorder-date']}> {new Date(parseInt(launchDate, 10)).toDateString()} </span></div>)
              : (null)
          }
        </div>
      );
    }
    return null;
  }

  preOrderDisclaimerText() {
    if (this.state.isPreOrderItem) {
      let disclaimerText = at(this.state, 'data.preOrder.disclaimerText') || 'Limited Edition';
      return (
        <div className={styles['preorder-disclaimer-text']}>
          {disclaimerText}
        </div>
      );
    }
    return null;
  }
  showSizeChart() {
    this.setState({
      showSizeChart: true
    });
    document.body.style.overflow = 'hidden';
    Track.event('PDP', 'SizeChart', `SizeChartOpen | ${at(window, '__myx.pdpData.id')}`);
  }

  hideSaleSlot() {
    this.setState({
      showSlotPopup: false
    });
    document.body.style.overflow = 'auto';
  }

  hideSizeChart() {
    this.setState({
      showSizeChart: false
    });
    document.body.style.overflow = 'auto';
  }

  showMoreColors() {
    this.setState({
      showMoreColors: true
    });
  }

  fetchRelated(recommendedSize) {
    if (recommendationEnabled) {
      const styleId = at(this.props, 'params.id');
      let url = `${config('recommendations')}/${styleId}`;
      url = recommendedSize ? `${url}/${recommendedSize}` : `${url}`;
      Client.get(url)
        .end((err, response) => {
          if (err) {
            return;
          }
          if (at(response, 'body.related.length')) {
            const related = keyBy(at(response, 'body.related'), 'type');
            const similarItems = at(related, 'Similar.products') || [];
            this.setState({
              related,
              similarItemsAvailable: similarItems.length > 0
            });
          }
        });
    }
  }

  fetchCrossLink() {
    if (crossSellEnabled) {
      const styleId = at(this.props, 'params.id');
      const url = `${config('crossSell')}/${styleId}`;
      Client.get(url).end((err, response) => {
        if (err) {
          return;
        }

        if (at(response, 'body.related.length')) {
          const crossSell = keyBy(at(response, 'body.related'), 'type');
          this.setState({
            crossSell
          });
        }
      });
    }
  }

  showSimilarProducts(sizeChart) {
    const similarProductsContainer = at(this, 'refs.similarComp.refs.simContainer');
    if (sizeChart) {
      this.hideSizeChart();
    }
    if (similarProductsContainer) {
      const header = document.getElementById('desktop-header-cnt');
      let scrollDist = similarProductsContainer.getBoundingClientRect().top + pageYOffset;
      if (header) {
        scrollDist -= header.clientHeight;
      }
      scroll(scrollDist);
    }
  }

  showServiceabilityHalfCard(type) {
    document.body.style.overflow = 'hidden';
    this.setState({
      serviceabilityHalfCard: type
    });
  }

  hideServiceabilityHalfCard() {
    document.body.style.overflow = '';
    this.setState({
      serviceabilityHalfCard: null
    });
  }

  showCrossSellProducts() {
    const crossSellProductsContainer = at(this, 'refs.crossSell.refs.crossContainer');

    if (crossSellProductsContainer) {
      const header = document.getElementById('desktop-header-cnt');
      let scrollDist = crossSellProductsContainer.getBoundingClientRect().top + pageYOffset;
      if (header) {
        scrollDist -= header.clientHeight;
      }
      scroll(scrollDist);
    }
  }

  fetchSizeReco(pdpData, profileId) {
    const payload = this.getSizeRecoPayload(pdpData, profileId);
    const fetchRecoPath = at(pdpData, 'sizeRecoLazy.action').slice(1);
    const availableSkuIds = (at(pdpData, 'sizes') || []).filter(size => size.available).map(size => size.skuId);
    Client.post(`${config('sizeReco')}${fetchRecoPath}`, payload)
      .end((err, resp) => {
        if (err) {
          this.fetchRelated();
          return;
        }
        const entityDetails = {
          'entity_id': at(pdpData, 'id'),
          'entity_type': 'product',
          'entity_name': at(pdpData, 'name')
        };
        const recoTextObj = at(resp, 'body');

        // Send event
        TrackMadalytics.sendEvent('RecommendedSize', 'react', entityDetails, {
          custom: {
            v1: `${at(recoTextObj, 'title')} | ${at(recoTextObj, 'description')}`,
            v2: profileId,
            v3: at(recoTextObj, 'level'),
            v4: at(recoTextObj, 'recommendedSkuId')
          }
        });

        this.setState({
          recoTextObj,
          recoLoading: false
        }, () => {
          const recommendedSkuId = at(recoTextObj, 'recommendedSkuId');
          if (recommendedSkuId && availableSkuIds.indexOf(recommendedSkuId) !== -1) {
            this.setState({
              selectedSkuid: recommendedSkuId
            });
            // Get Correspoding label
            const sizes = at(pdpData, 'data.sizes') || [];
            const recommedSkuFromSize = sizes.filter(item => item.skuId === recommendedSkuId);
            if (recommedSkuFromSize.length > 0) {
              return this.fetchRelated(at(recommedSkuFromSize[0], 'label'));
            }
            return this.fetchRelated();
          }
          return this.fetchRelated();
        });
      });
  }

  fetchSizeProfiles(data) {
    const fetchProfilesPath = at(data, 'sizeRecoLazy.sizeProfileAction');
    if (fetchProfilesPath) {
      const currentProfile = getFromSession('profileSelected');
      let selectedProfile;
      Client.get(`${config('sizeReco')}${fetchProfilesPath.slice(1)}`).end((err, response) => {
        if (err) {
          this.fetchSizeReco(data);    // fetch fallback recommendation
          return;
        }
        const profiles = at(response, 'body.data.sizeProfile.profileList');
        if (profiles) {
          if (profiles.findIndex(elem => (elem.pidx === currentProfile)) !== -1) {
            selectedProfile = currentProfile;
          } else {
            selectedProfile = at(profiles, '0.pidx') || '';
          }
        } else {
          selectedProfile = '';
        }

        this.setState({
          sizeProfiles: profiles,
          selectedProfile
        });
        this.fetchSizeReco(data, selectedProfile);
      });
    } else {
      this.setState({
        showReco: false
      }, () => {
        this.fetchRelated();
      });
    }
  }

  toggleMobileImageZoom() {
    this.setState({
      mobileImageZoom: !this.state.mobileImageZoom
    });
  }

  toggleRequestLoader() {
    this.setState({
      isLoading: !this.state.isLoading
    });
  }

  toggleEmiPlans(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      showEmiPlans: !this.state.showEmiPlans
    }, () => {
      if (this.state.showEmiPlans) {
        document.body.style.overflowY = 'hidden';
      } else {
        document.body.style.overflowY = 'auto';
        document.body.style.position = 'static';
      }
    });
  }

  personalisedCouponInfo() {
    const data = this.state.data || {};
    // Data not present in V2 PdpData
    const coupon = at(data, 'price.personalizedCoupon');
    return coupon ? (
      <div className={styles.PPCouponInfo}>{`Pre-applied Deal - ${coupon}`}</div>
    ) : null;
  }

  getPromiseDate(pincode, PostFilterEnablecheckServiceability = true) {
    if (!pincode) {
      pincode = localStorage.getItem("pincode") || null;
    }
    let selectedSkuId = this.state.selectedSkuid;
    if (!selectedSkuId) {
      selectedSkuId = getDefaultSkuSeller(this.state.data).skuId;
    }
    const selectedSkuData = (get(this.state, "data.sizes") || []).find(
      ({ skuId }) => skuId === selectedSkuId
    );
    const selectedSellerPartnerId = get(
      selectedSkuData,
      "selectedSeller.sellerPartnerId"
    );
    const sellerServiceable = get(
      selectedSkuData,
      "selectedSeller.serviceable"
    );
    if (
      pincode &&
      selectedSkuId &&
      sellerServiceable &&
      (!this.state.promiseDate)
    ) {
      serviceabilityAPI
        .checkV2(
          this.state.data,
          [{ skuId: selectedSkuId, sellerPartnerId: selectedSellerPartnerId }],
          pincode
        )
        .then(serviceability => {
          const selectedServiceability =
            get(
              (get(serviceability, "itemServiceabilityEntries") || []).find(
                itemEntry => {
                  return (
                    Number(itemEntry.skuId) === selectedSkuId &&
                    Number(itemEntry.itemReferenceId) ===
                    selectedSellerPartnerId
                  );
                }
              ),
              "serviceabilityEntries"
            ) || [];
          const promiseDate = get(
            selectedServiceability.find(
              s =>
                s.serviceType === "DELIVERY" || s.serviceType === "TRY_AND_BUY"
            ),
            "promiseDate"
          );
          this.setState({
            promiseDate,
            checkSizeServiceability: PostFilterEnablecheckServiceability
          });
        });
    }
  }

  render() {
    if (this.state.data) {
      const imageZoom = this.state.zoom || this.state.showSizeChart;
      const pdpBaseClasses = pdpBindClassNames({
        'pdp-zoom-container': imageZoom,
        'pdp-container': true,
        mobileImageZoom: this.state.mobileImageZoom
      });
      const { titleTag: { data: titleTag = '' } = {} } = this.state.xceleratorTag;
      const globalCardText = ('globalstore.cardtext');
      const isGlobal = at(window, '__myx.pdpData.flags.globalStore');
      return (
        <div key={at(this.state, 'data.id')}>
          <main
            className={pdpBaseClasses}>
            <BreadCrumbs data={this.state.data} />
            <div className={`${styles.details} ${commonUtilityStyles.clearfix}`}>
              <ImageGrid
                data={this.state.data}
                xceleratorTag={this.state.xceleratorTag}
                similarItemsAvailable={this.state.similarItemsAvailable}
                showSimilarProducts={this.showSimilarProducts}
                onZoom={this.onZoom} />
              <ChatBot />
              <Bruce />
              <div className={styles['description-container']}>
                <div className={styles['price-info']}>
                  {this.getBrandMarkup()}
                  {this.getDescMarkup()}
                  {ratingsEnabled && !this.isFastFashion() ? <OverallRating data={this.state.data.ratings} /> : null}
                  {this.getDiscountContainer()}
                  <DiscountTimer
                    selectedSkuid={this.state.selectedSkuid}
                    data={this.state.data} />
                  {this.personalisedCouponInfo()}
                  <p className={styles['selling-price']}>
                    {this.getLoyaltyHtml()}
                    {this.preOrderItemScarcityCheck()}
                    {this.getAdditionalMessage()}
                  </p>
                  {this.launchDateForPreOrderItem()}
                  {this.preOrderDisclaimerText()}
                  {titleTag !== '' && (
                    <XceleratorTag page={'pdp'} tag={titleTag} position={'inInfo'} />
                  )}
                </div>
                {isGlobal && globalCardText && (<div className={styles['global-product']}>
                  <p className={styles.globalHeader}><strong>Global Product</strong></p>
                  <div className={styles.globalDesc}>
                    <p className={styles.globalSubText}> {globalCardText.subText}</p>
                    {globalCardText.points.map((point, index) => (
                      <p className={pdpBindClassNames(styles.globalPoint, `gp-${index}`)}>{point}</p>
                    ))}
                    <a href="/faqs" className={styles.globalReadMore}>Read More</a>
                  </div>
                </div>)}
                <Colors
                  data={this.state.data}
                  showMoreHandler={this.showMoreColors}
                  showMoreColors={this.state.showMoreColors} />
                <SizeButtons
                  data={this.state.data}
                  selectedSkuid={this.state.selectedSkuid}
                  showSelectSizeError={this.state.showSelectSizeError}
                  showSizeChart={this.showSizeChart}
                  showReco={this.state.showReco}
                  recoTextObj={this.state.recoTextObj}
                  recoLoading={this.state.recoLoading}
                  sizeProfiles={this.state.sizeProfiles}
                  selectProfile={this.selectProfile}
                  selectedProfile={this.state.selectedProfile}
                  relatedProducts={!isEmptySimilar(this.state.related)}
                  profilesShown={this.state.profilesShown}
                  showSimilarProducts={this.showSimilarProducts}
                  showProfiles={this.showProfiles}
                  onLoginClick={this.onLoginClick}
                  trackLoginEvent={this.trackLoginEvent}
                  selectSize={this.selectSize}
                  addToBagCallback={this.addToBag} />
                <ActionCTA
                  stylesCTA={styles}
                  state={this.state}
                  systemAttributes={at(window, '__myx.pdpData.systemAttributes')}
                  addToBag={this.addToBag}
                  addToWishlist={this.addToWishlist} />
                {this.getReturnable()}
                {!at(this.state, "data.flags.outOfStock") && <SelectedSizeSellerInfo
                  data={this.state.data}
                  addToBag={this.addToBag}
                  promiseDate={this.state.promiseDate}
                  getPromiseDate={this.getPromiseDate}
                  serviceabilityCallDone={this.state.serviceabilityCallDone}
                  selectedSkuid={this.state.selectedSkuid} />}

                <Pincode
                  data={this.state.data}
                  onError={this.error}
                  promiseDate={this.state.promiseDate}
                  serviceable={this.state.serviceable}
                  getPromiseDate={this.getPromiseDate}
                  handlePincode={this.isPincodeEntered}
                  serviceabilityCallDone={this.state.serviceabilityCallDone}
                  showServiceabilityHalfCard={this.showServiceabilityHalfCard}
                  filterUnserviceableSellers={this.filterUnserviceableSellers}
                  checkSizeServiceability={at(this.state, 'checkSizeServiceability')}
                  pincodeEntered={this.state.pincodeEntered}
                  selectedSkuid={this.state.selectedSkuid ? this.state.selectedSkuid : null} />

                <Meta
                  data={this.state.data}
                  pincodeEntered={this.state.pincodeEntered} />

                <Offers
                  notify={this.refs.notify}
                  isPreOrderItem={this.state.isPreOrderItem}
                  toggleRequestLoader={this.toggleRequestLoader}
                  toggleEmiPlans={this.toggleEmiPlans}
                  emiPlans={emiPlans}
                  selectedSkuid={this.state.selectedSkuid}
                  data={this.state.data} />
                <ProductDescriptors data={this.state.data} />

                {ratingsEnabled && !this.isFastFashion() ? (<DetailedReviews {...this.state.data.ratings} onZoom={this.onZoom} />) : null}

                <Supplier data={this.state.data} />

              </div>
              {recommendationEnabled && (
                <Similar ref="similarComp" data={this.state.data} related={this.state.related} />
              )}

              <CrossLinks data={this.state.data} />

              {crossSellEnabled && (
                <CrossSell ref="crossSell" data={this.state.data} related={this.state.crossSell} />
              )}

            </div>

            {this.state.showSizeChart && (
              <SizeChart
                show={this.state.showSizeChart}
                serviceabilityCallDone={this.state.serviceabilityCallDone}
                showSizeChart={this.state.showSizeChart}
                hide={this.hideSizeChart}
                data={this.state.data}
                addToBagCallback={this.addToBag}
                skuidsAddedToBag={this.state.sizesAddedToBag}
                selectedSkuid={this.state.selectedSkuid}
                selectSize={this.selectSize}
                state={this.state}
                addToWishlist={this.addToWishlist}
                showReco={this.state.showReco}
                recoTextObj={this.state.recoTextObj}
                recoLoading={this.state.recoLoading}
                sizeProfiles={this.state.sizeProfiles}
                selectProfile={this.selectProfile}
                selectedProfile={this.state.selectedProfile}
                relatedProducts={!isEmptySimilar(this.state.related)}
                profilesShown={this.state.profilesShown}
                showSimilarProducts={this.showSimilarProducts}
                showProfiles={this.showProfiles}
                onLoginClick={this.onLoginClick}
                trackLoginEvent={this.trackLoginEvent}
                addToBag={this.addToBag} />
            )}
            <Notify ref="notify" />
            <Loader show={this.state.isLoading} />
          </main>
          <AdmissionControl
            showSlotPopup={this.state.showSlotPopup}
            hideSaleSlot={this.hideSaleSlot}
            slots={getSlots(this.state.showSlotPopup)} />
          {this.state.showEmiPlans && (
            this.getEmiPlanModal()
          )}
          {this.state.serviceabilityHalfCard &&
            <DeliveryHalfCard
              onToggle={this.hideServiceabilityHalfCard}
              type={this.state.serviceabilityHalfCard} />
          }
        </div>
      );
    }
    if (this.state.isLoading) {
      return (
        <div className={styles['pdp-container']}>
          <Notify ref="notify" />
          <Loader show={this.state.isLoading} />
        </div>
      );
    }
    return (
      <div className={styles['error-message']}>Oops! Something went wrong. Please try again in some time.</div>
    );
  }
}

Pdp.propTypes = {
  data: React.PropTypes.object,
  location: React.PropTypes.object
};

Pdp.defaultProps = {
  data: null
};

export default Pdp;
