import React from 'react';
import styles from './colors.css';
import ColorUtils from '../../../utils/colors';
import get from 'lodash/get';
import classNames from 'classnames/bind';
import { securify } from '../../../utils/securify';
import { getFeatures } from '../../../utils';
import Client from '../../../services';
import config from '../../../config';
const secure = securify();

const bindClassNames = classNames.bind(styles);

class Colors extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      availableColors: [],
      fetching: false
    };

    this.fetchRelated = this.fetchRelated.bind(this);
    this.toggleFetching = this.toggleFetching.bind(this);
    this.filterOOSColorItems = this.filterOOSColorItems.bind(this);
  }

  componentDidMount() {
    const colors = get(this.props.data, 'colours', null);
    if (colors && colors.length) {
      this.fetchRelated().then(this.filterOOSColorItems).catch(error => {
        console.error(`COLORS API ERROR: ${error}`);
      });
    }
  }

  getColorThumbs() {
    const colors = get(this.state, 'availableColors', []);
    const title = colors.length > 1 ? 'More Colors' : 'More Color';

    if (!colors.length) {
      return null;
    }

    return (
      <div className={styles.thumbs}>
        <p className={styles.heading}><strong>{title}</strong></p>
        {
          colors.map((colorInfo, id) => {
            const color = colorInfo.label;
            const colorCode = (color && ColorUtils[color.toLowerCase()]) || ColorUtils.grey;
            const backgroundStyle = {
              backgroundColor: colorCode
            };
            const moreColorsInProduct = colorInfo.url || colorInfo.styleId || '';
            const imgSrc = secure(colorInfo.image).replace('assets.myntassets.com', 'assets.myntassets.com/f_auto,h_150,q_auto:best,w_112');
            return (
              <a
                key={id}
                title={color || ''}
                href={`/${moreColorsInProduct.toLowerCase()}`}>
                <img
                  className={styles.image}
                  style={backgroundStyle}
                  src={imgSrc} />
              </a>
            );
          })
        }
      </div>
    );
  }

  getShadeGroup() {
    let relatedStyles = get(this.props, 'data.relatedStyles');
    let showMoreButton = null;
    const productId = get(this.props, 'data.id');
    const totalRelatedStyles = relatedStyles.length;

    const shadeListClass = bindClassNames({
      shadeList: true,
      shadeListDesktop: true
    });

    if (relatedStyles.length > 6 && !this.props.showMoreColors) {
      relatedStyles = relatedStyles.slice(0, 5);
      showMoreButton = (
        <div className={styles.shade} onClick={this.props.showMoreHandler}>
          <div className={`${styles.imageContainer} ${styles.moreImageContainer}`}>
            {
              relatedStyles.slice(0, 4).map((product, id) => (
                <div key={id} className={styles.grid}>
                  <img src={secure(product.imageUrl)} />
                </div>
              ))
            }
          </div>
          <span className={styles.shadeName}>View more</span>
        </div>
      );
    }

    return (
      <div>
        <p className={styles.heading}>
          <span className={styles.shadeTitle}>Available in <strong>{totalRelatedStyles} Shades</strong></span>
        </p>
        <ul className={shadeListClass}>
          {
            relatedStyles.map((product, id) => (
              <li key={id}>
                <a className={styles.shade} key={`c-${id}`} title={product.displayText || ''} href={`/${product.styleId}`}>
                  <div className={styles.imageContainer}>
                    <img src={secure(product.imageUrl)} />
                    {productId === product.styleId ? <div className={`myntraweb-sprite ${styles.selected}`} /> : null}
                  </div>
                  <span className={styles.shadeName}>{product.displayText}</span>
                </a>
              </li>
            ))
          }
          {showMoreButton}
        </ul>
      </div>
    );
  }

  fetchRelated() {
    if (getFeatures('pdp.recommendation.enable') === 'true') {
      this.toggleFetching();
      const URL = `${config('recommendations')}/${get(this.props, 'data.id')}/?colors=true`;
      return new Promise((resolve, reject) => {
        Client.get(URL).end((error, res) => {
          this.toggleFetching(false);
          if (error) {
            return reject(error);
          }
          const data = get(res, 'body.related', []);
          const { products = [] } = data.find(({ type = '' }) => type.toLowerCase() === 'colourvariants');
          return resolve(products);
        });
      });
    }
  }

  toggleFetching(flag = true) {
    this.setState({ fetching: flag });
  }

  filterOOSColorItems(products = []) {
    const { colours = [] } = get(this.props, 'data');
    const hasProductWith = styleId => products.find(({ id }) => id === styleId)
    const availableColors = colours.filter(({ styleId }) => hasProductWith(styleId))
    this.setState({ availableColors });
  }

  hasColors() {
    const colours = get(this.props, 'data.colours');
    return colours && colours.length;
  }

  hasShadeGroup() {
    const relatedStyles = get(this.props, 'data.relatedStyles');
    return relatedStyles && relatedStyles.length > 0;
  }

  render() {
    const hasColors = this.hasColors();
    const hasShadeGroup = this.hasShadeGroup();

    if ((hasColors || hasShadeGroup) && !this.state.fetching) {
      return (
        <div className={styles.container}>
          {hasShadeGroup ? (
            this.getShadeGroup()
          ) : (
            this.getColorThumbs()
          )}
        </div>
      );
    }
    return null;
  }
}

Colors.propTypes = {
  data: React.PropTypes.object.isRequired,
  showMoreColors: React.PropTypes.bool,
  showMoreHandler: React.PropTypes.func
};

export default Colors;
