import React from 'react';
import at from 'v-at';

import styles from './breadcrumbs.css';
import { stripSpecialChars } from '../../Header/helpers';

function getLinkURL(link = '') {
  return stripSpecialChars(link);
}

function BreadCrumbs(props) {
  let masterCategory = at(props, 'data.analytics.masterCategory') || '';
  if (masterCategory === 'Apparel') {
    masterCategory = 'Clothing';
  } else if (masterCategory === 'Home') {
    masterCategory = 'Home Furnishing';
  }
  const gender = at(props, 'data.analytics.gender') || '';
  const articleType = at(props, 'data.analytics.articleType') || '';
  const brandName = at(props, 'data.analytics.brand') || '';
  return (
    <div className={styles.container}>
      <a href="/" className={styles.link}>Home</a>
      <span className={styles.separator}>/</span>
      <a href={`/${getLinkURL(masterCategory)}`} className={styles.link}>{masterCategory}</a>
      <span className={styles.separator}>/</span>
      <a
        href={`/${getLinkURL(`${gender} ${masterCategory}`)}`}
        className={styles.link}>
        {`${gender} ${masterCategory}`}
      </a>
      <span className={styles.separator}>/</span>
      <a href={`/${getLinkURL(articleType)}`} className={styles.link}>{articleType}</a>
      <span className={styles.separator}>/</span>
      <a
        href={`/${getLinkURL(`${brandName} ${articleType}`)}`}
        className={styles['bold-link']}>
        {`${brandName} ${articleType}`}
      </a>
      <span className={styles['bold-separator']}>&gt;</span>
      <a href={`/${getLinkURL(brandName)}`} className={styles['bold-link']}>More by {brandName}</a>
    </div>
  );
}

BreadCrumbs.propTypes = {
  data: React.PropTypes.object.isRequired
};

export default BreadCrumbs;
