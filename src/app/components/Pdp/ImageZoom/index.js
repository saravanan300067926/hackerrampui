import React from 'react';
import Desktop from './desktop';
import { isBrowser } from '../../../utils';

function ImageZoom(props) {
  if (!isBrowser()) {
    return null;
  }
  return <Desktop {...props} />;
}

export default ImageZoom;
