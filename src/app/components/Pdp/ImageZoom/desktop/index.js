import React from 'react';
import at from 'v-at';
import { getProductImageUrl } from '../../utils';
import styles from './desktop-image-zoom.css';


class ImageZoom extends React.Component {

  constructor(props) {
    super(props);
    this.addToCart = this.addToCart.bind(this);
    this.navigate = this.navigate.bind(this);
    this.arrowKeys = this.arrowKeys.bind(this);
    this.scrollImage = this.scrollImage.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  componentDidMount() {
    document.addEventListener('keydown', this.onKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyPress);
  }

  onKeyPress = event => {
    if (event.key === 'Escape') {
      this.props.hide();
    }
  }

  getDiscount() {
    const data = this.props.data || {};
    const discounted = at(data, 'price.discounted');
    const mrp = at(data, 'price.mrp');
    if (discounted < mrp) {
      const discountText = at(data, 'price.discount.label');
      return (
        <p className={styles.discount}>
          <s className={styles.mrp}>Rs. {mrp}</s>
          <span className={styles['discount-text']}>{discountText || ''}</span>
        </p>
      );
    }
    return null;
  }

  getSelectedImage() {
    const styleImage = at(this.props, 'data.media.albums.0');
    if (styleImage) {
      const imgConfig = {
        width: 1080,
        height: 1440,
        q: 90
      };
      const selectedImage = at(styleImage, `images.${this.props.selectedImage}`);
      if (selectedImage) {
        return getProductImageUrl(selectedImage, imgConfig);
      }
    }
    return null;
  }

  getThumbnails() {
    const albums = at(this.props, 'data.media.albums.0');
    const images = albums && albums.images || [];

    if (images) {
      const imgConfig = {
        width: 52,
        height: 68,
        q: 90
      };
      return (
        <div className={styles['thumbnail-container']}>
          {
            images.map((item, index) => {
              const url = getProductImageUrl(item, imgConfig);
              const isSelected = at(this.props, 'selectedImage') === index;
              const className = isSelected ? styles['selected-thumbnail'] : styles.thumbnail;
              return (
                <button className={styles['thumbnail-button']} key={index} onClick={() => this.props.setSelectedImage(index)}>
                  <img className={className} src={url} />
                </button>
              );
            })
          }
        </div>
      );
    }
    return null;
  }

  arrowKeys(event) {
    if (event.keyCode === 37) {
      this.navigate('previous');
    } else if (event.keyCode === 39) {
      this.navigate('next');
    }
  }

  navigate(params) {
    const albums = at(this.props, 'data.media.albums.0');
    const images = at(albums, 'images');
    let currentKeyIndex = this.props.selectedImage;

    if (params === 'previous') {
      if (currentKeyIndex <= 0) {
        currentKeyIndex = images.length;
      }
      this.props.setSelectedImage(currentKeyIndex - 1);
    }

    if (params === 'next') {
      if (currentKeyIndex >= (images.length - 1)) {
        currentKeyIndex = -1;
      }
      this.props.setSelectedImage(currentKeyIndex + 1);
    }
  }

  addToCart() {
    this.props.addToBag('cart');
  }

  contentClick(event) {
    event.stopPropagation();
  }

  scrollImage(event) {
    if (this.refs.imageContainer.scrollTop >= 0) {
      this.refs.imageContainer.scrollTop = event.clientY > 200 ? event.clientY + 15 : event.clientY;
    }
  }

  render() {
    const images = at(this.props, 'data.media.albums.0.images') || [];
    const containerStyle = this.props.zoom ? styles.container : styles['hide-container'];
    return (
      <div className={containerStyle} onClick={this.props.hide} onKeyDown={this.arrowKeys} tabIndex="1">
        <div className={styles.content} onClick={this.contentClick}>
          <div className={styles['image-container']} onMouseMove={this.scrollImage} ref="imageContainer">
            <img className={styles['primary-image']} src={this.getSelectedImage()} />
          </div>
          {this.getThumbnails()}
          <button className={styles.close} onClick={this.props.hide}>
            <span className={`myntraweb-sprite ${styles['close-icon']}`}></span>
          </button>
          {images.length > 1 && (
            <div>
              <button className={styles.previous} onClick={() => this.navigate('previous')}>
                <span className={`myntraweb-sprite ${styles['previous-icon']}`}></span>
              </button>
              <button className={styles.next} onClick={() => this.navigate('next')}>
                <span className={`myntraweb-sprite ${styles['next-icon']}`}></span>
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

ImageZoom.propTypes = {
  data: React.PropTypes.object.isRequired,
  selectedImage: React.PropTypes.number.isRequired,
  setSelectedImage: React.PropTypes.func.isRequired,
  hide: React.PropTypes.func.isRequired,
  zoom: React.PropTypes.bool.isRequired,
  selectedSkuid: React.PropTypes.number,
  showSelectSizeError: React.PropTypes.bool.isRequired,
  selectSize: React.PropTypes.func.isRequired,
  addToBag: React.PropTypes.func.isRequired
};

export default ImageZoom;
