import React from 'react';
import at from 'v-at';
import getKvPair from '../../../utils/kvpairs';
import styles from './meta.css';

const EMPTY_ARR_READ_ONLY = [];

function getMinDeliveryChargeValue() {
  if (typeof window !== 'undefined') {
    const kvPairs = window.__myx_kvpairs__ || {};
    const shippingValue = kvPairs['shipping.charges.cartlimit'];
    if (shippingValue) {
      return (
        <div className={styles.info}>
          <div className={styles.desc}>Free Delivery on order above Rs. {shippingValue}</div>
        </div>
      );
    }
  }
  return null;
}

function getReturns(props) {
  const returnable = at(props, 'data.flags.isReturnable');
  const exchangeable = at(props, 'data.flags.isExchangeable');
  const isGlobal = at(props, 'data.flags.globalStore');
  if (!returnable && !exchangeable) {
    return (
      <span className={styles.info}>
        <span>This item is not returnable. Items like inner-wear, personal care, make-up, socks and certain
        accessories do not come under our return policy. </span>
        <a href="/faqs#returns" className={styles.link}>Read More.</a>
      </span>
    );
  }

  const descriptors = at(props, 'data.serviceability.descriptors') || EMPTY_ARR_READ_ONLY;
  const globalMoreInfo = isGlobal ? (getKvPair('globalstore.moreinfo') || EMPTY_ARR_READ_ONLY) : EMPTY_ARR_READ_ONLY;
  const moreInfo = [...descriptors, ...globalMoreInfo];
  if (moreInfo && moreInfo.length && at(props, 'pincodeEntered') === false) {
    return moreInfo.map((item, index) => (
      <div className={styles.info} key={index}>
        <div className={styles.desc}>{item}</div>
      </div>
    ));
  }

  return null;
}

function Meta(props) {
  return (
    <div>
      <div className={styles.container}>
        <div className={styles.info}>
          {/*<div className={styles.dot} />*/}
          <div className={styles.desc}>100% Original Products</div>
        </div>
        {getMinDeliveryChargeValue()}
        {getReturns(props)}
      </div>
    </div>
  );
}

Meta.propTypes = {
  data: React.PropTypes.object.isRequired,
  pincodeEntered: React.PropTypes.bool
};

export default Meta;
