import React from 'react';
import styles from './product-list.css';

import { Link } from 'react-router';

import Track from '../../../utils/track';
import ProductItem from '../ProductItem';
import at from 'v-at';

function ProductList(props) {
  if (at(props, 'products.length')) {
    return (
      <ul className={styles.list}>
        {
          props.products.map((product, index) =>
            <li className={styles.gist} key={index}>
              <Link
                className={styles.link}
                target="_blank"
                to={`/${(product.landingPageUrl || (product.id && product.id.toString()) || '').toLowerCase()}`}
                onClick={() => { Track.event('pdp', props.eventName, `from-${props.from || ''}__to-${product.id}`); }}>
                <ProductItem product={product} />
              </Link>
            </li>
          )
        }
      </ul>
    );
  }
  return null;
}

ProductList.propTypes = {
  products: React.PropTypes.array.isRequired,
  from: React.PropTypes.number
};

export default ProductList;
