import React from 'react';
import styles from './pincode.css';
import Track from '../../utils/track';
import at from 'v-at';
import get from "lodash/get";
import features from '../../utils/features';
import { isOutOfStock } from './utils';
import find from 'lodash/find';
import iconProvider from './Pincode/DeliveryIconProvider';
import HalfCardInteractor from './Pincode/HalfCards/HalfCardInteractor';
import PincodeInteractor from './Pincode/PincodeInteractor';
import DownArrow from '../../resources/svg/DownArrow';
import serviceabilityAPI from './serviceabilityAPI.js';
import { getDeliveryMessage } from './helper';
import kvpairs from '../../utils/kvpairs';
import updateLocationContext from '../../common/LocationContext/LocationContext';
import { addressOnCart } from './AddressOnCart/AddressOnCart';

const haveLocalStorage = typeof localStorage !== 'undefined' && typeof localStorage === 'object';
const autoFetch = features('autoFetch.serviceability.enable') === 'true';

class Pincode extends React.Component {
  constructor(props) {
    super(props);
    const pincode = parseInt(this.retrieveFromBrowserSession('pincode'), 10);
    this.state = {
      showServiceability: true,
      isLoading: false,
      pincode: !isNaN(pincode) ? pincode : '',
      allServiceabilityInfo: null,
      serviceabilityInfo: null,
      inputValue: '',
      promiseDate: props.promiseDate,
      pincodeEntered: props.pincodeEntered
    };
    this.checkServiceability = this.checkServiceability.bind(this);
    this.checkAnotherPinCode = this.checkAnotherPinCode.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.clearBrowserSession = this.clearBrowserSession.bind(this);
    this.retrieveFromBrowserSession = this.retrieveFromBrowserSession.bind(this);
    this.storeInBrowserSession = this.storeInBrowserSession.bind(this);
    this.getServiceabilityMarkup = this.getServiceabilityMarkup.bind(this);
    this.getOldDescriptors = this.getOldDescriptors.bind(this);
    this.getNewDescriptors = this.getNewDescriptors.bind(this);
    this.openHalfCard = this.openHalfCard.bind(this);
    this.getServiceabilityPayload = this.getServiceabilityPayload.bind(this);
  }

  componentDidMount() {
    const pCode = parseInt(this.retrieveFromBrowserSession('pincode'), 10);

    if (!isNaN(pCode)) {
      this.setState({
        inputValue: pCode
      })
      if(autoFetch) {
        this.checkServiceability(null, { pCode });
      }
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const pCode = parseInt(this.retrieveFromBrowserSession('pincode'), 10);
    if (autoFetch && nextProps.checkSizeServiceability && (!isNaN(pCode) || this.state.pincode)) {
      this.checkServiceability(null, { pCode, selectedSkuid: nextProps.selectedSkuid});
      return;
    }
    const updateDelivery = nextProps.promiseDate && nextProps.promiseDate !== this.state.promiseDate
    if (updateDelivery) {
      this.setState({
        promiseDate: nextProps.promiseDate
      });
    }
  }

  getStyleOption(selectedSkuid) {
    const skuId = selectedSkuid || this.props.selectedSkuid;
    if (skuId) {
      const selectedSizeOption = this.props.data.sizes.filter((item) => (item.skuId === parseInt(skuId, 10)));
      return selectedSizeOption[0];
    }
    // sending the first non out of stock size
    const defaultSizeOption = find(at(this.props, 'data.sizes'), (item) => (item.available === true));
    return defaultSizeOption;
  }

  getServiceabilityMarkup(listItems) {
    const isServiceable = this.props.serviceable;
    if (isServiceable === false) {
      return (<p className={styles.error}>Unfortunately we do not ship to your pincode</p>);
    }
    return listItems.length > 0 ? <ul className={styles['serviceability-list']}>{listItems}</ul> : null;
  }

  getOldDescriptors(item, index) {
    if (item.icon === "truck") {
      item.title = getDeliveryMessage(
        this.state.promiseDate,
        get(this.props.data, "flags.disableBuyButton"),
        get(this.props.data, "serviceability.launchDate"),
        get(this.props.data, "preOrder")
      );
    }
    return (
      <li key={`pincode-${index}`}>
        <div className={styles.tickSmallContainer}><div className={styles.tickSmall} /></div>
        <div className={styles.serviceabilityContainer}>
          <h4>{item.title}</h4>
          <p dangerouslySetInnerHTML={{ '__html': item.description }}></p>
        </div>
      </li>
    );
  }

  getNewDescriptors(item, index) {
    if (item.icon === "truck") {
      item.title = getDeliveryMessage(
        this.state.promiseDate,
        get(this.props.data, "flags.disableBuyButton"),
        get(this.props.data, "serviceability.launchDate"),
        get(this.props.data, "preOrder")
      );
    }
    const Icon = iconProvider(item.icon);
    const hasHalfCard = HalfCardInteractor.hasHalfCard(item.viewMore) && HalfCardInteractor.featureEnabled();
    return (
      <li className={styles.serviceabilityItem} key={`pincode-${index}`}>
        {Icon && <Icon className={styles.serviceabilityIcon} />}
        <h4 className={styles.serviceabilityTitle}>{item.title}</h4>
        {hasHalfCard && <span
          className={styles.serviceabilityViewMore}
          onClick={() => this.openHalfCard(item.viewMore)}>
          MORE INFO
          <DownArrow fill="#ff3e6c" className={styles.rightArrow} />
        </span>}
      </li>
    );
  }

  getServiceability() {
    const serviceabilityInfo = at(this.state, 'serviceabilityInfo.contents');
    let listItems = [];
    const inputText = this.state.serviceabilityInfo ? this.state.pincode : this.state.inputValue;
    const pincodeCheckMsg = kvpairs('web.pincodecheck.msg') || 'Please enter PIN code to check delivery time & Pay on Delivery Availability';
    if (serviceabilityInfo && serviceabilityInfo.length && this.state.showServiceability) {
      const renderFn = PincodeInteractor.featureEnabled() ? this.getNewDescriptors : this.getOldDescriptors;
      listItems = serviceabilityInfo.map(renderFn);
    }
    return (
      <div className={styles.checkServiceAbilityhalfCard}>
        <div className={styles.deliveryContainer}>
          <h4>Delivery Options <span className={`myntraweb-sprite ${styles.deliveryOptionsIcon}`} /></h4>
          <form ref="serviceability" autoComplete="off"
                onSubmit={e => this.checkServiceability(e, { userInput: true })} >
            <input
              type="text"
              placeholder="Enter pincode"
              className={styles.code}
              onChange={this.handleChange}
              value={inputText}
              disabled={!!this.state.serviceabilityInfo}
              name="pincode">
            </input>
            {!!this.state.serviceabilityInfo && this.props.serviceable ?
              <div className={styles.tickcontainer}><div className={styles.tick} /></div> : null
            }
            {!this.state.serviceabilityInfo && !this.state.pincodeEntered ? <input type="submit" className={styles.check} value="Check" />
            : <button type="button" className={styles['check-another-pincode']} onClick={this.checkAnotherPinCode}>Change</button>}
          </form>
          {!this.state.serviceabilityInfo && (
            <p className={styles.enterPincode}>{pincodeCheckMsg}</p>
          )}
          {this.getServiceabilityMarkup(listItems)}
        </div>
      </div>
    );
  }

  openHalfCard(type) {
    this.props.showServiceabilityHalfCard(type);
    if (window.ga) {
      window.ga('send', 'event', 'new_user_onboarding', 'click_view_more_serviceability_on_pdp', `halfCard-${type}`);
    }
  }

  checkAnotherPinCode() {
    this.clearBrowserSession('pincode');
    updateLocationContext("", true);
    addressOnCart.clear();
    this.setState({
      serviceabilityInfo: null,
      showServiceability: true,
      allServiceabilityInfo: null,
      inputValue: '',
      pincodeEntered: false,
      pincode: ''
    }, () => {
      this.props.filterUnserviceableSellers(null, null, false);
      this.props.handlePincode(false);
    });
  }

  isCODEnabled() {
    let paymentModes = at(this.state.serviceabilityInfo,
      'CourierServiceabilityInfoResponse.order.summary.availablePaymentModes.paymentMode');
    if (paymentModes && !(paymentModes instanceof Array)) {
      paymentModes = [paymentModes];
    }
    return paymentModes.some((mode) => (mode === 'cod'));
  }

  track(response) {
    const isServiceable = at(response, 'body.isServiceable');
    const components = at(response, 'body.cards.0');
    const analytics = at(components, 'components.0.props.serviceability.analytics');
    const launchDate = get(this.props, "data.serviceability.launchDate");
    const startDate = launchDate
      ? new Date(launchDate).getTime()
      : Date.now();
    const daysToDeliver = `${Math.ceil(
      (promiseDate - startDate) / (1000 * 60 * 60 * 24)
    )}`;
    const codEnabled = at(analytics, 'codEnabled');
    const onlineEnabled = at(analytics, 'onlineEnabled');
    const pincode = this.state.pincode;
    if (isServiceable !== false && (codEnabled || onlineEnabled)) {
      Track.event('PincodeWidget', pincode, `COD_${codEnabled}_OL_${onlineEnabled}_Days_${daysToDeliver}`);
    } else {
      Track.event('PincodeWidget', pincode, 'COD_0_OL_0');
    }
  }

  storeInBrowserSession(data) {
    try {
      if (haveLocalStorage && data) {
        localStorage.setItem(data.label, data.content);
      }
    } catch (e) {
      console.log('Something wrong with localStorage while storing, ', e);
    }
  }

  retrieveFromBrowserSession(label) {
    let content = null;
    try {
      if (haveLocalStorage && label) {
        content = localStorage.getItem(label);
      }
    } catch (e) {
      console.log('Something wrong with localStorage while retriving, ', e);
    }
    return content;
  }

  clearBrowserSession(label) {
    try {
      if (haveLocalStorage && label) {
        localStorage.removeItem(label);
      }
    } catch (e) {
      console.log('Something wrong with localStorage while clearing, ', e);
    }
  }

  getServiceabilityPayload(pincode) {
    const pdpData = get(this.props, "data");
    const uidx = get(window, '__myx_session__.login');
    const epoch = Date.now();
    let payload = {
      pincode: `${pincode}`,
      clientId: "2297",
      paymentMode: "ALL",
      serviceTypes: ["FORWARD", "EXCHANGE", "RETURN"],
      shippingMethod: "NORMAL",
      clientReferenceId: `${uidx ? uidx : "guest"}_${epoch}`
    };
    let items = [];
    (get(pdpData, "sizes") || []).forEach(size => {
      (get(size, "sizeSellerData") || []).forEach(seller => {
        const sellerPartnerId = get(seller, "sellerPartnerId");
        items.push({
          itemReferenceId: `${sellerPartnerId}` || "",
          skuId: size.skuId.toString() || "",
          procurementTimeInDays:
            get(
              pdpData,
              `serviceability.procurementTimeInDays.${sellerPartnerId}`
            ) || 0,
          availableInWarehouses: get(seller, "warehouses") || [],
          itemValue: get(seller, "discountedPrice") || get(pdpData, "mrp"),
          isHazmat: get(pdpData, "flags.isHazmat") || false,
          isLarge: get(pdpData, "flags.isLarge") || false,
          isJewellery: get(pdpData, "flags.isJewellery") || false,
          isFragile: get(pdpData, "flags.isFragile") || false,
          codValue: get(seller, "discountedPrice") || get(pdpData, "mrp"),
          codEnabled: get(pdpData, "flags.codEnabled") || false,
          tryAndBuyEnabled: get(pdpData, "flags.tryAndBuyEnabled") || false,
          isExchangeable: get(pdpData, "flags.isExchangeable") || false,
          isReturnable: get(pdpData, "flags.isReturnable") || false,
          openBoxPickupEnabled:
            get(pdpData, "flags.openBoxPickupEnabled") || false,
          articleType: get(pdpData, "analytics.articleType") || "",
          measurementModeEnabled:
            get(pdpData, "flags.measurementModeEnabled") || false,
          sampleModeEnabled: get(pdpData, "flags.sampleModeEnabled") || false,
          articleGender: get(pdpData, "analytics.gender") || ""
        });
      });
    });
    payload.items = items;
    return payload;
  }

  checkServiceability(e, data = {}) {
    if (e) {
      e.preventDefault();
    }

    const pincode = data.pCode || parseInt(this.refs.serviceability.pincode.value, 10);
    const allServiceabilityData = at(this.state, 'allServiceabilityInfo');

    if (pincode && pincode < 1000000 && pincode > 99999) {
      if (data.userInput) {
        updateLocationContext("" + pincode);
        addressOnCart.updatePincode(pincode);
      }
      const returnPeriod = get(this.props, "data.serviceability.returnPeriod");
      const descriptors = get(this.props, "data.serviceability.descriptors");

      this.setState({
        isLoading: true,
        pincode,
        pincodeEntered: true
      });

      const selectedSkuid = data.selectedSkuid || this.props.selectedSkuid;
      let skuId, sellerPartnerId;
      if (selectedSkuid) {
        skuId = selectedSkuid;
        sellerPartnerId = get(
          (get(this.props, "data.sizes") || []).find(
            size => size.skuId === skuId
          ),
          "selectedSeller.sellerPartnerId"
        );
      } else {
        const bbw = (get(this.props, "data.buyButtonSellerOrder") || []).find(
          pair => {
            if (pair.hasOwnProperty("serviceable")) {
              return pair.serviceable;
            }
            return true;
          }
        ) || {};
        skuId = bbw.skuId;
        sellerPartnerId = bbw.sellerPartnerId;
      }

      Track.event('PincodeWidget', pincode, 'CheckPincode');

      let serviceabilityInfo = (allServiceabilityData || []).find(
        entry =>
          entry.skuId === skuId && entry.sellerPartnerId === sellerPartnerId
      ) || { pincode };

      if (allServiceabilityData) {
        if (haveLocalStorage && parseInt(localStorage.pincode, 10) !== pincode && get(serviceabilityInfo, 'isServiceable')) {
          this.storeInBrowserSession({ label: 'pincode', content: pincode });
        }
        this.setState({
          isLoading: false,
          serviceabilityInfo
        }, () => {
          this.props.handlePincode(true);
        });
      } else {
        const payload = this.getServiceabilityPayload(pincode);
        serviceabilityAPI.checkV3(payload, returnPeriod, descriptors).then(response => {
          let serviceabilityInfo = (response || []).find(
            entry =>
              entry.skuId === skuId && entry.sellerPartnerId === sellerPartnerId
          );
          if(serviceabilityInfo && get(serviceabilityInfo, 'isServiceable')) {
            this.storeInBrowserSession({ label: 'pincode', content: pincode });
          }

          this.setState({
            serviceabilityInfo,
            allServiceabilityInfo: response || []
          });
          if(response && response.length) {
            this.props.filterUnserviceableSellers(response, pincode);
          }
        })
        .catch(err => {
          this.props.onError('Oops! Something went wrong. Please try again in some time.');
            this.setState({
              isLoading: false
            }, () => {
              this.props.handlePincode(true);
            });
        });
      }
    } else {
      this.props.onError('Please enter a valid pincode');
    }
  }

  showServiceability() {
    this.setState({
      showServiceability: true
    });
  }

  toggleCardDisplay(e, which) {
    e.stopPropagation();
    if (which !== 'pincodeCheck') {
      this.setState({
        showServiceability: !this.state.showServiceability
      });
    }
  }

  handleChange(event) {
    const value = event.target.value;
    const intValue = parseInt(value, 10);

    if (!isNaN(intValue) && intValue && intValue.toString().length < 7) {
      this.setState({ inputValue: intValue });
    }

    if (value === '') {
      this.setState({
        inputValue: value
      });
    }
  }

  render() {
    if (isOutOfStock(this.props) || !at(this.props, 'data.serviceability')) {
      return null;
    }
    return (
      <div className={styles.serviceability}>
        {this.getServiceability()}
      </div>
    );
  }
}

Pincode.propTypes = {
  data: React.PropTypes.object.isRequired,
  selectedSkuid: React.PropTypes.string,
  toggleRequestLoader: React.PropTypes.func,
  onError: React.PropTypes.func.isRequired,
  handlePincode: React.PropTypes.func,
  showServiceabilityHalfCard: React.PropTypes.func,
  filterUnserviceableSellers: React.PropTypes.func,
  getPromiseDate: React.PropTypes.func,
  promiseDate: React.PropTypes.string,
  serviceable: React.PropTypes.bool,
  pincodeEntered: React.PropTypes.bool
};

export default Pincode;
