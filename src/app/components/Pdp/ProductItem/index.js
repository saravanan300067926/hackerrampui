import React from 'react';
import styles from './product-item.css';
import { srcSet } from '../../Search/helpers';
import { getProductImageUrl } from '../utils';
import at from 'v-at';
import get from "lodash/get";
import LazyLoad from 'react-lazyload';
import StarIcon from '../../Ratings/StarIcon';
import { isBrowser } from '../../../utils';
import { getWebpUrl } from '../../../utils/getWebpUrl';

const featureGates = isBrowser() ? window.__myx_features__ || {} : {};
const ratingsEnabled = featureGates['web.ratings.enable'] === true;

function getImage(product) {
  let srcSetImg = '';
  const imgSrc = getProductImageUrl(product.defaultImage, {
    width: 420,
    height: 560,
    q: 90
  });
  if (imgSrc) {
    srcSetImg = srcSet(imgSrc);
  }

  const webpUrlImageUrl = getWebpUrl(imgSrc);

  return (
    <LazyLoad className={styles.image} width={210} height={280} offset={500} once>
      <div className={styles.imageCtn} >
        <picture className="img-responsive">
          <source
            srcSet={webpUrlImageUrl}
            type="image/webp" />
          <img
            className={styles.image}
            src={imgSrc}
            alt={product.name}
            title={product.name}
            srcSet={srcSetImg} />
        </picture>
      </div>
    </LazyLoad>
  );
}

function getPrice(product) {
  const sellingPrice = at(product, 'price.discounted');
  const mrp = at(product, 'price.mrp');
  if (sellingPrice === mrp) {
    return '';
  }
  return `Rs. ${mrp}`;
}

function ProductItem(props) {
  const product = props.product;
  const isFastFashion = get(product, 'isFastFashion');
  if (product) {
    return (
      <div className={styles.container}>
        {getImage(product)}
        {ratingsEnabled && product.rating && product.rating > 0 && !isFastFashion && (
          <div className={styles.ratingsContainer}>
            <span>{product.rating.toString().indexOf('.') !== -1 ? product.rating.toFixed(1) : product.rating}</span>
            <StarIcon rating={product.rating} cssClasses={styles.starIcon} />
          </div>)
        }
        <div className={styles.metaContainer}>
          <p className={styles.brand}>{at(product, 'brand.name')}</p>
          <p className={styles.title}>{product.info || product.name}</p>
          <div className={styles.pricing}>
            <span className={styles['selling-price']}>Rs. {at(product, 'price.discounted') || at(product, 'price.mrp')}</span>
            <s className={styles.mrp}>{getPrice(product)}</s>
            <span className={styles.discount} dangerouslySetInnerHTML={{ __html: (at(product, 'price.discount.label') || '') }}></span>
          </div>
        </div>
      </div>
    );
  }
  return null;
}

ProductItem.propTypes = {
  product: React.PropTypes.object.isRequired
};

export default ProductItem;
export { getImage };
