import get from "lodash/get";
import config from '../../config';
import Client from '../../services';

function cleanResponse(body, returnPeriod, descriptors) {
  return (get(body, "itemServiceabilityEntries") || []).map(entry => {
    let contents = [];
    const normalDelivery = (get(entry, "serviceabilityEntries") || []).find(
      sEntry => sEntry.serviceType === "DELIVERY"
    );
    if (normalDelivery) {
      contents.push({
        title: "",
        description: "",
        icon: "truck"
      });
      let mode;
      if (
        normalDelivery.availablePaymentModes.indexOf("CASH_ON_DELIVERY") !== -1
      ) {
        mode = "Cash";
      }
      if (
        normalDelivery.availablePaymentModes.indexOf("CARD_ON_DELIVERY") !== -1
      ) {
        mode += `${mode ? "/" : ""}Card`;
      }
      const codDisabled = (descriptors || []).some(descriptor =>
        /pay\son\sdelivery/i.test(descriptor)
      );
      if (mode) {
        contents.push({
          title: `${codDisabled ? "Pay" : mode} on delivery available`,
          description: "",
          icon: "card"
        });
      } else {
        contents.push({
          title: `${codDisabled ? "Pay" : "Cash"} on delivery not available`,
          description: "",
          icon: "card"
        });
      }
    }

    const returns = (get(entry, "serviceabilityEntries") || []).find(
      sEntry =>
        sEntry.serviceType === "OPEN_BOX_PICKUP" ||
        sEntry.serviceType === "CLOSED_BOX_PICKUP"
    );
    const exchanges = (get(entry, "serviceabilityEntries") || []).find(
      sEntry => sEntry.serviceType === "EXCHANGE"
    );
    if (returns || exchanges) {
      let retExch = returns ? "return" : "";
      retExch = exchanges
        ? `${retExch.length ? `${retExch} & ` : ""}exchange`
        : retExch;
      contents.push({
        title: `Easy ${returnPeriod} days ${retExch} available`,
        description: "",
        icon: "exchange",
        viewMore: "returnAndExchange"
      });
    } else {
      contents.push({
        title: `Return & Exchange not available`,
        description: "",
        icon: "exchange",
        viewMore: ""
      });
    }

    const tryAndBuy = (get(entry, "serviceabilityEntries") || []).find(
      sEntry => sEntry.serviceType === "TRY_AND_BUY"
    );
    if (tryAndBuy) {
      contents.push({
        title: "Try & Buy available",
        description: "",
        icon: "try",
        viewMore: "try"
      });
    }
    return {
      isServiceable: get(entry, "serviceable") && !!normalDelivery,
      skuId: Number(get(entry, "skuId")),
      sellerPartnerId: Number(get(entry, "itemReferenceId")),
      contents
    };
  });
}

class serviceabilityAPI {
  constructor() {
    this.checkV3 = this.checkV3.bind(this);
    this.checkV2 = this.checkV2.bind(this);
  }

  checkV3(payload, returnPeriod, descriptors) {
    return new Promise((resolve, reject) => {
        Client.post(config('serviceabilityV3'), payload).end((err, res) => {
          if (err) {
            reject(err);
          } else {
            const body = get(res, "body");
            if (!body) reject("NULL RESPONSE");

            const serviceability = cleanResponse(body, returnPeriod, descriptors);
            resolve(serviceability);
          }
        });
    });
  }

  checkV2(pdpData, queryPairs = [], pincode) {
    const epoch = Date.now();
    const uidx = get(window, '__myx_session__.login');
    let finalPayload = {
      pincode: `${pincode}`,
      clientReferenceId: `${uidx ? uidx : "guest"}_${epoch}`,
      clientId: "2297",
      consolidationEnabled: false,
      paymentMode: "ALL",
      serviceType: "FORWARD",
      shippingMethod: "NORMAL",
      nonWorkingDays: []
    };
    let items = [];
    queryPairs.forEach(pair => {
      const sellerPartnerId = get(pair, "sellerPartnerId");
      const sellerData = (
        get(
          (get(pdpData, "sizes") || []).find(size => size.skuId === pair.skuId),
          "sizeSellerData"
        ) || []
      ).find(seller => seller.sellerPartnerId === sellerPartnerId);
      items.push({
        itemReferenceId: `${sellerPartnerId}` || "",
        codValue: get(sellerData, "discountedPrice") || get(pdpData, "mrp"),
        itemValue: get(sellerData, "discountedPrice") || get(pdpData, "mrp"),
        isHazmat: get(pdpData, "flags.isHazmat") || false,
        isLarge: get(pdpData, "flags.isLarge") || false,
        isJewellery: get(pdpData, "flags.isJewellery") || false,
        isFragile: get(pdpData, "flags.isFragile") || false,
        codEnabled: get(pdpData, "flags.codEnabled") || false,
        openBoxPickupEnabled:
          get(pdpData, "flags.openBoxPickupEnabled") || false,
        tryAndBuyEnabled: get(pdpData, "flags.tryAndBuyEnabled") || false,
        isReturnable: get(pdpData, "flags.isReturnable") || false,
        procurementTimeInDays:
          get(
            pdpData,
            `serviceability.procurementTimeInDays.${sellerPartnerId}`
          ) || 0,
        launchDate: get(pdpData, "serviceability.launchDate") || "",
        availableInWarehouses: get(sellerData, "warehouses"),
        skuId: pair.skuId.toString()
      });
    });
    finalPayload.items = items;

    return new Promise((resolve, reject) => {
      if (queryPairs.length === 0) {
        reject("No items to get promise date");
      }
      Client.post(config('serviceabilityV2'), finalPayload).end((err, res) => {
        if (err) {
          reject(err);
        } else {
          const body = get(res, "body");
          if (!body) reject("NULL RESPONSE");
          resolve(body);
        }
      });
    });
  }
}

export default new serviceabilityAPI();
