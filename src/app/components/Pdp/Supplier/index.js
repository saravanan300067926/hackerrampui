import React, { PropTypes } from 'react';
import at from 'v-at';
import get from 'lodash/get';
import styles from './supplier.css';
import ContactSellerCard from './contactSellerCard';
import ContactSellerTooltip from './contactSellerTooltip';

class Supplier extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showSellerContact: false,
      showMoreDetails: false
    };
    this.showSellerContactCard = this.showSellerContactCard.bind(this);
    this.getProductCode = this.getProductCode.bind(this);
    this.hideSellerContactCard = this.hideSellerContactCard.bind(this);
  }

  getSupplierInfo() {
    const styleOptions = at(this.props, 'data.sizes') || [];
    let availableStyleOption = styleOptions.filter((item) => item.available)[0];

    if (!availableStyleOption) {
      availableStyleOption = styleOptions[0];
    }

    const supplyType = at(availableStyleOption, 'supplyType');
    const partner = supplyType && supplyType === 'JUST_IN_TIME' ? '( Supplied By Partner )' : '';
    return partner;
  }

  getSupplier(sellerName, partner) {
    if (sellerName) {
      return (
        <span>
          Sold by: <span onClick={this.showSellerContactCard} className={styles.productSellerName}>
            {sellerName}
            {this.state.showSellerContact && (
              <span className={styles.contactSellerContainer}>
                {this.props.mobile ? (
                  <ContactSellerCard seller={sellerName} toggleCard={this.hideSellerContactCard} />
                ) : (
                  <ContactSellerTooltip seller={sellerName} toggleCard={this.hideSellerContactCard} />
                )}
              </span>
            )}
          </span> <span className={styles['partner-name']}>{partner} </span>
        </span>
      );
    } return null;
  }

  getProductCode() {
    const productCode = at(this.props, 'data.id') || '';
    if (productCode) {
      return (<span>Product Code: <span className={styles.styleId}>{productCode}</span></span>);
    }
    return null;
  }

  showSellerContactCard(e) {
    e.stopPropagation();
    this.setState({
      showSellerContact: true
    });
  }

  hideSellerContactCard(e) {
    e.stopPropagation();
    this.setState({
      showSellerContact: false
    });
  }

  toggleShowMoreDetails = () => {
    this.setState(oldState => (
      { showMoreDetails: !oldState.showMoreDetails }
    ));
  }

  render() {
    if (this.props.mobile) {
      return null;
    }
    const partner = this.getSupplierInfo();
    const sellerName = get(this.props, 'data.selectedSeller.sellerName');
    const supplierHtml = this.getSupplier(sellerName, partner);
    return (
      <div className={`${styles['product-code-and-supplier']} ${styles.desktopCodeSupplier}`}>
        <div className={styles.supplier}>{this.getProductCode()}</div>
        <div className={styles.supplier}>{supplierHtml}</div>
        <ExtraSupplierInfo
          showMoreDetails={this.state.showMoreDetails}
          viewMoreClick={this.toggleShowMoreDetails}
          manufacturer={this.props.data.manufacturer}
          countryOfOrigin={this.props.data.countryOfOrigin} />
      </div>
    );
  }
}

Supplier.propTypes = {
  data: PropTypes.object.isRequired,
  mobile: PropTypes.bool
};

export default Supplier;


const ExtraSupplierInfo = (props) => {
  const { manufacturer, countryOfOrigin, showMoreDetails, viewMoreClick } = props;
  if (!manufacturer && !countryOfOrigin) {
    return null;
  }

  const manufacturerDetails = (
    <div className={styles.manufacturer}>
      <div><strong> Manufacturer/Packer/Importer Details </strong></div>
      <div className={styles['met-values']}> {manufacturer} </div>
    </div>
  );

  const countryDetails = (
    <div className={styles.manufacturer}>
      <div><strong> Country of origin </strong></div>
      <div className={styles['met-values']}> {countryOfOrigin} </div>
    </div>
  );

  const viewMoreLink = (
    <div className={styles['viewmore-link']} onClick={viewMoreClick}> View Supplier Information</div>
  );

  let view;

  if (!showMoreDetails) {
    view = viewMoreLink;
  } else {
    view = (
      <div>
      {manufacturer ? manufacturerDetails : null}
      {countryOfOrigin ? countryDetails : null}
      </div>
    );
  }
  return view;
};

ExtraSupplierInfo.propTypes = {
  manufacturer: PropTypes.string,
  countryOfOrigin: PropTypes.string,
  showMoreDetails: PropTypes.bool,
  viewMoreClick: PropTypes.func
};
