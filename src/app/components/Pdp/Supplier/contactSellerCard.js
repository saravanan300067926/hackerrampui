import React, { PropTypes } from 'react';
import styles from './supplier.css';

const ContactSellerCard = ({ seller, toggleCard }) => (
  <div>
    <div className={styles.halfCardBackdrop} onClick={toggleCard}></div>
    <div className={styles.contactSellerHalfCard}>
      <h4>Do you want to contact Myntra about {seller}?</h4>
      <div className={styles.buttons}>
        <span className={styles.button} onClick={toggleCard}>Cancel</span>
        <a className={styles.button} href="/contactus">Yes</a>
      </div>
    </div>
  </div>
);

ContactSellerCard.propTypes = {
  seller: PropTypes.string,
  toggleCard: PropTypes.func
};

export default ContactSellerCard;
