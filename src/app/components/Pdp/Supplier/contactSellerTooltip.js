import React, { PropTypes } from 'react';
import styles from './supplier.css';

const ContactSellerTooltip = ({ seller, toggleCard }) => (
  <div className={styles.supplierContactTootip}>
    <div className={styles['triangle-with-shadow']}></div>
    <h4>Do you want to contact Myntra about {seller}?</h4>
    <span className={styles.button} onClick={toggleCard}>Cancel</span>
    <a className={styles.button} href="/contactus">Yes</a>
  </div>
);

ContactSellerTooltip.propTypes = {
  seller: PropTypes.string,
  toggleCard: PropTypes.func
};

export default ContactSellerTooltip;
