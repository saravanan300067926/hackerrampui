import get from 'lodash/get';
import find from "lodash/find";
import { inPriceRevealMode } from '../../utils/slotUtils';
import priorityCheckout from '../../utils/priorityCheckoutUtils';
import { getFeatures, isBrowser } from '../../utils';
const sellableInventoryModeEnabled = getFeatures('sellableInventoryModeEnabled') || false;


const setEMIValueInOffers = pdpData => {
    const kvPairs = isBrowser() ? window.__myx_kvpairs__ || {} : {};
    let emiPlans = {};
    if (kvPairs['product.emi.info']) {
      emiPlans = JSON.parse(kvPairs['product.emi.info']);
    }
    const discountedPrice = get(pdpData, "selectedSeller.discountedPrice");
    pdpData.offers = pdpData.offers.map(offer => {
      if (offer.type === "EMI") {
        const emiValue = Math.round(
          discountedPrice *
            (emiPlans.minInterestRate / 100 + 1) /
            emiPlans.maxMonths
        );
        offer.description = `EMI starting from Rs.${emiValue}/month`;
      }
      return offer;
    });
  };
  
  const setComboOffer = pdpData => {
    const discount = get(pdpData, "selectedSeller.discount");
    if (get(discount, "heading")) {
      const combo = {
        type: "COMBO",
        title: get(discount, "heading"),
        description: get(discount, "description"),
        action: get(discount, "link"),
        freeItem: get(discount, "freeItem"),
        freeitemImage: get(discount, "freeItemImage")
      };
      let found = false;
      for (let i = 0; i < pdpData.offers.length; i++) {
        if (pdpData.offers[i].type === "COMBO") {
          pdpData.offers[i] = combo;
          found = true;
          break;
        }
      }
      if (!found) {
        pdpData.offers.push(combo);
      }
    }
  };

const getDefaultSellerInfo = (pdpData, selectedSellerPartnerId, selectedSkuId) => {
  const {
    sellerPartnerId,
    sellerDataWithPrice,
    customSeller
  } = getDefaultSkuSeller(pdpData, selectedSellerPartnerId, selectedSkuId);

  const sizeSellerData = find(
    get(pdpData, "sellers"),
    seller => seller.sellerPartnerId === sellerPartnerId
  );

  const discountId = get(sellerDataWithPrice, "discountId");
  const discount = find(
    get(pdpData, "discounts"),
    discount => discount.discountId === discountId
  );

  const sellerName = get(sizeSellerData, "sellerName");
  const discountedPrice =
    get(sellerDataWithPrice, "discountedPrice") || get(pdpData, "mrp");
  const serviceable = get(sellerDataWithPrice, "serviceable");

  return {
    sellerPartnerId,
    sellerName,
    discountedPrice,
    discount,
    customSeller,
    serviceable
  };
};

const getDefaultSkuSeller = (
  pdpData,
  selectedSellerPartnerId,
  selectedSkuId
) => {
  let customSeller = false;
  const filteredSellerOrder = (
    get(pdpData, "buyButtonSellerOrder") || []
  ).filter(pair => {
    const condition = (get(pdpData, "sizes") || []).some(
      size =>
        size.skuId === pair.skuId &&
        isSellerAvailableForSku(
          size,
          (get(size, "sizeSellerData") || []).find(
            seller => seller.sellerPartnerId === pair.sellerPartnerId
          )
        )
    );
    if (pair.hasOwnProperty("serviceable")) {
      return pair.serviceable && condition;
    }
    return condition;
  });
  let sellerPartnerId = selectedSellerPartnerId;
  let skuId = selectedSkuId;
  if (!sellerPartnerId && !skuId) {
    const defaultSeller = get(filteredSellerOrder, "0");
    sellerPartnerId = get(defaultSeller, "sellerPartnerId");
    skuId = get(defaultSeller, "skuId");
  } else {
    customSeller = true;
  }
  const sellerDataWithPrice = find(
    get(
      find(get(pdpData, "sizes"), size => size.skuId === skuId),
      "sizeSellerData"
    ),
    seller => {
      if (seller.hasOwnProperty("serviceable")) {
        return seller.sellerPartnerId === sellerPartnerId && seller.serviceable;
      }
      return seller.sellerPartnerId === sellerPartnerId;
    }
  );
  if (selectedSellerPartnerId && selectedSkuId && !sellerDataWithPrice) {
    return getDefaultSkuSeller(pdpData);
  }
  return {
    skuId,
    sellerPartnerId,
    sellerDataWithPrice,
    customSeller
  };
};

const isSellerAvailableForSku = (skuData, sellerInfo) => {
  let available;
  if (
    sellableInventoryModeEnabled ||
    inPriceRevealMode() ||
    priorityCheckout()
  ) {
    available = skuData.available && get(sellerInfo, "sellableInventoryCount", 0) > 0;
  }
  available = skuData.available && get(sellerInfo, "availableCount", 0) > 0;
  if (sellerInfo && sellerInfo.hasOwnProperty("serviceable")) {
    return sellerInfo.serviceable && available;
  }
  return available;
};

const setSelectedSeller = (pdpData, selectedSeller, skuId) => {
    pdpData.commonPrice = true;
    let currentPrice;
    pdpData.sizes.forEach(skuData => {
        let bestSeller = get(skuData, "sizeSellerData.0") || {};
        if (selectedSeller.customSeller && skuData.skuId === skuId) {
        bestSeller =
            find(
            skuData.sizeSellerData,
            seller => seller.sellerPartnerId === selectedSeller.sellerPartnerId
            ) || {};
        }
        if (!isSellerAvailableForSku(skuData, bestSeller)) {
          const nextBestSeller = (get(skuData, "sizeSellerData") || []).find(
            seller => isSellerAvailableForSku(skuData, seller)
          );
          if (isSellerAvailableForSku(skuData, nextBestSeller)) {
            bestSeller = nextBestSeller;
          }
        }
        if (skuData.available) {
          if (!currentPrice) {
            currentPrice = bestSeller.discountedPrice || pdpData.mrp;
          } else {
            if ((bestSeller.discountedPrice || pdpData.mrp) !== currentPrice) {
              pdpData.commonPrice = false;
            }
          }
        }
        const discountId = get(bestSeller, "discountId");
        const discount = find(
        get(pdpData, "discounts"),
        discount => discount.discountId === discountId
        );
        skuData.selectedSeller = {
        sellerPartnerId: bestSeller.sellerPartnerId,
        discountedPrice: bestSeller.discountedPrice || get(pdpData, "mrp"),
        availableCount: bestSeller.availableCount,
        sellableInventoryCount: bestSeller.sellableInventoryCount,
        warehouses: bestSeller.warehouses,
        serviceable: bestSeller.serviceable,
        discount
        };
    });
};

const getFilteredPDPDataByServiceability = (
    pdpData,
    serviceablePairs = [],
    selectedSkuId = null
  ) => {
    if (pdpData) {
      let selectedSellerPartnerId;
      pdpData.buyButtonSellerOrder = pdpData.buyButtonSellerOrder.map(pair => {
        if (serviceablePairs !== null) {
          if (
            serviceablePairs.some(
              p =>
                p.skuId === pair.skuId &&
                p.sellerPartnerId === pair.sellerPartnerId
            )
          ) {
            pair.serviceable = true;
          } else {
            pair.serviceable = false;
          }
        } else {
          delete pair.serviceable;
        }
        return pair;
      });
      pdpData.sizes = pdpData.sizes.map(size => {
        const serviceableSellers = (serviceablePairs || [])
          .filter(pair => pair.skuId === size.skuId)
          .map(pair => pair.sellerPartnerId);
        size.sizeSellerData = (size.sizeSellerData || []).map(seller => {
          if (serviceablePairs !== null) {
            if (serviceableSellers.indexOf(seller.sellerPartnerId) === -1) {
              seller.serviceable = false;
            } else {
              seller.serviceable = true;
            }
          } else {
            delete seller.serviceable;
          }
          return seller;
        });
        if (selectedSkuId === size.skuId) {
          selectedSellerPartnerId = get(size, "selectedSeller.sellerPartnerId");
        }
        if (serviceablePairs !== null) {
          if (!size.hasOwnProperty("availableOld")) {
            size.availableOld = get(size, "available");
          }
          size.available = size.available
            ? (size.sizeSellerData.filter(seller => seller.serviceable) || [])
                .length > 0
            : size.available;
        } else if (size.hasOwnProperty("availableOld")) {
          size.available = size.availableOld;
          delete size.availableOld;
        }
        return size;
      });
      const sellerPartnerId = new URLSearchParams(window.location.search).get(
        "sellerPartnerId"
      );
      const skuId = new URLSearchParams(window.location.search).get("skuId");
  
      pdpData.selectedSeller = getDefaultSellerInfo(
        pdpData,
        Number(sellerPartnerId),
        Number(skuId)
      );
      setSelectedSeller(pdpData, pdpData.selectedSeller, Number(skuId));
      setEMIValueInOffers(pdpData);
      setComboOffer(pdpData);

      if (selectedSellerPartnerId) {
        const newSelectedSellerPartnerId = get(
          (get(pdpData, "sizes") || []).find(
            size => size.available && size.skuId === selectedSkuId
          ),
          "selectedSeller.sellerPartnerId"
        );
        if (selectedSellerPartnerId !== newSelectedSellerPartnerId) {
          selectedSkuId = null;
        }
      }
    }
    return {
      data: pdpData,
      selectedSkuid: selectedSkuId
    };
  };

  const getDeliveryMessage = (
    promiseDate,
    disableBuyButton,
    launchDate,
    preOrder
  ) => {
    if (!promiseDate) {
      return "Loading...";
    }
    launchDate = launchDate ? new Date(launchDate).valueOf() : Date.now();
    const deliveryDays = Math.ceil(
      (promiseDate - launchDate) / (1000 * 60 * 60 * 24)
    );
    if (disableBuyButton) {
      return `Expected Delivery Time:  ${deliveryDays} day${
        deliveryDays > 1 ? "s" : ""
      }`;
    } else if (preOrder) {
      return `Get in  ${deliveryDays} day${
        deliveryDays > 1 ? "s" : ""
      } after the launch date`;
    }
    const formattedDate = new Date(promiseDate)
      .toDateString()
      .replace(/\s\d{4}$/, "")
      .replace(/^([^\s]{3})/, "$1,");
    return `Get it by ${formattedDate}`;
  };
  

export { getDefaultSellerInfo, getDefaultSkuSeller, setSelectedSeller, isSellerAvailableForSku, getFilteredPDPDataByServiceability, setEMIValueInOffers, setComboOffer, getDeliveryMessage };
