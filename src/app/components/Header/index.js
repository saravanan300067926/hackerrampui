import React from 'react';
import at from 'v-at';
import get from 'lodash/get';
import Desktop from './Desktop';
import Mobile from './Mobile';
import { isBrowser, fetchTopNav } from '../../utils';
import { beacon } from '../../utils/beacon';
import bus from 'bus';
import userActions from './userActions.json';
import Notify from '../Notify';
import Track from '../../utils/track';
import Jsuri from 'jsuri';
import Client from '../../services';
import config from '../../config';

class Header extends React.Component {

  constructor(props) {
    super(props);
    const browserNavigationData = typeof localStorage !== 'undefined' && typeof localStorage === 'object' && localStorage.getItem('v1navData') ?
      JSON.parse(localStorage.getItem('v1navData')) : (isBrowser() && at(window, '__myx_navigationData__')) || null;
    const nData = isBrowser() ? browserNavigationData : at(this.props, 'navData');
    this.state = {
      navData: nData,
      doRender: typeof nData === 'object',
      userActionsData: userActions.data,
      sessionData: at(this.props, 'session') || {},
      animateCount: false
    };
  }

  componentDidMount() {
    const _self = this;
    if (isBrowser() && at(window, '__myx_navigationData__')) {
      try {
        _self.setState({ navData: at(window, '__myx_navigationData__'), doRender: true });
        localStorage.setItem('v1navData', JSON.stringify(at(window, '__myx_navigationData__')));
      } catch (e) {
        console.log('Browser does not support localStorage');
      }
    } else {
      fetchTopNav('topnav', (err, navigationData) => {
        if (!err) {
          if (typeof localStorage !== 'undefined' && typeof localStorage === 'object') {
            try {
              localStorage.setItem('v1navData', JSON.stringify(navigationData));
            } catch (e) {
              console.log('Browser does not support localStorage');
            }
          }
          _self.setState({ navData: navigationData, doRender: true });
          return;
        }
        _self.setState({ navData: null, doRender: true });
      });
    }

   // const pageUrl = location.href || '';

    // beacon call enabled on earlyslotinfo page
    if (isBrowser()) {
      beacon((err, data) => {
        if (!err) {
          this.configureAnalytics();
          Client.get(config('cartSummary')).end((_err, res) => {
            const sessionData = at(data, 'session');
            if (!_err) {
              sessionData['CART:totalQuantity'] = at(res, 'body.count');
            }
            this.setState({ sessionData });
            bus.emit('user.loggedIn', at(data, 'session.isLoggedIn'));
          });
        }
      });
    }

    bus.on('cart.add', (data) => {
      if (at(data, 'res.ok') && at(data, 'res.status') === 200) {
        const productData = at(data, 'res.body');
        const cartCount = at(productData, 'count');
        const sesData = at(this.state, 'sessionData');

        if (cartCount) {
          sesData['CART:totalQuantity'] = productData.count;
        }
        this.setState({
          sessionData: sesData,
          animateCount: true
        });

        window.setTimeout(() => {
          this.setState({
            animateCount: false
          });
        }, 3000);
        this.refs.notify.info({
          message: 'Added to bag',
          position: 'right',
          thumbnail: at(data, 'productImage') || null
        });
      } else {
        const error = at(data.res, 'body.error.message') || 'Oops! Something went wrong. Please try again in some time!';
        this.refs.notify.error(error);
      }
    });

    bus.on('wishlist.add', (stateVal) => {
      if (at(stateVal, 'res.body.status') === 200 && at(stateVal, 'res.body.status.statusType') !== 'ERROR') {
        Track.event('shopping', 'addToWishList', `${at(stateVal, 'res.body.data.productID')}`);
        this.refs.notify.info({
          message: 'Added to wishlist',
          thumbnail: at(stateVal, 'productImage'),
          position: 'right'
        });
      }
    });

    bus.on('cart.updateSummary', () => { // arg: data
      // This has been taken from old code. No events were triggered. Keeping for future use
    });
    bus.on('wishlist.updateCount', () => { // arg: count
      // This has been taken from old code. No events were triggered. Keeping for future use
    });

    bus.on('cart.addFromWishlist', (totalCount) => {
      if (totalCount && !isNaN(totalCount)) {
        const sesData = at(this.state, 'sessionData');
        sesData['CART:totalQuantity'] = totalCount;
        this.setState({ sessionData: sesData });
      }
    });
    this.configureAnalytics();
  }


  configureAnalytics() {
    if (at(window, 'Madalytics') && typeof window.Madalytics.configure === 'function') {
      window.Madalytics.configure({
        ...get(window, '__myx_instrumentation_env', {}),
        clientId: 'lzZNMYGoPkQVWOGL3wg81DLeJ4arpd'
      });
    }
  }

  isApp() {
    if (isBrowser()) {
      const deviceChannel = at(window, '__myx_deviceData__.deviceChannel') || '';
      const uriObj = new Jsuri(window.location.href);
      const appMode = uriObj.getQueryParamValue('mode') || '';
      if (appMode) {
        return deviceChannel.toLowerCase() === 'mobile_app' || appMode.toLowerCase() === 'app';
      }
      return deviceChannel.toLowerCase() === 'mobile_app';
    } return false;
  }

  render() {
    const isMobile = at(this.props, 'isMobile');
    const nData = isBrowser() ? at(this.state, 'navData') : at(this.props, 'navData');
    const isApp = isBrowser() ? this.isApp() : at(this.props, 'isApp');
    if (!isApp) {
      if (at(this.state, 'doRender')) {
        if (isMobile) {
          return (
            <div>
              <Mobile
                userActions={at(this.state, 'userActionsData')}
                navData={nData} session={at(this.state, 'sessionData')}
                notifyMessage={at(this.state, 'notifyMessage')} />
              <Notify ref="notify" />
            </div>
          );
        }
        return (
          <div>
            <Desktop
              animateCount={at(this.state, 'animateCount')}
              userActions={at(this.state, 'userActionsData')}
              navData={at(this.state, 'navData')}
              session={at(this.state, 'sessionData')}
              notifyMessage={at(this.state, 'notifyMessage')} />
            {isBrowser() && <Notify ref="notify" />}
          </div>
        );
      } return null;
    } return null;
  }

}

export default Header;
