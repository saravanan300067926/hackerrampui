import React from 'react';
import ReactDOM from 'react-dom';
import Header from './index';
import { isBrowser } from '../../utils';
import at from 'v-at';

const hRender = () => {
  const isItMobile = isBrowser() && (at(window, '__myx_deviceType__') === 'mobile' || at(window, '__myx_deviceData__.isMobile'));
  const myxSession = (isBrowser() ? window.__myx_session__ : {});

  ReactDOM.render(
    <Header isMobile={isItMobile} session={myxSession} />,
    document.getElementById('desktop-headerMount')
  );
};

export default hRender;
