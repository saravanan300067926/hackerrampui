import React from "react";

function Close(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      fill={props.fill || "#fff"}
      className="bi bi-x"
      viewBox="0 0 20 20"
    >
      <path
        fillRule="evenodd"
        d="M5.646 5.646a.5.5 0 000 .708l8 8a.5.5 0 00.708-.708l-8-8a.5.5 0 00-.708 0z"
        clipRule="evenodd"
      ></path>
      <path
        fillRule="evenodd"
        d="M14.354 5.646a.5.5 0 010 .708l-8 8a.5.5 0 01-.708-.708l8-8a.5.5 0 01.708 0z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default Close;
