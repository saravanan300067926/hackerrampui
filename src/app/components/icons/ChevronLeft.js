import React from "react";

function ChevronLeft() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      fill="#fff"
      className="bi bi-chevron-left"
      viewBox="0 0 20 20"
    >
      <path
        fillRule="evenodd"
        d="M13.354 3.646a.5.5 0 010 .708L7.707 10l5.647 5.646a.5.5 0 01-.708.708l-6-6a.5.5 0 010-.708l6-6a.5.5 0 01.708 0z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default ChevronLeft;
