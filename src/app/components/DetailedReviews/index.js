import React from 'react';
import UserReview from './UserReview';
import DetailedRating from '../Ratings/DetailedRating';
import Loader from '../Loader';
import Client from '../../services';
import config from '../../config';
import BreadCrumbs from './BreadCrumbs';
import DropdownFilter from './DropdownFilter';
import ProductDetails from './ProductDetails';
import ImageReviewsGallery from './ImageReviewsGallery';
import { get, uniqBy } from 'lodash';
import styles from './detailed-reviews.css';

const INITIAL_FILTER = {
  page: 1,
  sort: 0,
  rating: 0,
  size: 12,
  minCount: 3,
  noReview: null
};

class Reviews extends React.Component {
  constructor(props) {
    super(props);
    this.refInfiniteScroll = null;

    const reviewsCount = get(this.props, 'reviewInfo.reviewsCount') || get(window, '__myx.reviewsData.reviewsMetaData.reviewCount') || 0;
    const reviews = get(this.props, 'reviewInfo.topReviews') || get(window, '__myx.reviewsData.reviews') || [];

    this.state = {
      page: INITIAL_FILTER.page,
      sort: INITIAL_FILTER.sort,
      rating: INITIAL_FILTER.rating,
      size: INITIAL_FILTER.size,
      pageName: get(window, '__myx.pageName'),
      styleID: get(window, '__myx.pdpData.id'),
      reviewsCount: parseInt(reviewsCount, 10),
      reviews,
      loading: false,
      onZoom: false,
      noReview: INITIAL_FILTER.noReview,
      topReviews: get(this.props, 'reviewInfo.topReviews') || get(window, '__myx.pdpData.ratings.reviewInfo.topReviews'),
      topImages: get(this.props, 'reviewInfo.topImages') || get(window, '__myx.pdpData.ratings.reviewInfo.topImages'),
      topImageReviews: get(this.props, 'reviewInfo.topImageReviews') || get(window, '__myx.pdpData.ratings.reviewInfo.topImageReviews'),
      imageCount: get(this.props, 'reviewInfo.reviewsImageCount') || get(window, '__myx.pdpData.ratings.reviewInfo.reviewsImageCount')
    };

    [
      'toggleLoading',
      'onFilterChange',
      'initInfiniteScroll'
    ].forEach(method => (this[method] = this[method].bind(this)));
  }

  componentDidMount() {
    this.initInfiniteScroll();
  }

  onFilterChange(filter) {
    const reviewParam = {
      [filter.paramKey]: filter.paramValue,
      page: INITIAL_FILTER.page
    };
    if (filter.paramKey === 'sort') {
      reviewParam.rating = INITIAL_FILTER.rating;
    }
    this.getReviews(reviewParam);
  }

  onZoom = (flag = true) => {
    this.setState({
      onZoom: flag
    });
    this.props.onZoom(flag);
  }

  onVote(payload) {
    Client.post(config('reviewsSetVote'), payload).then(s => {
      console.log(`Voted: ${s}`);
    }).catch(console.error);
  }

  getReviews({
    page = this.state.page,
    sort = this.state.sort,
    rating = this.state.rating,
    size = this.state.size,
    style = this.state.styleID
  }) {
    if (!this.state.loading) {
      const url = `${config('reviews')}${style}?size=${size}&sort=${sort}&rating=${rating}&page=${page}&includeMetaData=true`;
      this.toggleLoading(true);
      Client.get(url)
        .then(({ body }) => {
          let newReviews = body.reviews || [];
          newReviews = page === INITIAL_FILTER.page ? newReviews : [...this.state.reviews, ...newReviews];
          if (newReviews.length) {
            this.setState({
              reviewsCount: parseInt(body.reviewsMetaData.reviewCount, 10),
              reviews: uniqBy(newReviews, e => e.reviewId || e.id),
              page,
              sort,
              rating,
              noReview: INITIAL_FILTER.noReview
            });
          }
          if (page === 1 && !newReviews.length) {
            // Well, we can get empty review [] during infinite scroll(pulling more review) as well
            // and that call will obviously have 'page' param, and hence we can run this block
            // when rating filter(SORT/Starts) is changed.
            this.setState({
              noReview: `There are no ${rating}-star reviews for this product`
            });
          }
        }).catch(err => {
          console.error(`[Reviews Error]: ${err}`);
        })
        .finally(() => {
          this.toggleLoading(false);
        });
    }
  }

  toggleLoading(flag = true) {
    this.setState({
      loading: flag
    });
  }

  initInfiniteScroll() {
    if (this.refInfiniteScroll) {
      const observer = new IntersectionObserver(entries => {
        if (entries[0].isIntersecting && this.state.noReview === null) {
          this.getReviews({
            page: this.state.page + 1
          });
        }
      });
      observer.observe(this.refInfiniteScroll);
    }
  }

  render() {
    const {
      styleID,
      reviewsCount,
      reviews,
      pageName,
      loading,
      onZoom,
      sort,
      rating,
      imageCount,
      topImages,
      topReviews,
      topImageReviews,
      noReview
    } = this.state;
    const combinedStyles = `${styles.reviewsContainer} ${pageName === 'reviews' ? styles.ownPage : ''} ${onZoom ? styles.zIndexMagic : ''}`.trim();
    if (!reviewsCount) {
      return (
        <div className={pageName === 'reviews' ? styles.noReviewDetailedRating : ''} >
          <DetailedRating data={get(window, '__myx.pdpData.ratings')} inPdp={pageName !== 'reviews'} />
          {
            pageName === 'reviews' ? <div className={styles.noReviewsAvailable}>There are no reviews for this product</div> : ''
          }
        </div>
      );
    }

    return (
      <main className={combinedStyles}>
        {pageName === 'reviews' && <BreadCrumbs />}
        <div className={styles.flexContainer}>
          {
            pageName === 'reviews' && (
              <div className={styles.flexProduct}>
                <ProductDetails />
                <Loader show={loading} />
              </div>
            )
          }
          <div className={styles.flexReviews}>
            <DetailedRating data={get(window, '__myx.pdpData.ratings')} inPdp={pageName !== 'reviews'} />

            <ImageReviewsGallery
              styleID={styleID}
              onZoom={this.onZoom}
              topImages={topImages}
              topReviews={topReviews}
              topImageReviews={topImageReviews}
              imageCount={imageCount} />

            <div className={styles.headline}>
              Customer Reviews ({reviewsCount})
              {pageName === 'reviews' && (
                <DropdownFilter sort={sort} rating={rating} onFilterChange={this.onFilterChange} />
              )}
            </div>

            <div className={styles.userReviewsContainer}>
              {
                noReview === null
                  ? reviews.map((review, key) => (<UserReview
                    reviewId={review.id || review.reviewId}
                    onZoom={this.onZoom}
                    onVote={this.onVote}
                    border={pageName === 'reviews'}
                    key={key}
                    images={review.images}
                    downvotes={parseInt(review.downvotes, 10)}
                    upvotes={parseInt(review.upvotes, 10)}
                    userName={review.userName}
                    reviewText={review.reviewText || review.review}
                    userRating={parseInt(review.userRating, 10)}
                    timestamp={parseInt(review.timestamp || review.updatedAt, 10)} />
                  ))
                  : <div className={styles.noReview}>{noReview}</div>
              }
            </div>
            {
              pageName !== 'reviews' && reviewsCount > topReviews.length && (
                <a href={`/reviews/${styleID}`} className={styles.allReviews}>View all {reviewsCount} reviews</a>
              )
            }
            {
              pageName === 'reviews' && (
                <div
                  className={styles.infiniteScroll}
                  ref={(e) => { this.refInfiniteScroll = e; }} />
              )
            }
          </div>
        </div>
      </main>
    );
  }
}

Reviews.propTypes = {
  reviewInfo: React.PropTypes.object,
  location: React.PropTypes.object,
  onZoom: React.PropTypes.func
};

Reviews.defaultProps = {
  onZoom: () => {}
};

export default Reviews;

