import React from 'react';
import { get } from 'lodash';
import LazyLoad from 'react-lazyload';
import Skeleton from '../Skeleton';
import styles from './product-details.css';

export default function ProductDetails() {
  const getBrandMarkup = () => {
    let brandName = get(window, '__myx.pdpData.analytics.brand') || get(window, '__myx.pdpData.brand.name');
    return brandName ? <h1 className={styles.brand}>{brandName}</h1> : null;
  };

  const getDescMarkup = () => {
    const brandName = get(window, '__myx.pdpData.analytics.brand') || get(window, '__myx.pdpData.brand.name');
    const name = get(window, '__myx.pdpData.name');
    return name
      ? <h1 className={`${styles.name} ${styles.bb1}`}>
          {name.replace(`${brandName} `, '')}
      </h1> : null;
  };

  const getPrice = () => {
    const discountData = get(window, '__myx.pdpData.price.discount');
    const mrp = get(window, '__myx.pdpData.price.mrp');
    if (discountData) {
      const discountedPrice = get(window, '__myx.pdpData.price.discounted');
      return discountedPrice || mrp;
    }
    return mrp;
  };

  const getDiscountContainer = () => {
    const mrp = get(window, '__myx.pdpData.price.mrp');
    const discountData = get(window, '__myx.pdpData.price.discount');
    const discountedPrice = get(window, '__myx.pdpData.price.discounted');
    const finalPrice = getPrice();

    if (typeof discountData !== 'undefined') {
      const discountText = get(discountData, 'label');
      if (discountedPrice < mrp) {
        return (
          <p className={styles.discountContainer}>
            <span className={styles.price}><strong>{`Rs. ${finalPrice}`}</strong></span>
            <span className={styles.mrp}><s>Rs. {mrp}</s></span>
            <span className={styles.discount}>{discountText || ''}</span>
          </p>
        );
      }
      return (<p className={styles.discountContainer}>
        <span className={styles.price}><strong>{`Rs. ${finalPrice}`}</strong></span>
        <span className={styles.discount} dangerouslySetInnerHTML={{ __html: discountText || '' }} />
      </p>);
    }
    return (<p className={styles.discountContainer}>
      <strong className={styles.price}>{`Rs. ${finalPrice}`}</strong>
    </p>);
  };

  const getThumbImage = () => {
    const image = get(window, '__myx.pdpData.media.albums.0.images.0');
    const imageURL = image.imageURL ? image.imageURL.replace(/^http:\/\//i, 'https://') : null;
    return (
      <LazyLoad height={500} offset={100} once>
        <div className={styles.imageWrapper}>
          <div className={styles.image} style={{ backgroundImage: `url(${imageURL})` }} />
          <Skeleton />
        </div>
      </LazyLoad>
    );
  };

  return (
    <div className={styles.container}>
      {getThumbImage()}
      {getBrandMarkup()}
      {getDescMarkup()}
      {getDiscountContainer()}
    </div>
  );
}
