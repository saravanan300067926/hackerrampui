import React from 'react';
import { get } from 'lodash';
import styles from './breadcrumbs.css';

function BreadCrumbs() {
  const link = `${window.location.protocol}//${window.location.host}/${get(window, '__myx.pdpData.id')}`;
  const caption = get(window, '__myx.pdpData.name');
  return (
    <div className={styles.container}>
      <a href={link} className={styles.link}>{caption}</a>
      <span className={styles.separator}>/</span>
      <a className={styles.boldLink}>All Reviews</a>
    </div>
  );
}

export default BreadCrumbs;
