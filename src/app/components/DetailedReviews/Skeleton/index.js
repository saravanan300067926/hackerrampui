import React from 'react';
import styles from './skeleton.css';

export default function () {
  return <div className={styles.skeletonLoader} />;
}
