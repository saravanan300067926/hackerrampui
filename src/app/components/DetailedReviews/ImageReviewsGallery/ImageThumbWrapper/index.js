import React from 'react';
import LazyLoad from 'react-lazyload';
import Skeleton from '../../Skeleton';
import { applyTransformation } from '../../helpers';
import styles from './image-thumb-wrapper.css';

export default function ImageThumbWrapper({ images = [], total = 0, handleThumbClick, showLast }) {
  if (!images.length) {
    return null;
  }
  return (
    <div className={styles.imageThumbWrapper}>
      {total ? <div className={styles.headline}>Customer Photos ({total})</div> : null}
      <div className={styles.container}>
        {
          images.map((image, imageIndex) => {
            const imgSrc = applyTransformation(image.imageUrl || image.url, { h: 150, q: 75, w: 150 });
            const isLast = (images.length >= 4 && (images.length - 1) === imageIndex);
            return (
              <div key={imageIndex} onClick={() => handleThumbClick({ image, imageIndex, isLast, reviewId: image.reviewId })} >
                <ImageThumb
                  imgSrc={imgSrc}
                  total={total - imageIndex}
                  isLast={!showLast ? isLast : false} />
              </div>
            );
          })
        }
      </div>
    </div>
  );
}
ImageThumbWrapper.propTypes = {
  images: React.PropTypes.array,
  handleThumbClick: React.PropTypes.func,
  total: React.PropTypes.number,
  showLast: React.PropTypes.bool
};


const ImageThumb = ({ imgSrc, total, isLast }) => (
  <LazyLoad
    height={75}
    offset={200}
    once>
    <div className={`${styles.thumb} ${isLast ? styles.thumbLast : ''}`}>
      <img className={styles.image} src={imgSrc} />
      {isLast && <span className={styles.imageCount}>+{total}</span>}
      <Skeleton />
    </div>
  </LazyLoad>
);
ImageThumb.propTypes = {
  imgSrc: React.PropTypes.string,
  isLast: React.PropTypes.bool,
  total: React.PropTypes.number
};
