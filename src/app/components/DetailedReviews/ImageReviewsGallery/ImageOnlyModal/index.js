import React from 'react';
import Close from '../../../icons/Close';
import Skeleton from '../../Skeleton';
import LazyLoad from 'react-lazyload';
import styles from './image-only-modal.css';
import { applyTransformation } from '../../helpers';

// Number of images can fit inside the imageWrapper and api calls to be made
const MIN_BATCH = {
  size: 3,
  canFit: 12
};
const MAX_BATCH = {
  size: 5,
  canFit: 48
};
class ImageOnlyModal extends React.Component {
  constructor(props) {
    super(props);
    this.currentCount = 0;
    this.refImageWrapper = null;

    [
      'getImageHTML',
      'handleThumbClick',
      'handleBackdropClick',
      'initScrollToLoad'
    ].forEach(method => (this[method] = this[method].bind(this)));
  }

  componentDidMount() {
    this.initScrollToLoad();
  }

  getImageHTML() {
    const reviews = this.props.reviews || [];
    const combinedStyles = `${styles.imageLazyWrapper} ${this.props.total <= 10 ? styles.imageMinEnable : ''}`;
    return reviews.length && reviews.map(review => {
      const reviewImages = review.reviewImages || review.images;
      return reviewImages.map((image, imageIndex) => {
        this.currentCount++;
        const imageURL = applyTransformation(image.imageUrl || image.url, { h: 200, q: 75, w: 200 });
        return (
          <LazyLoad key={imageIndex}>
            <div className={combinedStyles} onClick={() => this.handleThumbClick({ image, imageIndex })}>
              <img
                className={styles.image}
                src={imageURL} />
              <Skeleton />
            </div>
          </LazyLoad>
        );
      });
    });
  }

  handleThumbClick(params) {
    this.props.onCloseModal(false);
    this.props.handleThumbClick(params);
  }

  handleBackdropClick() {
    this.props.onCloseModal(false);
  }

  initScrollToLoad() {
    if (this.refImageWrapper) {
      const { reviews, total, isLoading } = this.props;
      const { size, canFit } = total <= 10 ? MIN_BATCH : MAX_BATCH;
      const last = reviews[reviews.length - 1].last;
      if (this.currentCount < canFit && !last && !isLoading) {
        const observer = new IntersectionObserver(entries => {
          if (entries[0].isIntersecting) {
            this.props.handleLoadMore(size);
          }
        });
        observer.observe(this.refImageWrapper);
      }
    }
  }

  render() {
    return (
      <div className={styles.container} onClick={this.handleBackdropClick}>
        <div className={styles.modalContainer} onClick={e => e.stopPropagation()}>
          <header className={styles.header}>
            <span className={styles.closeWrapper} onClick={this.handleBackdropClick}>
              <Close fill={"#7e808c"}/>
            </span>
            All Customer Photos
          </header>
          <div className={styles.imageWrapper}>
            {this.getImageHTML()}
            <div className={styles.placeholder} ref={(el) => { this.refImageWrapper = el; }} />
          </div>
        </div>
      </div>
    );
  }
}

ImageOnlyModal.propTypes = {
  reviews: React.PropTypes.array,
  handleThumbClick: React.PropTypes.func,
  onCloseModal: React.PropTypes.func,
  total: React.PropTypes.number,
  handleLoadMore: React.PropTypes.func,
  isLoading: React.PropTypes.bool
};

export default ImageOnlyModal;
