import React from 'react';
import { get } from 'lodash';
import ChevronLeft from '../../../icons/ChevronLeft';
import ChevronRight from '../../../icons/ChevronRight';
import Close from '../../../icons/Close';
import Skeleton from '../../Skeleton';
import LazyLoad from 'react-lazyload';
import styles from './image-reviews-modal.css';
import UserReview from '../../UserReview';
import { applyTransformation } from '../../helpers';

class ImageReviewsModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    [
      'getCurrentImage',
      'handleClick',
      'checkDisable',
      'handleBackdropClick'
    ].forEach(method => (this[method] = this[method].bind(this)));
  }

  getCurrentImage() {
    const reviewImages = get(this.props.review, 'reviewImages') || get(this.props.review, 'images') || [];
    if (reviewImages.length) {
      const { imageIndex } = this.props;
      const imageSrc = reviewImages[imageIndex].imageUrl || reviewImages[imageIndex].url;
      return applyTransformation(imageSrc, { w: 600, q: 80 });
    }
    return null;
  }

  handleClick(n) {
    const { imageIndex } = this.props;
    const images = get(this.props.review, 'reviewImages') || get(this.props.review, 'images') || [];
    const newIndex = imageIndex + n;
    if (newIndex >= images.length || newIndex < 0) {
      this.props.handlePaginateReviews(n);
    } else {
      this.props.setActiveImageIndex(newIndex);
    }
  }

  checkDisable() {
    try {
      const review = this.props.review;
      const reviewImages = get(review, 'reviewImages') || get(review, 'images') || [];
      const { imageIndex } = this.props;
      const isPrevDisabled = review.first && imageIndex === 0;
      const isNextDisabled = review.last && imageIndex === reviewImages.length - 1;
      return {
        isPrevDisabled,
        isNextDisabled
      };
    } catch (err) {
      console.error(`[REVIEWS ERROR]: ${err}`);
    }
    return false;
  }

  handleBackdropClick() {
    this.props.onCloseModal(false);
  }

  render() {
    const { imageIndex } = this.props;
    const reviewImages = get(this.props.review, 'reviewImages') || get(this.props.review, 'images') || [];
    const review = get(this.props, 'review');
    const { isPrevDisabled, isNextDisabled } = this.checkDisable();
    return (
      <div className={styles.container} onClick={this.handleBackdropClick}>
        <div className={styles.modalContainer} onClick={e => e.stopPropagation()}>
          <div className={styles.imageWrapper}>
            <LazyLoad>
              <div className={styles.lazyLoaderContainer}>
                <div
                  className={styles.image}
                  style={{ backgroundImage: `url(${this.getCurrentImage()})` }} />
              </div>
            </LazyLoad>
            <div className={styles.buttonContainer}>
              <div className={`${styles.actionButton} ${styles.closeModalButton}`} onClick={() => this.props.onCloseModal(false)}>
                {/*<span className={`myntraweb-sprite-2x ${styles.reviewClose}`} />*/}
                <Close />
              </div>
              <div
                className={`${styles.actionButton} ${isPrevDisabled ? styles.disableAction : ''}`}
                onClick={() => {
                  if (!isPrevDisabled) {
                    this.handleClick(-1);
                  }
                }}>
                <ChevronLeft/>
              </div>
              <div
                className={`${styles.actionButton} ${isNextDisabled ? styles.disableAction : ''}`}
                onClick={() => {
                  if (!isNextDisabled) {
                    this.handleClick(1);
                  }
                }}>
                <ChevronRight />
              </div>
            </div>

            <div className={styles.imageMiniThumbWrapper}>
              {
                reviewImages.length && reviewImages.map((img, key) => {
                  const imgSrc = applyTransformation(img.imageUrl || img.url, { h: 120, q: 75, w: 120 });
                  return (
                    <LazyLoad key={key}>
                      <div className={styles.lazyLoaderContainer}>
                        <div
                          onClick={() => this.props.setActiveImageIndex(key)}
                          className={`${styles.imageThumb} ${imageIndex === key ? styles.active : ''}`}
                          style={{ backgroundImage: `url(${imgSrc})` }} />
                        <Skeleton />
                      </div>
                    </LazyLoad>
                  );
                })
              }
            </div>

          </div>
          <div className={styles.reviewWrapper}>
            <UserReview
              fromModal={true}
              reviewId={review.id || review.reviewId}
              onZoom={review.onZoom}
              onVote={review.onVote}
              downvotes={parseInt(review.downvotes, 10)}
              upvotes={parseInt(review.upvotes, 10)}
              userName={review.userName}
              reviewText={review.reviewText}
              userRating={parseInt(review.userRating, 10)}
              timestamp={parseInt(review.timestamp || review.updatedAt, 10)} />
          </div>
        </div>
      </div>
    );
  }
}

ImageReviewsModal.propTypes = {
  imageIndex: React.PropTypes.number,
  review: React.PropTypes.object,
  onCloseModal: React.PropTypes.func,
  handlePaginateReviews: React.PropTypes.func,
  setActiveImageIndex: React.PropTypes.func
};

export default ImageReviewsModal;
