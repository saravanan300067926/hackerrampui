import React from 'react';
import ImageThumbWrapper from './ImageThumbWrapper';
import ImageReviewsModal from './ImageReviewsModal';
import ImageOnlyModal from './ImageOnlyModal';
import Loader from '../../Loader';
import { uniqBy } from 'lodash';
import { mapImagesToReview, getImageCount } from '../helpers';
import Client from '../../../services';
import config from '../../../config';

/*
  This Component is serving two purposes or in two places.
  1) On paginated image gallery reviews. Here we get Initial top images and rest we call api
  2) Inside user review. here we get review data from the prop.
*/
const INITIAL_BATCH = {
  index: 1,
  size: 10
};

export default class ImageReviewsGallery extends React.Component {
  constructor(props) {
    super(props);

    this.activeBatchIndex = INITIAL_BATCH.index;
    this.batchSize = INITIAL_BATCH.size;

    this.state = this.setInitialState.call(this.props);

    [
      'getReviews',
      'setActiveImageIndex',
      'handlePaginateReviews',
      'appendFirstLastFlag',
      'handleImageLoadMore',
      'toggleLoading',
      'handleThumbClick',
      'toggleImageReviewModal',
      'toggleImageOnlyModal',
      'handleEscapePressed'
    ].forEach(method => (this[method] = this[method].bind(this)));
  }

  componentDidMount() {
    if (this.props.hasOwnData) {
      this.appendFirstLastFlag();
    }
    this.attachEscapeKey();
  }

  componentWillUnmount() {
    this.detachEscapeKey();
  }

  getReviews() {
    const hasReachedEnd = this.state.reviews[this.state.reviews.length - 1].last || false;
    if (!hasReachedEnd) {
      const style = this.props.styleID;
      const url = `${config('reviewsByBatch')}${style}?size=${this.batchSize}&page=${this.activeBatchIndex++}`;
      return Client.get(url)
        .then(({ body }) => {
          const { reviews = [], images = [] } = body;
          if (reviews.length || images.length) {
            // Need to map the images against review, as API response sucks
            return Promise.resolve(mapImagesToReview(images, reviews));
          }
          return Promise.resolve(false);
        }).catch(err => {
          this.toggleLoading(false);
          console.error(`[Reviews Error]: ${err}`);
          return Promise.reject(false);
        });
    }
    return Promise.resolve(false);
  }

  setActiveImageIndex(index) {
    this.setState({
      activeImageIndex: index
    });
  }

  setInitialState() {
    return {
      activeReviewIndex: 0,
      activeImageIndex: 0,
      showImageReviewsModal: false,
      showImageOnlyModal: false,
      topImages: this.topImages,
      imageCount: this.imageCount,
      hasOwnData: this.hasOwnData,
      loading: false,
      reviews: this.hasOwnData
        ? this.review
        : mapImagesToReview(
            this.topImages,
            [...this.topReviews, ...this.topImageReviews]
          )
    };
  }

  async handlePaginateReviews(n) {
    if (n) {
      const { activeReviewIndex, reviews } = this.state;
      const newReviewIndex = activeReviewIndex + n;
      const review = reviews[newReviewIndex];
      if (review) {
        //-- Executes when we already have reviews in the sate --//
        // When user is traversing backward, we have to show the last image from the list
        // When user is traversing forward, then the index is 0 because that's the starting point.
        const newReviewImgIndex = newReviewIndex > activeReviewIndex ? 0 : review.reviewImages.length - 1;
        this.setState({
          activeReviewIndex: newReviewIndex,
          activeImageIndex: newReviewImgIndex
        }, this.appendFirstLastFlag);
      } else {
        // Getting new reviews
        this.toggleLoading();
        const newReviews = await this.getReviews();
        this.setState({
          loading: false,
          activeImageIndex: 0,
          reviews: uniqBy([...reviews, ...newReviews], e => e.reviewId)
        }, () => {
          // Append review index, if api does not return empty obj.
          this.appendFirstLastFlag(newRev => {
            const index = newRev[newRev.length - 1].last ? activeReviewIndex : newReviewIndex;
            this.setState({
              activeReviewIndex: index
            });
          });
        });
      }
    }
  }

  appendFirstLastFlag(callback = () => {}) {
    let reviews = this.props.hasOwnData ? this.state.reviews : [...this.state.reviews];
    const { hasOwnData, imageCount } = this.props;
    if (!hasOwnData) {
      const currReviewsImgCount = getImageCount(reviews);
      reviews[0].first = true;
      if (currReviewsImgCount === imageCount) {
        reviews[reviews.length - 1].last = true;
      }
    } else {
      // When we have an only one review(object) being passed from UserReview component.
      reviews = {
        ...reviews,
        last: true,
        first: true
      };
    }
    this.setState({ reviews }, () => {
      callback(reviews);
    });
  }

  async handleImageLoadMore(size = 1) {
    this.toggleLoading();
    let { reviews: oldReviews } = this.state;
    if (this.activeBatchIndex === 1) {
      oldReviews = [];
    }
    let newReviews = [];
    try {
      while (size !== 0) {
        const reviews = await this.getReviews();
        if (reviews !== false) {
          newReviews = [...newReviews, ...reviews];
          this.setState({
            reviews: [...oldReviews, ...newReviews]
          });
        } else {
          break;
        }
        size--;
      }
    } catch (err) {
      console.error(`[REVIEWS ERROR]: ${err}`);
    } finally {
      this.toggleLoading(false);
      this.setState({
        activeReviewIndex: 0,
        activeImageIndex: 0
      }, this.appendFirstLastFlag);
    }
  }

  toggleLoading(flag = true) {
    this.setState({
      loading: flag
    });
  }

  handleThumbClick(params) {
    const { hasOwnData, reviews } = this.state;
    if (hasOwnData) {
      this.setState({
        activeImageIndex: params.imageIndex
      }, this.toggleImageReviewModal);
    } else {
      const activeReviewIndex = reviews.map(({ reviewId }) => reviewId).indexOf(params.image.reviewId);
      if (params.isLast) {
        this.setState({
          activeReviewIndex,
          activeImageIndex: params.imageIndex
        }, this.toggleImageOnlyModal);
      } else {
        this.setState({
          activeReviewIndex,
          activeImageIndex: 0
        }, this.toggleImageReviewModal);
      }
    }
  }

  toggleImageReviewModal(flag = true) {
    this.props.onZoom(flag);
    this.setState({
      showImageReviewsModal: flag
    });
  }

  toggleImageOnlyModal(flag = true) {
    this.props.onZoom(flag);
    this.toggleLoading(false);
    this.setState({
      showImageOnlyModal: flag
    });
  }

  attachEscapeKey() {
    document.addEventListener('keydown', this.handleEscapePressed);
  }

  detachEscapeKey() {
    document.removeEventListener('keydown', this.handleEscapePressed, false);
  }

  handleEscapePressed(e) {
    if (e.keyCode === 27) {
      this.toggleImageReviewModal(false);
      this.toggleImageOnlyModal(false);
    }
  }

  render() {
    const {
      topImages,
      imageCount,
      reviews,
      activeReviewIndex,
      activeImageIndex,
      showImageReviewsModal,
      showImageOnlyModal,
      loading,
      hasOwnData
    } = this.state;

    return (
      <div>
        <Loader show={loading} />
        <ImageThumbWrapper
          showLast={hasOwnData}
          images={topImages}
          total={imageCount}
          handleThumbClick={this.handleThumbClick} />
        {
          showImageOnlyModal && !showImageReviewsModal && !hasOwnData ? (
            <ImageOnlyModal
              handleThumbClick={this.handleThumbClick}
              onCloseModal={this.toggleImageOnlyModal}
              handleLoadMore={this.handleImageLoadMore}
              reviews={reviews}
              total={imageCount}
              isLoading={loading} />
          ) : null
        }
        {
          showImageReviewsModal && !showImageOnlyModal ? (
            <ImageReviewsModal
              imageIndex={activeImageIndex}
              handlePaginateReviews={this.handlePaginateReviews}
              onCloseModal={this.toggleImageReviewModal}
              setActiveImageIndex={this.setActiveImageIndex}
              review={hasOwnData ? reviews : reviews[activeReviewIndex] || reviews[activeReviewIndex - 1]} />
          ) : null
        }
      </div>
    );
  }
}

ImageReviewsGallery.propTypes = {
  hasOwnData: React.PropTypes.bool,
  review: React.PropTypes.object,
  topImages: React.PropTypes.array,
  topReviews: React.PropTypes.array,
  topImageReviews: React.PropTypes.array,
  imageCount: React.PropTypes.number,
  styleID: React.PropTypes.number,
  onZoom: React.PropTypes.func
};

ImageReviewsGallery.defaultProps = {
  hasOwnData: false,
  onZoom: () => {}
};
