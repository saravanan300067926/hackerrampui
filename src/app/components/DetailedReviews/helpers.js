export const mapImagesToReview = (images, reviews) => {
  // TODO for V2: improve performance

  // This function maps images to a review.
  // For Eg: [ images[], reviews[] ] => reviews[ images[] ]

  if (!reviews || !images) {
    return null;
  }
  const temp = [];
  images.forEach(image => {
    let existing = false;
    const review = reviews.find(r => r.reviewId === image.reviewId || r.id === image.reviewId);
    for (let i = 0; i < temp.length; i++) {
      const t = temp[i];
      if (t.reviewId === image.reviewId) {
        // Append this new image as we have already seen this review id.
        existing = true;
        temp[i] = {
          ...t,
          reviewImages: [...t.reviewImages, image]
        };
      }
    }
    if (!existing) {
      // Pushing new review with it's images
      const reviewId = review.id || review.reviewId;
      const reviewText = review.review || review.reviewText;
      const timestamp = review.updatedAt || review.timestamp;

      delete review.id;
      delete review.uidx;
      delete review.review;
      delete review.updatedAt;
      delete review.rejectedReason;
      delete review.status;
      delete review.style;

      temp.push({
        ...review,
        reviewId,
        reviewText,
        timestamp,
        reviewImages: [image]
      });
    }
  });
  return temp;
};

export const getImageCount = (reviews) => (
  reviews.reduce((count, review) => count + review.reviewImages.length, 0)
);

export const applyTransformation = (src, attr = {}) => {
  // Cloudinary Transformation
  let props = Object.keys(attr).reduce(
    (url, key) => (url += `${key}_${attr[key]},`),
    ''
  );
  props += 'c_thumb,fl_progressive/';
  const searchKey = 'assets.myntassets.com/';
  const startPos = src.indexOf(searchKey) + searchKey.length;
  return src.substr(0, startPos) + props + src.substr(startPos);
};

export const getHumanDate = timestamp => {
  const d = new Date(parseInt(timestamp, 10));
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  return `${d.getDate()} ${months[d.getMonth()]} ${d.getFullYear()}`;
};
