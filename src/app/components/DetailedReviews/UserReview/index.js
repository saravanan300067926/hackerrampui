import React from 'react';
import Star from '../../icons/Star';
import ThumbsDown from '../../icons/ThumbsDown';
import ThumbsUp from '../../icons/ThumbsUp';
import ImageReviewsGallery from '../ImageReviewsGallery';
import { startCase } from 'lodash';
import { getHumanDate } from '../helpers';
import { isLoggedIn } from '../../../utils';
import styles from './user-review.css';

class UserReview extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { vote: undefined };
    this.handleOnVote = this.handleOnVote.bind(this);
  }
  handleOnVote(vote) {
    if (!this.state.vote && isLoggedIn()) {
      // Set vote only if it is undefined
      this.setState({ vote }, () => {
        this.props.onVote({
          reviewId: this.props.reviewId,
          isUpvote: vote === 'UP'
        });
      });
    }
  }

  render() {
    const {
      userRating,
      reviewText,
      images,
      onZoom,
      userName,
      timestamp,
      upvotes,
      downvotes,
      fromModal
    } = this.props;
    const { vote } = this.state;

    return (
      <div className={`${styles.userReviewWrapper} ${fromModal ? styles.fromModal : ''}`}>
        <div className={styles.main}>
          <div className={styles.starWrapper}>
            <StarRating userRating={userRating} />
          </div>
          <div className={styles.reviewTextWrapper}>{reviewText}</div>
          {
            images && images.length ? (
              <ImageReviewsGallery
                hasOwnData={true}
                onZoom={onZoom}
                topImages={images}
                review={this.props} />
            ) : null
          }
        </div>
        <div className={styles.footer}>
          <div className={styles.left}>
            <span>{startCase(userName.toLowerCase())}</span>
            <span>{getHumanDate(timestamp)}</span>
          </div>
          <div className={styles.right}>
            <Votes
              handleOnVote={this.handleOnVote}
              vote={vote}
              upvotes={upvotes}
              downvotes={downvotes}
              userRating={userRating} />
          </div>
        </div>
      </div>
    );
  }
}
UserReview.propTypes = {
  onZoom: React.PropTypes.func,
  onVote: React.PropTypes.func,
  userName: React.PropTypes.string,
  images: React.PropTypes.array,
  fromModal: React.PropTypes.bool,
  reviewId: React.PropTypes.string,
  reviewText: React.PropTypes.string,
  userRating: React.PropTypes.number.isRequired,
  timestamp: React.PropTypes.number.isRequired,
  upvotes: React.PropTypes.number.isRequired,
  downvotes: React.PropTypes.number.isRequired
};


const StarRating = ({ userRating }) => {
  const stars = ['', styles.oneStar, styles.twoStars, styles.threeStars, styles.fourStars, styles.fiveStars];
  return (
    <span className={`${styles.starRating} ${stars[userRating]}`}>
      {userRating}
      <span className={styles.starIcon}>
         <Star />
      </span>
    </span>
  );
};
StarRating.propTypes = {
  userRating: React.PropTypes.number
};


const Votes = ({ downvotes = 0, upvotes = 0, vote, handleOnVote }) => (
  <div className={styles.votes}>
    <div className={styles.thumb} onClick={() => handleOnVote('UP')}>
      {vote === 'UP' ? (
        <ThumbsUp className={styles.thumbIcon} />
      ) : (
        <ThumbsDown className={`${styles.thumbIcon} ${styles.thumbsDown} ${styles.rotate180}`} />
      )}
      {vote === 'UP' ? upvotes + 1 : upvotes}
    </div>
    <div className={styles.thumb} onClick={() => handleOnVote('DOWN')}>
      {vote === 'DOWN' ? (
        <ThumbsUp className={`${styles.thumbIcon} ${styles.thumbsUp} ${styles.rotate180}`} />
      ) : (
        <ThumbsDown className={styles.thumbIcon} />
      )}
      {vote === 'DOWN' ? downvotes + 1 : downvotes}
    </div>
  </div>
);

Votes.propTypes = {
  vote: React.PropTypes.string,
  upvotes: React.PropTypes.number,
  downvotes: React.PropTypes.number,
  handleOnVote: React.PropTypes.func
};

export default UserReview;
