import React from 'react';
import styles from './dropdown-filter.css';

const Icon = () => <span className={`myntraweb-sprite ${styles.starIcon}`} />;

export default [
  {
    heading: 'SORT BY',
    type: 'SORT',
    data: [
      {
        paramKey: 'sort',
        paramValue: 0,
        caption: 'Most Helpful'
      },
      {
        paramKey: 'sort',
        paramValue: 1,
        caption: 'Most Recent'
      }
    ]
  },
  {
    type: 'DIVIDER'
  },
  {
    heading: 'FILTER BY',
    type: 'RATING',
    data: [
      {
        paramKey: 'rating',
        paramValue: 5,
        caption: <span>5 <Icon /> Rating</span>
      },
      {
        paramKey: 'rating',
        paramValue: 4,
        caption: <span>4 <Icon /> Rating</span>
      },
      {
        paramKey: 'rating',
        paramValue: 3,
        caption: <span>3 <Icon /> Rating</span>
      },
      {
        paramKey: 'rating',
        paramValue: 2,
        caption: <span>2 <Icon /> Rating</span>
      },
      {
        paramKey: 'rating',
        paramValue: 1,
        caption: <span>1 <Icon /> Rating</span>
      }
    ]
  }
];
