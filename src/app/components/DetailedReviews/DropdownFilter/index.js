import React from 'react';
import styles from './dropdown-filter.css';
import filterDropdownData from './filterDropdownData';

export default class DropdownFilter extends React.Component {
  state = {
    openDropdown: false,
    activePlaceholder: filterDropdownData[0].data[0].caption
  };

  getFilterData = () => {
    const { onFilterChange } = this.props;
    const { activePlaceholder } = this.state;
    return filterDropdownData.map((value, key) => {
      const data = [];
      if (value.type === 'DIVIDER') {
        return <div className={styles.divider} key={key} />;
      }
      data.push(
        <div className={styles.heading} key={key}>{value.heading}</div>
      );
      if (value.data) {
        data.push(value.data.map((r, i) => {
          const combinedStyle = [styles.item];
          if (r.caption === activePlaceholder) {
            combinedStyle.push(styles.active);
          }
          return (
            <div
              key={i}
              className={combinedStyle.join(' ')}
              onClick={() => {
                onFilterChange(r);
                this.setState({
                  openDropdown: false,
                  activePlaceholder: r.caption
                });
              }}>
              {r.caption}
            </div>
          );
        }));
      }
      return data;
    });
  }

  render() {
    const { activePlaceholder, openDropdown } = this.state;
    return (
      <div className={styles.dropdownFilterContainer}>
        <div className={styles.active} onClick={() => this.setState(({ openDropdown: open }) => ({ openDropdown: !open }))}>
          {activePlaceholder}
          <span className={`myntraweb-sprite ${styles.arrowIcon} ${openDropdown ? styles.open : ''}`.trim()} />
        </div>
        <div className={`${styles.dropdown} ${openDropdown ? styles.open : ''}`.trim()}>
          {this.getFilterData()}
        </div>
      </div>
    );
  }
}

DropdownFilter.propTypes = {
  sort: React.PropTypes.number,
  rating: React.PropTypes.number,
  onFilterChange: React.PropTypes.func
};
