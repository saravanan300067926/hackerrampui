import React from 'react';
import at from 'v-at';
import { Link } from 'react-router';
import Notify from '../Notify';
import styles from './forgot-password.css';
import Track from '../../utils/track';
import { loadFacebookConnect } from '../../utils';
import config from '../../config';
import Client from '../../services';
import Loader from '../Loader';

const DEFAULT_ERROR_MESSAGE = 'Oops! Something went wrong. Please try again in some time';

class ForgotPassword extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showMessage: false,
      isLoading: false,
      userToken: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getLoginPath = this.getLoginPath.bind(this);
  }

  componentDidMount() {
    loadFacebookConnect();
    this.setState({ userToken: at(window, '__myx_session__.USER_TOKEN') });
    Track.screen('/forgot');
  }

  getLoginPath() {
    const referer = at(this.props, 'location.query.referer');
    let path = '/login';
    if (referer) {
      path = `${path}?referer=${referer}`;
    }
    window.location.href =  path;
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.isLoading) {
      return;
    }

    const parameters = {
      email: this.refs.forgotPassword.email.value,
      xsrf: this.refs.forgotPassword.xsrf.value,
      action: 'forgotPassword'
    };

    Track.event('forgot', 'submit');

    this.setState({
      isLoading: true
    });

    Client.post(config('forgotPassword'), parameters).end((err, response) => {
      if (err) {
        this.refs.notify.error(DEFAULT_ERROR_MESSAGE);
        this.setState({
          isLoading: false
        });
        return;
      }

      if (at(response, 'body.httpCode') === 400) {
        Track.event('forgot', 'error');
        this.setState({
          isLoading: false
        });
        this.refs.notify.error(at(response, 'body.message') || DEFAULT_ERROR_MESSAGE);
      } else {
        Track.event('forgot', 'success');
        this.setState({
          showMessage: true,
          isLoading: false
        });
      }
    });
  }

  renderContent() {
    if (at(this.state, 'showMessage')) {
      return (
        <p className={styles.message}>We have sent a reset password link to your email account.</p>
      );
    }
    return (
      <div>
        <p className={styles.subtitle}>We will send you a link to reset your password.</p>
        <p className={styles.subtitle}>Enter Email Address</p>
        <form className={styles.form} ref="forgotPassword" onSubmit={this.handleSubmit}>
          <input className={styles['user-input']} type="email" name="email" placeholder="Enter email address" />
          <input type="hidden" name="xsrf" value={this.state.userToken} />
          <input className={styles.button} type="submit" value="Send Reset Link" />
        </form>
        <Loader show={this.state.isLoading} />
      </div>
    );
  }

  render() {
    return (
      <div className={styles.container}>
        <div className={styles.box}>
          <p className={styles.title}>Forgot Password ?</p>
          {this.renderContent()}
          <div className={styles['login-link']} onClick={this.getLoginPath}>Login!</div>
        </div>
        <Notify ref="notify" />
      </div>
    );
  }
}

export default ForgotPassword;
