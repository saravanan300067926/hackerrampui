import React from 'react';
import at from 'v-at';
import styles from './ac.css';
import kvpairs from '../../utils/kvpairs';
import months from '../../utils/months';
import cookies from '../../utils/cookies';
import { getTimeRange, getSlots } from '../../utils/slotUtils';
import { isBrowser } from '../../utils';
import AdmissionSlot from './AdmissionSlot';
import Client from '../../services';
import config from '../../config';
import Loader from '../Loader';
import Notify from '../Notify';
import { securify } from '../../utils/securify';
const secure = securify();

class AdmissionControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      slots: this.props.slots || {},
      show: false,
      showMore: false,
      slotSelected: this.getSelectedSlot(this.props),
      loading: false,
      slotUpdated: false
    };
    this.onSlotSelected = this.onSlotSelected.bind(this);
    this.fetchSlots = this.fetchSlots.bind(this);
  }
  componentDidMount() {
    console.log('component did mount');
    if (this.props.showSlotChangeScreenOnly) {
      this.fetchSlots();
    }
  }

  componentWillReceiveProps(props) {
    if (props.showSlotChangeScreenOnly) {
      this.setState({ showMore: true, slotSelected: this.getSelectedSlot(props) });
    }
    this.setState({ slots: props.slots });
  }

  onSlotSelected(selectedSlotId, selectedSlotContext) {
    if (!this.state.loading) {
      this.setState({ slotSelected: { selectedSlotId, selectedSlotContext } });
    }
  }

  onUpdateSlotSuccess(response) {
    if (this.props.getSlotInfo) {
      setTimeout(() => {
        this.props.getSlotInfo(response);
      }, 700);
    }
    const slots = getSlots(true) || {};
    if (this.props.showSlotChangeScreenOnly) {
      this.setState({
        loading: false,
        slotUpdated: true,
        slots
      });
      this.close();
    } else {
      this.setState({
        loading: false,
        slotUpdated: true,
        showMore: false,
        slots
      });
    }
  }

  getSelectedSlot(props) {
    if (props.showSlotChangeScreenOnly) {
      const selectedSlot = at(props.slots, 'sl') || {};
      return { selectedSlotId: selectedSlot.id || '', selectedSlotContext: selectedSlot.c || '' };
    }
    return { selectedSlotId: '', selectedSlotContext: '' };
  }

  getToken() {
    return at(window, '__myx_session__.USER_TOKEN');
  }

  allSlots() {
    const allSlots = this.state.availableSlot || [];
    if (allSlots && allSlots.length) {
      return (
        <div className={styles.slotitems}>
          {
            allSlots.map((slot, i) => {
              const slotSelId = at(this.state.slotSelected, 'selectedSlotId');
              const startDate = new Date(slot.slotStartTime);
              const endDate = new Date(slot.slotEndTime);
              const slotProps = {
                available: slot.isAvailable,
                selected: slot.id === slotSelId,
                startDate,
                endDate
              };
              return (
                <AdmissionSlot
                  key={`slot${i}`}
                  id={slot.id}
                  context={slot.context}
                  slotProps={slotProps}
                  slotClickHandler={this.onSlotSelected}
                  slotSelected={this.state.slotSelected} />
              );
            })
          }
        </div>
      );
    }
    return null;
  }

  slotScreen() {
    if (!this.state.showMore && !this.props.showSlotChangeScreenOnly) {
      const slotSelected = at(this.state.slots, 'sl');
      this.state.slotSelected.selectedSlotId = slotSelected.id;
      this.state.slotSelected.selectedSlotContext = slotSelected.context;
      const startDate = new Date(slotSelected.st);
      const endDate = new Date(slotSelected.et);

      let kvdata = {
        'title': 'GREAT!',
        'p1': 'You\'ve won an exclusive early access to the EORS sale.',
        'p2': 'Buy before the sale opens for all!',
        'image': 'https://constant.myntassets.com/web/assets/img/11481093702784-eors_logo.png'
      };
      try {
        kvdata = JSON.parse(kvpairs('admissioncontrol.popup.data')) || kvdata;
      } catch (e) {
        // console.log('Error, falling back to default values');
      }
      return (
        <div className={styles.current}>
          <div className={styles.cinner}>
            <div className={styles.title}>{kvdata.title}</div>
            <p>{kvdata.p1}</p>
            <p>{kvdata.p2}</p>
            <div className={styles.slot}>
              <div className={styles.image}>
                <img src={secure(kvdata.image)} />
              </div>
              <div className={styles.selected}>
                <div className={styles.date}>
                  {startDate.getDate()} {months[startDate.getMonth()]} {startDate.getFullYear()}
                </div>
                <div className={styles.time}>
                {getTimeRange(startDate, endDate)}
                </div>
              </div>
            </div>
            {!this.state.slotUpdated ?
              (<div className={`${styles.moreslots} ${styles.show}`} onClick={() => { this.flipScreen(); }}>
                <span>CHECK ANOTHER TIME SLOT</span>
              </div>) :
              (<div></div>)
            }
          </div>
          <div className={styles.confirm} onClick={() => { this.close(); }}>
            I'LL BE THERE
          </div>
        </div>
      );
    }
    return null;
  }

  flipScreen() {
    if (this.props.showSlotChangeScreenOnly) {
      this.close();
    }
    if (!this.state.showMore) {
      this.fetchSlots();
    } else {
      this.setState({
        showMore: !this.state.showMore
      });
    }
  }


  actions() {
    return (
      <div className={styles.actions}>
        <div className={styles.cancel} onClick={() => { this.flipScreen(); }}>
          CANCEL
        </div>
        <div className={styles.confirm} onClick={() => { this.confirm(); }}>
          CONFIRM
        </div>
      </div>
    );
  }

  moreSlots() {
    if (this.state.slotsNotAvailable) {
      return <div className={styles.slotsNotAvailable}> No More Slots Available </div>;
    }
    if (this.state.showMore || this.props.showSlotChangeScreenOnly) {
      const slotSelected = at(this.state.slots, 'sl');
      const startDate = new Date(slotSelected.st);
      return (
        <div className={styles.slots}>
          <div className={styles.title}>
            TIME SLOTS
            <div className={styles.date}>
              {startDate.getDate()} {months[startDate.getMonth()]} {startDate.getFullYear()}
            </div>
            {this.allSlots()}
            {this.actions()}
          </div>
        </div>
      );
    }
    return null;
  }

  updateSlot() {
    if (!this.state.loading) {
      const selectedSlotId = this.state.slotSelected.selectedSlotId;
      const selectedSlotContext = this.state.slotSelected.selectedSlotContext;
      const data = {
        id: selectedSlotId,
        context: selectedSlotContext
      };
      if (data.id === at(this.state, 'slots.sl.id')) {
        this.close();
        return;
      }

      this.setState({ loading: true });
      Client.put(`${config('admission')}/updateSlot`, data, { 'X-CSRF-TOKEN': this.getToken() }).end((err, response) => {
        if ((err || at(response, 'body.status.statusType') === 'ERROR')) {
          this.setState({ loading: false });
          this.refs.notify.error('The slot you selected is full. You can either try selecting another slot.' +
            ' Or if you cancel, the previously allocated slot will still be reserved for you.');
        } else if (response) {
          this.onUpdateSlotSuccess(response);
        }
      });
    }
  }

  confirm() {
    this.updateSlot();
  }

  close() {
    if (!this.state.loading) {
      // set sts cookie
      cookies.set('sts', 1, 1);
      this.setState({
        showMore: false
      });
      this.enableScroll();
      this.setState({ slotSelected: { selectedSlotId: '', selectedSlotContext: '' } });
      this.props.hideSaleSlot();
    }
  }

  fetchSlots() {
    if (this.state.loading) {
      return;
    }
    this.setState({
      loading: true
    });
    Client.get(`${config('admission')}/getSlot`).end((err, response) => {
      const availableSlot = at(response, 'body.data.availableSlots') || [];
      if (availableSlot.length === 0) {
        this.setState({
          slotsNotAvailable: true,
          showMore: !this.state.showMore.slots,
          availableSlot,
          loading: false
        });
        return;
      }
      this.setState({
        showMore: !this.state.showMore.slots,
        availableSlot,
        loading: false
      });
    });
    return;
  }

  enableScroll() {
    document.body.style.overflowY = 'auto';
    document.body.style.position = 'static';
  }

  disableScroll() {
    document.body.style.overflowY = 'hidden';
    document.body.style.position = 'fixed';
  }

  render() {
    if (!isBrowser()) {
      return null;
    }
    const slotsShownCookie = cookies.get('sts');
    if (slotsShownCookie && !this.props.showSlotChangeScreenOnly) {
      return null;
    }
    if (
      this.props.showSlotChangeScreenOnly &&
      this.state.loading &&
      !this.state.availableSlot
    ) {
      return <Loader show={this.state.loading} />;
    }
    if (this.props.showSlotPopup && at(this.state.slots, 'sl')) {
      this.disableScroll();
      return (
        <div className={styles.container}>
          <div className={styles.inner}>
            <div className={`myntraweb-sprite ${styles.close}`} onClick={() => { this.close(); }} ></div>
            {this.slotScreen()}
            {this.moreSlots()}
          </div>
          <Notify ref="notify" />
          <Loader show={this.state.loading} />
        </div>
      );
    }
    return null;
  }
}

AdmissionControl.propTypes = {
  hideSaleSlot: React.PropTypes.func.isRequired,
  getSlotInfo: React.PropTypes.func,
  showSlotPopup: React.PropTypes.bool.isRequired,
  slots: React.PropTypes.object.isRequired,
  showSlotChangeScreenOnly: React.PropTypes.bool
};

export default AdmissionControl;
