import React from 'react';
import styles from './admissionSlot.css';
import { getTimeRange } from '../../../utils/slotUtils';

class AdmissionSlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props.slotProps,
      ...this.props.slotSelected
    };
  }

  componentWillReceiveProps(props) {
    if (props.slotSelected.selectedSlotId !== this.props.id) {
      this.setState({ selected: false, selectedSlotId: props.slotSelected.selectedSlotId, selectedSlotContext: props.slotSelected.selectedSlotContext });
    }
  }

  handleSlotClick(event) {
    if (this.state.selected || !this.state.available) {
      event.preventDefault();
      event.stopPropagation();
      return;
    }
    this.setState({ selected: true });
    this.props.slotClickHandler(this.props.id, this.props.context);
  }

  render() {
    return (
      <div
        className={`${styles.slot} ${!this.state.available ? styles.disabled : ''} ${this.state.selected ? styles.selected : ''}`}
        onClick={(e) => { this.handleSlotClick(e); }}>
        {!this.state.available ? <span> - TIME SLOT FULL - </span> : ''}
        {getTimeRange(this.state.startDate, this.state.endDate)}
      </div>
    );
  }
}

AdmissionSlot.propTypes = {
  id: React.PropTypes.string.isRequired,
  context: React.PropTypes.string.isRequired,
  slotProps: React.PropTypes.object.isRequired,
  slotSelected: React.PropTypes.object.isRequired,
  slotClickHandler: React.PropTypes.func.isRequired
};

export default AdmissionSlot;
