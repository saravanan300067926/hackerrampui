import React from 'react';
import Client from '../../../services';
import config from '../../../config';
import { isLoggedIn } from '../../../utils';
import at from 'v-at';
import PropTypes from 'prop-types';
import styles from './add-to-cart.css';

const AddToCartButton = ({ addedToCart, toggleSizeSelector, className, disable }) => (
  <button className={`${styles.btnAddToCart} ${className} ${disable ? styles.disabled : ''}`} onClick={toggleSizeSelector}>
    <span className={`myntraweb-sprite ${styles['icon-bag']} ${styles['custom-icon']}`} />
    {addedToCart ? 'Go to Bag' : 'Add To Bag'}
  </button>
);
AddToCartButton.propTypes = {
  toggleSizeSelector: PropTypes.func,
  addedToCart: PropTypes.bool,
  className: PropTypes.string,
  disable: PropTypes.bool
};
AddToCartButton.defaultProps = {
  className: '',
  disable: false
};


const SizeSelector = ({ sizes, productStyleId, setSelectedSize, show }) => {
  if (sizes && sizes.length && show) {
    return (
      <div className={styles.sizeSelectorButtonWrapper}>
        {
          sizes.map((styleOption, index) => {
            if (styleOption.originalStyle === false && productStyleId !== styleOption.styleId) {
              return (
                <button className={styles.sizeButton} key={index} onClick={() => setSelectedSize(styleOption.skuId)} >
                  <p className={styles['unified-size']}>
                    {styleOption.label}
                    {
                      styleOption.discountedPrice || styleOption.price
                        ? <span className={styles['sku-price']}> - Rs. {styleOption.discountedPrice || styleOption.price}</span>
                        : null
                    }
                  </p>
                </button>
              );
            }
            return (
              <button className={styles.sizeButton} key={index} onClick={() => setSelectedSize(styleOption.skuId)}>
                <p className={styles['unified-size']}>
                  {styleOption.label}
                  {
                    styleOption.discountedPrice || styleOption.price
                      ? <span className={styles['sku-price']}> - Rs. {styleOption.discountedPrice || styleOption.price}</span>
                      : null
                  }
                </p>
              </button>
            );
          })
        }
      </div>
    );
  }
  return null;
};
SizeSelector.propTypes = {
  sizes: PropTypes.array,
  show: PropTypes.bool,
  setSelectedSize: PropTypes.func,
  productStyleId: PropTypes.number
};


export default class AddToCart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      showSizeSelector: false,
      addedToCart: false,
      sizes: this.props.sizes,
      selectedSize: null,
      response: null
    };
    [
      'toggleSizeSelector',
      'setSelectedSize',
      'callAddToCart',
      'getReferer'
    ].forEach(
      method => (this[method] = this[method].bind(this))
    );
  }

  setSelectedSize(selectedSize) {
    if (selectedSize) {
      this.setState({ selectedSize }, () => {
        this.callAddToCart();
      });
    }
  }

  getReferer(url = '/') {
    if (typeof window !== 'undefined') {
      return `${url}?referer=${window.location.href}`;
    } return `${url}`;
  }

  toggleSizeSelector() {
    const { redirectToCheckout, loginRequired, onError, disable } = this.props;
    if (disable) {
      return false;
    }
    if (!isLoggedIn() && loginRequired) {
      onError('Please re-confirm your login once again');
      setTimeout(() => {
        window.location.href = this.getReferer('https://www.myntra.com/login');
      }, 4000);
      return false;
    }
    if (this.state.addedToCart && redirectToCheckout) {
      window.location.href = 'https://www.myntra.com/checkout/cart';
      return false;
    }
    if (this.state.sizes && this.state.sizes.length > 1) {
      this.setState({ showSizeSelector: true });
    } else {
      this.setSelectedSize(this.state.sizes[0].skuId);
    }
    return true;
  }

  callAddToCart() {
    if (this.state.selectedSize) {
      const headers = {
        'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN')
      };
      const parameters = {
        skuid: this.state.selectedSize,
        styleid: this.props.productStyleId,
        xsrf: at(window, '__myx_session__.USER_TOKEN')
      };
      Client.post(config('cart'), parameters, headers).end((err) => {
        if (err) {
          console.error(`[ADD TO CART ERROR] : ${err}`);
          this.props.onError('Something went wrong');
        } else {
          this.setState({
            showSizeSelector: false,
            isLoading: false,
            addedToCart: true
          }, () => this.props.onSuccess('Added to cart'));
        }
      });
    }
  }

  render() {
    return (
      <div className={styles.container}>
        <AddToCartButton
          disable={this.props.disable}
          className={this.props.btnClassName}
          toggleSizeSelector={this.toggleSizeSelector}
          addedToCart={this.state.addedToCart} />
        <SizeSelector
          show={this.state.showSizeSelector}
          sizes={this.state.sizes}
          productStyleId={this.props.productStyleId}
          setSelectedSize={this.setSelectedSize} />
      </div>
    );
  }

}
AddToCart.propTypes = {
  sizes: PropTypes.array,
  productStyleId: PropTypes.number,
  redirectToCheckout: PropTypes.bool,
  loginRequired: PropTypes.bool,
  onSuccess: PropTypes.func,
  onError: PropTypes.func,
  btnClassName: PropTypes.string,
  disable: PropTypes.bool
};
AddToCart.defaultProps = {
  sizes: [],
  productStyleId: null,
  redirectToCheckout: false,
  loginRequired: false,
  onSuccess: () => {},
  onError: () => {},
  btnClassName: '',
  disable: false
};
