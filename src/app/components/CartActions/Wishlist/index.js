import React from 'react';
import Client from '../../../services';
import { isLoggedIn } from '../../../utils';
import at from 'v-at';
import PropTypes from 'prop-types';
import styles from './wishlist.css';

export default class Wishlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addedToWishlist: false,
      productStyleId: this.props.productStyleId
    };
    [
      'handleClick',
      'getReferer',
      'CallAddToWishlist',
      'fetchWishlistSummary'
    ].forEach(
      method => (this[method] = this[method].bind(this))
    );
  }

  componentDidMount() {
    if (isLoggedIn()) {
      this.fetchWishlistSummary();
    }
  }

  getReferer(url = '/') {
    if (typeof window !== 'undefined') {
      return `${url}?referer=${window.location.href}`;
    } return `${url}`;
  }

  fetchWishlistSummary() {
    Client.get('/web/wishlistapi/summary').end((err, response) => {
      if ((err || at(response, 'body.status.statusType') === 'ERROR')) {
        console.error(`[YOUCAM WISHLIST ERROR]: ${err}`);
      } else {
        const styleIds = at(response, 'body.data.styleIds') || [];
        if (styleIds.includes(this.state.productStyleId)) {
          this.setState({ addedToWishlist: true });
        }
      }
    });
  }

  handleClick() {
    const { redirectToCheckout, loginRequired } = this.props;
    if (!isLoggedIn() && loginRequired) {
      window.location.href = this.getReferer('https://www.myntra.com/login');
    } else if (this.state.addedToWishlist && redirectToCheckout) {
      window.location.href = 'https://www.myntra.com/checkout/cart';
    } else {
      this.setState({
        addedToWishlist: true
      }, this.CallAddToWishlist);
    }
  }

  CallAddToWishlist() {
    const endPointUrl = '/web/wishlistapi/addition';
    const headers = { 'X-CSRF-TOKEN': at(window, '__myx_session__.USER_TOKEN') };
    const parameters = {
      styleId: this.state.productStyleId
    };
    const defaultErrorMessage = 'Something went wrong';
    Client.post(endPointUrl, parameters, headers).end((err, response) => {
      if (err) {
        if (at(response, 'body.code') === 1004 || at(response, 'body.status.statusCode') === '403') {
          window.location.href = this.getReferer('https://www.myntra.com/login');
        } else {
          this.props.onError(defaultErrorMessage);
        }
      } else {
        if (at(response, 'body.code') === 1004 || at(response, 'body.status.statusCode') === '403') {
          window.location.href = this.getReferer('https://www.myntra.com/login');
        }
        if (at(response, 'body.status.response.meta.error') === 'MAX_LIMIT_REACHED') {
          this.props.onError('You have reached the item limit of your Wishlist.');
        } else {
          this.props.onSuccess('Item Wishlisted');
        }
      }
    });
  }

  render() {
    const buttonStyle = [styles.btn];
    const iconStyle = [];
    let buttonCaption = 'Wishlist';
    if (this.state.addedToWishlist) {
      buttonCaption = 'Wishlisted';
      buttonStyle.push(styles.btnGrayBG);
      iconStyle.push(styles.iconAdded);
    } else {
      buttonStyle.push(styles.btnPlain);
      iconStyle.push(styles.iconAdd);
    }
    return (
      <div className={styles.container}>
        <button className={buttonStyle.join(' ')} onClick={this.handleClick}>
          <span className={`myntraweb-sprite ${iconStyle}`} />
          {buttonCaption}
        </button>
      </div>
    );
  }
}
Wishlist.propTypes = {
  productStyleId: PropTypes.number,
  redirectToCheckout: PropTypes.bool,
  loginRequired: PropTypes.bool,
  onSuccess: PropTypes.func,
  onError: PropTypes.func
};
Wishlist.defaultProps = {
  productStyleId: null,
  redirectToCheckout: false,
  loginRequired: false,
  onSuccess: () => {},
  onError: () => {}
};
