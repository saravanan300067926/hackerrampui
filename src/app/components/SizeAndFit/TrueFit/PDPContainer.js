import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

function sendTextLoadEvent(id, name, brand) {
  window.Madalytics.sendRaw(
    'PDP - 3P size recommendation prompt on PDP', {
      'event_type': 'widgetLoad',
      'event_category': 'PDP',
      'payload': {
        'custom': {
          'v1': 'truefit'
        },
        'screen': {
          'name': `Shopping Page-PDP${window.location.pathname}`,
          'url': window.location.href,
          'variant': 'web',
          'data_set': {
            'entity_type': 'product',
            'entity_name': name || brand.name,
            'entity_id': id
          }
        }
      }
    }
  );
}

function sendWidgetRenderEvent(id, name, brand) {
  window.Madalytics.sendRaw(
    'PDP - 3P size recommendation click on Find Size', {
      'event_type': 'widgetLoad',
      'event_category': 'PDP',
      'payload': {
        'custom': {
          'v1': 'truefit'
        },
        'screen': {
          'name': `Shopping Page-PDP${window.location.pathname}`,
          'url': window.location.href,
          'variant': 'web',
          'data_set': {
            'entity_type': 'product',
            'entity_name': name || brand.name,
            'entity_id': id
          }
        }
      }
    }
  );
}

function sendRecommendationCompleteEvent(id, name, brand, recommendedSize) {
  window.Madalytics.sendRaw('PDP - 3P size recommended', {
    'event_type': 'widgetLoad',
    'event_category': 'PDP',
    'payload': {
      'custom': {
        'v1': 'truefit'
      },
      'screen': {
        'name': `Shopping Page-PDP${window.location.pathname}`,
        'url': window.location.href,
        'variant': 'web',
        'dataSet': {
          'entity_type': 'product',
          'entity_name': name || brand.name,
          'entity_id': id,
          'entity_optional_attributes': {
            'recommended_size': recommendedSize
          }
        }
      }
    }
  });
}

class TrueFitPDPContainer extends React.PureComponent {

  componentDidMount() {
    const trueFitAddToBagCallbackRegistrationTimer = setInterval(() => {
      if (typeof window.tfcapi === 'function') {
        clearInterval(trueFitAddToBagCallbackRegistrationTimer);
        this.initEvents();
      }
    }, 50);
  }

  initEvents() {
    // Flags used to prevent multiple event calls for render and success hooks
    let isRenderEventCalled = false;
    let isSizeRecommendedEventCalled = false;

    const { id, name, brand } = this.props.data;
    // TrueFit text load
    window.tfcapi('event', 'tfc-fitrec-product', 'render', () => {
      if (!isRenderEventCalled) {
        sendTextLoadEvent(id, name, brand);
        isRenderEventCalled = true;
      }
    });
    // TrueFit popup opened
    window.tfcapi('event', 'tfc-fitrec-register', 'open', () => sendWidgetRenderEvent(id, name, brand));
    // Size recommendation complete
    window.tfcapi('event', 'tfc-fitrec-product', 'success', data => {
      if (!isSizeRecommendedEventCalled) {
        sendRecommendationCompleteEvent(id, name, brand, data.fitRecommendation.size);
        isSizeRecommendedEventCalled = true;
      }
    });
    // Product added to bag
    window.tfcapi('event', 'tfc-fitrec-register', 'addtobag', data => this.props.onAddToBag(data));
  }

  render() {
    const { id, sizes, colours } = this.props.data;
    const isLoggedIn = get(window, '__myx_session__.isLoggedIn', false);
    const uidx = get(window, '__myx_session__.login', '');
    const colonSeparatedSizes = sizes
      .filter(size => size.available)
      .reduce((result, availableSize) => `${result}:${availableSize.label}`, '')
      .substring(1); // remove the initial semicolon
    return (
      <div
        style={{ margin: '16px 0' }}
        className="tfc-fitrec-product"
        id={id}
        data-locale="en_IN"
        data-user-id={uidx}
        data-registered={isLoggedIn}
        data-colorid={colours}
        data-availablesizes={colonSeparatedSizes} />
    );
  }
}

TrueFitPDPContainer.propTypes = {
  data: PropTypes.object.isRequired,
  onAddToBag: PropTypes.func.isRequired
};

export default TrueFitPDPContainer;
