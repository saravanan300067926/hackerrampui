import React from 'react';
import cookies from '../../utils/cookies';
import styles from './register.css';
import { isBrowser, sanitize, loadFacebookConnect, getFeatures } from '../../utils';
import Notify from '../Notify';
import Validations from '../../utils/validations';
import at from 'v-at';
import Track from '../../utils/track';
import Loader from '../Loader';
import { Link } from 'react-router';
import config, { root } from '../../config';
import Client from '../../services';
import get from 'lodash/get';
import trim from 'lodash/trim';
import join from 'lodash/join';
import getConstants from '../../constants';
import { getIdToken } from '../Login/index';

const DEFAULT_ERROR_MESSAGE = 'Oops! Something went wrong. Please try again in some time';
const captchaEnabled = getFeatures('showSignupCaptcha') === 'true';

const PasswordWidget = (props) => {
  const appliedStyle = props.success ? styles.success : styles.error;
  return (<span
    className={`${styles.widget} ${props.clickedSubmit ? appliedStyle : styles.disabled}`}>
    <span className={styles['widget-text']}>
      {props.children}
    </span>
  </span>);
};

class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      captchaURL: captchaEnabled ? this.getCaptchaUrl() : null,
      isFocussed: false,
      showEmailError: false,
      showPasswordError: false,
      showMobileError: false,
      showGenderError: false,
      captchaError: null,
      userToken: '',
      clickedSubmit: false,
      passwordErrorMessage: '',
      passwordErrorCodes: []
    };
    this.register = this.register.bind(this);
    this.facebookLogin = this.facebookLogin.bind(this);
    this.gPlusLoginSuccess = this.gPlusLoginSuccess.bind(this);
    this.gPlusLoginFailed = this.gPlusLoginFailed.bind(this);
    this.setFocus = this.setFocus.bind(this);
    this.setBlur = this.setBlur.bind(this);
    this.onChange = this.onChange.bind(this);
    this.getPasswordStrengthWidgets = this.getPasswordStrengthWidgets.bind(this);
    this.changeCaptcha = this.changeCaptcha.bind(this);
  }

  componentDidMount() {
    loadFacebookConnect();
    this.setState({ userToken: at(window, '__myx_session__.USER_TOKEN') });
    cookies.set('bc', 'true');
    this.registerGoogleLogin();
    Track.screen('/register');
  }

  onLoginAction(response, key) {
    if (at(response, 'body.httpCode') === 400) {
      const errorMessage = at(response, 'body.message') || DEFAULT_ERROR_MESSAGE;
      this.refs.notify.error(errorMessage);
      Track.event('register', key, errorMessage);
    } else {
      if (window.localStorage) {
        window.localStorage.clear();
      }
      this.navigateToIndex();
      Track.event('register', key, 'success');
    }
  }

  onChange(event) {
    if (!this.state.clickedSubmit) {
      return;
    }
    const { type, name, value } = event.target;

    if (type === 'radio' && name === 'gender') {
      if (this.state.showGenderError === true) {
        this.setState({
          showGenderError: false
        });
      }
    } else {
      if (name === 'email') {
        let showEmailError = false;
        if (!Validations.isValidEmail(value)) {
          showEmailError = true;
        }
        if (this.state.showEmailError !== showEmailError) {
          this.setState({
            showEmailError
          });
        }
      } else if (name === 'password') {
        let showPasswordError = false;
        const passwordValid = Validations.isValidPassword(value);
        if (passwordValid.error) {
          showPasswordError = true;
        }
        if (this.state.showPasswordError !== showPasswordError
          || this.state.passwordErrorMessage !== passwordValid.errorMessage
          || this.state.passwordErrorCodes !== passwordValid.errorCodes) {
          this.setState({
            showPasswordError,
            passwordErrorMessage: passwordValid.errorMessage,
            passwordErrorCodes: passwordValid.errorCodes
          });
        }
      } else if (name === 'mobile') {
        let showMobileError = false;
        if (!Validations.isValidMobile(value)) {
          showMobileError = true;
        }
        if (this.state.showMobileError !== showMobileError) {
          this.setState({
            showMobileError
          });
        }
      }
    }
  }

  getPasswordStrengthWidgets() {
    const passwordLength = getConstants('passwordLength');
    const passwordSpecialCharsEnabled = getConstants('passwordSpecialCharsEnabled');
    const passwordUpperCaseEnabled = getConstants('passwordUpperCaseEnabled');
    const passwordnumberEnabled = getConstants('passwordnumberEnabled');

    return (<div className={styles['password-widgets']}>
      {passwordLength !== 0 ? <PasswordWidget
        clickedSubmit={this.state.clickedSubmit}
        success={this.state.passwordErrorCodes.indexOf('password_length') === -1}>
        {passwordLength} Characters
      </PasswordWidget> : null}
      {passwordSpecialCharsEnabled ? <PasswordWidget
        clickedSubmit={this.state.clickedSubmit}
        success={this.state.passwordErrorCodes.indexOf('special character') === -1}>
        1 Special
      </PasswordWidget> : null}
      {passwordUpperCaseEnabled ? <PasswordWidget
        clickedSubmit={this.state.clickedSubmit}
        success={this.state.passwordErrorCodes.indexOf('uppercase') === -1}>
        1 Uppercase
      </PasswordWidget> : null}
      {passwordnumberEnabled ? <PasswordWidget
        clickedSubmit={this.state.clickedSubmit}
        success={this.state.passwordErrorCodes.indexOf('number') === -1}>
        1 Numeric
      </PasswordWidget> : null}
    </div>);
  }

  setFocus() {
    this.setState({
      isFocussed: true
    });
  }

  setBlur() {
    this.setState({
      isFocussed: false
    });
  }

  getReferer() {
    return sanitize(get(this.props, 'location.query.referer') || (`${at(window, 'location.protocol')}//${at(window, 'location.host')}`));
  }

  getCaptcha() {
    if (this.state.captchaURL) {
      const captcha = `${root()}${this.state.captchaURL.substring(1)}`;
      return (
        <div>
          <img className={styles.captcha} src={captcha} />
          <div className={styles.changeCaptcha} onClick={this.changeCaptcha}>
            <img src="https://constant.myntassets.com/web/assets/img/4f8acb92-0001-433f-a8b6-27ac954ef94e1546949499874-refresh_icon.png" />
            Change Text
          </div>
          <fieldset className={styles['input-container']}>
            <input
              ref={"captchaInput"}
              className={styles['user-input-captcha']}
              name="captcha"
              type="text"
              autoComplete="off"
              maxLength="15"
              placeholder="Please enter the characters shown above" />
          </fieldset>
          {this.state.captchaError && (
            <div className={styles.captchaError}>
              {this.state.captchaError}
            </div>
          )}
        </div>
      );
    }
    return null;
  }

  getCaptchaUrl() {
    const randomNumber = Math.floor(Math.random() * 10000000000);
    return `/captcha/captcha.php?id=loginPageCaptcha&rand=${randomNumber}`;
  }

  getLoginPath() {
    let referer = new URLSearchParams(window.location.search);
    referer = sanitize(referer.get('referer'));
    let path = '/login';
    if (referer) {
      path = `${path}?referer=${referer}`;
    }
    return path;
  }

  getEmailError() {
    if (this.state.showEmailError) {
      return (
        <div>
          <span className={styles['error-icon']}>!</span>
          <p className={styles['error-message']}>Please enter a valid email id</p>
        </div>
      );
    }
    return null;
  }

  getPasswordError() {
    if (this.state.showPasswordError) {
      return (
        <div>
          <span className={styles['error-icon']}>!</span>
          <p className={styles['error-message']}>{this.state.passwordErrorMessage}</p>
        </div>
      );
    }
    return null;
  }

  getMobileError() {
    if (this.state.showMobileError) {
      return (
        <div>
          <span className={styles['error-icon']}>!</span>
          <p className={styles['error-message']}>Please enter a valid mobile number (10 digits)</p>
        </div>
      );
    }
    return null;
  }

  getGenderError() {
    if (this.state.showGenderError) {
      return (
        <div>
          <span className={styles['gender-error-icon']}>!</span>
          <p className={styles['gender-error-message']}>Please select your gender</p>
        </div>
      );
    }
    return null;
  }

  getGender() {
    // this.refs.registerForm.gender is NodeList , cant forEach
    for (let i = this.refs.registerForm.gender.length - 1; i >= 0; i--) {
      const g = this.refs.registerForm.gender[i];
      if (g.checked) {
        return g.value;
      }
    }
    return this.refs.registerForm.gender.value;
  }

  changeCaptcha() {
    const captchaInput = this.refs.captchaInput;
    if (captchaInput) {
      captchaInput.value = '';
    }
    this.setState({
      captchaURL: this.getCaptchaUrl(),
      captchaError: null
    });
  }

  registerGoogleLogin() {
    const gPlusLoginSuccess = this.gPlusLoginSuccess;
    const gPlusLoginFailed = this.gPlusLoginFailed;

    if (at(window, 'gapi')) {
      window.gapi.load('auth2', () => {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        const auth2 = window.gapi.auth2.init({
          client_id: '787245060481-4padi0c9g2l1pnbie52fmpfdpo43209i.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin'
        });
        auth2.attachClickHandler(document.getElementById('gPlusLogin'),
          {}, gPlusLoginSuccess, gPlusLoginFailed);
      });
    }
  }

  sendSignupRequest(key, parameters) {
    if (this.state.isLoading) {
      return;
    }

    this.setState({
      isLoading: true
    });

    Track.event('register', key, 'submit');

    Client.post(config(key), parameters).end((err, res) => {
      if (err) {
        this.setState({
          isLoading: false
        });
        this.refs.notify.error(DEFAULT_ERROR_MESSAGE);
      }
      this.setState({
        isLoading: false,
        captchaURL: captchaEnabled ? this.getCaptchaUrl() : at(res, 'body.captchaURL')
      });
      this.onLoginAction(res, key);
    });
  }

  facebookLogin() {
    if (at(window, 'FB')) {
      window.FB.login((response) => {
        if (response.status === 'connected') {
          const parameters = {
            token: at(response, 'authResponse.accessToken'),
            xsrf: this.refs.registerForm.xsrf.value,
            action: 'facebook'
          };
          this.sendSignupRequest('facebook', parameters);
        }
      }, { scope: 'public_profile,email' });
    }
  }

  validate(parameters) {
    let showGenderError = false;
    let showEmailError = false;
    let showPasswordError = false;
    let showMobileError = false;
    let captchaError = false;
    let passwordErrorMessage = '';
    let passwordErrorCodes = [];
    if (!Validations.isValidEmail(parameters.email)) {
      showEmailError = true;
    }
    const passwordValid = Validations.isValidPassword(parameters.password);
    if (passwordValid.error) {
      showPasswordError = true;
      passwordErrorMessage = passwordValid.errorMessage;
      passwordErrorCodes = passwordValid.errorCodes;
    }
    if (!Validations.isValidMobile(parameters.mobile)) {
      showMobileError = true;
    }
    if (!parameters.gender) {
      showGenderError = true;
    }
    if (captchaEnabled && !parameters.captcha) {
      captchaError = 'Please enter the captcha text.';
    }
    this.setState({
      showGenderError,
      showEmailError,
      showPasswordError,
      showMobileError,
      captchaError,
      passwordErrorMessage,
      passwordErrorCodes
    });
    const isError = showGenderError || showEmailError || showMobileError || showPasswordError || captchaError;
    if (isError) {
      window.ga('send', 'event', 'signup', 'signup_credentials_incorrect');
      if (showPasswordError) {
        window.ga('send', 'event', 'signup', 'password_rule_not_satisfied', join(passwordErrorCodes, ' | '));
      }
    }
    return !isError;
  }

  register(event) {
    event.preventDefault();

    this.setState({
      clickedSubmit: true
    });

    const parameters = {
      email: this.refs.registerForm.email.value,
      password: this.refs.registerForm.password.value,
      mobile: this.refs.registerForm.mobile.value,
      gender: this.getGender(),
      captcha: this.refs.registerForm.captcha ? this.refs.registerForm.captcha.value : null,
      action: 'signup',
      usertype: 'C',
      xsrf: this.refs.registerForm.xsrf.value
    };

    if (this.validate(parameters)) {
      if (captchaEnabled) {
        this.sendCaptchaVerifyRequest(parameters);
      } else {
        this.sendSignupRequest('signup', parameters);
      }
    }
  }

  sendCaptchaVerifyRequest(parameters) {
    this.setState({
      isLoading: true
    });

    const data = {
      value: trim(parameters.captcha),
      token: parameters.xsrf
    };

    Client.post('/captcha/verify/login', data).end((err, res) => {
      if (err) {
        let errorMessage = DEFAULT_ERROR_MESSAGE;

        if (at(res, 'body.error') === 'forbidden') {
          errorMessage = 'Invalid Session. Please refresh the page and try again.';
        }

        this.refs.notify.error(errorMessage);

        this.setState({
          isLoading: false
        });
        return;
      }

      if (res.body === true) {
        this.setState({
          isLoading: false
        }, () => {
          this.sendSignupRequest('signup', parameters);
        });
      } else {
        this.setState({
          isLoading: false,
          captchaError: 'Wrong captcha text entered. Please try again or change text.'
        });
      }
    });
  }

  gPlusLoginSuccess(googleUser) {
    const parameters = {
      token: getIdToken(googleUser),
      action: 'google',
      xsrf: this.refs.registerForm.xsrf.value
    };
    this.sendSignupRequest('google', parameters);
  }

  gPlusLoginFailed() {
    this.refs.notify.error(DEFAULT_ERROR_MESSAGE);
  }

  navigateToIndex() {
    window.location.href = this.getReferer();
  }

  render() {
    return (
      <div className={styles.container}>
        <div className={styles.box}>
          <div className={styles.header}>
            <p className={styles.title}>Signup with Myntra</p>
          </div>
          <div className={styles['third-party-register']}>
            <p className={styles['button-info-text']}>EASILY USING</p>
            <div className={styles['button-container']}>
              <button className={styles.facebook} onClick={this.facebookLogin}>
                <span className={styles['fb-logo']}></span>
                FACEBOOK
              </button>
              <button className={styles.google} id="gPlusLogin">
                <span className={styles['gplus-logo']}></span>
                GOOGLE
              </button>
            </div>
          </div>
          <p className={styles['button-info-text']}>- OR USING EMAIL -</p>
          <form ref="registerForm" method="POST" className={styles['register-form']} onSubmit={this.register} noValidate>
            <fieldset className={styles[this.state.isFocussed ? 'input-container-pink' : 'input-container']}>
              <div className={styles['input-item']}>
                <input
                  className={styles['user-input-email']}
                  onFocus={this.setFocus}
                  onBlur={this.setBlur}
                  onChange={this.onChange}
                  name="email"
                  type="email"
                  placeholder="Your Email Address" />
                {this.getEmailError()}
              </div>
              <div className={styles['input-item']}>
                <input
                  className={styles['user-input-password']}
                  onFocus={this.setFocus}
                  onBlur={this.setBlur}
                  onChange={this.onChange}
                  name="password"
                  type="password"
                  placeholder="Choose Password" />
                {this.getPasswordStrengthWidgets()}
                {this.getPasswordError()}
              </div>
              <div className={styles['input-item']}>
                <input
                  type="number"
                  className={styles['user-input-mobile']}
                  name="mobile"
                  min="1000000000"
                  max="9999999999"
                  onFocus={this.setFocus}
                  onBlur={this.setBlur}
                  onChange={this.onChange}
                  placeholder="Mobile Number (For order status updates)" />
                {this.getMobileError()}
              </div>
              <input type="hidden" name="xsrf" value={this.state.userToken} />
              <fieldset data-type="horizontal" className={styles.gender}>
                <legend className={styles['gender-title']}>{`I${String.fromCharCode(39)}m a`}</legend>
                <input type="radio" className={styles['gender-radio']} id="male" name="gender" value="male" onChange={this.onChange} />
                <label className={styles['gender-label']} htmlFor="male">Male</label>
                <input type="radio" className={styles['gender-radio']} id="female" name="gender" value="female" onChange={this.onChange} />
                <label className={styles['gender-label']} htmlFor="female">Female</label>
                {this.getGenderError()}
              </fieldset>
            </fieldset>
            {this.getCaptcha()}
            <fieldset className={styles['register-button-container']}>
              <button className={styles['register-button']}>REGISTER</button>
            </fieldset>
          </form>
          <div className={styles['link-container']}>
            <div className={styles['login-link']}>
              <span className={styles['info-text']}>Already have an account?</span>
              <Link className={styles['create-account-link']} to={this.getLoginPath()}>Login!</Link>
            </div>
          </div>
        </div>
        <Notify ref="notify" />
        <Loader show={this.state.isLoading} />
      </div>
    );
  }
}

Register.propTypes = {
  config: React.PropTypes.object,
  history: React.PropTypes.object.isRequired
};

Register.defaultProps = {
  config: (isBrowser() ? { session: window.__myx_session__ } : null)
};

PasswordWidget.propTypes = {
  success: React.PropTypes.bool,
  clickedSubmit: React.PropTypes.bool,
  children: React.PropTypes.node
};

export default Register;
