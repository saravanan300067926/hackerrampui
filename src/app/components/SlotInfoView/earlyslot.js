 // jshint ignore:line

 module.exports = {
    "appSlotApiCall": false,
    "saleStartTime": "1569695400000",
    "platinumSlotStartTime": "1545399000000",
    "slotCreativeURL": "https://constant.myntassets.com/web/assets/img/f1ea9c81-afae-4138-b099-d01eb79a666e1565178908086-rtf-slot-info.png",
    "topBanner": {
      "imgUrl": "https://constant.myntassets.com/web/assets/img/63c5104b-fc3c-4828-94f6-030231a8bf1d1544711618858-mySale-MastHead.jpg",
      "landingUrl": "/"
    },
    "couponsCreativeBanner": {
      "imgUrl": "https://constant.myntassets.com/web/assets/img/3549345c-ff5c-4111-a4a5-c193a776acca1544786060790-MyntraInsider-Banner-for-mySale.jpg",
      "landingUrl": "/myntrainsider"
    },
    "slotInfo": {
      "slot": {
        "beforeSlot": {
          "desc": "You have an early entry slot. Shop before the sale starts & get your favourite products before they’re sold out. Hurry!"
        },
        "atSlot": {
          "desc": "You have an exclusive VIP Slot. Shop before the sale starts and grab your favourite products before they run out."
        },
        "afterSlot": {
          "desc": "Your VIP Slot has ended. You can continue to shop during the sale starting midnight. See you there!"
        },
        "title": "EXCLUSIVE GOLD SLOT",
        "platinumTitle": "EXCLUSIVE PLATINUM SLOT",
        "slot_change_text": "CHANGE SLOT TIME",
        "links": [
          {
            "title": "SHOP NOW",
            "url": "/welcome-to-myntra"
          }
        ]
      },
      "noSlot": {
        "title": "EXCLUSIVE GOLD SLOT",
        "desc": "Play games to earn Insider Points. Use these Points to get a slot for early access to the sale before it starts.",
        "dateDesc": "Shop before sale starts",
        "timeDesc": "GET GOLD SLOT NOW",
        "links": [
          {
            "title": "PLAY GAMES",
            "url": "/growth/prebuzz-buypage",
            "id": "play_games"
          }
        ]
      }
    },
    "couponsInfo": {
      "enable": "true",
      "title": "EORS Coupons",
      "desc": "",
      "links": [
        {
          "title": "VIEW ALL COUPONS",
          "url": "/growth/prebuzz-buypage",
          "id": "view_all_coupons"
        }
      ]
    },
    "quickCash": {
      "enable": "true",
      "background": "https://constant.myntassets.com/web/assets/img/373efad3-c8f3-41a2-8cbb-1d7d5d3b34ff1543578937171-bg_quickcash_profile.png",
      "title": {
        "__html": "<b>QuickCash</b> earned so far"
      },
      "nonLoggedInTitle": {
        "__html": "<b>Invite & Earn</b>"
      },
      "desc": {
        "__html": "Invite your friends to the End Of Reason &amp; get <i>₹</i>50 for every friend who visits. They get <i>₹</i>50 too."
      },
      "links": [
        {
          "title": "KNOW MORE",
          "title2": "KNOW MORE",
          "url": "/referrals-dashboard",
          "urlForApp": "/referrals-dashboard",
          "id": "earn_cash"
        },
        {
          "title": "EARN POINTS",
          "url": "share://dialogTitle=Myntra Fashion Upgrade&body=${body}&subject=${subject}&imageUrl=${giveawayInfo.socialShare.whatsappImg || 'https://constant.myntassets.com/web/assets/img/39ad71e5-3fe7-4341-bc16-db9bd33aa4301542117202226-907c47a0-8f9b-446f-80ce-9e62e09cf3501542113933443-whatsapp-image-720-tiny.jpg'",
          "id": "know_more"
        }
      ]
    }
}
