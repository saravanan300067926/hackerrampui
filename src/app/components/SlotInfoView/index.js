/* global MyntApp:false */
import React from 'react';
import { getSlots, cookieExists, showEarlySlotInfo } from '../../utils/slotUtils';
import months from '../../utils/months';
import styles from './slotinfoview.css';
import at from 'v-at';
import get from 'lodash/get';
import bus from 'bus';
import kvpairs from '../../utils/kvpairs';
import Client from '../../services';
import config from '../../config';
import AdmissionControl from '../AdmissionControl';
import { isBrowser } from '../../utils';
import DefaultSlotConfig from './earlyslot';
import TrackMadalytics from '../../utils/trackMadalytics';
import Track from '../../utils/track';
import isEmpty from 'lodash/isEmpty';
let earlySlotData = DefaultSlotConfig;
const kvdata = kvpairs('earlyslot.config') || null;
if (kvdata) {
  earlySlotData = kvdata;
}

const isApp = () => {
  if (isBrowser()) {
    const deviceChannel = at(window, '__myx_deviceData__.deviceChannel') || '';
    return deviceChannel.toLowerCase() === 'mobile_app';
  } return false;
};

function getTimeRange(start, end) {
  if (!start || !end) {
    return null;
  }
  let st;
  let et;
  const sHours = start.getHours();
  let sMins = start.getMinutes();
  const eHours = end.getHours();
  let eMins = end.getMinutes();
  if (sMins < 10) {
    sMins = `0${sMins}`;
  }
  if (eMins < 10) {
    eMins = `0${eMins}`;
  }
  if (sHours > 12) {
    st = `${sHours - 12}:${sMins} PM`;
  } else if (sHours === 0) {
    st = `12:${sMins} AM`;
  } else if (sHours === 12) {
    st = `12:${sMins} PM`;
  } else {
    st = `${sHours}:${sMins} AM`;
  }
  if (eHours > 12) {
    et = `${eHours - 12}:${eMins} PM`;
  } else if (eHours === 0) {
    et = `12:${eMins} AM`;
  } else if (eHours === 12) {
    et = `12:${eMins} PM`;
  } else {
    et = `${eHours}:${eMins} AM`;
  }
  return `${st} - ${et}`;
}

class SlotInfoView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: true,
      requestToSlotMade: false,
      showSlotDetails: false,
      showSlotPopup: false
    };
    this.hideSaleSlot = this.hideSaleSlot.bind(this);
    this.getCookieSlot = this.getCookieSlot.bind(this);
  }

  componentWillMount() {
    const appSlotApiCall = earlySlotData.appSlotApiCall || false;
    if (isApp() && appSlotApiCall) {
      return this.getSlotInfo();
    }
    if (cookieExists('stp')) {
      this.getCookieSlot();
    } else if (!this.state.requestToSlotMade) {
      this.getSlotInfo();
    }
    return null;
  }

  componentDidMount() {
    this.displayLP();
    // Start listening to beacon upadate events in the bus.
    bus.on('beacon-data', (data) => {
      this.displayLP(data);
    });
    TrackMadalytics.sendEvent('My Sale - mySale page load', null, {}, {
      'custom': {}
    });
    Track.event('MySale', '');
    if (typeof MyntApp !== 'undefined' && typeof MyntApp.updateLoyaltyPoints === 'function') {
      MyntApp.updateLoyaltyPoints();
    }
  }

  getCookieSlot() {
    const slots = getSlots(true) || {};
    const slotSelected = at(slots, 'sl');
    const startDate = !isEmpty(slotSelected) ? new Date(slotSelected.st) : null;
    const endDate = !isEmpty(slotSelected) ? new Date(slotSelected.et) : null;
    this.setState({
      busy: false,
      showSlotDetails: true,
      slotSelected,
      startDate,
      endDate
    });
  }

  getChangeLink(presentDate, endDate) {
    const slotModeStart = at(earlySlotData, 'platinumSlotStartTime');
    const slot = at(earlySlotData, 'slotInfo.slot');
    if (showEarlySlotInfo() && parseInt(slotModeStart, 10) > presentDate) {
      return (
        <div
          className={styles.changeLink}
          onClick={() => {
            this.handleChangeClick();
            this.sendMAevents('My Sale - Slot Change Initiate', 'CHANGE SLOT TIME', 'button');
            this.sendGAevents('mysale', 'change slot click', getTimeRange(this.state.startDate, this.state.endDate));
          }}>
          {slot.slot_change_text}
        </div>
      );
    } else if (slotModeStart <= presentDate && presentDate <= endDate) {
      const link = slot.links[0] || {};
      return (<div className={styles.changeLink}>
        <a
          href={link.url}>
          {at(link, 'title')}
        </a>
      </div>);
    }
    return null;
  }

  getLink(links) {
    const linkclass = links.length > 1 ? styles.gameLink : styles.changeLink;
    return links.map((entities, index) =>
      (<a
        href={entities.url}
        onClick={() => {
          this.sendMAevents(entities.maEventType, entities.title, 'button');
          this.sendGAevents('mysale', entities.gaEventAction, entities.eventLabel);
        }}>
        <div
          className={linkclass}
          key={index}>
            {at(entities, 'title')}
        </div>
      </a>
      ));
  }

  getSaleImage() {
    const topBanner = at(earlySlotData, 'topBanner');
    return (
      <div className={styles.saleImage}>
        <img
          src={topBanner.imgUrl}
          onClick={() => {
            this.sendMAevents('My Sale - Promo Banner click', 'Promo Banner', 'image');
            this.sendGAevents('mysale', 'promo banner click');
          }} />
      </div>
    );
  }

  getCouponsCreativeImage() {
    const couponsCreativeBanner = at(earlySlotData, 'couponsCreativeBanner');
    return couponsCreativeBanner.imgUrl ? (
      <a href={couponsCreativeBanner.landingUrl}>
        <div className={styles.couponsCreativeImage}>
          <img src={couponsCreativeBanner.imgUrl} onClick={this.sendMAevents('', 'Coupons Creative Banner', 'image')} />
        </div>
      </a>
    ) : null;
  }

  getSlotInfo() {
    this.setState({ requestToSlotMade: true });
    Client.get(`${config('admission')}/getSlotInfo`).end((err, response) => {
      if ((err || at(response, 'body.status.statusType') === 'ERROR')) {
        this.setState({ busy: false });
      } else if (response) {
        const slotSelected = at(response, 'body.data.assignedSlot');
        const startDate = !isEmpty(slotSelected) ? new Date(slotSelected.slotStartTime) : null;
        const endDate = !isEmpty(slotSelected) ? new Date(slotSelected.slotEndTime) : null;
        this.setState({
          busy: false,
          showSlotDetails: true,
          startDate,
          endDate,
          slotSelected
        });
      }
    });
  }

  getSlotUser(startDate, endDate) {
    const slotInfo = at(earlySlotData, 'slotInfo.slot');
    const slotModeStart = at(earlySlotData, 'platinumSlotStartTime');
    const isPlatinumSlot = startDate.getTime() === parseInt(slotModeStart, 10);
    const title = isPlatinumSlot ? slotInfo.platinumTitle : slotInfo.title;
    const presentDate = new Date().getTime();
    const slotBackgroundColor = presentDate > endDate ? '#d4d5d9' : '#731a43';
    const slotEndColor = presentDate > endDate ? '#7e808c' : '#282c3f';
    const slotInfoText = (presentDate > endDate && slotInfo) ? at(slotInfo, 'afterSlot.desc') : at(slotInfo, 'beforeSlot.desc');
    const imageurl = get(earlySlotData, 'slotCreativeURL', '');
    return (
      <div className={styles.slotInfo}>
        <div className={styles.slotContainer} style={{ 'backgroundColor': slotBackgroundColor }}>
          <div className={styles.slot} style={{ 'backgroundImage': `url(${imageurl})` }}>
            <div className={styles.selected}>
              <div className={styles.slotTitle}>{title}</div>
              <div className={styles.date} style={{ 'color': slotEndColor }}>
                {startDate.getDate()} {months[startDate.getMonth()]} {startDate.getFullYear()}
              </div>
              <div className={styles.time} style={{ 'color': slotEndColor }}>
                {getTimeRange(startDate, endDate)}
              </div>
            </div>
          </div>
        </div>
        <div className={styles.text}>
          {slotInfoText}
        </div>
        {this.getChangeLink(presentDate, endDate.getTime())}
      </div>
    );
  }

  getNoSlotUser() {
    const presentDate = new Date().getTime();
    const slotInfo = at(earlySlotData, 'slotInfo.noSlot');
    const slotBackgroundColor = '#9b3c4e';
    const slotInfoText = slotInfo ? slotInfo.desc : '';
    const links = slotInfo ? slotInfo.links : [];
    const saleStartTime = earlySlotData.saleStartTime;
    const imageurl = get(earlySlotData, 'slotCreativeURL', '');
    links.forEach(link => {
      if (link.id === 'play_games') {
        link.maEventType = 'My Sale - Play Games click';
        link.gaEventAction = 'play games click';
        link.eventLabel = null;
      }
      if (link.id === 'wishlist') {
        link.maEventType = 'My Sale - Wishlist click';
        link.gaEventAction = null;
        link.eventLabel = null;
      }
    });
    if (presentDate < saleStartTime) {
      return (
        <div className={styles.slotInfo}>
          <div className={styles.slotContainer} style={{ 'backgroundColor': slotBackgroundColor }}>
            <div className={styles.slot} style={{ 'backgroundImage': `url(${imageurl})` }}>
              <div className={styles.selected}>
                <div className={styles.slotTitle}>{slotInfo.title}</div>
                <div className={styles.date}>
                  {slotInfo.dateDesc}
                </div>
                <div className={styles.time}>
                  {slotInfo.timeDesc}
                </div>
              </div>
            </div>
          </div>
          <div className={styles.text}>
            {slotInfoText}
          </div>
          {this.getLink(slotInfo.links)}
        </div>
      );
    }
    return null;
  }

  getQuickcash() {
    const quickCash = at(earlySlotData, 'quickCash');
    const isEnable = quickCash.enable === 'true';
    const links = quickCash ? quickCash.links : [];
    const showPointStyle = this.state.loggedIn ? styles.userLP : styles.noDisplay;
    const showLpDescStyle = this.state.loggedIn ? styles.lpDesc : styles.lpDescloggedout;
    const showRupeeStyle = this.state.loggedIn ? styles.rupee : styles.noDisplay;
    let titleObj = this.state.loggedIn ? quickCash.title : quickCash.nonLoggedInTitle;
    let descObj = quickCash.desc;
    try {
      const title = at(titleObj, '__html').replace(/&lt;/ig, '<');
      const desc = at(quickCash, 'desc.__html').replace(/&lt;/ig, '<');
      titleObj.__html = title;
      descObj.__html = desc;
    } catch (e) {
      console.log('Error', e);
    }
    let arr = [];
    links.forEach(link => {
      if (link.id === 'earn_cash') {
        link.maEventType = null;
        link.gaEventAction = 'quick cash dashboard click';
        link.eventLabel = null;
        if (isApp()) {
          link.url = link.urlForApp;
        }
        if (this.state.userLP) {
          link.title = link.title2;
        }
      }
      if (link.id === 'know_more') {
        link.maEventType = 'My Sale - Earn Cash click';
        link.gaEventAction = 'earn cash click';
        link.eventLabel = this.state.userLP;
      }
    });
    links.forEach(link => {
      if (link.id !== 'know_more') {
        arr.push(link);
      }
    });
    if (isApp()) {
      arr = links;
    }
    const point = this.state.showPoints ? this.state.userLP : null;
    return isEnable ? (
      <div className={styles.quickCashContainer}>
        <div
          className={styles.quickcash}
          style={{ 'background': `url(${quickCash.background}) no-repeat`, 'backgroundSize': 'contain', 'backgroundColor': '#dfe8ff' }}>
          <div className={showLpDescStyle}>
            <div className={showRupeeStyle}>₹</div>
            <div className={showPointStyle}>
              {point}
            </div>
            <div className={styles.quickcashTitle}>
              <div dangerouslySetInnerHTML={titleObj} />
            </div>
          </div>
        </div>
        <div className={styles.text}>
          <div dangerouslySetInnerHTML={descObj} />
        </div>
        {this.getLink(arr)}
      </div>
    ) : null;
  }

  getCoupons() {
    const couponsInfo = at(earlySlotData, 'couponsInfo');
    const isEnable = couponsInfo.enable === 'true';
    const links = couponsInfo ? couponsInfo.links : [];
    let arr = [];
    links.forEach(link => {
      if (link.id === 'scan_qr_code') {
        link.maEventType = 'My Sale - Scan QR click';
        link.gaEventAction = 'scan qr code click';
        link.eventLabel = null;
      }
      if (link.id === 'view_all_coupons') {
        link.maEventType = 'My Sale - View Coupons click';
        link.gaEventAction = 'view coupons click';
        link.eventLabel = null;
      }
    });
    links.forEach(link => {
      if (link.id !== 'scan_qr_code') {
        arr.push(link);
      }
    });
    if (isApp() && this.state.isOSCompatible) {
      arr = links;
    }
    return isEnable ? (
      <div className={styles.couponsContainer}>
        <div className={styles.couponsTitle}>
          {couponsInfo.title}
        </div>
        <div className={styles.couponsText}>
          {couponsInfo.desc}
        </div>
        {this.getLink(arr)}
      </div>
    ) : null;
  }

  sendMAevents(eventType, name, type) {
    TrackMadalytics.sendEvent(eventType, null, {
      'entity_name': name,
      'entity_type': type,
      'entity_id': `${type}_${name}`
    }, {
      'custom': {}
    });
  }

  sendGAevents(event, action, label) {
    Track.event(event, action, label);
  }

  displayLP(data) {
    const enableBanner = get(
      data, 'kvpairs.referAndEarn.banner.enable',
      get(window, '__myx_kvpairs__.referAndEarn.banner.enable')
    ) === true;
    const enableRefferals = get(
      data, 'kvpairs.referAndEarn.enable',
      get(window, '__myx_kvpairs__.referAndEarn.enable')
    ) === true;
    const userLP = data && data.lp ? data.lp.points : at(window, '__myx_lp__.points');
    const loggedIn = data && data.session ? data.session.login : at(window, '__myx_session__.login');
    const showPoints = loggedIn && this.hasValidPoints(userLP);
    const redirectUri = '/referrals-dashboard';
    const isReferralsBannerOn = enableBanner && enableRefferals;
    const isOSCompatible = data ? data.isOSCompatible : at(window, '__myx_isOSCompatible__');
    this.setState({ isReferralsBannerOn, userLP, loggedIn, showPoints, redirectUri, isOSCompatible });
  }

  hasValidPoints(lp) {
    return (!isNaN(lp));
  }

  handleChangeClick() {
    this.setState({
      showSlotPopup: true
    });
  }

  hideSaleSlot() {
    this.setState({
      showSlotPopup: false
    });
    document.body.style.overflow = 'auto';
    document.body.style.position = 'static';
  }

  render() {
    if (this.state.busy) {
      return (
        <div className={styles.loaderDiv}>
          <div className={styles.spinner}></div>
        </div>
      );
    } else if (this.state.showSlotDetails && this.state.loggedIn) {
      if (this.state.slotSelected && this.state.startDate && this.state.endDate) {
        const startDate = this.state.startDate;
        const endDate = this.state.endDate;
        const presentDate = new Date().getTime();
        if (startDate && endDate) {
          return presentDate < endDate ? (
            <div className={styles.mainContainer}>
              <div className={styles.subContainer}>
                {this.getSaleImage()}
                {this.getSlotUser(startDate, endDate)}
                {this.getQuickcash()}
                {this.getCouponsCreativeImage()}
                {this.getCoupons()}
              </div>
              <AdmissionControl
                showSlotChangeScreenOnly={true}
                showSlotPopup={this.state.showSlotPopup}
                hideSaleSlot={this.hideSaleSlot}
                getSlotInfo={this.getCookieSlot}
                slots={getSlots(this.state.showSlotPopup)} />
            </div>
          ) : (
            <div className={styles.mainContainer}>
              <div className={styles.subContainer}>
                {this.getSaleImage()}
                {this.getQuickcash()}
                {this.getCouponsCreativeImage()}
                {this.getCoupons()}
                {this.getSlotUser(startDate, endDate)}
              </div>
              <AdmissionControl
                showSlotChangeScreenOnly={true}
                showSlotPopup={this.state.showSlotPopup}
                hideSaleSlot={this.hideSaleSlot}
                getSlotInfo={this.getCookieSlot}
                slots={getSlots(this.state.showSlotPopup)} />
            </div>
          );
        }
      }
    }
    return (
      <div className={styles.mainContainer}>
        <div className={styles.subContainer}>
          {this.getSaleImage()}
          {this.getNoSlotUser()}
          {this.getQuickcash()}
          {this.getCouponsCreativeImage()}
          {this.getCoupons()}
        </div>
        <AdmissionControl
          showSlotChangeScreenOnly={true}
          showSlotPopup={this.state.showSlotPopup}
          hideSaleSlot={this.hideSaleSlot}
          getSlotInfo={this.getCookieSlot}
          slots={getSlots(this.state.showSlotPopup)} />
      </div>
    );
  }
}

SlotInfoView.propTypes = {
  show: React.PropTypes.bool,
  hideHandler: React.PropTypes.func
};

export default SlotInfoView;
