import React, { Component, PropTypes } from 'react';
import { isBrowser } from '../../../utils';
import at from 'v-at';
import styles from './index.css';

class Seo extends Component {

  constructor(props) {
    super(props);
    let mData = props.metaData;
    let products = [];
    if (isBrowser()) {
      mData = at(window.__myx.searchData, 'seo.metaData');
      products = at(window.__myx.searchData, 'results.products');
    }
    this.state = {
      expand: true,
      metaData: mData,
      products
    };
  }

  getMarkUp(description = '') {
    if (!at(this.state, 'expand') && !this.props.desktop) {
      return { __html: `${description.substr(0, 250)}...` };
    } return { __html: description };
  }

  getReadMoreSection() {
    if (!this.props.desktop) {
      return (
        <span
          onClick={() => this.updateRead()}
          className={styles.toggleRead}> read {at(this.state, 'expand') ? 'less' : 'more'}
        </span>
      );
    } return null;
  }

  getPriceList(title) {
    const products = at(this.state, 'products') || [];
    let bodyData = [];
    for (let i = 0; i < 10 && products[i]; i++) {
      bodyData.push(
        <tr key={`pl-${i}`}>
          <td><a href={(products[i].landingPageUrl || '').toLowerCase()} >{products[i].product}</a></td>
          <td>Rs. {products[i].price}</td>
        </tr>
      );
    }
    return (
      <table style={{ marginTop: '40px' }}>
        <thead>
          <tr>
            <th>{title}</th>
            <th>PRICE (RS)</th>
          </tr>
        </thead>
        <tbody>
          {bodyData}
        </tbody>
      </table>
    );
  }

  getDate() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!

    const yyyy = today.getFullYear();
    if (dd < 10) {
      dd = `0${dd}`;
    }
    if (mm < 10) {
      mm = `0${mm}`;
    }
    today = `${dd}/${mm}/${yyyy}`;
    return today;
  }

  updateRead() {
    this.setState({
      expand: !this.state.expand
    });
  }

  render() {
    const meta = at(this.state, 'metaData') || {};
    const priceDescFlag = true;
    if (meta.page_description) {
      return (
        <div className={styles.seoContainer}>
          {priceDescFlag ? <div className={styles.priceContainer}>
            <h2 className={styles.title}> {meta.page_title} price list </h2>
            {this.getPriceList(meta.page_title)}
            <div className={styles.dateTitle}>Data last updated on {this.getDate()}</div>
          </div> : null}
          <h2 className={styles.title}> buy {meta.page_title} </h2>
          <div
            className={styles.descContainer}
            dangerouslySetInnerHTML={this.getMarkUp(meta.page_description.replace(/http:\/\/www.myntra.com/g, ''))} />
          {this.getReadMoreSection()}
        </div>
      );
    } return null;
  }

}

Seo.propTypes = {
  seoData: PropTypes.object,
  desktop: PropTypes.bool,
  metaData: PropTypes.object
};

export default Seo;
