import React from 'react';
import ReactDOM from 'react-dom';
import Footer from './index';
import at from 'v-at';
import { isBrowser } from '../../utils';

const fRender = () => {
  const isItMobile = isBrowser() && at(window, '__myx_deviceType__') === 'mobile';
  const dData = isBrowser() && at(window, '__myx_deviceData__') ? window.__myx_deviceData__[0] : null;
  const pageName = isBrowser() ? at(window, '__myx.pageName') : '';
  const seoData = isBrowser() && at(window, '__myx_seo__') ? at(window, '__myx_seo__') : null;
  if (document.getElementById('web-footerMount')) {
    ReactDOM.render(
      <Footer
        isMobile={isItMobile}
        pageName={pageName}
        seo={seoData}
        footerData={at(seoData, 'footerData')}
        deviceData={dData} />, document.getElementById('web-footerMount')
    );
  }
};

export default fRender;
