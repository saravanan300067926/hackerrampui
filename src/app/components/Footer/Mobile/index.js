import React from 'react';
import styles from './mobile.css';
import at from 'v-at';
import statics from '../links.json';
import Seo from '../seo/index.js';
import { isBrowser } from '../../../utils';
import onlineShoppingGyan from '../onlineShoppingGyan';
import { legalCompliance } from '../Common';

const constructLink = (links = []) => {
  let linkArr = null;
  try {
    if (typeof links !== 'undefined') {
      linkArr = links.map((val, index) => {
        if (val && val.linkUrl && val.name) {
          if (val && val.linkUrl && (val.linkUrl.indexOf('http') >= 0 || val.linkUrl.match('^/'))) {
            return <a key={index} href={val.linkUrl}> {val.name} </a>;
          } return <a key={index} href={`/${val.linkUrl}`}> {val.name} </a>;
        } return null;
      });
    } return linkArr;
  } catch (ex) {
    console.log('Footer link failed with ', ex);
  } return null;
};

class Mobile extends React.Component {

  constructor(props) {
    super(props);
  }

  getOnlineShoppingText(pageName) {
    const url = isBrowser() ? window.location.href : '';
    if (pageName === 'Home' || pageName === 'Landing Page') {
      return <div className={styles.gyanContainer} dangerouslySetInnerHTML={{ __html: onlineShoppingGyan(pageName, url) }} />;
    } return null;
  }

  render() {
    const footerData = this.props.fData;
    const nData = at(this.props, 'navData') ? at(this.props, 'navData') : statics.navigation;
    return (
      <div className={styles.base}>
        <Seo {...this.props.seo} desktop={false} />
        <div className={styles.usefulLinks}>
          <a href="/" className={styles.oShopping}> ONLINE SHOPPING </a>
          {constructLink(nData)}
        </div>
        <div className={styles.socialLinks}>
          <p> KEEP IN TOUCH </p>
          <a href="https://www.facebook.com/myntra" className={`myntraweb-footer-sprite ${styles.facebook}`}></a>
          <a href="https://twitter.com/myntra" className={`myntraweb-footer-sprite ${styles.twitter}`}></a>
          <a href="https://www.youtube.com/user/myntradotcom" className={`myntraweb-footer-sprite ${styles.youtube}`}></a>
          <a href="https://www.instagram.com/myntra" className={`myntraweb-footer-sprite ${styles.instagram}`}></a>
        </div>
        <div className={styles.usefulLinks}>
          <p> USEFUL LINKS </p>
          {constructLink(statics.usefulLinks)}
        </div>
        {footerData ? <div className={styles.popularLinks}>
          <p> POPULAR LINKS </p>
          {constructLink(footerData)}
        </div> : null}
        <p className={styles.contact}>
          <span> Have issues ? &nbsp;</span>
          <a href="mailto:support@myntra.com"> Contact Us </a>
        </p>
        <p className={styles.copyright}> &copy; {new Date().getFullYear()} www.myntra.com. All rights reserved. </p>
        {legalCompliance(at(this, 'props.pageName'))}
        {this.getOnlineShoppingText(at(this, 'props.pageName'))}
      </div>
    );
  }
}

Mobile.propTypes = {
  fData: React.PropTypes.array,
  pageName: React.PropTypes.string,
  seo: React.PropTypes.object
};

export default Mobile;
