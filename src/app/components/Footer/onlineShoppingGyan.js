const onlineHomePageShoppingGyan = `
<h1><strong>ONLINE SHOPPING MADE EASY AT MYNTRA</strong></h1>
<p>If you would like to experience the best of online shopping for men, women and kids in India, you are at the right place. Myntra is the ultimate destination for fashion and lifestyle, being host to a wide array of merchandise including <a class="seolink" href="/clothing">clothing</a>, footwear, accessories, jewellery, personal care products and more. It is time to redefine your style statement with our treasure-trove of trendy items. Our online store brings you the latest in designer products straight out of fashion houses. You can shop online at Myntra from the comfort of your home and get your favourites delivered right to your doorstep.</p>
<h3><strong>BEST ONLINE SHOPPING SITE IN INDIA FOR FASHION</strong></h3>
<p>Be it clothing, footwear or accessories, Myntra offers you the ideal combination of fashion and functionality for men, women and kids. You will realise that the sky is the limit when it comes to the types of outfits that you can purchase for different occasions.</p>
<ul>
<li><strong>Smart men&rsquo;s clothing</strong> - At Myntra you will find myriad options in smart formal shirts and trousers, cool T-shirts and jeans, or kurta and pyjama combinations for men. Wear your attitude with printed T-shirts. Create the back-to-campus vibe with varsity T-shirts and distressed jeans. Be it gingham, buffalo, or window-pane style, checked shirts are unbeatably smart. Team them up with chinos, cuffed jeans or cropped trousers for a smart casual look. Opt for a stylish layered look with biker jackets. Head out in cloudy weather with courage in water-resistant jackets. Browse through our innerwear section to find supportive garments which would keep you confident in any outfit.</li>
<li><strong>Trendy women&rsquo;s clothing</strong> - <a class="seolink" href="/shop/women">Online shopping for women</a> at Myntra is a mood-elevating experience. Look hip and stay comfortable with chinos and printed shorts this summer. Look hot on your date dressed in a little black dress, or opt for red dresses for a sassy vibe. Striped dresses and T-shirts represent the classic spirit of nautical fashion. Choose your favourites from among Bardot, off-shoulder, shirt-style, blouson, embroidered and peplum tops, to name a few. Team them up with skinny-fit jeans, skirts or palazzos. Kurtis and jeans make the perfect fusion-wear combination for the cool urbanite. Our grand <a class="seolink" href="/saree">sarees</a> and lehenga-choli selections are perfect to make an impression at big social events such as weddings. Our salwar-kameez sets, kurtas and Patiala suits make comfortable options for regular wear.</li>
<li><strong>Fashionable footwear</strong> - While clothes maketh the man, the type of footwear you wear reflects your personality. We bring you an exhaustive lineup of options in casual shoes for men, such as sneakers and loafers. Make a power statement at work dressed in brogues and oxfords. Practice for your marathon with running shoes for men and women. Choose shoes for individual games such as tennis, football, basketball, and the like. Or step into the casual style and comfort offered by sandals, sliders, and flip-flops. Explore our lineup of fashionable footwear for ladies, including pumps, heeled boots, wedge-heels, and pencil-heels. Or enjoy the best of comfort and style with embellished and metallic flats.</li>
<li><strong>Stylish accessories</strong> - Myntra is one of the best online shopping sites for classy accessories that perfectly complement your outfits. You can select smart analogue or digital watches and match them up with belts and ties. Pick up spacious bags, backpacks, and wallets to store your essentials in style. Whether you prefer minimal jewellery or grand and sparkling pieces, our online jewellery collection offers you many impressive options.</li>
<li><strong>Fun and frolic</strong> - Online shopping for kids at Myntra is a complete joy. Your little princess is going to love the wide variety of pretty dresses, ballerina shoes, headbands and clips. Delight your son by picking up sports shoes, superhero T-shirts, football jerseys and much more from our online store. Check out our lineup of toys with which you can create memories to cherish.</li>
<li><strong>Beauty begins here </strong>- You can also refresh, rejuvenate and reveal beautiful skin with personal care, beauty and grooming products from Myntra. Our soaps, shower gels, skin care creams, lotions and other ayurvedic products are specially formulated to reduce the effect of aging and offer the ideal cleansing experience. Keep your scalp clean and your hair uber-stylish with shampoos and hair care products. Choose makeup to enhance your natural beauty.</li>
</ul>
<p>Myntra is one of the best online shopping sites in India which could help transform your living spaces completely. Add colour and personality to your bedrooms with bed linen and curtains. Use smart tableware to impress your guest. Wall decor, clocks, <a class="seolink" href="/photo-frames">photo frames</a> and artificial plants are sure to breathe life into any corner of your home.</p>
<h3><strong>AFFORDABLE FASHION AT YOUR FINGERTIPS</strong></h3>
<p>Myntra is one of the unique online shopping sites in India where fashion is accessible to all. Check out our new arrivals to view the latest designer clothing, footwear and accessories in the market. You can get your hands on the trendiest style every season in western wear. You can also avail the best of ethnic fashion during all Indian festive occasions. You are sure to be impressed with our seasonal discounts on footwear, trousers, shirts, backpacks and more. The end-of-season sale is the ultimate experience when fashion gets unbelievably affordable.</p>
<h3><strong>MYNTRA INSIDER</strong></h3>
<p>Every online shopping experience is precious. Hence, a cashless reward-based customer loyalty program called <a class="seolink" href="/myntrainsider">Myntra Insider</a> was introduced to enhance your online experience. The program is applicable to every registered customer and measures rewards in the form of Insider Points.</p>
<p>There are four levels to achieve in the program, as the Insider Points accumulate. They are - Insider, Select, Elite or Icon. Apart from offering discounts on Myntra and partner platform coupons, each tier comes with its own special perks.</p>
<p><strong>Insider</strong></p>
<ul>
<li style="list-style-type: disc;">Opportunity to master any domain in fashion with tips from celebrity stylists at Myntra Masterclass sessions.</li>
<li style="list-style-type: disc;">Curated collections from celeb stylists.</li>
</ul>
<p><strong>Elite</strong></p>
<ul>
<li style="list-style-type: disc;">VIP access to special sale events such as the End of Reason Sale (EORS) and product launches.</li>
<li style="list-style-type: disc;">Exclusive early access to Limited Edition products</li>
</ul>
<p><strong>Icon</strong></p>
<ul>
<li style="list-style-type: disc;">Chance to get on guest lists for special events.</li>
</ul>
<h3><strong>Myntra Studio - The Personalised Fashion Feed You Wouldn&rsquo;t Want To Miss Out On</strong></h3>
<p>The world wide web is evolving at a relentless pace, and with an accelerated growth each passing year, there is bound to be an overwhelming surge of online content. It was for this very reason that personalisation of search feeds was proposed as a solution to combat the overload of irrelevant information.</p>
<p>Several social media platforms such as Facebook and Instagram along with various online shopping websites have chosen to help filter content, increasing user engagement, retention and customer loyalty.</p>
<p>Myntra is one such online shopping website that joins the list of platforms that help curate a personalised fashion feed. Named the<a class="seolink" href="/studio/home">Myntra Studio</a>, this personalised search feed brings you the latest men and women&rsquo;s fashion trends, celebrity styles, branded content and daily updates from your favourite fashion labels.</p>
<p>If you are wondering how impactful Myntra Studio can be, we are listing out five perks of having a rich, meaningful, and personalised fashion feed in your life.</p>
<ul>
<li><strong>Keep Up With What Your Favourite Fashion Icons Are Upto</strong></li>
<p>The #OOTD, AKA outfit of the day hashtag trend has been a rage among fashion bloggers and stylists. The whole concept of building an outfit from scratch and showcasing it to a huge community of enthusiasts using the hashtag has helped individuals with understanding trends and making suitable for daily wear.</p>
<p>Imagine if you could keep up with every piece of clothing and accessory worn by the fashion icons you look upto. From Sonam Kapoor to Hailey Baldwin Bieber, Myntra Studio has a &lsquo;Stories&rsquo; feature to help track celebrity fashion trends, exploring details such as their outfit of the day. This way, you would not ever miss out on the latest celebrity fashion trends, from all around the world.</p>
<li><strong>Quick Fashion Tip And Tricks</strong></li>
<p>Whether it is draping a saree into a dhoti style, wearing the right lingerie under certain dresses or discovering multiple uses out of heavy ethnic wear, Myntra Studio will help you acquire some unique and useful fashion hacks. Each hack is designed with the intention to help you get the best wear out of everything in your wardrobe.</p>
<li><strong>Updates on What Is Trending and New Product Launches</strong></li>
<p>Since fast fashion seems to be extremely hard to keep up with these days, a quick update on what is trending in accessories, clothing and footwear would certainly be of great help. Myntra Studio helps you stay connected to the most beloved and sought after brands such as Puma, Coverstory, The Label Life and so many more.</p>
<p>Your feed keeps you updated with stories of what the brands are creating including clothing, footwear and jewellery, along with their new seasonal collections.</p>
<li><strong>Explicit Step-By-Step Beauty Routines From Experts</strong></li>
<p>Just like fashion, the beauty community keeps on growing, and with brands such as Huda Beauty, MAC and the latest Kay Beauty by Katrina Kaif, are constantly coming up with mind-blowing products. Whether it is creating a no-makeup look, different winged eyeliners, do-it-yourself facial masks and other personal care beauty routines, Myntra Studio is here for you.</p>
<li><strong>Celebrity Confessions And A Look Into Their Lives</strong></li>
<p>A bonus feature that Myntra Studio has in store for you is celebrity confessions and a peek into their lives. So, Myntra helps you stay connected to your most beloved celebrities in a matter of clicks.</p>
<p>If you are very particular when it comes to the content you wish to view and engage with on social media, the ability to intricately filter content helps achieve that. Applying the same formula for hardcore fashion lovers and shoppers, Myntra Studio brings you a daily fashion fix incorporating everything that you love, all at one place. Sign up on Myntra today and start organising your fashion feed, just the way you want to.</p>
</ul>
<h3><strong>MYNTRA APP</strong></h3>
<p>Myntra, India&rsquo;s no. 1 online fashion destination justifies its fashion relevance by bringing something new and chic to the table on the daily. Fashion trends seem to change at lightning speed, yet the Myntra shopping app has managed to keep up without any hiccups. In addition, Myntra has vowed to serve customers to the best of its ability by introducing its first-ever loyalty program, The Myntra Insider. Gain access to priority delivery, early sales, lucrative deals and other special perks on all your shopping with the Myntra app. Download the Myntra app on your <a class="seolink" href="https://play.google.com/store/apps/details?id=com.myntra.android">Android</a> or <a class="seolink" href="https://itunes.apple.com/in/app/myntra-indias-fashion-store/id907394059">IOS</a> device today and experience shopping like never before!</p>
<h3><strong>HISTORY OF MYNTRA</strong></h3>
<p>Becoming India&rsquo;s no. 1 fashion destination is not an easy feat. Sincere efforts, digital enhancements and a team of dedicated personnel with an equally loyal customer base have made Myntra the online platform that it is today. The original B2B venture for personalized <a class="seolink" href="/gifts">gifts</a> was conceived in 2007 but transitioned into a full-fledged ecommerce giant within a span of just a few years. By 2012, Myntra had introduced 350 Indian and international brands to its platform, and this has only grown in number each passing year. Today Myntra sits on top of the online fashion game with an astounding social media following, a loyalty program dedicated to its customers, and tempting, hard-to-say-no-to deals.</p>
<p>The Myntra shopping app came into existence in the year 2015 to further encourage customers&rsquo; shopping sprees. Download the app on your Android or IOS device this very minute to experience fashion like never before</p>
<h3><strong>SHOP ONLINE AT MYNTRA WITH COMPLETE CONVENIENCE</strong></h3>
<p>Another reason why Myntra is the best of all online stores is the complete convenience that it offers. You can view your favourite brands with price options for different products in one place. A user-friendly interface will guide you through your selection process. Comprehensive size charts, product information and high-resolution images help you make the best buying decisions. You also have the freedom to choose your payment options, be it card or cash-on-delivery. The 30-day returns policy gives you more power as a buyer. Additionally, the try-and-buy option for select products takes customer-friendliness to the next level.</p>
<p>Enjoy the hassle-free experience as you shop comfortably from your home or your workplace. You can also shop for your friends, family and loved-ones and avail our gift services for special occasions.</p>
`;

const menOnlineShoppingGyan = `
  <h1><strong>MEN’S SHOPPING AT MYNTRA: A SUPERIOR EXPERIENCE </strong></h1>
  <p> Myntra is one of the best sites when it comes to online shopping for men. The finest of material, superior design and 
  unbeatable style go into the making of our men’s shopping collection. Our range of online shopping men’s wear, accessories, 
  footwear and personal care products are second to none. Compared with other men’s shopping sites, Myntra brings you the best 
  price products which won’t hurt your pocket. With seasonal discounts on trendy casual wear, <a href="/suits" class="seoLink">suits</a>,
   blazers, sneakers and more, online shopping for men at Myntra just gets even more irresistible!  </p>
  <h3><strong>ONLINE SHOPPING FOR MEN: OPTIONS UNLIMITED </strong></h3>
  <p>At Myntra, our online shopping fashion for men collection features plenty of options to create multiple outfits. At our men’s 
  online shop we have brought together an exhaustive range of products from the best men’s brands. Here is a list of must-haves from 
  the wide variety of awesome products at Myntra:</p>
  <ul>
  	<li>Opt for a charming yet laid-back look with cool <strong>T-shirts</strong> and casual shirts worn with stylish jeans, casual trousers or 
  	shorts. Stay sharp and sophisticated with our smart options in formal shirts and trousers. Look dapper when meeting your clients 
  	in our smooth suits. Put on trendy blazers for formal occasions. On your online men’s clothes’ shopping journey, make sure you 
  	include kurtas, jackets and sherwanis from our festive wear collection. Stay warm and comfortable in sweaters and sweatshirts. 
  	Get fit and ready for adventure, with our sports and active wear collection. </li>
  	<li>Once you are done with your online men’s clothes’ shopping, make sure you pick up the right accessories to complement your 
  	look. Whether you are travelling to work or outside the city our wide variety of bags, backpacks and luggage collection will 
  	ensure you are well-packed. Our beautiful <a href="/watches" class="seoLink">watches</a> and <a href="/smart-watches" class="seoLink">smart watches</a>
  	 work well to enhance your overall style quotient. Reach out for our sunglasses during the summers – let your eyes stay 
  	 protected while you opt for maximum swag.</li>
  	<li>Bring impeccable style to your shoe closet with our incredible collection of footwear for men. Look classy during formal 
  	and semi-formal occasions with derbies, loafers and oxfords. Stay hip and happening in parties with boat shoes, monks and 
  	brogues from our casual men’s footwear range. Lead an active lifestyle with sneakers and running shoes from our sports 
  	footwear selection. Pick up sandals, floaters and flip-flops for a trip to the beach. We also host socks in our men’s online 
  	shopping collection. That’s basically everything under one roof!
	</li>
  </ul>
  <p>Make sure you check out fun printed men’s T-shirts featuring your favourite characters from DC Comics and Marvel studios. 
  Relive the magic of your favourite superhero from <a href="/justice-league" class="seoLink">Justice League</a>. Fly high with 
  <a href="/superman" class="seoLink">Superman</a>, battle the bad guys with <a href="/batman" class="seoLink">Batman</a>, or get 
  trendy in lightning-speed with a Flash T-shirt. Grab our cool <a href="/marvel-tshirts" class="seoLink">Marvel Avengers T-shirts</a>. 
  Stay powered up with the Iron Man, or walk with the warriors in a Thor T-shirt. </p>
  <p>Our online shopping fashion for mens collection includes even more amazing merchandise such as innerwear, sleepwear, track 
  pants, personal care, wallets, belts and other fashion accessories. </p>
  <h3><strong>MEN’S SHOPPING MADE EASY AT MYNTRA </strong></h3>
  <p>Myntra is the most convenient men’s online store, what with our simplified shopping and payment procedures. With just a few 
  clicks of the mouse or taps on your smartphone, you can buy your favorites from the best men’s brands right away.  </p>
`;

const womenOnlineShoppingGyan = `
  <h1><strong>ONLINE SHOPPING FOR WOMEN: MYNTRA ALL THE WAY </strong></h1>
  <p> When it comes to women, shopping is more than just a therapy. We believe that it is a joyous activity where you get to 
  choose a whole new lifestyle. If you would like access to a wide variety of products for women online shopping is the way to go. 
  And of all the online shopping sites for women, Myntra is the place to find the finest brands of women’s fashion and lifestyle 
  products.  The women’s shopping range at Myntra includes the best price merchandise of top-notch quality. </p>
  <h3><strong>WOMEN’S ONLINE SHOPPING: CLOTHES, ACCESSORIES, FOOTWEAR, AND MORE </strong></h3>
  <p>While a few online shopping sites for women might focus on apparel, others on ethnic wear, and yet others on home décor, 
  Myntra brings you everything under one roof.  Here is a list of must-haves from the wide variety of awesome products at Myntra:</p>
  <ul>
  	<li>Your online shopping clothes’ list should include a healthy mix of apparel suitable for casual, formal and festive occasions. 
  	Look cool in <a href="/tshirts" class="seoLink">T-shirts</a>, tops and jeans. Ooze maximum oomph with our top-of-the-range dresses. 
  	Climb up the corporate ladder with chic formal wear. Try the ethnic touch with our pretty printed-kurtas, Patiala 
  	<a href="/salwar-suit" class="seoLink">salwars</a> and dhotis. Stride into a wedding with confidence in a lehenga choli or an elegant saree. </li>
  	<li>If you are generally reluctant to step out to purchase innerwear for women online shopping provides a comfortable, private 
  	setting where you can shop in peace. Right from basic <a href="/bras" class="seoLink">bras</a> and briefs, to shape wear and 
  	swimwear, we have an option for each of your needs. You can include in your online clothes’ shopping list cosy winter wear and 
  	sporty sweatshirts and tracksuits as well. </li>
  	<li>Once you are done with your online clothes’ shopping, it is time to buy smart jewellery. Our range of pretty 
  	<a href="/rings" class="seoLink">rings</a>, necklaces, bracelets and more have been crafted to highlight your natural beauty. 
  	You can find smart accessories here as well such as handbags, purses, sunglasses and beautiful analogue and digital watches for 
  	any occasion.</li>
  	<li>Your online women’s shopping adventure should include a trip to our footwear section. Feel fabulous in fine footwear from the 
  	best brands crafted to perfection. Be it handcrafted mojaris and juttis or single-toe flats from our ethnic section, or pumps and 
  	peep-toe shoes from our western section, you are sure to be dressed to impress.</li>
  </ul>
  <p>And there are more fabulous products such as sportswear, blazers, <a href="/gowns" class="seoLink">gowns</a>, dress material, 
  bath accessories, <a href="/kajal" class="seoLink">kajal</a> and <a href="/makeup" class="seoLink">makeup</a> for women online. 
  The women’s online shopping experience at Myntra is filled with unlimited choices!</p>
  <h3><strong>ONLINE SHOPPING FOR WOMEN MADE EASY </strong></h3>
  <p>The women’s shopping experience at a physical store is fraught with problems. Long payment queues, crowded shops and struggles 
  for parking spaces are not enjoyable. At Myntra you can complete your shopping and make your payment in a jiffy. Just a few clicks, 
  	or a few taps on your smartphone, and you are done! Enjoy your online women’s shopping journey at Myntra and get set to redefine your 
  lifestyle.  
</p>
`;

const kidsOnlineShoppingGyan = `
  <h1><strong>MYNTRA FOR KIDS: IRRESISTIBLY COOL AND COMFORTABLE  </strong></h1>
  <p>It is all fun and frolic when it comes to online shopping for kids at Myntra. We bring you an exhaustive lineup of children’s 
  dresses, accessories and footwear for all occasions. We understand the amount of care which goes into raising a child. Therefore 
  at Myntra, we have taken all efforts to bring you top-notch local and international kids’ brands. You don’t have to step into a 
  kids’ store anymore, or browse through several kids’ online shopping sites. You will find the best price kids’ products of the 
  highest quality right here. You can do all your online shopping for kids at Myntra without any worries.</p>
  <h3><strong>MYNTRA: THE ONE-STOP KIDS’ SHOP </strong></h3>
  <p>Myntra brings you a wide range of the latest kids’ dress collection as well as accessories, footwear and more. Browse through 
  our exhaustive collection to learn why we are a single-destination kids’ shop, where you can find everything you need for your 
  precious one.</p>
  <ul>
  	<li>Children’s clothing – We bring you the finest collection of children’s clothing online for all occasions. Dress your 
  	children in smart printed T-shirts and tops, paired with jeans and trousers. Our trendy, summery collections include shorts 
  	and skirts of all varieties. Opt for cute <a href="/kids-clothing" class="seoLink">children’s dresses</a> from our kids’ store. 
  	You will find many party options from our kids’ dress collection including sequined, shimmery and other embellished versions. 
  	Keep your babies warm and comfortable in rompers and sleep wear. Your kids’ shopping should include some ethnic wear for festivals. 
  	Choose from a fine range of lehenga-cholis, kurta-pyjamas, sherwanis and more. Stock up on sweaters, jackets and sweatshirts for 
  	the winters. Track pants and tracksuits work well for training and sports. </li>
  	<li>Children’s footwear – Your kids’ online shopping is not complete without footwear. Check out sneakers and 
  	<a href="/sports-shoes" class="seoLink">sports shoes</a> at Myntra for boys and girls which feature comfortable fits to keep them 
  	active through the day. Your daughters can keep their feet comfortable in beautifully crafted footwear such as Mary Janes, heels, 
  	wedges, ballerinas and woven flats. We also feature very functional footwear for boys in our kids’ shopping range such as sandals, 
  	clogs, slip-on shoes and tennis shoes. Buy colourful boots for rainy days and flip-flops for the next trip to the beach. </li>
  	<li>Children’s accessories – Your kid’s outfit is not complete without the addition of smart accessories from our kids’ online store. 
  	School-going children would love our cool versions of <a href="/school-bags" class="seoLink">school bags</a> and backpacks. Our 
  	colourful printed sunglasses and dial watches add to their cuteness factor.  Your little girls are surely going to love our 
  	collection of fancy hairbands and hair clips.</li>
  </ul>
  <p>Make sure you bring home fun, printed merchandise featuring your child’s favourite characters from DC Comics and Marvel studios. 
  They can now glide along with <a href="/spiderman" class="seoLink">Spiderman</a>, and pack a punch with Thor or the <strong>Incredible</strong> 
  <a href="/hulk" class="seoLink">Hulk</a>. They can battle the dark forces with the <a href="/justice-league-boys-tshirts" class="seoLink">Justice League</a>, 
  flying high with <a href="/superman-boys-clothing" class="seoLink">Superman</a>, geared up with <a href="/batman-boys-clothing" class="seoLink">Batman</a>, 
  and jumping around with Flash or Wonder Woman. You can check out a variety of cool kids’ T-shirts jackets, joggers, footwear, bath 
  towels, socks, action figures and play sets in this category. </p>
  <h3><strong>THE MOST CONVENIENT KID’S ONLINE STORE </strong></h3>
  <p>Compared with other kids’ online shopping sites, Myntra is one of the most convenient places for purchasing everything that a 
  kid needs. With simplified browsing, selection and payment procedures, you are bound to enjoy your kids’ online shopping experience. 
  And we are not just a kids’ store. You can buy great quality merchandise for men and women as well from the comfort of your home. </p>
`;

const onlineShoppingGyan = function (pageName, url) {
  let shoppingGyan = '';
  if (pageName === 'Home') {
    shoppingGyan = onlineHomePageShoppingGyan;
  } else {
    const currentPage = url.substring(url.indexOf('/shop/') + 6, url.length);
    if (currentPage === 'men') {
      shoppingGyan = menOnlineShoppingGyan;
    } else if (currentPage === 'women') {
      shoppingGyan = womenOnlineShoppingGyan;
    } else if (currentPage === 'kids') {
      shoppingGyan = kidsOnlineShoppingGyan;
    }
  }
  return shoppingGyan;
};

export default onlineShoppingGyan;
