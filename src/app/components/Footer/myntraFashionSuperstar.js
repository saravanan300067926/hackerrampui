function myntraFashionSuperstar() {
  return `
      <div>
      <h3><strong>ABOUT THE SHOW:</strong></h3>
      <p>If you’ve always dreamt of living in the spotlight, flaunting your style &amp; getting the world to follow you, 
      you’re one step closer to living that dream! Myntra Fashion Superstar, a first of its kind digital reality show, 
      brings to you a unique chance to become India's next big fashion influencer.</p>
      <p>Apply now by uploading your video!</p>

      <h3><strong>HOW TO APPLY?</strong></h3>

      <p><strong>Step 1: Make a video</strong><br />Create a 60-second video about your personal style. 
      Make sure you talk about your fashion and style sensibilities in the video.</p>
      
      <p><strong>Step 2: Sign-in</strong><br />Make sure you’re signed into Myntra with a valid email id or phone 
      number so that we can reach you if you’re shortlisted.</p>

      <p><strong>Step 3: Upload</strong><br />Upload your audition video here. Go through the T&amp;C below before you hit upload</p>

      <p><strong>Step 4: Sit back and relax</strong><br />We will reach out to you on the email id/ number 
      you’ve provided at the time of signing in if you’re shortlisted.</p>
      <p><strong>Step 4: Be alert</strong><br />Once we reach out to you, you‘ll have 24 hours to get back 
      to us with your details as asked by Myntra, to be considered for further evaluation.</p>

      <p>Also follow <strong>@MyntraFashionSuperstar</strong> on Facebook and Instagram and wait for further 
      announcements regarding the auditions on here.</p>

      <h3><strong>THE WINNER OF THE SHOW</strong></h3>
      <ul>
        <li>Will be crowned Myntra Fashion Superstar</li>
        <li>Will become the hot fashion expert on Myntra & Zoom</li>
        <li>Will get featured in a top fashion magazine</li>
      </ul>
    </div>
  `;
}

export default myntraFashionSuperstar;
