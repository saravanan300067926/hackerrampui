import React from 'react';
import styles from './desktop.css';
import onlineShoppingGyan from '../onlineShoppingGyan';
import myntraFashionSuperstar from '../myntraFashionSuperstar';
import statics from '../links.json';
import Seo from '../seo/index.js';
import { legalCompliance } from '../Common';
import { isBrowser } from '../../../utils';
import at from 'v-at';
import LazyLoad from 'react-lazyload';

function trackPlayStoreClick() {
  if (window.ga) {
    window.ga('send', 'event', 'android_app_download', 'click_link_on_footer', window.location.href);
  }
}

const constructLink = (links = []) => {
  let linkArr = null;
  try {
    if (typeof links !== 'undefined' && links) {
      linkArr = links.map((val, index) => {
        if (val && val.linkUrl && val.name) {
          if ((val.linkUrl.indexOf('http') >= 0 || val.linkUrl.indexOf('https') >= 0 || val.linkUrl.match('^/'))) {
            return <a key={index} href={val.linkUrl}> {val.name} </a>;
          } return <a key={index} href={`/${val.linkUrl}`}> {val.name} </a>;
        } return null;
      });
    } return linkArr;
  } catch (ex) {
    console.log('Footer link failed with ', ex);
  } return null;
};

const getOnlineShoppingText = (pageName) => {
  const url = isBrowser() ? window.location.href : '';
  if (pageName === 'Home' || pageName === 'Landing Page') {
    return <div className={styles.gyanContainer} dangerouslySetInnerHTML={{ __html: onlineShoppingGyan(pageName, url) }} />;
  } return null;
};

const getMyntraFashionSuperstar = () => {
  if (isBrowser() && window.location.pathname.includes('/shop/myntra-fashion-superstar')) {
    return (
      <div
        className={`${styles.gyanContainer} ${styles.myntraFashionSuperstarContainer}`}
        dangerouslySetInnerHTML={{ __html: myntraFashionSuperstar() }} />
    );
  } return null;
};

const getFreeDeliveryChargeValue = () => {
  if (isBrowser()) {
    const kvPairs = at(window, '__myx_kvpairs__') || {};
    const shippingValue = kvPairs['shipping.charges.cartlimit'];
    if (shippingValue) {
      return (
        <div className={styles.section}>
          <div className={styles.delivery}>
            <LazyLoad height={43} once offset={100}>
              <img
                src="https://constant.myntassets.com/web/assets/img/cafa8f3c-100e-47f1-8b1c-1d2424de71041574602902399-truck.png"
                style={{ width: '48px', height: '43px' }} />
            </LazyLoad>
          </div>
          <div><strong>Get free delivery </strong>for every order above Rs. {shippingValue}</div>
        </div>
      );
    }
    return null;
  }
  return null;
};

const Desktop = (props) =>
  <footer className={styles.footerContainer}>
    <div className={styles.base}>
      <Seo {...props.seo} desktop />
      <div className={styles.genericInfo}>
        <div className={styles.shopLinks} >
          <p className={styles.gInfoTitle}>
            <a href="/?src=onlineShopping"> ONLINE SHOPPING </a>
          </p>
          {constructLink(props.navData)}
          <a key="giftCards#1" href="/giftcard">
            Gift Cards
          </a>
          <a key="myntrainsider#1" href="/myntrainsider?cache=false">
            Myntra Insider
            <span className={styles.superscript}> New </span>
          </a>
        </div>
        <div className={styles.usefulLinks}>
          <p className={styles.gInfoTitle}> USEFUL LINKS </p>
          {constructLink(statics.usefulLinks)}
        </div>
        <div className={styles.appExperience}>
          <p className={styles.gInfoTitle}> EXPERIENCE MYNTRA APP ON MOBILE </p>
          <div className={styles.downLinkContainer}>
            <a
              href={statics.googleAppDownloadLink} onClick={trackPlayStoreClick}>
              <LazyLoad height={42} once offset={100}>
                <img
                  className={styles.androidDownLink}
                  src="https://constant.myntassets.com/web/assets/img/80cc455a-92d2-4b5c-a038-7da0d92af33f1539674178924-google_play.png" />
              </LazyLoad>
            </a>
            <a href={statics.iosAppDownLoadLink} >
              <LazyLoad height={42} once offset={100}>
                <img
                  className={styles.iOSDownLink}
                  src="https://constant.myntassets.com/web/assets/img/bc5e11ad-0250-420a-ac71-115a57ca35d51539674178941-apple_store.png" />
              </LazyLoad>
            </a>
          </div>
          <div className={styles.keepInTouch}> KEEP IN TOUCH </div>
          <a href="https://www.facebook.com/myntra" className={styles.facebook}>
            <LazyLoad height={20} once offset={100}>
              <img
                src="https://constant.myntassets.com/web/assets/img/d2bec182-bef5-4fab-ade0-034d21ec82e31574604275433-fb.png"
                style={{ width: '20px', height: '20px' }} />
            </LazyLoad>
          </a>
          <a href="https://twitter.com/myntra" className={styles.twitter}>
            <LazyLoad height={20} once offset={100}>
              <img
                src="https://constant.myntassets.com/web/assets/img/f10bc513-c5a4-490c-9a9c-eb7a3cc8252b1574604275383-twitter.png"
                style={{ width: '20px', height: '20px' }} />
            </LazyLoad>
          </a>
          <a href="https://www.youtube.com/user/myntradotcom" className={styles.youtube}>
            <LazyLoad height={20} once offset={100}>
              <img
                src="https://constant.myntassets.com/web/assets/img/a7e3c86e-566a-44a6-a733-179389dd87111574604275355-yt.png"
                style={{ width: '28px', height: '20px' }} />
            </LazyLoad>
          </a>
          <a href="https://www.instagram.com/myntra" className={styles.instagram}>
            <LazyLoad height={20} once offset={100}>
              <img
                src="https://constant.myntassets.com/web/assets/img/b4fcca19-5fc1-4199-93ca-4cae3210ef7f1574604275408-insta.png"
                style={{ width: '20px', height: '22px', position: 'relative', top: '1px' }} />
            </LazyLoad>
          </a>
        </div>
        <div className={styles.promises}>
          <div className={styles.section}>
            <div className={styles.original}>
              <LazyLoad height={40} once offset={100}>
                <img
                  src="https://constant.myntassets.com/web/assets/img/6c3306ca-1efa-4a27-8769-3b69d16948741574602902452-original.png"
                  style={{ width: '48px', height: '40px' }} />
              </LazyLoad>
            </div>
            <div><strong>100% ORIGINAL </strong> guarantee for all products at myntra.com </div>
          </div>
          <div className={styles.section}>
            <div className={styles.return}>
              <LazyLoad height={49} once offset={100}>
                <img
                  src="https://constant.myntassets.com/web/assets/img/ef05d6ec-950a-4d01-bbfa-e8e5af80ffe31574602902427-30days.png"
                  style={{ width: '48px', height: '49px' }} />
              </LazyLoad>
            </div>
            <div><strong>Return within 30days </strong>of receiving your order</div>
          </div>
          {getFreeDeliveryChargeValue()}
        </div>
      </div>
      <div className={styles.popularSearch}>
        {props.fData ? <div>
          <hr />
          <div className={styles.pSearchTitle}> POPULAR SEARCHES </div>
          <div className={styles.pSearchlinks}> {constructLink(props.fData)} </div> </div> : null}
      </div>
      <div className={styles.fInfoSection}>
        <div className={styles.contact}>
          In case of any concern, <a href={'/contactus'}> Contact Us </a>
        </div>
        <div className={styles.copywrite}>
          &copy; {new Date().getFullYear()} www.myntra.com. All rights reserved.
        </div>
      </div>
      {legalCompliance(props.pageName)}
      {getOnlineShoppingText(props.pageName)}
      {getMyntraFashionSuperstar()}
    </div>
  </footer>;

Desktop.propTypes = {
  navData: React.PropTypes.array,
  fData: React.PropTypes.array,
  pageName: React.PropTypes.string,
  seo: React.PropTypes.object
};

export default Desktop;
