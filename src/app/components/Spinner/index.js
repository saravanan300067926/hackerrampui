import React from 'react';
import styles from './spinner.css';

function Spinner() {
  return <div className={styles.spinner} />;
}

export default Spinner;
