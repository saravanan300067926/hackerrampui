import React from 'react';
import styles from './loader.css';
import Spinner from '../Spinner';

class Loader extends React.Component {
  render() {
    if (this.props.show) {
      return (
        <div className={styles.container}>
          <Spinner />
        </div>
      );
    }
    return null;
  }
}

Loader.propTypes = {
  show: React.PropTypes.bool.isRequired
};

Loader.defaultProps = {
  show: false
};

export default Loader;
