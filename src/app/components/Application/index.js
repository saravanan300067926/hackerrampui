import React, { PropTypes } from 'react';
import styles from './application.css';
import { onMount } from './actions';
import { getDateOffset } from '../../utils/serverDateUtil';
import Madalytics from 'madalytics-web';
import FreeShippingBanner from '../Search/Results/Banners/FreeShippingBanner';

global.Madalytics = Madalytics;

class Application extends React.Component {
  componentDidMount() {
    getDateOffset();
    onMount();
  }

  render() {
    return (
      <div>
        <div className={styles.base}>
          {this.props.children}
        </div>
        <FreeShippingBanner />
      </div>
    );
  }
}

Application.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ])
};

export default Application;
