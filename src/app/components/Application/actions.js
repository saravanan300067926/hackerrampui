import { isBrowser } from '../../utils';
import at from 'v-at';
import includes from 'lodash/includes';
import {isApp} from "../../utils/isApp";
import updateLocationContext from '../../common/LocationContext/LocationContext';


function pushDataLayerObjectForGtm() {
  const traffic = window.__myx_traffic__ || {};
  const session = window.__myx_session__ || {};
  const deviceData = window.__myx_deviceData__ || {};
  let source;

  if (includes(window.location.search, 'utm_source')) {
    source = 'direct';
  } else {
    source = traffic.source;
  }

  window.dataLayer.push({
    pageName: traffic.pageName || at(window, '__myx.pageName'),
    utmCampaign: traffic.campaign,
    campaign: traffic.campaign,
    medium: traffic.medium,
    source,
    channel: traffic.channel,
    userEmail: session.login,
    userHashId: session.userHashId,
    userFBId: session.fb_uid,
    isLoggedIn: session.login ? '1' : '0',
    isBuyer: session.returningCustomer ? '1' : '0',
    deviceName: deviceData.deviceName,
    deviceType: deviceData.deviceType
  });
}

export function onMount() {
  if (!isBrowser()) {
    return;
  }
  pushDataLayerObjectForGtm();
  if (isApp()) {
    document.getElementById('mountRoot').style.minHeight = '550px';
  }
  updateLocationContext();
}

export function reset() {
  if (!isBrowser()) {
    return;
  }
  document.getElementById('mountRoot').style.minHeight = '550px';
}
