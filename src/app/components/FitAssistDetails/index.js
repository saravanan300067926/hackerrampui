import React from 'react';
import at from 'v-at';
import classNames from 'classnames/bind';
import Styles from './fitAssistDetails.css';

const bindClass = classNames.bind(Styles);

function getStarted() {
  window.location = '/my/sizeProfiles';
}

function goBack() {
  window.history.back();
}

class FitAssistDetails extends React.PureComponent {
  render() {
    const referrer = at(window, '__myx.referrer');
    return (
      <div>
      {
        at(window, '__myx_deviceData__.isMobile') &&
          <div className={Styles.container}>
            <div className={bindClass('myntraweb-sprite', 'myntraLogo')}></div>
            <div className={bindClass('myntraweb-sprite', 'scaleImage')}></div>
            <div>
              <div className={Styles.title}>FIT ASSIST</div>
              <div className={Styles.subTitle}>YOU FIND THE FASHION, WE FIND THE FIT</div>
            </div>
            <div className={Styles.divider}></div>
            <div className={Styles.intro}>
              <div style={{ marginBottom: '6px' }}>Ever felt disappointed that a t-shirt you ordered online didn't fit you well?</div>
              <div>Or the 'perfect' shirt that you ordered for your father on Father's day didn't fit him?</div>
            </div>
            <span className={Styles.mainHighlight}>
              Myntra Fit Assist helps you find the right size whenever you shop for yourself or others.
              So get started by giving your size information!
            </span>
            <div className={Styles.subBlock}>
              <div className={Styles.blockHeading}>TAG YOUR PURCHASES</div>
              <div className={Styles.intro}>So, you just bought a trendy pair of jeans for your spouse. Tag this item to her.
                Next time you shop for her, Myntra will recommend the perfect size!
              </div>
              <div className={Styles.mainGif}>
                <img src="https://constant.myntassets.com/web/assets/img/11504857276610-fitassist.gif" />
              </div>
            </div>
            <div className={Styles.subBlock}>
              <div className={Styles.blockHeading}>FILL YOUR SIZE DETAILS</div>
              <div className={Styles.intro}>Want the recommendation to be more precise, fill in your size details.
                And know what, it only takes 2 minutes.
              </div>
            </div>
            {referrer === 'si' ? <div className={Styles.btn} onClick={goBack}>CLOSE</div> :
              <div className={Styles.btn} onClick={getStarted}>GET STARTED NOW</div>}
          </div>
      }
      </div>
    );
  }
}

export default FitAssistDetails;
