import React, { PropTypes } from 'react';
import styles from './halfcard.css';

function decode(escapedHTML) {
  return escapedHTML.replace(/&lt;/g, '<').replace(/&gt;/g, '>');
}

const EscapedDiv = ({ className, content }) => (
  <div
    className={className}
    dangerouslySetInnerHTML={{ __html: decode(content) }} />
);
EscapedDiv.propTypes = {
  className: PropTypes.string,
  content: PropTypes.string
};

class HalfCardContainer extends React.Component {

  constructor(props) {
    super(props);
    this.getOpaqueStyles = this.getOpaqueStyles.bind(this);
    this.getContentStyles = this.getContentStyles.bind(this);
    this.getCloseIconStyle = this.getCloseIconStyle.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.getHeader = this.getHeader.bind(this);
    this.getSubtitledHeader = this.getSubtitledHeader.bind(this);
    this.getContainerHeight = this.getContainerHeight.bind(this);
    this.getContainerWidth = this.getContainerWidth.bind(this);
    this.focusCard = this.focusCard.bind(this);
    this.blurCard = this.blurCard.bind(this);
  }

  focusCard() {
    if (this.cardOverlay) {
      this.cardOverlay.focus();
    }
  }

  blurCard() {
    if (this.cardOverlay) {
      this.cardOverlay.blur();
    }
  }

  componentDidMount() {
    //window.addEventListener("keydown", this.onKeyPress);
    if (this.props.on === true) {
      this.focusCard();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.on === true) {
      this.focusCard();
    } else {
      this.blurCard();
    }
  }


  componentWillUnmount() {
    this.blurCard();
  }


  onKeyPress = event => {
    if (event.key === 'Escape') {
      this.props.onToggle(!this.props.on);
    }
  }

  getOpaqueStyles() {
    return this.props.on ? { display: 'block' } : { display: 'none' };
  }

  getContentStyles() {
    return this.props.on ? { right: '0' } : { right: '-34%' };
  }
  getCloseIconStyle() {
    return this.props.closeOnLeft ? styles.closeRightCard : styles.closeLeftCard;
  }

  getContainerWidth() {
    return this.props.width === 'wide' ? styles.wideLeftCard : styles.narrowLeftCard;
  }

  getContainerHeight() {
    return this.props.footer ? styles.footerGap : styles.noFooterGap;
  }

  getHeader() {
    return (
      <div className={styles.paddedHeader}>
        <div className={styles.actualHeader}>{this.props.title}</div>
        <div
          className={this.getCloseIconStyle()}
          onClick={this.toggleCard}>
          <span className={styles.removeCharacter}>✕</span>
        </div>
      </div>
    );
  }

  getSubtitledHeader() {
    return (
      <div className={styles.subtitledHeader}>
        <div className={styles.gridHeaderLeft} onClick={this.toggleCard}>✕</div>
        <div className={styles.gridHeader}>
          <div className={styles.gridHeaderTitle}>{this.props.title}</div>
          <div className={styles.gridHeaderSubTitle}>{this.props.subTitle}</div>
        </div>
      </div>
    );
  }

  toggleCard = e => {
    e.stopPropagation();
    this.props.onToggle(!this.props.on);
  }

  render() {
    const { className } = this.props;
    const on = this.props.on;
    return (
      <div className={styles.leftCardContainer}>
        <div
          className={styles.leftCardOverlay}
          onClick={this.toggleCard}
          style={this.getOpaqueStyles()} />
        <div
          className={`${styles.leftCard} ${className} ${this.getContainerWidth()}`}
          style={this.getContentStyles()} >
          <div
            className={`${styles.actualContent} ${this.getContainerHeight()}`}
            tabIndex="0"
            ref={divRef => {
              this.cardOverlay = divRef;
            }}
            onKeyDown={this.onKeyPress} >
            {this.props.subTitle ? this.getSubtitledHeader() : this.getHeader()}
            <div className={styles.paddedContent}>
              {this.props.render({
                on,
                toggleCard: this.toggleCard
              })}
            </div>
          </div>
          {this.props.footer && <EscapedDiv className={styles.footer} content={this.props.footer} />}
        </div>
      </div>
    );
  }
}

HalfCardContainer.propTypes = {
  className: PropTypes.string,
  onToggle: PropTypes.func,
  render: PropTypes.func.isRequired,
  on: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  closeOnLeft: PropTypes.bool,
  titleCenter: PropTypes.bool,
  width: PropTypes.string,
  footer: PropTypes.string
};

export default HalfCardContainer;
