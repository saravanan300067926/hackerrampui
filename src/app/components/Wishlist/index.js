import React from 'react';
import styles from './index.css';
import ItemCard from './ItemCard';
import WishlistNotAvailable from './WishlistNotAvailable';
import WishlistEmpty from './WishlistEmpty';
import SlotDetails from './SlotDetails';
import WishlistLogin from './WishlistLogin';
import { getAllWishlistedItems } from './utils/actions';
import { Helper } from './utils/helper';
import InfiniteScroll from 'react-infinite-scroller';
import findIndex from 'lodash/findIndex';
import differenceBy from 'lodash/differenceBy';
import Notify from '../Notify';
import at from 'v-at';
import { isBrowser } from '../../utils';
import getLoyalty from '../../utils/loyalty';
import get from 'lodash/get';
import HalfCardContainer from '../HalfCard';
import Product from '../Search/Results/Product';
import Image from '../Search/Results/Product/Image';
import { getProductImageUrl } from './utils/imgUtils';
import { securify } from '../../utils/securify';
import AdmissionControl from '../AdmissionControl';
import { isShowSlotPopup, getSlots } from '../../utils/slotUtils';
const secure = securify();

class Wishlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {
        count: 0,
        value: [
        ],
        currentCount: 0,
        pageSize: 20,
        deListedItems: []
      },
      requestToItemsMade: false,
      hasMore: true,
      similarItems: { showSimilarItems: false, itemId: '' },
      sizeOptions: { showSizeOptions: false, itemId: '' },
      showError: false,
      busy: false,
      isLoggedIn: {
        value: Helper.isLoggedIn(),
        message: 'Login to view items in your wishlist.'
      },
      styleSkuMap: null,
      isWishlistEnabled: true,
      similarProducts: null,
      enableProductScroller: true,
      allSimilarItems: [],
      showSlotPopup: false
    };

    this.onShowSimilarClick = this.onShowSimilarClick.bind(this);
    this.showSizeOptions = this.showSizeOptions.bind(this);
    this.removeWishlistItem = this.removeWishlistItem.bind(this);
    this.setBusy = this.setBusy.bind(this);
    this.notify = this.notify.bind(this);
    this.onToggle = this.onToggle.bind(this);
    this.mapSimilarItemstoProduct = this.mapSimilarItemstoProduct.bind(this);
    this.hideSaleSlot = this.hideSaleSlot.bind(this);
  }

  componentWillMount() {
    fireEvents('WISHLIST', 6);
    this.getAllWishlistItems();
  }

  componentDidMount() {
    Helper.setInitialScrollTop();
    if (Helper.isApp() && document.getElementById('web-footerMount')) {
      document.getElementById('web-footerMount').style.display = 'none';
    }

    this.setState({
      isWishlistEnabled: Helper.isWishlistEnabled()
    });


    if (at(window, 'Madalytics') && typeof window.Madalytics.send === 'function') {
      const uidx = get(window, '__myx_session__.login', '');
      window.Madalytics.send('ScreenLoad', {
        type: 'Collection/Wishlist',
        url: window.location.href,
        variant: 'web',
        name: `Collection/me/${uidx}/wishlist`,
        'data_set': {
          'entity_id': 'wishlist',
          'entity_name': 'Wishlist',
          'entity_type': 'collection',
          'entity_optional_attributes': {}
        }
      });
    }

    document.addEventListener('visibilitychange', () => {
      if (document.hidden !== undefined) {
        this.setState({ enableProductScroller: !document.hidden });
      }
    }, false);
  }

  onShowSimilarClick(value, itemId, allSimilarItems) {
    this.setValue('similarItems', { showSimilarItems: value, itemId });
    this.setState({ allSimilarItems });
  }

  onToggle() {
    document.getElementsByTagName('body')[0].style.overflow = 'auto';
    this.setValue('similarItems', { showSimilarItems: false });
    this.state.allSimilarItems = [];
  }

  setBusy(busy) {
    this.setState({ busy });
  }

  getAllWishlistItems() {
    if (!this.state.busy && this.state.hasMore && !this.state.requestToItemsMade) {
      this.setState({ requestToItemsMade: true });
      let offset = get(this.state, 'items.currentCount') || 0;
      offset++;
      const options = {
        offset,
        page: Math.floor((this.state.items.currentCount + this.state.items.deListedItems.length) / this.state.items.pageSize),
        pageSize: this.state.items.pageSize
      };
      getAllWishlistedItems(options, this.getItemsSuccess.bind(this), this.getItemsError.bind(this));
    }
  }

  getItemsSuccess(items, totalItemsCount, styleSkuMap, pageStyleIds) {
    if (totalItemsCount <= 0) {
      this.setValue('items', { count: 0 });
    } else {
      items = items || [];
      // The below if block is to handle changes from other browsers or apis
      if (this.state.items.count && this.state.items.count !== totalItemsCount) {
        this.reInitData();
        this.getAllWishlistItems();
        return;
      }
      this.setValue('items', { count: totalItemsCount });
      this.setState({ styleSkuMap });
      let newItems = [];
      try {
        newItems = differenceBy(items, this.state.items.value, 'id');
      } catch (ignore) {
        newItems = [];
        // Ignoring Client Side Error
      }
      pageStyleIds = pageStyleIds || [];
      this.handleDelistedItems(items, pageStyleIds);
      const allItems = this.state.items.value.concat(newItems);
      if (allItems && totalItemsCount > 0) {
        this.setValue('items',
          {
            count: totalItemsCount,
            currentCount: allItems.length,
            value: allItems
          }
        );
      }
      const currentFetchedItemsCount = this.state.items.currentCount + this.state.items.deListedItems.length;
      if (currentFetchedItemsCount < totalItemsCount) {
        // more items to be fetched
        this.setState({ hasMore: true });
      } else if (currentFetchedItemsCount > totalItemsCount) {
        // some items already shown may have been deleted. so reload the items
        this.reInitData();
        this.getAllWishlistItems();
      } else if (currentFetchedItemsCount === totalItemsCount) {
        if (this.state.items.deListedItems.length > 0) {
          this.setValue('items',
            {
              count: totalItemsCount - this.state.items.deListedItems.length
            }
          );
        }
        this.setState({ hasMore: false });
      }
    }
    this.setState({ requestToItemsMade: false });
  }

  getItemsError(statusCode) {
    this.setState({ requestToItemsMade: true });
    this.setState({ showError: true });
    if (statusCode === '403') {
      this.setValue('isLoggedIn', { value: false });
    }
  }

  setValue(field, _field) {
    this.setState({ [field]: { ...this.state[field], ..._field } });
  }

  getStylePrefix() {
    if (Helper.isApp()) {
      return `${styles.mobile} ${styles.app}`;
    } else if (Helper.isMobile()) {
      return styles.mobile;
    }
    return styles.desktop;
  }

  getPreLaunchAttr = (systemAttributes = []) => systemAttributes.filter(
    ({ systemAttributeValueEntry: { systemAttributeEntry } }) => systemAttributeEntry.attributeName === 'Enable Buy Button'
  );

  getItemCards() {
    const isMobile = Helper.isMobile();
    const items = [];
    const len = at(this.state, 'items.value.length');

    for (let i = 0; i < len; i++) {
      const item = this.state.items.value[i];
      const sizeInfo = {
        availableSizes: item.sizes.split(','),
        styleSkuMap: this.state.styleSkuMap
      };
      const loyalty = getLoyalty(item, 'wishlist');
      let pdpUrl = `${item.id}`;
      if (item.selectedSku) {
        const preSelectedSeller = item.selectedSellerPartnerId;
        pdpUrl = preSelectedSeller ? `${pdpUrl}?skuId=${item.selectedSku}&sellerPartnerId=${preSelectedSeller}` : `${pdpUrl}?skuId=${item.selectedSku}`;
      }
      const imageUrl = item.searchImage || '';
      const prelaunchAttr = (item.systemAttributes && this.getPreLaunchAttr(item.systemAttributes)) || [];
      const isPreLaunch = at(prelaunchAttr[0], 'systemAttributeValueEntry.valueName') === 'No';
      items.push(
        <ItemCard
          key={`item${i}`}
          id={`item${i}`}
          name={item.name}
          actualPrice={item.mrp}
          image={secure(imageUrl)}
          pdpUrl={pdpUrl}
          isMobile={isMobile}
          isOutOfStock={this.isOutOfStock(item)}
          similarItems={at(this.state, 'similarItems.showSimilarItems')}
          similarClickHandler={this.onShowSimilarClick}
          sizeInfo={sizeInfo}
          styleId={item.id}
          advanceOrderType={item.advanceOrderType}
          prelaunchAttr={prelaunchAttr}
          isPreLaunch={isPreLaunch}
          sizeOptions={this.state.sizeOptions}
          sizeOptionsHandler={this.showSizeOptions}
          removeItemHandler={this.removeWishlistItem}
          busy={this.state.busy}
          selecetedSku={item.selectedSku}
          selectedSellerPartnerId={item.selectedSellerPartnerId}
          setBusy={this.setBusy}
          notifyHandler={this.notify}
          loyalty={loyalty}
          inventoryInfo={item.inventoryInfo} />
      );
    }
    const size = at(this.state, 'items.pageSize');

    for (let i = 0; i < size; i++) {
      items.push(
        <div key={`items${len + i}`} className={styles.dummyData} />
      );
    }
    return items;
  }

  hideSaleSlot() {
    this.setState({
      showSlotPopup: false
    });
    document.body.style.overflow = 'auto';
  }

  mapSimilarItemstoProduct(similar) {
    const similarImage = getProductImageUrl(similar.defaultImage, {
      width: 210,
      height: 280,
      q: 90
    });
    const { mrp, discounted, discount = null } = similar.price || {};
    return {
      url: similar.landingPageUrl,
      brand: similar.brand ? similar.brand.name : '',
      title: similar.info,
      styleId: similar.id,
      image: similarImage,
      srcSetImg: similarImage,
      searchImage: similarImage,
      additionalInfo: similar.info,
      originalPrice: mrp,
      discountedPrice: discounted,
      discount,
      imageHeight: 230,
      discountPercentage: discount ? discount.label : '',
      loyalty: getLoyalty(similar, 'wishlist') // Page type is wishlist because similar items response is simialr to Wishlist
    };
  }

  handleDelistedItems(newItems, pageStyleIds) {
    const deListedItems = [...this.state.items.deListedItems];
    for (let i = 0; i < pageStyleIds.length; i++) {
      const pageStyleId = pageStyleIds[i];
      const elementIndex = findIndex(newItems, (item) => item.styleid === pageStyleId);
      if (elementIndex === -1 && deListedItems.indexOf(pageStyleId) === -1) {
        deListedItems.push(pageStyleId);
      }
    }
    this.setValue('items', { deListedItems });
  }

  reInitData() {
    this.setState({
      items: {
        count: 0,
        value: [
        ],
        currentCount: 0,
        pageSize: 6,
        deListedItems: []
      },
      requestToItemsMade: false,
      hasMore: true,
      similarItems: { showSimilarItems: false },
      sizeOptions: { showSizeOptions: false },
      showError: false,
      busy: false,
      styleSkuMap: null,
      allSimilarItems: []
    });
  }

  showSizeOptions(value, itemId) {
    this.setValue('sizeOptions', { showSizeOptions: value, itemId });
  }

  removeWishlistItem(styleId) {
    const removedList = this.state.items.value.filter((item) => item.id !== styleId);
    this.setValue('items', { value: removedList, count: this.state.items.count - 1, currentCount: this.state.items.currentCount - 1 });
  }

  isOutOfStock(item) {
    if (item.sizes) {
      return false;
    }
    return true;
  }

  notify(type, showLoginPage, message, thumbnailUrl, action, actionHandler) {
    if (type === 'error') {
      this.refs.notify.error(message);
      if (showLoginPage) {
        this.setValue('isLoggedIn', { value: false, message: 'Invalid session. Login to continue.' });
      }
    } else {
      if (action && actionHandler) {
        this.refs.notify.info({
          message,
          thumbnail: thumbnailUrl,
          position: 'right',
          action: {
            name: action,
            handler: actionHandler
          }
        });
      } else {
        this.refs.notify.info({
          message,
          thumbnail: thumbnailUrl,
          position: 'right'
        });
      }
      if (action === 'VIEW BAG') {
        this.setState({
          showSlotPopup: isShowSlotPopup()
        });
      }
    }
  }

  render() {
    const isMobile = Helper.isMobile();
    if (!this.state.isWishlistEnabled) {
      return (
        <WishlistNotAvailable isMobile={isMobile} />
      );
    }


    const isLoggedIn = isBrowser() ? this.state.isLoggedIn.value : at(this.props, 'location.dehydratedState.isLoggedIn');
    if (!isLoggedIn) {
      return (
        <WishlistLogin isMobile={isMobile} message={this.state.isLoggedIn.message} />
      );
    }


    if (this.state.items.count === 0 && !this.state.requestToItemsMade && !this.state.showError) {
      return (
        <div>
          <SlotDetails />
          <WishlistEmpty isMobile={isMobile} />
          <AdmissionControl
            showSlotPopup={this.state.showSlotPopup}
            hideSaleSlot={this.hideSaleSlot}
            slots={getSlots(this.state.showSlotPopup)} />
        </div>
      );
    }

    const noItems = 'https://constant.myntassets.com/web/assets/img/6fd5fff9-0f04-403c-9006-a3de8d484d8b1540466020183-bg-art-3x.png';
    const loaderImage = 'https://constant.myntassets.com/web/assets/img/11480423660550-loader.gif';
    const loader = <img src={loaderImage} className={styles.loader} alt="Loading..." />;
    return (
      <div style={{ padding: "0 45px" }} className={this.state.busy ? styles.showModal : styles.hideModal}>
        <SlotDetails />
        <div className={this.getStylePrefix()}>
          <div>
            <div className={styles.headingDiv}>
              <span className={styles.heading}>My Wishlist </span>
              <span className={styles.count}>{this.state.items.count} {(this.state.items.count > 1) ? 'items' : 'item'}</span>
            </div>
            <InfiniteScroll
              pageStart={0}
              threshold={100}
              loadMore={() => { this.getAllWishlistItems(); }}
              hasMore={this.state.hasMore && !this.state.showError && !this.state.busy}
              loader={loader}>
              <ul className={styles.wishListContainer}>
                {this.getItemCards()}
              </ul>
            </InfiniteScroll>
          </div>
          {<HalfCardContainer
            on={this.state.similarItems.showSimilarItems !== false}
            onToggle={this.onToggle}
            title={'Similar Products'}
            render={() => {
              if (this.state.allSimilarItems && this.state.similarItems.showSimilarItems) {
                return (<div className={styles.similarItemContainer}>
                  <ul className={`${styles.base} ${styles.similarGrid}`}>
                    {this.state.allSimilarItems.length > 0 ? this.state.allSimilarItems.map((item, index) =>
                      (<Product
                        {...this.mapSimilarItemstoProduct(item)}
                        key={`similar-${index}`}
                        isHoverDisabled={true}
                        isSimilarProduct={true}
                        isProductScrollerEnabled={false}
                        index={index} />)
                    ) : <li className={styles.noSimilarContainer}>
                        <img
                          height={370}
                          src={noItems} />
                        <div className={styles.noSimilartext}> No Similar Items </div>
                      </li>}</ul>
                </div>);
              }
              return null;
            }} />}
          <div className={this.state.showError ? styles.errorDiv : styles.hide}>{Helper.getDefaultErrorMessage()}</div>
          <img
            className={this.state.busy ? `${styles.loadingImage} ${styles.show}` : `${styles.loadingImage} ${styles.hide}`}
            src={loaderImage} />
        </div>
        <Notify ref="notify" />
        <AdmissionControl
          showSlotPopup={this.state.showSlotPopup}
          hideSaleSlot={this.hideSaleSlot}
          slots={getSlots(this.state.showSlotPopup)} />
      </div>
    );
  }
}

export default Wishlist;
