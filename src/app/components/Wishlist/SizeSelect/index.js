import React from 'react';
import styles from './sizeselect.css';
import { Helper } from '../utils/helper';
import Modal from '../../Utils/Modal/Modal.js';
import get from "lodash/get";
import SellersList from '../sellersList';
import PriceInfo from '../../Pdp/PriceInfo';


class SizeSelect extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedSize: props.selectedSize || '',
      showSizes: this.props.showSizes,
      selectedSellerPartnerId: get(props, 'bestSeller.sellerPartnerId'),
      showSellerList: false
    };
    this.changeSellerHandler = this.changeSellerHandler.bind(this);
    this.onDone = this.onDone.bind(this);
    this.onMoreSellerClick = this.onMoreSellerClick.bind(this);
    this.getProductInfo = this.getProductInfo.bind(this);
    this.getSizeItems = this.getSizeItems.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState({ selectedSize: props.selectedSize || '', showSizes: props.showSizes });
    if (props.selectedSize !== this.state.selectedSize) {
      this.setState({
        showSellerList: false
      })
    }
  }

  onSizeSelectionDone() {
    if (this.state.selectedSize) {
      this.closeSizeSelectDialog();
      this.props.sizeSelectionDoneHandler(this.state.selectedSize);
    }
  }

  getSizeItems() {
    let commonPrice = true;
    let lastPrice;
    const sizeOptions = this.props.inventoryInfo || [];
    const selectedSellerPartnerId = this.props.selectedSellerPartnerId;
    const selectedSkuId = Helper.getSkuIdForSize(this.state.selectedSize, this.props.inventoryInfo);
    const defaultSku = get(this.props, "selecetedSku");
    sizeOptions.forEach((size, index) => {
      const bestSeller = Helper.getBestSeller(
        size.sellersData,
        selectedSellerPartnerId,
        size.skuId,
        defaultSku
      );
      size.available = get(bestSeller, "availableCount") > 0;
      size.discountedPrice = get(bestSeller, "discountedPrice") || this.props.mrp;
      commonPrice =
        commonPrice && size.available && index > 0
          ? lastPrice === size.discountedPrice
          : commonPrice;
      if (commonPrice || index === 0) {
        lastPrice = size.discountedPrice;
      }
    });
    return sizeOptions.map((size, index) => {
      let buttonClass = `${styles.sizeButton}`;
      if (!commonPrice) {
        buttonClass = `${styles.big} ${buttonClass}`;
      }
  
      if (!size.available) {
        buttonClass = `${buttonClass} ${styles.disabled}`;
      }
  
      if (size.skuId === selectedSkuId) {
        buttonClass = `${buttonClass} ${styles.selected}`
      }
  
      return (
        <button
          key={`sizeDiv${index}`}
          className={buttonClass}
          value={size.label}
          onClick={() => { this.handleSizeSelection(size.skuId); }}>
          <b>{size.label}</b>
          {!commonPrice &&
          size.available && (
            <div className={styles.price}>
              Rs.{size.discountedPrice}
            </div>
          )}
        </button>
      );
    });
  }

  getProductInfo() {
    const url = this.props.pdpUrl;
    const { name='Nike', productInfo='', productImageURL=url, bestSeller, mrp } = this.props;
    const discountedPrice = get(bestSeller, 'discountedPrice', '');
    const discountLabel = get(bestSeller, "discountDisplayLabel");
    return (
      <div className={styles.productInfo}>
        <img
          src={productImageURL} />
        <div className={styles.info}>
          <div style={{ fontWeight: '500'}}>{productInfo}</div>
          <div className={styles.name}>{name}</div> 
          <PriceInfo
            discountData={{ 'label': discountLabel}}
            mrp={mrp}
            discountedPrice={discountedPrice} />
        </div>
      </div>
    );
  }

  changeSellerHandler(e) {
      this.setState({
        selectedSellerPartnerId: Number(e.target.value)
      })
  }

  onMoreSellerClick() {
    this.setState({
      showSellerList: !this.state.showSellerList
    })
  }

  onDone() {
    this.props.moveToBag(this.state.selectedSize, this.state.selectedSellerPartnerId );
  }

  getInCardSizeSelectDialog() {

    const { bestSeller } = this.props;
    const selectedSizeData = this.state.selectedSize ? (
      get(this.props, "inventoryInfo") || []
    ).find(size => size.label === this.state.selectedSize) : get(this.props, "inventoryInfo.0");
    const sizeSellers = (get(selectedSizeData, "sellersData") || []).filter(
      seller => seller.availableCount
    );
    const bestPrice = Math.min(
      ...sizeSellers.map(seller => seller.discountedPrice)
    );
    const moreSellersMessage = `${sizeSellers.length - 1} more seller${
      sizeSellers.length > 2 ? "s" : ""
    } available `;
    const sellerName = get(bestSeller, 'name', '');
    const discountedPrice = get(bestSeller, 'discountedPrice', '');
    return this.state.showSizes ? (
      <Modal onClose={this.props.sizeOptionsCloseHandler}>
        <div
          className={this.state.showSizes ? `${styles.sizeDisplayDiv} ${styles.showSizeDisplayDiv}` : styles.sizeDisplayDiv}>
            {this.getProductInfo()}
          <div className={styles.sizeDisplayHeader}>
            <span className={styles.sizeSelectLabel}>Select Size</span>
            <span className={`myntraweb-sprite ${styles.sizeDisplayRemoveMark}`} onClick={() => { this.props.sizeOptionsCloseHandler(); }}></span>
          </div>
          <div className={styles.sizeButtonsContaier}>
            {this.getSizeItems()}
          </div>
          {this.state.selectedSize &&
            <div className={styles.sellerInfoContainer}>
              <div>
                Seller: <b>{sellerName}</b>
              </div>
              {sizeSellers.length > 1 && (
                <div className={styles.moreSellerContainer} onClick={this.onMoreSellerClick}>
                  <span
                    className={styles.sizeMoreSellers}
                    onClick={this.showSellerPopup}
                  >
                    {moreSellersMessage}
                  </span>
                  {bestPrice < discountedPrice ? (
                    <span>
                      from Rs.
                      {` ${bestPrice}`}
                    </span>
                  ) : null}
                  <span className={`${this.state.showSellerList ? styles.upArrow : styles.downArrow} myntraweb-sprite`} />
                </div>
              )}
            </div>
          }
          {this.state.showSellerList && this.state.selectedSize && sizeSellers.length > 1 &&
            <SellersList
              sizeSellers={sizeSellers}
              selectedSellerPartnerId={this.state.selectedSellerPartnerId}
              onSellerChange={this.changeSellerHandler}
              mrp={this.props.mrp} />
          }
          <div
            onClick={this.onDone}
            className={styles.done}>
            Done
          </div>
        </div>
      </Modal>
    ) : null;
  }

  getOutOfCardSizeSelectDialog() {
    if (this.state.showSizes) {
      Helper.disableScroll();
    }
    return (
      <div className={this.state.showSizes ? styles.sizeSelectOptionsContainerActive : styles.sizeSelectOptionsContainer}>
        <div className={styles.backdrop} onClick={() => { this.closeSizeSelectDialog(); }}></div>
        <div className={this.state.showSizes ? `${styles.sizeSelectDisplayDiv} ${styles.active}` : styles.sizeSelectDisplayDiv}>
          <div className={styles.heading}>Select Size</div>
          <div className={styles.sizeItems}>
            {this.getSizeItems()}
          </div>
        </div>
        <button
          className={this.state.showSizes ? `${styles.doneBtn} ${styles.active}` : styles.doneBtn}
          disabled={this.state.selectedSize === ''}
          onClick={(event) => { this.onSizeSelectionDone(event); }}>
          <div>DONE</div>
        </button>
      </div>
    );
  }

  getSizeSelectDialog() {
    if (this.showSizeOptionsInCard()) {
      return this.getInCardSizeSelectDialog();
    }
    return this.getOutOfCardSizeSelectDialog();
  }

  closeSizeSelectDialog() {
    this.props.sizeOptionsCloseHandler();
    Helper.enableScroll();
  }

  handleSizeSelection(skuId) {
    const size = Helper.getSizeForSkuId(skuId, this.props.inventoryInfo);
    this.setState({ selectedSize: size });
    if (this.props.newSizeSelectionHandler) {
      this.props.newSizeSelectionHandler(size);
    }
  }

  showSizeOptionsInCard() {
    if (Helper.isMobile()) {
      return Helper.isiPad();
    }
    return true;
  }

  render() {
    return this.getSizeSelectDialog();
  }
}

SizeSelect.propTypes = {
  sizes: React.PropTypes.arrayOf(React.PropTypes.string),
  selectedSize: React.PropTypes.string,
  sizeSelectionDoneHandler: React.PropTypes.func,
  sizeOptionsCloseHandler: React.PropTypes.func,
  showSizes: React.PropTypes.bool.isRequired,
  newSizeSelectionHandler: React.PropTypes.func,
  moveToBag: React.PropTypes.func,
  bestSeller: React.PropTypes.object,
  selectedSizeData: React.PropTypes.object,
  pdpUrl: React.PropTypes.string,
  name: React.PropTypes.string,
  mrp: React.PropTypes.number,
  inventoryInfo: React.PropTypes.array,
  defaultSku: React.PropTypes.number,
  selectedSellerPartnerId: React.PropTypes.number
};


export default SizeSelect;
