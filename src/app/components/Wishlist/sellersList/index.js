import React from "react";
import styles from './sellersList.css';
import find from "lodash/find";
import get from 'lodash/get';
import PriceInfo from '../../Pdp/PriceInfo';

const getSellerName = (allSellers, sellerPartnerId) => {
  return get(
    find(
      allSellers,
      seller => seller.sellerPartnerId === sellerPartnerId
    ),
    "sellerName"
  );
}

const SellersList = props => {
  const { sizeSellers, mrp, onSellerChange, allSellers=[], deliveryMessages, selectedSellerPartnerId } = props;
    return (
      <div className={styles.sellerList}>
        {
          sizeSellers.map((seller) => {
            const sellerName = seller.name ? seller.name : getSellerName(allSellers, seller.sellerPartnerId);
            return (
              <label className={`${styles.customRadio}`}>
                <input
                  className={`${styles.sellerInput}`}
                  type="radio" value="option1"
                  onChange={onSellerChange}
                  value={seller.sellerPartnerId}
                  checked={seller.sellerPartnerId === selectedSellerPartnerId} />
                <div className={styles.radioIndicator}></div>
                <div className={styles.sellerInfoContainer}>
                  <div className={styles.price}>{sellerName}</div>
                  {
                    <PriceInfo
                      discountData={{ 'label': seller.discountDisplayLabel}}
                      mrp={mrp}
                      discountedPrice={seller.discountedPrice} />
                  }
                  {deliveryMessages && <div className={styles.deliveryInfo}>{deliveryMessages[seller.sellerPartnerId]}</div>}
                </div>
              </label>
            )
          })
        }
      </div>
    );
}

export default SellersList;