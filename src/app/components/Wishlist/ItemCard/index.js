import React from 'react';
import styles from './itemcard.css';
import { Link } from 'react-router';
import { Helper } from '../utils/helper';
import SizeSelect from '../SizeSelect';
import Track from '../../../utils/track';
import Cookies from '../../../utils/cookies';
import ItemDetails from '../ItemDetails';
import { moveWishlistedItemtoBag, removeWishlistedItem, getSimilarItems } from '../utils/actions';
import bus from 'bus';
import at from 'v-at';
import get from "lodash/get";
import { setWStateChangedCookie } from '../../../utils/wishlistSummaryManager';
import priorityCheckout, {
  PRIORITY_CHECKOUT_MESSAGE,
  CART_FULL_ERROR_CODE,
  PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE
} from '../../../utils/priorityCheckoutUtils';
import { srcSet } from '../utils/imgUtils';
import find from 'lodash/find';
import OutsideClickDetector from '../../Utils/OutsideClickDetector';
import { getWebpUrl } from '../../../utils/getWebpUrl';


class ItemCard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showSimilarItems: false,
      allSimilarItems: { value: [] },
      showSizes: false,
      isBusy: false,
      availableSizes: this.props.sizeInfo.availableSizes,
      selectedSize: '',
      selectedSkuId: this.props.selecetedSku || '',
      moveToBagButton: { text: 'MOVE TO BAG' }
    };
    this.moveToBag = this.moveToBag.bind(this);
    this.closeSizeOptions = this.closeSizeOptions.bind(this);
    this.setSelectedSize = this.setSelectedSize.bind(this);
    this.getAllSimilarItemsSuccess = this.getAllSimilarItemsSuccess.bind(this);
    this.defatulSizeSelected = this.defatulSizeSelected.bind(this);
    this.openSizeOptions = this.openSizeOptions.bind(this);
  }

  componentDidMount() {
    const container = document.getElementsByClassName('index-wishListContainer')[0];
    const itemCard = document.getElementById(this.props.id);

    this.outsideClickDetector = new OutsideClickDetector(container, itemCard);
  }

  componentWillReceiveProps(props) {
    const showSizes = this.isShowSizes(props.sizeOptions);
    this.setState({
      showSizes,
      showSimilarItems: this.isShowSimilarItems(props.similarItems),
      availableSizes: props.sizeInfo.availableSizes,
      styleSkuMap: props.sizeInfo.styleSkuMap,
      selectedSize: showSizes ? this.state.selectedSize || this.defatulSizeSelected() : '',
      isBusy: props.busy
    });
  }

  getSelectedSizeFromMap() {
    if (this.state.styleSkuMap && this.state.styleSkuMap.hasOwnProperty(this.props.styleId)) {
      const selectedSkuId = this.state.styleSkuMap[this.props.styleId];
      const allSkuForSizes = this.props.allSkuForSizes || [];
      const skuForSelectedSize = allSkuForSizes.length > 0 && allSkuForSizes.filter((item) => item.indexOf(`${selectedSkuId}:`) !== -1);
      if (skuForSelectedSize && skuForSelectedSize.length === 1 && skuForSelectedSize[0]) {
        const skuSplitArray = skuForSelectedSize[0].split(':');
        if (skuSplitArray && skuSplitArray.length === 3) {
          return skuSplitArray[0];
        }
      }
    }
    return null;
  }

  setValue(field, _field) {
    this.setState({ [field]: { ...this.state[field], ..._field } });
  }

  getSrcSetImage() {
    let imgSrc = at(this.props, 'image', "");

    imgSrc = imgSrc.replace(
      'assets.myntassets.com',
      `assets.myntassets.com/w_200,c_limit,fl_progressive,dpr_2.0`
    );

    const webpImageSrc = getWebpUrl(imgSrc);

    return (
      <picture className="img-responsive">
        <source
          srcSet={webpImageSrc}
          type="image/webp" />
        <img
          className={this.props.isOutOfStock ? styles.outOfStockItemImage : styles.itemImage}
          src={imgSrc}
        />
      </picture>
    );
  }

  getImageUrl(imageUrl) {
    let srcSetImg = '';
    const imgSrc = imageUrl || at(this.props, 'image');

    if (imgSrc) {
      srcSetImg = srcSet(imgSrc);
    }

    return imgSrc || srcSetImg;
  }

  getItemCardStyle() {
    if (!this.showSizeOptionsInCard() && this.state.showSizes) {
      return styles.itemCardSelected;
    }
    return this.state.showSimilarItems ? styles.itemCardSelected : styles.itemCard;
  }

  getAllSimilarItems(item, showSimilarItems) {
    if (showSimilarItems) {
      this.props.setBusy(true);
      if (item && item.name && item.styleId) {
        const options = {
          name: item.name,
          styleId: item.styleId
        };
        document.getElementsByTagName('body')[0].style.overflow = 'hidden';
        return getSimilarItems(options, this.getAllSimilarItemsSuccess,
          this.getAllSimilarItemsFailure);
      }
    }
    return '';
  }

  getAllSimilarItemsSuccess(similarItems) {
    similarItems = similarItems || this.state.allSimilarItems.value || [];
    this.props.setBusy(false);
    if (!this.state.showSimilarItems) {
      this.props.similarClickHandler(!this.state.showSimilarItems, this.props.styleId, similarItems);
    }
    this.setValue('allSimilarItems', { value: similarItems });
    Track.event('ProductRecos', 'Load', `Wishlist | Similar | ${this.props.styleId}`, 'wishlistDetails');
  }

  getAllSimilarItemsFailure(similarItems) {
    this.props.similarClickHandler(!this.state.showSimilarItems, this.props.styleId, similarItems);
    this.setValue('allSimilarItems', { value: similarItems });
    Track.event('ProductRecos', 'Load', `Wishlist | Similar | ${this.props.styleId}`, 'wishlistDetails');
  }

  getSizeButtonStyle(size) {
    let sizeButtonStyle = `${styles.sizeButton}`;
    if (this.state.selectedSize && this.state.selectedSize === size) {
      sizeButtonStyle = `${styles.sizeButton} ${styles.sizeButtonSelected}`;
    }
    return sizeButtonStyle;
  }

  setSelectedSize(selectedSize) {
    this.setState({ 
      selectedSize,
      selectedSkuId: Helper.getSkuIdForSize(selectedSize, this.props.inventoryInfo)
    });
  }

  getItemCardActions() {
    if (at(this.props, 'isPreLaunch')) {
      return (
        <div className={styles.actionDiv}>
          <span className={`${styles.flex} ${styles.boldFont} ${styles.prelaunchTxt}`}>
         {at(this.props, 'prelaunchAttr.0.metaInfo', '')}
          </span>
        </div>
      );
    }
    if (this.props.isOutOfStock) {
      return (
        <div className={this.enableShowSimilar() ? styles.actionDiv : `${styles.actionDiv} ${styles.actionDivDisabled}`}>
          <Link
            to={'/'}
            className={`${styles.moveToBag} ${styles.boldFont} ${this.state.isBusy ? styles.actionBusy : ''}`}
            onClick={(event) => { this.showSimilar(event); }}>SHOW SIMILAR</Link>
        </div>);
    }
    return (
      <div className={this.enableMoveToBag() ? styles.actionDiv : `${styles.actionDiv} ${styles.actionDivDisabled}`}>
        <span className={`${styles.flex} ${styles.hiddenSpan}`}>
          <Link
            to={'/checkout/cart'}
            className={this.state.isBusy ? `${styles.moveToBag} ${styles.boldFont} ${styles.actionBusy}` :
              `${styles.moveToBag} ${styles.boldFont}`}
            onClick={(event) => {
              this.openSizeOptions(event);
            }}>
            {this.state.moveToBagButton.text}
          </Link>
          {priorityCheckout() && <span className={styles.eorsText}>{PRIORITY_CHECKOUT_MESSAGE}</span>}
        </span>
      </div>);
  }

  getSizeSelectDialog(bestSeller) {
    return (
      <SizeSelect
        inventoryInfo={this.props.inventoryInfo}
        bestSeller={bestSeller}
        sizes={this.state.availableSizes}
        selectedSize={this.state.selectedSize}
        sizeOptionsCloseHandler={this.closeSizeOptions}
        showSizes={this.state.showSizes}
        newSizeSelectionHandler={this.setSelectedSize}
        sizeSelectionDoneHandler={this.moveToBag}
        pdpUrl={this.getImageUrl()}
        name={this.props.name}
        moveToBag={this.moveToBag}
        defaultSku={this.props.selecetedSku}
        selectedSellerPartnerId={this.props.selectedSellerPartnerId}
        mrp={this.props.actualPrice} />
    );
  }

  getOutOfStockText() {
    if (this.props.isOutOfStock) {
      return (<div className={styles.outOfStockText}>OUT OF STOCK</div>);
    }
    return null;
  }

  defatulSizeSelected() {
    const selectedSkuId = this.props.selecetedSku;
    if (selectedSkuId) {
      const inventoryInfo = this.props.inventoryInfo || [];
      const selectedInventory = find(inventoryInfo, item => item.skuId === selectedSkuId);
      return at(selectedInventory, 'label') || '';
    }
    return '';
  }

  showSizeOptionsInCard() {
    if (Helper.isMobile()) {
      return Helper.isiPad();
    }
    return true;
  }

  isShowSizes(sizeOptions) {
    if (sizeOptions.showSizeOptions && sizeOptions.itemId === this.props.styleId) {
      return true;
    }
    return false;
  }

  isShowSimilarItems(similarItems) {
    if (similarItems.showSimilarItems && similarItems.itemId === this.props.styleId) {
      return true;
    }
    return false;
  }

  openSizeOptions(event) {
    if (!this.state.isBusy && this.enableMoveToBag()) {
      this.closeSimilarItems(true);
      Track.event('shopping', 'initiateATC', `Wishlist | ${this.props.styleId}`, 'wishlistDetails');
      this.props.sizeOptionsHandler(true, this.props.styleId);
    }
    event.preventDefault();
    event.stopPropagation();
    this.outsideClickDetector.attach(this.closeSizeOptions);
  }

  closeSizeOptions() {
    this.props.sizeOptionsHandler(false, '');
    this.outsideClickDetector.detach();
  }

  closeSimilarItems(handleStateUpdate) {
    const node = document.getElementById('showSimilarMainDiv');
    if (node) {
      node.parentNode.removeChild(node);
    }
    if (handleStateUpdate) {
      this.props.similarClickHandler(false, this.props.styleId);
    }
  }

  moveToBag(sizeSelected, selectedSellerPartnerId) {
    const isOneSize = this.state.availableSizes.length === 1;
    const oneSize = (isOneSize) ? this.state.availableSizes[0] : undefined;

    const selectedSize = sizeSelected || this.state.selectedSize || oneSize;

    if (selectedSize) {
      const moveToBagPostData = {
        skuId: Helper.getSkuIdForSize(selectedSize, this.props.inventoryInfo),
        styleId: this.props.styleId,
        sellerPartnerId: selectedSellerPartnerId
      };
      if (moveToBagPostData.skuId && moveToBagPostData.styleId) {
        this.setState({ selectedSkuId: moveToBagPostData.skuId });
        this.props.setBusy(true);
        moveWishlistedItemtoBag(moveToBagPostData,
          { 'X-CSRF-TOKEN': Helper.getXsrfToken() },
          this.moveToBagSuccess.bind(this),
          this.moveToBagFailure.bind(this)
        );
      } else {
        this.props.notifyHandler('error', false, Helper.getDefaultErrorMessage());
      }
    } else {
      this.props.notifyHandler('error', false, 'Size not selected.');
      this.closeSizeOptions();
    }
  }

  navigateToCart() {
    Helper.navigateTo('checkout/cart', true);
  }

  moveToBagSuccess(response, sellerPartnerId) {
    Track.event('shopping', 'addToCart', `Wishlist | ${this.props.styleId} | ${this.state.selectedSkuId} | ${this.state.selectedSize} | ${sellerPartnerId}`, 'wishlistDetails');
    this.props.setBusy(false);
    let notifyMessage = 'Item successfully added to bag';
    if (this.props.isMobile) {
      notifyMessage = 'Item added to bag';
    }
    this.props.notifyHandler('info', false, notifyMessage, this.getImageUrl(), 'VIEW BAG', this.navigateToCart.bind(this));
    this.closeSizeOptions();
    bus.emit('cart.addFromWishlist', at(response, 'body.cartCount'));
    this.invokeRemoveHandler();
    setWStateChangedCookie(true);
  }

  moveToBagFailure(statusCode, statusMessage) {
    this.props.setBusy(false);
    if (statusCode === '403' || statusCode === 403) {
      this.props.notifyHandler('error', true, 'Invalid session. Please login and try again.');
    } else {
      if (priorityCheckout() && statusCode === CART_FULL_ERROR_CODE) {
        statusMessage = PRIORITY_CHECKOUT_CART_FULL_ERROR_MESSAGE;
      }
      this.props.notifyHandler('error', false, statusMessage || Helper.getDefaultErrorMessage());
    }
    this.closeSizeOptions();
  }

  showSimilar(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.enableShowSimilar()) {
      if (!this.state.isBusy) {
        this.closeSimilarItems();
        this.closeSizeOptions();
        const showSimilarItems = !this.state.showSimilarItems;
        return this.state.allSimilarItems.value.length === 0 ?
          this.getAllSimilarItems({ name: this.props.name, styleId: this.props.styleId }, showSimilarItems) :
          this.getAllSimilarItemsSuccess();
      }
    }
    return '';
  }

  enableShowSimilar() {
    return !Helper.isShowSimilarDisabled();
  }

  removeItem() {
    if (!this.state.isBusy) {
      this.props.setBusy(true);
      this.closeSimilarItems(true);
      const removeData = {
        styleId: this.props.styleId
      };
      removeWishlistedItem(removeData,
        { 'X-CSRF-TOKEN': Helper.getXsrfToken() },
        this.removeItemSuccess.bind(this),
        this.removeItemFailure.bind(this));
    }
  }

  invokeRemoveHandler() {
    this.props.removeItemHandler(this.props.styleId);
  }

  removeItemSuccess() {
    Track.event('shopping', 'Remove', `Wishlist | ${this.props.styleId}`, 'wishlistDetails');
    setWStateChangedCookie(true);
    this.props.notifyHandler('info', false, 'Item removed from wishlist', this.getImageUrl());
    this.invokeRemoveHandler();
    this.props.setBusy(false);
  }

  removeItemFailure(statusCode, statusMessage) {
    this.props.setBusy(false);
    if (statusCode === '403' || statusCode === 403) {
      this.props.notifyHandler('error', true, 'Invalid session. Please login and try again.');
    } else {
      const message = statusMessage || Helper.getDefaultErrorMessage();
      this.props.notifyHandler('error', false, message);
    }
  }

  cookieExists(cname) {
    return Cookies.get(cname) || false;
  }

  enableMoveToBag() {
    if (Helper.inPriceRevealMode() && this.props.advanceOrderType !== 'preorder') {
      return this.cookieExists('stp') && !this.cookieExists('stb');
    }
    return true;
  }

  render() {
    const selectedSizeData = this.state.selectedSkuId ? (
      get(this.props, "inventoryInfo") || []
    ).find(size => size.skuId === this.state.selectedSkuId) : get(this.props, "inventoryInfo.0");
    const bestSeller = Helper.getBestSeller(
      get(selectedSizeData, "sellersData"),
      this.props.selectedSellerPartnerId,
      this.state.selectedSkuId,
      this.props.selecetedSku
    );
    const discountData = {
      label: get(bestSeller, "discountDisplayLabel")
    };
    const discountedPrice = get(bestSeller, "discountedPrice");

    const hasDiscount = discountedPrice && discountedPrice !== this.props.actualPrice;

    return (
      <div id={this.props.id} className={this.getItemCardStyle()}>
        <div className={styles.itemImageDiv}>
          <a
            href={`${Helper.getSecureRoot()}${this.props.pdpUrl}`}
            target="_blank">
            {
              this.props.advanceOrderType === 'preorder'
                ? <div className={`myntraweb-sprite ${styles.preOrderBadge}`}>PRE-ORDER</div>
                : null
            }
            {this.getSrcSetImage()}
          </a>
          <div
            className={this.state.isBusy ? `${styles.removeIcon} ${styles.actionBusy}` : styles.removeIcon}
            onClick={() => this.removeItem()}>
            <span className={`myntraweb-sprite ${styles.removeMark}`}></span>
          </div>
        </div>
        {this.getSizeSelectDialog(bestSeller)}
        <div className={styles.itemActions}>
          {this.getOutOfStockText()}
          <ItemDetails
            actualPrice={`Rs.${Helper.formatRupees(this.props.actualPrice)}`}
            discountedPrice={`Rs.${Helper.formatRupees(discountedPrice)}`}
            discountPercent={discountData.label}
            hasDiscount={hasDiscount}
            loyalty={this.props.loyalty}
            name={this.props.name}
            description={this.props.description} />
          {this.getItemCardActions()}
        </div>
      </div>
    );
  }
}

ItemCard.propTypes = {
  id: React.PropTypes.string.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string,
  image: React.PropTypes.string.isRequired,
  actualPrice: React.PropTypes.string,
  advanceOrderType: React.PropTypes.string,
  pdpUrl: React.PropTypes.string,
  styleId: React.PropTypes.number.isRequired,
  isMobile: React.PropTypes.bool.isRequired,
  isOutOfStock: React.PropTypes.bool.isRequired,
  similarClickHandler: React.PropTypes.func.isRequired,
  similarItems: React.PropTypes.bool.isRequired,
  sizeInfo: React.PropTypes.object.isRequired,
  sizeOptions: React.PropTypes.object.isRequired,
  sizeOptionsHandler: React.PropTypes.func.isRequired,
  removeItemHandler: React.PropTypes.func.isRequired,
  busy: React.PropTypes.bool.isRequired,
  allSkuForSizes: React.PropTypes.arrayOf(React.PropTypes.string),
  setBusy: React.PropTypes.func.isRequired,
  notifyHandler: React.PropTypes.func.isRequired,
  loyalty: React.PropTypes.object,
  inventoryInfo: React.PropTypes.array,
  selecetedSku: React.PropTypes.number,
  selectedSellerPartnerId: React.PropTypes.number
};

export default ItemCard;
