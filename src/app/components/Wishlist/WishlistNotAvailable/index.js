import React from 'react';
import styles from './wishlistNA.css';

const WishlistNotAvailable = (props) =>
  <div className={props.isMobile ? styles.mobile : styles.desktop}>
    <div className={styles.container}>
      <div className={styles.heading}>WISHLIST NOT AVAILABLE</div>
      <div className={styles.info}>Sorry, we are experiencing high volume of traffic now. Your wishlist will be back soon!</div>
      <div>
        <a href="/" className={styles.button}>CONTINUE SHOPPING</a>
      </div>
    </div>
  </div>;

WishlistNotAvailable.propTypes = {
  isMobile: React.PropTypes.bool.isRequired
};

export default WishlistNotAvailable;

