
import Client from '../../../services';
import at from 'v-at';

export const getAllWishlistedItems = (options, successCallback, errorCallback) => {
  const page = options.page;
  const offset = options.offset;
  const pageSize = options.pageSize || 5;
  const query = isNaN(page) || isNaN(pageSize) ? '' : `?offset=${offset}&size=${pageSize}`;
  
  Client.get(`web/wishlistapi/apify/getWishlist${query}`).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
    } else if(response && successCallback) {
      successCallback(at(response,'body.products'), at(response,'body.totalCount'), at(response,'body.data.styleSkuMap'), at(response,'body.data.pageStyleIds'));
    }
  });
}

export const getSimilarItems = (options, successCallback, errorCallback) => {
  if (options && options.name && options.styleId) {
    Client.get(`web/related/${options.styleId}`)
    .end((err, response) => {
      if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
        errorCallback(at(response.body, 'body.status.statusCode'), at(response, 'body.status.statusMessage') || err);
      }  else if(response && successCallback) {
        successCallback(at(response,'body.related.0.products'));
      }
    });
  } else {
    console.log('Required params (item name and style id) to get similar items not provided');
  }
}


export const moveWishlistedItemtoBag = (data, headers, successCallback, errorCallback) => {
  data = data || {};
  data.action = 'MOVE_TO_CART';
  Client.post(`web/wishlistapi/apify/moveToBag`, data, headers).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'statusCode') || at(response, 'body.status.statusCode') , at(response, 'body.error.message'));
    } else if(response && successCallback) {
      successCallback(response, data.sellerPartnerId);
    }
  });
}

export const removeWishlistedItem = (data, headers, successCallback, errorCallback) => {
  data = data || {};
  data.action = 'REMOVE_ITEM';
  Client.post(`web/wishlistapi/apify/delete`, data, headers).end((err, response) => {
    if((err || at(response, 'body.status.statusType') === 'ERROR') && errorCallback) {
      errorCallback(at(response, 'statusCode') || at(response, 'body.status.statusCode'), at(response, 'body.error.message'));
    } else if(response && successCallback) {
      successCallback(response);
    }
  });
}
