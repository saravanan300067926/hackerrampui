import at from 'v-at';
import { securify } from '../../../utils/securify';
const secure = securify();

function getProductImageUrl(imgObj = null, config = {}) {
    if (imgObj) {
      const secureSrc = at(imgObj, 'secureSrc');
      const imageFormula = secureSrc || at(imgObj, 'src')
      || `${at(imgObj, 'securedDomain')}${at(imgObj, 'resolutionFormula')}`;
      const url = imageFormula.replace('($width)', config.width).replace('($height)', config.height).replace('($qualityPercentage)', config.q);
      return secure(url);
    }
    return null;
  };

const widthRatios = ['1.5', '2', '2.5', '3'];

const srcSet = (src) => {
  if (src) {
    return widthRatios.map((ratio, index) =>
      `${src.replace('assets.myntassets.com', `assets.myntassets.com/dpr_${ratio}`)
        .replace('q_95', 'q_80')} ${ratio}x${index === widthRatios.length - 1 ? '' : ','}`
    ).join('');
  } return '';
};

export { getProductImageUrl, srcSet };