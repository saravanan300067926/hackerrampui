import at from 'v-at';
import kvpairs from '../../../utils/kvpairs';
import features from '../../../utils/features';
import { isBrowser } from  '../../../utils';
import get from "lodash/get";
import find from 'lodash/find';

export const Helper = {
  isMobile: function() {
    if (isBrowser()) {
      return (at(window, '__myx_deviceType__') === 'mobile' || at(window, '__myx_deviceData__.isMobile'));
    } return false;
  },
  isApp: function() {
    if (isBrowser()) {
      const deviceChannel = at(window, '__myx_deviceData__.deviceChannel') || '';
      return deviceChannel.toLowerCase() === 'mobile_app';
    } return false;
  },
  getRootUrl: function() {
    if (isBrowser()) {
      return at(window, '__myx_env__.hosts.root') || 'http://www.myntra.com/';
    } return false;
  },
  getSecureRoot: function() {
    if (isBrowser()) {
      return at(window, '__myx_env__.hosts.secureRoot') || 'https://www.myntra.com/';
    } return false;
  },
  isLoggedIn: function() {
    if (isBrowser()) {
      return at(window, '__myx_session__.isLoggedIn') || false;
    } return false;
  },
  getXsrfToken: function() {
    if (isBrowser()) {
      return at(window, '__myx_session__.USER_TOKEN') || '';
    } return false;
  },
  navigateTo: function(path, isSecure) {
    if (isBrowser()) {
      path = path || '';
      window.location.href = isSecure ? `${this.getSecureRoot()}${path}` : `${this.getRootUrl()}${path}`;
    }
  },
  getDefaultErrorMessage: function() {
    return 'Something went wrong. Please try again later.';
  },
  sleep: function(duration, callback) {
    if (isBrowser()) {
      window.setTimeout(callback, duration);
    }
  },
  scrollToBottom: function(location) {
    if (isBrowser() && location && document && document.body) {
        let targetDiv = (document.getElementById(location) && document.getElementById(location).parentNode);
        let lengthOffset = 410;
          const scrollTopValue = targetDiv && targetDiv.offsetTop && !isNaN(targetDiv.offsetTop) ? (targetDiv.offsetTop + lengthOffset) : null;
          if (scrollTopValue && scrollTopValue > document.body.scrollTop) {
            document.body.scrollTop = scrollTopValue - lengthOffset - 250;
          }
      }
  },
  scrollToTop: function(location, offset) {
    if (isBrowser() && location && document && document.body) {
        let targetDiv = document.querySelector(`#${location}`) ;
        let lengthOffset = 450;
        if(offset) {
          lengthOffset = offset;
        }
          const scrollTopValue = targetDiv && targetDiv.offsetTop && !isNaN(targetDiv.offsetTop) ? (targetDiv.offsetTop - lengthOffset) : null;
          if (scrollTopValue && scrollTopValue < document.body.scrollTop) {
            document.body.scrollTop = scrollTopValue;
          }
      }
  },
  setInitialScrollTop: function() {
    if (isBrowser()) {
      document.body.scrollTop = 0;
    }
  },
  formatRupees: function (number, round) {
      const _r = typeof round === 'undefined' ? true : round;
      let num = number < 0 ? 0.00 : number;
      num = (_r ? Math.round(num):parseFloat(num, 10).toFixed(2)).toString();
      let one = (num == 1) ? true : false,
          arr = [],
          digits,
          dp = '00',
          curr;
      if(!_r){
          let _dec = num.split('.');
           if(_dec.length > 1){
               num = _dec[0];
               dp = _dec[1];
           }
      }
      while (num) {
          digits = digits ? 2 : 3;
          arr.push(num.slice(-digits));
          num = num.slice(0, -digits);
      }
      curr = arr.reverse().join(',');
      return _r ? curr : curr+'.'+dp;
  },
  isWishlistEnabled: function() {
    return features('wishlist.enable') === 'true';
  },
  inPriceRevealMode: function() {
    let priceRevealData = kvpairs('hrdr.pricereveal.data');
    if (typeof priceRevealData === 'string') {
      try {
        priceRevealData = JSON.parse(priceRevealData);
        return priceRevealData.enable === 'true';
      } catch(e) {
        console.log('price reveal data is not a proper json object');
      }
    } else if (typeof priceRevealData === 'object') {
      return priceRevealData.enable === 'true';
    }
    return false;
  },
  isShowSimilarDisabled: function() {
    return features('wishlist.oos.showsimilar.disable') === 'true';
  },
  isiPad: function() {
    if (isBrowser()) {
      const iPadUA = window && window.navigator ? window.navigator.userAgent : null;
      return iPadUA && iPadUA.match(/iPad/i) != null;
    } return false;
  },
  enableScroll: function() {
    if (isBrowser()) {
      document.body.style.overflowY = 'auto';
      document.body.style.position = 'static';
    }
  },
  disableScroll: function() {
    if (isBrowser()) {
      document.body.style.overflowY = 'hidden';
    }
  },
  getBestSeller: function(sellersData = [], selectedSellerPartnerId, skuId, defaultSku) {
    let bestSeller;
  if (selectedSellerPartnerId && defaultSku && skuId === defaultSku) {
    bestSeller = sellersData.find(
      seller =>
        seller.availableCount > 0 &&
        seller.sellerPartnerId === selectedSellerPartnerId
    );
  }
  if (!bestSeller) {
    bestSeller =
      sellersData.find(seller => seller.availableCount > 0) ||
      get(sellersData, "0");
  }
  return bestSeller;
  },
  getSkuIdForSize: function(selectedSize, inventory) {
    const inventoryInfoForSelectedSize = find(inventory, item => (
      item.label === selectedSize
    ));
    return at(inventoryInfoForSelectedSize, 'skuId');
  },
  getSizeForSkuId: function(skuid, inventory) {
    const inventoryInfoForSelectedSize = find(inventory, item => (
      item.skuId === skuid
    ));
    return at(inventoryInfoForSelectedSize, 'label');
  }
}
