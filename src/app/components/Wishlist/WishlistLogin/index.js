import React from 'react';
import styles from './wishlistLogin.css';
import { isBrowser } from '../../../utils';

const WishlistLogin = (props) =>
  <div className={props.isMobile ? styles.mobile : styles.desktop}>
    <div className={styles.container}>
      <div className={styles.heading}>PLEASE LOG IN</div>
      <div className={styles.info}>{props.message || 'Login to view items in your wishlist.'}</div>
      <div className={`myntraweb-sprite ${styles.icon}`}></div>
      <div>
        <a href={`/login?referer=${isBrowser() ? location.href : ''}`} className={styles.button}>LOGIN</a>
      </div>
    </div>
  </div>;

WishlistLogin.propTypes = {
  isMobile: React.PropTypes.bool.isRequired,
  message: React.PropTypes.string.isRequired
};

export default WishlistLogin;
