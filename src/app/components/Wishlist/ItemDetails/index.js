import React from 'react';
import styles from './itemdetails.css';
import commonStyles from '../../../resources/coin.css';

const Loyalty = props => (
  <div className={styles.loyalty}>
    {`Or pay Rs. ${props.discount} + `}
    <div className={commonStyles.coin} />
    {` ${props.points} Points`}
  </div>
);

Loyalty.propTypes = {
  discount: React.PropTypes.number,
  points: React.PropTypes.number
};

const ItemDetails = (props) => (
  <div className={styles.itemDetails}>
    <p className={styles.itemDetailsLabel}>{props.name}</p>
    <p className={styles.itemDetailsDescription}>{props.description}</p>
    <div className={styles.itemPricing}>
      <span className={props.hasDiscount ? styles.boldFont : styles.hide} > {props.discountedPrice} </span>
      <span className={props.hasDiscount ? styles.strike : styles.boldFont} > {props.actualPrice} </span>
      <span className={props.hasDiscount ? styles.discountPercent : styles.hide} > {props.discountPercent} </span>
    </div>
    {props.loyalty.enabled ? <Loyalty {...props.loyalty} /> : null}
  </div>
);

ItemDetails.propTypes = {
  actualPrice: React.PropTypes.string,
  discountedPrice: React.PropTypes.string,
  discountPercent: React.PropTypes.string,
  hasDiscount: React.PropTypes.bool.isRequired,
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  loyalty: React.PropTypes.object
};

export default ItemDetails;
