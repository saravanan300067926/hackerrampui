import React from 'react';
import styles from './slotdetails.css';
import { showEarlySlotInfo, getSlots, getTimeRange } from '../../../utils/slotUtils';
import at from 'v-at';
import AdmissionControl from '../../AdmissionControl';

class SlotDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSlotPopup: false
    };
    this.hideSaleSlot = this.hideSaleSlot.bind(this);
  }

  getSelectedSlotDateTime() {
    let slotDateTime = '';
    const slots = getSlots(true) || {};
    const slotSelected = at(slots, 'sl');
    if (slotSelected) {
      const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
      ];
      const startDate = new Date(slotSelected.st);
      const endDate = new Date(slotSelected.et);
      slotDateTime = startDate && endDate ? `${monthNames[startDate.getMonth()]} ${startDate.getDate()}  |  ${getTimeRange(startDate, endDate)}` : '';
    }
    return slotDateTime;
  }

  getChangeLink() {
    return (
      <div className={styles.changeLink} onClick={() => this.handleChangeClick()}>
        CHANGE
      </div>
    );
  }

  handleChangeClick() {
    this.setState({ showSlotPopup: true });
  }

  hideSaleSlot() {
    this.setState({
      showSlotPopup: false
    });
    document.body.style.overflow = 'auto';
    document.body.style.position = 'static';
  }

  render() {
    if (showEarlySlotInfo()) {
      return (
        <div className={styles.container}>
          <div className={styles.subcontainer}>
            <div className={styles.text}>
              Early Slot: <span>{this.getSelectedSlotDateTime()}</span>
              {this.getChangeLink()}
            </div>
          </div>
          {this.state.showSlotPopup && <AdmissionControl
            showSlotChangeScreenOnly={true}
            showSlotPopup={this.state.showSlotPopup}
            hideSaleSlot={this.hideSaleSlot}
            slots={getSlots(this.state.showSlotPopup)} />}
        </div>
      );
    }
    return null;
  }
}

SlotDetails.propTypes = {
  highlight: React.PropTypes.bool,
  rank: React.PropTypes.number,
  name: React.PropTypes.string,
  iconUrl: React.PropTypes.string,
  membersCount: React.PropTypes.number,
  totalShoppedAmount: React.PropTypes.number,
  totalSavedAmount: React.PropTypes.number
};

export default SlotDetails;
