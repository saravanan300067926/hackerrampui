import React from 'react';
import styles from './similarcards.css';
import Slider from 'react-slick';
import { Helper } from '../utils/helper';

class SimilarCards extends React.Component {

  constructor(props) {
    super(props);
    this.settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1
    };
  }

  componentDidMount() {
    if (!this.props.isMobile) {
      Helper.scrollToBottom('showSimilarMainDiv');
    }
    const node = document.getElementById('showSimilarMainDiv');
    if (node) {
      node.className += ' similarcards-active';
    }
  }

  getSimilarItems() {
    if (this.props.similarItems.length > 0) {
      const items = this.props.similarItems.map((item, i) =>
        (
        <div key={`similarItemCard${i}`} className={styles.similarItemDiv}>
          <a href={`${Helper.getSecureRoot()}${item.pdpUrl}`}>
            <img
              className={styles.similarImage}
              src={item.image}
              onClick={() => Helper.navigateTo(item.pdpUrl, true)} />
          </a>
          <div className={styles.itemDetails}>
            <div className={styles.itemDetailsLabel}>
              {item.name}
            </div>
            <div className={`${styles.itemDetailsLabel}`}>
              {this.props.isMobile ? (
                <span className={styles.boldFont}>{item.hasDiscount ? item.discountedPrice : item.actualPrice}</span>
              ) : (
                <div>
                  <span className={item.hasDiscount ? `${styles.strike}` : `${styles.boldFont}`}>{item.actualPrice}</span>
                  <span className={item.hasDiscount ? `${styles.boldFont}` : `${styles.hide}`}> {item.discountedPrice}</span>
                  <span className={item.hasDiscount ? `${styles.discountPercent}` : `${styles.hide}`}> {item.discountPercent}</span>
                </div>
              )}
            </div>
          </div>
        </div>
        )
      );
      return items;
    }
    return this.getNoSimiarItemsDiv();
  }

  getArrowStyle() {
    let arrowStyle = '';
    if (this.props.clickedCard === 2) {
      arrowStyle = styles.arrowUpSecond;
    } else if (this.props.clickedCard === 3) {
      arrowStyle = styles.arrowUpThird;
    } else if (this.props.clickedCard === 4) {
      arrowStyle = styles.arrowUpFourth;
    }
    return arrowStyle;
  }

  getNoSimiarItemsDiv() {
    return (
      <div className={styles.errorDiv}>No similar items found.</div>
    );
  }

  getMobileSimilarCards() {
    Helper.disableScroll();
    const productListStyle = {
      width: `${this.props.similarItems.length === 0 ? '100%' : `${157 * this.props.similarItems.length}px`}`
    };
    return (
      <div id="showSimilarMainDiv" className={styles.mobileContainer}>
        <div className={styles.backdrop} onClick={() => { this.closeSimilarItemsDisplay(); }}></div>
        <div style={productListStyle} className={`${styles.similarProductList} ${this.props.similarItems.length === 0 && styles.noSimilarProducts}`}>
          <h3 className={styles.heading}>SIMILAR STYLES</h3>
          {this.getSimilarItems()}
        </div>
      </div>
    );
  }

  getArrowMarker() {
    const arrowStyle = this.getArrowStyle();
    if (typeof SVGRect !== 'undefined') {
      return (
        <div className={`${styles.svgArrowUp} ${arrowStyle}`}>
          <svg height="40" width="90">
            <polyline points="0,40 10,30 30,50" className={styles.svgArrow}></polyline>
          </svg>
        </div>
      );
    }
    return (<div className={`${styles.arrowUp} ${arrowStyle}`}></div>);
  }

  getDesktopSimilarCards() {
    const similarItems = this.getSimilarItems();
    let similarItemsSlider;
    if (similarItems instanceof Array) {
      similarItemsSlider = (
        <Slider {...this.settings}>
          {similarItems}
        </Slider>
      );
    } else {
      similarItemsSlider = this.getNoSimiarItemsDiv();
    }
    return (
      <div id="showSimilarMainDiv" className={styles.showSimilarDiv}>
        <div>
          {this.getArrowMarker()}
          {similarItemsSlider}
        </div>
        <div className={styles.removeIcon} onClick={() => { this.closeSimilarItemsDisplay(); }}>
          <span className={`myntraweb-sprite ${styles.removeMark}`}></span>
        </div>
      </div>
    );
  }

  getSimilarItemCards() {
    if (this.enableShowSimilar()) {
      if (this.props.isMobile) {
        return this.getMobileSimilarCards();
      }
      return this.getDesktopSimilarCards();
    }
    return null;
  }

  setValue(field, _field) {
    this.setState({ [field]: { ...this.state[field], ..._field } });
  }

  closeSimilarItemsDisplay() {
    const node = document.getElementById('showSimilarMainDiv');
    if (node) {
      node.parentNode.removeChild(node);
      this.props.similarClickHandler(false, this.props.styleId);
    }
    if (this.props.isMobile) {
      Helper.enableScroll();
    }
  }

  enableShowSimilar() {
    return !Helper.isShowSimilarDisabled();
  }

  render() {
    return this.getSimilarItemCards();
  }
}

SimilarCards.propTypes = {
  similarItems: React.PropTypes.arrayOf(React.PropTypes.object),
  clickedCard: React.PropTypes.number,
  similarClickHandler: React.PropTypes.func,
  setBusy: React.PropTypes.func,
  isMobile: React.PropTypes.bool,
  styleId: React.PropTypes.number.isRequired
};

export default SimilarCards;
