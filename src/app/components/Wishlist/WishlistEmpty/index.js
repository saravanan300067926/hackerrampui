import React from 'react';
import styles from './wishlistEmpty.css';

const WishlistEmpty = (props) =>
  <div className={props.isMobile ? styles.mobile : styles.desktop}>
    <div className={styles.container}>
      <div className={styles.heading}>YOUR WISHLIST IS EMPTY</div>
      <div className={styles.info}>Add items that you like to your wishlist. Review them anytime and easily move them to the bag.</div>
      <div className={`myntraweb-sprite ${styles.icon}`}></div>
      <div>
        <a href="/" className={styles.button}>CONTINUE SHOPPING</a>
      </div>
    </div>
  </div>;

WishlistEmpty.propTypes = {
  isMobile: React.PropTypes.bool.isRequired
};

export default WishlistEmpty;

