import React, { Component } from 'react';
import styles from './index.css';
import { isBrowser } from '../../utils';
import scroll from '@myntra/myx/lib/scroll';

class PageNotFound extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      popularSearch: [
        { name: 'Nike Shoes,', link: 'nike-shoes' },
        { name: 'Woodland Shoes,', link: 'woodland-shoes' },
        { name: 'Adidas Shoes,', link: 'sports-shoes' },
        { name: 'Titan Watches,', link: 'titan' },
        { name: 'Fila Shoes,', link: 'fila-shoes' },
        { name: 'Puma Shoes,', link: 'puma-shoes' },
        { name: 'Fastrack Watches', link: 'fastrack-watches' }
      ],
      productSection: [
        { name: 'Men', link: 'shop/men' },
        { name: 'Women', link: 'shop/women' },
        { name: 'Kids', link: 'shop/kids' },
        { name: 'Home & Living', link: 'home-furnishing-menu' }
      ]
    };
  }

  componentDidMount() {
    scroll(0);
  }

  getSearchTerm = () => {
    let terms = null;
    if (isBrowser()) {
      try {
        terms = window.location.pathname.replace('/', '');
      } catch (ex) {
        console.log(ex);
      }
    }
    return terms;
  }

  getPopularSearch = () => (
    this.state.popularSearch.map((val, index) =>
      <a key={index} href={`/${val.link}?src=404`}>{val.name}</a>
    )
  )

  getProductSection = () => (
    this.state.productSection.map((val, index) =>
      <a className={styles.productSections} key={index} href={`/${val.link}?src=404`}>{val.name}</a>
    )
  )

  trackInput = (e) => {
    this.setState({
      searchTerm: e.target.value
    });
    if (e.which === 13 && isBrowser()) {
      window.location = `/${this.state.searchTerm}?src=404`;
    }
  }

  render() {
    return (
      <center className={styles.PageNotFoundContainer}>
        <div>
          <p> You searched for <span className={styles.searchTerm}> {this.getSearchTerm()} </span> </p>
          <img className={styles.notFoundImage} src="https://constant.myntassets.com/web/assets/img/11488523304066-search404.png" />
          <p className={styles.infoBig}> We couldn't find any matches! </p>
          <p className={styles.infoSmall}>Please check the spelling or try searching something else</p>
        </div>
        <div className={styles.searchBox}>
          <input
            type="text"
            className={styles.inputBox}
            value={this.state.searchTerm}
            placeholder="Shoes, T-shirts, Tops etc."
            onChange={(e) => this.trackInput(e)}
            onKeyUp={(e) => this.trackInput(e)} />
          <a href={`/${this.state.searchTerm}`} className={styles.searchBtn}> SEARCH </a>
        </div>
        <div className={styles.popularSearch}>
          <span>Popular searches: </span>
          {this.getPopularSearch()}
        </div>
      </center>
    );
  }
}

export default PageNotFound;
