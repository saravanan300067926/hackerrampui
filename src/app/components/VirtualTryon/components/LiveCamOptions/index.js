import React from 'react';
import PropTypes from 'prop-types';
import Style from './livecamoptions.css';
import {
  enableSplitScreen,
  disableSplitScreen,
  saveScreenShot
} from './helpers';
import get from 'lodash/get';
import { isIosApp } from '../../../../utils/isApp';
const isIos = isIosApp();

const splitScreenClick = function (props) {
  window.Madalytics.sendRaw(
    'Youcam v2 - Split Screen Click', {
      'event_type': 'widgetClick',
      'event_category': 'Youcam v2',
      'payload': {
        'screen': {
          'name': props.screenName,
          'type': get(window, '__myx_deviceType__', ''),
          'url': window.location.href,
          'data_set': {
            'entity_type': props.pdpData.analytics.articleType,
            'entity_name': props.pdpData.name,
            'entity_id': props.pdpData.id
          }
        }
      }
    }
  );
  props.onSplitScreenClick();
};

const capturePhotoClick = function (props) {
  window.Madalytics.sendRaw(
    'Youcam v2 - Take Photo Click', {
      'event_type': 'widgetClick',
      'event_category': 'Youcam v2',
      'payload': {
        'screen': {
          'name': props.screenName,
          'type': get(window, '__myx_deviceType__', ''),
          'url': window.location.href,
          'data_set': {
            'entity_type': props.pdpData.analytics.articleType,
            'entity_name': props.pdpData.name,
            'entity_id': props.pdpData.id
          }
        }
      }
    }
  );
  props.onScreenShotClick();
};

const PopupButtons = props => (
  <div className={`${Style['floating-btn-container']} ${props.visible ? Style.visible : ''}`}>
    <button
      className={`${Style['floating-action-btn']} ${Style['floating-btn-top-radius']}`}
      onClick={() => splitScreenClick(props)}>
      Split screen
    </button>
    {!isIos && <button
      className={`${Style['floating-action-btn']} ${Style['floating-btn-bottom-radius']}`}
      onClick={() => capturePhotoClick(props)}>
      Take a photo
    </button>}
  </div>
);

class LiveCamOptions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pdpData: props.pdpData || window.__myx.pdpData,
      imageSaevdtoGallery: false
    };
    if (props.modeDetection === 'live') {
      this.state.screenName = 'Youcam Live Makeup Screen';
    } else if (props.modeDetection === 'model') {
      this.state.screenName = 'Youcam Model Mode Screen';
    } else if (props.modeDetection === 'photo') {
      this.state.screenName = 'Youcam Photo Mode Screen';
    } else {
      this.state.screenName = 'Youcam Select try-on Screen';
    }
  }

  toggleOptions() {
    this.props.showOptions({ showOptions: !this.props.expanded, closeIcon: !this.props.closeIcon });
    this.setState({ ...this.state });
  }

  exitSplitScreenClick() {
    window.Madalytics.sendRaw(
      'Youcam v2 - Exit Screen', {
        'event_type': 'screenExit',
        'event_category': 'Youcam v2',
        'payload': {
          'screen': {
            'name': this.state.screenName,
            'type': get(window, '__myx_deviceType__', ''),
            'url': window.location.href,
            'data_set': {
              'entity_type': this.state.pdpData.analytics.articleType,
              'entity_name': this.state.pdpData.name,
              'entity_id': this.state.pdpData.id
            }
          }
        }
      }
    );
    this.disableSplitScreenMode();
  }

  enableSplitScreenMode() {
    this.props.showOptions({ showOptions: false, closeIcon: false, isSplitScreenEnabled: true });
    enableSplitScreen();
  }

  disableSplitScreenMode() {
    this.props.showOptions({ isSplitScreenEnabled: false });
    disableSplitScreen();
  }

  takeScreenShot() {
    this.setState({ ...this.state, imageSaevdtoGallery: true });
    this.props.showOptions({ showOptions: false, closeIcon: false });
    saveScreenShot();
  }

  render() {
    const { visible } = this.props;
    if (this.state.imageSaevdtoGallery) {
      setTimeout(() => {
        this.setState({ imageSaevdtoGallery: false });
      }, 4000);
    }
    if (this.props.isSplitScreenEnabled) {
      return (
        <div className={Style['splitScreen-container']}>
          <button
            className={Style['splitscreen-exit-btn']}
            onClick={() => this.exitSplitScreenClick()}>
            Exit Split Screen
          </button>
        </div>
      );
    } else if (visible) {
      return (
        <div className={Style['options-container']} ref={youcamNode => (this.youcamNode = youcamNode)}>
          <PopupButtons
            pdpData={this.state.pdpData}
            visible={this.props.expanded}
            screenName={this.state.screenName}
            onSplitScreenClick={() => this.enableSplitScreenMode()}
            onScreenShotClick={() => this.takeScreenShot()} />
          <div className={Style['options-btn']} onClick={() => this.toggleOptions()}>
            <div className={!this.props.closeIcon ? Style['options-img'] : Style['close-img']} />
          </div>
          {this.state.imageSaevdtoGallery ? <div className={Style['img-popup']}> Your photo is saved in your gallery </div> : ''}
        </div>
      );
    }

    return null;
  }
}

PopupButtons.propTypes = {
  onSplitScreenClick: PropTypes.func.isRequired,
  onScreenShotClick: PropTypes.func.isRequired,
  pdpData: PropTypes.object.isRequired,
  visible: PropTypes.bool,
  screenName: PropTypes.string
};

LiveCamOptions.propTypes = {
  visible: PropTypes.bool,
  pdpData: PropTypes.object.isRequired,
  expanded: PropTypes.bool,
  showOptions: PropTypes.func,
  closeIcon: PropTypes.bool,
  isSplitScreenEnabled: PropTypes.bool,
  modeDetection: PropTypes.string
};

export default LiveCamOptions;
