function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return `${s4()}-${s4()}-${s4()}`;
}

function generateUniqueImageName() {
  return `myntra-tryon-${guid()}.jpeg, Photo is saved in your gallery`;
}

function downloadImage(base64Image) {
  const element = document.createElement('a');
  element.setAttribute('href', base64Image);
  element.setAttribute('download', generateUniqueImageName());
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

export function enableSplitScreen() {
  window.YMK.enableCompare();
}

export function disableSplitScreen() {
  window.YMK.disableCompare();
}

export function saveScreenShot() {
  window.YMK.snapshot('base64', base64Image => downloadImage(base64Image));
}
