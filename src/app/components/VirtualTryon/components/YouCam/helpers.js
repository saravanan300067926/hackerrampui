const YOUCAM_API_KEY = '0dHxeG0un4oz5Zg3XBhReg==';

// eslint-disable-next-line no-unused-vars
global.ymkAsyncInit = () => {
  const YouCamSdk = window.YMK;
  const MAX_SUPPORTED_WIDTH = 545;
  const canvasWidth =
    window.screen.width > MAX_SUPPORTED_WIDTH
      ? MAX_SUPPORTED_WIDTH
      : window.screen.width;
  const config = {
    disable2ColorComparison: true,
    hideButtonsOnResult: true,
    showCompareCaption: true,
    width: canvasWidth,
    height: canvasWidth * 1.1
  };
  YouCamSdk.init(config);
};

function loadYoucamSdk() {
  const d = window.document;
  const s = d.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src =
    `//plugins-myntra.makeupar.com/c1819_1/sdk.js?apiKey=${YOUCAM_API_KEY}`;
  const x = d.getElementsByTagName('script')[0];
  x.parentNode.insertBefore(s, x);
}

export default loadYoucamSdk;
