import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Style from './youcam.css';
import LoadingExperience from '../LoadingExperience';
import PdpInfo from '../PdpInfo';
import Shades from '../Shades';
import ActionButtons from '../ActionButtons';
import Options from '../LiveCamOptions';
import Notify from '../../../Notify';
import get from 'lodash/get';
import { isIosApp } from '../../../../utils/isApp';
const initialState = {
  sdkLoadProgress: -1,
  isCanvasLoaded: false
};
class YouCam extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      pdpData: props.pdpData || window.__myx.pdpData,
      showOptions: false,
      closeIcon: false,
      ymkWidth: '',
      ymkHeight: '',
      isSplitScreenEnabled: false,
      articleType: false
    };
    this.initYMKEventListneres = this.initYMKEventListneres.bind(this);
    this.triggerError = this.triggerError.bind(this);
    this.toggleBtnOptions = this.toggleBtnOptions.bind(this);
    this.notifyError = this.notifyError.bind(this);
    this.notifySuccess = this.notifySuccess.bind(this);
  }

  componentWillMount() {
    const { base64Image } = this.props;
    const youcamConfig = base64Image ? [false, base64Image] : [true];

    const timer = setInterval(() => {
      if (typeof window.YMK === 'object') {
        this.initYMKEventListneres();
        window.YMK.open(...youcamConfig);
        clearInterval(timer);
      }
    });
  }

  componentDidMount() {
    document.addEventListener('click', this.toggleBtnOptions);
    if (this.props.modeDetection === 'live') {
      window.Madalytics.sendRaw(
        'Youcam v2 - Live Makeup Screen Load', {
          'event_type': 'screenLoad',
          'event_category': 'Youcam v2',
          'payload': {
            'screen': {
              'name': 'Youcam Live Makeup Screen',
              'type': get(window, '__myx_deviceType__', ''),
              'url': window.location.href,
              'data_set': {
                'entity_type': this.props.pdpData.analytics.articleType,
                'entity_name': this.props.pdpData.name,
                'entity_id': this.state.pdpData.id
              }
            }
          }
        }
      );
    } else if (this.props.modeDetection === 'model') {
      window.Madalytics.sendRaw(
        'Youcam v2 - Model Mode Screen Load', {
          'event_type': 'screenLoad',
          'event_category': 'Youcam v2',
          'payload': {
            'screen': {
              'name': 'Youcam Model Mode Screen Load',
              'type': get(window, '__myx_deviceType__', ''),
              'url': window.location.href,
              'data_set': {
                'entity_type': this.props.pdpData.analytics.articleType,
                'entity_name': this.props.pdpData.name,
                'entity_id': this.state.pdpData.id
              }
            }
          }
        }
      );
    }
  }

  componentWillUnmount() {
    window.YMK.close();
  }

  initYMKEventListneres() {
    window.YMK.addEventListener('loading', sdkLoadProgress => {
      this.setState({ ...this.state, sdkLoadProgress });
    });

    window.YMK.addEventListener('noFaceInPhoto', () => {
      this.triggerError('No face detected');
    });

    window.YMK.addEventListener('invalidPhoto', () => {
      this.triggerError('Invalid Photo');
    });

    window.YMK.addEventListener('photoResolutionNotSupport', () => {
      this.triggerError('Photo resolution not supported');
    });

    window.YMK.addEventListener('cameraFailed', () => {
      console.log('cameraFailed');
      this.triggerError('cameraFailed');
    });

    window.YMK.addEventListener('loaded', () => {
      const newState = {
        isCanvasLoaded: true
      };
      this.setState({ ymkWidth: document.getElementById('YMK-module').firstChild.style.width });
      this.setState({ ymkHeight: document.getElementById('YMK-module').firstChild.style.height });
      this.setState({ ...this.state, ...newState });
    });
  }

  toggleBtnOptions(e) {
    if (this.youcamNode.contains(e.target) && this.state.showOptions) {
      this.setState({ showOptions: false, closeIcon: false });
    } else {
      return;
    }
  }

  triggerError(errMsg) {
    console.error(`[youcam error]: ${errMsg}`);
    this.notifyError(errMsg);
    setTimeout(() => this.props.setInitState(), 2500);
  }

  notifyError(message) {
    this.refs.notify.error(message);
  }

  notifySuccess(message) {
    this.refs.notify.info({
      message
    });
  }

  render() {
    const { sdkLoadProgress, isCanvasLoaded } = this.state;
    const moduleParams = {
      width: this.state.ymkWidth,
      height: this.state.ymkHeight,
      zIndex: 4,
      position: 'absolute'
    };

    const NoShade = () => (
      <div className={Style.noShades}>
        <div className={Style.noShadeSelected}> No Shade Selected </div>
        <div className={Style.selectShade}> Select a shade </div>
      </div>
    );

    return (
      <div className={`${Style.box} ${Style.container}`} >
        <LoadingExperience
          progress={sdkLoadProgress}
          hide={isCanvasLoaded} />
        <div className={isCanvasLoaded ? Style['ymk-visible'] : Style['ymk-invisible']}>
          {this.state.isSplitScreenEnabled ? <div className={Style.splitScreenMsg}> No shade applied </div> : ''}
          {this.state.isSplitScreenEnabled ? <div className={`${Style.splitScreenMsg} ${Style.right20p} ${Style.textWrap}`}>
            {this.state.articleType == null ? 'None' : this.state.pdpData.articleAttributes['Colour Shade Name']}
          </div> : ''}
          <div className={`${Style.box} ${Style.row} ${Style.content}`} ref={youcamNode => { this.youcamNode = youcamNode; }}>
            {this.state.showOptions ? <div style={moduleParams} /> : ''}
            <div id="YMK-module" />
          </div>
          <Options
            expanded={this.state.showOptions}
            closeIcon={this.state.closeIcon}
            showOptions={optionFlags => this.setState({ showOptions: optionFlags.showOptions,
              closeIcon: optionFlags.closeIcon, isSplitScreenEnabled: optionFlags.isSplitScreenEnabled })}
            visible={isCanvasLoaded}
            isSplitScreenEnabled={this.state.isSplitScreenEnabled}
            pdpData={this.state.pdpData}
            modeDetection={this.props.modeDetection} />
          <div className={`${Style.box} ${Style.row} ${Style.footer}`}>
            {this.state.articleType == null ? <NoShade /> : <PdpInfo data={this.state.pdpData} />}
            <Shades
              selectedShade={shadeApply => this.setState({ pdpData: shadeApply })}
              productName={productInfo => this.setState({ articleType: productInfo.articleType })}
              data={this.state.pdpData}
              styleId={this.props.styleId}
              ymkLoaded={this.state.isCanvasLoaded}
              defaultSelectNone={this.state.articleType}
              modeDetection={this.props.modeDetection} />
            {
              !isIosApp() && (
                <ActionButtons
                  disable={this.state.articleType === null}
                  notifyError={this.notifyError}
                  notifySuccess={this.notifySuccess}
                  product={this.state.pdpData} />
              )
            }
          </div>
        </div>
        <Notify ref="notify" />
      </div>
    );
  }
}

YouCam.propTypes = {
  base64Image: PropTypes.string,
  pdpData: PropTypes.object.isRequired,
  youcamConfig: PropTypes.object,
  styleId: PropTypes.string,
  setInitState: PropTypes.func,
  modeDetection: PropTypes.string
};

export default YouCam;
