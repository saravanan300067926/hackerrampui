import React from 'react';
import get from 'lodash/get';
import style from './modelSelector.css';
import base64 from './models-base64-images.js';

class ModelSelector extends React.PureComponent {

  modelSelect(modelImage) {
    const dataSet = {
      'entity_type': get(this.props, 'pdpData.analytics.articleType'),
      'entity_name': get(this.props, 'pdpData.name'),
      'entity_id': get(this.props, 'pdpData.id')
    };
    window.Madalytics.sendRaw(
      'Youcam v2 - Select Model', {
        'event_type': 'widgetClick',
        'event_category': 'Youcam v2',
        'payload': {
          'screen': {
            'name': 'Youcam Model Mode Screen Load',
            'type': get(window, '__myx_deviceType__', ''),
            'url': window.location.href,
            dataSet
          }
        }
      }
    );
    this.props.onModelSelect(modelImage);
    this.props.modeDetection('model');
  }

  render() {
    return (<div>
      <div className={style.modelHeader}> Select a model </div>
      <div className={style.container}>
        {base64.map((eachModel, index) => (
          <a key={index} onClick={() => this.modelSelect(eachModel)} >
            <img
              className={style.image}
              src={eachModel} />
          </a>
        ))}
      </div>
    </div>);
  }
}

export default ModelSelector;
