/* global MyntApp:false */
import React from 'react';
import get from 'lodash/get';
import findIndex from 'lodash/findIndex';
import Shade from './Shade';
import {
  NONE_SHADE,
  isNoneShade,
  applyShade,
  clearSelectedShade
} from './helpers';
import Style from './shades.css';
import config from '../../../../config';
import Client from '../../../../services';
import PropTypes from 'prop-types';
import ShadeExperience from '../LoadingExperience/ShadeExperience';

class Shades extends React.Component {
  constructor(props) {
    super(props);
    const systemAttributes = get(this.props, 'data.systemAttributes') || [];
    const metaInfo = this.getMetaInfo(systemAttributes);
    this.relatedStyles = get(this.props, 'data.relatedStyles') || [];
    let defaultShade = [];
    if (this.relatedStyles && this.relatedStyles.length) {
      const indexVal = findIndex(this.relatedStyles, eachShade => `${eachShade.styleId}${''}` === this.props.styleId);
      if (metaInfo && metaInfo.length) {
        defaultShade = this.relatedStyles.splice(indexVal, 1);
      } else {
        this.relatedStyles.splice(indexVal, 1);
      }
      this.relatedStyles.splice(10);
    }

    this.state = {
      selectedShade: {},
      onLoadShadeSelect: false,
      selectedShadePdp: {},
      relativeShades: [],
      shadeResponse: [],
      defaultShade,
      metaInfo,
      showShades: false
    };
    this.shadeChangeHandler = this.shadeChangeHandler.bind(this);
    this.fetchPDP = this.fetchPDP.bind(this);
  }

  componentDidMount() {
    Promise.all(this.relatedStyles.map(eachStyleID => {
      const shade = eachStyleID;
      return new Promise(resolve => {
        this.fetchPDP(shade).then(value => {
          resolve(value);
        }).catch(() => {
          resolve(undefined);
        });
      });
    }))
      .then(values => {
        const stylesWithSystemAttr = [];
        for (let i = 0; i < values.length; i++) {
          const style = values[i];
          const systemAttributes = get(style, 'systemAttributes') || [];
          const metaInfo = this.getMetaInfo(systemAttributes);
          if (systemAttributes.length > 0 && metaInfo && metaInfo.length > 0) {
            stylesWithSystemAttr.push(get(style, 'id'));
          }
        }
        const shadesWithSystemAttributes = [];
        for (let i = 0; i < this.relatedStyles.length; i++) {
          const item = this.relatedStyles[i];
          if (stylesWithSystemAttr.indexOf(item.styleId) >= 0) {
            shadesWithSystemAttributes.push(item);
          }
        }
        this.setState({ relativeShades: shadesWithSystemAttributes, selectedShade: this.props.styleId });
        this.props.productName({ articleType: null });
      })
      .finally(() => this.setState({ showShades: true, articleType: false }));
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.ymkLoaded !== this.props.ymkLoaded && this.state.metaInfo && this.state.metaInfo.length) {
      this.setState({ onLoadShadeSelect: true });
      applyShade(this.state.metaInfo);
      this.props.productName({ articleType: false });
    }
  }

  getMetaInfo(systemAttributes) {
    const youcamSystemAttribute = systemAttributes.find((value) => (
     get(value, 'systemAttributeValueEntry.valueName') === 'Youcam' || get(value, 'systemAttributeValueEntry.valueCode') === 'YOUCAM'
    ));
    return get(youcamSystemAttribute, 'metaInfo');
  }

  isSelected(shade) {
    return shade.styleId === this.state.selectedShade.styleId;
  }

  fetchPDP(selectedShade) {
    return new Promise((resolve, reject) => {
      Client.get(`${config('pdpV1')}${selectedShade.styleId}`)
        .end((err, response) => {
          if (err) {
            console.error('fetch product return err', err);
            reject(err);
          }
          resolve({ ...get(response, 'body') });
        });
    });
  }

  shadeChangeHandler(selectedShade) {
    this.setState({ onLoadShadeSelect: false });
    const styleId = `${selectedShade.styleId}${''}`;
    if (typeof window.MyntApp !== 'undefined' && typeof MyntApp.onTryOnColourChanged === 'function') {
      MyntApp.onTryOnColourChanged(styleId);
    }

    if (isNoneShade(selectedShade)) {
      clearSelectedShade();
      this.props.productName({ articleType: null });
    } else {
      this.fetchPDP(selectedShade).then(shadeValue => {
        this.props.selectedShade(shadeValue);
        this.props.productName({ articleType: false });
        const systemAttributes = get(shadeValue, 'systemAttributes');
        const metaData = this.getMetaInfo(systemAttributes);
        if (metaData && metaData.length) {
          applyShade(metaData);
        } else {
          clearSelectedShade();
        }
      })
      .catch(() => undefined);
    }
    this.setState({ selectedShade });
  }

  render() {
    let shades = [];
    shades = shades.concat(this.state.defaultShade, this.state.relativeShades || []);
    if (shades.length > 1) {
      shades.unshift(NONE_SHADE);
    }
    return (
      <div className={Style['shade-container']}>
        {this.state.showShades ? (
          shades.map(shade => (
            <Shade
              key={shade.styleId}
              data={shade}
              selectNone={this.props.defaultSelectNone}
              selected={this.state.onLoadShadeSelect && this.props.styleId === `${shade.styleId}${''}` ? true : this.isSelected(shade)}
              onClick={selectedShade => this.shadeChangeHandler(selectedShade)} />
          ))
         ) : <ShadeExperience />}
      </div>
    );
  }
}

Shades.propTypes = {
  selectedShade: PropTypes.func,
  styleId: PropTypes.string,
  productName: PropTypes.func,
  modeDetection: PropTypes.string,
  defaultSelectNone: PropTypes.bool,
  ymkLoaded: PropTypes.bool
};

export default Shades;
