export const NONE_SHADE = {
  styleId: 'none',
  displayText: 'None',
  imageUrl: ''
};

export function isNoneShade(shade) {
  return shade.styleId === NONE_SHADE.styleId;
}

export function clearSelectedShade() {
  window.YMK.reset();
}

export function applyShade(shade) {
  window.YMK.applyMakeupBySku(shade);
}
