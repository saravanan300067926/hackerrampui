import React from 'react';
import PropTypes from 'prop-types';
import { isNoneShade } from './helpers';
import Style from './shades.css';

const CheckMark = () => (
  <div className={`myntraweb-sprite ${Style.whiteCheckMark}`}> </div>
);

const getShadeChildren = (shade, selectNone, imageClickHandler, selected) => {
  const { displayText } = shade;
  const imageUrl = shade.imageUrl || '';
  const ValidShade = (
    <div>
      <img
        src={imageUrl.replace(/https?:/, '')}
        alt={displayText}
        className={Style['shade-img']}
        onClick={() => imageClickHandler(shade)} />
      {selected && <CheckMark shade={shade} />}
      <p className={Style['shade-text']}>{displayText}</p>
    </div>
  );
  const NoneShade = (
    <div>
      <div
        className={Style['none-shade']}
        onClick={() => imageClickHandler(shade)} >
        {selectNone === null && <CheckMark shade={shade} />}
        <div className={Style.diagnolLine} />
      </div>
      <p className={`${Style.marginTop10} ${Style['shade-text']}`}>{displayText}</p>
    </div>
  );
  return isNoneShade(shade) ? NoneShade : ValidShade;
};

const Shade = ({ data, selectNone, onClick, selected }) => {
  const { styleId } = data;
  return (
    <div>
      <div key={styleId} className={Style.shade}>
        {getShadeChildren(data, selectNone, onClick, selected)}
      </div>
    </div>
  );
};

CheckMark.propTypes = { shade: PropTypes.object.isRequired };
Shade.propTypes = {
  data: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  selected: PropTypes.bool,
  selectNone: PropTypes.bool
};

export default Shade;
