import React from 'react';
import PropTypes from 'prop-types';
import Style from './navbar.css';

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      disable: this.props.disable || false,
      title: this.props.title || 'TITLE'
    }
  }
  // onClick={() => window.YMK.close() }
  render() {
    return (
      <div className={Style['nav-container']}>
        <div className={`${Style['back-btn']} ${Style['icon-back']} ${this.state.disable ? Style.disabled : ''}`} />
        <div className={this.state.disable ? Style.disabled : ''}>{this.state.title}</div>
        <a className={Style['close-button']} href={window.location.search === '?page=canvas' ? `${window.location.pathname}${'?live=true'}` : `${window.location.pathname}${window.location.search}`} >
          Close
        </a>
      </div>
    )
  }
};

NavBar.propTypes = {
  disable: PropTypes.bool,
  title: PropTypes.string
};

export default NavBar;
