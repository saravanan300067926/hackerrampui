import React from 'react';
import PropTypes from 'prop-types';
import Style from './pdpinfo.css';

class PDPInfo extends React.PureComponent {
  render() {
    const { name, price } = this.props.data;
    return (
      <div className={Style['pdp-info']}>
        <div className={Style.title}>{name}</div>
        <div className={Style['price-container']}>
          <span className={Style['icon-rupee']} />
          <span className={Style['price-discounted']}>Rs. {price.discounted}</span>
          {price.discount && (
            <div>
              <span className={Style['icon-rupee price-mrp']} />
              <span className={Style['price-mrp']}>Rs. {price.mrp}</span>
              <span className={Style['discount-percent']} dangerouslySetInnerHTML={{ __html: price.discount.label }}></span>
            </div>
          )}
        </div>
      </div>
    );
  }
}

PDPInfo.propTypes = {
  data: PropTypes.shape({
    style: PropTypes.shape({
      name: PropTypes.string,
      price: PropTypes.shape({
        mrp: PropTypes.number,
        discounted: PropTypes.number,
        discount: PropTypes.shape({
          label: PropTypes.string
        })
      })
    })
  })
};

export default PDPInfo;