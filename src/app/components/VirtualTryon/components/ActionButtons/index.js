/* global MyntApp:false */
import React from 'react';
import PropTypes from 'prop-types';
import at from 'v-at';
import styles from './actionbuttons.css';
import { AddToCart, Wishlist } from '../../../CartActions';

class ActionButtons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className={styles.container} >
        <Wishlist
          btnClassName={styles.cartButton}
          loginRequired={true}
          redirectToCheckout={true}
          productStyleId={at(this.props.product, 'id')}
          sizes={at(this.props.product, 'sizes')}
          onError={(msg) => this.props.notifyError(msg)}
          disable={this.props.disable}
          onSuccess={(msg) => this.props.notifySuccess(msg)} />
        <AddToCart
          disable={this.props.disable}
          btnClassName={styles.cartButton}
          loginRequired={true}
          redirectToCheckout={true}
          productStyleId={at(this.props.product, 'id')}
          sizes={at(this.props.product, 'sizes')}
          onError={(msg) => this.props.notifyError(msg)}
          onSuccess={(msg) => this.props.notifySuccess(msg)} />
      </div>
    );
  }
}

ActionButtons.propTypes = {
  product: PropTypes.object,
  disable: PropTypes.bool,
  notifySuccess: PropTypes.func,
  notifyError: PropTypes.func
};

export default ActionButtons;
