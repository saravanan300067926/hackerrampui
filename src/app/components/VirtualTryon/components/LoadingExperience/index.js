import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Style from './loadingexperience.css';
import ShadeExperience from './ShadeExperience';

function getRandomSlogan() {
  const SLOGANS = [
    'Let’s get glam!',
    'Life isn’t perfect, but your makeup can be',
    'Myntra Beauty try-on loading...',
    'Almost there…'
  ];

  return SLOGANS[Math.floor(Math.random() * SLOGANS.length)];
}

class LoadingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slogan: getRandomSlogan()
    };
    this.sloganInterval = setInterval(() => {
      this.setState({ ...this.state, slogan: getRandomSlogan() });
    }, 2000);
  }
  componentWillUnmount() {
    clearInterval(this.sloganInterval);
  }
  render() {
    if (this.props.hide) {
      clearInterval(this.sloganInterval);
    }

    return this.props.hide ? null : (
      <div className={Style.container}>
        <div className={Style['banner-image-container']}>
          <div className={Style['banner-image']} />
          <span className={Style['banner-bold-text']}>Loading your vanity</span>
          <span className={Style['banner-light-text']}>
            {this.state.slogan}
          </span>
        </div>
        <progress
          className={Style['sdk-loading-progress']}
          id="sdk-loading-progress"
          min="0"
          max="100"
          defaultValue="0"
          value={this.props.progress} />
        <div className={Style.loadFooterContainer}>
          <ShadeExperience />
          <div className={Style.loadActions}></div>
        </div>
      </div>
    );
  }
}

LoadingScreen.propTypes = {
  hide: PropTypes.bool,
  progress: PropTypes.number
};

export default LoadingScreen;
