import React from 'react';
import Style from './loadingexperience.css';

class ShadeExperience extends React.PureComponent {
  render() {
    const shadesIterate = [1, 2, 3, 4, 5, 6];
    return (
      <div>
        <div className={Style.loadShades}>
          {shadesIterate.map(eachShade => (
            <div key={eachShade}>
              <div className={Style.eachShade}></div>
              <div className={Style.shadeHost}></div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default ShadeExperience;
