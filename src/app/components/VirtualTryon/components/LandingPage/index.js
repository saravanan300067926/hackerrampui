import React from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import Style from './landingpage.css';
import { isIosApp } from '../../../../utils/isApp';
const isIos = isIosApp();
class LandingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pdpData: props.pdpData || window.__myx.pdpData
    };
    this.handleUploadSelfie = this.handleUploadSelfie.bind(this);
    this.liveCamEvent = this.liveCamEvent.bind(this);
    this.modelClickEvent = this.modelClickEvent.bind(this);
    this.photoModeEvent = this.photoModeEvent.bind(this);
  }

  componentDidMount() {
    window.Madalytics.sendRaw(
      'Youcam v2 - Select Try-on Screen Load', {
        'event_type': 'screenLoad',
        'event_category': 'Youcam v2',
        'payload': {
          'screen': {
            'name': 'Youcam Select try-on',
            'type': get(window, '__myx_deviceType__', ''),
            'url': window.location.href,
            'data_set': {
              'entity_type': this.props.pdpData.analytics.articleType,
              'entity_name': this.props.pdpData.name,
              'entity_id': this.state.pdpData.id
            }
          }
        }
      }
    );
  }

  handleUploadSelfie(files) {
    const dataSet = {
      'entity_type': this.props.pdpData.analytics.articleType,
      'entity_name': this.props.pdpData.name,
      'entity_id': this.state.pdpData.id
    };
    const self = this;
    if (files && files[0]) {
      const FR = new FileReader();
      FR.addEventListener('load', e => {
        window.Madalytics.sendRaw(
          'Youcam v2 - Photo Mode Screen Load', {
            'event_type': 'screenLoad',
            'event_category': 'Youcam v2',
            'payload': {
              'screen': {
                'name': 'Youcam Photo Mode Screen Load',
                'url': window.location.href,
                'type': get(window, '__myx_deviceType__', ''),
                dataSet
              }
            }
          }
        );
        self.props.onSelfieUpload(e.target.result);
      });
      FR.readAsDataURL(files[0]);
    }
  }

  photoModeEvent() {
    this.props.modeDetection('photo');
    window.Madalytics.sendRaw(
      'Youcam v2 - Photo Mode Click', {
        'event_type': 'widgetClick',
        'event_category': 'Youcam v2',
        'payload': {
          'screen': {
            'name': 'Youcam Select try-on',
            'url': window.location.href,
            'type': get(window, '__myx_deviceType__', ''),
            'data_set': {
              'entity_type': this.props.pdpData.analytics.articleType,
              'entity_name': this.props.pdpData.name,
              'entity_id': this.state.pdpData.id
            }
          }
        }
      }
    );
  }

  modelClickEvent() {
    window.Madalytics.sendRaw(
      'Youcam v2 - Model Mode Click', {
        'event_type': 'widgetClick',
        'event_category': 'Youcam v2',
        'payload': {
          'screen': {
            'name': 'Youcam Select try-on',
            'url': window.location.href,
            'type': get(window, '__myx_deviceType__', ''),
            'data_set': {
              'entity_type': this.props.pdpData.analytics.articleType,
              'entity_name': this.props.pdpData.name,
              'entity_id': this.state.pdpData.id
            }
          }
        }
      }
    );
  }

  liveCamEvent() {
    window.Madalytics.sendRaw(
      'Youcam v2 - Live Makeup Click', {
        'event_type': 'widgetClick',
        'event_category': 'Youcam v2',
        'payload': {
          'screen': {
            'name': 'Youcam Select try-on',
            'url': window.location.href,
            'type': get(window, '__myx_deviceType__', ''),
            'data_set': {
              'entity_type': this.props.pdpData.analytics.articleType,
              'entity_name': this.props.pdpData.name,
              'entity_id': this.state.pdpData.id
            }
          }
        }
      }
    );
  }

  render() {
    const { styleId, live } = this.props;
    return (
      <div className={Style['layout-container']}>
        <span className={Style['mode-selector-header']}>Select try-on experience</span>
        {live && !isIos && <a
          className={`${Style['mode-btn']} ${Style['mode-btn-big']}`}
          href={`/youcam/${styleId}?page=canvas&mode=app`}
          onClick={() => this.liveCamEvent()}>
          <div className={Style.header}>
            <span className={`myntraweb-sprite ${Style['icon-chevron']}`} />
            Live makeup
            <span className={`myntraweb-sprite ${Style['icon-right']} ${Style['icon-rightCenter']}`} />
          </div>
          <span className={Style.tag}>Try shades live on your face</span>
        </a>}
        <div className={Style['mode-btn-container']}>
          <a
            className={`${Style['mode-btn']} ${Style['mode-btn-small']}`}
            href={`/youcam/${styleId}?page=choosemodel&mode=app`}
            onClick={() => this.modelClickEvent()}>
            <span className={`myntraweb-sprite ${Style['icon-model']}`} />
            <span className={`myntraweb-sprite ${Style['icon-right']} `} />
            <span className={Style['mode-btn-small-header']}>Apply on model</span>
          </a>
          <input
            type="file"
            accept="image/png, image/jpg, image/jpeg, image/svg"
            className={Style['hidden-inputfile']}
            id="selfieUpload"
            onClick={() => this.photoModeEvent()}
            onChange={e => this.handleUploadSelfie(e.target.files)} />
          <label
            htmlFor="selfieUpload"
            className={`${Style['mode-btn']} ${Style['mode-btn-small']} ${Style['mode-btn-small-right']}`}>
            <span className={`myntraweb-sprite ${Style['icon-photo']}`} />
            <span className={`myntraweb-sprite ${Style['icon-right']} `} />
            <span className={Style['mode-btn-small-header']}>Select a photo</span>
          </label>
        </div>
      </div>
    );
  }
}

LandingPage.propTypes = {
  styleId: PropTypes.string,
  live: PropTypes.string,
  pdpData: PropTypes.object,
  modeDetection: PropTypes.func
};

export default LandingPage;
