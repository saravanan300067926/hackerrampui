import React from 'react';
import PropTypes from 'prop-types';
import loadYouCam from './components/YouCam/helpers';
import YouCam from './components/YouCam';
import LandingPage from './components/LandingPage';
import ModelChooser from './components/ModelSelector';

class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.setQueryParam();
    this.state = {
      pdpData: props.pdpData || window.__myx.pdpData,
      base64Image: null,
      launchMode: ''
    };
  }

  componentWillMount() {
    loadYouCam();
  }

  setQueryParam(key = 'mode', value = 'app') {
    if (history.pushState) {
      const params = new URLSearchParams(window.location.search);
      if (!params.get('mode')) {
        params.set(key, value);
        const newUrl = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${params.toString()}`;
        window.location.href = newUrl;
      }
    }
  }

  setImage(base64Image) {
    this.setState({ ...this.state, base64Image });
  }

  setInitState() {
    this.setState({
      ...this.state,
      base64Image: null,
      launchMode: ''
    });
  }

  render() {
    const { live, page } = this.props.location.query;
    const { styleId } = this.props.params;
    let children = null;
    if (!styleId) {
      children = <div> Please give styleId to open youcam on </div>;
    }

    if (page && page === 'choosemodel') {
      children = (
        <ModelChooser
          styleId={styleId}
          pdpData={this.state.pdpData}
          onModelSelect={modelData => this.setImage(modelData)}
          modeDetection={launchMode => this.setState({ launchMode })} />
      );
    }

    if (page === 'canvas' || this.state.base64Image) {
      children = (
        <YouCam
          {...this.state}
          modeDetection={page === 'canvas' ? 'live' : this.state.launchMode}
          styleId={styleId}
          setInitState={() => this.setInitState()} />
      );
    }

    return children || <LandingPage
      live={live} pdpData={this.state.pdpData}
      styleId={styleId}
      onSelfieUpload={selfieData => this.setImage(selfieData)}
      modeDetection={launchMode => this.setState({ launchMode })} />;
  }
}

App.propTypes = {
  pdpData: PropTypes.object,
  location: PropTypes.shape({
    query: PropTypes.object
  }),
  params: PropTypes.shape({
    styleId: PropTypes.string
  })
};

export default App;
