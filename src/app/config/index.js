const ENV = process.env.NODE_ENV;
const ROUTES = {
  ROOT: 'http://www.myntra.com/',
  SECURE_ROOT: 'https://www.myntra.com/',
  biro: {
    root: 'http://knuth.myntra.com/',
    path: 'biro/desktop'
  },
  mobileVerify: {
    root: 'http://knuth.myntra.com/',
    path: 'user/mobile/addphone?verify=true'
  },
  otpVerify: {
    root: 'http://knuth.myntra.com/',
    path: 'user/mobile/verifyphone'
  },
  isNewUser: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/user/status'
  },
  locationContext: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/user/locationContext'
  },
  searchProduct: {
    root: 'http://searchservice.myntra.com/',
    path: 'search-service/searchservice/search/getquery'
  },
  filteredSearch: {
    root: 'http://knuth.myntra.com/',
    path: 'v2/search'
  },
  filteredSearchOld: {
    root: 'https://developer.myntra.com/',
    path: 'v2/search/data'
  },
  pdp: {
    root: 'http://knuth.myntra.com/',
    path: 'v2/product/'
  },
  pdpV1: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/product/'
  },
  reviews: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/reviews/product/'
  },
  reviewsByID: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/reviews/review/'
  },
  reviewsByBatch: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/reviews/batch/'
  },
  reviewsSetVote: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/reviews/vote'
  },
  cart: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/cart/default/add'
  },
  cartSummary: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/cart/default/summary'
  },
  wishlist: {
    path: 'myntra-absolut-service/absolut/wishlist'
  },
  wishlistNew: {
    root: 'https://developer.myntra.com/',
    path: 'collections/me/wishlist'
  },
  wishlistApify: {
    root: 'http://knuth.myntra.com/',
    path: 'v1/user/wishlist'
  },
  wishlistApifyV2: {
    root: 'http://knuth.myntra.com/',
    path: 'v2/user/wishlist'
  },
  mfg: {
    root: 'https://developer.myntra.com/',
    path: 'mfg/v1'
  },
  admission: {
    root: 'https://developer.myntra.com/',
    path: 'admission'
  },
  signin: {
    root: 'http://knuth.myntra.com/',
    path: 'auth/v1/login'
  },
  signout: {
    root: 'http://knuth.myntra.com/',
    path: 'auth/v1/signout'
  },
  google: {
    root: 'http://knuth.myntra.com/',
    path: 'auth/v1/glogin'
  },
  facebook: {
    root: 'http://knuth.myntra.com/',
    path: 'auth/v1/fblogin'
  },
  signup: {
    root: 'http://knuth.myntra.com/',
    path: 'auth/v1/signup'
  },
  forgotPassword: {
    root: 'http://knuth.myntra.com/',
    path: 'auth/v1/forgetpassword'
  },
  signoutOld: {
    path: 'myntapi/idp/auth/signout'
  },
  topnav: {
    root: 'https://developer.myntra.com/',
    path: 'config/nav/v1/web/top'
  },
  search: {
    root: 'http://force.myntra.com/',
    path: 'force-service/v1/suggest/query'
  },
  coupons: {
    root: 'http://knuth.myntra.com/v2/product/',
    path: 'offers'
  },
  recommendations: {
    root: 'http://knuth.myntra.com/v2/product/',
    path: 'related'
  },
  crossSell: {
    root: 'http://knuth.myntra.com/v2/product/',
    path: 'cross-sell'
  },
  serviceability: {
    root: 'http://knuth.myntra.com/',
    path: 'serviceability/check'
  },
  serviceabilityV2: {
    root: 'http://knuth.myntra.com/',
    path: 'v2/serviceability/check'
  },
  serviceabilityV3: {
    root: 'http://knuth.myntra.com/',
    path: 'v3/serviceability/check'
  },
  sizeReco: {
    root: 'http://knuth.myntra.com',
    path: ''
  },
  idp: {
    root: 'http://www.myntra.com/',
    path: 'myntapi/idp/auth/'
  },
  'userData': {
    path: 'beacon/user-data'
  },
  instrumentation: {
    debug: false
  },
  statsd: {
    app: 'myx-search',
    prefix: `myx-${process.env.NODE_ENV}-`,
    host: 'statsd-frontend.myntra.com',
    port: 8125,
    isEnabled: true,
    samplingRate: 1
  }
};

const STATICMETA = {
  root: 'http://myntra.myntassets.com/',
  defaultRoot: 'http://myntra.myntassets.com/',
  secureRoot: 'https://constant.myntassets.com/',
  cloudinaryRoot: 'http://assets.myntassets.com/',
  cloudinarySecureRoot: 'https://assets.myntassets.com/'
};

const CONFIGURATION = {
  serverSideRenderingEnabled: false,
  webrender: {
    root: 'http://webrender.myntra.com'
  }
};

let config = ROUTES;

function init() {
  try {
    if (ENV === 'release') {
      config = require('./dockins').ROUTES;
    }
  } catch (err) {
    // console.log(ENV, ' environment not found. Picking default Environment');
  }
}

init();

export default (route) => {
  const isBrowser = (typeof window !== 'undefined');
  return ((isBrowser ? '/' : (config[route].root || config.ROOT)) + ((isBrowser) ? 'web/' : '') + (config[route].path || ''));
};

export const getFilteredSearchBase = () => config.filteredSearch.root + config.filteredSearch.path;

export { ROUTES, CONFIGURATION, STATICMETA };

export function get(key) {
  return CONFIGURATION[key];
}

export function root() {
  return config.ROOT;
}

export function getRouteConfig(key) {
  return config[key];
}

export function secureRoot() {
  return config.SECURE_ROOT;
}
