const ROOT = `http://${process.env.CLUSTER_NAME}${process.env.BROWSEHAPROXY}${process.env.UI_SUFFIX}/`;
const SECURE_ROOT = `https://${process.env.CLUSTER_NAME}${process.env.BROWSEHAPROXY}${process.env.UI_SUFFIX}/`;

const ROUTES = {
  ROOT,
  SECURE_ROOT,
  biro: {
    root: 'http://d7devapi.myntra.com/',
    path: 'biro/desktop'
  },
  mobileVerify: {
    root: 'http://d7knuth.myntra.com/',
    path: 'user/mobile/addphone?verify=true'
  },
  otpVerify: {
    root: 'http://d7knuth.myntra.com/',
    path: 'user/mobile/verifyphone'
  },
  isNewUser: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v1/user/status'
  },
  locationContext: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v1/user/locationContext'
  },
  filteredSearch: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v2/search'
  },
  cart: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v1/cart/default/add'
  },
  cartSummary: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v1/cart/default/summary'
  },
  wishlist: {
    root: 'http://d7absolut.myntra.com/',
    path: 'myntra-absolut-service/absolut/wishlist'
  },
  wishlistNew: {
    root: 'http://d7devapi.myntra.com/',
    path: 'collections/me/wishlist'
  },
  wishlistApify: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v1/user/wishlist'
  },
  wishlistApifyV2: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v2/user/wishlist'
  },
  mfg: {
    root: 'http://d7devapi.myntra.com/',
    path: 'mfg/v1'
  },
  pdp: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v2/product/'
  },
  pdpV1: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v1/product/'
  },
  crossSell: {
    root: 'http://d7knuth.myntra.com/v2/product/',
    path: 'cross-sell/'
  },
  serviceability: {
    root: 'http://d7knuth.myntra.com/',
    path: 'serviceability/check'
  },
  serviceabilityV2: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v2/serviceability/check'
  },
  serviceabilityV3: {
    root: 'http://d7knuth.myntra.com/',
    path: 'v3/serviceability/check'
  },
  topnav: {
    root: 'http://d7devapi.myntra.com/',
    path: 'config/nav/v1/web/top'
  },
  search: {
    root: 'http://d7forceservice.mpreprod.myntra.com/',
    path: 'force-service/v1/suggest/query'
  },
  coupons: {
    root: 'http://d7knuth.myntra.com/v2/product/',
    path: 'offers'
  },
  recommendations: {
    root: 'http://d7knuth.myntra.com/v2/product/',
    path: 'related/'
  },
  sizeReco: {
    root: 'http://d7knuth.myntra.com',
    path: ''
  },
  idp: {
    root: `http://${process.env.CLUSTER_NAME}${process.env.BROWSEHAPROXY}${process.env.UI_SUFFIX}/`,
    path: 'myntapi/idp/auth/'
  },
  instrumentation: {
    debug: true
  },
  admission: {
    root: 'http://d7devapi.myntra.com/',
    path: 'admission'
  },
  statsd: {
    host: '192.168.20.53'
  },
  signin: {
    root: 'http://d7knuth.myntra.com/',
    path: 'auth/v1/login'
  },
  signup: {
    root: 'http://d7knuth.myntra.com/',
    path: 'auth/v1/signup'
  },
  google: {
    root: 'http://d7knuth.myntra.com/',
    path: 'auth/v1/glogin'
  },
  facebook: {
    root: 'http://d7knuth.myntra.com/',
    path: 'auth/v1/fblogin'
  },
  forgotPassword: {
    root: 'http://d7knuth.myntra.com/',
    path: 'auth/v1/forgetpassword'
  },
  signout: {
    root: 'http://d7knuth.myntra.com/',
    path: 'auth/v1/signout'
  },
  signoutOld: {
    path: 'myntapi/idp/auth/signout'
  },
  reviews: {
    root: 'http://d7devapi.myntra.com/',
    path: 'v1/reviews/product/'
  },
  reviewsByID: {
    root: 'http://d7devapi.myntra.com/',
    path: 'v1/reviews/review/'
  },
  reviewsByBatch: {
    root: 'http://d7devapi.myntra.com/',
    path: 'v1/reviews/batch/'
  },
  reviewsSetVote: {
    root: 'http://d7devapi.myntra.com/',
    path: 'v1/reviews/vote'
  },
  userData: {
    path: 'beacon/user-data'
  }
};

export { ROUTES };
