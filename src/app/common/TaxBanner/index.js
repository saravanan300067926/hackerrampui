import React from 'react';
import styles from './TaxBanner.css';
import get from 'lodash/get';
import { isBrowser } from '../../utils';

const kvPairs = isBrowser() ? get(window, '__myx_kvpairs__', {}) : {};
const taxBannerData = kvPairs['web.taxBanner'];

const TaxBanner = () => (
  get(taxBannerData, 'desktop.enable', false)
    ? (
    <div
      className={styles.taxBanner}
      style={{ backgroundColor: get(taxBannerData, 'desktop.bgColor', '#03a685') }}>
      {get(taxBannerData, 'desktop.info', '')}
    </div>
    ) : null
);

export default TaxBanner;
