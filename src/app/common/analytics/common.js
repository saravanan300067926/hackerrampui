import get from "lodash/get";

const constructDataSet = data => {
    if (data) {
      return {
        name: data.fromType,
        data_set: {
          data: [
            {
              entity_type: data.type,
              entity_optional_attributes: {
                discountedPrice:
                get(data, 'discountedPrice') ||
                  (get(data, 'selectedSeller') && get(data, 'selectedSeller.discountedPrice')),
                price: data.mrp,
                seller_partner_id:
                get(data, 'sellerPartnerId') ||
                  (get(data, 'selectedSeller') && get(data, 'selectedSeller.sellerPartnerId')),
                "sku-id": data.skuId,
                "storefront-id": data.storeFrontId,
                v_position: data.vPos,
                h_position: data.hPos,
                url: data.url
              },
              entity_id: data.id
            }
          ]
        }
      };
    }
    return { name: "", data_set: { data: [{}] } };
  };
  
  const constructDataSetForScreen = data =>
    constructDataSet(data).data_set.data[0];
  
  export { constructDataSet, constructDataSetForScreen };
  