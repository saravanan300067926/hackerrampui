// Checkout team to change import if needed.
import Client from '../../../services';
import config from '../../../config';
import get from "lodash/get";

function hasNoResults(body) {
  const errorCode = get(body, "code", "");
  const errorMessage = get(body, "message", "");
  return errorCode !== "" && errorMessage !== "";
}

function hasEmptyBody(body) {
  return Object.entries(body).length === 0;
}

// Checkout team to change apiCall logic if required.
/**
 * Wrapper around m-agent to provide a typed API for easy autocomplete.
 *
 * [API Contract]{@link https://confluence.myntracorp.com/confluence/pages/viewpage.action?pageId=113673979}
 * @param {Object.<string, Object>} request
 * @return {Promise<{pincode: string, source: string, ttl: string}>}
 */
function locationAPI(request) {
  return new Promise((resolve, reject) => {
    Client
      .post(config('locationContext'), request)
      .send(request)
      .end((err, res) => {
        const body = get(res, "body", {});
        if (
          Client.errorHandler(err, res) ||
          hasEmptyBody(body) ||
          hasNoResults(body)
        ) {
          reject(err);
        } else {
          resolve(body);
        }
      });
  });
}

function singletonAPI(API) {
  let inProgress = false;
  return function(request) {
    if (!inProgress) {
      inProgress = true;
      return API(request)
        .then(response => {
          inProgress = false;
          return response;
        })
        .catch(error => {
          inProgress = false;
          throw error;
        });
    } else {
      return Promise.reject("Request in Progress");
    }
  };
}

export default singletonAPI(locationAPI);
