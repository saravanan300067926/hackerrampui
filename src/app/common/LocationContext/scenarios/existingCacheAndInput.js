import { sourceCookie, ulcCookie } from "../data/Cookies";
import locationAPI from "../api/locationAPI";

// CASE 4
function existingCacheAndInput(pincode) {
  // ulcCookie, sourceCookie
  const previousPincode = ulcCookie.get().pincode;
  const previousSource = sourceCookie.get().source;
  locationAPI({
    previousContext: {
      pincode: previousPincode,
      source: previousSource
    },
    currentContext: {
      addressId: "",
      pincodeSource: [
        {
          pincode: pincode,
          source: "USER"
        }
      ]
    }
  })
    .then(response => {
      ulcCookie.set(response.pincode);
      sourceCookie.set(response.ttl, response.source);
    })
    .catch(() => {});
}

export default existingCacheAndInput;
