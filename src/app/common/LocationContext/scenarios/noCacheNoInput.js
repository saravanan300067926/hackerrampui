import locationAPI from "../api/locationAPI";
import { sourceCookie, ulcCookie } from "../data/Cookies";

// CASE 1
function noCacheNoInput() {
  locationAPI({
    previousContext: {
      pincode: "",
      source: ""
    },
    currentContext: null
  })
    .then(response => {
      ulcCookie.set(response.pincode);
      sourceCookie.set(response.ttl, response.source);
    })
    .catch(() => {});
}

export default noCacheNoInput;
