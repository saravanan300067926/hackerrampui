import locationAPI from "../api/locationAPI";
import { sourceCookie, ulcCookie } from "../data/Cookies";

//CASE 2
function noCacheButInput(pincode) {
  locationAPI({
    previousContext: {
      pincode: "",
      source: ""
    },
    currentContext: {
      addressId: "",
      pincodeSource: [
        {
          pincode: pincode,
          source: "USER"
        }
      ]
    }
  })
    .then(response => {
      ulcCookie.set(response.pincode);
      sourceCookie.set(response.ttl, response.source);
    })
    .catch(() => {});
}

export default noCacheButInput;
