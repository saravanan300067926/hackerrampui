import locationAPI from "../api/locationAPI";
import { sourceCookie, ulcCookie } from "../data/Cookies";

// CASE 3
function expiredCacheNoInput() {
  // sourceCookie, ulcCookie
  const previousPincode = ulcCookie.get().pincode;
  const previousSource = sourceCookie.get().source;
  locationAPI({
    previousContext: {
      pincode: previousPincode,
      source: previousSource
    },
    currentContext: null
  })
    .then(response => {
      ulcCookie.set(response.pincode);
      sourceCookie.set(response.ttl, response.source);
    })
    .catch(() => {});
}

export default expiredCacheNoInput;
