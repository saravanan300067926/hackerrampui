// IMPORTS for documentation
import { sourceCookie, ulcCookie } from "./data/Cookies";
import locationAPI from "./api/locationAPI";
import locationMachine from "./data/Machine";
import { isApp, isIosApp } from '../../utils/isApp';

// Add device check middleware which sets user location cookies if webview
/**
 * Assumes that your cookies object has a get/set function,
 * that returns/sets the cookie value for a cookie name.
 *
 * See also: [Confluence]{@link https://confluence.myntracorp.com/confluence/display/STOR/Location+Context+Feature},
 * {@link locationMachine}, {@link locationAPI}, {@link ulcCookie}, {@link sourceCookie}
 *
 * @example
 * UseCases
 * RoutineUpdation: updateLocationContext()
 * UserInput: updateLocationContext(pincode)
 * UserClear: updateLocationContext("", true)
 */
export default function updateLocationContext(pincode = "", manualClear = false) {
  // Do not trigger for webview, rely on app data to be correct.
  if (isApp() || isIosApp()) {
    return;
  }
  locationMachine(pincode, manualClear);
}
