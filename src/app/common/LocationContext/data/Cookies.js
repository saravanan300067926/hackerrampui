// Checkout team needs to replace this import for use in their code.
import cookies from "../../../utils/cookies";
import get from "lodash/get";

// If ever changed, also change getPincodeAndSource in src/app/services/header.js
// Add addressId regex here when that is implemented
const ulcRegex = {
  pincode: /pincode:(\d{6})/i,
  // IP, GEO, USER
  source: /source:(\w{2,4})/i,
  expiry: /expiry:(\d{13})/i
};

// https://github.com/sindresorhus/is-regexp
function isRegex(input) {
  return Object.prototype.toString.call(input) === "[object RegExp]";
}

/**
 * Assumes that your cookies object has a get function,
 * that returns the cookie value for a cookie name.
 */
const Cookie = {
  set(name, object) {
    const strings = Object.entries(object).map(
      ([key, value]) => `${key}:${value}`
    );
    cookies.set(name, strings.join("|"), 3650);
  },

  /**
   * For a given object of name: regex,
   * (where the value you want to extract is the only group),
   * extracts them from the cookie given by cookieName
   * and returns their values in an object.
   *
   * @example const { source, expiry } = get("mynt-loc-src", {
   *   source: /source:(\w{2,4})/i,
   *   expiry: /expiry:(\d{13})/i
   * })
   *
   * @param {string} cookieName
   * @param {Object<string, RegExp>} nameToRegexMap
   * @return {Object<string, string>} { name: regexMatch || "" }
   */
  get(cookieName, nameToRegexMap) {
    const rawCookie = cookies.get(cookieName) || "";
    const cookie = decodeURIComponent(rawCookie);
    const results = {};

    Object.entries(nameToRegexMap).forEach(([name, regex]) => {
      const result = isRegex(regex) && regex.exec(cookie);
      results[name] = get(result, "1", "");
    });

    return results;
  }
};

/**
 * Uses "mynt-ulc-api" cookie.
 * We do not have addressId so currently we won't set that value
 * Format = "pincode:<pincode>"
 * Example = "pincode%3A560008"
 *
 */
const ulcCookie = {
  name: "mynt-ulc-api",
  /**
   * @return {{pincode: string}}
   */
  get() {
    return Cookie.get(this.name, {
      pincode: ulcRegex.pincode
    });
  },
  /**
   * @param {string} pincode
   */
  set(pincode) {
    Cookie.set(this.name, { pincode });
  }
};

function getTimeStamp(seconds) {
  return Date.now() + parseInt(seconds) * 1000;
}

/**
 * Uses "mynt-loc-src" cookie.
 * Format = "expiry:<expiryTimeStamp>|source:<source>"
 * Example = expiry:1590653518924|source:IP
 */
const sourceCookie = {
  name: "mynt-loc-src",
  /**
   * @return {{expiry: string, source: string}}
   */
  get() {
    return Cookie.get(this.name, {
      expiry: ulcRegex.expiry,
      source: ulcRegex.source
    });
  },

  /**
   *  @param {string} expiry
   *  @param {string} source
   */
  set(expiry, source) {
    Cookie.set(this.name, {
      expiry: getTimeStamp(expiry),
      source
    });
  }
};

export { ulcRegex, sourceCookie, ulcCookie, Cookie };
