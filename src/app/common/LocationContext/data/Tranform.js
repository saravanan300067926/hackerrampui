import { sourceCookie } from "./Cookies";

export const Cache = {
  VALID: "valid",
  EXPIRED: "expired",
  NULL: "null"
};
export const Input = {
  PINCODE: "pincode",
  NULL: "null"
};

export function scenario(cache, input) {
  return `${cache}, ${input}`;
}

function expired(timestampString) {
  const timestamp = parseInt(timestampString) || 0;
  return timestamp < Date.now();
}

function getCacheEnum(manualClear) {
  const { source, expiry } = sourceCookie.get();
  if (expiry === "") {
    return Cache.NULL;
  } else if (manualClear || (source !== "USER" && expired(expiry))) {
    return Cache.EXPIRED;
  } else {
    return Cache.VALID;
  }
}

function getInputEnum(input) {
  if (input === "") {
    return Input.NULL;
  } else {
    return Input.PINCODE;
  }
}

export function getEnum(input, manualClear) {
  return [
    getCacheEnum(manualClear),
    getInputEnum(input)
  ];
}
