import { Cache, Input, getEnum, scenario } from "./Tranform";
import existingCacheAndInput from "../scenarios/existingCacheAndInput";
import expiredCacheNoInput from "../scenarios/expiredCacheNoInput";
import noCacheButInput from "../scenarios/noCacheButInput";
import noCacheNoInput from "../scenarios/noCacheNoInput";

/**
 * Explanation: [Confluence]{@link https://confluence.myntracorp.com/confluence/display/STOR/Location+Context+Feature}
 *
 * CASE 1: noCacheNoInput
 *
 * CASE 2: noCacheButInput
 *
 * CASE 3: expiredCacheNoInput
 *
 * CASE 4: existingCacheAndInput
 *
 * @param {string} pincode
 * @param {boolean} manualClear
 */
export default function machine(pincode, manualClear) {
  const [cacheEnum, inputEnum] = getEnum(pincode, manualClear);
  switch (scenario(cacheEnum, inputEnum)) {
    // CASE 1
    case scenario(Cache.NULL, Input.NULL):
      noCacheNoInput();
      break;

    // CASE 2
    case scenario(Cache.NULL, Input.PINCODE):
      noCacheButInput(pincode);
      break;

    // CASE 3
    case scenario(Cache.EXPIRED, Input.NULL):
      expiredCacheNoInput();
      break;

    // CASE 4
    case scenario(Cache.VALID, Input.PINCODE):
    case scenario(Cache.EXPIRED, Input.PINCODE):
      existingCacheAndInput(pincode);
      break;

    case scenario(Cache.VALID, Input.NULL):
    default:
      break;
  }
}
