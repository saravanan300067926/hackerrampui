import React from 'react';
import styles from './xcelerator.css';

const XceleratorTag = ({ tag, position, page }) => {
  const klassName =
    position === 'onImage'
      ? `${page}XceleratorImageTag`
      : `${page}XceleratorInfoTag`;
  return <div className={styles[klassName]}>{tag}</div>;
};

XceleratorTag.propTypes = {
  tag: React.PropTypes.string.isRequired,
  position: React.PropTypes.string.isRequired,
  page: React.PropTypes.string.isRequired
};

export default XceleratorTag;
