import { getFeatures } from './utils';

const constants = {
  passwordLength: 8,
  passwordSpecialCharsEnabled: getFeatures('password.specialchar.enable') || false,
  passwordUpperCaseEnabled: true,
  passwordnumberEnabled: true
};

export default (key) => constants[key];
