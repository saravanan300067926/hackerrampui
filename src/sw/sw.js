import sw from 'sw-toolbox';
import assetMap from "../../webpack-assets.json";
import 'core-js/fn/array/includes';

let assets = [];

const excludedChunks = [
    'd-fitassistdetails',
    'd-globalleaderboard',
    'd-groupdashboard',
    'd-joingroup',
    'd-preeors',
    'd-slotinfoview',
    'sizechart',
    'd-appsizechart',
    'd-virtualtryon'
];

self.addEventListener('activate', event => {
  event.waitUntil(
    self.caches.keys().then(
      cacheNames =>
        Promise.all(
          cacheNames.map(cacheName => self.caches.delete(cacheName))
        )
    )
  );
});

for (let key of Object.keys(assetMap)) {
  if (!excludedChunks.includes(key)) {
    let chunk = assetMap[key];
    for (let asset of Object.keys(chunk)) {
      if (asset !== "text") {
        assets.push(chunk[asset]);
      }
    }
  }
}

sw.options.successResponses = /^200$/;

sw.precache([
  'https://constant.myntassets.com/web/assets/img/favicon.ico',
  'https://constant.myntassets.com/www/fonts/WhitneyHTF-Book.woff',
  'https://constant.myntassets.com/www/fonts/WhitneyHTF-SemiBold.woff',
  ...assets
]);

sw.router.get('/(.*)', sw.cacheFirst, {
  origin: 'constant.myntassets.com',
  cache: { name: 'static ', maxAgeSeconds: 86400 }
});

sw.router.get('/(.*)', sw.cacheFirst, {
  origin: 'assets.myntassets.com',
  cache: { name: 'images', maxEntries: 200 }
});

sw.router.get('/(.*)', sw.cacheFirst, {
  origin: 'constant.myntassets.com',
  cache: { name: 'images', maxEntries: 200 }
});

sw.router.get('/(.*)', sw.fastest, {
  origin: 'apis.google.com',
  cache: {
    name: 'tracker',
    maxAgeSeconds: 60 * 60 * 24
  }
});

sw.router.get('/(.*)', sw.fastest, {
  origin: 'www.googletagmanager.com',
  cache: {
    name: 'tracker',
    maxAgeSeconds: 60 * 60 * 24
  }
});

sw.router.get('/(.*)', sw.fastest, {
  origin: 'www.google-analytics.com',
  cache: {
    name: 'tracker',
    maxAgeSeconds: 60 * 60 * 24
  }
});

sw.router.get('/(.*)', sw.fastest, {
  origin: 'connect.facebook.net',
  cache: {
    name: 'tracker',
    maxAgeSeconds: 60 * 60 * 24
  }
});

self.importScripts('https://cdn.izooto.com/scripts/workers/2d34f47ca3a13cbc90559ae77170feca968c14e4.js');

