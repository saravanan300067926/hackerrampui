import { getFingerPrintHeaders, getLocationHeaders } from '../app/services/header';

const express = require('express');
const app = express();
const env = process.env.NODE_SUBENV || 'production';
const router = express.Router();

import config from '../app/config';
import client from '../app/services';
import at from 'v-at';
import { ROUTES } from '../app/config';
import Cookies from '../utils/cookies';
const parallel = require('async/parallel');
const reflect = require('async/reflect');
// import { groupsMeSample, rewardsMeSample, spendsMeSample, campaignRewards } from './sampleResp';
const getDefaultErrorMessage = function() {
  return 'Oops! Something unexpected happened. Please try again in some time.';
}
const logError = function(action, message, err) {
  message = message || 'Error';
  err = err || '';
  console.error(`${new Date()} Error: [MFG-${action}] ${message}.${err}`);
}

const logInfo = function(action, message) {}

const getXidFromCookie = function(req, res) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  return cookies.get('xid');
}

const getAbTestsFromCookie = function(req, res) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  logInfo('ABTests-Cookie', cookies.get('_mxab_'));
  return cookies.get('_mxab_');
}

const loginCheck = function(req, res, action, message) {
  message = message || 'continue';
  if (at(req, 'myx.session.isLoggedIn')) {
    return true;
  }
  logError(action, 'Login check failed. User not logged in.');
  sendErrorResponse(res, 403, '403', `Not authorized. Please login to ${message} `);
}

const tokenCheck = function(req, res, action) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  if(req.header('X-CSRF-TOKEN') === cookies.get('_xsrf')) {
    return true;
  }
  logError(action, 'Invalid session. Token mismatch');
  sendErrorResponse(res, 403, '403', 'Invalid session. Please login and try again');
}

const getEnvironmentPrefix = function() {
    if (env === 'production' || env === 'development') {
        return '';
    } else if (/fox/.test(env)) {
        return 'fox-';
    }
    return `${env}-`;
}

const getHeaders = function(req, res) {
  let xid = getXidFromCookie(req, res);
  xid = xid || '';
  const cookie = Cookies(req, res);
  const headers = {
    cookie: `${getEnvironmentPrefix()}xid=` + xid + ';',
    'X-Forwarded-For': req.header('X-Forwarded-For') || '',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  if (xid) {
    headers.xid = xid;
  }
  const cookieAbTest = getAbTestsFromCookie(req, res);
  if (cookieAbTest) {
    headers['x-myntra-abtest'] = cookieAbTest;
  }
  return headers;
}

const sendErrorResponse = function(res, responseCode, statusCode, message) {
  res.status(responseCode || 200).send({
    status: {
      statusCode: statusCode || '200',
      statusType: 'ERROR',
      statusMessage: message || getDefaultErrorMessage()
    }
  });
}

const handleResponse = function(action, serviceResponseBody, serviceResponseCode, res) {
  if(serviceResponseCode === 401 || serviceResponseCode === 403) {
    logError(action, 'Auth error from service due to invalid xid');
    sendErrorResponse(res, 403, '403', `Not authorized. Please login to continue `);
    return;
  }
  if(serviceResponseCode !== 200) {
    logError(action, 'Response code is not 200');
    sendErrorResponse(res, 200, '200', 'Error from service - Response code is not 200');
    return;
  }
  res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.setHeader('Expires', '-1');
  res.setHeader('Pragma', 'no-cache');
  res.status(200).send(serviceResponseBody);
}

const getLeaderBoardData = function(req, res, callback) {
  const limit = at(req, 'query.limit') || 100;
  const offset = at(req, 'query.offset') || 0;
  logInfo('Get-Global-Leaderboard', `${config('mfg')}/campaign/leaderboard?limit=${limit}&offset=${offset}`);
  client.get(`${config('mfg')}/campaign/leaderboard?limit=${limit}&offset=${offset}`, getHeaders(req, res)).end((error,response) => {
    if (error || at(response, 'body.meta.code') !== 200) {
      logError('Get-Global-Leaderboard', 'Error from service ' + (error || 'Response code is not 200'));
      callback(null, at(response, 'body.data'));
      // callback({ context: 'leaderBoardData', error });
    } else {
      callback(null, at(response, 'body.data'));
    }
  });
}

const getGroupDashBoardData = function(req, res, callback) {
  client.get(`${config('mfg')}/group/user/me`, getHeaders(req, res)).end((error,response) => {
    if (error || at(response, 'body.meta.code') !== 200) {
      logError('GetGroupDashboard', 'Error from service ' + (error || 'Response code is not 200'));
      callback(null, at(response, 'body.data'));
    } else {
      callback(null, at(response, 'body.data'));
    }
  });
}

const getOffersData = function(req, res, callback, getOffersData) {
  const offersDataRequested = at(req, 'query.offersData');
  logInfo('Get-Offers-Data', `${config('mfg')}/campaign/offer`);
  if (offersDataRequested || getOffersData) {
    client.get(`${config('mfg')}/campaign/offer`, getHeaders(req, res)).end((error,response) => {
      if (error || at(response, 'body.meta.code') !== 200) {
        logError('Get-Offers-Data', 'Error from service ' + (error || 'Response code is not 200'));
        callback({ context: 'offersData', error });
      } else {
        callback(null, at(response, 'body.data'));
      }
    });
  } else {
    callback(null, {});
  }
}

router.get('/leaderboard/global', (req, res) => {
  parallel([
    reflect(function(callback) {
      getLeaderBoardData(req, res, callback)
    }),
    reflect(function(callback) {
      getOffersData(req, res, callback)
    })
  ], function(err, results) {
      const resp = {};
      if (results[0].error && results[1].error) {
        sendErrorResponse(res, 200, '200', 'Error while getting leader board and offers data');
        return;
      }
      if (results[0].value) {
        resp.leaderBoardData = results[0].value;
      }
      if (results[1].value) {
        resp.offersData = results[1].value;
      }
      handleResponse('Get-Global-Leaderboard', resp, 200, res);
  });
});

router.get('/leaderboard/group', (req, res) => {
  if (loginCheck(req, res, 'GetGroupDashboard', 'getting details for group dashboard')) {
    parallel([
      reflect(function(callback) {
        getGroupDashBoardData(req, res, callback)
      }),
      reflect(function(callback) {
        getOffersData(req, res, callback, true)
      })
    ], function(err, results) {
        const resp = {};
        if (results[0].error && results[1].error) {
          sendErrorResponse(res, 200, '200', 'Error while getting group dashboard and offers data');
          return;
        }
        if (results[0].value) {
          resp.dashBoardData = results[0].value;
        }
        if (results[1].value) {
          resp.offersData = results[1].value;
        }
        handleResponse('GetGroupDashboard', resp, 200, res);
    });
  }
});

const getMyGroupDetails = function(req, res, callback) {
  logInfo('PrepJoin-MyGroup', `${config('mfg')}/group/user/me`);
  client.get(`${config('mfg')}/group/user/me`, getHeaders(req, res)).end((error,response) => {
    if (error || at(response, 'body.meta.code') !== 200) {
      logError('PrepJoin-MyGroup', 'Error from service ' + (error || 'Response code is not 200'));
      callback({ context: 'MyGroup', error });
    } else {
      callback(null, at(response, 'body.data'));
    }
  });
};

const getJoiningGroupDetails = function(req, res, invitecode, callback) {
  logInfo('PrepJoin-MyGroup', `${config('mfg')}/group/${invitecode}`);
  client.get(`${config('mfg')}/group/${invitecode}`, getHeaders(req, res)).end((error,response) => {
    if (at(response, 'body.meta.code') === 400) {
      callback(null, {});
    } else if (error) {
      logError('PrepJoin-JoiningGroup', 'Error from service ' + error);
      callback({ context: 'JoiningGroup', error });
    } else if (at(response, 'body.meta.code') !== 200) {
      logError('PrepJoin-JoiningGroup', 'Error from service - Response code is not 400 or 200');
      callback({ context: 'JoiningGroup', error: 'Response code is not 400 or 200' });
    } else {
      callback(null, at(response, 'body.data'));
    }
  });
};

router.get('/preparejoin', (req, res) => {
  const inviteCode = at(req, 'query.invitecode');
  if (inviteCode) {
    if (loginCheck(req, res, 'PrepareJoin', 'preparing to join group')) {
      parallel({
        myGroup: function(callback) {
          getMyGroupDetails(req, res, callback)
        },
        joiningGroup: function(callback) {
          getJoiningGroupDetails(req, res, inviteCode, callback)
        }
      }, function(err, results) {
        if (err) {
          sendErrorResponse(res, 200, '200', 'Error from service' + err);
        } else if (results && results.myGroup && results.joiningGroup) {
          const response = {
            "myGroup" : {
              "id": at(results, 'myGroup.groupId') || '',
              "name": at(results, 'myGroup.groupName') || ''
            },
            "joiningGroup" : {
              "id": at(results, 'joiningGroup.groupId') || '',
              "name": at(results, 'joiningGroup.groupName') || '',
              "currentSize": at(results, 'joiningGroup.actualGroupSize') || 0,
              "maxSize": at(results, 'joiningGroup.maxGroupSize') || 20,
              "icon": at(results, 'joiningGroup.groupImage') || '',
              "campaignStatus": at(results, 'joiningGroup.campaign.status') || '',
              "frozen": at(results, 'joiningGroup.isFrozen') || false
            }
          }
          handleResponse('PrepareJoin', response, 200, res);
        }
      });
    }
  } else {
    sendErrorResponse(res, 200, '200', 'No invite code');
  }
});

router.put('/joingroup', (req, res) => {
  if(loginCheck(req, res, 'JoinGroup', 'join group') && tokenCheck(req, res, 'JoinGroup')) {
    const data = req.body;
    if (data && data.inviteCode) {
      logInfo('JoinGroup - URL', `${config('mfg')}/group/${data.inviteCode}/join`);
      client.put(`${config('mfg')}/group/${data.inviteCode}/join`, {userName: data.userName}, getHeaders(req, res)).end((error,response) => {
        if (error) {
          logError('JoinGroup', 'Error from service ' + error);
          sendErrorResponse(res);
        } else {
          handleResponse('JoinGroup', at(response, 'body.data'), at(response, 'body.meta.code'), res);
        }
      });
    } else {
      const errorMessage = 'Insufficient data. Required data (groupId) is missing';
      logError('JoinGroup', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  }
});

router.put('/checkin', (req, res) => {
  if(loginCheck(req, res, 'Checkin', 'checkin to group') && tokenCheck(req, res, 'Checkin')) {
    const data = req.body;
    if (data && data.groupId) {
      logInfo('Checkin - URL', `${config('mfg')}/group/${data.groupId}/checkin`);
      client.put(`${config('mfg')}/group/${data.groupId}/checkin`, {}, getHeaders(req, res)).end((error,response) => {
        if (error) {
          logError('Checkin', 'Error from service ' + error);
          sendErrorResponse(res);
        } else {
          handleResponse('Checkin', at(response, 'body.data'), at(response, 'body.meta.code'), res);
        }
      });
    } else {
      const errorMessage = 'Insufficient data. Required data (groupId) is missing';
      logError('Checkin', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  }
});


/**
 * fetch users rewards
 */
const getUserRewards = function(req, res, callback) {
  logInfo('getUserRewards', `${config('mfg')}/rewards/me`);
  return client.get(`${config('mfg')}/rewards/me`, getHeaders(req, res));
}

/**
 * Check if user is part of any group
 */
const checkUserGroup = function(req, res, callback) {
  logInfo('checkUserGroup', `${config('mfg')}/group/user/me`);
  return client.get(`${config('mfg')}/group/user/me`, getHeaders(req, res));
}

/**
 * Fetch campaign main offer
 */
const getCampaignOffer = function(req, res, callback) {
  logInfo('getRewardsMainOffer', `${config('mfg')}/campaign/offer`);
  return client.get(`${config('mfg')}/campaign/offer`, getHeaders(req, res));
}

/**
 * Get info about user spending pattern
 */
const getUserSpends = function(req, res, callback) {
  logInfo('getUserSpends', `${config('mfg')}/spends/me`);
  return client.get(`${config('mfg')}/spends/me`, getHeaders(req, res));
}

router.get('/rewards/data', (req, res) => {
  getRewardsAndSpends(req, res);
});


function getRewardsAndSpends(req, res) {
  parallel([
    reflect(function(callback) {
      getUserSpends(req, res, callback)
      .end((error,response) => {
        if (error || at(response, 'body.meta.code') !== 200) {
          logError(' /spends/me ', 'Error from service ' + (error || 'Response code is not 200'));
          callback({ context: 'rewards', error });
          // callback(null, spendsMeSample.data);
        } else {
          callback(null, at(response, 'body.data'));
          // callback(null, spendsMeSample.data);
        }
      });
    }),
    reflect(function(callback) {
      getUserRewards(req, res, callback)
      .end((error,response) => {
        if (error || at(response, 'body.meta.code') !== 200) {
          logError('/rewards/me', 'Error from service ' + (error || 'Response code is not 200'));
          callback({ context: 'rewards', error });
          // callback(null, rewardsMeSample.data);
        } else {
          callback(null, at(response, 'body.data'));
          // callback(null, rewardsMeSample.data);
        }
      });
    }),
    reflect(function(callback) {
      getCampaignOffer(req, res, callback)
      .end((error,response) => {
        if (error || at(response, 'body.meta.code') !== 200) {
          logError(' /campaign/offer ', 'Error from service ' + (error || 'Response code is not 200'));
          callback({ context: 'rewards', error });
          // callback(null, campaignRewards.data);
        } else {
          callback(null, at(response, 'body.data'));
          // callback(null, campaignRewards.data);
        }
      });
    })
  ], function(err, results) {
      const resp = {};
      if (results[0].error && results[1].error && results[2].error) {
        sendErrorResponse(res, 200, '200', 'Error while getting rewards and spends data');
        return;
      }
      if (results[0].value) {
        resp.spends = results[0].value;
      }
      if (results[1].value) {
        resp.rewards = results[1].value;
      }
      if (results[2].value) {
        resp.offer = results[2].value;
      }

      handleResponse('get /rewards/user ', resp, 200, res);
  });
  }

export default router;
