import { getFingerPrintHeaders, getLocationHeaders } from '../app/services/header';

const express = require('express');
const router = express.Router();
import config from '../app/config';
import Client from '../app/services';

router.get('/product/:style', (req, res) => {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  const handleAuthTokenUpdate = require('@myntra/myx/lib/handleAuthTokenUpdate');
  const reviewsHeaders = {
    'at': cookies.get('at') || 'dummyvalue',
    ...getLocationHeaders(cookies),
    ...getFingerPrintHeaders(req, cookies)
  };
  const rtToken = cookies.get('rt')
  if (rtToken) {
    reviewsHeaders['rt'] = rtToken
  }
  const style = req.params.style;
  const page = req.query.page || 1;
  const sort = req.query.sort || 0;
  const rating = req.query.rating || 0;
  const size = req.query.size || 12;

  const url = `${config('reviews')}${style}?size=${size}&sort=${sort}&rating=${rating}&page=${page}&includeMetaData=true`;

  Client.get(url, reviewsHeaders).end((err, response) => {
    handleAuthTokenUpdate(req, res, response);
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    res.send(200, response.body);
  });

});

router.get('/review/:reviewID', (req, res) => {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  const handleAuthTokenUpdate = require('@myntra/myx/lib/handleAuthTokenUpdate');

  const reviewsHeaders = {
    'at': cookies.get('at') || 'dummyvalue',
    ...getLocationHeaders(cookies)
  };
  const rtToken = cookies.get('rt')
  if (rtToken) {
    reviewsHeaders['rt'] = rtToken
  }
  const url = `${config('reviewsByID')}${req.params.reviewID}`;
  Client.get(url, reviewsHeaders).end((err, response) => {
    handleAuthTokenUpdate(req, res, response);
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    res.send(200, response.body);
  });

});

router.get('/batch/:style', (req, res) => {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  const handleAuthTokenUpdate = require('@myntra/myx/lib/handleAuthTokenUpdate');
  const reviewsHeaders = {
    'at': cookies.get('at') || 'dummyvalue',
    ...getLocationHeaders(cookies)
  };
  const rtToken = cookies.get('rt')
  if (rtToken) {
    reviewsHeaders['rt'] = rtToken
  }
  const page = req.query.page || 1;
  const size = req.query.size || 5;
  const style = req.params.style;

  const url = `${config('reviews')}${style}/images?size=${size}&page=${page}`;
  Client.get(url, reviewsHeaders).end((err, response) => {
    handleAuthTokenUpdate(req, res, response);
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    res.send(200, response.body);
  });
});

router.post('/vote', (req, res) => {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  const handleAuthTokenUpdate = require('@myntra/myx/lib/handleAuthTokenUpdate');
  const reviewsHeaders = {
    'at': cookies.get('at') || 'dummyvalue',
    'Content-Type': 'application/json; charset=utf-8',
    ...getLocationHeaders(cookies)
  };
  const rtToken = cookies.get('rt')
  if (rtToken) {
    reviewsHeaders['rt'] = rtToken
  }
  const payload = {
    reviewId: req.body.reviewId,
    isUpvote: req.body.isUpvote
  }
  Client.post(config('reviewsSetVote'), payload, reviewsHeaders).end((err, response) => {
    handleAuthTokenUpdate(req, res, response);
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    res.send(200, response.body);
  });
});

export default router;
