var url = require('url');

var skipConversion = function(req) {
  var skipConversionPaths = ['/web/', '/shoppinggroups/join/'];
  var i;
  for (i = 0; i < skipConversionPaths.length; i++) {
    if (req.path && req.path.indexOf(skipConversionPaths[i]) > -1) {
      return true;
    }
  }
  return false;
}

module.exports = function lowercasePaths() {
  return function _lowercasePaths(req, res, next) {
    if (!skipConversion(req) &&
      (req.path.toLowerCase() !== req.path)) {
      var parsedUrl = url.parse(req.originalUrl);
      parsedUrl.pathname = parsedUrl.pathname.toLowerCase();
      res.redirect(301, url.format(parsedUrl));
    } else {
      next();
    }
  };
}
