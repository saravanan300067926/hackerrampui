import get from 'lodash/get';
const express = require('express');
const env = process.env.NODE_SUBENV || 'production';
const router = express.Router();
import config from '../app/config';
import client from '../app/services';
import at from 'v-at';
import handleAuthTokenUpdate from '@myntra/myx/lib/handleAuthTokenUpdate';
import assignSlot from '@myntra/myx/lib/server/ware/assignSlot';
import { getFingerPrintHeaders, getLocationHeaders } from '../app/services/header';
import Cookies from '../utils/cookies';

const getDefaultErrorMessage = function () {
  return 'Oops! Something unexpected happened. Please try again in some time.';
};
const logError = function (action, message, err) {
  message = message || 'Error';
  err = err || '';
  console.error(`${new Date()} Error: [Wishlist-${action}] ${message}.${err}`);
};

const logInfo = function (action, message) {};

const getFromCookie = function (req, res, cname) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  return cookies.get(cname) || false;
};

const loginCheck = function (req, res, action, message) {
  message = message || 'continue';
  if (at(req, 'myx.session.isLoggedIn')) {
    return true;
  }
  logError(action, 'Login check failed. User not logged in.');
  sendErrorResponse(res, 403, '403', `Not authorized. Please login to ${message} `);
};


const tokenCheck = function (req, res, action) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  const tokenPosted = cookies.get('_xsrf') || at(req, 'myx.session.USER_TOKEN');

  if (req.header('X-CSRF-TOKEN') === tokenPosted) {
    return true;
  }

  logError(action, 'Invalid session. Token mismatch');
  sendErrorResponse(res, 403, '403', 'Invalid session. Please login and try again');
};

const getEnvironmentPrefix = function () {
  if (env === 'production' || env === 'development') {
    return '';
  } else if (/fox/.test(env)) {
    return 'fox-';
  }
  return `${env}-`;
};

const getHeaders = function (req, res) {
  const xid = getFromCookie(req, res, 'xid') || '';
  const cookie = Cookies(req, res);
  return {
    cookie: `${getEnvironmentPrefix()}xid=` + xid + ';',
    xid,
    'X-Forwarded-For': req.header('X-Forwarded-For') || '',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
};

const getAddToWishListHeader = function (req, res) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);

  const atToken = (cookies.get('at') && cookies.get('xid') ? cookies.get('at') : at(req, 'myx.tokens.at')) || 'dummyvalue';
  const rtToken = cookies.get('rt');

  return {
    at: atToken,
    ...(rtToken && { rt: rtToken }),
    ...getLocationHeaders(cookies),
    ...getFingerPrintHeaders(req, cookies)
  };
};

const sendErrorResponse = function (res, responseCode, statusCode, message, errObj) {
  res.status(responseCode || 200).send({
    status: {
      statusCode: statusCode || '200',
      statusType: 'ERROR',
      statusMessage: message || getDefaultErrorMessage(),
      response: errObj || {}
    }
  });
};

const wishlistEnabledCheck = function (req, res) {
  if (req.myx.features && req.myx.features['wishlist.enable'] === 'true') {
    return true;
  }
  const errorMessage = 'Wishlist feature is turned off through feature gate';
  logError('WishlistEnabledCheck', errorMessage);
  sendErrorResponse(res, 200, 200, errorMessage);
};

const priceRevealCheck = function (req, res) {
  if (req.myx.kvpairs && req.myx.kvpairs['hrdr.pricereveal.data']) {
    let priceRevealData = req.myx.kvpairs['hrdr.pricereveal.data'];
    try {
      priceRevealData = JSON.parse(priceRevealData);
    } catch (e) {
      logError('priceRevealCheck', 'price reveal kvpair data is not in proper json format');
    }
    priceRevealData = priceRevealData || {};
    if (priceRevealData.enable === 'true') {
      if (getFromCookie(req, res, 'stp') && !getFromCookie(req, res, 'stb')) {
        logInfo('priceRevealCheck', 'Pass cookie (stp) present. so allowing add to bag');
        return true;
      }
      const errorMessage = 'Price reveal is enabled. Move to bag action is temporarily turned off';
      logError('PriceRevealCheck', errorMessage);
      sendErrorResponse(res, 200, 200, errorMessage);
      return;
    }
  }
  return true;
};

const handleResponse = function (action, serviceResponse, res, formNoCacheRes) {
  if ((at(serviceResponse, 'body.meta.code') === 401 && at(serviceResponse, 'body.meta.errorType') === 'ERROR') || at(serviceResponse, 'statusCode') === 401) {
    logError(action, 'Auth error from service due to invalid xid');
    sendErrorResponse(res, 403, '403', `Not authorized. Please login to continue `);
    return;
  }
  if (formNoCacheRes) {
    res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.setHeader('Expires', '-1');
    res.setHeader('Pragma', 'no-cache');
  }
  res.status(200).send(at(serviceResponse, 'body'));
}

const handleResponseApify = function (action, serviceResponse, res, formNoCacheRes, req) {
  if ((at(serviceResponse, 'body.meta.code') === 401 && at(serviceResponse, 'body.meta.errorType') === 'ERROR') || at(serviceResponse, 'statusCode') === 401) {
    const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
    if (at(req, 'myx.session.isLoggedIn')) {
      cookies.del('xid');
    }
    logError(action, 'Auth error from service due to invalid xid');
    sendErrorResponse(res, 403, '403', 'Not authorized. Please login to continue ');
    return;
  }
  if (formNoCacheRes) {
    res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.setHeader('Expires', '-1');
    res.setHeader('Pragma', 'no-cache');
  }
  res.status(200).send(at(serviceResponse, 'body'));
};

router.get('/getAllItems', (req, res) => {
  if (wishlistEnabledCheck(req, res) && loginCheck(req, res, 'GetAllItems', 'view all wishlisted items')) {
    const page = at(req, 'query.page');
    const pageSize = at(req, 'query.pageSize');
    const query = isNaN(page) || isNaN(pageSize) ? '' : `?page=${page}&pageSize=${pageSize}`;
    client.get(`${config('wishlistNew')}${query}`, {...getHeaders(req, res), xid: '10848892-76de-11e9-83c5-02010a54f583'}).end((error, response) => {
      if (error) {
        logError('GetAllItems Error from service ', + error);
        sendErrorResponse(res);
      } else {
        handleResponse('GetAllItems', response, res, true);
      }
    });
  }
});

router.get('/apify/getWishlist', (req, res) => {
  if (wishlistEnabledCheck(req, res) && loginCheck(req, res, 'GetAllItems', 'view all wishlisted items')) {
    const size = at(req, 'query.size');
    const offset = at(req, 'query.offset');
    const query = isNaN(size) || isNaN(offset) ? '' : `?size=${size}&offset=${offset}`;
    client.get(`${config('wishlistApifyV2')}${query}`, getAddToWishListHeader(req, res)).end((error, response) => {
      if (error) {
        handleAuthTokenUpdate(req, res, response);
        logError('GetWishlistItem', 'Error from service ' + error);
        sendErrorResponse(res, 200, '200', getDefaultErrorMessage(), response.body);
      } else {
        handleAuthTokenUpdate(req, res, response);
        handleResponseApify('AddItem', response, res, true, req);
      }
    });
  }
});

router.get('/summary', (req, res) => {
  if (wishlistEnabledCheck(req, res) && loginCheck(req, res, 'Summary', 'get the wishlist summary')) {
    client.get(`${config('wishlistNew')}/summary`, getHeaders(req, res)).end((error, response) => {
      if (error) {
        logError('Summary', 'Error from service ' + error);
        sendErrorResponse(res);
      } else {
        handleResponse('Summary', response, res, true);
      }
    });
  }
});

router.post('/moveToBag', (req, res) => {
  if (wishlistEnabledCheck(req, res) && priceRevealCheck(req, res) && loginCheck(req, res, 'MoveToBag', 'move an item to bag') && tokenCheck(req, res, 'MoveToBag')) {
    const data = req.body;
    data.listID = 'wishlist';
    if (data.styleID && data.skuId) {
      client.post(`${config('wishlistNew')}/moveToBag`, data, getHeaders(req, res)).end((error, response) => {
        if (error) {
          logError('MoveToBag', 'Error from service ' + error);
          sendErrorResponse(res, 200, at(response, 'body.meta.code'), at(response, 'body.meta.errorDetail'), response.body);
        } else {
          handleResponse('MoveToBag', response, res, true);
        }
      });
    } else {
      const errorMessage = 'Insufficient data. Required data (styleID and skuId) is missing';
      logError('MoveToBag', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  }
});

router.post('/addItem', (req, res, next) => {
  const assignSlotATW = get(req, 'myx.features["slots.assignSlotATW"]');
  if (assignSlotATW === true) {
    assignSlot(req, res, next);
  } else {
    next();
  }
}, (req, res) => {
  const xid = getFromCookie(req, res, 'xid');
  if (xid) {
    if (wishlistEnabledCheck(req, res) && loginCheck(req, res, 'AddItem', 'add an item to wishlist') && tokenCheck(req, res, 'AddItem')) {
      const data = req.body;
      data.listID = 'wishlist';
      if (data.styleID) {
        client.post(`${config('wishlistNew')}/add`, data, getHeaders(req, res)).end((error, response) => {
          if (error) {
            logError('AddItem', 'Error from service ' + error);
            sendErrorResponse(res, 200, '200', getDefaultErrorMessage(), response.body);
          } else {
            handleResponse('AddItem', response, res, true);
          }
        });
      } else {
        const errorMessage = 'Insufficient data. Required data (styleID) is missing';
        logError('AddItem', errorMessage);
        sendErrorResponse(res, 200, '200', errorMessage);
      }
    }
  } else {
    res.send(400, {
      error: {
        message: 'Oops! Something went wrong. Please refresh and try again in some time'
      }
    });
  }
});

router.post(['/addition', '/apify/moveToBag', '/apify/delete'], (req, res, next) => {
  const reqUrl = get(req, 'originalUrl') || get(req, 'baseUrl') || '';
  if (reqUrl.indexOf('/apify/delete') !== -1) {
    next();
    return;
  }
  const assignSlotATW = get(req, 'myx.features["slots.assignSlotATW"]');
  const assignSlotATC = get(req, 'myx.features["slots.assignSlotATC"]');
  if ((reqUrl.indexOf('addition') !== -1 && assignSlotATW === true) || (reqUrl.indexOf('moveToBag') !== -1 && assignSlotATC === true)) {
    assignSlot(req, res, next);
  } else {
    next();
  }
}, (req, res) => {
  if (wishlistEnabledCheck(req, res) && loginCheck(req, res, 'AddItem', 'add an item to wishlist')) {
    const data = req.body;
    const action = data.action;
    let context = 'add';
    if (action === 'MOVE_TO_CART') {
      context = 'movetocart';
    }
    if (action === 'REMOVE_ITEM') {
      context = 'delete';
    }
    data.listID = 'wishlist';
    if (data.styleId) {
      client.post(`${config('wishlistApify')}/${context}`, data, getAddToWishListHeader(req, res)).end((error, response) => {
        if (error) {
          handleAuthTokenUpdate(req, res, response);
          logError('AddItem', 'Error from service ' + error);
          sendErrorResponse(res, 200, '200', getDefaultErrorMessage(), response.body);
        } else {
          if (req.session) {
            req.session.reset();
          }
          handleAuthTokenUpdate(req, res, response);
          handleResponseApify('AddItem', response, res, true, req);
        }
      });
    } else {
      const errorMessage = 'Insufficient data. Required data (styleID) is missing';
      logError('AddItem', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  } else {
    res.send(400, {
      error: {
        message: 'Oops! Something went wrong. Please refresh and try again in some time'
      }
    });
  }
});

router.post('/removeItem', (req, res) => {
  if (wishlistEnabledCheck(req, res) && loginCheck(req, res, 'RemoveItem', 'remove the item from wishlist') && tokenCheck(req, res, 'RemoveItem')) {
    const data = req.body;
    if (data && data.styleID) {
      data.listID = 'wishlist';
      client.post(`${config('wishlistNew')}/delete`, data, getHeaders(req, res)).end((error, response) => {
        if (error) {
          logError('RemoveItem', 'Error from service ' + error);
          sendErrorResponse(res);
        } else {
          handleResponse('RemoveItem', response, res, true);
        }
      });
    } else {
      const errorMessage = 'Insufficient data. Required data (styleID) is missing';
      logError('RemoveItem', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  }
});

router.post('/moveItem', (req, res) => {
  if (loginCheck(req, res, 'MoveItem', 'move the item to wishlist') && tokenCheck(req, res, 'MoveItem')) {
    const data = req.body;
    if (data && data.styleID && data.skuId && data.itemID) {
      data.listID = 'wishlist';
      client.post(`${config('wishlistNew')}/moveToCollection`, data, getHeaders(req, res)).end((error, response) => {
        if (error) {
          logError('MoveItem', 'Error from service ' + error);
          sendErrorResponse(res);
        } else {
          handleResponse('MoveItem', response, res, true);
        }
      });
    } else {
      const errorMessage = 'Insufficient data. Required data (styleID, skuId, itemID) is missing';
      logError('MoveItem', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  }
});

export default router;
