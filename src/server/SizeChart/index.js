import config from '../../app/config';
import client from '../../app/services';
import at from 'v-at';

export const normalizeMeasurements = (data = {}) => {
  let productOptions = data.productOptions;

  if (productOptions instanceof Array) {
    productOptions = productOptions.map((productOption) => {
      const measurement = typeof productOption.measurement === 'object'
        ? productOption.measurement
        : (productOption.measurement && JSON.parse(productOption.measurement));

      const measurements = [];
      const dimensions = measurement ? Object.keys(measurement) : null;

      if (measurement && dimensions instanceof Array) {
        dimensions.forEach((key) => {
          const expression = /_hidden$/i;
          if (!expression.test(key)) {
            const dimension = {
              facet: key,
              type: measurement[key].type,
              value: at(measurement[key], 'value.value') || '',
              unit: at(measurement[key], 'value.unit') || ''
            };
            measurements.push(dimension);
          }
        });
        delete productOption.measurement;
        productOption.measurements = measurements;
        return productOption;
      }
      return productOption;
    });

    data.productOptions = productOptions;
    return data;
  }

  return data;
}

export const SizeChartHandler = (req, res) => {
  const styleId = req.params.id;
  const headers = {
    Authorization: 'Basic bW9iaWxlfm1vYmlsZTptb2JpbGU='
  };
  client.get(config('sizechart') + styleId, headers).end((err, response) => {
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    res.send(200, normalizeMeasurements(at(response, 'body.data.0')));
  });
}

// module.exports = handler;
