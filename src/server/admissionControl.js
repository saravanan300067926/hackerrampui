import { getFingerPrintHeaders, getLocationHeaders } from '../app/services/header';

const express = require('express');
const app = express();
const env = process.env.NODE_SUBENV || 'production';
const router = express.Router();

import config from '../app/config';
import client from '../app/services';
import at from 'v-at';
import get from 'lodash/get';
import Cookies from '../utils/cookies';
import { ROUTES } from '../app/config';

const getDefaultErrorMessage = function() {
  return 'Oops! Something unexpected happened. Please try again in some time.';
}
const logError = function(action, message, err) {
  message = message || 'Error';
  err = err || '';
  console.error(`${new Date()} Error: [AdmissionControl-${action}] ${message}.${err}`);
}

const logInfo = function(action, message) {}

const getXidFromCookie = function(req, res) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  return cookies.get('xid');
}

const loginCheck = function(req, res, action, message) {
  message = message || 'continue';
  if (at(req, 'myx.session.isLoggedIn')) {
    return true;
  }
  logError(action, 'Login check failed. User not logged in.');
  sendErrorResponse(res, 403, '403', `Not authorized. Please login to ${message} `);
}

const tokenCheck = function(req, res, action) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);

  if(req.header('X-CSRF-TOKEN') ===  cookies.get('_xsrf')) {
    return true;
  }
  logError(action, 'Invalid session. Token mismatch');
  sendErrorResponse(res, 403, '403', 'Invalid session. Please login and try again');
}

const getEnvironmentPrefix = function() {
    if (env === 'production' || env === 'development') {
        return '';
    } else if (/fox/.test(env)) {
        return 'fox-';
    }
    return `${env}-`;
}

const getHeaders = function(req, res) {
  const xid = getXidFromCookie(req, res) || '';
  const cookie = Cookies(req, res);

  return {
    cookie: `${getEnvironmentPrefix()}xid=` + xid + ';',
    xid,
    'X-Forwarded-For': req.header('X-Forwarded-For') || '',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
}

const sendErrorResponse = function(res, responseCode, statusCode, message) {
  res.status(responseCode || 200).send({
    status: {
      statusCode: statusCode || '200',
      statusType: 'ERROR',
      statusMessage: message || getDefaultErrorMessage()
    }
  });
}

const handleResponse = function(action, serviceResponse, res) {
  if(at(serviceResponse, 'body.meta.code') === 401 && at(serviceResponse, 'body.meta.errorType') === 'ERROR') {
    logError(action, 'Auth error from service due to invalid xid');
    sendErrorResponse(res, 403, '403', `Not authorized. Please login to continue `);
    return;
  }

  // copy only cookie header fix for decoding error fix
  if (serviceResponse && serviceResponse.headers['set-cookie']) {
    res.setHeader('set-cookie', serviceResponse.headers['set-cookie']);
  }

  res.status(200).send(at(serviceResponse, 'body'));
}

router.get('/getSlotInfo', (req, res) => {
  if (loginCheck(req, res, 'getSlotInfo', 'to get slot info')) {
    const contextValue = get(req, 'myx.features["slots.context"]') || 'general';
    client.get(`${config('admission')}/slot?context=${contextValue}`, getHeaders(req, res)).end((error,response) => {
      if (error || at(response, 'body.meta.code') !== 200) {
        logError('getSlotInfo', 'Error from service ' + (error || 'Response code is not 200'));
        sendErrorResponse(res);
      } else {
        handleResponse('getSlotInfo', response, res);
      }
    });
  }
});

router.put('/updateSlot', (req, res) => {
  if(loginCheck(req, res, 'UpdateSlot', 'to update the slot') && tokenCheck(req, res, 'UpdateSlot')) {
    const data = req.body;
    try {
      logInfo('UpdateSlot --- Request body', JSON.stringify(data));
      if(data && data.id && data.context) {
        const contextValue = get(req, 'myx.features["slots.context"]') || 'general';
        client.put(`${config('admission')}/reserve?context=${contextValue}`, data, getHeaders(req, res)).end((error,response) => {
          if (error || at(response, 'body.meta.code') !== 200) {
            logError('UpdateSlot', 'Error from service ' + (error || 'Response code is not 200'));
            sendErrorResponse(res);
          } else {
            handleResponse('UpdateSlot', response, res);
          }
        });
      } else {
        const errorMessage = 'Insufficient data. Required data (slotId, context) is missing';
        logError('UpdateSlot', errorMessage);
        sendErrorResponse(res, 200, '200', errorMessage);
      }
    } catch(e) {
      const errorMessage = 'Request body is not a valid json';
      logError('UpdateSlot', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  }
});

router.get('/getSlot', (req, res) => {
  if(loginCheck(req, res, 'getSlot', 'to get the slot')) {
    try {
        const contextValue = get(req, 'myx.features["slots.context"]') || 'general';
        client.get(`${config('admission')}/slot?context=${contextValue}`,getHeaders(req, res)).end((error,response) => {
          if (error || at(response, 'body.meta.code') !== 200) {
            logError('UpdateSlot', 'Error from service ' + (error || 'Response code is not 200'));
            sendErrorResponse(res);
          } else {
            handleResponse('UpdateSlot', response, res);
          }
        });
    } catch(e) {
      const errorMessage = 'Request body is not a valid json';
      logError('UpdateSlot', errorMessage);
      sendErrorResponse(res, 200, '200', errorMessage);
    }
  }
});

export default router;
