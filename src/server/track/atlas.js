const _ = require('lodash');
const services = require('myx-services/');
const heads = require('myx-services/heads');
const resolve = require('myx-services/resolve');
const Uri = require('jsuri');
const isBrowser = (typeof window !== 'undefined');
const os = isBrowser ? null : require('os' + '');
const mcookies = require('@myntra/myx/lib/server/cookies');
const mcrypto = isBrowser ? null : require('@myntra/myx/lib/server/cookies/crypto' + '');
const abSvc = require('myx-services/ab');
const request = require('superagent');
const mixins = require('@myntra/myx/lib/mixins');
_.mixin(mixins);

module.exports = {
  track: function(req, data, callback, options) {
    if (typeof options == 'undefined') options = {};
    // callback = callback || function(){};
    // we're assuming this is node, of course
    if (isBrowser) {
      return;
    }

    const features = _.at(req, 'myx.features');
    if(features && features['atlas.disable'] === 'true') {
      return;
    }

    req.myx = _.extend(req.myx || {}, data._myx);
    delete data._myx;

    const uri = new Uri(req.headers.referer);
    const cookies = mcookies(req, {});

    data = _.extend({}, {
      nodejs: true,
      myx: true,
      timestamp: Date.now(),
      microSessionID: req.myx.microsessid,
      trafficData: req.myx.traffic,
      deviceData: req.myx.deviceData,
      isRobot: _.at(req, 'myx.deviceData.isRobot'),
      userAgent: req.headers['user-agent'],
      "x-myntra-app": req.headers["x-myntra-app"] || cookies.get('x-myntra-app') || '',
      clientIP: req.clientIP ? req.clientIP : req.ip,
      serverName: os.hostname(),
      referer: {
        host: uri.host(),
        domain: uri.host().replace('www.', ''),
        path: uri.path(),
        query: uri.query().substr(0, 500),
        protocol: uri.protocol()
      },
      hostURL: req.headers.host + req.url,
      // hostURL: req.protocol + '://' + req.host + req.originalUrl, // is there a better way?
      uuid: req.myx.uuid,
      rememberLogin: mcrypto.decrypt(cookies.get('ru')),
      login: _.at(req, 'myx.session.login') || '',
      cookieId: _.at(req, 'myx.session.xid') || '',
      bid5: _.at(req, 'myx.session.bid5') || ''
    }, abSvc.atlas(req.myx.ab), data || {});

    if (options.log || process.env.NODE_ATLAS_LOG === 'full') {
      console.log('AtlasData:', JSON.stringify(data));
    }
    const spec = require('myx-services/resolve')(':atlas');
    const url = spec.url || (spec.root + spec.path);

    //2000ms timeout for Atlas calls.
    request
      .post(url)
      .timeout(2000)
      .send(data)
      .on('error', function(err) {
        console.error('Error while sending atlas data', err);
      })
      .end(function(err, res){
        if(err){
          if(typeof err.message === 'string' && err.message.search('timeout') != -1){
            console.error('ATLAS TIMEDOUT AFTER 2000ms');
          }else{
            console.error('Atlas Fail: code='+err.code + ' errno='+err.errno + ' message='+err.message + ' url='+req.url);
          }
        }
        // silent failure
        callback && callback();
      });
  }
};
