const express = require('express');
const router = new express.Router();
const stats = require('./stats')();
const atlas = require('./atlas');
const _ = require('lodash');
const clientIP = require('@myntra/myx/lib/server/ware/clientIP.js');

router.post('/search/track/stats', [require('@myntra/myx/lib/server/ware/cookies/uuid').light,pushStats]);
router.post('/search/track/atlas',clientIP, pushToAtlas);

function pushToAtlas(req, res) {
  // quickly send back
  res.type('json');
  res.send('{}');
  // and _then_ track
  if (_.isArray(req.body)) {
    _.each(req.body, function(data) {
      atlas.track(req, data, function(err) {
        if (err) console.error('unable to send to atlas (xhr)');
      });
    });
  } else {
    atlas.track(req, req.body, function(err) {
      if (err) console.error('unable to send to atlas (xhr)');
    });
  }
}

function pushStats(req, res) {
  // quickly send back
  res.type('json');
  res.send('{}');
  // and _then_ track
  const query = req.body;
  if (_.isArray(query)) {
    _.each(query, $stats);
  } else {
    $stats(query);
  }

  function $stats(query) {
    const method = query.method;
    const name = query.name;
    const value = query.value;
    const rate = query.rate;

    try {
      stats[method]('xhr.' + name, value, rate, function(err) {
        if (err) console.error('unable to send stats call:', err);
      });
    } catch (e) {
      console.error('failed to send stats call', e, method, name, value, rate);
    }
  }
}

export default router;
