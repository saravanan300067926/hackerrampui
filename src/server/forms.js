import { getFingerPrintHeaders, getLocationHeaders } from '../app/services/header';

const express = require('express');
const env = process.env.NODE_SUBENV || 'production';
const router = express.Router();
const validators = require('@myntra/myx/lib/components/login/validators');
import client from '../app/services';
import at from 'v-at';
import last from 'lodash/last';
import reduce from 'lodash/reduce';
import isEmpty from 'lodash/isEmpty';
import config, { ROUTES, getRouteConfig } from '../app/config';
import get from 'lodash/get';
import handleAuthTokenUpdate from '@myntra/myx/lib/handleAuthTokenUpdate';
import assignSlot from '@myntra/myx/lib/server/ware/assignSlot';
import middleware from '../routes/middleware';
import Cookies from '../utils/cookies';

router.get(`/${ROUTES.topnav.path}`, middleware, (req, res) => {
  const cookie = Cookies(req, res);
  client.get(config('topnav'), {
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  }).then((response) => {
    if (response) {
      res.send(200, response.body);
    } else {
      res.send(200, {
        status: {
          statusType: 'ERROR',
          statusMessage: 'Oops! Something unexpected happened. Please try again in some time.'
        }
      });
    }
  });
});

function getEnvironmentPrefix() {
  if (env === 'production' || env === 'development') {
    return '';
  } else if (/fox/.test(env)) {
    return 'fox-';
  }
  return `${env}-`;
}


router.post(`/${ROUTES.signout.path}`, middleware, (req, res) => {
  var cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  var headers = {};
  var atToken = cookies.get('at');
  var rtToken = cookies.get('rt');
  var xid = cookies.get('xid') || '';

  if (req && req.__cookies__) {
    delete(req.__cookies__.at);
    delete(req.__cookies__.xid);
    delete(req.__cookies__.rt);
    delete(req.__cookies__.sxid);
  }

  if (atToken && rtToken) {
    headers = {
      at: atToken,
      rt: rtToken,
      xid: xid,
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };

    client.post(config('signout'), {}, headers)
      .end(function (err, response) {
        var statusCode = at(response, 'statusCode');
        var cookies = require('@myntra/myx/lib/server/cookies')(req, res);
        // to handle if the auth tokens are updated
        handleAuthTokenUpdate(req, res, response);

        const rtExpired = at(response, 'updatedTokens.rtExpired');

        // handle the rt expired case in case of logout
        if (rtExpired) {
          if (at(req, 'myx.deviceData.isMobile')) {
            res.redirect(301, '/');
            return;
          } else {
            res.send(200, {
              httpCode: 200,
              message: 'Successfully logged out.'
            });
            return;
          }
        }

        if (statusCode === 200 && typeof response !== 'undefined') {
          // signout success
          cookies.del('nc'); //delete notification count cookie
          cookies.del('sfrate');

          // delete ftc
          cookies.del('ftc');

          // delete xid
          cookies.del('xid');
          cookies.del('sxid');

          // delete slot cookies
          cookies.del('stp', { httpOnly: false, domain: '.myntra.com', path: '/', secure: false });
          cookies.del('sts');
          cookies.del('stb', { httpOnly: false, domain: '.myntra.com', path: '/', secure: false });

          // delete switch variant cookie
          cookies.del('_pv');

          // delete token cookies
          cookies.del('at');
          cookies.del('rt');

          // csrf token
          cookies.del('_xsrf');

          // isLoggedIn false set
          cookies.del('ilgim');

          // Remove FirstTimeCustomer cookie
          cookies.del('newUser');
          //cookies.del('ftc');

          // Remove abTests
          cookies.del('_mxab_');

          cookies.del('mynt-ulc');
          cookies.del('mynt-ulc-api');
          cookies.del('mynt-loc-src');

          if (at(req, 'myx.deviceData.isMobile')) {
            res.redirect(301, '/');
          } else {
            if (req.session) {
              req.session.reset();
            }
            res.send(200, {
              httpCode: 200,
              message: 'Successfully logged out.'
            });
          }
        } else {
          res.send(200, {
            httpCode: 400,
            message: 'Oops! Something unexpected happened. Please refresh and try again in some time.'
          });
        }
      });
  } else {
    res.send(200, {
      httpCode: 400,
      message: 'Oops! Something unexpected happened. Please refresh and try again in some time.'
    });
  }
});

//logout from old routes -> mymyntra and giftcards
router.post('/' + ROUTES.signoutOld.path, middleware, (req, res) => {
  var cookies = require('@myntra/myx/lib/server/cookies')(req, res);

  if (req && req.__cookies__) {
    delete(req.__cookies__.at);
    delete(req.__cookies__.xid);
    delete(req.__cookies__.rt);
    delete(req.__cookies__.sxid);
  }

  var atToken = cookies.get('at');
  var rtToken = cookies.get('rt');

  if (atToken && rtToken) {
    var xid = cookies.get('xid') || '';

    if (!xid) {
      res.send(200, {
        status: {
          statusType: 'ERROR',
          statusMessage: 'Oops! Something unexpected happened. Please refresh the page and try again.'
        }
      });
      return;
    }

    var headers = {
      at: atToken,
      rt: rtToken,
      xid: xid,
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };

    client.post(config('signout'), {}, headers)
      .end(function (err, response) {
        var statusCode = at(response, 'statusCode');
        handleAuthTokenUpdate(req, res, response);

        const rtExpired = at(response, 'updatedTokens.rtExpired');

        // handle the rt expired case in case of logout
        if (rtExpired) {
          if (at(req, 'myx.deviceData.isMobile')) {
            res.redirect(301, '/');
            return;
          } else {
            res.send(200, {
              httpCode: 200,
              message: 'Successfully logged out.'
            });
            return;
          }
        }

        if (statusCode === 200 && typeof response !== 'undefined') {
          // signout success
          cookies.del('nc'); //delete notification count cookie
          cookies.del('sfrate');

          // delete xid
          cookies.del('xid');
          cookies.del('sxid');

          // delete slot cookies
          cookies.del('stp', { httpOnly: false, domain: '.myntra.com', path: '/', secure: false });
          cookies.del('sts');
          cookies.del('stb', { httpOnly: false, domain: '.myntra.com', path: '/', secure: false });

          // delete switch variant cookie
          cookies.del('_pv');

          // delete token cookies
          cookies.del('at');
          cookies.del('rt');

          // csrf token
          cookies.del('_xsrf');

          cookies.del('ilgim');

          if (at(req, 'myx.deviceData.isMobile')) {
            res.redirect(301, '/');
          } else {
            res.send(200, {
              status: {
                statusType: 'SUCCESS',
                statusMessage: 'Successfully logged out.'
              }
            });
          }
        } else {
          res.send(200, {
            status: {
              statusType: 'ERROR',
              statusMessage: 'Oops! Something unexpected happened. Please refresh the page and try again in some time.'
            }
          });
        }
      });
  } else {
    res.send(200, {
      httpCode: 400,
      message: 'Oops! Something unexpected happened. Please refresh and try again in some time.'
    });
  }
});

router.post([('/' + ROUTES.signin.path), ('/' + ROUTES.google.path), ('/' + ROUTES.facebook.path), ('/' + ROUTES.signup.path), ('/' + ROUTES.forgotPassword.path)], middleware, (req, res) => {
  var action = req.body.action,
    when = require('@myntra/myx/lib/when'),
    ent = require('ent'),
    async = require('@myntra/myx/lib/async'),
    cookies = require('@myntra/myx/lib/server/cookies')(req, res),
    userAgent = req.get('user-agent') || '',
    deviceId = cookies.get('_d_id') || '',
    w = when(req),
    payload,
    validationResult = true,
    tokenPosted = req.body && req.body.xsrf ? req.body.xsrf : null,
    headers = {
      'X-CSRF-TOKEN': tokenPosted,
      'X-Forwarded-For': req.header('X-Forwarded-For') || '',
      'xid': cookies.get('xid') || '',
      'at': cookies.get('at') || at(req, 'myx.tokens.at') || 'dummyvalue',
      'User-Agent': userAgent,
      'x-meta-app': `deviceId=${deviceId};appFamily=${userAgent};reqChannel=web;`,
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };

  var dec = function (x) {
    return ent.decode(x || '');
  };

  switch (action) {
    case 'signin':
      payload = {
        userId: dec(req.body.email)
          .trim(),
        accessKey: dec(req.body.password)
          .trim(),
        captchaValue: dec(req.body.captcha)
          .trim()
      };
      validationResult = validateSignin(req);
      break;
    case 'signout':
      payload = {
        xsrf: dec(req.body.xsrf).trim
      };
      validationResult = true;
      break;
    case 'signup':
      payload = {
        userId: dec(req.body.email)
          .trim(),
        accessKey: dec(req.body.password)
          .trim(),
        phoneNumber: dec(req.body.mobile)
          .trim(),
        gender: dec(req.body.gender)
          .trim(),
        usertype: dec(req.body.usertype),
        captchaValue: dec(req.body.captcha)
      };
      validationResult = validateSignup(req);
      break;

    case 'facebook':
      payload = {
        accessToken: req.body.token
      };
      break;

    case 'google':
      payload = {
        idToken: req.body.token
      };
      break;

    case 'forgotPassword':
      payload = {
        userId: dec(req.body.email)
          .trim()
      };
      headers.clientid = 'myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61';
      validationResult = validateForgotPassword(req);
      break;
    case 'guestlogin':
      payload = {
        userId: req.body.email,
        usertype: req.body.usertype
      };
      //Checking for email validity
      validationResult = validateForgotPassword(req);
      break;
  }

  if (!validationResult) {
    if (action === 'signin') {
      res.send(200, {
        httpCode: 400,
        message: 'Please enter a valid email id and password'
      });
    }
    else if (action === 'signup') {
      res.send(200, {
        httpCode: 400,
        message: 'Please enter valid credentials'
      });
    }
    else if (action === 'forgotPassword') {
      res.send(200, {
        httpCode: 400,
        message: 'Please enter a valid email id'
      });
    }
  } else {
    async.parallel([
      w.all,
      function (done) {
        client.post(config(action), payload, headers)
          .end(function (err, response) {
            done(err, response);
          });
        //}
      }
    ], function callback(err, resArr) {
      err && console.error(err);

      var response = last(resArr),
        result = err ? {
          code: 2002,
          httpCode: 400
        } : response && response.body;


      var statusCode = at(response, 'statusCode');

      if (statusCode === 200 && typeof response !== 'undefined') {
        if (action === 'forgotPassword') {
          handleAuthTokenUpdate(req, res, response);
          res.send(200, response.body);
        } else {
          if (req.session) {
            req.session.reset();
          }
          var xid = response.headers.xid,
            sxid = response.headers.sxid,
            atToken = response.headers.at,
            rtToken = response.headers.rt;

          var sessionExpirySecs = at(req, 'myx.kvpairs.sessionExpirySecs');
          var xidExpiry = new Date(new Date().getTime() + (isNaN(sessionExpirySecs) ? 7 * 24 * 60 * 60 : sessionExpirySecs) * 1000); // 7 days default

          var atTokenExpirySecs = at(req, 'myx.kvpairs.atTokenExpirySecs');
          var rtTokenExpirySecs = at(req, 'myx.kvpairs.rtTokenExpirySecs');
          var defaultTokenExpirySecs = 186 * 24 * 60 * 60; // default 6 months expiry

          var atExpiry = new Date(new Date().getTime() + (isNaN(atTokenExpirySecs) ? defaultTokenExpirySecs : atTokenExpirySecs) * 1000);
          var rtExpiry = new Date(new Date().getTime() + (isNaN(rtTokenExpirySecs) ? defaultTokenExpirySecs : rtTokenExpirySecs) * 1000);

          xid && cookies.set('xid', xid, {
            expires: xidExpiry
          });

          // this is a session cookie, i.e. expires when the user closes the browser
          sxid && cookies.set('sxid', sxid, {
            secure: true,
            encode: String
          });

          atToken && cookies.set('at', atToken, {
            'encode': String,
            'expires': atExpiry
          });

          rtToken && cookies.set('rt', rtToken, {
            'encode': String,
            'expires': rtExpiry
          });

          // delete the portal variant cookie if already existing.
          cookies.del('_pv');

          // delete the abtests cookie
          cookies.del('_mxab_');

          // csrf token
          cookies.del('_xsrf');

          // isLoggedIn true set
          cookies.set('ilgim', 'true', {
            secure: false,
            httpOnly: false,
            expires: rtExpiry
          });

          res.send(200, response.body);
        }
      } else {

        var body = {
          httpCode: 400,
          message: 'Oops! Something unexpected happened. Please try again in some time.'
        };

        if (response && !isEmpty(response.body)) {
          body = response.body;
        }

        res.send(200, body);
      }

      w.destroy();
    });
  }
});

function validateSignup(req) {
  return !reduce(['email', 'password', 'gender'], function (hasErrors, field) {
    var input = req.body[field],
      error = '';
    if (input && input.trim().length > 100) {
      error = "Length too long";
    }
    else if (field === 'gender') {
      error = (input === "male" || input === "female") ? '' : 'Please choose valid gender';
    }
    else {
      error = validators[field](input);
    }
    return hasErrors || error;
  }, false);
}

function validateSignin(req) {
  return !reduce(['email', 'password'], function (hasErrors, field) {
    var input = req.body[field],
      error = '';
    if (input && input.trim().length > 100) {
      error = "Length too long";
    }
    else {
      error = validators[field](input);
    }
    return hasErrors || error;
  }, false);
}

function validateForgotPassword(req) {
  var error = validators['email'](req.body.email);
  return error === '';
}
router.post('/cart/default', middleware, (req, res) => {  // ToDo Need to be romoved after some time.
  var skuid = req.body.skuid,
    styleid = req.body.styleid,
    sellerPartnerId = req.body.sellerPartnerId,
    quantity = req.body.quantity,
    payload = {
      skuId: skuid,
      id: styleid,
      sellerPartnerId,
      quantity
    },
    cookies = require('@myntra/myx/lib/server/cookies')(req, res),
    //uniqueId = at(req, 'cookies._d_id') || '',
    //xsrftoken = cookies.get('_xsrf') || at(req, 'myx.session.USER_TOKEN'),
    atToken = cookies.get('at') || 'dummyvalue',
    rtToken = cookies.get('rt'),
    //xid = cookies.get('xid'),
    headers = {
      'at': atToken,
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };
  if(rtToken){
    headers.rt = rtToken
  }

  if (atToken) {
    client.post(config('cart'), payload, headers).end(function (err, response) {
      if (err) {
        res.send(200, {
          error: {
            message: 'Oops! Something went wrong. Please try again in some time.'
          }
        });
        return;
      }
      handleAuthTokenUpdate(req, res, response);
      res.send(200, at(response, 'body'));
    });
  } else {
    res.send(200, {
      error: {
        message: 'Oops! Something went wrong. Please refresh and try again in some time.'
      }
    });
  }
});

router.get('/' + ROUTES.cartSummary.path, middleware, (req, res) => {
  var cookies = require('@myntra/myx/lib/server/cookies')(req, res),
    atToken = cookies.get('at') || 'dummyvalue',
    rtToken = cookies.get('rt'),
    headers = {
      'at': atToken,
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };

  if (rtToken) {
    headers.rt = rtToken;
  }

  client.get(config('cartSummary'), headers).end(function (err, response) {
    if (err) {
      res.send(500, {
        error: err
      });
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.post('/' + ROUTES.cart.path, middleware, (req, res, next) => {
  const assignSlotATC = get(req, 'myx.features["slots.assignSlotATC"]');
  if (at(req, 'myx.session.isLoggedIn') && assignSlotATC === true) {
    assignSlot(req, res, next);
  } else {
    next();
  }
}, (req, res) => {
  var skuid = req.body.skuid,
    styleid = req.body.styleid,
    sellerPartnerId = req.body.sellerPartnerId,
    quantity = req.body.quantity,
    payload = {
      skuId: skuid,
      id: styleid,
      sellerPartnerId,
      quantity
    },
    cookies = require('@myntra/myx/lib/server/cookies')(req, res),
    uniqueId = at(req, 'cookies._d_id') || '',
    atToken = (cookies.get('xid') && cookies.get('at') ? cookies.get('at') : at(req, 'myx.tokens.at')) || 'dummyvalue',
    rtToken = cookies.get('rt'),
    uidx = at(req, 'myx.session.login') || '',
    headers = {
      'x-myntra-app': `deviceID=${uniqueId};customerID=${uidx};reqChannel=web;`,
      'at': atToken,
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };
    if(rtToken){
      headers.rt = rtToken
    }

  if (atToken) {
    client.post(config('cart'), payload, headers).end(function (err, response) {
      if (err) {
        res.send(200, {
          error: {
            message: 'Oops! Something went wrong. Please try again in some time.'
          }
        });
        return;
      }
      if (req.session) {
        req.session.reset();
      }
      handleAuthTokenUpdate(req, res, response);
      res.send(200, at(response, 'body'));
    });
  } else {
    res.send(200, {
      error: {
        message: 'Oops! Something went wrong. Please refresh and try again in some time.'
      }
    });
  }
});

router.post('/' + ROUTES.wishlist.path, middleware, (req, res) => {
  var xsrf = req.body.xsrf,
    skuid = req.body.skuid,
    styleId = req.body.styleId,
    payload = {
      styleId: styleId,
      skuId: skuid,
      quantity: 1,
      operation: 'ADD'
    },
    cookies = require('@myntra/myx/lib/server/cookies')(req, res),
    xsrftoken = cookies.get('_xsrf') || at(req, 'myx.session.USER_TOKEN'),
    xid = cookies.get('xid'),
    headers = {
      cookie: req.headers.cookie,
      'X-CSRF-TOKEN': req.body.xsrf,
      'X-Forwarded-For': req.header('X-Forwarded-For') || '',
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };

  if ((xsrftoken === xsrf) && xid) {
    if (at(req, 'myx.session.isLoggedIn')) {
      client.put(config('wishlist'), payload, headers).end(function (err, response) {
        if (err) {
          res.send(500, {
            error: err
          });
          return;
        }
        handleAuthTokenUpdate(req, res, response);
        res.send(200, at(response, 'body'));
      });
    } else {
      res.send(403, {
        error: {
          message: 'Please login and then add to wishlist'
        }
      });
    }
  } else {
    res.send(400, {
      error: {
        message: 'Oops! Something went wrong. Please refresh and try again in some time'
      }
    });
  }
});

const getFromCookie = function (req, res, cname) {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res);
  return cookies.get(cname) || false;
};

router.get('/' + ROUTES.coupons.path + '/:id/:sellerPartnerId', middleware, (req, res) => {
  var styleId = req.params.id;
  var sellerPartnerId = req.params.sellerPartnerId;
  const rtToken = getFromCookie(req, res, 'rt');
  const cookie = Cookies(req, res);
  var headerParams = {
    'at': getFromCookie(req, res, 'at') || 'dummy',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  if (rtToken) {
    headerParams.rt = rtToken;
  }
  const coupons = getRouteConfig('coupons');
  client.get(coupons.root + styleId + '/' + coupons.path + '/' + sellerPartnerId, headerParams).end(function (err, response) {
    if (err) {
      res.send(500, {
        error: err
      });
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.get('/' + ROUTES.pdp.path + ':id', middleware, (req, res) => {
  var styleId = req.params.id;

  const cookie = Cookies(req, res);
  const rtToken = getFromCookie(req, res, 'rt');
  var headerParams = {
    'at': getFromCookie(req, res, 'at') || 'dummy',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  if (rtToken) {
    headerParams.rt = rtToken;
  }

  client.get(config('pdp') + styleId, headerParams).end((err, response) => {
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, at(response, 'body.style'));
  });
});

router.get('/' + ROUTES.pdpV1.path + ':id', middleware, (req, res) => {
  var styleId = req.params.id;

  const cookie = Cookies(req, res);
  const rtToken = getFromCookie(req, res, 'rt');
  var headerParams = {
    'at': getFromCookie(req, res, 'at') || at(req, 'myx.tokens.at') || 'dummy',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  if (rtToken) {
    headerParams.rt = rtToken;
  }

  client.get(config('pdpV1') + styleId, headerParams).end((err, response) => {
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, at(response, 'body.style'));
  });
});

router.get('/user/size-profiles/', middleware, (req, res) => {
  const cookie = Cookies(req, res);
  const atToken = getFromCookie(req, res, 'at') || at(req, 'myx.tokens.at') || 'dummy';
  const rtToken = getFromCookie(req, res, 'rt') || at(req, 'myx.tokens.rt') || '';
  const headerParams = {
    at: atToken,
    xid: getFromCookie(req, res, 'xid') || '',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  if (rtToken) {
    headerParams.rt = rtToken;
  }

  client.get(`${config('sizeReco')}/user/size-profiles?gender=${req.query.gender}&articleType=${req.query.articleType}`, headerParams).end((err, response) => {
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.post('/product/:id/size/recommendation', middleware, (req, res) => {
  const cookie = Cookies(req, res);
  const headerParams = {
    'x-myntra-abtest': getFromCookie(req, res, '_mxab_') || '',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  client.post(`${config('sizeReco')}/product/${req.params.id}/size/recommendation`, req.body, headerParams).end((err, response) => {
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.get('/' + ROUTES.filteredSearch.path + '/:query', middleware, (req, res) => {
  const atToken = getFromCookie(req, res, 'at') || at(req, 'myx.tokens.at') || '';
  const rtToken = getFromCookie(req, res, 'rt') || at(req, 'myx.tokens.rt') || '';

  let clientQuery = client.get(`${config('filteredSearch')}/${req.originalUrl.replace('/web/v2/search/', '')}`);
  clientQuery.set('at', atToken);

  if (rtToken) {
    clientQuery.set('rt', rtToken);
  }
  const cookie = Cookies(req, res)
  clientQuery.set({
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  });

  const uniqueId = at(req, 'cookies._d_id') || '';
  const uidx = at(req, 'myx.session.login') || '';
  clientQuery.set('x-myntra-app', `deviceID=${uniqueId};customerID=${uidx};reqChannel=web;`);

  if (get(req, 'myx.features["search.personalisation"]') === "true" && get(req, 'cookies["xid"]')) {
    clientQuery = clientQuery.set('xid', req.cookies['xid']);
  }

  clientQuery
    .end((err, response) => {
      if (err) {
        res.status(err.status || 500).send(err);
        return;
      }

      handleAuthTokenUpdate(req, res, response);

      res.send(response.status || 200, response.body);
    });
});

router.get('/' + ROUTES.recommendations.path + '/:id/:size?', middleware, (req, res) => {
  var styleId = req.params.id;
  let size = req.params.size || null;
  const recommendations = getRouteConfig('recommendations');
  let url = recommendations.root + styleId + '/' + recommendations.path;
  url = (size) ? url + '/' + size : url;

  const atToken = getFromCookie(req, res, 'at') || at(req, 'myx.tokens.at') || 'dummy';
  const rtToken = getFromCookie(req, res, 'rt') || at(req, 'myx.tokens.rt') || '';

  const cookie = Cookies(req, res);
  let headers = {
    at: atToken,
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  if (rtToken) {
    headers.rt = rtToken;
  }

  client.get(url, headers).end((err, response) => {
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.get('/' + ROUTES.crossSell.path +'/:id', middleware, (req, res) => {
  var styleId = req.params.id;
  var maxCount = req.query.maxCount || 15;
  const crossSell = getRouteConfig('crossSell');
  let url = crossSell.root + styleId + '/' + crossSell.path + '?maxCount=' + maxCount;

  const atToken = getFromCookie(req, res, 'at') || at(req, 'myx.tokens.at') || 'dummy';
  const rtToken = getFromCookie(req, res, 'rt') || at(req, 'myx.tokens.rt') || '';

  const cookie = Cookies(req, res);
  let headers = {
    at: atToken,
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };

  if (rtToken) {
    headers.rt = rtToken;
  }
  client.get(url, headers).end((err, response) => {
    if (err) {
      res.status(err.status || 500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.post('/' + ROUTES.serviceability.path, middleware, (req, res) => {
  var parameters = req.body;
  const cookie = Cookies(req, res);
  client.post(config('serviceability'), parameters, {
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  }).end(function (err, response) {
    if (err) {
      res.send(500, err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.post('/' + ROUTES.serviceabilityV2.path, middleware, (req, res) => {
  var parameters = req.body;
  const cookie = Cookies(req, res);
  var headerParams = {
    'at': getFromCookie(req, res, 'at') || at(req, 'myx.tokens.at') || 'dummy',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  client.post(config('serviceabilityV2'), parameters, headerParams).end(function (err, response) {
    if (err) {
      res.send(500, err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.post('/' + ROUTES.serviceabilityV3.path, middleware, (req, res) => {
  var parameters = req.body;
  const cookie = Cookies(req, res);
  var headerParams = {
    'at': getFromCookie(req, res, 'at') || at(req, 'myx.tokens.at') || 'dummy',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  client.post(config('serviceabilityV3'), parameters, headerParams).end(function (err, response) {
    if (err) {
      res.send(500, err);
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.post('/user/mobile/addphone', middleware, (req, res) => {
  const uri = config('mobileVerify');
  const params = req.body;
  const cookie = Cookies(req, res);
  const headerParams = {
    'xid': getFromCookie(req, res, 'xid') || '',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  client.post(uri, params, headerParams).end((err, resp) => {
    if (err) {
      res.status(500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, resp);
    res.status(resp.status).send(resp.body);
  });
});

router.post('/user/mobile/verifyphone', middleware, (req, res) => {
  const uri = config('otpVerify');
  const params = req.body;
  const cookie = Cookies(req, res);
  const headerParams = {
    'xid': getFromCookie(req, res, 'xid') || '',
    ...getLocationHeaders(cookie),
    ...getFingerPrintHeaders(req, cookie)
  };
  client.post(uri, params, headerParams).end((err, resp) => {
    if (err) {
      res.status(500).send(err);
      return;
    }
    handleAuthTokenUpdate(req, res, resp);
    res.status(resp.status).send(resp.body);
  });
});

router.get('/' + ROUTES.isNewUser.path, middleware, (req, res) => {
  var cookies = require('@myntra/myx/lib/server/cookies')(req, res),
  atToken = cookies.get('at') || 'dummyvalue',
  rtToken = cookies.get('rt'),
  headers = {
    'at': atToken,
    ...getLocationHeaders(cookies),
    ...getFingerPrintHeaders(req, cookies)
  };
  if(rtToken){
    headers.rt = rtToken
  }

  client.get(config('isNewUser'), headers).end(function (err, response) {
    if (err) {
      res.send(500, {
        error: err
      });
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

router.post('/' + ROUTES.locationContext.path, middleware, (req, res) => {
  const cookies = require('@myntra/myx/lib/server/cookies')(req, res),
    atToken = cookies.get('at') || 'dummyvalue',
    rtToken = cookies.get('rt'),
    headers = {
      'at': atToken,
      ...getLocationHeaders(cookies),
      ...getFingerPrintHeaders(req, cookies)
    };
  if(rtToken){
    headers.rt = rtToken
  }
  const params = req.body;

  client.post(config('locationContext'), params, headers).end(function (err, response) {
    if (client.errorHandler(err, response, res)) {
      res.send(500, {
        error: err
      });
      return;
    }
    handleAuthTokenUpdate(req, res, response);
    res.send(200, response.body);
  });
});

export default router;
